<?php


namespace app\admin\model;


use think\Model;
use think\Validate;

class VideoComentsModel extends Model
{
    protected $name ='video_coments';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $autoWriteTimestamp = true;
    public function users(){
        return  $this->hasOne(UsersModel::class,'id','uid')->setEagerlyType(0);
    }
    public function getValidate(){
        $validate = new Validate([
            'uid'  => 'require',
            'content' => 'require|min:3',

        ]);
        return $validate;
    }
}