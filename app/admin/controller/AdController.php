<?php

/**
 * 商家分类
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class AdController extends AdminbaseController {
    function index(){
			
        $data = $this->request->param();
        $map[] =['A.status','=',1];
        $id=isset($data['id']) ? $data['id']: '';
        if($id!=''){
             $map[]=['A.id','=',$id];
        }
        $name=isset($data['name']) ? $data['name']: '';
        if($name!=''){
            $map[]=['A.name','like','%'.$name.'%'];
        }
        if(!isset($data['name'])){
            $data['name'] = '';
        }
        $list = Db::name('shop_apply')->alias('A')
            ->field("A.*,B.thumb1,thumb2,thumb3,thumb4")
            ->leftJoin('ad B','A.id = B.shop_id')
            ->where($map)
            ->order('id DESC')
            ->paginate(20,false,['query'=>['name'=>$data['name']]]); 
            
        $list->each(function($v,$k){
           if($v['thumb1']){
                 $v['thumb1']=get_upload_path($v['thumb1']);

           }
           
           if($v['thumb2']){
                 $v['thumb2']=get_upload_path($v['thumb2']);

           }
                       
           if($v['thumb3']){
                 $v['thumb3']=get_upload_path($v['thumb3']);

           }
                      
           if($v['thumb4']){
                 $v['thumb4']=get_upload_path($v['thumb4']);

           }
           return $v; 
        });     
            
            
            
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);
            
        return $this->fetch(); 
    }
		
    function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('ad')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        
        $action="删除商家分类：{$id}";
        setAdminLog($action);
                    
        $this->resetcache();
        $this->success("删除成功！");				
    }		
    //排序
    public function listOrder() { 
		
        $model = DB::name('ad');
        parent::listOrders($model);
        
        
        $this->resetcache();
        $this->success("排序更新成功！");
    }	
    

    function add(){        
        return $this->fetch();
    }	
    function addPost(){
        if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$name=$data['name'];

			if($name==""){
				$this->error("请填写名称");
			}
			$thumb=$data['thumb'];
			if($thumb==""){
				$this->error("请上传图标");
			}

            $des=$data['url'];
            if($des==''){
                $this->error("请填写url");
            }

        
            
			$id = DB::name('ad')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }
            

            
            $this->resetcache();
            $this->success("添加成功！");
            
		}
    }		
    function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('ad')
            ->where("note_id={$id}")
            ->find();
        
        $this->assign('data', $data);
        $this->assign('note_id', $id);
        return $this->fetch(); 			
    }
    
    function editPost(){
        if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            $note_id=$data['note_id'];
            $r=Db::name('ad')
            ->where("note_id={$note_id}")
            ->find();
            if($r){
                $id = DB::name('ad')->where("note_id={$note_id}")->update($data);
            }else{
               $id = DB::name('ad')->insertGetId($data);
            }
             if(!$id){
                $this->error("添加失败！");
            }
            $this->success("修改成功！");
		}	
    }
    
    function sedit(){
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('ad')
            ->where("shop_id={$id}")
            ->find();
        
        $this->assign('data', $data);
        $this->assign('shop_id', $id);
        return $this->fetch(); 	
        
        
    }
    
    
    public function editShop(){
           if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            $note_id=$data['shop_id'];
            $r=Db::name('ad')
            ->where("shop_id={$note_id}")
            ->find();
            if($r){
                $id = DB::name('ad')->where("shop_id={$note_id}")->update($data);
            }else{
               $id = DB::name('ad')->insertGetId($data);
            }
             if(!$id){
                $this->error("添加失败！");
            }
            $this->success("修改成功！");
		}	
        
        
    }
    
    function resetCache(){
        $key='getShopClass';
        $rules= DB::name('ad')
                ->order('list_order asc,id desc')
                ->select();
        if($rules){
            setcaches($key,$rules);
        }else{
			delcache($key);
		}
        
        return 1;
    }
    
    public function note(){
        $data = $this->request->param();
        $judge = isset($data['judge']) ? $data['judge']: 0;
        $map[] =['draft','=',0];
        $id=isset($data['id']) ? $data['id']: '';
        if($id!=''){
            $map[]=['A.id','=',$id];
        }
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $map[]=['A.uid','=',$uid];
        }
        $list = Db::name('dynamic')->alias('A')
            ->where($map)->field("A.*,C.name as sName,D.user_nickname")->leftJoin('shop_apply C','A.shop_id = C.id')
            ->leftJoin('user D','A.uid = D.id')
            ->order('id desc')
            ->paginate(20);
        $page = $list->render();
        $this->assign("page", $page);    
        $this->assign('list', $list);
        $this->assign("judge", $judge);
        return $this->fetch(); 	  
    }
    public function shop(){
        $data = $this->request->param();
        $judge = isset($data['judge']) ? $data['judge']: 0;
        $map[] =['status','=',1];
        $id=isset($data['id']) ? $data['id']: '';
        if($id!=''){
            $map[]=['id','=',$id];
        }
        $list = Db::name('shop_apply')
            ->where($map)->field("name,id")
            ->paginate(20);
        $page = $list->render();
        $this->assign("page", $page);    
        $this->assign('list', $list);
        $this->assign("judge", $judge);
        return $this->fetch(); 	  
    }
    
    //帖子列表    
    public function noteAd(){
        $data = $this->request->param();
        $map[] =['draft','=',0];
        $id=isset($data['id']) ? $data['id']: '';
        if($id!=''){
             $map[]=['A.id','=',$id];
        }
        $title=isset($data['title']) ? $data['title']: '';
        if($title!=''){
            $map[]=['A.title','like','%'.$title.'%'];
        }
        $list = Db::name('dynamic')->alias('A')
            ->where($map)->field("A.*,D.user_nickname,B.thumb1,thumb2,thumb3,thumb4")
            ->leftJoin('user D','A.uid = D.id')->leftJoin('ad B','A.id = B.note_id')
            ->order('A.id DESC')
            ->paginate(20); 
            
                
        $list->each(function($v,$k){
           if($v['thumb1']){
                 $v['thumb1']=get_upload_path($v['thumb1']);

           }
           
           if($v['thumb2']){
                 $v['thumb2']=get_upload_path($v['thumb2']);

           }
                       
           if($v['thumb3']){
                 $v['thumb3']=get_upload_path($v['thumb3']);

           }
                      
           if($v['thumb4']){
                 $v['thumb4']=get_upload_path($v['thumb4']);

           }
           return $v; 
        });    
            
            
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('list', $list);
            
        return $this->fetch(); 
    }
    
    
    
}
