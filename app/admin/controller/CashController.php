<?php


namespace app\admin\controller;


use app\admin\model\ScannelCashModel;
use app\api\model\ShopModel;
use app\admin\model\UsersModel;
use cmf\controller\AdminBaseController;

class CashController extends AdminBaseController
{
    public function index(){
        $status  = [
            -1=>'全部',
            0=>'申请中',
            1=>'已通过',
            2=>'已拒绝',
            3=>'已完成',
        ];
        $bank_name = $this->request->request('bank_name','','trim');
        $bank_account = $this->request->request('bank_account','','trim');
        $key = $this->request->request('key','','trim');
        $start_time = $this->request->request('start_time','','trim');
        $end_time = $this->request->request('end_time','','trim');
        $time_type = $this->request->request('time_type','','trim');
        $uid = $this->request->request('uid','','trim');
        $shopid = $this->request->request('shopid','','trim');
        $where = function ($q)use($bank_account,$bank_name,$key,$start_time,$end_time,$time_type,$uid,$shopid,$status){
            if($bank_account){
                $q->where('bank_account');
            }
            if($bank_name){
                $q->where('bank_name',$bank_name);
            }
            if($uid){
                $q->where('uid',$uid);
            }
            if($shopid){
                $q->where('shopid',$shopid);
            }

            if($time_type == 1){
                $str = 'createtime';
            }else{
                $str = 'handle_time';
            }
            if($start_time){
                $q->where($str,'>=',strtotime($start_time));
            }
            if($end_time){
                $q->where($str,'<=',strtotime($end_time));
            }
            $status = $this->request->request('status',-1);

            if($status != -1){
                $q->where('status',$status);
            }

            if($key){
                $q->where(function($r)use($key){
                    $r->whereOr('bank_account','LIKE','%'.$key.'%');
                    $r->whereOr('bank_name','LIKE','%'.$key.'%');
                    $r->whereOr('shopid','LIKE','%'.$key.'%');
                    $r->whereOr('bank_cardid','LIKE','%'.$key.'%');
                    $r->whereOr('bank_username','LIKE','%'.$key.'%');
                    $r->whereOr('mobile','LIKE','%'.$key.'%');
                    $r->whereOr('company_name','LIKE','%'.$key.'%');
                    $r->whereOr('ssm_no','LIKE','%'.$key.'%');
                    $r->whereOr('swift_code','LIKE','%'.$key.'%');
                });
            }

        };
        $list = ScannelCashModel::where($where)->order('id','desc')->paginate(25);
        return $this->fetch('',[
            'list'  => $list,
            'status' => $status
        ]);
    }

    public function verify(){
        if($this->request->isGet()){
            $id = input('id');
            $info = ScannelCashModel::where('id',$id)->find();
            return $this->fetch('',['data'=>$info]);
        }
        $status = $this->request->request('status',0);
        $reason = $this->request->request('reason','');
        if(!$status){
            $this->error('请选择操作结果');
        }
        $id = input('id');
        $info = ScannelCashModel::find($id);
        if(!$info){
            $this->error('不存在该信息');
        }
       
        if(input('yjwc')){
            if($info['status'] != 1){
                $this->error('该提现无法操作为 已完成');
            }
            $info->status = 3;
            $info->handle_time = time();
            $info->save();
            $this->success('操作成功',url('/admin/cash/edit',['id'=>$id]));
        }
         if($info['status'] != 0){
            $this->error('该信息已经被处理了');
        }
        
        $info->handle_time = time();
        $info->status = $status;
        $info->reason = $reason;
        $bool = $info->save();
        if($bool){
            if($status == 2){ //拒绝时退还
                if($info['uid'] > 0){
                    $rs = UsersModel::where('id',$info['uid'])->setInc('coin',$info['coin']);
                }else{
                     $rs = ShopModel::where('id',$info['shopid'])->setInc('coin',$info['coin']);
                }
                
            }
            $this->success('操作成功');
        }else{
            $this->error('操作失败');
        }
    }

    public function edit(){

        if($this->request->isGet()){
            $id = input('id');
            $info = ScannelCashModel::where('id',$id)->find();
            return $this->fetch('',['data'=>$info]);
        }else{
            $id = input('id');
            $info = ScannelCashModel::where('id',$id)->find();
            $info->status = 3;
            $info->handle_time = time();
            $info->save();
            $this->success('操作成功');
        }
    }
}