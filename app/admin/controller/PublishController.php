<?php

/* 帖子管理 */

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class PublishController extends AdminBaseController
{

    protected function getStatus($k = '')
    {
        $status = [
            "0" => "待审核",
            "1" => "审核通过",
            "-1" => "审核拒绝",
        ];
        if ($k == '') {
            return $status;
        }
        return isset($status[$k]) ? $status[$k] : '';
    }

    protected function initialize()
    {
        parent::initialize();
        $res = Db::name('shop_class')->field('id,name')->select();
        $topic = Db::name('shop_topic')->field('topic_id,topic_name')->group('topic_name')->select();
        $this->assign('topicData', $topic);
        $this->assign('type', $res);
    }


    public function index()
    {

        $data = $this->request->param();
        $map = [];
        $status = isset($data['status']) ? $data['status'] : '';
        if ($status != '') {
            $map[] = ['status', '=', $status];
        }

        $isdel = isset($data['isdel']) ? $data['isdel'] : '';
        if ($isdel != '') {
            $map[] = ['isdel', '=', $isdel];
        }

        $type = isset($data['type']) ? $data['type'] : '';
        if ($type != '') {
            $map[] = ['A.cat_id', '=', $type];
        }


        $uid = isset($data['uid']) ? $data['uid'] : '';
        if ($uid != '') {
            $map[] = ['A.uid', '=', $uid];
        }
        $name = isset($data['name']) ? $data['name'] : '';
        if ($name != '') {
            $map[] = ['C.name', 'like', '%' . $name . '%'];
        }

        $keyword = isset($data['keyword']) ? $data['keyword'] : '';
        if ($keyword != '') {
            $map[] = ['A.title', 'like', '%' . $keyword . '%'];
        }

        $ordertype = isset($data['ordertype']) ? $data['ordertype'] : 'A.id DESC';
        $map[] = ['draft', '=', 0];

        $list = Db::name('dynamic')->alias('A')
            ->where($map)->field("A.*,B.name as cName,C.name as sName,D.user_nickname")
            ->leftJoin('shop_class B', 'A.cat_id = B.id')->leftJoin('shop_apply C', 'A.shop_id = C.id')
            ->leftJoin('user D', 'A.uid = D.id')
            ->order($ordertype)
            ->paginate(20);


        $list->each(function ($v, $k) {
            $v['userinfo'] = getUserInfo($v['uid']);
            if ($v['thumb']) {

                //   $thumbs=preg_split('/;/',$v['thumb']);
                $thumbs = explode(',', $v['thumb']);

                // var_dump($thumbs);

                $thumb = [];
                foreach ($thumbs as $k1 => $v1) {
                    $thumb[$k1] = get_upload_path($v1);
                }
                $v['thumb'] = $thumb;
            }
            if ($v['video']) {
                $videos = explode(',', $v['video']);
                $video = [];
                foreach ($videos as $k1 => $v1) {
                    $video[$k1] = get_upload_path($v1);
                }
                $v['video'] = $video;
            }


            return $v;
        });
        $list->appends($data);

        $page = $list->render();
        $this->assign("page", $page);

        $this->assign('list', $list);

        $this->assign('status', $this->getStatus());


        $this->assign('status2', $status);
        $this->assign('isdel2', $isdel);

        return $this->fetch('index');
    }

    public function wait()
    {
        return $this->index();
    }

    public function nopass()
    {
        return $this->index();
    }

    public function lower()
    {
        return $this->index();
    }

    public function see()
    {
        $id = $this->request->param('id', 0, 'intval');

        $data = Db::name('dynamic')
            ->where("id={$id}")
            ->find();
        if (!$data) {
            $this->error("信息错误");
        }

        if ($data['thumb']) {
            $thumbs = preg_split('/;/', $data['thumb']);
            $thumb = [];
            foreach ($thumbs as $k1 => $v1) {
                $thumb[] = get_upload_path($v1);
            }
            $data['thumb'] = $thumb;
        }

        $data['href'] = get_upload_path($data['href']);
        $data['voice'] = get_upload_path($data['voice']);

        $this->assign('data', $data);
        return $this->fetch();
    }

    public function setstatus()
    {
        $id = $this->request->param('id', 0, 'intval');
        if (!$id) {
            $this->error("数据传入失败！");
        }
        $status = $this->request->param('status', 0, 'intval');

        $nowtime = time();

        $rs = DB::name("dynamic")->where("id={$id}")->update(['status' => $status]);
        if (!$rs) {
            $this->error("操作失败");
        }

        $this->success("操作成功");
    }
    
    
    public function is_recommend()
    {
        $id = $this->request->param('id', 0, 'intval');
        if (!$id) {
            $this->error("数据传入失败！");
        }
        $is_recommend = $this->request->param('is_recommend', 0, 'intval');

        $nowtime = time();

        $rs = DB::name("dynamic")->where("id={$id}")->update(['is_recommend' => $is_recommend]);
        if (!$rs) {
            $this->error("操作失败");
        }

        $this->success("操作成功");
    }
    public function setrecom()
    {
        $id = $this->request->param('id', 0, 'intval');
        if (!$id) {
            $this->error("数据传入失败！");
        }
        $recoms = $this->request->param('recoms', 0, 'intval');

        $nowtime = time();

        $rs = DB::name("dynamic")->where("id={$id}")->update(['recommend_val' => $recoms]);
        if ($rs === false) {
            $this->error("操作失败");
        }

        $this->success("操作成功");
    }

    /* 上下架 */
    public function setDel()
    {
       
        $id = $this->request->param('id', 0, 'intval');
        $isdel = $this->request->param('isdel', 0, 'intval');
        $reason = $this->request->param('reason');
        if ($reason == '') {
            $reason = '';
        }
        if ($isdel == 1) {
            $status = 2;
        } else {
            $status = 1;
        }

        if ($id) {

            //判断用户是否注销
            $uid = DB::name("dynamic")->where(['id' => $id])->value("uid");
            if ($uid) {
                $is_destroy = checkIsDestroy($uid);


                if ($is_destroy && $isdel == 0) {
                    $this->error("该用户已注销,动态不可上架");
                }
            }


            $result = DB::name("dynamic")->where(['id' => $id])->update([
                'isdel' => $isdel,
                'status' => $status,
                'reason' => $reason]);

            if ($result) {
                // if($isdel==1){
                //     //将视频喜欢列表的状态更改
                //     DB::name("dynamic_like")->where("dynamicid={$id}")->setField('status',0);

                //     //更新此视频的举报信息
                //     $data1=array(
                //         'status'=>1,
                //         'uptime'=>time()
                //     );

                //     DB::name("dynamic_report")->where("dynamicid={$id}")->update($data1);
                // }

                // if($isdel==0){
                //     //将视频喜欢列表的状态更改
                //     DB::name("dynamic_like")->where("dynamicid={$id}")->setField('status',1);
                // }


            }
            reflashUserLogMoney($uid);
            $this->success('操作成功');

        } else {
            $this->error('数据传入失败！');
        }

    }

    public function del()
    {
        $data = $this->request->param();

        if (isset($data['id'])) {
            $id = $data['id']; //获取删除id
            $dynamic = Db::name('dynamic')->where("id={$id}")->find();
            $uid = $dynamic['uid'];
          
            $rs = DB::name('dynamic')->where("id={$id}")->delete();
            if (!$rs) {
                $this->error("删除失败！");
            }
            //删除评论
            DB::name("note_comment")->where("note_id={$id}")->delete();
            //删除点赞
            DB::name("note_like")->where("note_id={$id}")->delete();
            //删除收藏
            DB::name("collect")->where(['collect_id' => $id, 'type' => 1])->delete();
            //删除浏览记录
            DB::name("invitation_visit")->where("note_id={$id}")->delete();
           //查询帖子阅读量
            reflashUserLogMoney($uid);


        } elseif (isset($data['ids'])) {
            $ids = $data['ids'];
            $uids = DB::name('dynamic')->where('id', 'in', $ids)->column('uid');
            $rs = DB::name('dynamic')->where('id', 'in', $ids)->delete();
            if (!$rs) {
                $this->error("删除失败！");
            }

            DB::name("note_comment")->where('note_id', 'in', $ids)->delete();
            //删除点赞
            DB::name("note_like")->where('note_id', 'in', $ids)->delete();
            //删除收藏
            DB::name("collect")->where(['type' => 1])->where('collect_id', 'in', $ids)->delete();
            //删除浏览记录
            DB::name("invitation_visit")->where('note_id', 'in', $ids)->delete();
            foreach ($uids as $v){
                reflashUserLogMoney($v['uid']);
            }
        }

        $this->success("删除成功！");
    }

    //详情
    public function edit()
    {
        $id = $this->request->param('id');

        $class = Db::name('shop_class')->select();

        $this->assign('class', $class);

        $info = Db::name('dynamic')->where(['id' => $id])->find();
        //  print_r($info['custom']);exit;
        // if($info['custom']){
        $info['custom'] = $info['custom'] ? explode(',', $info['custom']) : [];
        //}


        // foreach ($info as $k => $v) {
        //     $info[$k]['thumb'] = explode(',', $v['thumb']);
        // }
        $info['thumb'] = explode(',', $info['thumb']);
        foreach ($info['thumb'] as $k => $v) {
            $info['thumb'][$k] = get_upload_path($v);
        }
        $info['video'] = explode(',', $info['video']);
        foreach ($info['video'] as $k => $v) {
            $info['video'][$k] = get_upload_path($v);
        }
        // $info['status'] = $this->getStatus($info['status']);
        // $res=Db::name("dynamic")->alias('A')
        //     ->field("A.*,B.name as cName,C.name as sName,D.user_nickname")
        //     ->leftJoin('shop_class B','A.cat_id = B.id')->leftJoin('shop_apply C','A.shop_id = C.id')
        //     ->leftJoin('user D','A.uid = D.id')->where("A.id={$id}")->find();
        // if($res['thumb']){
        //   $res['thumb'] = explode(',', $res['thumb']);
        //   foreach ($res['thumb'] as &$val){
        //       $val =  get_upload_path($val);
        //   }

        // }    
        // print_r($res);exit;  
        $this->assign('id', $id);
        $this->assign('info', $info);
        $this->assign("status", $this->getStatus());
        return $this->fetch();
    }

    public function editPost()
    {

        $data = $this->request->param();

        $data['uid'] ?: $this->error('用户id不能为空!');
        $data['title'] ?: $this->error('帖子标题不能为空!');
        $data['content'] ?: $this->error('帖子内容不能为空!');
        $data['thumb'] ?: $this->error('帖子图片不能为空!');
        //$data['shop_id'] ?: $this->error('帖子所属店铺不能为空!');
        isset($data['custom']) ?$data['custom']: $this->error('帖子话题不能为空!');

        if(!empty($data['video'])){
            $data['video'] = implode(',', $data['video']);
        }
        $id = $data['id'];
        unset($data['id']);
        if ($data['custom']) {
            $data['custom'] = implode(',', $data['custom']);
        }
        $data['thumb'] = implode(',', $data['thumb']);
        // $latlng = explode(',', $data['latlng']);
        // $data['lat'] = $latlng[0];
        // $data['lng'] = $latlng[1];
        $data['addtime'] = time();
        // unset($data['latlng']);
        // var_dump($data);die;

        $res = Db::name('dynamic')->where('id', $id)->update($data);

        if ($res) {
            return $this->success('编辑成功!');
        } else {
            return $this->error('编辑失败!');
        }




    }


    public function add()
    {

        $class = Db::name('shop_class')->select();

        // var_dump($class);die;

        $this->assign('class', $class);
        return $this->fetch();


    }


    public function addPost()
    {

        $data = $this->request->param();


        $data['uid'] ?: $this->error('用户id不能为空!');
        $data['title'] ?: $this->error('帖子标题不能为空!');
        $data['content'] ?: $this->error('帖子内容不能为空!');
        $data['thumb'] ?: $this->error('帖子图片不能为空!');
        $data['shop_id'] ?: $this->error('帖子所属店铺不能为空!');
        $data['custom'] ?: $this->error('帖子话题不能为空!');

        $data['thumb'] = implode(',', array_filter($data['thumb']));
        //  $latlng = explode(',', $data['latlng']);
        // $data['lat'] = $latlng[0];
        // $data['lng'] = $latlng[1];
        $data['addtime'] = time();
        // unset($data['latlng']);
        // var_dump($data);die;
        if ($data['custom']) {
            $data['custom'] = implode(',', $data['custom']);
        }
        $res = Db::name('dynamic')->insert($data);

        if ($res) {
            return $this->success('新增成功!');
        } else {
            return $this->error('新增失败!');
        }

        // var_dump($data);die;

    }
    public function is_top(){
        $id = input('id');
        $is_top = input('is_top');
        $res = Db::name('dynamic')->where('id',$id)->update(['is_top'=>$is_top]);
        return $this->success('操作成功');
         
    }


}