<?php


namespace app\admin\controller;


use app\api\model\ShopLogModel;
use app\api\model\ShopModel;
use cmf\controller\AdminBaseController;

class ShopLogController extends AdminBaseController
{
    public function index(){
        $shop_id = input('shop_id');
        $where = [];
        if($shop_id){
            $where['shop_id'] = [
                '=',
                $shop_id
            ];
        }
        $list = ShopLogModel::with('shops')->where($where)->paginate(25);
        
        return $this->fetch('',[
           'list' => $list
        ]);
    }
    public function manual(){
        if($this->request->isGet()){
            return $this->fetch();
        }else{
            $shopid = input('shop_id');
            $action = input('action');
            $type = input('trans_type');
            $change_coin = input('change_coin');
            if($type == 1){
                $method = 'setInc';
            }else{
                $method = 'setDec';
            }
            call_user_func([ShopModel::where('id',$shopid),$method],'coin',$change_coin);
            $ret = ShopLogModel::addlog($shopid,$change_coin,$action,$type);

            if($ret){
                return $this->success('添加成功');
            }else{
                return $this->success('添加失败');
            }
        }
    }
}