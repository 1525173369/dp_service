<?php
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use think\db\Query;

/**
 * Class UserController
 * @package app\admin\controller
 * @adminMenuRoot(
 *     'name'   => '管理组',
 *     'action' => 'default',
 *     'parent' => 'user/AdminIndex/default',
 *     'display'=> true,
 *     'order'  => 10000,
 *     'icon'   => '',
 *     'remark' => '管理组'
 * )
 */
class UsersController extends AdminBaseController
{


    public function index()
    {
        $data = $this->request->param();
        $map=[];
        $uid=isset($data['uid']) ? $data['uid']: '';
        $judge = isset($data['judge']) ? $data['judge']: 0;
        if($uid!=''){
             $map[]=['id','=',$uid];
        }
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['mobile|user_nickname','like','%'.$keyword.'%'];
        }
        $users = Db::name('user')
            ->where('user_type', 2)
            ->where($map)
            ->order("id DESC")
            ->paginate(10);
        $users->each(function($v,$k){
	
			$v['avatar']=get_upload_path($v['avatar']);

            return $v;           
        });
        // 获取分页显示
        $page = $users->render();
        $this->assign("judge", $judge);
        $this->assign("page", $page);
        $this->assign("users", $users);
        return $this->fetch();
    }
    public function shop()
    {
        $data = $this->request->param();
        $map=[];
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
             $map[]=['id','=',$uid];
        }
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['name','like','%'.$keyword.'%'];
        }
        $users = Db::name('shop_apply')
            ->where('status', 1)
            ->where($map)
            ->order("id DESC")
            ->paginate(10);
        $users->each(function($v,$k){

			$v['thumb']=get_upload_path($v['thumb']);

            return $v;
        });
        // 获取分页显示
        $page = $users->render();
        $this->assign("page", $page);
        $this->assign("users", $users);
        return $this->fetch();
    }

    public function topic()
    {
        $data = $this->request->param();
        $map=[];
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $map[]=['id','=',$uid];
        }
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['name','like','%'.$keyword.'%'];
        }
        $users = Db::name('shop_apply')
            ->where('status', 1)
            ->where($map)
            ->order("id DESC")
            ->paginate(10);
        $users->each(function($v,$k){

            $v['thumb']=get_upload_path($v['thumb']);

            return $v;
        });
        // 获取分页显示
        $page = $users->render();
        $this->assign("page", $page);
        $this->assign("users", $users);
        return $this->fetch();
    }
  
}
