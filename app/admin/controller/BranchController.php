<?php
/**
 * 门店管理
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class BranchController extends AdminBaseController {
    


    public function index(){       
        
        $data = $this->request->param();
        $map=[];
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['shop_name','like','%'.$keyword.'%'];
        }
        
        $list = Db::name("branch")
            ->where($map)
            ->order("id desc")
            ->paginate(20);
        $list->each(function($v,$k){
            $v['address']= urldecode($v['address']);
            return $v;           
        });    
            
        // return $list;exit;    
        $page = $list->render();
        $this->assign("page", $page);
        $this->assign("list",$list);
        
        return $this->fetch();
        
    }
    


    
    function add(){

        $pid=0;
        $data = $this->request->param();
        if($data){
            $pid=$data['pid'];
        }
       
        $this->assign('pid', $pid);
        return $this->fetch();
    }
    
    function addPost(){
        if ($this->request->isPost()) {
            
            $data = $this->request->param();

            $name=$data['shop_name'];

            if($name==""){
                $this->error("请填写名称");
            }
            
            $data['create_time'] = time();
            $address=$data['address'];
            if($address==''){
                $this->error("请填写搜索地址");
            }

            $id = DB::name('branch')->insert($data);

            if(!$id){
                $this->error("添加失败！");
            }
            
            $this->success("添加成功！");
            

        //获取门店详情

        }
    }
    
        //获取门店详情

    public function edit(){
        $id=$this->request->param('id');
        $info=Db::name("branch")->where("id={$id}")
        ->find();
        $info['address'] = urldecode($info['address']);
        if(!$info){
            $this->error("门店不存在");
        }

        $this->assign('data',$info);

        return $this->fetch();
    }
    
    public function editPost(){
            $data = $this->request->param();
            
            // var_dump($data);exit;
            $name=$data['shop_name'];

            if($name==""){
                $this->error("请填写名称");
            }



            $address=$data['address'];
            
            $datas['shop_name'] = $data['shop_name'];
            $datas['address'] = $data['address'];
            $datas['lat'] = $data['lat'];
            $datas['lng'] = $data['lng'];
            if($address==''){
                $this->error("请填写搜索地址");
            }
            $datas['update_time'] = time();
            
            // print_r($datas);exit;
            $rs = DB::name('branch')->where("id={$data['id']}")->update($datas);
            
            if(!$rs){
                $this->error("编辑失败！");
            }
            
            $this->success("编辑成功！");

        return $this->fetch();
    }
    
    
    public function del()
    {
        
        $data=$this->request->param();
        $info=Db::name("shop_apply")->where("branch={$data['id']}")->order("id asc")->find();
        if($info){
            $this->error("该门店已绑定店铺,无法删除！");
            return;
        }
        
        // $map[]=['branch','like','%'.$data['id'].','.'%'];
        // $info_c=Db::name("shop_apply")->where($map)->order("id asc")->find();
        
        // print_r($map);exit;
        

        if(isset($data['id'])){

            $id = $data['id'];

            $rs = DB::name('branch')->where("id={$id}")->delete();
            if(!$rs){
                $this->error("删除失败！");
            }

        }else if(isset($data['ids'])){
            $ids=$data['ids'];
            $rs = DB::name('branch')->where("id",'in',$ids)->delete();
            if(!$rs){
                $this->error("删除失败！");
            }
        }

        
        
        $this->success("删除成功！",url("branch/index"));
    }

}