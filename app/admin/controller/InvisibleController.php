<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Validate;
use think\Db;
class InvisibleController extends AdminBaseController
{




    public function index(){
        $res = Db::name('agreement')->where("id=3")->find();
        $this->assign('res', $res);
        return $this->fetch();
        
    }
    public function editPost(){
        $data           = $this->request->param();
         if(isset($data['content'])){
            $data['content'] = htmlspecialchars_decode($data['content']);
        }
    	$id = DB::name('agreement')->where("id=3")->update($data);
        $this->success("保存成功！");
    }

    public function appear(){
        $res = Db::name('agreement')->where("id=4")->find();
        $this->assign('res', $res);
        return $this->fetch();
        
    }
    public function appearPost(){
        $data           = $this->request->param();
         if(isset($data['content'])){
            $data['content'] = htmlspecialchars_decode($data['content']);
        }
    	$id = DB::name('agreement')->where("id=4")->update($data);
        $this->success("保存成功！");
    }

}

