<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Validate;
use think\Db;
class AgreementController extends AdminBaseController
{


    public function index()
    {
        $res = Db::name('agreement')->where("id=1")->find();
        $this->assign('res', $res);
        return $this->fetch();
    }

    public function  editPost()
    {
        $data           = $this->request->param();
         if(isset($data['content'])){
            $data['content'] = htmlspecialchars_decode($data['content']);
        }
    	$id = DB::name('agreement')->where("id=1")->update($data);
        $this->success("保存成功！");
    }

    public function about(){
        $res = Db::name('agreement')->where("id=2")->find();
        $this->assign('res', $res);
        return $this->fetch();
        
    }
    public function aboutPost(){
        $data           = $this->request->param();
         if(isset($data['content'])){
            $data['content'] = htmlspecialchars_decode($data['content']);
        }
    	$id = DB::name('agreement')->where("id=2")->update($data);
        $this->success("保存成功！");
    }

}

