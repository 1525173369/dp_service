<?php


namespace app\admin\controller;


use app\admin\model\VideoComentsModel;
use app\admin\model\VideosModel;
use cmf\controller\AdminBaseController;
use Think\Db;
use think\Validate;

class VideoController extends AdminbaseController
{
    protected $model;
    protected $validate;
    protected function initialize()
    {
        parent::initialize();
        $res = Db::name('shop_class')->field('id,name')->select();
        $topic = Db::name('shop_topic')->field('topic_id,topic_name')->group('topic_name')->select();
        $this->assign('topicData', $topic);
        $this->assign('type', $res);
        $model = new VideosModel();
        $this->model = $model;

        $validate = new Validate([
            'uid'  => 'require',
            'desc' => 'require|min:6',
            'shop_id' => 'require',
            'video_url' => 'require',
            'likes' => 'require',
            'comments' => 'require',
            'collects' => 'require',
            'shares'  => 'require',
            'hot' =>  'require',
            'visits' =>  'require',
        ]);
        $this->validate = $validate;  //验证类

    }

    public function indexs(){
        $model = $this->model;
        $uid = $this->request->request('uid','','trim');
        $shopname = $this->request->request('shopname','','trim');
        $keyword = $this->request->request('keyword','','trim');
        $where = [];
        $where = function($q)use($uid,$shopname,$keyword){
            if($uid){
                $q->where('videos_model.uid',$uid);
            }
            if($shopname){
                $q->where('shops.name','LIKE','%'.$shopname.'%');
            }
            if($keyword){
                $q->where('videos_model.desc','LIKE','%'.$keyword.'%');
            }
            //$q->where('videos_model.is_delete',0);
        };

        $list = $model->with(['users','shops'])->where($where)->order('id','desc')->paginate(20);

        $this->assign('list',$list);
        return $this->fetch();
    }


    public function adds(){  //
        if($this->request->isPost()){
            $data = $this->request->request();
            $check = $this->validate->check($data);
            if(!$check){
                $this->error($this->validate->getError());
            }
            if(!isset($data['video_img']) || !$data['video_img']){
                $data['video_img'] = $this->model->convertToFlv($data['video_url']);
            }

            $bool = VideosModel::create($data);
            if($bool){
                 $this->success('添加成功');
            }
            $this->error('添加失败');
        }else{
            return $this->fetch();
        }
    }


    public function edits($id){
        $info = $this->model->find($id);
        $this->assign('id',$id);
        if($this->request->isPost()){
            $data = $this->request->post();
            $check = $this->validate->check($data);
            if(!$check){
                $this->error($this->validate->getError());
            }
            if(!isset($data['video_img']) || !$data['video_img'] || $data['video_img'] == ""){
                $data['video_img'] = $this->model->convertToFlv($data['video_url']);
            }
            $data['updatetime']= time();
            $bool = $this->model->where('id',$id)->update($data);
           
            $bool?$this->success('编辑成功'):$this->error('编辑失败');
        }else{
            $this->assign('info',$info);
            return $this->fetch();
        }
    }

    public function multe($id){
        $info = $this->model->find($id);
        if($info['status'] == 0){
            $status = 1;
        }else{
            $status = 0;
        }
        $data = [
            'status' => $status,
            'updatetime' => time()
        ];
        $bool = $this->model->where('id',$id)->update($data);
        if($bool){
            $this->success('操作成功');
        }
        $this->error('操作失败');
    }

    public function del($id = null){
        $ids =  $this->request->request('ids');
        if($ids){
            $id  = $ids;
        }else{
            if(!$id){
                $id = $this->request->request('id','','trim');
            }

            if(!$id){
                $this->error('请传入id');
            }
        }


        $bool = $this->model->SoftDelete($id);
        if($bool){
            $this->success('删除成功');
        }
        $this->error('删除失败');
    }

    public function comments($id=false){
        if(!$id){
            $id = $this->request->request('id','','trim');
        }
        if(!$id){
            $this->error('请传入id');
        }
        $uid = $this->request->request('uid','','trim');
        $keyword = $this->request->request('keyword','','trim');
        $where = function ($q)use($uid,$keyword){
            if($uid){
                $q->where('video_coments_model.uid',$uid);
            }
            if($keyword){
                $q->where('content','LIKE','%'.$keyword.'%');
            }
        };
        $query = [
            'keyword' => $keyword,
            'uid' => $uid,
            'id' => $id
        ];
        $list = VideoComentsModel::where('video_id',$id)->where($where)->with('users')->paginate(20,[],['query'=>$query]);

        $this->assign('comment_id',$id);
        $this->assign('list',$list);
        return $this->fetch();
    }
    public function comments_del($id=false){
        $ids = $this->request->request('ids');
        if($ids){
            $where = function ($q)use($ids){
                $q->whereIn('id',$ids);
            };
        }else{
            if(!$id){
                $id = $this->request->request('id','','trim');
            }
            if(!$id){
                $this->error('请传入id');
            }
            $where = [
                'id' => $id
            ];
        }

        $bool = VideoComentsModel::where($where)->delete();
        return $bool?$this->success('删除成功'):$this->error('删除失败');
    }


}