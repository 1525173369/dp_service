<?php

/**
 * 商家分类
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class AreaController extends AdminbaseController {
    
        
    protected function initialize()
    {
         parent::initialize();
         $area = Db::name('area')->where('is_hw',0)->select();
         $list = array2level($area);  
         $this->assign('list', $list);
    }
    
    
    
    
    function index(){
    	
    	return $this->fetch();
    }
		
    function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        $pid = DB::name('area')->where(['pid' => $id])->find();
        if(!empty($pid)){
            
             $this->error('此分类下存在子分类，不可删除');
            
            
        }
        $rs = DB::name('area')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        
        $this->success("删除成功！");				
    }		
    //排序
    public function listOrder() { 
		
        $model = DB::name('area');
        parent::listOrders($model);
        
        $action="更新商家分类排序";
        setAdminLog($action);
        
        $this->resetcache();
        $this->success("排序更新成功！");
    }	
    

    function add(){
        $pid=0;
        $data = $this->request->param();
        if($data){
            $pid=$data['pid'];
        }
       
        $this->assign('pid', $pid);
        return $this->fetch();
    }	
    function addPost(){
        if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$name=$data['name'];

			if($name==""){
				$this->error("请填写名称");
			}


            $first=$data['first'];
            if($first==''){
                $this->error("请填写首字母");
            }

			$id = DB::name('area')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }
            
            $this->success("添加成功！");
            
		}
    }		
    function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('area')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $this->assign('data', $data);
        return $this->fetch(); 			
    }
    
    function editPost(){
        if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            
			$name=$data['name'];

			if($name==""){
				$this->error("请填写名称");
			}
            
			$id = DB::name('area')->where("id={$data['id']}")->update($data);
            if($id===false){
                $this->error("修改失败！");
            }
            $this->success("修改成功！");
		}	
    }
    
    function resetCache(){
        $key='getShopClass';
        $rules= DB::name('area')
                ->order('list_order asc,id desc')
                ->select();
        if($rules){
            setcaches($key,$rules);
        }else{
			delcache($key);
		}
        
        return 1;
    }
}
