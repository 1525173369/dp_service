<?php
/**
 * 流水记录
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class ScannelorderController extends AdminBaseController {
    

	public function index(){       
		$data = $this->request->param();
		$map=[];
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        $user_id=isset($data['user_id']) ? $data['user_id']: '';
        if($keyword!=''){
            $map[]=['ordersn','like','%'.$keyword.'%'];
        }
        if($user_id!=''){
            $map[]=['shopid','like','%'.$user_id.'%'];
        }
        
        
		$list = Db::name("scannel_order")
		    ->where($map)
			->order("id desc")
			->paginate(20);
		$page = $list->render();
		$this->assign("page", $page);
		$this->assign("list",$list);
		
		return $this->fetch();
	    
	}
	
	    //获取流水记录详情
    public function info(){
        $id=$this->request->param('id');
        $info=Db::name("scannel_order")->where("id={$id}")
        ->find();
        if(!$info){
            $this->error("流水记录不存在");
        }

        $this->assign('data',$info);

        return $this->fetch();
    }
	

}