<?php

/**
 * 认领管理
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\ShopGoodsModel;
use app\admin\model\GoodsImagesModel;

class AttestationController extends AdminbaseController {
    
    
    protected function getStatus($k=''){
        $status=[
            '0'=>'审核中',
            '1'=>'通过',
            '2'=>'拒绝',
        ];
        if($k==''){
            return $status;
        }
        return isset($status[$k])?$status[$k]:'';
    }
    
    protected function initialize()
    {
         parent::initialize();
    }

    
    function index(){
        $data = $this->request->param();
        $map=[];
        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['A.username','like','%'.$keyword.'%'];
        }
         $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
            $map[]=['A.uid','=',$uid];
        }
         $status=isset($data['status']) ? $data['status']: '';
        if($status!=''){
            $map[]=['A.status','=',$status];
        }
			

    	$lists = Db::name("attestation")->alias('A')
            ->where($map)
            ->field("A.*,B.name as sName,C.user_nickname")
            ->leftJoin('shop_apply B','A.shop_id = B.id')->leftJoin('user C','A.uid = C.id')
                ->order("A.id DESC")
                ->paginate(20);

        $page = $lists->render();
    	$this->assign('lists', $lists);
    	$this->assign("page", $page);
    	$this->assign('status', $this->getStatus());
    	return $this->fetch();
    }
    
  
    function del(){

    	$data=$this->request->param();

    	if(isset($data['id'])){

    		$id = $data['id'];


	        $rs = DB::name('attestation')->where("id={$id}")->delete();
	        if(!$rs){
	            $this->error("删除失败！");
	        }




    	}else if(isset($data['ids'])){
    		$ids=$data['ids'];
    		$rs = DB::name('attestation')->where("id",'in',$ids)->delete();
	        if(!$rs){
	            $this->error("删除失败！");
	        }
    	}

        
        
        $this->success("删除成功！",url("attestation/index"));
    }

    //审核/详情
    function edit(){
        $id             = $this->request->param('id');
    	$info = Db::name("attestation")->alias('A')
            ->field("A.*,B.name as sName,C.user_nickname")
            ->leftJoin('shop_apply B','A.shop_id = B.id')->leftJoin('user C','A.uid = C.id')
                ->where("A.id={$id}")
                ->order("A.id DESC")
                ->find();
        $info['card_front']= get_upload_path($info['card_front']);
        $info['card_back']=  get_upload_path($info['card_back']);
        $info['certificate']=  get_upload_path($info['certificate']);
        $this->assign('data', $info);
    	$this->assign('status', $this->getStatus());
        return $this->fetch();
    }

    //编辑提交
    public function editPost(){
        $data           = $this->request->param();
        $data['uptime']=time();
        $rs = Db::name("attestation")->where("id={$data['id']}")->find();
        $res = Db::name("shop_apply")->where("id={$rs['shop_id']}")->find();
        if(!$res){
             $this->error("没有此店铺");
        }
        if($res['claim']==1 && $data['status']==1){
            $this->error("该店铺已被人认证！");
        }elseif($data['status']==1 && $res['claim']==0){
            Db::name("shop_apply")->where("id={$rs['shop_id']}")->update([
            'uid'=>$rs['uid'],
            'claim'=>1
            ]);
        }elseif($data['status']==0 && $res['claim']==1){
              $this->error("该店铺已被人认证！请不要修改");
        }elseif($data['status']==2 && $res['claim']==1){
              $this->error("该店铺已被人认证！请不要修改");
        }
        $r = Db::name("attestation")->where("id={$data['id']}")->update($data);

        $this->success("保存成功！", url("Attestation/index"));
    }

    //商品评论列表
    public function commentlist(){
        $data = $this->request->param();
        $goods_id=$data['goods_id'];
        $map=[];

        $map[]=['goodsid','=',$goods_id];
        $map[]=['is_append','=','0'];
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['content','like','%'.$keyword.'%'];
        }
            

        $lists = Db::name("shop_order_comments")
                ->where($map)
                ->order("id asc")
                ->paginate(20);
        
        $lists->each(function($v,$k){
            $v['userinfo']=getUserInfo($v['uid']);
            $v['shop_userinfo']=getUserInfo($v['shop_uid']);
            
            if($v['thumbs']){
                $thumb_arr=explode(',',$v['thumbs']);
                foreach ($thumb_arr as $k1 => $v1) {
                    $thumb_arr[$k1]=get_upload_path($v1);
                }
                $v['thumb_arr']=$thumb_arr;
            }else{
                $v['thumb_arr']=[];
            }
            if($v['video_thumb']){
                $v['video_thumb']=get_upload_path($v['video_thumb']);
            }

            if($v['video_url']){
                $v['video_url']=get_upload_path($v['video_url']);
            }
            
            

            //获取追评信息
            $append_comment=Db::name("shop_order_comments")->where("orderid={$v['orderid']} and is_append=1")->find();

            if($append_comment){

                $append_comment['userinfo']=getUserInfo($append_comment['uid']);
                $append_comment['shop_userinfo']=getUserInfo($append_comment['shop_uid']);

                if($append_comment['thumbs']){
                    $thumb_arr=explode(',',$append_comment['thumbs']);
                    foreach ($thumb_arr as $k1 => $v1) {
                        $thumb_arr[$k1]=get_upload_path($v1);
                    }
                    $append_comment['thumb_arr']=$thumb_arr;
                }else{
                   $append_comment['thumb_arr']=[];
                }

                if($append_comment['video_thumb']){
                    $append_comment['video_thumb']=get_upload_path($append_comment['video_thumb']);
                }

                if($append_comment['video_url']){
                    $append_comment['video_url']=get_upload_path($append_comment['video_url']);
                }

            }
            


            $v['append_comment']=$append_comment;


            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

        $this->assign('lists', $lists);
        $this->assign('goods_id', $goods_id);

        $this->assign("page", $page);
        
        return $this->fetch();
    }

    //删除视频评论
    function delComment(){
        $id = $this->request->param('id', 0, 'intval');
        $rs=Db::name("shop_order_comments")->where("id={$id}")->delete();
        if(!$rs){
            $this->error("评论删除失败");
        }

        $this->success("删除成功");
    }

    //评论视频播放
    function videoplay(){
        $data=$this->request->param();
        $url=$data['url'];
        $this->assign('url',$url);

        return $this->fetch();
    }
    
}
