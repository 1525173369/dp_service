<?php

/**
 * 店铺申请
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\ShopApplyModel;
class ShopapplyController extends AdminbaseController {
    protected function getStatus($k=''){
        $status=array(
            '0'=>'待处理',
            '1'=>'审核成功',
            '2'=>'审核失败',
        );
        if($k==''){
            return $status;
        }
        return isset($status[$k])?$status[$k]:'';
    }
    
    protected function initialize()
    {
         parent::initialize();
         $res = Db::name('shop_class')->field('id,name')->select();
         $topic = Db::name('shop_topic')->field('topic_id,topic_name')->select();
         $result = Db::name('branch')->field('id,shop_name')->select();
         $this->assign("option",$result);
         $this->assign('shop_class', $res);
         $this->assign('topicData', $topic);
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $status=isset($data['status']) ? $data['status']: '';
        if($status!=''){
            $map[]=['status','=',$status];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){

                $map[]=['uid','=',$uid];
            
        }
        $name = isset($data['name'])?$data['name']:'';
        if($name != ''){
             $map[]=['A.name','=',$name];
        }
        
        $id = isset($data['id'])?$data['id']:'';
        if($id != ''){
             $map[]=['A.id','=',$id];
        }

        $lists = Db::name("shop_apply")->alias('A')
                ->field("A.*,B.name as classname")
                ->leftJoin('shop_class B','A.classid = B.id')
                ->where($map)
                ->order("A.id DESC")
                ->paginate(20);

        $lists->each(function($v,$k){
            //$v['thumb']=get_upload_path($v['thumb']);
            $v['userinfo']= getUserInfo($v['uid']);
            if($v['uid']==0){
                $v['userinfo']['user_nickname']='无人认领';
            }
            $v['tel']= m_s($v['uid']);
            $v['cardno']=m_s($v['cardno']);
            $v['phone']=m_s($v['phone']);


           
            return $v;           
        });
                
        $lists->appends($data);
        $page = $lists->render();


        $this->assign('lists', $lists);

        $this->assign("page", $page);
        
        $this->assign("status", $this->getStatus());
        
        return $this->fetch();          
    }
    
    function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $dynamicids=DB::name("dynamic")->where("shop_id='{$id}'")->where('draft',0)->column('id');
        if($dynamicids){
             $this->error("请先删除属于该店铺帖子再做操作");
        }else{
            //删除所有草稿箱数据
            DB::name('dynamic')->where('shop_id',$id)->where('draft',1)->delete();
        }
        
        //删除评论
        DB::name("shop_comment")->where("shop_id={$id}")->delete();
        //删除商铺认领
        DB::name("attestation")->where("shop_id={$id}")->delete();
        //删除店铺收藏
        DB::name("collect")->where(['collect_id' => $id,'type'=>0])->delete();
        
        $rs = DB::name('shop_apply')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        
        $this->success("删除成功！",url("shopapply/index"));
            
    }
    
    
    function edit(){
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('shop_apply')
            ->where("id={$id}")
            ->find();
            // print_r($data);exit;
        if(!empty($data['topic'])){
            $data['topic'] = explode(',',$data['topic']); 
        }else{
            $data['topic']=[];
        }    
        if(!$data){
            $this->error("信息错误");
        }
        
        $data['thumb']= get_upload_path($data['thumb']);
        $data['certificate']= get_upload_path($data['certificate']);

        $data['userinfo']= getUserInfo($data['uid']);
    
        //获取一级店铺分类
        $oneGoodsClass = Db::name('shop_class')->where(['id'=>$data['classid']])->find();
      
        
        $branch_res = Db::name('branch')->wherein('id',$data['branch'])->select();
        
        // $data['branch'] = $branch_res;
        
        // print_r($data);exit;
        
        $data['shop_class'] = $oneGoodsClass['name'];

     
        $this->assign('data', $data);
        $this->assign("status", $this->getStatus());
        $this->assign("branch", $branch_res);
        
        return $this->fetch();
        
    }
    
    function editPost(){
        if ($this->request->isPost()) {
            $data = $this->request->param();

            $shop_status=$data['status'];

            $reason=$data['reason'];
            
            $str = '';
            $branchid = [];
            
          

            //$r = new (app\common\validate\ShopGoodsValidate::class);
            $strs = '';
            if(isset($data['branchid'])){
                 if($data['branchid']){
                     foreach($data['branchid'] as $key => $value){
                         $branchid[] = $value['option'];
                     }
                 }
                $branchid = implode(',',array_unique($branchid));
                //$strs = rtrim($str, ",");
                $strs = $branchid;
            }
           
            if($shop_status==2){ //审核失败
                if(trim($reason)==""){
                    $this->error("请填写审核失败原因");
                }
            }
            // $data['branch'] = $strs;
            // $data['uptime']=time();
            if($data['uid']){
                if($r = DB::name('shop_apply')->where('uid',$data['uid'])->where('id','<>',$data['id'])->find()){
                 $this->error('该用户已经绑定过了,请勿重复绑定,他的店铺ID是'.$r['id']);
                }
            }
            
            
                    
            $datas['uid'] = $data['uid'];
            $datas['name'] = $data['name'];
            $datas['uid'] = $data['uid'];
            $datas['uid'] = $data['uid'];
            $datas['thumb'] = $data['thumb'];
            $datas['des'] = $data['des'];
            $datas['cardno'] = $data['cardno'];
            $datas['link'] = $data['link'];
            $datas['service_phone'] = $data['service_phone'];
            $datas['username'] = $data['username'];
            $datas['status'] = $data['status'];
            $datas['claim'] = $data['claim'];
            $datas['reason'] = $data['reason'];
            $datas['b_cert'] = $data['b_cert'];
            $datas['region'] = $data['region'];
            $datas['brand_address'] = $data['brand_address'];
            $datas['merch_type'] = $data['merch_type'];
            $datas['first_user_name'] = $data['first_user_name'];
            $datas['first_user_mobile'] = $data['first_user_mobile'];
            $datas['first_user_email'] = $data['first_user_email'];
            $datas['second_user_name'] = $data['second_user_name'];
            $datas['second_user_mobile'] = $data['second_user_mobile'];
            $datas['second_user_email'] = $data['second_user_email'];
            $datas['company_name'] = $data['company_name'];
            $datas['company_logo'] = $data['company_logo'];
            $datas['company_img'] = $data['company_img'];
            $datas['company_address'] = $data['company_address'];
            $datas['company_link'] = $data['company_link'];
            $datas['company_description'] = $data['company_description'];
            $datas['branch'] = $strs;
            $datas['uptime']=time();
            $datas['shop_account'] = $data['shop_account'];
            $datas['shop_password'] = $data['shop_password'];
            if(!$datas['uid']){
                 $datas['coin'] = $data['coin'];
            }else{
                 $datas['coin'] = 0;
            }
           
            
            if($datas['shop_account']){
                $rs = DB::name('shop_apply')->where('shop_account',$datas['shop_account'])->where('id','<>',$data['id'])->count();
                if($rs){
                    $this->error("已经存在该商家账号啦！");
                }
                
            }
            
            
           
            $rs = DB::name('shop_apply')->where("id={$data['id']}")->update($datas);

            if($rs===false){
                $this->error("修改失败！");
            }
            if($datas['uid'] && $data['coin'] > 0){ //存在后,应该把该账户钱全部转入进用户腰包
                $user = Db::name('user')->where('id',$datas['uid'])->setInc('coin',$data['coin']); //存入进去
            }

            $this->success("修改成功！");
        }
    }
    
    public function add()
    {
        

        $this->assign("status", $this->getStatus());

        return $this->fetch();
    }
    
    public function addPost(){
        $data = $this->request->param();
        
        // var_dump($data);exit;
        $options = array();
        $str = '';
        $branch = [];
        if($data['branchid']){
            foreach($data['branchid'] as $key => $value){
                $branch[] = $value['option'];
            }
        }
        $branch_id = implode(',',array_unique($branch));
        //$strs = rtrim($str, ",");
        $strs = $branch_id;
        // $branch_res = Db::name('branch')->wherein('id',$strs)->select();
        // print_r($data);exit;
        $model = new ShopApplyModel();
        $data['topic'] = [1,2]; //该地方因为验证类去验证了,所以加死,看到数据库好像也没有用 传入死的就行了
        $result = $this->validate($data, 'ShopApply');

        if ($result !== true) {
            $this->error($result);
        }
        $data['uptime']=time();
        $data['addtime']=time();
        if($data['topic']){
            $datas['topic'] = implode(',',$data['topic']);
        }
        // foreach($branch_res as $val){
            
        //     $data['branch'][$key] = serialize($val);
            
        // }
        // print_r($data);
        // exit;
        
        // if($data['latlng']){
        //     $data['latlng'] =explode(',',$data['latlng']); 
        //     $data['lat']=$data['latlng'][0];
        //     $data['lng']=$data['latlng'][1];
        //     unset($data['latlng']);
        // }
        // print_r($data);exit;
        if($data['uid']){
            if($r = DB::name('shop_apply')->where('uid',$data['uid'])->find()){
              $this->error('该用户已经绑定过了,请勿重复绑定,他的店铺ID是'.$r['id']);
            }
        }
        
        
        
        $datas['uid'] = $data['uid'];
        $datas['provider_id'] = $data['provider_id'];
        $datas['thumb'] = $data['thumb'];
        $datas['des'] = $data['name'];
        $datas['des'] = $data['des'];
        $datas['link'] = $data['link'];
        $datas['service_phone'] = $data['service_phone'];
        $datas['username'] = $data['username'];
        $datas['phone'] = $data['phone'];
        $datas['status'] = $data['status'];
        $datas['claim'] = $data['claim'];
        $datas['b_cert'] = $data['b_cert'];
        $datas['region'] = $data['region'];
        $datas['brand_address'] = $data['brand_address'];
        $datas['merch_type'] = $data['merch_type'];
        $datas['first_user_name'] = $data['first_user_name'];
        $datas['first_user_mobile'] = $data['first_user_mobile'];
        $datas['first_user_email'] = $data['first_user_email'];
        $datas['second_user_name'] = $data['second_user_name'];
        $datas['second_user_mobile'] = $data['second_user_mobile'];
        $datas['second_user_email'] = $data['second_user_email'];
        $datas['company_name'] = $data['company_name'];
        $datas['company_logo'] = $data['company_logo'];
        $datas['company_img'] = $data['company_img'];
        $datas['company_address'] = $data['company_address'];
        $datas['company_link'] = $data['company_link'];
        $datas['company_description'] = $data['company_description'];
        $datas['branch'] = $strs;
        $datas['classid'] = $data['classid'];
        $datas['uptime']=time();
        $datas['addtime']=time();
        $datas['shop_account'] = $data['shop_account'];
        $datas['shop_password'] = $data['shop_password'];
        $datas['coin'] = $data['coin'];
        if(!empty($datas['uid'])){
            $datas['claim']=1;
        }
        
        if($datas['shop_account']){
                $rs = DB::name('shop_apply')->where('shop_account',$datas['shop_account'])->count();
                if($rs){
                    $this->error("已经存在该商家账号啦！");
                }
        }
        
       

        if($model->insert($datas)){
            $this->success("添加成功！",url("Shopapply/index"));
        }else{
            $this->error("添加失败！");
        }
    }
}
