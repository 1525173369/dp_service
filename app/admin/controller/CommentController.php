<?php

/* 动态评论 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class CommentController extends AdminBaseController
{

    
    public function index()
    {
        
        $data = $this->request->param();
        $map=[];
        
        $status=isset($data['status']) ? $data['status']: '';
        $dynamicid=isset($data['dynamicid'])? $data['dynamicid'] : '0' ;
        if($dynamicid!=0){
            $map[]=['note_id','=',$dynamicid];
        }

        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){

            $map[]=['A.user_id','=',$uid];
            
        }
        
        
        $list = Db::name('note_comment')->alias('A')
            ->where($map)
            ->field("A.*,B.user_nickname")
            ->leftJoin('user B','A.user_id = B.id')
            ->order("comment_id desc")
            ->paginate(20);
        

        
        $list->appends($data);
        $list->each(function($v,$k){
	
			$v['content']=urldecode($v['content']);

            return $v;           
        });
        $page = $list->render();
        $this->assign("page", $page);
        $this->assign("dynamicid", $dynamicid);
            
        $this->assign('list', $list);

        return $this->fetch();
    }

    public function del()
    {
        $data = $this->request->param();
        
        if (isset($data['id'])) {
            $id = $data['id']; //获取删除id
           // dump($id);die;
            $info=DB::name('note_comment')->where("comment_id={$id}")->find();
            $rs = DB::name('note_comment')->where("comment_id={$id}")->delete();
            if(!$rs){
                $this->error("删除失败！");
            }
            
            DB::name('dynamic')
                ->where("id={$info['note_id']} and comments>=1")
                ->setDec('comments','1');

            
        } elseif (isset($data['ids'])) {
            $ids = $data['ids'];

            $infos=DB::name('note_comment')->field('note_id')->where('comment_id', 'in', $ids)->select()->toArray();

            $rs = DB::name('note_comment')->where('comment_id', 'in', $ids)->delete();
            if(!$rs){
                $this->error("删除失败！");
            }
            foreach($infos as $k=>$v){
                DB::name('dynamic')
                    ->where("id={$v['note_id']} and comments>=1")
                    ->setDec('comments','1');
            }


        }
        
        $this->success("删除成功！");	
        
    }


}
