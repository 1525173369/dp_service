<?php
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class QuestController extends AdminBaseController
{
    /**
     * 问卷调查管理
     * @adminMenu(
     *     'name'   => '问卷调查',
     *     'parent' => 'admin/Setting/default',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 50,
     *     'icon'   => '',
     *     'remark' => '问卷调查管理',
     *     'param'  => ''
     * )
     * @return mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function index()
    {
        
        $info = Db::name("user_quest")->order("list_order DESC")->paginate(20);
        
        $page = $info->render();
        
        $this->assign('info', $info);

    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }

    /**
     * 添加问卷调查
     * @adminMenu(
     *     'name'   => '添加问卷调查',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加问卷调查',
     *     'param'  => ''
     * )
     */
    public function add()
    {
        return $this->fetch();
    }

    /**
     * 添加问卷调查提交保存
     * @adminMenu(
     *     'name'   => '添加问卷调查提交保存',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '添加问卷调查提交保存',
     *     'param'  => ''
     * )
     */
    public function addPost()
    {
        $data = $this->request->param();
        $info = [];
        $info['title'] = $data['title'];
        $info['add_time']=time();
        if(!$info['title']){
            $this->error("标题不能为空！");
        }
        $id = DB::name('user_quest')->insertGetId($info);
        if(!$id){
            $this->error("添加失败！");
        }
        if(isset($data['TestPaperList'])&&!empty($data['TestPaperList'])){
            foreach ($data['TestPaperList'] as $val){
                $question['content'] = serialize($val);
                $question['type'] = 0;
                $question['paper_id'] = $id;
                DB::name('question')->insert($question);
            }
            
        }
        if(isset($data['multiple'])&&!empty($data['multiple'])){
           foreach ($data['multiple'] as $val){
                $question['content'] = serialize($val);
                $question['type'] = 1;
                $question['paper_id'] = $id;
                DB::name('question')->insert($question);
            }
            
        }
        if(isset($data['questions'])&&!empty($data['questions'])){
          foreach ($data['questions'] as $val){
                $question['content'] = serialize($val);
                $question['type'] = 3;
                $question['paper_id'] = $id;
                DB::name('question')->insert($question);
            } 
            
        }
        $this->success("添加成功！", url("Quest/index"));
    }

    /**
     * 编辑问卷调查
     * @adminMenu(
     *     'name'   => '编辑问卷调查',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '编辑问卷调查',
     *     'param'  => ''
     * )
     * @return mixed
     * @throws \think\Exception\DbException
     */
    public function edit()
    {
        $id = $this->request->param('id', 0, 'intval');
        $info = Db::name('user_quest')->where("id={$id}")->find();
        $questionD = Db::name('question')->where("paper_id={$id}")->where('type=0')->order('id asc')->select();
        $questionM = Db::name('question')->where("paper_id={$id}")->where('type=1')->order('id asc')->select();
        $questionA = Db::name('question')->where("paper_id={$id}")->where('type=3')->order('id asc')->select();
        //print_r($questionD);
        $questionD->each(function($v,$k){
            
           $v['content']= unserialize($v['content']);
           return $v; 
         });
        $questionM->each(function($v,$k){
            
           $v['content']= unserialize($v['content']);
           return $v; 
         });
        $questionA->each(function($v,$k){
            
           $v['content']= unserialize($v['content']);
           return $v; 
         });
      //print_r($questionD);exit;
        $this->assign("info", $info);
    	$this->assign("questionD", $questionD);
        $this->assign("questionM", $questionM);
        $this->assign("questionA", $questionA);
        return $this->fetch();
    }
    

    /**
     * 编辑问卷调查提交保存
     * @adminMenu(
     *     'name'   => '编辑问卷调查提交保存',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '编辑问卷调查提交保存',
     *     'param'  => ''
     * )
     */
    public function editPost()
    {
        $data = $this->request->param();
       // print_r($data);exit;
        $id = $data['id'];
        Db::name('user_quest')->where("id={$id}")->update(['title'=>$data['title']]);
        Db::name('question')->where("paper_id={$id}")->delete();
        if(isset($data['TestPaperList'])&&!empty($data['TestPaperList'])){
            foreach ($data['TestPaperList'] as $val){
                $question['content'] = serialize($val);
                $question['type'] = 0;
                $question['paper_id'] = $id;
                DB::name('question')->insert($question);
            }
            
        }
        if(isset($data['multiple'])&&!empty($data['multiple'])){
           foreach ($data['multiple'] as $val){
                $question['content'] = serialize($val);
                $question['type'] = 1;
                $question['paper_id'] = $id;
                DB::name('question')->insert($question);
            }
            
        }
        if(isset($data['questions'])&&!empty($data['questions'])){
          foreach ($data['questions'] as $val){
                $question['content'] = serialize($val);
                $question['type'] = 3;
                $question['paper_id'] = $id;
                DB::name('question')->insert($question);
            } 
            
        }

        $this->success("保存成功！", url("Quest/index"));
    }


    public function census(){
         $id = $this->request->param('id', 0, 'intval');
         $questionD = Db::name('question')->where("paper_id={$id}")->where('type=0')->order('id asc')->select();
         $questionD->each(function($v,$k){
            
           $v['content']= unserialize($v['content']);
           return $v; 
         });
        $questionM = Db::name('question')->where("paper_id={$id}")->where('type=1')->order('id asc')->select();
        $questionM->each(function($v,$k){
            
           $v['content']= unserialize($v['content']);
           return $v; 
         });
        $questionA =  Db::name('question')->where("paper_id={$id}")->where('type=3')->order('id asc')->select();
        $questionA->each(function($v,$k){
            
           $v['content']= unserialize($v['content']);
           return $v; 
         });
     //   print_r($questionA);exit;
     	$this->assign("questionD", $questionD);
     	$this->assign("questionM", $questionM);
 	 	$this->assign("questionA", $questionA);
 		$this->assign("paper_id", $id);
        return $this->fetch();
    }
    
    public function ans(){
        $data = $this->request->param();
        $name = 'a'.$data['qid'];
        $map[]=['paper_id','=',$data['paper_id']];
        $list = Db::name('user_answer')->where($map)->field($name)->paginate(20);
        $page = $list->render();
        $this->assign("name", $name);
        $this->assign("page", $page);
        $this->assign("list", $list);
        return $this->fetch();
        
    }
    

    public function vital(){
        $data = $this->request->param();
        $new=[];
        if($data['type']==0){
            $name = 'q'.$data['id'];
            $map[]=['paper_id','=',$data['paper_id']];
            $map[]=[$name,'=','A'];
         // $questionD = Db::name('user_answer')->field('count(*) as count,'.$name.'')->where("paper_id={$data['paper_id']}")->group($name)->select();
            $new[] = Db::name('user_answer')->where($map)->count();
            $map1[]=['paper_id','=',$data['paper_id']];
            $map1[]=[$name,'=','B'];
            $new[] = Db::name('user_answer')->where($map1)->count();
            $map2[]=['paper_id','=',$data['paper_id']];
            $map2[]=[$name,'=','C'];
            $new[] = Db::name('user_answer')->where($map2)->count();
            $map3[]=['paper_id','=',$data['paper_id']];
            $map3[]=[$name,'=','D'];
            $new[] = Db::name('user_answer')->where($map3)->count();
           
        }elseif ($data['type']==1) {
            $name = 'd'.$data['id'];
            $map[]=['paper_id','=',$data['paper_id']];
            $r = Db::name('user_answer')->where($map)->field("$name as qname")->select();
            $newA=$newB=$newC=$newD=0;
            foreach ($r as $val){
                $res = explode(",",$val['qname']);
                if(in_array("A", $res)){
                    $newA = $newA+1;
                }
                if(in_array("B", $res)){
                    $newB = $newB+1;
                }
                if(in_array("C", $res)){
                    $newC = $newC+1;
                }
                if(in_array("D", $res)){
                    $newD = $newD+1;
                }
            }
            $new[]=$newA;$new[]=$newB;$new[]=$newC;$new[]=$newD;
        }
        return $new;
    }

    /**
     * 删除问卷调查
     * @adminMenu(
     *     'name'   => '删除问卷调查',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '删除问卷调查',
     *     'param'  => ''
     * )
     */
    public function delete()
    {
        $id = $this->request->param('id', 0, 'intval');
        
        $res = Db::name('user_quest')->delete($id);
        
        if($res) {
            $this->success("删除成功！", url("quest/index"));
        }
        $this->success("删除失败！", url("quest/index"));
    }

    /**
     * 问卷调查排序
     * @adminMenu(
     *     'name'   => '问卷调查排序',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '问卷调查排序',
     *     'param'  => ''
     * )
     */
    public function listOrder()
    {
        parent::listOrders('user_quest');
        $this->success("排序更新成功！");
    }

    /**
     * 问卷调查显示隐藏
     * @adminMenu(
     *     'name'   => '问卷调查显示隐藏',
     *     'parent' => 'index',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '问卷调查显示隐藏',
     *     'param'  => ''
     * )
     */
    public function toggle()
    {
        $data = $this->request->param();
        
        if (isset($data['ids']) && !empty($data["display"])) {
            $ids = $this->request->param('ids/a');
            Db::name('user_quest')->where('id', 'in', $ids)->update(['status' => 1]);
            $this->success("更新成功！");
        }

        if (isset($data['ids']) && !empty($data["hide"])) {
            $ids = $this->request->param('ids/a');
            Db::name('user_quest')->where('id', 'in', $ids)->update(['status' => 0]);
            $this->success("更新成功！");
        }


    }

}