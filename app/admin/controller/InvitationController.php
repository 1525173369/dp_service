<?php
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
class InvitationController extends AdminBaseController
{

    /**
     * 话题分类
     */
    public function index(){
			
    	$lists = Db::name("shop_topic")
            //->where()
            ->order("topic_num desc")
            ->paginate(20);
            
        
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
    	
    	return $this->fetch();
    }
    public function add(){        
        return $this->fetch();
    }
    
    public function addPost(){
        if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$name=$data['topic_name'];
            $data['addtime']=time();
			if($name==""){
				$this->error("请填写名称");
			}
			$id = DB::name('shop_topic')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }
            
            $action="添加话题：{$id}";
            setAdminLog($action);
       
            $this->success("添加成功！");
            
		}
    }
    
   public function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('shop_topic')
            ->where("topic_id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $this->assign('data', $data);
        return $this->fetch(); 			
    }
    

    
    public function editPost(){
        if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            
			$name=$data['topic_name'];

			if($name==""){
				$this->error("请填写名称");
			}
			$id = DB::name('shop_topic')->update($data);
            if($id===false){
                $this->error("修改失败！");
            }
            
            $action="修改话题：{$data['topic_id']}";
            setAdminLog($action);
        
            $this->success("修改成功！");
		}	
    }
    
     public  function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('shop_topic')->where("topic_id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        
        $action="删除话题：{$id}";
        setAdminLog($action);

        $this->success("删除成功！");				
    }	


}