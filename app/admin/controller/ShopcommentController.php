<?php

namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class ShopcommentController extends AdminBaseController
{

    
    public function index()
    {
        
        $data = $this->request->param();
        $map=[];
        
        $status=isset($data['status']) ? $data['status']: '';
        $dynamicid=isset($data['shopid'])? $data['shopid'] : '0' ;
        if($dynamicid!=0){
            $map[]=['shop_id','=',$dynamicid];
        }

        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){

            $map[]=['A.user_id','=',$uid];
            
        }
        
        
        $list = Db::name('shop_comment')->alias('A')
            ->where($map)
            ->field("A.*,B.user_nickname")
            ->leftJoin('user B','A.user_id = B.id')
            ->order("comment_id desc")
            ->paginate(20);
  
        
        $list->appends($data);
        $list->each(function($v,$k){
	
			$v['content']=urldecode($v['content']);

            return $v;           
        });
        $page = $list->render();
        $this->assign("page", $page);
        $this->assign("dynamicid", $dynamicid);
            
        $this->assign('list', $list);

        return $this->fetch();
    }

    public function del()
    {
        $data = $this->request->param();

        if (isset($data['id'])) {
            $id = $data['id']; //获取删除id
            
            $info=DB::name('shop_comment')->where("comment_id={$id}")->find();
            $rs = DB::name('shop_comment')->where("comment_id={$id}")->delete();
            if(!$rs){
                $this->error("删除失败！");
            }
            
            DB::name('shop_apply')
                ->where("id={$info['shop_id']} and consumption>=1")
                ->setDec('consumption','1');

            
        } elseif (isset($data['ids'])) {
            $ids = $data['ids'];

            $infos=DB::name('shop_comment')->field('shop_id')->where('comment_id', 'in', $ids)->select()->toArray();

            $rs = DB::name('shop_comment')->where('comment_id', 'in', $ids)->delete();
            if(!$rs){
                $this->error("删除失败！");
            }
            foreach($infos as $k=>$v){
                DB::name('shop_apply')
                    ->where("id={$v['shop_id']} and consumption>=1")
                    ->setDec('consumption','1');
            }


        }
        
        $this->success("删除成功！");	
        
    }


}