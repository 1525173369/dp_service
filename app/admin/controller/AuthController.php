<?php

/**
 * 认证
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class AuthController extends AdminbaseController {
    
    protected function getStatus($k=''){
        $status=array(
            '0'=>'处理中',
            '1'=>'审核成功',
            '2'=>'审核失败',
        );
        if($k===''){
            return $status;
        }
        
        return isset($status[$k]) ? $status[$k]: '';
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $status=isset($data['status']) ? $data['status']: '';
        if($status!=''){
            $map[]=['status','=',$status];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
           

                $map[]=['uid','=',$uid];
            
        }
        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['real_name|mobile','like','%'.$keyword.'%'];
        }
			

    	$lists = Db::name("user_auth")
                ->where($map)
                ->order("addtime DESC")
                ->paginate(20);
        
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['uid']);
			$v['mobile']=m_s($v['mobile']);
			$v['cer_no']=m_s($v['cer_no']);
            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);
    

    	$this->assign("page", $page);
        
        $this->assign('status', $this->getStatus());
    	
    	return $this->fetch();
    }
    
	function del(){
        
        $uid = $this->request->param('uid', 0, 'intval');
        
        $rs = DB::name('user_auth')->where("uid={$uid}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
         DB::name('user')->where("id={$uid}")->update(['user_status'=>2]);
        
        $action="删除会员认证信息：{$uid}";
        setAdminLog($action);
        
        $this->success("删除成功！");
            
	}
    function addMoney($uid,$data){
        $where = "type=1 and uid =".$uid;

        if($data['title'] == '完善资料'){
            $where .= " and title = '{$data['title']}'";
        }
        $res= DB::name('user_log')->where($where)->find();
        if($res && $data['title'] == '完善资料'){
            return;
        }

        DB::name('user_log')->insert([
            'title'=>$data['title'],
            'type'=>1,
            'createtime'=>time(),
            'money'=>$data['money'],
            'uid'=>$uid,
            'readid'=>$data['readid']?$data['readid']:0,
            'sid'=>$data['dyId']?$data['dyId']:0
        ]);

//        DB::name('user')->where('id',$uid)
//            ->update( array('coin' => new NotORM_Literal("coin + {$data['money']}") ) );

        return;
    }
    function edit(){
        
        $uid   = $this->request->param('uid', 0, 'intval');
        
        $data=Db::name('user_auth')
            ->where("uid={$uid}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $data['userinfo']=getUserInfo($data['uid']);
        $data['mobile']=m_s($data['mobile']);
        $data['cer_no']=m_s($data['cer_no']);
		$data['front_view']=get_upload_path($data['front_view']);
		$data['back_view']=get_upload_path($data['back_view']);
        $status=$this->getStatus();
        if($data['status']!=0){ //已经处理过的不显示处理中
            unset($status[0]);
        }
        // ?x-image-process=image/watermark,text_IEZvciBBc2twZXJ0IFZlcmlmaWNhdGlvbiBPbmx5,size_30,color_FF0000,type_ZmFuZ3poZW5nc2h1c29uZw==,g_center,rotate_30,fill_1
        if(strstr($data['front_view'],'static-ap.askpert.com') && !strstr($data['front_view'],'?x-image-process=image/watermark')){
            $data['front_view'] = $data['front_view'].'?x-image-process=image/watermark,text_IEZvciBBc2twZXJ0IFZlcmlmaWNhdGlvbiBPbmx5,size_30,color_FF0000,type_ZmFuZ3poZW5nc2h1c29uZw==,g_center,rotate_30,fill_1';
        }  
        
        if(strstr($data['back_view'],'static-ap.askpert.com') && !strstr($data['back_view'],'?x-image-process=image/watermark')){
            $data['back_view'] = $data['back_view'].'?x-image-process=image/watermark,text_IEZvciBBc2twZXJ0IFZlcmlmaWNhdGlvbiBPbmx5,size_30,color_FF0000,type_ZmFuZ3poZW5nc2h1c29uZw==,g_center,rotate_30,fill_1';
        } 
        
        $this->assign('status', $status);
            
        $this->assign('data', $data);
        return $this->fetch();
	}
	function editPost(){
		if ($this->request->isPost()) {
            
            $data = $this->request->param();
            //$reason = $this->request->request('reason');
            $data['reason'] = htmlspecialchars_decode($data['reason']);
			$status=$data['status'];
			$uid=$data['uid'];

			if($status=='0'){
				$this->success("修改成功！");
			}

            
            $data['uptime']=time();
            
			$rs = DB::name('user_auth')->update($data);
            if($rs===false){
                $this->error("修改失败！");
            }
            
            if($status=='2'){
                $action="修改会员认证信息：{$uid} - 拒绝";
                DB::name('user')->where("id={$uid}")->update(['user_status'=>2]);
                
            }else if($status=='1'){
                $config= DB::name('option')
                    ->field('option_value')
                    ->where("option_name='site_info'")
                    ->find();
                $config=json_decode($config['option_value'],true);
                DB::name('user')->where('id='.$uid)->setInc('coin',$config['coin']);
                $this->addMoney($uid, [
                    'title' => '实名认证审核成功',
                    'money' => $config['coin'],
                    'readid' => 0,
                    'dyId'=>''
                ]);
                $action="修改会员认证信息：{$uid} - 同意";
                DB::name('user')->where("id={$uid}")->update(['user_status'=>1]);

                $this->addReferralCoin($uid, $config['coin_ref']);
            }
            setAdminLog($action);
            
            $this->success("修改成功！");
		}
	}
	
    function addReferralCoin($uid, $reward_coin) {
        if(isset($uid) && intval($uid) > 0 && isset($reward_coin) && floatval($reward_coin) > 0) {
            $uinfo = DB::name('user')->field('pid')->where('id='.$uid)->find(); 
            if($uinfo) {
                // 检查之前有没有拿过
                $pid = $uinfo['pid'];
                $title = '实名认证推荐人奖励 ('.$uid.')';

                if(isset($pid) && $pid > 0) {
                    $chk = DB::name('user_log')->where(['uid' => $pid, 'type' => 1, 'ref_uid' => $uid, 'title' => $title])->find(); 
                    if($chk) {
                        return;
                    }

                    // 写入数据
                    $rs = DB::name('user_log')->insertGetId([
                        'title'=>$title,
                        'type'=>1,
                        'createtime'=>time(),
                        'money'=>$reward_coin,
                        'uid'=>$pid,
                        'readid'=>0,
                        'sid'=>0,
                        'ref_uid'=>$uid
                    ]);

                    if($rs) {
                        //$add_ret = DB::name('user')->where('id='.$pid)->setInc('coin',$reward_coin);
                        $config = getConfigPub();
                        $forward_time = $config['forward_time'];
                        if($forward_time < 0){
                            $forward_time = 0; //防止负数
                        }
                        $data =  [
                            'uid' => $uid,
                            'coin' => $reward_coin,
                            'createtime' => time(),
                            'updatetime' => time(),
                            'status' => 0,
                            'forward_time' => time() + 86400*$forward_time,
                            'user_log_id'  => $rs
                        ];
                        Db::name('user_forward_coin')->insert($data);
                        $action= $pid.' - 实名认证推荐人奖励 ('.$uid.') ['.$reward_coin.']';
                        setAdminLog($action);
                    }
                }
            }
        }
    }
}
