<?php
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\MessageModel;
class MessageController extends AdminbaseController {

    public function index(){

        $data = $this->request->param();
        $users = [];
        if($data['user_id_array']){

            $users = Db::name('user')->field('id,user_nickname')->where('id','IN',$data['user_id_array'])->select();
        }

        $this->assign('users',$users);
        return $this->fetch();
    }

    public function paper(){

        $data = $this->request->param();
        $users = [];
        if($data['user_id_array']){

            $users = Db::name('user')->field('id,user_nickname')->where('id','IN',$data['user_id_array'])->select();
        }
        $question = Db::name('user_quest')->field('id,title')->where('status','=',1)->select();
        $this->assign('question',$question);
        $this->assign('users',$users);
        return $this->fetch();
    }
    public function question(){
        $data = $this->request->param();
        $type = $data['type'];//type:1全体，0
        $users = $data['user'];

        $question_id = isset($data['question'])?$data['question']:0;
        if(!$question_id){
            $this->error("请选择问卷！");
        }
        $r  = Db::name('user_quest')->field('id,title')->where('status','=',1)->where('id','=',$question_id)->find();
        if($type == 1){
            $map=array();
            $andPushids=DB::name("user_pushid")
                ->field("pushid")
                ->where($map)
                ->where(['terminal' => 'android'])
                ->select()
                ->toArray();

            $andPushids=array_filter(array_column($andPushids,'pushid'));

            $iosPushids=DB::name("user_pushid")
                ->field("pushid")
                ->where($map)
                ->where(['terminal' => 'ios'])
                ->select()
                ->toArray();
            $iosPushids=array_filter(array_column($iosPushids,'pushid'));
            $title='您有新的问卷调查';
            $content="系统给你发送问卷调查";
            if(!empty($iosPushids)) {
                $res = adminPush('ios', 'token_list', 'notify', $title, $content, $iosPushids, '', 5,['id'=>$question_id,'title'=>$r['title']]);
            }
            if(!empty($andPushids)) {
                $res1 = adminPush('android', 'token_list', 'notify', $title, $content, $andPushids,'', 5,['id'=>$question_id,'title'=>$r['title']]);
            }

        }else{
            if (!empty($users)) {
                $map[]  =['uid','in',$users];

                $andPushids=DB::name("user_pushid")
                    ->field("pushid")
                    ->where($map)
                    ->where(['terminal' => 'android'])
                    ->select()
                    ->toArray();
                $andPushids=array_filter(array_column($andPushids,'pushid'));

                $iosPushids=DB::name("user_pushid")
                    ->field("pushid")
                    ->where($map)
                    ->where(['terminal' => 'ios'])
                    ->select()
                    ->toArray();
                $iosPushids=array_filter(array_column($iosPushids,'pushid'));
                $title='您有新的问卷调查';
                $content="系统给你发送问卷调查";
                if(!empty($iosPushids)) {
                    $res = adminPush('ios', 'token_list', 'notify', $title, $content, $iosPushids, '', 5,['id'=>$question_id,'title'=>$r['title']]);
                }
                if(!empty($andPushids)) {
                    $res1 = adminPush('android', 'token_list', 'notify', $title, $content, $andPushids,'', 5,['id'=>$question_id,'title'=>$r['title']]);
                }


            }


        }
        $this->success("发送成功！");
    }

    public function send(){
        $data = $this->request->param();
        $text = $data['message'];
        $title = $data['title'];
        $type = $data['type'];
        $users = $data['user'];
// 		$title=str_replace("\r","", $title);
//         $title=str_replace("\n","", $title);

//         $text=str_replace("\r","", $text);

        $text=htmlspecialchars_decode($text);
        $title = htmlspecialchars_decode($title);

        if(!$text){
            $this->error("消息不能为空！");
        }
        if(!$title){
            $this->error("标题不能为空！");
        }
        $message = array(
            'title'=>$title,
            'message' => $text,
            'category' => 0,
            'send_time' => time()
        );
        if ($type == 1) {
            //全体用户系统消息
            $message['type'] = 1;
            DB::name('message')->insert($message);
            $map=array();
            $andPushids=DB::name("user_pushid")
                ->field("pushid")
                ->where($map)
                ->where(['terminal' => 'android'])
                ->select()
                ->toArray();

            $andPushids=array_filter(array_column($andPushids,'pushid'));

            $iosPushids=DB::name("user_pushid")
                ->field("pushid")
                ->where($map)
                ->where(['terminal' => 'ios'])
                ->select()
                ->toArray();
            $iosPushids=array_filter(array_column($iosPushids,'pushid'));
            //$content =$text;
            /*  $pattern = "/\<.*?\>/";
              $content = preg_replace($pattern,"",$text);
              $content = strip_tags(trim($content));*/

            $content = htmlspecialchars_decode(strip_tags(trim($text)));
            $title = htmlspecialchars_decode(strip_tags(trim($title)));
            $content  = str_replace('&nbsp;',' ',$content);

            if(!empty($iosPushids)) {
                $res = adminPush('ios', 'token_list', 'notify', $title, $content, $iosPushids, '', 4,'');
            }
            if(!empty($andPushids)) {
                $res1 = adminPush('android', 'token_list', 'notify', $title, $content, $andPushids,'', 4,'');
            }
        } else {
            //个体消息
            $message['type'] = 0;
            if (!empty($users)) {
                #$model = new MessageModel();
                $model = Db::name('message');
                $create_message_id = $model->insertGetId($message);
                foreach ($users as $key) {
                    DB::name('user_message')->insert(array('user_id' => $key, 'message_id' => $create_message_id, 'status' => 0, 'category' => 0));
                }
            }
            if (!empty($users)) {
                $map[]  =['uid','in',$users];

                $andPushids=DB::name("user_pushid")
                    ->field("pushid")
                    ->where($map)
                    ->where(['terminal' => 'android'])
                    ->select()
                    ->toArray();
                $andPushids=array_filter(array_column($andPushids,'pushid'));

                $iosPushids=DB::name("user_pushid")
                    ->field("pushid")
                    ->where($map)
                    ->where(['terminal' => 'ios'])
                    ->select()
                    ->toArray();
                $iosPushids=array_filter(array_column($iosPushids,'pushid'));
                //$content=$text;




                $content = htmlspecialchars_decode(strip_tags(trim($text)));
                $title = htmlspecialchars_decode(strip_tags(trim($title)));
                $content  = str_replace('&nbsp;',' ',$content);
                if(!empty($iosPushids)) {
                    $res = adminPush('ios', 'token_list', 'notify', $title, $content, $iosPushids, '', 4,'');
                }
                if(!empty($andPushids)) {
                    $res1 = adminPush('android', 'token_list', 'notify', $title, $content, $andPushids,'', 4,'');
                }


            }


        }

        // $touid = implode(',', $users);

        //  $id=addSysytemInfo($touid,$content,0,$title);
        // if(!$id){
        //     $this->error("推送记录写入失败！");
        // }

        // $action="推送信息ID：{$id}";
        // setAdminLog($action);
        $this->success("发送成功！");
    }

    public function messagelog(){
        // $data = $this->request->param();
        // $map[] =['A.status','=',1];
        // $name=isset($data['name']) ? $data['name']: '';
        // if($name!=''){
        //     $map[]=['A.name','like','%'.$name.'%'];
        // }
        $list = Db::name('message')
            ->order('message_id desc')
            ->paginate(20);

        $list->each(function($v,$k){
            if($v['type']==0){
                $userIds = Db::name('user_message')->where("message_id={$v['message_id']}")
                    ->field("user_id")->select();
                $new=[];
                foreach ($userIds as $val){
                    $new[]=$val['user_id'];
                }
                $v['user_ids']=implode(',',$new);
            }
            return $v;
        });
        $page = $list->render();
        $this->assign("page", $page);
        $this->assign('list', $list);

        return $this->fetch();


    }

    function del(){

        $id = $this->request->param('id', 0, 'intval');
        $message = DB::name('message')->where("message_id={$id}")->find();
        if($message['type']==0){
            DB::name('user_message')->where("message_id={$id}")->delete();
            $rs = DB::name('message')->where("message_id={$id}")->delete();
        }else{
            $rs = DB::name('message')->where("message_id={$id}")->delete();
        }

        if(!$rs){
            $this->error("删除失败！");
        }

        $this->success("删除成功！",url("message/messagelog"));

    }

}
