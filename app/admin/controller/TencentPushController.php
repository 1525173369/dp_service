<?php

/* 腾讯推送 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class TencentPushController extends AdminBaseController
{
    
    public function test() {
        $account_list = ['02e7f2d852ba25bbae75285cdaa26a9551cc'];
        // adminPush('ios', 'account', 'notify', '标题', '内容', '', $account_list);
        adminPush('android', 'token', 'notify', '标题', '内容', $account_list, '');
        
    }
    
    
}