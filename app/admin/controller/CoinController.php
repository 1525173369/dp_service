<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class CoinController extends AdminBaseController
{
    protected function getTransType($k=''){
        $trans_type=array(
            '1'=>'收入',
            '0'=>'支出',
        );
        if($k===''){
            return $trans_type;
        }
        
        return isset($trans_type[$k]) ? $trans_type[$k]: '';
    }

    function index(){
        $data = $this->request->param();
        $map=[];
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        
        $type=isset($data['trans_type']) ? $data['trans_type']: '';
        if($type!=''){
            $map[]=['type','=',$type];
        }
        
        $uid=isset($data['uid']) ? $data['uid']: '';
        if($uid!=''){
                $map[]=['uid','=',$uid];
        }

        $sid=isset($data['sid']) ? $data['sid']: '';
        if($sid!=''){
                $map[]=['sid','=',$sid];
        }
        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['title','like','%'.$keyword.'%'];
        }
			

    	$lists = Db::name("user_log")
                ->where($map)
                ->order("createtime DESC")
                ->paginate(20);
        
        $lists->each(function($v,$k){
			$v['userinfo']=getUserInfo($v['uid']);
            if($v['type'] == '0') {
                $v['money'] = $v['money'] * -1;
            }
            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
        $this->assign('trans_type', $this->getTransType());
    	
    	return $this->fetch();
    }

    function manual() {
        $this->assign('trans_type', $this->getTransType());
    	
    	return $this->fetch();
    }

    function addTrans() {
        if ($this->request->isPost()) {
            $data = $this->request->param();

            if(!isset($data['uid']) || intval($data['uid']) < 1) {
                $this->error("Invalid User Id!");
            }else
            {
                // Check User Exists
                $userinfo = DB::name('user')->where('id','=',$data['uid'])->find();
                if(!$userinfo) {
                    $this->error("User Id Not Found!");
                }
            }

            if(!isset($data['money']) || floatval($data['money']) < 0.01) {
                $this->error("Invalid Coin Value!");
            }

            if(!isset($data['title']) || empty($data['title'])) {
                $this->error("Invalid Detail!");
            }

            if(!isset($data['trans_type']) || $this->getTransType($data['trans_type']) == '') {
                $this->error("Invalid Transaction Type!");
            }

            $data['type'] = $data['trans_type'];
            unset($data['trans_type']);

            $data['sid'] = 0;
            $data['readid'] = 0;
            $data['admin_id'] = session('ADMIN_ID');
            $data['createtime']=time();
           
            $trans_type = intval($data['type']);
            $uid = $data['uid'];
            $coin = $data['money'];
            
            $data['title'] = htmlspecialchars_decode($data['title']);
            $ins_id = DB::name('user_log')->insertGetId($data);
            if($ins_id ) {
                switch($trans_type) {
                    case 0:
                        $rs1 = DB::name('user')->where('id='.$uid)->setDec('coin', $coin);
                        break;
                    
                    case 1:
                        $rs1 = DB::name('user')->where('id='.$uid)->setInc('coin', $coin);
                        break;
                }

                if(isset($rs1) && $rs1) {
                    setAdminLog("Manual Add Coin, Transaction #".$ins_id." [".$coin."] to ".$uid.".");
                    $this->success('Transaction Added.');
                }else
                {
                    setAdminLog("Manual Add Coin, Failed Balance, Transaction #".$ins_id." [".$coin."] to ".$uid.".");
                    $this->error('Transaction Added But Balance Not Adjusted');
                }
            }else
            {
                $this->error('Failed to Add Transaction.');
            }
        }
    }
}