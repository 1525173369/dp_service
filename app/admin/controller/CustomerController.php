<?php

/* 动态评论 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
class CustomerController extends AdminBaseController{

    public function index(){
        if($this->request->post()){
            $operating_gb = $this->request->post('operating_gb');
            $operating_en = $this->request->post('operating_en');
            $operating_ms = $this->request->post('operating_ms');
            $email = $this->request->post('email');
            Db::name('about')->where('id',1)->update([
                'operating_gb' => $operating_gb,
                'operating_en' => $operating_en,
                'operating_ms' => $operating_ms,
                'email' => $email
            ]);
            $this->success("编辑成功");
        }
        $info = Db::name('about')->where('id',1)->find();
        $this->assign('info', $info);
        return $this->fetch(':fx');
    }
}

?>