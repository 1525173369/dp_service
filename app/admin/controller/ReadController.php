<?php

/**
 * 商家分类
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class ReadController extends AdminbaseController {
    
    public function index(){
        $list = Db::name('read')
            ->order('id asc')
            ->paginate(20); 
        $page = $list->render();
        $this->assign("page", $page);
            
        $this->assign('lists', $list);
        return $this->fetch(); 
    }
    function add(){        
        return $this->fetch();
    }
    
    function addPost(){
        if ($this->request->isPost()) {
            
            $data = $this->request->param();
            
			$num=$data['num'];

			if($num==""){
				$this->error("请填写数量");
			}
			$percentage=$data['percentage'];
			if($percentage==""){
				$this->error("请填写金币数量");
			}
            
			$id = DB::name('read')->insertGetId($data);
            if(!$id){
                $this->error("添加失败！");
            }
            $this->success("添加成功！");
            
		}
    }
    
    function edit(){
        
        $id   = $this->request->param('id', 0, 'intval');
        
        $data=Db::name('read')
            ->where("id={$id}")
            ->find();
        if(!$data){
            $this->error("信息错误");
        }
        
        $this->assign('data', $data);
        return $this->fetch(); 			
    }
    
    function editPost(){
        if ($this->request->isPost()) {
            
            $data      = $this->request->param();
            
			$num=$data['num'];

			if($num==""){
				$this->error("请填写数量");
			}
			$percentage=$data['percentage'];
			if($percentage==""){
				$this->error("请填写百分比");
			}

			$id = DB::name('read')->update($data);
            if($id===false){
                $this->error("修改失败！");
            }

            $this->success("修改成功！");
		}	
    }
    
    function del(){
        
        $id = $this->request->param('id', 0, 'intval');
        
        $rs = DB::name('read')->where("id={$id}")->delete();
        if(!$rs){
            $this->error("删除失败！");
        }
        $this->success("删除成功！");				
    }
    
}
