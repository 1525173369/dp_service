<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use app\admin\model\RouteModel;
use cmf\controller\AdminBaseController;

use think\Db;

/**
 * Class SettingController
 * @package app\admin\controller
 * @adminMenuRoot(
 *     'name'   =>'设置',
 *     'action' =>'default',
 *     'parent' =>'',
 *     'display'=> true,
 *     'order'  => 0,
 *     'icon'   =>'cogs',
 *     'remark' =>'系统设置入口'
 * )
 */
class SettingController extends AdminBaseController
{

    /**
     * 网站信息
     * @adminMenu(
     *     'name'   => '网站信息',
     *     'parent' => 'default',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 0,
     *     'icon'   => '',
     *     'remark' => '网站信息',
     *     'param'  => ''
     * )
     */
    public function site()
    {
        $content = hook_one('admin_setting_site_view');

        if (!empty($content)) {
            return $content;
        }

        $noNeedDirs     = [".", "..", ".svn", 'fonts'];
        $adminThemesDir = WEB_ROOT . config('template.cmf_admin_theme_path') . config('template.cmf_admin_default_theme') . '/public/assets/themes/';
        $adminStyles    = cmf_scan_dir($adminThemesDir . '*', GLOB_ONLYDIR);
        $adminStyles    = array_diff($adminStyles, $noNeedDirs);
        $cdnSettings    = cmf_get_option('cdn_settings');
        $cmfSettings    = cmf_get_option('cmf_settings');
        $adminSettings  = cmf_get_option('admin_settings');
        $comment_app   =  cmf_get_option('comment_app');

        $adminThemes = [];
        $themes      = cmf_scan_dir(WEB_ROOT . config('template.cmf_admin_theme_path') . '/*', GLOB_ONLYDIR);

        foreach ($themes as $theme) {
            if (strpos($theme, 'admin_') === 0) {
                array_push($adminThemes, $theme);
            }
        }

        if (APP_DEBUG && false) { // TODO 没确定要不要可以设置默认应用
            $apps = cmf_scan_dir(APP_PATH . '*', GLOB_ONLYDIR);
            $apps = array_diff($apps, $noNeedDirs);
            $this->assign('apps', $apps);
        }

        $this->assign('site_info', cmf_get_option('site_info'));
        $this->assign("admin_styles", $adminStyles);
        $this->assign("templates", []);
        $this->assign("admin_themes", $adminThemes);
        $this->assign("cdn_settings", $cdnSettings);
        $this->assign("admin_settings", $adminSettings);
        $this->assign("cmf_settings", $cmfSettings);
        $this->assign("comment_app", $comment_app);
        return $this->fetch();
    }

    /**
     * 网站信息设置提交
     * @adminMenu(
     *     'name'   => '网站信息设置提交',
     *     'parent' => 'site',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '网站信息设置提交',
     *     'param'  => ''
     * )
     */
    public function sitePost()
    {
        if ($this->request->isPost()) {
            $result = $this->validate($this->request->param(), 'SettingSite');
           
            if ($result !== true) {
                $this->error($result);
            }
            $comment = $this->request->param('comment_app/a');
            cmf_set_option('comment_app', $comment);
            $options = $this->request->param('options/a');
            $options['least_trans'] = (int)$options['least_trans'];
            $user_cash_rate = $options['user_cash_rate'];
            $checks = explode(':',$user_cash_rate);
            if(count($checks) != 2){
                $this->error('用户提现金币比例规则不正确');
            }
            if(!is_numeric($checks[0]) || !is_numeric($checks[1])){
                $this->error('用户提现金币比例规则不正确');
            }
            $options['user_cash_rate'] = intval($checks[0]).':'.intval($checks[1]);
            unset($checks);
            $user_cash_rate = $options['merch_cash_rate'];
            $checks = explode(':',$user_cash_rate);
            if(count($checks) != 2){
                $this->error('商家提现金币比例规则不正确');
            }
            if(!is_numeric($checks[0]) || !is_numeric($checks[1])){
                $this->error('商家提现金币比例规则不正确');
            }
             $options['merch_cash_rate'] = intval($checks[0]).':'.intval($checks[1]);
            
            
            
            cmf_set_option('site_info', $options);

            $cmfSettings = $this->request->param('cmf_settings/a');

            $bannedUsernames                 = preg_replace("/[^0-9A-Za-z_\\x{4e00}-\\x{9fa5}-]/u", ",", $cmfSettings['banned_usernames']);
            $cmfSettings['banned_usernames'] = $bannedUsernames;
            cmf_set_option('cmf_settings', $cmfSettings);

            $cdnSettings = $this->request->param('cdn_settings/a');
            cmf_set_option('cdn_settings', $cdnSettings);

            $adminSettings = $this->request->param('admin_settings/a');

            $routeModel = new RouteModel();
            if (!empty($adminSettings['admin_password'])) {
                $routeModel->setRoute($adminSettings['admin_password'] . '$', 'admin/Index/index', [], 2, 5000);
            } else {
                $routeModel->deleteRoute('admin/Index/index', []);
            }

            $routeModel->getRoutes(true);

            if (!empty($adminSettings['admin_theme'])) {
                $result = cmf_set_dynamic_config([
                    'template' => [
                        'cmf_admin_default_theme' => $adminSettings['admin_theme']
                    ]
                ]);

                if ($result === false) {
                    $this->error('配置写入失败!');
                }
            }

            cmf_set_option('admin_settings', $adminSettings);

            $this->success("保存成功！", '');

        }
    }

    /**
     * 密码修改
     * @adminMenu(
     *     'name'   => '密码修改',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '密码修改',
     *     'param'  => ''
     * )
     */
    public function password()
    {
        return $this->fetch();
    }

    /**
     * 密码修改提交
     * @adminMenu(
     *     'name'   => '密码修改提交',
     *     'parent' => 'password',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '密码修改提交',
     *     'param'  => ''
     * )
     */
    public function passwordPost()
    {
        if ($this->request->isPost()) {

            $data = $this->request->param();
            if (empty($data['old_password'])) {
                $this->error("原始密码不能为空！");
            }
            if (empty($data['password'])) {
                $this->error("新密码不能为空！");
            }

            $userId = cmf_get_current_admin_id();

            $admin = Db::name('user')->where("id", $userId)->find();

            $oldPassword = $data['old_password'];
            $password    = $data['password'];
            $rePassword  = $data['re_password'];

            if (cmf_compare_password($oldPassword, $admin['user_pass'])) {
                if ($password == $rePassword) {

                    if (cmf_compare_password($password, $admin['user_pass'])) {
                        $this->error("新密码不能和原始密码相同！");
                    } else {
                        Db::name('user')->where('id', $userId)->update(['user_pass' => cmf_password($password)]);
                        $this->success("密码修改成功！");
                    }
                } else {
                    $this->error("密码输入不一致！");
                }

            } else {
                $this->error("原始密码不正确！");
            }
        }
    }

    /**
     * 上传限制设置界面
     * @adminMenu(
     *     'name'   => '上传设置',
     *     'parent' => 'default',
     *     'display'=> true,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '上传设置',
     *     'param'  => ''
     * )
     */
    public function upload()
    {
        $uploadSetting = cmf_get_upload_setting();
        $this->assign('upload_setting', $uploadSetting);
        return $this->fetch();
    }

    /**
     * 上传限制设置界面提交
     * @adminMenu(
     *     'name'   => '上传设置提交',
     *     'parent' => 'upload',
     *     'display'=> false,
     *     'hasView'=> false,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '上传设置提交',
     *     'param'  => ''
     * )
     */
    public function uploadPost()
    {
        if ($this->request->isPost()) {
            //TODO 非空验证
            $uploadSetting = $this->request->post();

            cmf_set_option('upload_setting', $uploadSetting);
            $this->success('保存成功！');
        }

    }

    /**
     * 清除缓存
     * @adminMenu(
     *     'name'   => '清除缓存',
     *     'parent' => 'default',
     *     'display'=> false,
     *     'hasView'=> true,
     *     'order'  => 10000,
     *     'icon'   => '',
     *     'remark' => '清除缓存',
     *     'param'  => ''
     * )
     */
    public function clearCache()
    {
        $content = hook_one('admin_setting_clear_cache_view');

        if (!empty($content)) {
            return $content;
        }

        cmf_clear_cache();
        return $this->fetch();
    }

    /**
     * 私密设置
     */
    public function configpri(){
        $siteinfo=cmf_get_option('site_info');
        $name_coin=$siteinfo['name_coin'];
        $this->assign('name_coin',$name_coin);
        $this->assign('config', cmf_get_option('configpri'));

        return $this->fetch();
    }

    /**
     * 私密设置提交
     */
    public function configpriPost(){

        if ($this->request->isPost()) {
            
            $options = $this->request->param('options/a');

            if($options['reg_reward']==''){
                $this->error("登录配置请填写注册奖励");
            }

            if(!is_numeric($options['reg_reward'])){
                $this->error("注册奖励必须为数字");
            }

            if(floor($options['reg_reward']) !=$options['reg_reward']){
                $this->error("注册奖励必须为整数");  
            }

            if($options['iplimit_times']==''){
                $this->error("登录配置请填写短信验证码IP限制次数");
            }

            if(!is_numeric($options['iplimit_times'])){
                $this->error("短信验证码IP限制次数必须为数字");
            }

            if(floor($options['iplimit_times']) !=$options['iplimit_times']){
                $this->error("短信验证码IP限制次数必须为整数");  
            }


            // if(!is_numeric($options['speak_limit'])){
            //     $this->error("发言等级限制必须为数字");
            // }

            // if(floor($options['speak_limit']) !=$options['speak_limit']){
            //     $this->error("发言等级限制必须为整数");  
            // }

            // if($options['barrage_limit']==''){
            //     $this->error("直播配置请填写弹幕等级限制");
            // }

            // if(!is_numeric($options['barrage_limit'])){
            //     $this->error("弹幕等级限制必须为数字");
            // }

            // if(floor($options['barrage_limit']) !=$options['barrage_limit']){
            //     $this->error("弹幕等级限制必须为整数");  
            // }

            // if($options['barrage_fee']==''){
            //     $this->error("直播配置请填写弹幕费用");
            // }

            // if(!is_numeric($options['barrage_fee'])){
            //     $this->error("弹幕费用必须为数字");
            // }

            // if(floor($options['barrage_fee']) !=$options['barrage_fee']){
            //     $this->error("弹幕费用必须为整数");  
            // }


            // if($options['distribut1']>40){
            //     $this->error("邀请一级分成不能大于40%！");
            // }

            // if($options['userlist_time']==''){
            //     $this->error("直播配置请填写用户列表请求间隔");
            // }

            // if(!is_numeric($options['userlist_time'])){
            //     $this->error("用户列表请求间隔必须为数字");
            // }

            // if(floor($options['userlist_time']) !=$options['userlist_time']){
            //     $this->error("用户列表请求间隔必须为整数");  
            // }
            
            // if($options['userlist_time']<5){
            //     $this->error("用户列表请求间隔不能小于5秒");
            // }

            // if($options['mic_limit']==''){
            //     $this->error("直播配置请填写连麦等级限制");
            // }

            // if(!is_numeric($options['mic_limit'])){
            //     $this->error("连麦等级限制必须为数字");
            // }

            // if(floor($options['mic_limit']) !=$options['mic_limit']){
            //     $this->error("连麦等级限制必须为整数");  
            // }

            
            
            // $game_switch=isset($_POST['game_switch'])?$_POST['game_switch']:'';
            
            // $options['game_switch']='';
            
            // if($game_switch){
            //     $options['game_switch']=implode(',',$game_switch);
            // }
            
            // if(isset($options['shop_payment_time'])){
            //     $options['shop_payment_time'] = 1;
            // }
            // $shop_payment_time=$options['shop_payment_time'];

            // if($shop_payment_time<1){
            //     $this->error("店铺付款失效时间必须大于0");
            // }

            // if(floor($shop_payment_time)!=$shop_payment_time){
            //     $this->error("店铺付款失效时间必须为正整数");
            // }

            // $shop_shipment_time=$options['shop_shipment_time'];

            // if($shop_shipment_time<1){
            //     $this->error("店铺发货失效时间必须大于0");
            // }

            // if(floor($shop_shipment_time)!=$shop_shipment_time){
            //     $this->error("店铺发货失效时间必须为正整数");
            // }
            
            // $shop_receive_time=$options['shop_receive_time'];

            // if($shop_receive_time<1){
            //     $this->error("店铺自动确认收货时间必须大于0");
            // }

            // if(floor($shop_receive_time)!=$shop_receive_time){
            //     $this->error("店铺自动确认收货时间必须为正整数");
            // }


            // $shop_refund_time=$options['shop_refund_time'];

            // if($shop_refund_time<1){
            //     $this->error("买家发起退款,卖家不做处理自动退款时间必须大于0");
            // }

            // if(floor($shop_refund_time)!=$shop_refund_time){
            //     $this->error("买家发起退款,卖家不做处理自动退款时间必须为正整数");
            // }

            
            // $shop_refund_finish_time=$options['shop_refund_finish_time'];

            // if($shop_refund_finish_time<1){
            //     $this->error("卖家拒绝买家退款后,买家不做任何操作,退款自动完成时间必须大于0");
            // }

            // if(floor($shop_refund_finish_time)!=$shop_refund_finish_time){
            //     $this->error("卖家拒绝买家退款后,买家不做任何操作,退款自动完成时间必须为正整数");
            // }

            cmf_set_option('configpri', $options,true);
            $this->resetcache('getConfigPri',$options);

            setcaches('sensitive_words',explode(',',$options['sensitive_words']));
            
            $this->success("保存成功！", '');

        }
    }

    protected function resetcache($key='',$info=[]){
        if($key!='' && $info){
            delcache($key);
            setcaches($key,$info);
        }
    }
}