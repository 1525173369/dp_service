<?php

/**
 * 商品
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\ShopGoodsModel;
use app\admin\model\GoodsImagesModel;

class GoodsController extends AdminbaseController {
    
    
    protected function getStatus($k=''){
        $status=[
            '-2'=>'管理员下架',
            '-1'=>'商家下架',
            '0'=>'审核中',
            '1'=>'通过',
            '2'=>'拒绝',
        ];
        if($k==''){
            return $status;
        }
        return isset($status[$k])?$status[$k]:'';
    }
    
    protected function initialize()
    {
         parent::initialize();
         $res = Db::name('shop_class')->field('id,name')->select();
         $this->assign('type', $res);
    }

    protected function getType($k=''){
        $type=[
            
            '0'=>'站内商品',
            '1'=>'外链商品',
        ];
        if($k==''){
            return $type;
        }
        return isset($type[$k])?$type[$k]:'';
    }
    
    function index(){
        $data = $this->request->param();
        $map=[];
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }
        

        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['goods_name','like','%'.$keyword.'%'];
        }
			

    	$lists = Db::name("shop_goods")
                ->where($map)
                ->order("id DESC")
                ->paginate(20);
        
        $lists->each(function($v,$k){
	
			$v['original_img']=get_upload_path($v['original_img']);

            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

    	$this->assign('lists', $lists);

    	$this->assign("page", $page);
        
    	$this->assign('status', $this->getStatus());
        $this->assign('type', $this->getType());
    	
    	return $this->fetch();
    }
    
    public function add()
    {
        return $this->fetch();
    }
    
    public function addPost(){
        $data = $this->request->param();
        $model = new ShopGoodsModel();
        $result         = $this->validate($data, 'ShopGoods');
        if ($result !== true) {
            $this->error($result);
        }
        $data['last_update']=time();
        if($data['goods_content']){
            $data['goods_content'] = htmlspecialchars_decode($data['goods_content']);
        }
        $image=[];
        if(isset($data['photo_urls']) && !empty($data['photo_urls'])){
            $image = $data['photo_urls'];
             unset($data['photo_urls']);
             unset($data['photo_names']);
        }  
        $id = $model->insertGetId($data);
        if(!empty($image)){
              $images = [];
              foreach ($image as $val){
                  $images[]=[
                      'goods_id'=>$id,
                      'image_url'=>$val,
                      ];
              }
          	$rs = DB::name('goods_images')->insertAll($images);
            
        }
        $this->success("添加成功！", url("Goods/index"));
        
    }
    

    //上架/下架
    function setStatus(){

        $data = $this->request->param();
        $status=$data['status'];
        
        if(isset($data['id'])){

        	$id=$data['id'];

            $goodsinfo=DB::name('shop_goods')->where("id={$id}")->find();



        	$rs = DB::name('shop_goods')->where("id={$id}")->update(['is_on_sale'=>$status]);
	        if(!$rs){
	            $this->error("操作失败！");
	        }



        }else if(isset($data['ids'])){

        	$ids = $data['ids'];
        	$rs = DB::name('shop_goods')->where('id', 'in', $ids)->update(['is_on_sale'=>$status]);
            if(!$rs){
                $this->error("操作失败！");
            }


        }
        
        $this->success("操作成功！");
    }
    
    
    function setRecom(){
        
        $id = $this->request->param('id', 0, 'intval');
        $isrecom = $this->request->param('isrecom', 0, 'intval');
        
        $rs = DB::name('shop_goods')->where("id={$id}")->update(['isrecom'=>$isrecom]);
        if(!$rs){
            $this->error("操作失败！");
        }
        
        $this->success("操作成功！");
    }
		
    function del(){

    	$data=$this->request->param();

    	if(isset($data['id'])){

    		$id = $data['id'];

           // $goodsinfo=DB::name('shop_goods')->where("id={$id}")->find();

	        $rs = DB::name('shop_goods')->where("id={$id}")->delete();
	        if(!$rs){
	            $this->error("删除失败！");
	        }

           // $title='你的商品“'.$goodsinfo['name'].'”已被平台删除。';
            //写入记录
            //$id=addSysytemInfo($goodsinfo['uid'],$title,1);
            //jPush($goodsinfo['uid'],$title);

            //删除商品访问记录
            //Db::name("user_goods_visit")->where("goodsid={$id}")->delete();


    	}else if(isset($data['ids'])){
    		$ids=$data['ids'];
    		$rs = DB::name('shop_goods')->where("id",'in',$ids)->delete();
	        if(!$rs){
	            $this->error("删除失败！");
	        }
    	}

        
        
        $this->success("删除成功！",url("Goods/index"));
    }

    //审核/详情
    function edit(){
        $id             = $this->request->param('id');
        $shopGoodsModel = new ShopGoodsModel();
        $result         = $shopGoodsModel->where('id', $id)->find();
        $images=Db::name("goods_images")->where("goods_id={$result['id']}")->select();
        $this->assign('images', $images);
       // print_r($images);
        
        if($result){
            $result = json_encode($result);
            $result = json_decode($result,1);
         
           if($result['start']){
               $result['start'] = date('Y-m-d H:i:s',$result['start']);
           }else{
               $result['start'] = '';
           } 
            if($result['end']){
               $result['end'] = date('Y-m-d H:i:s',$result['end']);
           } else{
                $result['end'] = '';
           }
        }
       
        $this->assign('result', $result);
        return $this->fetch();
    }

    //编辑提交
    public function editPost(){
        $data           = $this->request->param();
        $model = new ShopGoodsModel();
        $result         = $this->validate($data, 'ShopGoods');
        if ($result !== true) {
            $this->error($result);
        }
        $data['last_update']=time();
        if(isset($data['goods_content'])){
            $data['goods_content'] = htmlspecialchars_decode($data['goods_content']);
        }
        
        $image=[];
        if(isset($data['photo_urls']) && !empty($data['photo_urls'])){
            $image = $data['photo_urls'];
             unset($data['photo_urls']);
             unset($data['photo_names']);
        }else{
            Db::name("goods_images")->where("goods_id={$data['id']}")->delete();
        }  
        if(!empty($image)){
              $images = [];
              foreach ($image as $val){
                  $images[]=[
                      'goods_id'=>$data['id'],
                      'image_url'=>$val,
                      ];
              }
            $r = Db::name("goods_images")->where("goods_id={$data['id']}")->delete();
           // print_r($r);exit;
            DB::name('goods_images')->insertAll($images);
            
        }
        if(isset($data['start']) && $data['start']){
            $data['start'] = strtotime($data['start']);
        }
        if(isset($data['end']) && $data['end']){
            $data['end'] = strtotime($data['end']);
        }
        $model->save($data, ['id' => $data['id']]);
        $this->success("保存成功！", url("goods/index"));
    }

    //商品评论列表
    public function commentlist(){
        $data = $this->request->param();
        $goods_id=$data['goods_id'];
        $map=[];

        $map[]=['goodsid','=',$goods_id];
        $map[]=['is_append','=','0'];
        
        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['addtime','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['addtime','<=',strtotime($end_time) + 60*60*24];
        }

        
        $keyword=isset($data['keyword']) ? $data['keyword']: '';
        if($keyword!=''){
            $map[]=['content','like','%'.$keyword.'%'];
        }
            

        $lists = Db::name("shop_order_comments")
                ->where($map)
                ->order("id asc")
                ->paginate(20);
        
        $lists->each(function($v,$k){
            $v['userinfo']=getUserInfo($v['uid']);
            $v['shop_userinfo']=getUserInfo($v['shop_uid']);
            
            if($v['thumbs']){
                $thumb_arr=explode(',',$v['thumbs']);
                foreach ($thumb_arr as $k1 => $v1) {
                    $thumb_arr[$k1]=get_upload_path($v1);
                }
                $v['thumb_arr']=$thumb_arr;
            }else{
                $v['thumb_arr']=[];
            }
            if($v['video_thumb']){
                $v['video_thumb']=get_upload_path($v['video_thumb']);
            }

            if($v['video_url']){
                $v['video_url']=get_upload_path($v['video_url']);
            }
            
            

            //获取追评信息
            $append_comment=Db::name("shop_order_comments")->where("orderid={$v['orderid']} and is_append=1")->find();

            if($append_comment){

                $append_comment['userinfo']=getUserInfo($append_comment['uid']);
                $append_comment['shop_userinfo']=getUserInfo($append_comment['shop_uid']);

                if($append_comment['thumbs']){
                    $thumb_arr=explode(',',$append_comment['thumbs']);
                    foreach ($thumb_arr as $k1 => $v1) {
                        $thumb_arr[$k1]=get_upload_path($v1);
                    }
                    $append_comment['thumb_arr']=$thumb_arr;
                }else{
                   $append_comment['thumb_arr']=[];
                }

                if($append_comment['video_thumb']){
                    $append_comment['video_thumb']=get_upload_path($append_comment['video_thumb']);
                }

                if($append_comment['video_url']){
                    $append_comment['video_url']=get_upload_path($append_comment['video_url']);
                }

            }
            


            $v['append_comment']=$append_comment;


            return $v;           
        });
        
        $lists->appends($data);
        $page = $lists->render();

        $this->assign('lists', $lists);
        $this->assign('goods_id', $goods_id);

        $this->assign("page", $page);
        
        return $this->fetch();
    }

    //删除视频评论
    function delComment(){
        $id = $this->request->param('id', 0, 'intval');
        $rs=Db::name("shop_order_comments")->where("id={$id}")->delete();
        if(!$rs){
            $this->error("评论删除失败");
        }

        $this->success("删除成功");
    }

    //评论视频播放
    function videoplay(){
        $data=$this->request->param();
        $url=$data['url'];
        $this->assign('url',$url);

        return $this->fetch();
    }
    
}
