<?php
/**
 * 商品订单列表
 */
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;

class OrderController extends AdminbaseController {


    
    protected function getStatus($k=''){
        $status=[
            '-1'=>'已关闭',
            '0'=>'待付款',
            '1'=>'待发货',
            '2'=>'待收货',
            '3'=>'待评价',

        ];
        if($k==''){
            return $status;
        }
        return isset($status[$k]) ? $status[$k]: '';
    }


    
    function index(){

        $data = $this->request->param();
        $map=[];

        $start_time=isset($data['start_time']) ? $data['start_time']: '';
        $end_time=isset($data['end_time']) ? $data['end_time']: '';
        
        if($start_time!=""){
           $map[]=['add_time','>=',strtotime($start_time)];
        }

        if($end_time!=""){
           $map[]=['add_time','<=',strtotime($end_time) + 60*60*24];
        }


        $orderno=isset($data['orderno']) ? $data['orderno']: '';
        if($orderno!=''){
            $map[]=['order_sn','=',$data['orderno']];
        }

        
        $status=isset($data['status']) ? $data['status']: '';
        if($status!=''){
            $map[]=['order_status','=',$status];
        }

        $buyer_uid=isset($data['buyer_uid']) ? $data['buyer_uid']: '';
        $user_id=isset($data['user_id']) ? $data['user_id']: '';

        if($buyer_uid!=''){
             $map[]=['consignee','like','%'.$buyer_uid.'%'];
        }
        if($user_id!=''){
             $map[]=['user_id','like','%'.$user_id.'%'];
        }

        $lists = Db::name("order")
            ->where($map)
            ->order("add_time desc")
            ->paginate(20);
        

        
        $page = $lists->render();

        $this->assign('lists', $lists);

        $this->assign("page", $page);
        
        $this->assign("status", $this->getStatus());
     
        return $this->fetch();
    }

    //获取商品订单详情
    public function info(){
        $id=$this->request->param('id');
        $info=Db::name("order")->alias('A')->where("A.order_id={$id}")
        ->field("A.*,B.goods_name,goods_id,goods_num,B.goods_price as unit_price,C.user_nickname")
        ->leftJoin('user C','A.user_id = C.id')
        ->leftJoin('order_goods B','A.order_id = B.order_id')
        ->find();
        if(!$info){
            $this->error("商品订单不存在");
        }
        $journal = Db::name("order_action")->where("order_id={$id}")->order('log_time desc')->select();
        $this->assign('journal',$journal);
        $this->assign('data',$info);
        $this->assign("status", $this->getStatus());
        return $this->fetch();
    }
    
    public function editpost(){
        $data = $this->request->param();
        $actionData = [];
        $actionData['action_note']=$data['action_note'];
        if($data['order_status']==1){
            unset($data['action_note']);
            $rs = DB::name('order')->where("order_id={$data['order_id']}")->update($data);
           $actionData['status_desc']='设为已支付';
        }elseif ($data['order_status']==2) {
            unset($data['action_note']);
            $rs = DB::name('order')->where("order_id={$data['order_id']}")->update($data);
           $actionData['status_desc']='设为已发货';
        }elseif($data['order_status']==3){
            unset($data['action_note']);
            $rs = DB::name('order')->where("order_id={$data['order_id']}")->update($data);
            $actionData['status_desc']='设为已收货';
        }
        $actionData['log_time']=time();
        $actionData['order_id']=$data['order_id'];
        $actionData['action_user']=session('ADMIN_ID');
         DB::name('order_action')->insert($actionData);
        $this->success("操作成功！");
        
    }
    
    

}
