<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 小夏 < 449134904@qq.com>
// +----------------------------------------------------------------------
namespace app\admin\controller;

use cmf\controller\AdminBaseController;
use think\Db;
use app\admin\model\Menu;

class MainController extends AdminBaseController
{

    /**
     *  后台欢迎页
     */
    public function index()
    {
        $dashboardWidgets = [];
        $widgets          = cmf_get_option('admin_dashboard_widgets');

        $defaultDashboardWidgets = [
            '_SystemCmfHub'           => ['name' => 'CmfHub', 'is_system' => 1],
            '_SystemCmfDocuments'     => ['name' => 'CmfDocuments', 'is_system' => 1],
            '_SystemMainContributors' => ['name' => 'MainContributors', 'is_system' => 1],
            '_SystemContributors'     => ['name' => 'Contributors', 'is_system' => 1],
            '_SystemCustom1'          => ['name' => 'Custom1', 'is_system' => 1],
            '_SystemCustom2'          => ['name' => 'Custom2', 'is_system' => 1],
            '_SystemCustom3'          => ['name' => 'Custom3', 'is_system' => 1],
            '_SystemCustom4'          => ['name' => 'Custom4', 'is_system' => 1],
            '_SystemCustom5'          => ['name' => 'Custom5', 'is_system' => 1],
        ];

        if (empty($widgets)) {
            $dashboardWidgets = $defaultDashboardWidgets;
        } else {
            foreach ($widgets as $widget) {
                if ($widget['is_system']) {
                    $dashboardWidgets['_System' . $widget['name']] = ['name' => $widget['name'], 'is_system' => 1];
                } else {
                    $dashboardWidgets[$widget['name']] = ['name' => $widget['name'], 'is_system' => 0];
                }
            }

            foreach ($defaultDashboardWidgets as $widgetName => $widget) {
                $dashboardWidgets[$widgetName] = $widget;
            }


        }

        $dashboardWidgetPlugins = [];

        $hookResults = hook('admin_dashboard');

        if (!empty($hookResults)) {
            foreach ($hookResults as $hookResult) {
                if (isset($hookResult['width']) && isset($hookResult['view']) && isset($hookResult['plugin'])) { //验证插件返回合法性
                    $dashboardWidgetPlugins[$hookResult['plugin']] = $hookResult;
                    if (!isset($dashboardWidgets[$hookResult['plugin']])) {
                        $dashboardWidgets[$hookResult['plugin']] = ['name' => $hookResult['plugin'], 'is_system' => 0];
                    }
                }
            }
        }

        $smtpSetting = cmf_get_option('smtp_setting');

        $this->assign('dashboard_widgets', $dashboardWidgets);
        $this->assign('dashboard_widget_plugins', $dashboardWidgetPlugins);
        $this->assign('has_smtp_setting', empty($smtpSetting) ? false : true);
        //数据统计start
        $arr[0]= time()-((date('w')==0?7:date('w'))-1)*24*3600;
        $arr[1]= time()-((date('w')==0?7:date('w'))-2)*24*3600;
        $arr[2]= time()-((date('w')==0?7:date('w'))-3)*24*3600;
        $arr[3]= time()-((date('w')==0?7:date('w'))-4)*24*3600;
        $arr[4]= time()-((date('w')==0?7:date('w'))-5)*24*3600;
        $arr[5]= time()-((date('w')==0?7:date('w'))-6)*24*3600;
        $arr[6]= time()-((date('w')==0?7:date('w'))-7)*24*3600;
        $new=array();
        foreach($arr as $k=>$v){
        	
        	$new[$k]['start']=mktime(0,0,0,date("m",$v),date("d",$v),date("Y",$v));    
        	$new[$k]['end']= mktime(23,59,59,date("m",$v),date("d",$v),date("Y",$v));
        }
        $newUser=[];
        foreach($new as $k=>$v){
              $map[]=['create_time','>=',$v['start']];
              $map[]=['create_time','<=',$v['end']];
              $newUser[] = Db::name('User')->where($map)->count();
        }
        $userNum = Db::name('User')->count();
        $orderNum = Db::name('order')->count();
		$beginToday=mktime(0,0,0,date('m'),date('d'),date('Y')); 
		$endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
		$mapToday[]=['add_time','>=',$beginToday];
		$mapToday[]=['add_time','<=',$endToday];
		$newOrderNum = Db::name('Order')->where($mapToday)->count();
		$noteNum = Db::name('Dynamic')->count();
		//数据统计end
		$this->assign('noteNum',$noteNum);
        $this->assign('newUser',json_encode($newUser));
        $this->assign('userNum',$userNum);
        $this->assign('orderNum',$orderNum);
        $this->assign('newOrderNum',$newOrderNum);
        return $this->fetch();
    }

    public function dashboardWidget()
    {
        $dashboardWidgets = [];
        $widgets          = $this->request->param('widgets/a');
        if (!empty($widgets)) {
            foreach ($widgets as $widget) {
                if ($widget['is_system']) {
                    array_push($dashboardWidgets, ['name' => $widget['name'], 'is_system' => 1]);
                } else {
                    array_push($dashboardWidgets, ['name' => $widget['name'], 'is_system' => 0]);
                }
            }
        }

        cmf_set_option('admin_dashboard_widgets', $dashboardWidgets, true);

        $this->success('更新成功!');

    }

}
