<?php

$data =  [
    'en' => require_once APP_PATH.'/api/lang/en.php',
    'gb' => require_once APP_PATH.'/api/lang/gb.php',
    'ms' => require_once APP_PATH.'/api/lang/ms.php',
];
return $data;