<?php
/**
 * 推送类
 */

namespace app\api\behavior;

use app\api\model\RemindModel;
use Org\Net\Http;
use think\Db;
use think\facade\Config;
use think\facade\Log;
use think\Hook;
class SendPush
{
    /**
     * @var
     * 语言中 ,由于ios和安卓不同可能出现不同
     */
    protected $defaultLang = 'gb';  //默认中文发送
    protected $uid;  //发起者

    // 1.评论,2收藏,3.点赞评论,4.回复评论和@他人,5.点赞,
    protected $type = 1;
    protected $user;
    protected $key = 'lang:';  //获取到当前用户属于那个语言, 在phpapi种的 user.switchingLanguage Api中
    protected $title = '';
    protected $content = '';
    protected $android = [];  //安卓消息结构体
    protected $ios  = [];  //ios消息结构体
    protected $config = [];
    protected $ios_plat ;
    protected $android_plat;
    protected $to_uid;  //接收者
    protected $target_id;  //目标源ID
    protected $remind = '';
    protected $send_type = 2;
    protected $is_at = 0;
    public function  run($param){
        try {
         
            $uid = $param['uid'];
            $title = @$param['title'];
            $type = $param['type'];
            $content = @$param['content'];
            $is_at = (isset($param['is_at']) && $param['is_at'] == 1)?1:0;
            #var_dump($param);
            $this->is_at = $is_at;
            $touid = $param['to_uid'];
            $this->uid = $uid;
            $this->to_uid = $touid;
            if($this->uid == $this->to_uid){
                return ;
            }
            if(isset($param['video_id'])){  //如果是视频推送
                $this->target_id = $param['video_id'];
            }
            if(isset($param['send_type'])){
                $this->send_type = $param['send_type'];
            }

            $this->type = $type;
            //获取当前用户语言
            $lang = $GLOBALS['redisdb']->get($this->key.$touid);
         
         
            switch ($lang){
                case 'ms':
                    $this->defaultLang = 'ms';
                    break;
                case 'en':
                    $this->defaultLang = 'en';
                    break;
                default:
                    $this->defaultLang = 'gb';
                    break;
            }
        
         
            $this->excuteBody();
            $this->TencentPush();
        }catch (\Exception $e){
            echo $e->getMessage();die;
            Log::error('hook钩子出现问题:传入参数为'.json_encode($param).'--问题为:'.$e->getMessage().'--行数为:'.$e->getLine());
        }

    }
    //处理内容问题
    public function  excuteBody(){
        $title = $this->title;
        $content = $this->content;
        $type = $this->type;
        $remind  = '';
        if(!$title || !$content){
            if($type == 2){
                $remind =  __('用户收藏了你的视频');
                $title  = __('收藏了你的视频',$this->defaultLang);
                $content  = __('收藏了你的视频',$this->defaultLang);
                
               
            }
            if($type == 1){
                $remind = __('用户评论了你的视频');
                $title  = __('评论了你的视频',$this->defaultLang);
                $content  = __('评论了你的视频',$this->defaultLang);
                
               
            }
            if($type == 3){
                $remind = __('用户点赞了你的评论');
                $title  = __('点赞了你的评论',$this->defaultLang);
                $content  = __('点赞了你的评论',$this->defaultLang);
                
             
            }
            if($type == 4){
              
                //评论评论或者@Ta人消息
                if(!$this->is_at){
                     $remind =  __('用户评论了你的评论');
                     $title  = __('评论了你的评论',$this->defaultLang);
                     $content  = __('评论了你的评论',$this->defaultLang);
                }else{
                     $remind =  __('在评论中提及你');
                     $title  = __('在评论中提及你',$this->defaultLang);
                     $content  = __('在评论中提及你',$this->defaultLang);
                     
                }
               
                
              
            }
            if($type == 5){
                $remind =  __('用户点赞了你的视频');
                $title  = __('点赞了你的视频',$this->defaultLang);
                $content  = __('点赞了你的视频',$this->defaultLang);
                
              
            }
            if($type == 6){
                $remind =  __('用户分享了你的视频');
                $title  = __('分享了你的视频',$this->defaultLang);
                $content  = __('分享了你的视频',$this->defaultLang);
               
            }
            $this->remind = $remind;

        }
        $this->title = $title;
        $this->content = $content;
      


    }

    /**
     * @param $data
     * 腾讯推送 TPNS
     */
    public function TencentPush(){
       
        $user = getUserInfo($this->to_uid);
        $users = getUserInfo($this->uid);
        $title = $users['user_nickname'].' '.$this->title;
      
        $content =$users['user_nickname'].' '. $this->content;
        $info = getConfigPri();
        $android = array(
            'AccessId' => $info['TPNS_android_id'],
            'SecretKey' => $info['TPNS_android_secret'],
        );

        $ios = array(
            'AccessId' => $info['TPNS_ios_id'],
            'SecretKey' => $info['TPNS_ios_secret'],
        );
        $this->android_plat = $android;
        $this->ios_plat = $ios;
        $host = $info['TPNS_service']; //域名
        $envirement = $info['TPNS_sandbox'];  //环境
        $api = $host.'/v3/push/app';
        $data = [
            'audience_type' => 'token_list',
            'message_type' => 'notify',
            'environment' => $envirement,
            'message' =>  [
                'title' => $title,
                'content' => $content,
            ]
        ];
       

        $return = $this->curl($api,$data);
        if($return){
            if($this->type != 6){  //除了分享不记录
                //模型类型为:0收藏1点赞2评论3订阅（关注）4系统消息5问卷
                //钩子:1.评论,2收藏,3.点赞评论,4.回复评论和@他人,5.点赞,
                switch ($this->type){
                    case 1:
                        $action = 2;
                        break;
                    case 2:
                        $action = 0;
                        break;
                    case 3:
                        $action = 1;
                        break;
                    case 4:
                        $action = 2;
                        break;
                    case 5:
                        $action = 1;
                        break;
                    default:
                        $action =  0;
                        break;
                }
                $remind = [
                    'type' => $action,
                    'user_id' => $this->to_uid,
                    'target' => 'video',
                    'target_id' =>  $this->target_id,
                    'target_owner' => $this->uid,
                    'action' => $this->remind,
                    'remind_type' => 1
                ];
                RemindModel::create($remind);  //创建消息记录
            }
        }
    }
    protected function curl($url,$data){
       
        
        $push = Db::name('user_pushid')->where('uid',$this->to_uid)->find();
       
        if(!$push){
            return false;
        }
        $terminal = $push['terminal'];
        $config = [];
        if($terminal  == 'ios'){
            $config = $this->ios_plat;
        }
        if($terminal == 'android'){
            $config  =$this->android_plat;
        }
        
        if ($terminal == "android") {
            $custom_content = ['type' => $this->send_type];
            // 动作类型
            $action_type = ['action_type' => '1', 'activity' => 'com.askpert.review'];
            $terminalMsg = ['badge_type' => -2, 'custom_content' => json_encode($custom_content), 'action_type' => $action_type];
        } else if ($terminal == "ios") {
            $custom_content = ['type' => $this->send_type];
            $terminalMsg = ['badge_type' => -2, 'aps' => array("alert" => array($data['message']['title'] => $data['message']['content'])), 'custom_content' => json_encode($custom_content)];
        }
        $data['message'][$terminal] = $terminalMsg;
        
       
       
       
        if(!$config){
            return false;
        }
        $token_list = [
            $push['pushid']
        ];

        $data['token_list'] = $token_list;
     
        $data = json_encode($data);
        $time = time();
        $str = $time . $config['AccessId'] . $data;
        $sign = base64_encode(hash_hmac('sha256', $str, $config['SecretKey']));

        $head =  array('Sign:' . $sign, 'TimeStamp:' . $time, 'AccessId:' . $config['AccessId']);
        $ch = curl_init ();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $head);
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, 1 );
        curl_setopt ( $ch, CURLOPT_HEADER, 0 );
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $data );
        $return = curl_exec ( $ch );
        curl_close ( $ch );
    
        return $return;
    }










}