<?php


namespace app\api\model;


class CommentsModel extends BaseModel
{
    protected $name = 'video_coments';
    public function users(){
       return $this->hasOne(UsersModel::class,'id','uid');
    }
    public function userLikes(){
        return $this->hasOne(UserVideoCommentLikesModel::class,'comment_id','id');
    }
}