<?php


namespace app\api\model;


use cmf\lib\Storage;
use Think\Db;
use think\Exception;
use think\File;
use think\Validate;

class VideoModel extends BaseModel
{
    protected $name = 'video';
    public  $softDelete = 1;
    public $_validate = 1;

    public function getValidate(){
        $validate = new Validate([
            'uid'  => 'require',
           /* 'desc' => 'require|min:3',*/
           /* 'shop_id' => 'require',*/
            'video_url' => 'require',

        ]);
        return $validate;
    }

    public function comments()
    {
       return $this->hasMany(CommentsModel::class,'video_id','id')->limit($this->offset,$this->limit);
    }
    public function users(){
        return $this->hasOne(UsersModel::class,'id','uid');
    }
    public function shops(){
        return $this->hasOne(ShopModel::class,'id','shop_id');
    }
    

    public  static function userAttention($uid,$to_uid){
        $bool = Db::name('user_attention')->where('uid',$uid)->where('touid',$to_uid)->count();
        if($bool){
            return 1;
        }
        return 0;
    }
    //设置话题id时
    public function setTopicAttr($value,$data){
        $custom = $data['custom'];
        if($custom && is_string($custom)){
            if(is_string($value)){
                $value = explode(',',$value);
            }
            $custom = explode(',',$custom); //转为数组
            foreach ($custom as $k=>$v){
                if($v){
                    $topic_id = $this->getTopic($v);
                    if(!in_array($topic_id,$value)){
                        $value[] = $topic_id;
                    }
                }
            }
        }
        if(is_array($value)){
            $value = implode(',',$value);
        }
        return $value;
    }

    public function setCustomAttr($value){
        if($value && is_array($value)){
            $value = explode(',',$value);
        }
        return $value;
    }


   
    public function convertToFlv($input)  //视频转码操作
    {
        $name = time() . mt_rand(1000, 9999) . '.png';
        $output = '/' . FWS . $name;//time().mt_rand(1000,9999).'.png';
        $v = str_replace('https', 'http', $input);
        $command = "/usr/local/ffmpeg/bin/ffmpeg -v 0 -y -i $v -vframes 1 -ss 3 -vcodec mjpeg -f rawvideo  -aspect 16:9 $output";
        system($command, $code);
        if (!$code) {
            $storage = cmf_get_option('storage');
            $storage = new Storage($storage['type'], $storage['storages'][$storage['type']]);

            $file = new File('ffmpeg/' . $name, 'r');

            $file_path = $file->getPath();
            $uploadPath = WEB_ROOT . 'upload/';
            $fileType = $file->getType();
            $result = $storage->upload($name, 'ffmpeg/' . $name, $fileType);
            try {
                $url = $result['url'];
                unlink($output);
            } catch (\Exception $e) {
                $url = '/ffmpeg/' . $name;
            }
            return $url;
        }
    }


    public function getTopicAttr($value)
    {
        if(!$value){
            return [];
        }
        if ($value) {
            $value = explode(',', $value);
        } else {
            $value = [];
        }
        return $value;

    }

    public function getCustomAttr($value)
    {
        if ($value) {
            $value = explode(',', $value);
        } else {
            $value = [];
        }
        return $value;

    }

    public function getVideoUrlAttr($value)
    {
        if ($value) {
            return get_upload_path($value);
        }
        return '';
    }

    public function getVideoImgAttr($value)
    {
        if ($value) {
            return get_upload_path($value);
        }
        return '';
    }

    public function getTopic($name){
        $name = trim($name);
        $key = 'topic:'.$name;
        $topic_id = getcaches($key);
        if(!$topic_id){
            $result =Db::name('shop_topic')->where('topic_name',$name)->field('topic_id')->find();
            if($result){
                $topic_id = $result['topic_id'];
                setcaches($key,$topic_id,86400*7);
            }else{ //新增topic
                $data = [
                    'topic_name' => $name,
                    'addtime' => time()
                ];
                $topic_id = Db::name('shop_topic')->insertGetId($data);
            }
        }
        return $topic_id;
    }

    public function  usersLikes(){
        return $this->hasMany(UserVideoLike::class,'video_id','id');
    }
    public function checkInfo($id){
        $video_cache_key = $this->name.':'.$id;
        if(!$info = getcaches($video_cache_key)){
            $info = $this->where('id',$id)->find();
            if($info){
                $info  = $info->toArray();
                setcaches($video_cache_key,$info,7200);
            }
        }
        if(!$info){
            throw new Exception('不存在该视频',400);
        }

        if(!$info['status']){
            throw new Exception('该视频已经下架',400);

        }
        if($info['is_delete']){
            throw new Exception('该视频已经被删除了',400);

        }
        return $info;
    }





}