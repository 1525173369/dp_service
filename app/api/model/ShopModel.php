<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Powerless < wzxaini9@gmail.com>
// +----------------------------------------------------------------------
namespace app\api\model;

use think\Db;
use think\Model;

class ShopModel extends Model
{
    protected $name = 'shop_apply';
    public function getThumbAttr($value){
        if($value){
            $value = get_upload_path($value);
        }
        return $value;
    }
 
   
}