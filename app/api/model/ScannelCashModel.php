<?php


namespace app\api\model;


class ScannelCashModel extends BaseModel
{
    protected $name = 'scannel_cash';

    public function users(){
        return $this->hasOne(UsersModel::class,'id','uid')->setEagerlyType(0);
    }
    public function shops(){
        return $this->hasOne(ShopModel::class,'id','shopid')->setEagerlyType(0);
    }
    public function  getStatusAttr($value){
      
        $lan = isset($_GET['lan'])?$_GET['lan']:'gb';
        $success = '成功';
        $fail ='失败';
        $ing = '申请中';
         $finish = '已完成';
        if($lan  == 'en'){
            $success = 'Success';
            $fail  = 'Failed';
            $ing = 'Being processed';
            $finish = 'Completed';
        }
        if($lan == 'ms'){
            $success = 'Berjaya';
            $fail = 'Gagal';
            $ing = 'Sedang diproses';
            $finish = 'Completed';
        }
        if($value == 1){
            return '<span style="color: green">'.$success.'</span>';
        }
        if($value == 2){
            return '<span style="color: red">'.$fail.'</span>';
        }
        if($value == 0){
            return '<span style="color: grey">'.$ing.'</span>';
        }
        if($value == 3){
             return '<span style="color: orange">'.$finish.'</span>';
        }
    }
}