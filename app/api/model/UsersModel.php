<?php


namespace app\api\model;


use think\Model;

class UsersModel extends Model
{
    protected $name = 'user';
    public $pk = 'id';
    public function getAvatarAttr($value){
        if($value){
            return get_upload_path($value);
        }
        return $value;
    }
}