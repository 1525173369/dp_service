<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Powerless < wzxaini9@gmail.com>
// +----------------------------------------------------------------------
namespace app\api\model;

use think\Db;
use think\Model;

class ShopLogModel extends BaseModel
{
    protected $name = 'shop_log';
     //1是收益 2是支付 coin变化的金币,action 说明  
    public static function addlog($id,$coin,$action,$type = 1,$ordersn = ''){
        $info = ShopModel::find($id);
        $before_coin = $info['coin'];
        if($type == 1){
            $after_coin = $before_coin + $coin;
        }else{
            $after_coin = $before_coin - $coin;
        }
        $data  = [
           'shop_id' => $id,
           'before_coin' => $before_coin,
           'change_coin' => $coin,
           'action' => $action,
           'type' =>  $type,
           'after_coin' => $after_coin,
           'ordersn' =>  $ordersn
        ];
        return self::create($data);
    }

    public function shops(){
        return $this->hasOne(ShopModel::class,'id','shop_id');
    }
}