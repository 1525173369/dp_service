<?php


namespace app\api\model;


class UserVideoCollectModel extends BaseModel
{
    protected $name = 'user_video_collect';
    public $pk = 'uid,video_id';
    public function users(){
        return $this->hasOne(UsersModel::class,'id','uid')->field('id');
    }
    public function videos(){
          return $this->hasOne(VideoModel::class,'id','video_id')->field('id')->where('is_delete',0)->where('draft',0);
    }
}