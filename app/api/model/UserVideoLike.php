<?php


namespace app\api\model;


class UserVideoLike extends BaseModel
{
    protected $name = 'user_video_like';
    public function users(){
        return $this->hasOne(UsersModel::class,'id','uid')->setEagerlyType(0);
    }
}