<?php


namespace app\api\model;


class UserVideoVisitsModel extends BaseModel
{
    protected $name = 'video_visits';
    public function users(){
        return $this->hasOne(UsersModel::class,'id','uid')->setEagerlyType(0);
    }
}