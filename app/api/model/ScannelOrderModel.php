<?php


namespace app\api\model;


class ScannelOrderModel extends BaseModel
{
    protected $name  = 'scannel_order';
    public function users(){
        return $this->hasOne(UsersModel::class,'id','uid')->setEagerlyType(0);
    }
    public function susers(){
        return $this->hasOne(UsersModel::class,'id','shop_uid')->setEagerlyType(0);
    }
    public function shops(){
        return $this->hasOne(ShopModel::class,'id','shopid')->setEagerlyType(0);
    }
    
    
    public function users1(){
        return $this->hasOne(UsersModel::class,'id','uid')->setEagerlyType(0);
    }
    public function susers1(){
        return $this->hasOne(UsersModel::class,'id','shop_uid');
    }
    public function shops1(){
        return $this->hasOne(ShopModel::class,'id','shopid');
    }
}