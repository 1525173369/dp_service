<?php


namespace app\api\model;


use app\admin\model\UsersModel;
use think\Validate;

class VideoComentsModel extends BaseModel
{
    protected $name ='video_coments';
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $autoWriteTimestamp = true;
    public function users(){
        return  $this->hasOne(UsersModel::class,'id','uid')->setEagerlyType(0);
    }
    public function getValidate(){
        $validate = new Validate([
            'uid'  => 'require',
            'content' => 'require|min:1',

        ]);
        return $validate;
    }
    public static function details($id){
        $key = 'comment:'.$id;
        $info = getcaches($key);
        if(!$info){
            $info = self::where('id',$id)->find();
            if($info){
                setcaches($key,$info);
            }
        }
        return $info;
    }

}