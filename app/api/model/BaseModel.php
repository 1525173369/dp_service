<?php


namespace app\api\model;


use think\Model;
use think\Validate;

class BaseModel extends Model
{
    protected $createTime = 'createtime';
    protected $updateTime = 'updatetime';
    protected $autoWriteTimestamp = true;
    public $softDelete = false;
    public $deleteField = 'is_delete';
    public $page  = 1;
    public $limit = 15;
    public $offset = 0;
    public $pk = 'id';
    public $_validate = false;  //是否验证,如果需要则开启验证
    public function getValidate(){
        $validate = new Validate([

        ]);
        return $validate;
    }
    public function deletRedisCache($id){
        $key  = $this->name.':'.$id;
        delcache($key);
    }
    /**
     * 更新缓存不更新数据库
     * @param $id
     */
    public function updateRedisInfo($id,$data){
        $video_cache_key = $this->name.':'.$id;
        $info = getcaches($video_cache_key);
        $ret = true;
        if($info){
           
            foreach ($data as $k=>$v){
                $info[$k] = $v;
            }
            $ret = setcaches($video_cache_key,$info);
        }
        return $ret;
    }
    public function detail($id){
        $key = $this->name.':'.$id;
        $info = getcaches($key);
        if(!$info){
            $info = $this->where('id',$id)->find();
            if($info){
                $info = $info->toArray();
                setcaches($key,$info);
            }
        }
        return $info;
    }


}