<?php


namespace app\api\model;


use think\Validate;

class CashBankModel extends BaseModel
{
    protected $name = 'scannel_cash_bank';

    public function getValidate(){
        $validate = new Validate([
            'uid'  => 'require',
            'bank_name' => 'require',
            'bank_account' => 'require',
            'bank_username' => 'require',
            'mobile' => 'require',
            'is_default' => 'require',
        ]);
        return $validate;
    }
    public function detail($id,$type = 0){
        if($type){
            $info  = $this->where('id',$id)->find();
        }else{
            $key = 'cash_bank:'.$id;
            $info  = getcaches($key);
            if(!$info){
                $info  = $this->where('id',$id)->find();
                if($info){
                    setcaches($key,$info,7*86400);
                }
            }
        }
        return $info;
    }
    public function deletRedisCache($id){
        $key  = $this->name.':'.$id;
        delcache($key);
    }


}