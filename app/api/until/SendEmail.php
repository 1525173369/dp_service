<?php


namespace app\api\until;


use PHPMailer\PHPMailer\PHPMailer;
use Think\Exception;

class SendEmail
{
    protected $host = 'smtpdm-ap-southeast-1.aliyun.com';
    protected $Username  = 'noreply@sys.askpert.com';
    protected $password = 'QnDRFJw0tAAXt14s';
    protected $Subject = 'ASKPERT';

    protected $FromName = 'ASKPERT';

    protected $body;
    public $error_code = 0;
    public $error_msg = '';

    public  function setBody($content){
        $this->body = $content;
        return  $this;
    }
    public function __set($name,$value){
        $this->$name = $value;
        return $this;
    }
    public function __get($name){
        return $this->$name;
    }


    /**
     * @param $email  邮箱号
     * @param $deserver_id  设备ID
     * @param bool $is_request 是否为请求
     * @return bool
     * @throws \PHPMailer\PHPMailer\Exception
     */
    public function sendEmail($email){
        try {
            //vzbtqxiymozfbdjc
            //olwmaveedctcbdje

            if(!$this->body){
                throw new Exception('请设置body');
            }
            // 实例化PHPMailer核心类
            $mail = new PHPMailer(true);
            // 是否启用smtp的debug进行调试 开发环境建议开启 生产环境注释掉即可 默认关闭debug调试模式
           // $mail->SMTPDebug = 3;
            // 使用smtp鉴权方式发送邮件
            $mail->isSMTP();

            // smtp需要鉴权 这个必须是true
            $mail->SMTPAuth = true;
            // 链接qq域名邮箱的服务器地址
            $mail->Host = $this->host;
            // 设置使用ssl加密方式登录鉴权
            $mail->SMTPSecure = 'ssl';
            // 设置ssl连接smtp服务器的远程服务器端口号
            $mail->Port = 465;
            // 设置发送的邮件的编码
            $mail->CharSet = 'UTF-8';
            // 设置发件人昵称 显示在收件人邮件的发件人邮箱地址前的发件人姓名
            $mail->FromName = $this->FromName;
            // smtp登录的账号 QQ邮箱即可
            $mail->Username = $this->Username;
            // smtp登录的密码 使用生成的授权码
            $mail->Password = $this->password;
            // 设置发件人邮箱地址 同登录账号
            $mail->From = $this->Username;
            // 邮件正文是否为html编码 注意此处是一个方法
            $mail->isHTML(true);
            // 设置收件人邮箱地址

            $mail->addAddress($email);
            // 添加该邮件的主题
            $mail->Subject = $this->Subject;
            // 添加邮件正文
            #$href = $this->request->domain().'/api/video/emailOk/key/'.$deserver_id;
            $mail->Body = $this->body;
            // 为该邮件添加附件
            //$mail->addAttachment('./example.pdf');
            // 发送邮件 返回状态

            $status = $mail->send();

            if(!$status){
                throw new  Exception('未能发送成功邮件',40062);
            }
            return $status;

        } catch (Exception $e) {

            $this->error_code = $e->getCode();
            $this->error_msg = $e->getMessage();
            return false;
        }
    }


}