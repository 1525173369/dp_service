<?php
namespace app\api\until;


use Obs\Internal\Common\Model;
use Obs\ObsClient;
use think\File;

class ObsUpload
{
    protected $access_key = 'MHWLZKSQ5MQUEFWWUOX4';
    protected $secret = 'NoJ9YEm4PoVXePmTos2QXNH4JfbA84qluxBYmWWj';
    protected $endpoint = 'obs.ap-southeast-3.myhuaweicloud.com';
    protected $obsClient;  //obs客户端
    protected $Bucket = 'askpert-oss'; //仓库
    protected $host = 'https://static-ap.askpert.com';
    protected $key = '';
    protected $ext = 'jpg';
    public  $errorMsg;
    public  $errorCode;

    public function __construct()
    {
        $obsClient = new ObsClient([
            'key' => $this->access_key,
            'secret' => $this->secret,
            'endpoint' => $this->endpoint
        ]);
        $this->obsClient = $obsClient;
    }
    public function upload(File $file,$isadmin = false){
        try {
            $file = $file->getPathname();
            if($isadmin){
                $this->key =  md5(time().mt_rand(1000,9999));
            }else{
                $this->key =  'user_auth/'.md5(time().mt_rand(1000,9999));
            }

            $return = $this->obsClient->putObject([
                'Bucket' => $this->Bucket,
                'Key' => $this->key.'.'.$this->ext,
                'Body' => fopen($file, 'r')
            ]);
            $return = $return->toArray();
            if($isadmin){
                $data =[
                    'name' => $this->key.'.'.$this->ext,
                    'filepath' =>  $this->host.'/'.$this->key.'.'.$this->ext,
                    'preview_url' =>   $this->host.'/'.$this->key.'.'.$this->ext,
                    'url' =>   $this->host.'/'.$this->key.'.'.$this->ext,
                ];
                return $data;
            }
            return  $this->host.'/'.$this->key.'.'.$this->ext;
        }catch (\Exception $e){
            $this->setError($e->getMessage(),$e->getCode());
            return false;
        }
    }
    //设置桶
    public function setBucket($buck){
        $this->Bucket = $buck;
        return $this;
    }
    //设置key
    public function setKey($key){
        $this->key = $key;
        return $this;
    }
    //设置访问域名
    public function setHost($value){
        $this->host = $value;
        return $this;
    }
    //获取访问host
    public function getHost(){
        return $this->host;
    }
    //设置后缀
    public function setExt($value){
        $this->ext = $value;
        return $this->ext;
    }
    public function setError($msg,$code){
        $this->errorMsg = $msg;
        $this->errorCode = $code;
        return $this;
    }
    public function getError(){
        return $this->errorMsg.'--code码为:'.$this->errorCode;
    }
}