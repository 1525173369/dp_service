<?php


namespace app\api\controller;


use think\Db;

class CronTabController
{
    /**
     * 每分钟一次
     * 自动任务到账操作
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public function start(){
       $list =  Db::name('user_forward_coin')->where('forward_time','<',time())->where('status',0)->select();
     
       if($list){
           foreach ($list as $k=>$v){
               $uid  = $v['uid'];
               $coin = $v['coin'];
              
               Db::name('user')->where('id',$uid)->setInc('coin',$coin);
               Db::name('user_forward_coin')->where('id',$v['id'])->update(['status'=>1]);
               $user_log_id = $v['user_log_id']; //获取到日志id
               Db::name('user_log')->where('id',$user_log_id)->update(['is_hidden'=>0]);
           }
       }
       exit(0);
    }
    //obs视频转码
    public function obsVideoC(){
        connectionRedis();
        $redis_k = 'token_obs_zhuan';
        if(!$token = getcaches($redis_k)){
            $token = $this->getToken();
            setcaches($redis_k,$token,60000);
        }
       
        $list = Db::name('obs')->where('status',0)->limit(0,30)->select();
        $project_id = '0b26aac5d9000fe92f34c0047be91405';
        foreach ($list as $k=>$v){
                $url = $v['url'];
                $url =str_replace('https://static-ap.askpert.com/','',$url);
                list($file_name,$ext) = explode('.',$url);
                $data = [
                    'input' => [
                        'bucket' => 'askpert-oss',
                        'location' => 'ap-southeast-3',
                        'object' => $url
                    ],
                    'output' => [
                        'bucket' => 'askpert-oss',
                        'location' => 'ap-southeast-3',
                        'object' => '/'
                    ],
                    "trans_template_id"=> [7000784],
                    "output_filenames" => [$file_name.'.'.$ext]
                ];
                $data = json_encode($data);
               
                $api = 'https://mpc.ap-southeast-3.myhuaweicloud.com/v1/'.$project_id.'/transcodings';
                $header = [
                    'X-Auth-Token:'.$token,
                    'Content-Type:application/json'
                    ];
                $ret = Post($data,$api,false,$header);
                $ret = json_decode($ret,1);
                if(isset($ret['task_id'])){
                     Db::name('obs')->where('id',$v['id'])->update(['status'=>1,'task_id'=>$ret['task_id']]);
                }
        }
        
       
     
        
    }
    
    
    public function getSelectObs(){
        $api = 'https://mpc.ap-southeast-3.myhuaweicloud.com/v1/0b26aac5d9000fe92f34c0047be91405/transcodings?task_id=';
        connectionRedis();
        $redis_k = 'token_obs_zhuan';
        if(!$token = getcaches($redis_k)){
            $token = $this->getToken();
            setcaches($redis_k,$token,60000);
        }
       
        $list = Db::name('obs')->where('status',1)->where('task_id','>',0)->limit(0,30)->select();
        $header = [
                    'X-Auth-Token:'.$token,
                    'Content-Type:application/json'
        ];
        foreach ($list as $k=> $v){
           $content = $this->curlGet($api.$v['task_id'],$header);
           $content = json_decode($content,1);
           $status  = $content['task_array'][0]['status'];
           if($status == 'SUCCEEDED'){
               //修改video的视频地址
               if($v['video_id']){
                     $video = Db::name('video')->where('id',$v['video_id'])->find();
                     if($video){  //改变资源路径
                         Db::name('video')->where('id',$v['video_id'])->update(['video_url'=>$v['url'].'?time=1987']); //修改视频url为转码后的地址
                     }
                }
                 if($v['dynamic_id']){
                     $dynamic = Db::name('dynamic')->where('id',$v['dynamic_id'])->find(); 
                     //因为视频可能是多个
                     $video = $dynamic['video'];  //获取到的视频
                     $video = explode(',',$video);
                   
                     foreach ($video as $kv=>$vv){
                        if(strpos($vv,$v['url']) !==false){
                            #echo '包含该字符串';
                              $vv = $v['url'].'?time=1987';
                        }
                        $video[$kv] = $vv;
                     }
                     $video = implode(',',$video);
                     $dynamic = Db::name('dynamic')->where('id',$v['dynamic_id'])->update(['video'=>$video]); //修改转码
                 }
                  Db::name('obs')->where('id',$v['id'])->update(['status'=>2]);
           }
        }
        
        
    }
    
    
    
        //get
    public function curlGet($url,$header){
 
       
 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }
    
    
    public function getToken(){
         $r ='{
                "auth": {
                    "identity": {
                        "methods": [
                            "password"
                        ],
                        "password": {
                            "user": {
                                "domain": {
                                    "name": "hwc99481703"        
                                },
                                "name": "hwc99481703",            
                                "password": "MfQ4KB5zyLBXxHH"  
                            }
                        }
                    },
                    "scope": {
                        "project": {
                            "name": "ap-southeast-3"              
                        }
                    }
                }
            }';
        $f = Post($r,'https://iam.myhuaweicloud.com/v3/auth/tokens?nocatalog=true',1); //这儿拿token是正常的
        #$r  = json_decode($f,1);
        
        $token =   $GLOBALS['G_HEADER']['X-Subject-Token'];
        return $token;
    }





}