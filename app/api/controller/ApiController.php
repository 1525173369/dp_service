<?php

namespace app\api\controller;





use app\api\model\VideoModel;
use think\Collection;
use think\facade\Config;
use Think\Db;
use think\facade\App;
use think\Request;


abstract class ApiController
{
    protected $request;
    protected $model = false;  //模型类
    protected $app;
    protected $method;
    protected $limit;
    protected $page;
    protected $order;
    protected $orderField;
    protected $sonWhere=[];  //子查询的条件
    protected $with = [];
    protected $field= [];
    protected $lang = [];
    protected $lan = 'gb';
    protected $action = 1;
    public function __construct(Request  $request,App  $app)
    {
        header('content-type:application/json');
        connectionRedis();

        $this->request = $request;
        $this->baseModel();
        if(!$this->model){
            return $this->error('服务端内部错误,请联系管理员处理~');
        }
        $this->getLang();
        $this->app =$app;
        $page = $this->request->get('p',1);
        $this->page = $page;
        $limit = $this->request->get('limit',15);
        $this->limit = $limit;
        $this->order = $this->request->get('order','desc');
        $this->orderField = $this->request->get('order_field');
        if(!$this->orderField){
            $this->orderField = $this->model->pk;
        }

    }
    public function getConfig(){
        $config =  getConfigPub();
        $data  = [$config];
        $this->success('获取成功',$data);
    }
    public function getLang(){
        $lan = $this->request->request('lan','zh');
        if($lan == 'ms'){
            $lang = Config::get()['config']['ms'];
            $this->lang = $lang;
        }else if($lan == 'en'){
            $lang = Config::get()['config']['en'];
            $this->lang = $lang;
        }else{
            $lang = Config::get()['config']['gb'];
            $this->lang = $lang;
        }
    }



    //接口分发方式
    public function baseModel()
    {  //结构还可以增加
        // TODO: Implement baseModel() method.
        //$model = $this->app->make(new VideoModel());
        $model = new $this->model;
        $this->model = $model;
    }

    /**
     * 重组方式
     * @param $data
     * @return array|array[]|\array[][]|\array[][][]
     */
    public function ReclearData($data){
        $data  = Collection::make($data)->toArray();
        return $data;
    }

    public   final  function index(Request  $request){
        $method = $this->request->method();
        $method = strtolower($method); //转消息
        $this->method = $method;
        return $this->$method();
    }
    public function edit($id){
        try {
            if($this->request->method() != 'PUT'){
                //abort(404,'该方法不正确');
            }
            $model = $this->model->where('id',$id)->find();
            if(!$model){
                $this->error('不存在该模型数据');
            }
            $data = $this->request->request();
            $bool = $this->model->save($data,['id'=>$id]);
            if($bool){
                $this->success('更新成功');
            }
            $this->error('更新失败');
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }





    //分发功能
    public  function get(){

        // TODO: Implement get() method.
        $request = $this->request;
        $token = $request->request('token');
        #$this->checkToken($token);
        $get = $request->get(); //获取所有的参数
        $sonWhere = $this->sonWhere;
        $where = function($q)use($get,$sonWhere){
            foreach ($get as $k => $v){
                if(in_array($k,$this->field)){
                    $q->where($k,$v);
                }
            }
        };
        if($this->with){
            $model = $this->model->with($this->with);
        }else{
            $model = $this->model;
        }

        $data = $model->where($where)->where($sonWhere)->page($this->page,$this->limit)->order($this->orderField,$this->order)->select();
       
       
        $data = $this->ReclearData($data);  //整理数据会掉钱
        $data = [
            ['list' => $data]
        ];
        $this->success('获取成功',$data);
    }

    public  function delete($id){
        try {
            if($this->request->method() != 'DELETE'){
                //abort(404,'该方法不正确');
            }
            $softDelete = $this->model->softDelete;
            if($softDelete){  //表示软删除
                $dxr = [
                    $this->model->deleteField => 1
                ];
                $bool = $this->model->where('id',$id)->update($dxr);
            }else{
                $bool = $this->model->where('id',$id)->delete();
            }
            if($bool){
                $this->model->deletRedisCache($id);
            }
            $bool?$this->success('删除成功'):$this->error('删除失败');
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }

    }
    public  function post(){
        // TODO: Implement post() method.
        try {
            $request = $this->request;
            $data = $this->request->post();
            if($this->model->_validate){
                $validate = $this->model->getValidate();
                if($validate){
                    $check = $validate->check($data);
                }

                if(!$check){
                    return $this->error($validate->getError());
                }
            }
            $bool  = $this->model->create($data);
            if($bool){
                $this->success('保存成功');
            }
            $this->error('保存失败');

        }catch (\Exception $e){
            $this->error($e->getMessage());
        }


    }


    public final function result($code = 0,$msg = '',$data = [], $ret = 200){
        $data = [
            'ret' => $ret,
            'msg' => '',
            'data' => [
                'code' => $code,
                'info' => $data,
                'msg' => $msg
            ]
        ];
        $response = app('request')->request('response','json');   //响应方式
        if($response == 'xml'){
            exit(xml($data)->send());
        }

        exit(json($data)->send());
    }
    public final function success($msg = '操作成功',$data =[],$code = 0){
        if($this->action){
            $data = $this->_unsetNull($data);
        }
        if(isset($this->lang[$msg])){
            $msg = $this->lang[$msg];
        }

        return $this->result($code,$msg,$data);
    }

    public final function error($msg = '操作失败',$code = -1,$data = []){
        if(isset($this->lang[$msg])){
            $msg = $this->lang[$msg];
        }
        return $this->result($code,$msg,$data);
    }

     public function _unsetNull($arr){
            if($arr !== null){
                if(is_array($arr)){
                    if(!empty($arr)){
                        $arr = json_encode($arr);
                        $arr = json_decode($arr,1);
                        foreach($arr as $key => $value){
                            if($value === null && !is_array($value)){
                                $arr[$key] = '';
                            }else{
                                $arr[$key] = $this->_unsetNull($value);      //递归再去执行
                            }
                        }
                    }else{

                         if($arr === null && !is_array($value)){ $arr = ''; }
                    }
                }else{
                    if($arr === null && !is_array($value)){ $arr = ''; }         //注意三个等号
                }
            }else{ $arr = ''; }
            return $arr;
        }



    protected function checkToken($token,$uid = ''){
        if(!$uid){
            $uid = $this->request->request('uid');
        }
        $return = checkToken($uid,$token);

        if($return == 700){
             $this->error(__('Invalid Token'),700);
             exit();
        }
    }

    /**
     * 该地方只处理获取uid,如果需要获取用户信息,可通过redis的 token值和uid去拿到缓存数据
     * @param $token
     * @return false|mixed
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    protected function getBaseUserInfo($token){
        $token = trim($token);
        $key = 'user_token:'.$token;
        if($uid = getcaches($key)){
            return $uid;
        }
      
        $info = Db::name('user_token')->where('token',$token)->find();
        
        if($info){
            $uid = $info['user_id'];
            setcaches($key,$uid,30*86400);
        }else{
            $uid = false;
        }
        return $uid;
    }
    
    public function setObs($url,$id){
       if(!$url){
           return ;
       }
       $bool  =  Db::name('obs')->where('url',$url)->find();
       if(!$bool){
           $data =  [
               'url' => $url,
               'status' => 0,
               'createtime' => time(),
               'updatetime' => time(),
               'video_id' => $id
            ];
            Db::name('obs')->insert($data);
       }
    }
    

}