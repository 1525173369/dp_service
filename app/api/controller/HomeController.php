<?php

namespace app\api\controller;
use cmf\controller\HomeBaseController;
use think\db;

class HomeController extends HomeBaseController 
{
    
    /**
     * 配置信息
     * @desc 用于获取配置信息
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return array info[0] 配置信息
     * @return object info[0].guide 引导页
	 * @return string info[0].guide.switch 开关，0关1开
	 * @return string info[0].guide.type 类型，0图片1视频
	 * @return string info[0].guide.time 图片时间
	 * @return array  info[0].guide.list
	 * @return string info[0].guide.list[].thumb 图片、视频链接
	 * @return string info[0].guide.list[].href 页面链接
     * @return string msg 提示信息
     */
    public function getConfig() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $info = getConfigPub();
        $info['launchurl']='https://img.zcool.cn/community/01e4a1564da3d76ac7251c94308050.png@1280w_1l_2o_100sh.png';
        $rs['info'][0]['config'] = $info;
        
        return json($rs);
        
        unset($info['site_url']);
        unset($info['site_seo_title']);
        unset($info['site_seo_keywords']);
        unset($info['site_seo_description']);
        unset($info['site_icp']);
        unset($info['site_gwa']);
        unset($info['site_admin_email']);
        unset($info['site_analytics']);
        unset($info['copyright']);
        unset($info['qr_url']);
        unset($info['sina_icon']);
        unset($info['sina_title']);
        unset($info['sina_desc']);
        unset($info['sina_url']);
        unset($info['qq_icon']);
        unset($info['qq_title']);
        unset($info['qq_desc']);
        unset($info['qq_url']);
        unset($info['payment_des']);
        
        $info_pri = getConfigPri();
        
        $list = getLiveClass();
        $videoclasslist = getVideoClass();
        
        $level= getLevelList();
        
        foreach($level as $k=>$v){
            unset($v['level_up']);
            unset($v['addtime']);
            unset($v['id']);
            unset($v['levelname']);
            $level[$k]=$v;
        }
        
        $levelanchor= getLevelAnchorList();
        
        foreach($levelanchor as $k=>$v){
            unset($v['level_up']);
            unset($v['addtime']);
            unset($v['id']);
            unset($v['levelname']);
            $levelanchor[$k]=$v;
        }
        
        $info['liveclass']=$list;
        $info['videoclass']=$videoclasslist;
        
        $info['level']=$level;
        
        $info['levelanchor']=$levelanchor;
        
        // $info['tximgfolder']='';//腾讯云图片存储目录
        // $info['txvideofolder']='';//腾讯云视频存储目录
        // $info['txcloud_appid']='';//腾讯云视频APPID
        // $info['txcloud_region']='';//腾讯云视频地区
        // $info['txcloud_bucket']='';//腾讯云视频存储桶
        // $info['cloudtype']='1';//视频云存储类型
        
		$info['qiniu_domain']=DI()->config->get('app.Qiniu.space_host').'/';//七牛云存储空间地址
        $info['video_audit_switch']=$info_pri['video_audit_switch']; //视频审核是否开启
        
        /* 私信开关 */
        $info['letter_switch']=$info_pri['letter_switch']; //视频审核是否开启
        
        /* 引导页 */
        $domain = new Domain_Guide();
		$guide_info = $domain->getGuide();
        
        $info['guide']=$guide_info;
        
		/** 敏感词集合*/
		$dirtyarr=array();
		if($info_pri['sensitive_words']){
            $dirtyarr=explode(',',$info_pri['sensitive_words']);
        }
		$info['sensitive_words']=$dirtyarr;
		//视频水印图片
        $info['video_watermark']=get_upload_path($info_pri['video_watermark']); //视频审核是否开启
		 
        $info['shopexplain_url']=$info['site']."/portal/page/index?id=38";
        $info['stricker_url']=$info['site']."/portal/page/index?id=39";

        $info['shop_system_name']=$info_pri['shop_system_name']; //系统店铺名称
         
        $rs['info'][0] = $info;

        return $rs;
    }	
    
    
    public function getHot() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $slide = $this->getSlide();
        if($slide) {
            $rs['info'][0]['slide'] = $slide;
            $rs['info'][0]['list'] = array();
        }
        // var_dump($rs);die;
        // $rs['info']['slide'] = $slide;
//         $domain = new Domain_Home();
//         $search = isset($_GET['search']) ? $_GET['search'] : '';
// 		$key1='getSlide';
// 		$slide=getcaches($key1);
// 		if(!$slide){
// 			$slide = $domain->getSlide();
// 			setcaches($key1,$slide);
// 		}

// 		$key2="getHot_".$this->p;
// 		$list=getcaches($key2);
// 		if(!$list){
// 			$list = $domain->getHot($this->p,$search);
// 			setCaches($key2,$list,2); 
// 		}

//         $rs['info'][0]['slide'] = $slide;
        // $rs['info'][0]['list'] = $list;

        return json($rs);
    }
    
    
    /* 轮播 */
	public function getSlide(){
		$info=Db::name('slide_item')
			->where(['status' => '1', 'slide_id' => '1'])
			->order("list_order asc")
			->field('image,url,target')
			->select()
			->toarray();
			
		if($info) {	
    		foreach($info as $k=>$v){
    			$info[$k]['image']=get_upload_path($v['image']);
    		}
    		
    		$info;
		}
        return $info;
	}
}