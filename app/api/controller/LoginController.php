<?php

namespace app\api\controller;
use cmf\controller\HomeBaseController;
use think\db;

//登录
class LoginController extends HomeBaseController 
{

    protected $fields='id,user_nickname,avatar,avatar_thumb,sex,signature,coin,consumption,votestotal,province,city,birthday,user_status,end_bantime,login_type,last_login_time,location,education,professional,area,url1';

    public function appLogin()
    {
        // if ($this->request->isPost()) {
            $rs = array('code' => 0, 'msg' => '登录成功!', 'info' => array());
            $data = $this->request->param();
            $userlogin=checkNull($data['phone']);
            $usercode = checkNull($data["code"]);


            // var_dump(session('?reg_mobile','','login'));
            // var_dump(session('?reg_mobile_code','','login'));
            // var_dump(session('?reg_mobile_expiretime','','login'));

            if (!session('?reg_mobile','','login') || !session('?reg_mobile_expiretime','','login')) {
                $rs['code'] = 1001;
                $rs['msg'] = '请先获取验证码';
                return json($rs);
            }
            if ($userlogin == '') {
                $rs['code'] = 1002;
                $rs['msg'] = '请输入您的手机号';
                return json($rs);
            }
            if ($usercode == '') {
                $rs['code'] = 1002;
                $rs['msg'] = '请输入验证码';
                return json($rs);
            }
            
            if ($userlogin != session('reg_mobile','','login')) {
                $rs['code'] = 1001;
                $rs['msg'] = '手机号码不一致';
                return json($rs);
            }

            if ($usercode != session('reg_mobile_code', '', 'login')) {
                $rs['code'] = 1002;
                $rs['msg'] = '验证码错误';
                return json($rs);
            }
            
            
            $info=Db::name('user')
                ->where(['user_login'=>$userlogin, 'user_type'=>'2'])
                ->find();
                
                // var_dump($info);die;
            // if(!$info || $info['user_pass'] != $user_pass){
            //     $rs['code'] = 1001;
            //     $rs['msg'] = '账号或密码错误';
            //     return $rs;
            // }
            // unset($info['user_pass']);
            
            //无信息直接注册
            if (!$info) {
                $configinfo = getConfigPri();
                $data = array(
                    'user_login' => $userlogin,
                    'user_email' => '',
                    'mobile' => $userlogin,
                    'user_nickname' => 'WEB用户' . substr($userlogin, -4),
                    'user_pass' => '',
                    'signature' => '这家伙很懒，什么都没留下',
                    'avatar' => '/default.jpg',
                    'avatar_thumb' => '/default_thumb.jpg',
                    'last_login_ip' => get_client_ip(),
                    'create_time' => time(),
                    'user_status' => 1,
                    "user_type" => 2,//会员
                );
    
                if ($configinfo['reg_reward'] > 0) {
    
                    $data['coin'] = $configinfo['reg_reward'];
                }
    
                Db::name('user')->insert($data);
                $userid = Db::name('user')->getLastInsID();
                if (!$userid) {
                    $rs['code'] = 1009;
                    $rs['msg'] = '注册失败，请重试';
                    return json($rs);
                }
                
                $info=Db::name('user')
                ->where(['user_login'=>$userlogin, 'user_type'=>'2'])
                ->find();
                
                $info['isreg']='0';
                if($info['last_login_time']==0){
                    $info['isreg']='1';
                }
    
                $info['isagent']='0';
                if($info['isreg']==1){
                    $configinfo=getConfigPri();
                    if($configinfo['agent_switch']==1){
                        $info['isagent']='1';
                    }
                }
                
                if($info['birthday']){
                    $info['birthday']=date('Y-m-d',$info['birthday']);
                }else{
                    $info['birthday']='';
                }
                
                $token=md5(md5($info['id'].$userlogin.time()));

                $info['token']=$token;
                
                $info['avatar']=get_upload_path($info['avatar']);
    
                $info['avatar_thumb']=get_upload_path($info['avatar_thumb']);
    
                $this->updateToken($info['id'],$token);
                
                $rs['info'][0] = $info;
                
                
                return json($rs);
            }
            
            
            
            
            

            if($info['user_status']=='0'){
                $rs['code'] = 1003;
                $rs['msg'] = '该账号已被禁用';
                return $rs;
            }


            // if($info['end_bantime']>time()){
            //     $rs['code'] = 1002;
//                //禁用信息
//                $baninfo=$domain->getUserban($userlogin);
//                $rs['info'][0] =$baninfo;
                // return $rs;
            // }


            if($info['user_status']=='3'){
                $rs['code'] = 1004;
                $rs['msg'] = '该账号已注销';
                return $rs;
            }

            unset($info['user_status']);

            unset($info['end_bantime']);

            $info['isreg']='0';


            if($info['last_login_time']==0){
                $info['isreg']='1';
            }

            $info['isagent']='0';
            if($info['isreg']==1){
                $configinfo=getConfigPri();
                if($configinfo['agent_switch']==1){
                    $info['isagent']='1';
                }
            }



            if($info['birthday']){
                $info['birthday']=date('Y-m-d',$info['birthday']);
            }else{
                $info['birthday']='';
            }

            // $info['level']=getLevel($info['consumption']);
            // $info['level_anchor']=getLevelAnchor($info['votestotal']);

            $token=md5(md5($info['id'].$userlogin.time()));

            $info['token']=$token;
            
            $info['avatar']=get_upload_path($info['avatar']);

            $info['avatar_thumb']=get_upload_path($info['avatar_thumb']);

            $this->updateToken($info['id'],$token);


//            $domain = new Domain_Login();
//            $info = $domain->userLogin($userlogin,$user_pass);
//
//            if($info==1001){
//                $rs['code'] = 1001;
//                $rs['msg'] = '账号或密码错误';
//                return $rs;
//            }else if($info==1002){
//                $rs['code'] = 1002;
//                //禁用信息
//                $baninfo=$domain->getUserban($userlogin);
//                $rs['info'][0] =$baninfo;
//                return $rs;
//            }else if($info==1003){
//                $rs['code'] = 1003;
//                $rs['msg'] = '该账号已被禁用';
//                return $rs;
//            }else if($info==1004){
//                $rs['code'] = 1004;
//                $rs['msg'] = '该账号已注销';
//                return $rs;
//            }

            $rs['info'][0] = $info;

            return json($rs);
        // }


    }

    public function register()
    {
        // if ($this->request->isPost()) {
            $rs = array('code' => 0, 'msg' => '注册成功', 'info' => array());
            $data = $this->request->param();
            // var_dump($data);
            $userlogin = checkNull($data["userlogin"]);
            $usercode = checkNull($data["usercode"]);
            // $userpass = checkNull($data["userpass"]);
            // $userpass2 = checkNull($data["userpass2"]);
            // $agentid = checkNull(input("agentid"));
            // $taskId = checkNull(input("taskId"));

            // if (!isset($_SESSION['reg_mobile']) || !isset($_SESSION['reg_mobile_expiretime'])) {
            if (!session('?reg_mobile') || !session('?reg_mobile_expiretime')) {
                $rs['code'] = 1001;
                $rs['msg'] = '请先获取验证码';
                return json($rs);
            }
            if ($userlogin == '') {
                $rs['code'] = 1002;
                $rs['msg'] = '请输入您的手机号';
                return json($rs);
            }
            if ($usercode == '') {
                $rs['code'] = 1002;
                $rs['msg'] = '请输入验证码';
                return json($rs);
            }
            // if ($userpass == '') {
            //     $rs['code'] = 1002;
            //     $rs['msg'] = '请输入密码';
            //     return json($rs);
            // }
            // if ($userpass2 == '') {
            //     $rs['code'] = 1002;
            //     $rs['msg'] = '请输入确认密码';
            //     return json($rs);
            // }
            if ($userlogin != session('reg_mobile','','login')) {
                $rs['code'] = 1001;
                $rs['msg'] = '手机号码不一致';
                return json($rs);
            }

            if ($usercode != session('reg_mobile_code', '', 'login')) {
                $rs['code'] = 1002;
                $rs['msg'] = '验证码错误';
                return json($rs);
            }

            // $check = passcheck($userpass);

            // if ($check == 0) {
            //     $rs['code'] = 1004;
            //     $rs['msg'] = '密码6-12位数字与字母';
            //     return json($rs);
            // } else if ($check == 2) {
            //     $rs['code'] = 1005;
            //     $rs['msg'] = '密码不能纯数字或纯字母';
            //     return json($rs);
            // }

            // if ($userpass != $userpass2) {
            //     $rs['code'] = 1006;
            //     $rs['msg'] = '密码和确认密码不一致';
            //     return json($rs);
            // }
            $type = '';
            // if (!empty($agentid)) {
            //     $data = db('user')->where("user_activation_key='{$agentid}'")->find();
            //     $agentuid = $data['id'];
            //     if (!$agentuid) {
            //         $codeinfo = Db::name('agent_code')->field("uid")->where(["code" => $agentid])->find();
            //         if (!$codeinfo) {
            //             $this->assign("reason", '邀请码不正确');
            //             $this->display(':error');
            //             exit;
            //         }
            //         $agentuid = $codeinfo['uid'];
            //         $type = 1;
            //     }
            // }
            $ifreg = Db::name('user')->field("id")->where("user_login='{$userlogin}'")->find();
            if ($ifreg) {
                $rs['code'] = 1008;
                $rs['msg'] = '该账号已被注册';
                return json($rs);
            }
            // return $ifreg;
            /* 无信息 进行注册 */
            $configinfo = getConfigPri();
            $data = array(
                'user_login' => $userlogin,
                'user_email' => '',
                'mobile' => $userlogin,
                'user_nickname' => 'WEB用户' . substr($userlogin, -4),
                'user_pass' => cmf_password($userpass),
                'signature' => '这家伙很懒，什么都没留下',
                'avatar' => '/default.jpg',
                'avatar_thumb' => '/default_thumb.jpg',
                'last_login_ip' => get_client_ip(),
                'create_time' => time(),
                'user_status' => 1,
                "user_type" => 2,//会员
            );

            if ($configinfo['reg_reward'] > 0) {

                $data['coin'] = $configinfo['reg_reward'];
            }

            Db::name('user')->insert($data);
            $userid = Db::name('user')->getLastInsID();
            if (!$userid) {
                $rs['code'] = 1009;
                $rs['msg'] = '注册失败，请重试';
                return json($rs);
            }
            if (!empty($taskId) && $type == 1) {
                $result = [
                    'user_id' => $userid,
                    'task_id' => $taskId,
                    'uid' => $agentid,
                    'addtime' => time()
                ];

                Db::name('record')->insert($result);
            }


            return json($rs);
        // }
    }

    /* 手机登录 */
    /* 手机验证码 */
    public function getCode(){


        // echo  I("mobile");exit;

        $rs = array('code' => 0, 'msg' => '发送成功，请注意查收', 'info' => array());

        $limit = ip_limit();
        if( $limit == 1){
            $rs['code']=1003;
            $rs['msg']='您已当日发送次数过多';
            return json($rs);
        }



        $mobile = checkNull(input("mobile"));
        $ismobile=checkMobile($mobile);
        if(!$ismobile){
            $rs['code']=1001;
            $rs['msg']='请输入正确的手机号';
            return json($rs);
        }


        $where="user_login='{$mobile}'";

        // $checkuser = checkUser($where);

        // if($checkuser){
        //     $rs['code']=1004;
        //     $rs['msg']='该手机号已注册，请登录';
        //     $userid=db::name('user')->field('id')->where($where)->find();
        //     $result = [
        //         'user_id'=>$userid['id'],
        //         // 'task_id'=>$_SESSION['taskId'],
        //         // 'uid'=>$_SESSION['agentid'],
        //         'addtime'=>date('Y-m-d H:i:s',time())
        //     ];
        //     // Db::name('record')->insert($result);
        //     return json($rs);
        // }
        
        

        // if (isset($_SESSION['reg_mobile']) && isset($_SESSION['reg_mobile_expiretime'])) {
        if (session('?reg_mobile') && session('?reg_mobile_expiretime')) {

            // if($_SESSION['reg_mobile']==$mobile && $_SESSION['reg_mobile_expiretime']> time() ){
            if (session('reg_mobile') == $mobile && session('reg_mobile_expiretime') > time()) {
                $rs['code']=1002;
                $rs['msg']='验证码5分钟有效，请勿多次发送';
                return json($rs);
            }

        }
        $mobile_code = random(6,1);


        //密码可以使用明文密码或使用32位MD5加密
        $result = sendCode($mobile,$mobile_code);
        // var_dump($result);
        // var_dump($mobile_code);die;

        // echo  $result;exit;


        if($result['code']===0){
            session('reg_mobile', $mobile, 'login');
            session('reg_mobile_code', $mobile_code, 'login');
            session('reg_mobile_expiretime', time() +60*5, 'login');
            // $_SESSION['reg_mobile'] = $mobile;
            // $_SESSION['reg_mobile_code'] = $mobile_code;
            // $_SESSION['reg_mobile_expiretime'] = time() +60*5;
        }else if($result['code']==667){
            session('reg_mobile', $mobile, 'login');
            session('reg_mobile_code', $result['msg'], 'login');
            session('reg_mobile_expiretime', time() +60*5, 'login');
            // $_SESSION['reg_mobile'] = $mobile;
            // $_SESSION['reg_mobile_code'] = $result['msg'];
            // $_SESSION['reg_mobile_expiretime'] = time() +60*5;

            $rs['code']=1002;
            $rs['msg']='验证码为：'.$result['msg'];
        }else{
            $rs['code']=1002;
            $rs['msg']=$result['msg'];
        }
        // var_dump(session('reg_mobile'));
        // var_dump($_SESSION['reg_mobile_expiretime']);
        // var_dump($_SESSION['reg_mobile']);
        // session('?reg_mobile');
        return json($rs);
    }

    /* 更新token 登陆信息 */
    public function updateToken($uid,$token,$data=array()) {
        $nowtime=time();
        $expiretime=$nowtime+60*60*24*300;

        Db::name('user')
            ->where(['id'=>$uid])
            // ->update(array('last_login_time' => $nowtime, "last_login_ip"=>$_SERVER['REMOTE_ADDR'] ));
            ->update(['last_login_time' => $nowtime, "last_login_ip"=>$_SERVER['REMOTE_ADDR']]);

        $isok=Db::name('user_token')
            ->where(['user_id'=>$uid])
            ->update(["token"=>$token, "expire_time"=>$expiretime ,'create_time' => $nowtime]);
        if(!$isok){
            Db::name('user_token')
                ->insert(["user_id"=>$uid,"token"=>$token, "expire_time"=>$expiretime ,'create_time' => $nowtime]);
        }

        $token_info=array(
            'uid'=>$uid,
            'token'=>$token,
            'expire_time'=>$expiretime,
        );

        setcaches("token_".$uid,$token_info);

        return 1;
    }
}