<?php


namespace app\api\controller;



use app\admin\controller\UserController;
use app\api\model\ShopModel;
use app\api\model\ShopTopicModel;
use app\api\model\UserCommentLikesModel;
use app\api\model\UserVideoCommentLikesModel;
use app\api\model\UserVideoVisitsModel;
use app\api\model\UserVideoLike;
use app\api\model\VideoComentsModel;
use app\api\model\CommentsModel;
use app\api\model\UserVideoCollectModel;
use app\api\model\VideoModel;
use app\api\until\ObsUpload;
use Obs\ObsClient;
use PHPMailer\PHPMailer\PHPMailer;
use think\Collection;
use think\Db;
use think\Exception;
use think\facade\App;
use think\File;
use think\Request;

class VideoController extends ApiController
{
    protected $model = VideoModel::class;
    protected $field = ['id','uid','desc','shop_id','draft'];  //搜索数
    protected $sonWhere = [
        'status'=>['=',1],
        'is_delete'=>['=',0]
    ];


    public function setWith($uid){
        $data = [
            'users'=>function($q){
                 $q->field('avatar,user_nickname,id');
             }
        ];
        if($uid){
            $data['usersLikes'] =  function($q)use($uid){
                $q->where('uid',$uid);
            };
        }
        $data['shops'] = function($q){
            $q->field('id,name,claim,thumb,des');
        };
        
        $this->with = $data;
    }
    public function ReclearData($data){
       $data = Collection::make($data)->toArray();
     
       foreach ($data as $k=>$v){
           $topic = $v['topic'];
           $topic = ShopTopicModel::whereIn('topic_id',$topic)->column('topic_name','topic_id');
           unset($data[$k]['topic']);
           $data[$k]['topic'][] = $topic;
           $newTopic = [];
           foreach ($topic as $kt=>$vt){
               $newTopic[] = [
                   'topic_id' => $kt,
                   'topic_name' => $vt
               ];
           }
           $data[$k]['newTopic_ios'] = $newTopic;
           $data[$k]['topic'] = $newTopic;

           if(isset($v['users_likes']) && $v['users_likes']){
               $data[$k]['is_like'] = 1;
           }else{
               $data[$k]['is_like'] = 0;
           }
           $token =  $this->request->request('token');
           if(!$token){
               $user_attention = false;
           }else{
               $uid = $this->getBaseUserInfo($token);
             
               if(!$uid){
                   $user_attention = false;
               }else{
                   $user_attention =VideoModel::userAttention($uid,$v['uid']);
               }
           }
           if(isset($v['shops']) && $v['shops']){
               $data[$k]['shopName'] = $v['shops']['name'];
           }else{
               $data[$k]['shopName'] = '';
           }
           $data[$k]['user_attention'] =  $user_attention;

           //city
           //state
           //area
           //postal_code
           //country
           $data[$k]['lat'] = '30';
           $data[$k]['lng'] = '119';
           $data[$k]['city'] = '';
           $data[$k]['state'] = '';
           $data[$k]['area'] = '';
           $data[$k]['postal_code'] = '';
           $data[$k]['country'] = '';
           $data[$k]['people'] = 1;
           if(isset($uid)){
                $data[$k]['is_collect'] = UserVideoCollectModel::where('uid',$uid)->where('video_id',$v['id'])->count();
           }else{
                $data[$k]['is_collect'] = 0;
           }
          
           $uid = $this->request->request('uid');
           $token = $this->request->request('token');
           $data[$k]['share_url'] = $this->request->domain().'/portal/page/getVideo?id='.$v['id'].'&token='.$token.'&uid='.$uid;
           if(!$v['users']){
               unset($data[$k]);
           }

           //list($data[$k]['download_url']) = $v['video_url'];
       }
       return $data;
    }

    /**
     * showdoc
     * @catalog 视频
     * @title 添加视频
     * @description 添加视频接口
     * @method POST
     * @url {domian}/api/video/
     * @param token 必选 string 用户token
     * @param uid 必选 int 用户id
     * @param title 必选 string 标题
     * @param desc 必选 string 视频描述
     * @param shop_id 必选 int 商家id
     * @param video_url 必选 string 视频地址
     * @param video_img 必选 string 视频封面,当传入空时,服务端自动截取视频的第三s的画面
     * @param topic 可选 int 话题id
     * @param custom 可选 string 用户自定义话题
     * @param address 可选 string 地址
     * @param score 可选 int [1,5]评分,默认为1
     * @return {"ret":200,"msg":"保存成功","data":{"code":0,"info":[],"msg":"保存成功"}}{"ret":200,"msg":"保存失败","data":{"code":-1,"info":[],"msg":"保存失败"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @remark
     * @number 1
     */
    public function post()
    {
        $draft = $this->request->request('draft',0);
         if($draft){
            $this->model->_validate = false;   //取消验证
        }else{
            $score = $this->request->request('score');
            
            if($score > 0){
               
                 $shop_id = $this->request->request('shop_id');
                 $shop = ShopModel::find($shop_id); //获取到店铺
                 $points = round(($shop['points'] + $score)/2);// ($shop['points'] + $data['points']) / 2;
                 $r = ShopModel::where('id',$shop_id)->update(['points'=>$points,['consumption'=>'consumption + 1']]);
            }
        }
        $token = $this->request->request('token');
        $this->checkToken($token);
        $data = $this->request->request();
        file_put_contents('acun_log.log',$data['desc']);
        return parent::post(); // TODO: Change the autogenerated stub
    }
    
    
    public  function IosPost(){
        $draft = $this->request->request('draft',0);
        if($draft){
            $this->model->_validate = false;   //取消验证
        }else{
            $score = $this->request->request('score');
            
            if($score > 0){
               
                 $shop_id = $this->request->request('shop_id');
                 $shop = ShopModel::find($shop_id); //获取到店铺
                 $points = round(($shop['points'] + $score)/2);// ($shop['points'] + $data['points']) / 2;
                
                 $r = ShopModel::where('id',$shop_id)->update(['points'=>$points]);
            }
        }
        $data = $this->request->request();
        file_put_contents('axun_log.log',json_encode($data));
        $token = $this->request->request('token');
        $this->checkToken($token);
        try {
            $request = $this->request->request();
            if($this->model->_validate){
                $validate = $this->model->getValidate();
                if($validate){
                    $check = $validate->check($data);
                }
                if(!$check){
                    return $this->error($validate->getError());
                }
            }


            $data['uid'] = (int)$data['uid'];
            $bool  = $this->model->create($data);
            if($bool){
                if($request['video_url']){
                   $this->setObs($request['video_url'],$bool->id);
                }
                $this->success('保存成功');
            }
            $this->error('保存失败');

        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }

    /**
     * showdoc
     * @catalog 视频
     * @title 获取视频列表
     * @description 获取视频列表
     * @method GET
     * @url {domian}/api/video/
     * @param token 必选 token 用户token
     * @param uid 可选 int 用户id
     * @param id 可选 int 视频id
     * @param desc 可选 string 视频简介
     * @param topic 可选 int 话题id
     * @param shop_id 可选 int 商家id
     * @param custom 可选 int 用户自定义话题
     * @return {"ret":200,"msg":"获取成功","data":{"code":0,"info":[{"id":13,"uid":42071,"score":4,"desc":"wdadadwadad","topic":"539","custom":null,"shop_id":338,"video_img":"https:\/\/sn.askpert.com\/16233115545390.png?e=1623315156&token=-Gvse4W7T0DQ0PNptCArWnsdOgYqJAB4Tme6EZbo:Qpm7gYvSuDnR9vpQgwtAVA2SqfY=","video_url":"https:\/\/sn.askpert.com\/admin\/20210610\/93443a38b1f2074c42c4b06bb2cbb22f.mp4","createtime":1623311181,"updatetime":1623311556,"likes":0,"comments":0,"collects":0,"shares":0,"hot":0,"visits":0,"address":"重庆市渝中区大坪正街","extend":null,"status":1,"is_delete":0},{"id":11,"uid":42071,"score":4,"desc":"dwaddada","topic":"368,901","custom":null,"shop_id":338,"video_img":"admin\/20210610\/af6cb7afd46ec147143d222ed2d33ed6.jpg","video_url":"admin\/20210610\/9b02cd46ebf301cceef0fad438906f4f.mp4","createtime":1623310786,"updatetime":1623310786,"likes":0,"comments":0,"collects":0,"shares":0,"hot":0,"visits":0,"address":"0","extend":null,"status":1,"is_delete":0},{"id":10,"uid":42071,"score":4,"desc":"我的叽歪点击啊降低","topic":"1979,1735","custom":"1979,1735","shop_id":338,"video_img":null,"video_url":"admin\/20210609\/f6f3bb934c7ec664ed63652ed636e31e.mp4","createtime":1623232086,"updatetime":1623232086,"likes":0,"comments":0,"collects":0,"shares":0,"hot":0,"visits":0,"address":"0","extend":null,"status":1,"is_delete":1},{"id":9,"uid":42071,"score":1,"desc":"你好我很高兴人作为我1达瓦","topic":"2432","custom":"2432","shop_id":338,"video_img":"https:\/\/sn.askpert.com\/16233110208531.png?e=1623314621&token=-Gvse4W7T0DQ0PNptCArWnsdOgYqJAB4Tme6EZbo:qgfgizwLVcLjPoXLwwR-Ffbp8iU=","video_url":"https:\/\/sn.askpert.com\/admin\/20210609\/edd4772ed1c72780523e9af8e7d46e83.mp4","createtime":1623231962,"updatetime":1623311021,"likes":50,"comments":20,"collects":40,"shares":10,"hot":50,"visits":10,"address":"重庆市,渝中区,大坪正街","extend":null,"status":1,"is_delete":0},{"id":8,"uid":42071,"score":1,"desc":"你好我很高兴人作为我","topic":"2432","custom":"2432","shop_id":338,"video_img":"https:\/\/sn.askpert.com\/16233116725453.png?e=1623315277&token=-Gvse4W7T0DQ0PNptCArWnsdOgYqJAB4Tme6EZbo:KfsMtiWGG8Cj1TCYDlkf3aXikbo=","video_url":"https:\/\/sn.askpert.com\/admin\/20210609\/edd4772ed1c72780523e9af8e7d46e83.mp4","createtime":1623231953,"updatetime":1623311677,"likes":0,"comments":0,"collects":0,"shares":0,"hot":0,"visits":0,"address":"0","extend":null,"status":1,"is_delete":0}],"msg":"获取成功"}}2.950808sShowPageTrace
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @return_param info[uid] int 用户ID
     * @return_param info[score] int 评分
     * @return_param info[desc] string 视频简介
     * @return_param info[topic] int 视频话题
     * @return_param info[custom] int 视频自定义话题
     * @return_param info[shop_id] int 商家id
     * @return_param info[video_img] int 视频封面地址
     * @return_param info[video_url] int 视频地址
     * @return_param info[likes] int 喜欢人数
     * @return_param info[comments] int 评论人数
     * @return_param info[collects] int 收藏人数
     * @return_param info[shares] int 分享人数
     * @return_param info[hot] int 热度
     * @return_param info[visits] int 访问次数
     * @return_param info[address] string 地址
     * @remark 上面参数可以通过相应的条件去实现筛选,如果需要排序请传入排序的参数order_field->表示要排序的字段,order表示排序的方式两种(asc,desc)分别为正序和倒叙
     * @number 2 
     * 
     */
    public function get()
    {
        $token = $this->request->request('token');
        $uid = $this->getBaseUserInfo($token);
        $to_uid = $this->request->request('to_uid');
        $this->setWith($uid);
        $this->action = 0;
        $topic = $this->request->request('topic');
        if($topic){
             $this->model = $this->model->where('find_in_set('.$topic.',topic)');
        }
        $ids = $this->request->request('ids');
        if($ids){
             $ids = explode(',',$ids);
             $this->model = $this->model->whereIn('id',$ids);
        }
        
        $isblack = Db::name('user_black')
                        ->where(['uid'=>$uid,'type'=>1])
                        ->field('touid')
                        ->select();
                        
        $isblack_ = Db::name('user_black')
                        ->where(['touid'=>$uid,'type'=>1])
                        ->field('uid')
                        ->select();
        if($isblack){
            $arr_ = $this->toStr($isblack);
            $this->model = $this->model->whereNotIn('uid',$arr_);
        }
        
        if($isblack_){
            $arr_ = $this->toStr($isblack_);
            $this->model = $this->model->whereNotIn('uid',$arr_);
        }
        
       
        return parent::get(); // TODO: Change the autogenerated stub
    }
    
    public function toStr($str){
        $result = [];
        array_walk_recursive($str, function($value) use (&$result) {
            array_push($result, $value);
        });
        $isblack_str =implode(',',$result);
        return $isblack_str;
    }

    /**
     * showdoc
     * @catalog 视频
     * @title 更新视频
     * @description 更新视频接口
     * @method PUT
     * @url {domian}/api/video/edit/id/{id}----{id}为视频id
     * @param token 必选 string 用户token
     * @param uid 必选 int 用户id
     * @param desc 可选 string 视频描述
     * @param shop_id 可选 int 商家id
     * @param video_url 可选 string 视频地址
     * @param video_img 可选 string 视频封面,当不传入时,服务端自动截取视频的第三s的画面
     * @return {"ret":200,"msg":"保存成功","data":{"code":0,"info":[],"msg":"保存成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @remark
     * @number 1
     */
    public function edit($id)
    {
        $uid = $this->request->request('uid');
        $info = $this->model->where('id',$id)->find();
        if(!$info || $info['uid'] != $uid){
            $this->error('你没有权限修改该视频');
        }
        if($this->request->request('video_url')){
                   $this->setObs($this->request->request('video_url'),$id);
        }
  
        parent::edit($id); // TODO: Change the autogenerated stub
    }

    /**
     * showdoc
     * @catalog 视频
     * @title 删除视频
     * @description 删除视频接口
     * @method DELETE
     * @url {domian}/api/video/delete/id/{id}----{id}为视频id
     * @param token 必选 string 用户token
     * @param uid 必选 int 用户id
     * @return {"ret":200,"msg":"你没有权限删除","data":{"code":-1,"info":[],"msg":"你没有权限删除"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @remark
     * @number 1
     */
    public function delete($id)
    {
        $uid = $this->request->request('uid');
        $info = $this->model->where('id',$id)->find();

        if(!$info || $info['uid'] != $uid){
            $this->error('你没有权限删除');
        }
        if($info['is_delete']){
            $this->error('该数据已经被删除了');
        }
     
        parent::delete($id); // TODO: Change the autogenerated stub
    }


    /**
     * showdoc
     * @catalog 视频
     * @title 评论列表
     * @description 评论列表接口
     * @method GET
     * @url {domian}/api/video/comments/id/{id}----{id}为视频id
     * @param token 必选 string 用户token
     * @return {"ret":200,"msg":"","data":{"code":0,"info":[{"list":[{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}},{"uid":42071,"content":"你好","createtime":1612571869,"users":{"id":42071,"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"rktt"}}]}],"msg":"获取成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @return_param data[uid] int 评论用户的uid
     * @return_param data[users] obj 用户信息
     * @return_param data[users][avatar] string 有用头像
     * @return_param data[users][user_nickname] string 用户昵称
     * @return_param data[createtime] int 评论时间,10位时间戳
     * @remark
     * @number 1
     */
    public function comments($id){
        try {
            $info = $this->model->checkInfo($id);
            $token = $this->request->request('token');
            $uid = $this->getBaseUserInfo($token);
            if(!$uid){
                $this->error('请登录后再试',700);
            }
            $data = CommentsModel::where('video_id',$info['id'])
                ->with(['users'=>function($q){
                    $q->field('id,avatar,user_nickname');
                }])->field([
                    'id',
                    'uid',
                    'content',
                    'createtime',
                ])
                ->page($this->page,$this->limit)
                ->order($this->orderField,$this->order)
                ->select();
                
             $count = CommentsModel::where('video_id',$info['id'])
                ->with(['users'=>function($q){
                    $q->field('id,avatar,user_nickname');
                }])->field([
                    'id',
                    'uid',
                    'content',
                    'createtime',
                ])
                
                ->order($this->orderField,$this->order)
                ->count();    

            foreach ($data as $k=>$v){
                if($v['uid'] == $info['uid']){
                    $v['is_author'] = 1;
                }else{
                    $v['is_author'] = 0;
                }

                $islike = sIsMember('comment_like:'.$v['id'],$uid);
                if($islike){
                    $v['is_like'] = 1;
                }else{
                    $v['is_like'] = 0;
                }
                $v['user_likes_count'] = intval(sCard('comment_like:'.$v['id']));
                //$v['createtime'] = date('Y-m-d H:i:s',$v['createtime']);

                $data[$k] = $v;
                
                if(!$v['users']){
                    $data[$k]['users'] = [];
                }
            }
            
            $data = [
                ['list'=> $data,'count'=>$count]
            ];
            $this->success('获取成功',$data);
        }catch (\Exception $e){
            $this->error($e->getMessage().$e->getLine());
        }

    }
    /**
     * showdoc
     * @catalog 视频
     * @title 收藏接口
     * @description 收藏接口接口
     * @method POST
     * @url {domian}/api/video/collect/id/{id}----{id}为视频id
     * @param token 必选 string 用户token
     * @return {"ret":200,"msg":"","data":{"code":0,"info":[],"msg":"收藏成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @remark 再次调入时会取消,如果是已经是收藏了,再次调取将是取消
     * @number 1
     */
    public function collect($id){
        try {
            $token = $this->request->request('token');
            $uid = $this->getBaseUserInfo($token);
            if(!$uid){
                $this->error('请登录后再试',700);
            }
            $info = $this->model->checkInfo($id);
          
            $collect = 1;  //1表示收藏 0表示取消收藏
            $is_collect = UserVideoCollectModel::where('uid',$uid)->where('video_id',$id)->count();
            if($is_collect){
                $collect = 0;
            }
            Db::startTrans();
            try {
                if($collect){
                    $trans1 = $this->model->where('id',$id)->setInc('collects',1);
                    $data = [
                        'uid' => $uid,
                        'video_id' => $id
                    ];
                    $trans2 = UserVideoCollectModel::create($data);
                    $msg = '收藏成功';


                }else{
                    $trans1 = $this->model->where('id',$id)->setDec('collects',1);
                    $data = [
                        'uid' => $uid,
                        'video_id' => $id
                    ];
                    $trans2 = UserVideoCollectModel::where($data)->delete();
                    $msg = '取消成功';

                }
                if($trans1 && $trans2){
                    Db::commit();
                  
                    if($msg == '取消成功'){
                        $collects = $info['collects'] - 1;
                    }else{
                        $collects = $info['collects'] + 1;
                         hook('send_push',[
                        'uid'=>$uid,
                        'type'=>2,
                        'to_uid'=>$info['uid'],
                        'video_id' => $id,
                        'send_type' => 0
                    ]);
                    }
                   
                    $this->model->updateRedisInfo($id,['collects'=>$collects]);
                    $data = [];
                    $data[0] = [
                        'collect'=>$collect,
                        'count' => $collects
                        ];
                    $this->success($msg,$data);
                }
                Db::rollback();
                $this->error('服务器繁忙');
            }catch (\Exception $e){
                $this->error($e->getMessage(),$e->getCode());
            }

        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }
    /**
     * showdoc
     * @catalog 视频
     * @title 收藏接口列表
     * @description 收藏列表接口
     * @method GET
     * @url {domian}/api/video/collects/id/{id}----{id}为视频id
     * @param token 必选 string 用户token
     * @return {"ret":200,"msg":"","data":{"code":0,"info":[{"uid":42073,"video_id":12,"createtime":1623469586,"updatetime":1623469586,"users":{"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"手机用户1097","sex":1}},{"uid":42053,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/lh3.googleusercontent.com\/a-\/AOh14GhkT1NlagBkuJg-Xluz7dir-ym20cGHu5eia3JS5A=s96-c","user_nickname":"Choo Jia Yi","sex":2}},{"uid":42054,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"ying","sex":-1}},{"uid":42055,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"peanutbutter","sex":-1}},{"uid":42056,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"K1234567831","sex":-1}},{"uid":42057,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"手机用户2328","sex":2}},{"uid":42058,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/lh3.googleusercontent.com\/a-\/AOh14GiDVGujTJrSd6fFiWSIUUUF_i45ZPblMLrnOFYp=s96-c","user_nickname":"burger _lyn","sex":2}},{"uid":42059,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/lh3.googleusercontent.com\/a-\/AOh14GjyoIlogV8TC_v8LIyOJqskjAk0zRLt3NtgabXf=s96-c","user_nickname":"Wong Hui Lin","sex":2}},{"uid":42060,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/sn.askpert.com\/img_202106091006104421_IOS_.jpg","user_nickname":"hvv","sex":-1}},{"uid":42061,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/lh3.googleusercontent.com\/a-\/AOh14GgAIvawSBmZG_5_20hhZ-lXoGapeSHc9n1u4rdi-g=s96-c","user_nickname":"YJ","sex":2}},{"uid":42062,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"Jia XianYeoh","sex":2}},{"uid":42063,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"Lyn","sex":-1}},{"uid":42064,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/lh3.googleusercontent.com\/a\/AATXAJztWlH52jG6lu19H1ciCALiO4Qh8eGjxExVkXRW=s96-c","user_nickname":"yeoh zisan","sex":2}},{"uid":42065,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/lh3.googleusercontent.com\/a\/AATXAJyrk0vWtQKilFGUMCmacF4PL38ZU3kyeLSJul35=s96-c","user_nickname":"Chew Xiao Ying","sex":2}},{"uid":42066,"video_id":12,"createtime":1623469271,"updatetime":1623469271,"users":{"avatar":"https:\/\/ap1.askpert.com\/default.jpg","user_nickname":"手机用户2820","sex":2}}],"msg":"获取成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @return_param data[info][uid] int 收藏者
     * @return_param data[info][video] int 收藏者
     * @remark
     * @number 1
     */
    public function collects($id){
        try {
           $list =UserVideoCollectModel::where('video_id',$id)->withJoin(['users'])->page($this->page,$this->limit)->order('createtime','desc')->select();
           $list = Collection::make($list)->toArray();
           foreach ($list as $k=>$v){
              $v['users'] = [
                   'avatar' => $v['users']['avatar'],
                   'user_nickname' => $v['users']['user_nickname'],
                   'sex' => $v['users']['sex']
               ];
              $list[$k]['users'] = $v['users'];
           }
           $dxr = [$list];

           $this->success('获取成功',$list);
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }
    
    
    public function  myCollects(){
        $uid = input('uid');
        $user = Db::name('user')->where('id',$uid)->field('user_nickname,avatar')->find();
        $list =UserVideoCollectModel::where('user_video_collect_model.uid',$uid)->withJoin(['videos'])->page($this->page,$this->limit)->order('createtime','desc')->select();
        if(isset($_GET['ios'])){
           $videos = [];
           foreach ($list as $k=>$v){
              
               $videos[$k] = $v['videos'];
               
           }
          
           
           $videos = $this->ReclearData($videos);
           
           foreach ($videos as $k=>$v){
              
               $v['users'] = getUserInfo($v['uid']);
               $v['shops'] = Db::name('shop_apply')->where('id',$v['shop_id'])->find();
               $list[$k] = $v;
              
           }
           $list = ['list'=>$list];
        }
        $dxr = [$list];
        $this->success('获取成功',$dxr);
    }

    /**
     * showdoc
     * @catalog 视频
     * @title 评论接口
     * @description 评论接口
     * @method POST
     * @url {domian}/api/video/comment/id/{id}----{id}为视频id
     * @param token 必选 string 用户token
     * @param content 必选 string 评论内容
     * @param at 可选 int 是否为@人，1表示为@了，0表示未@
     * @param to_uid 可选 int 当上面参数at为1时表示艾特的对象id，用英文逗号隔开
     * @return {"ret":200,"msg":"","data":{"code":0,"info":[],"msg":"评论成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @remark
     * @number 1
     */
    public function comment($id){
        try {
            $token = $this->request->request('token');
            $uid = $this->getBaseUserInfo($token);
            if(!$uid){
                $this->error('请登录后再试',700);
            }
            $info = $this->model->checkInfo($id);
            $content = $this->request->request('content','','trim');
         
            Db::startTrans();
            try {
                $at = $this->request->request('at','','trim');
                $fox = 1;
                $to_uid_str = '';
                if($at){
                    $fox = 0;
                    $to_uid = $this->request->request('to_uid','','trim');
                    $to_uid = explode(',',$to_uid); //艾特的用户
                    $to_uid =  array_unique($to_uid);
                    $to_uid_str = implode(',',$to_uid);
                }
                $data = [
                    'uid' => $uid,
                    'content' => $content,
                    'video_id' => $info['id'],
                    'to_uid' => $info['uid'],
                    'at'  => $to_uid_str
                ];
                $validate = (new VideoComentsModel)->getValidate();
                $check = $validate->check($data);
                if(!$check){
                    return $this->error($validate->getError());
                }
                $trans1 = VideoComentsModel::create($data);
                $trans2 = VideoModel::where('id',$id)->setInc('comments',1);
                if($trans2 && $trans1){
                    Db::commit();
                  
                    hook('send_push',[
                        'uid'=>$uid,
                        'type'=>1,
                        'to_uid'=>$info['uid'],
                        'video_id' => $info['id'],
                        'send_type' => 2
                    ]);
                   
                    if($fox == 0){
                        foreach ($to_uid as $k=>$v){
                            hook('send_push',[
                                'uid'=>$uid,
                                'type'=>4,
                                'to_uid'=>$v,
                                'video_id' => $info['id'],
                                'send_type' => 2,
                                'is_at' => 1
                            ]);
                        }
                    }
                    $this->model->updateRedisInfo($id,['comments'=>$info['comments']+1]);
                    $data = [
                      'count' => $info['comments']+1
                    ];
                    $dxr[] = $data;
                    $this->success('评论成功',$dxr);
                }
                Db::rollback();
                $this->error('服务器繁忙');
            }catch (\Exception $e){
                $this->error($e->getMessage().'--line:'.$e->getLine());
            }
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }
    /**
     * showdoc
     * @catalog 视频
     * @title 喜欢列表接口
     * @description 喜欢列表接口接口
     * @method GET
     * @url {domian}/api/video/likes/id/{id}----{id}为视频id
     * @param token 必选 string 用户token
     * @return {"ret":200,"msg":"","data":{"code":0,"info":[{"uid":1,"video_id":13,"createtime":156798500,"updatetime":null,"users":{"id":1,"avatar":"","user_nickname":"admin"}}],"msg":"获取成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @remark
     * @number 1
     */
    public function likes($id){
        try {
            $token = $this->request->request('token');
            $uid = $this->getBaseUserInfo($token);
            if(!$uid){
                $this->error('请登录后再试',700);
            }
            $info = $this->model->checkInfo($id);

            $list = UserVideoLike::where('video_id',$info['id'])->withJoin('users')->page($this->page,$this->limit)->order($this->orderField,$this->order)->select();
            $list = Collection::make($list)->toArray();
            foreach ($list as $k=>$v){
                $list[$k]['users'] = [
                    'id' => $v['users']['id'],
                    'avatar'=> $v['users']['avatar'],
                    'user_nickname' => $v['users']['user_nickname']
                ];
            }
            $this->success('获取成功',$list);
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }
    /**
     * showdoc
     * @catalog 视频
     * @title 喜欢接口
     * @description 喜欢接口接口
     * @method GET
     * @url {domian}/api/video/like/id/{id}----{id}为视频id
     * @param token 必选 string 用户token
     * @return {"ret":200,"msg":"","data":{"code":0,"info":{"like":1},"msg":"喜欢成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @return_param data[info] obj like为1时表示已喜欢成功0表示已取消
     * @remark
     * @number 1
     */
    public function like($id){
        try {
            $token = $this->request->request('token');
            $uid = $this->getBaseUserInfo($token);
            if(!$uid){
                $this->error('请登录后再试',700);
            }
            $info = $this->model->checkInfo($id);
           // $likes = $info['likes'];
            $bool = UserVideoLike::where('uid',$uid)->where('video_id',$id)->count();
            $likes = UserVideoLike::where('video_id',$id)->count();
            
            if($bool){  //取消
               $trans2 = VideoModel::where('id',$id)->update(['likes'=>$likes - 1]);
               $bool =  UserVideoLike::where('video_id',$id)->where('uid',$uid)->delete();
               $msg = '取消成功';
               $data = [
                   'like' => 0
               ];
                $this->model->updateRedisInfo($id,['likes'=> $likes-1]);  //更新缓存
               
                $likes = $likes-1;
            }else{   //喜欢
                $trans2 = VideoModel::where('id',$id)->update(['likes'=>$likes + 1]);
                $bool =  UserVideoLike::create([
                    'uid' => $uid,
                    'video_id' => $id
                ]);
              
                hook('send_push',[
                    'uid'=>$uid,
                    'type'=>5,
                    'to_uid'=>$info['uid'],
                    'video_id' => $id,
                    'send_type' => 1
                ]);
               $data = [
                   'like' => 1
               ];
               $msg = '喜欢成功';
               $this->model->updateRedisInfo($id,['likes'=> $likes+1]);
               $likes = $likes+1;
            }

            $data['count'] = $likes;
            $dxr[] = $data;
            if($bool){
                $this->success($msg,$dxr);
            }
            $this->error($msg);
        }catch (\Exception $e){
            $this->error($e->getMessage().'行数为'.$e->getLine());
        }
    }
    /**
     * showdoc
     * @catalog 视频
     * @title 回复评论
     * @description 回复评论接口
     * @method POST
     * @url {domian}/api/video/ReplyComment/id/{id}----{id}为评论id
     * @param token 必选 string 用户token
     * @param content 必选 string 回复内容
     * @return {"ret":200,"msg":"","data":{"code":0,"info":[],"msg":"评论成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @remark
     * @number 1
     */
    public function ReplyComment($id){
        try {
            $token = $this->request->request('token');
            $uid = $this->getBaseUserInfo($token);
            if(!$uid){
                $this->error('请登录后再试',700);
            }
            $comment = VideoComentsModel::details($id);
            if(!$comment){
                $this->error('不存在该评论');
            }
            $content = $this->request->request('content','','trim');
     
            $content = str_replace('re:','',$content);
           
            if(!$content){
                $this->error('评论内容不能为空');
            }
            $fox = 1;
            $at = $this->request->request('at','','trim');
            $dsp = [];
            $to_uid_str = '';
            if($at){
               $fox = 0;
               $to_uid = $this->request->request('to_uid','','trim');
               $to_uid = explode(',',$to_uid);
               $to_uid =  array_unique($to_uid);
               $dsp = $to_uid;
               $to_uid_str = implode(',',$to_uid);
            }
            $data = [
                'uid' => $uid,
                'content' => $content,
                'video_id' => $comment['video_id'],
                'to_uid' => $comment['uid'],
                'at' => $to_uid_str,
                'send_type' => 2
            ];

            $bool = VideoComentsModel::create($data);
            if($bool){
                hook('send_push',[
                    'uid'=>$uid,
                    'type'=>4,
                    'to_uid'=>$comment['uid'],
                    'video_id' => $comment['video_id'],
                    'send_type' => 2
                ]);
                foreach ($dsp as $k=>$v){
                    hook('send_push',[
                        'uid'=>$comment['uid'],
                        'type'=>4,
                        'to_uid'=>$v,
                        'video_id' => $comment['video_id'],
                        'send_type' => 2,
                        'is_at' => 1
                    ]);
                }
                $data = [];
                $info = $this->model->checkInfo($comment['video_id']);
                $this->model->updateRedisInfo($info['id'],['comments'=>$info['comments']+1]);
                $this->success('评论成功',$data);
            }
           
            $this->error('服务器繁忙');
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }
    
    
    
   
    

    /**
     * showdoc
     * @catalog 视频
     * @title 点赞评论
     * @description 点赞评论接口
     * @method GET
     * @url {domian}/api/video/LikeComment/id/{id}----{id}为评论id
     * @param token 必选 string 用户token
     * @return {"ret":200,"msg":"","data":{"code":0,"info":{"is_like":0},"msg":"取消成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @return_param data[info][is_like] int 为1时表示点赞成功,0时为取消成功
     * @remark
     * @number 1
     */
    public function LikeComment($id){
        try {
            $token = $this->request->request('token');
            $uid = $this->getBaseUserInfo($token);
            if(!$uid){
                $this->error('请登录后再试',700);
            }
            $comment = VideoComentsModel::details($id);
            if(!$comment){
                $this->error('不存在该评论');
            }
            $count = UserVideoCommentLikesModel::where('uid',$uid)->where('comment_id',$id)->count();
            if($count){
                $bool  = UserVideoCommentLikesModel::where('uid',$uid)->where('comment_id',$id)->delete();
                $msg = '取消成功';
                $data = [
                    'is_like' => 0
                ];
            }else{
                $data = [
                    'uid' => $uid,
                    'comment_id' => $id
                ];
                $bool = UserVideoCommentLikesModel::create($data);
                $msg = '点赞成功';
                $data = [
                    'is_like' => 1
                ];
                hook('send_push',[
                    'to_uid'=>$comment['uid'],
                    'type'=>3,
                    'uid'=>$uid,
                    'video_id' => $comment['video_id'],
                    'send_type' => 1
                ]);
            }
            if($bool){
                if($data['is_like'] == 1){
                    sAdd('comment_like:'.$id,$uid); //如集合
                }else{
                    sRem('comment_like:'.$id,$uid);  //去除集合
                }
                $data['count'] = sCard('comment_like:'.$id);
                $dxr[] = $data;
                $this->success($msg,$dxr);
            }
            $this->error('服务器繁忙');
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }

    /**
     * 测试华为云工具(目前是ios身份认证的接口传入地方)
     */
    public function upload(){
        $obs = new ObsUpload();
        $file = $_FILES['file']['tmp_name'];
        $isadmin  = $this->request->request('is_merch',0);
        $file = new File($file);
        try {
            $resp = $obs->upload($file,$isadmin);
            $data = [
               'url' =>$resp    
            ];
        }catch (\Exception $e){
             $this->error($e->getMessage());
        }
        $sys = $this->request->request('sys',0);
        if($sys){
            $dxr[] = $data;
            $this->success('上传成功',$dxr);
        }else{
            $this->success('上传成功',$data);
        }

    }

     /* showdoc
     * @catalog 视频
     * @title 分享通知接口
     * @description 分享通知接口
     * @method GET
     * @url {domian}/api/video/share/id/{id}----{id}为视频id
     * @param token 必选 string 用户token
     * @return {"ret":200,"msg":"","data":{"code":0,"info":{"count":11},"msg":"取消成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @return_param data[info][count] int 为分享次数
     * @remark
     * @number 1
     */
    public function share($id){

        $token = $this->request->request('token');
        $uid = $this->getBaseUserInfo($token);
        if(!$uid){
            $this->error('请登录后再试',700);
        }
        $info = $this->model->where('id',$id)->find();
        if(!$info){
            $this->error('不存在该视频内容');
        }
        if(!$info['status']){
            $this->error('该视频已经下架');
        }
        if($info['is_delete']){
            $this->error('该视频已经被删除了');
        }
        $this->model->where('id',$id)->setInc('shares',1);
        $this->model->deletRedisCache($id);
        $data['count'] = ++$info['shares'] ;
        $dxr[] = $data;
        $this->success($dxr);

    }

    /**
     * 商家相关视频
     * @param $shop_id
     * @param $p
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     */
    public function  getVideoByShopId($shop_id,$p){
        if($p<1){
            $p=1;
        }
        $nums=10;
        $start=($p-1)*$nums;
        $res = Db::name('video')->where(['shop_id'=>$shop_id,'is_delete'=>0,'status'=>1])->limit($start,$nums)->select()->toArray();
        if($res){

            $rs['ret'] = 200;
            $rs['data']['code'] = 0;
            $rs['data']['msg'] = '获取成功';
            $rs['data']['info'] = $res;
            echo json_encode($rs);
            exit;

        }else{

            $rs['ret'] = 200;
            $rs['data']['code'] = 1;
            $rs['data']['msg'] = '获取失败';
            $rs['data']['info'] = $res;
            echo json_encode($rs);
            exit;

        }
    }
    /**
    * showdoc
    * @catalog 视频
    * @title 访问统计
    * @description 访问统计接口
    * @method GET
    * @url {domian}/api/video/addVisit/id/{id}----{id}为视频id
    * @param token 必选 string 用户token
    * @param uid 必选 int id
    * @return {"ret":200,"msg":"","data":{}}
    * @return_param ret int http状态,可忽略
    * @return_param data[code]  int 状态码,0表示成功,其他表示失败
    * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
    * @remark
    * @number 1
    */
    public function addVisit($id){
        $token = $this->request->request('token');
        $uid = $this->getBaseUserInfo($token);
        if(!$uid){
            $this->error('请登录后再试',700);
        }
        $info = $this->model->checkInfo($id);
        
        if(!$info){
             $this->error();
        }
        // $this->model->updateRedisInfo($id,['collects'=>$collects]);
        $ret =  UserVideoVisitsModel::where('uid',$uid)->where('video_id',$id)->count();
        if($ret){
            $this->success();
        }
        $ret = $this->model->where('id',$id)->setInc('visits',1);
        UserVideoVisitsModel::create(['uid'=>$uid,'video_id'=>$id]);
      
        $c = intval($info['visits'])+1;
      
        $dxr = ['visits'=>$c];
      
        $this->model->updateRedisInfo($id,$dxr);
        $this->success();
    }


    public function shareApp(){
        $data = [
            'Messenger' => 1,
            'WechatApp' => 1,
            'CopyLink' => 1,
            'FaceBook' => 1,
            'Instagram' => 1,
            'Message' => 1
        ];
        $dxr[0] = $data;
        $this->success('获取成功',$dxr);
    }
  /**
   * showdoc
   * @catalog 视频
   * @title 视频举报
   * @description 视频举报接口
   * @method GET
   * @url {domian}/api/video/eport/id/{id}----{id}为视频id
   * @param token 必选 string 用户token
   * @param uid 必选 int id
   * @param content 必选 string 举报标题
   * @param description 必选 text 举报内容
   * @param thumb 必选 string 举报图片,多图用英文逗号隔开
   * @return {"ret":200,"msg":"","data":{}}
   * @return_param ret int http状态,可忽略
   * @return_param data[code]  int 状态码,0表示成功,其他表示失败
   * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
   * @remark
   * @number 1
   */
    public function eport($id){
        try {
           
            $token = $this->request->request('token');
            $uid = $this->getBaseUserInfo($token);
            if(!$uid){
                $this->error('请登录后再试',700);
            }
            $keys = $uid.$id;
            if(!lock($keys)){
                $this->error('系统正在处理请稍后');
            }
            $uid = $this->request->request('uid');
            $content = $this->request->request('content');  //标题
            $description = $this->request->request('description');   //举报内容
            $thumb = $this->request->request('thumb');   //举报内容
            $info = $this->model->checkInfo($id);
            $data = [
                'uid' => $uid,
                'touid' => $info['uid'],
                'content' => $description,   //相反
                'description' => $content,
                'thumb' => $thumb,
                'report_id' => $info['id'],
                'type' => 2,
                'status' => 0,
                'add_time' => time()
            ];
          
            $rs = Db::name('report')->insert($data);
            if(!$rs){
                throw new Exception('服务器繁忙');
            }
          
            $this->success('举报成功');
        }catch (\Exception $e){
            unlock($keys);
            $this->error($e->getMessage());
        }


    }







}