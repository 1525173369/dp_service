<?php

namespace app\api\controller;
use cmf\controller\HomeBaseController;
use think\db;

//商家店铺
class ShopController extends HomeBaseController 
{
    
    /**
	 * 申请店铺
	 * @desc 用于申请店铺
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string msg 提示信息
	 */
	public function shopApply(){
		$rs = array('code' => 0, 'msg' => '店铺申请成功', 'info' => array());
		$data = $this->request->param();
		
		$uid=checkNull($data['uid']);
        $token=checkNull($data['token']);
        $username=checkNull($data['username']);//联系人姓名
        $cardno=checkNull($data['cardno']);//身份证号码
        $classid=checkNull($data['classid']);// 经营类目
        $contact=checkNull($data['contact']); //经营者联系人
        $country_code=checkNull($data['country_code']);//国家代号
        $phone=checkNull($data['phone']);//经营者手机号
        $province=checkNull($data['province']);//所在省份
        $city=checkNull($data['city']);//所在城市
        $area=checkNull($data['area']);//所在地区
        $address=checkNull($data['address']);//详细地址
        $service_phone=checkNull($data['service_phone']);//客服电话
        $certificate=checkNull($data['certificate']);//上传营业执照
        $other=checkNull($data['other']);//上传其他证件
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return json($rs);
		}

        
        
		//判断用户是否实名认证
		$isauth=isAuth($uid);
		if(!$isauth){
			$rs['code']=1001;
        	$rs['msg']="请先进行实名认证";
        	return json($rs);
		}

        if(!$username){
        	$rs['code']=1001;
        	$rs['msg']="请填写姓名";
        	return json($rs);
        }

        // if(mb_strlen($username)>4){
        // 	$rs['code']=1001;
        // 	$rs['msg']="姓名长度不能超过4个字";
        // 	return $rs;
        // }

        if(!$cardno){
        	$rs['code']=1001;
        	$rs['msg']="请填写身份证号";
        	return json($rs);
        }

        // if(!isCreditNo($cardno)){
        // 	$rs['code']=1001;
        // 	$rs['msg']="身份证号不合法";
        // 	return json($rs);
        // }

        if(!$classid){
        	$rs['code']=1001;
        	$rs['msg']="请选择经营类型";
        	return json($rs);
        }

		if(!$contact){
			$rs['code']=1001;
        	$rs['msg']="请填写经营者联系人";
        	return json($rs);
		}

// 		if(mb_strlen($contact)>4){
// 			$rs['code']=1001;
//         	$rs['msg']="经营者联系人不能超过4个字";
//         	return $rs;
// 		}

		if(!$phone){
			$rs['code']=1001;
        	$rs['msg']="请填写经营者手机号";
        	return json($rs);
		}

// 		if(!checkMobile($phone)){
// 			$rs['code']=1001;
//         	$rs['msg']="手机号码错误";
//         	return $rs;
// 		}

		if(!$province){
			$rs['code']=1001;
        	$rs['msg']="请选择所在省份";
        	return json($rs);
		}

		if(!$city){
			$rs['code']=1001;
        	$rs['msg']="请选择所在市";
        	return json($rs);
		}

		if(!$area){
			$rs['code']=1001;
        	$rs['msg']="请选择所在地区";
        	return json($rs);
		}

		if(!$address){
			$rs['code']=1001;
        	$rs['msg']="请填写详细地址";
        	return json($rs);
		}

		if(mb_strlen($address)>50){
			$rs['code']=1001;
        	$rs['msg']="详细地址必须在50字以内";
        	return json($rs);
		}

		if($service_phone){
// 			$checkmobile=checkMobile($service_phone);
// 			if(!$checkmobile){
// 				$rs['code']=1001;
// 	        	$rs['msg']="客服电话错误";
// 	        	return $rs;
// 			}
		}else{
			$service_phone=$phone;
		}

		if(!$certificate){
			$rs['code']=1001;
			$rs['msg']="请上传营业执照";
			return json($rs);
		}

		if(!$other){
			$rs['code']=1001;
			$rs['msg']="请上传其他证件";
			return json($rs);
		}

		//判断保证金是否缴纳
		$bond_res=Db::name('shop_bond')->where(['uid' => $uid])->find();
//         if(!$bond_res || $bond_res['status']==0 || $bond_res['status']==-1){//没有缴纳保证金
//             $rs['code']=1001;
// 			$rs['msg']="请缴纳保证金";
// 			return json($rs);
        // }

		//判断店铺审核状态
        $apply_info=Db::name('shop_apply')
                ->where(['uid' => $uid])
                ->find();

        if($apply_info){
            $apply_info['certificate_format']=get_upload_path($info['certificate']); //营业执照
            $apply_info['other_format']=get_upload_path($info['other']); //其他证件
    
            if($apply_info['status']==0){ //审核中
                $rs['code']=1001;
    			$rs['msg']="店铺审核中,请耐心等待";
    			return json($rs);
            }else if($apply_info['status']==1){ //审核通过
                $rs['code']=1001;
    			$rs['msg']="店铺已审核通过";
    			return json($rs);
            }
        } else {
            $apply_info['status']=-1;
        }
        
		$data=array(
			'username'=>$username,
	        'cardno'=>$cardno,
	        'contact'=>$contact,
	        'country_code'=>$country_code,
	        'phone'=>$phone,
	        'province'=>$province,
	        'city'=>$city,
	        'area'=>$area,
	        'address'=>$address,
	        'service_phone'=>$service_phone,
	       // 'receiver'=>$receiver,
	       // 'receiver_phone'=>$receiver_phone,
	       // 'receiver_province'=>$receiver_province,
	       // 'receiver_city'=>$receiver_city,
	       // 'receiver_area'=>$receiver_area,
	       // 'receiver_address'=>$receiver_address,
	        'certificate'=>$certificate,
	        'other'=>$other,
	        'status'=>1
	        

		);
		if($apply_info['status']==-1){ //无审核记录
			$data['uid']=$uid;
			$data['addtime']=time();
		}

		if($apply_info['status']==2){ //被拒绝
			
			$data['uptime']=time();
		}

		$configpri=getConfigPri();
		$shop_switch=$configpri['show_switch'];
		$shoporder_percent=$configpri['shoporder_percent'];

		$data['order_percent']=isset($shoporder_percent)?$shoporder_percent:0; //订单抽成比例

		if($shop_switch){
			$data['status']=0;
		}
        
        if($apply_info['status']==-1){ //无申请记录
            $res=Db::name('shop_apply')->insert($data);
            return json($rs);
        }

        if($apply_info['status']==2){
            $res=Db::name('shop_apply')->where("uid={$uid}")->update($data);
            return json($rs);
        }

        if(!isset($res)){
            $rs['code']=1002;
			$rs['msg']='店铺审核提交失败';
			return json($rs);
        }

        // if($apply_info['status']=1){

        //     //写入店铺总评分记录
        //     $data1=array(
        //         'shop_uid'=>$uid
        //     );

        //     Db::name('shop_points')->insert($data1);
        // }
        
		return json($rs);

	}
	
	
	/**
	 *获取保证金
	 *@desc 用于获取保证金设置数和用户是否缴纳保证金
	 *@return int code 状态码，0表示成功
	 *@return array info 状态码，0表示成功
	 *@return array info[0].shop_bond 后台设置的保证金金额
	 *@return array info[0].bond_status 用户是否缴纳保证金
	 *@return string msg 提示信息
	*/
	public function getBond(){
		$rs = array('code' => 0, 'msg' => '', 'info' => array());

        $data = $this->request->param();
		$uid=checkNull($data['uid']);
        $token=checkNull($data['token']);

//         $checkToken=checkToken($uid,$token);
// 		if($checkToken==700){
// 			$rs['code'] = $checkToken;
// 			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
// 			return $rs;
// 		}

		$configpri=getConfigPri();
		$shop_bond=$configpri['shop_bond'];
		$rs['info'][0]['shop_bond']=$shop_bond;

        $info=Db::name('shop_bond')->where(['uid' => $uid])->find();
        if(!$info){//没有缴纳保证金
            $rs['info']['bond_status']='0';
        }

        if($info['status']==0){//保证金已退回
            $rs['info']['bond_status']='0';
        }

        if($info['status']==2){//保证金已缴纳/已处理
            $rs['info']['bond_status']='1';
        }

		return json($rs);
	}
    
}
