<?php


namespace app\api\controller;


use app\api\model\ScannelOrderModel;
use app\api\model\ShopModel;
use app\api\model\ShopLogModel;
use app\api\model\UsersModel;
use Think\Db;

class ScannelController extends ApiController
{
    protected $model = ScannelOrderModel::class;
    protected $field = ['uid','shop_uid'];
    //扫码付款
    public function complete(){
         #$this->error('你不能给自己商铺扫码');
        $token = $this->request->request('token','','trim');
        $value = $this->request->request('value','','trim');
        $shopid = $this->request->request('shopid','','trim');
        $remark = $this->request->request('remark','','trim');
        if(!$token || !$value || !$shopid){
            $this->error('参数不能为空');
        }
        $uid = $this->getBaseUserInfo($token);  //获取uid
   
        $userInfo = UsersModel::where('id',$uid)->find();
        
        if($userInfo['coin'] < $value){
            $this->error('金币不足无法操作');
        }
        $shop = Db::name('shop_apply')->where('id',$shopid)->where('status',1)->find();
        if(!$shop){
            $this->error('该商铺无权限');
        }
       
        if($shop['uid']  == $uid){
            $this->error('你不能给自己商铺扫码');
        }
      
        Db::startTrans();
        try {
            $trans1 = UsersModel::where('id',$uid)->setDec('coin',$value);
            #$trans2 = UsersModel::where('id',$shop['uid'])->setInc('coin',$value);
            $ordersn = 'ASKPERT'.date('YmdHis').mt_rand(1000,9999).$uid;
            $trans5 = ShopLogModel::addlog($shopid,$value,'扫码收益',1,$ordersn);
            if($shop['uid']){
                $trans2 = UsersModel::where('id',$shop['uid'])->setInc('coin',$value);
            }else{
                $trans2 = ShopModel::where('id',$shopid)->setInc('coin',$value);
            }
            
            
           
            $data = [
                'uid' => $uid,
                'shop_uid' => $shop['uid'],
                'money' => $value,
                'shopid' => $shopid,
                'remarks' => $remark,
                'type' => 0,
                'ordersn' => $ordersn
            ];
            $trans3 = $this->model->create($data);
            $data = [
                'uid' => $uid,
                'sid' => 0,
                'type' => 0,
                'title' => '扫码支付金币',
                'shopname' => $shop['name'],
                'shopid' => $shopid,
                'createtime' => time(),
                'money' => $value,
                'ordersn' => $ordersn
            ];
            $trans4 =  Db::name('user_log')->insert($data);
            $action = '提现支出';
           
            if($trans1 && $trans2 && $trans3 &&  $trans4){
                Db::commit();
                $this->success('付款成功',$ordersn);
            }
            $this->error('付款失败');
        }catch (\Exception $e){
            $this->error($e->getMessage());
        }
    }
    public function  getShop(){
        $id =  input('id');
        $shop = Db::name('shop_apply')->where('id',$id)->where('status',1)->find();
        $this->success('获取成功',$shop);
    }
    /**
     * showdoc
     * @catalog 扫码
     * @title 扫码订单列表
     * @description 扫码订单列表接口
     * @method GET
     * @url {domian}/api/scannel/
     * @param token 必选 string 用户token
     * @param uid 可选 int 付款人uid,筛选字段
     * @param shop_uid 可选 int 收钱人的uid,筛选字段
     * @return {"ret":200,"msg":"","data":{"code":0,"info":[],"msg":"评论成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @remark 如果需要查询扫码订单,可以根据以上备注走!!!,
     * @number 1
     */
     
    public function editBank(){
        $id =  input('id');
        $uid =  input('uid');
        
        $userbankinfo = Db::name('scannel_cash_bank')->where('id',$id)->where('uid',$uid)->find();
        $this->success('获取成功',$userbankinfo);
         
     }
     
    public function editPost(){
        
        
        $id =  input('id');
        $data['bank_name'] =  input('bank_name');
        $data['bank_account'] =  input('bank_account');
        $data['bank_username'] =  input('bank_username');
        $data['bank_cardid'] =  input('bank_cardid');
        $uid =  input('uid');
        $data['is_default'] =  input('is_default');
        $data['updatetime'] =  time();
        $data['mobile'] =  input('mobile');
        
        try{
            
            $res = Db::name('scannel_cash_bank')->where('id',$id)->where('uid',$uid)->update($data);
            if($res){
                
                $this->success('修改成功',$res);    
                
            }else{
                $this->success('修改失败',$res);
                
            }    
            
        }catch(\Exception $e){
            $this->error($e->getMessage());
            
        }  
        
        
        
    }
    //转账
    public function transfer(){
        $uid =  input('uid');//用户ID
        $touid =  input('touid');//用户ID，接收转账
        $money = input('money');//转账金额
        $remark = input('remark');
        
        //查询用户当前的余额
        $userInfo = Db::name('user')->where('id',$uid)->find();
        //查询被转用户状态是否正常
        $touserinfo = Db::name('user')->where('id',$touid)/*->where('user_status','=',1)*/->find();
        if($userInfo['coin'] < $money){
            $this->error('金币不足无法操作');
        }
        if(!$touserinfo){
            $this->error('该用户不存在');
        }
        
        if($touserinfo['user_status'] != 1){
            $this->error('该用户未进行身份认证');
        }
        
        $coin = $touserinfo['coin'] + $money;
        $coins = $userInfo['coin'] - $money;
        $data = [
                'coin' => $coin
            ];
            
        $datas = [
                'coin' => $coins
            ];
            
        try{
            
            $res = Db::name('user')->where('id',$touid)->update($data);
            
            $result = Db::name('user')->where('id',$uid)->update($datas);
            
                            
            $ordersn = 'TS'.date('YmdHis').mt_rand(1000,9999).$uid;
            $data = [
                'uid' => $uid,
                'touid' => $touid,
                'money' => $money,
                'type' => 1,
                'remarks' => $remark,
                'ordersn' => $ordersn,
                'createtime' => time()
            ];
            
            $trans3 = $this->model->create($data);
                
                //添加转账记录
            $data = [
                'uid' => $uid,
                'sid' => 0,
                'type' => 0,
                'title' => '转账',
                'ordersn' => $ordersn,
                'createtime' => time(),
                'money' => $money
            ];
            $trans4 =  Db::name('user_log')->insert($data);
            
            
            $data = [
                'uid' => $touid,
                'sid' => 0,
                'type' => 1,
                'title' => '转账收益',
                'ordersn' => $ordersn,
                'createtime' => time(),
                'money' => $money
            ];
            $trans5 =  Db::name('user_log')->insert($data);
            
            
            if($result && $res && $trans3 && $trans4){
                $this->success('转账成功',[]);
            }else{
                $this->error('转帐失败',[]);
                
            }
                        
        }catch(\Exception $e){
            $this->error($e->getMessage());
            
        } 


    }
    
    //交易列表
    
    public function payList(){
        $uid =  input('uid');//用户ID
        $token =  input('token');
        //查询用户的所有支付记录
        $userpaylog = Db::name('scannel_order')->where('uid',$uid)->select()->toArray();
        foreach($userpaylog as $key=>$value){
            if($value['shop_uid']){
                $shopinfo = Db::name('shop_apply')->where('id',$value['shopid'])->field('uid,name')->find();
                $userpaylog[$key]['shopname'] = $shopinfo['name'];
  
            }else{
                
                    $userinfo = Db::name('user')->where('id',$value['shop_uid'])->find('id,user_nickname');
                    $userpaylog[$key]['shopname']=$userinfo['user_nickname']; 

                
            }
        }
        $this->success('获取成功',$userpaylog);
        
    }
    
    
    //交易细节
    
    public function transactionDetails(){
        
        $ordersn = $this->request->request('ordersn','','trim');
        $ordersninfo = Db::name('scannel_order')->where('ordersn',$ordersn)->find();
       
        // $ordersninfo['createtime'] = date('Y-m-d H:i:s',$ordersninfo['createtime']);
        $this->success('获取成功',$ordersninfo);
        
        
    }
    
    //通过token获取用户Uid
    public function getUidByToken(){
        $token = $this->request->request('token','','trim');
        $uid = $this->getBaseUserInfo($token);  //获取uid
        $shopinfo = Db::name('shop_apply')->where('uid',$uid)->find();
        if($shopinfo){
            return $shopinfo['id'];
        }
        return -1;
        
    }
    
     
     

}