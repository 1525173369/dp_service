<?php


namespace app\api\controller;

use think\Collection;
use app\api\model\CashBankModel;
use app\api\model\ScannelCashModel;
use app\api\model\UsersModel;
use app\api\model\ShopModel;
use Think\Db;
use think\Exception;

class CashController  extends ApiController
{
    protected $field = ['uid'];
    protected $model = ScannelCashModel::class;
    
    
    public function ReclearData($data){
        $data = Collection::make($data)->toArray();
        foreach ($data as $k=>$v){
            $data[$k]['createtime'] = date('Y-m-d H:i:s',$v['createtime']);   
        }
        return $data;
    }

    /**
     * showdoc
     * @catalog 用户提现
     * @title 提现列表
     * @description 用户提现列表
     * @method GET
     * @url {domian}/api/cash
     * @param uid 必选 string 用户id
     * @return {"ret":200,"msg":"","data":{"code":0,"info":[{"list":[{"id":39,"uid":42072,"shopid":338,"money":"100.00","bank_name":"招商银行","bank_account":"51162119804445","bank_username":"张三","bank_cardid":"123123123123","status":"<span style=\"color: grey\">申请中<\/span>","createtime":1625043030,"updatetime":1625043030,"ordersn":"TX202106301650301877","mobile":"17624562332","coin":"10000.00","rate":"0.01","handle_time":0},{"id":38,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: green\">成功<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":1625039291},{"id":37,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: green\">成功<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":1625039457},{"id":36,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: red\">失败<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":1625040244},{"id":35,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: red\">失败<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":1625039811},{"id":34,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: red\">失败<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":1625040268},{"id":33,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: red\">失败<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":1625040291},{"id":32,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: grey\">申请中<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":0},{"id":31,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: grey\">申请中<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":0},{"id":30,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: grey\">申请中<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":0},{"id":29,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: grey\">申请中<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":0},{"id":28,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: grey\">申请中<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":0},{"id":27,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: grey\">申请中<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":0},{"id":26,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: grey\">申请中<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":0},{"id":25,"uid":40921,"shopid":0,"money":"759.00","bank_name":"招商银行","bank_account":"1545456465","bank_username":"招商","bank_cardid":"511621199809135652","status":"<span style=\"color: grey\">申请中<\/span>","createtime":1625033951,"updatetime":1625033951,"ordersn":"","mobile":"17602341097","coin":"75900.00","rate":"0.01","handle_time":0}]}],"msg":"获取成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @return_param bank_account string 银行卡账号
     * @return_param bank_cardid string 银行卡所属用户身份证号码
     * @return_param bank_name string 银行名称
     * @return_param bank_username string 银行用户名
     * @return_param mobile string 银行用户名电话
     * @return_param coin float 提现金币
     * @return_param createtime int 提现提交时间
     * @return_param handle_time int 管理员处理时间
     * @return_param money float 对比后的钱
     * @return_param ordersn string 提现单号
     * @return_param rate float 金币和钱之间的比例
     * @return_param shopid int 如果大于0在表示商家提现,为0表示用户提现
     * @return_param status string 需要app自行匹配
     * @remark
     * @number 1
     */



    /**
     * showdoc
     * @catalog 用户提现
     * @title 用户提现
     * @description 用户提现接口
     * @method GET/POST
     * @url {domian}/api/cash/userCash
     * @param token 必选 string 用户token
     * @param cash_bank_id 必选 int 银行卡的账户id,根据cash_bank获取
     * @param cash_coin 必选 int 提现的金币
     * @return {"ret":200,"msg":"","data":{"code":0,"info":[{"list":[{"id":1,"bank_name":"招商银行","bank_account":"145151455","bank_username":"卢本伟","bank_cardid":"5116215454545","uid":40292,"is_default":1,"createtime":1569798500,"updatetime":1569798500}]}],"msg":"获取成功"}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @remark
     * @number 1
     */
    public function userCash(){
        try {
            $data = $_REQUEST;
            file_put_contents('axu.log',json_encode($data));
            $token = $this->request->request('token');
            $cash_bank_id = $this->request->request('cash_bank_id',0);
            $uid = $this->getBaseUserInfo($token);
           
            $cash_coin = $this->request->request('cash_coin');
            if(!$uid){
                throw new Exception('请登录后再试');
            }

            $CashBank = CashBankModel::where('id',$cash_bank_id)->where('uid',$uid)->find();
          
            if(!$CashBank){
                $this->error('不存在该银行卡');
            }
            $config =  getConfigPub();
            $user_least_coin  = $config['user_least_coin'];
            $user = UsersModel::where('id',$uid)->field('coin')->find();
            
            $auth_info = Db::name('user_auth')->where('uid',$uid)->where('status',1)->find();
            if(!$auth_info){
                 $this->error('请实名认证后提现');
            }
            if($auth_info['real_name'] != $CashBank['bank_username'] || !$auth_info['cer_no'] != $auth_info['bank_cardid']){
                
               # $this->error('身份证信息和实名不一致');
            }
            
            $coin = $user['coin'];
            if($cash_coin > $coin){
                throw new Exception('你的金币不足');
            }
            $coin = $cash_coin;
            if($coin <  $user_least_coin){
                throw new Exception('当前金币不满足提现要求');
            }
            $key  = $uid.'user_cash';
            if(!lock($key)){
                throw new Exception('请勿重复提交');
            }
            $shop = ShopModel::where('uid',$uid)->find();
            if($shop){
                $shopid = $shop['id'];
            }else{
                $shopid = 0;
            }
            Db::startTrans();
            $trans1 = UsersModel::where('id',$uid)->setDec('coin',$coin);
            $rate = $config['user_cash_rate']; //用户提现比例
            list($a,$b) = explode(':',$rate);
            
            $rate = number_format($b/$a,4);
            $money = round($coin *  $b/$a,2);
            $data = [
                'ordersn' => 'TX'.date('YmdHis').mt_rand(1000,9999),
                'uid' => $uid,
                'shopid' => $shopid,
                'money' => $money,
                'bank_name' => $CashBank['bank_name'],
                'bank_account' => $CashBank['bank_account'],
                'bank_cardid' => $CashBank['bank_cardid'],
                'mobile' => $CashBank['mobile'],
                'coin' => $coin,
                'rate' => $rate,
                'bank_username' => $CashBank['bank_username']
            ];
            $trans2  = ScannelCashModel::create($data);
            unlock($key);
            if($trans1  &&  $trans2){
                Db::commit();
                $this->success('申请成功');
            }

            Db::rollback();
            $this->error('服务器繁忙');
        }catch (\Exception $e){
            if(isset($key)){
                unlock($key);
            }
            $this->error($e->getMessage());
        }


    }



}