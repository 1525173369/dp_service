<?php


namespace app\api\controller;
use cmf\controller\HomeBaseController;
use think\db;

use app\admin\controller\UserController;
use app\api\model\ShopModel;
use app\api\model\ShopTopicModel;
use app\api\model\UserCommentLikesModel;
use app\api\model\UserVideoCommentLikesModel;
use app\api\model\UserVideoLike;
use app\api\model\VideoComentsModel;
use app\api\model\CommentsModel;
use app\api\model\UserVideoCollectModel;
use app\api\model\VideoModel;
use app\api\until\ObsUpload;
use Obs\ObsClient;
use think\Collection;
use think\Exception;
use think\facade\App;
use think\File;
use think\Request;

//商家相关的帖子
class DynamicController extends HomeBaseController
{
    public function getListByShop($shop_id,$p)
    {
        $data = $this->request->param();
        $uid=isset($data['uid']) ? $data['uid']: '';
        $token=isset($data['token']) ? $data['token']: '';
        $uid=(int)checkNull($uid);
        $token=checkNull($token);
        // $checkToken=checkToken($uid,$token);
        // if(!$uid){
        //     $this->error('请登录后再试',700);
        // }
	    if($p<1){
            $p=1;
        }
    	$nums=10;
    	$start=($p-1)*$nums;
        $res = Db::name('dynamic')->where(['shop_id'=>$shop_id,'isdel'=>0,'status'=>1])->limit($start,$nums)->select()->toArray();
        if($res){

            $rs['ret'] = 200;
            $rs['data']['code'] = 0;
            $rs['data']['msg'] = '获取成功';
            $rs['data']['info'] = $res;
            echo json_encode($rs);
            exit;

        }else{

            $rs['ret'] = 200;
            $rs['data']['code'] = 1;
            $rs['data']['msg'] = '获取失败';
            $rs['data']['info'] = $res;
            echo json_encode($rs);
            exit;

        }
        
    }

    
}
