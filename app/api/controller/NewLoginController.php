<?php


namespace app\api\controller;


use app\api\model\UsersModel;
use app\api\until\SendEmail;
use Think\Db;
use Think\Exception;

class NewLoginController extends ApiController
{
    protected $model = UsersModel::class;

      /**
       * showdoc
       * @catalog 三期/登录
       * @title 发送验证码
       * @description 发送验证码接口
       * @method GET/POST
       * @url {domian}/api/new_login/mobileCode
       * @param mobile 必选 string 手机号码,中间不能有特殊符号,如果是+8617602341097换成8617602341097
       * @return {"ret":200,"msg":"","data":{}}
       * @return_param ret int http状态,可忽略
       * @return_param data[code]  int 状态码,0表示成功,667表示测试环境,手机验证码会返回在msg里面,其他状态码都为错误了
       * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息,
       * @remark
       * @number 1
       */
    public  function mobileCode(){
        $mobile = $this->request->request('mobile','','trim');
        if(!$mobile){
            $this->error('手机号不能为空');
        }
        $bool =  $this->model->where('user_login',$mobile)->count();
        if($bool){
            $this->error('该手机号已经注册');
        }
        $code  = mt_rand(1000,9999);
        $result = sendCode($mobile,$code);
        if($result['code']==0){
            setcaches('Mobilecode:'.$mobile,$code,300);
            $this->success('发送成功');
        }else if($result['code']==667){
            if(isset($_GET['ios'])){
                $code = 0;
            }else{
                $code = 667;
            }
            setcaches('Mobilecode:'.$mobile,$result['msg'],300);
            $msg='验证码为：'.$result['msg'];
            $this->success($msg,[],$code);
        }else{
            $msg=$result['msg']?$result['msg']:'发送失败,未知错误';
            $this->error($msg);
        }
    }
     /**
      * showdoc
      * @catalog 三期/登录
      * @title 验证码确认
      * @description 验证码确认接口
      * @method GET/POST
      * @url {domian}/api/new_login/checkMobileCode
      * @param mobile 必选 string 手机号码,中间不能有特殊符号,如果是+8617602341097换成8617602341097
      * @param code 必选 string 验证码
      * @return {"ret":200,"msg":"","data":{}}
      * @return_param ret int http状态,可忽略
      * @return_param data[code]  int 状态码,0表示成功,其他状态码都为错误了
      * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息,
      * @remark
      * @number 1
      */
    public function checkMobileCode(){
        $mobile = $this->request->request('mobile');
        $code = $this->request->request('code');
        $key  = 'Mobilecode:'.$mobile;
        $value = getcaches($key);
        if($value != $code){
            $this->error('验证码错误');
        }
        delcache($key);
        $this->success('验证成功');
    }

    /**
     * showdoc
     * @catalog 三期/登录
     * @title 邮箱验证确认
     * @description 邮箱验证确认
     * @method GET/POST
     * @url {domian}/api/new_login/confirmNameAndEmail
     * @param email 必选 string 邮箱地址
     * @param name 必选 string 姓名
     * @param mobile 必选 string 电话号码
     * @return {"ret":200,"msg":"邮箱验证发送成功","data":{}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他状态码都为错误了
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息,
     * @return_param data[data][uniq_key] string 邮箱验证密钥,通过该id可查看该验证是否完成,也是最终登录键的auth凭证
     * @remark
     * @number 1
     */
    public function confirmNameAndEmail(){
        $email = $this->request->request('email'); //确认的邮箱
        $name = $this->request->request('name');  //确认的名称
        $mobile = $this->request->request('mobile'); //电话号码
        $EmailUntil = new SendEmail();
        $uniq_key = uniqid('askpert',true);
        $href =  $this->request->domain().'/api/new_login/emailOk/key/'.$uniq_key;
        // $content = "<a href='".$href."'>点击验证</a>";  //发送的html内容
       
        $content = <<<EOT


  <!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <div style="padding: 10px;">
        <div class="verify-title" style="width: 207px; margin: 10px auto 20px;">
            <img style="width: 100%; height: 100%;" src="https://static-ap.askpert.com/9e63c5864d1d7564feb65dc27a59b6f8.jpg">
        </div>
       
            <div style="font-size: 16px; margin-bottom: 20px;">Thank you for signing up!</div>
            <div style="font-size: 16px; margin-bottom: 20px;">Please click the button below to verify your email and activate your account.</div>
            <button style="border: 2px solid #437199; margin-bottom: 20px;margin-left: 25%; font-size: 16px; padding: 10px 20px; background-color: #5b9bd5; color: #fff;"><a href="{$href}" style="color:white" >Verify email now</a></button>
            <div style="font-size: 16px; margin-bottom: 20px;">Please note this link will expire within 24 hours.</div>
      
    </div>
</body>
</html>
        
EOT;



        
        
        try{
             $return = $EmailUntil->setBody($content)->sendEmail($email);
        }catch(\Exception $e){
             $this->error('邮箱错误');
        }
       
        if($return){
            $data = [
                'name' => $name,
                'email' => $email,
                'mobile' => $mobile,
                'time' => time()
            ];
            setcaches($uniq_key,$data,300); //设置缓存
            $dxr[] =  ['uniq_key'=>$uniq_key];
            $this->success('请求验证发送成功',$dxr);
        }
        $error_msg  = $EmailUntil->error_msg;
        $this->error($error_msg);
    }

    //邮箱验证回调地址 和上方confirm方法是一个
    public function emailOk($key){
        try {
            $value = getcaches($key);
            if(!$value){
                throw new \Exception('不存在该验证码信息');
            }
            //delcache($key);
            setcaches($key.'_ok',$value,600); //重新设置成功
            echo <<<EOT
Verification successful
EOT;

        }catch (\Exception $e){
            echo <<<EOT
不存在该验证码
EOT;
        }


    }
    /**
     * showdoc
     * @catalog 三期/登录
     * @title 验证码验证
     * @description 验证码验证接口
     * @method GET/POST
     * @url {domian}/api/new_login/emailVerify
     * @param uniq_key 必选 string 验证ID
     * @return {"ret":200,"msg":"验证成功","data":{}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他状态码都为错误了
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息,
     * @remark 切记,一定要验证成功后才能使其登录
     * @number 1
     */
    public function emailVerify(){
        $uniq_key = $this->request->request('uniq_key');
        $key =  $uniq_key.'_ok';
        $value = getcaches($key);
        if($value){
            $this->success('验证成功');
        }
        $this->error('验证失败');
    }
    /**
     * showdoc
     * @catalog 三期/登录
     * @title 注册
     * @description 验证码确认接口
     * @method GET/POST
     * @url {domian}/api/new_login/login
     * @param uniq_key 必选 string 验证ID
     * @param lan 必选 string 语言
     * @param as_id 必选 string 客户端as_id
     * @param device_id 必选 string 设备ID
     * @param device 必选 string 设备类型IOS,ANDROID
     * @return {"ret":200,"msg":"验证成功","data":{}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他状态码都为错误了
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息,
     * @remark 切记,一定要验证成功后才能使其登录
     * @number 1
     */
    public function login(){
        $uniq_key = $this->request->request('uniq_key'); //唯一键登录auth
        $lan = $this->request->request('lan','ms'); //唯一键登录auth
        $as_id = $this->request->request('as_id'); //as_id
        $device_id = $this->request->request('device_id'); //设备ID
        $device = $this->request->request('device','IOS'); //设备类型
        $value = getcaches($uniq_key.'_ok');
        
        if(!$value){
            $this->error('该注册凭据已经过期,请重新获取');
        }
        $mobile = $value['mobile'];
        $email = $value['email'];
        $name = $value['name'];
        //完成注册所有验证时的时间,目前按照这个时间作为用户注册时间
        $start_register = $value['time'];
        $configpri = getConfigPri();
        //注册奖励
        $reg_reward = $configpri['reg_reward'];
        $data = array(
            'user_login' => $mobile,
            'mobile' => $mobile,
            'user_nickname' => $name,
            'user_email' => $email,
            'user_pass' => '',
            'signature' => '这家伙很懒，什么都没留下',
            'avatar' => '/default.jpg',
            'avatar_thumb' => '/default_thumb.jpg',
            'last_login_ip' => $_SERVER['REMOTE_ADDR'],
            'create_time' => time(),
            'user_status' => 2,
            "user_type" => 2,//会员
            "coin" => 0,
            "code" => createCode(),
            "lang" => $lan
        );
        $exisit = UsersModel::where('user_login',$mobile)->count();
        if($exisit){
            $this->error('该用户已经注册');
        }
        $ret = UsersModel::create($data);
        if(!$ret){
            $this->error('服务器繁忙');
        }
        $uid = $ret['id'];

        $data = [
            'uid' => $ret['id'],
            'device_id' => $device_id,
            'as_id' => $as_id,
            'device_type' => $device,
            'add_time' => time()
        ];
        Db::name('user_devices')->insert($data);
        if ($reg_reward > 0) {
            addMoney($ret['id'], [
                'title' => '注册奖励',
                'money' => $reg_reward,
            ]);
        }
        $info = UsersModel::where('id',$ret['id'])->find();

        //unset($info['user_status']);

        unset($info['end_bantime']);

        $info['isreg'] = '0';


        if ($info['last_login_time'] == 0) {
            $info['isreg'] = '1';
        }



        if ($info['birthday']) {
            $info['birthday'] = date('Y-m-d', $info['birthday']);
        } else {
            $info['birthday'] = '';
        }


        $token = md5(md5($info['id'] . $mobile . time()));

        $info['token'] = $token;
        $info['avatar'] = get_upload_path($info['avatar']);
        $info['avatar_thumb'] = get_upload_path($info['avatar_thumb']);

        $this->updateToken($info['id'], $token);
        $dxr[] = $info;
       
        if($device == 'IOS'){
            $dxr[0]['id'] = (string) $dxr[0]['id'];
            $dxr[0]['user_status'] = (string) $dxr[0]['user_status'];
            $dxr[0]['coin'] = (string) $dxr[0]['coin'];
            $this->success('注册成功',$dxr);
        }else{
             $this->success('注册成功',$info);
        }
       

    }
     /**
     * showdoc
     * @catalog 三期/登录
     * @title 三方平台登录
     * @description 三方平台登录
     * @method GET/POST
     * @url {domian}/api/new_login/ThirdLogin
     * @param type 必选 int 平台ID,1谷歌,2华为,3facebook,4苹果
     * @param openid 必选 string 获取到的唯一id 
     * @return {"ret":200,"msg":"验证成功","data":{}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他状态码都为错误了
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息,
     * @remark 切记,一定要验证成功后才能使其登录
     * @number 1
     */
    public function  ThirdLogin(){
        $type = input('type'); //1 谷歌 2华为 3facebook 4苹果
        $openid = input('openid');
        $isIos =  input('ios',0);
        if($type == 1){
            $info = UsersModel::where('openid',$openid)->find();
        }
        if($type == 2){
            $info = UsersModel::where('huawei_openid',$openid)->find();
        }
        if($type == 3){
            $info = UsersModel::where('facebook_openid',$openid)->find();
        }
        if($type == 4){
            $info = UsersModel::where('apple_openid',$openid)->find();
        }
        if(!$info){
            $this->error('注册绑定后才能使用');
        }
        

       // unset($info['user_status']);

        unset($info['end_bantime']);

        $info['isreg'] = '0';


        if ($info['last_login_time'] == 0) {
            $info['isreg'] = '1';
        }



        if ($info['birthday']) {
            $info['birthday'] = date('Y-m-d', $info['birthday']);
        } else {
            $info['birthday'] = '';
        }
        $mobile = $info['mobile'];
        $token = md5(md5($info['id'] . $mobile . time()));

        $info['token'] = $token;
        $info['avatar'] = get_upload_path($info['avatar']);
        $info['avatar_thumb'] = get_upload_path($info['avatar_thumb']);

        $this->updateToken($info['id'], $token);
        $dxr[] = $info;
        
         if($isIos){
            $dxr[0]['id'] = (string) $dxr[0]['id'];
            $dxr[0]['user_status'] = (string) $dxr[0]['user_status'];
            $dxr[0]['coin'] = (string) $dxr[0]['coin'];
            $this->success('登录成功',$dxr);
        }else{
             $this->success('登录成功',$info);
        }
        
      
        
    }
     /**
     * showdoc
     * @catalog 三期/登录
     * @title 三方平台绑定
     * @description 三方平台绑定
     * @method GET/POST
     * @url {domian}/api/new_login/ThirdLogin
     * @param type 必选 int 平台ID,1谷歌,2华为,3facebook,4苹果
     * @param openid 必选 string 获取到的唯一id 
     * @param uid 必选 int 用户的ID 
     * @return {"ret":200,"msg":"验证成功","data":{}}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他状态码都为错误了
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息,
     * @remark 切记,一定要验证成功后才能使其登录
     * @number 1
     */
    
    public function bindAccount(){
        $uid = input('uid');
        $openid = input('openid');
        $type = input('type'); //1 谷歌 2华为 3facebook 4苹果
        $field = 'openid';
        if($type == 1){
            $info = UsersModel::where('openid',$openid)->find();
        }
        if($type == 2){
            $info = UsersModel::where('huawei_openid',$openid)->find();
            $field = 'huawei_openid';
        }
        if($type == 3){
            $info = UsersModel::where('facebook_openid',$openid)->find();
            $field = 'facebook_openid';
        }
        if($type == 4){
            $info = UsersModel::where('apple_openid',$openid)->find();
            $field = 'apple_openid';
        }
        if($info && $info['id'] == $uid){
            $info->$field = '';
            $info->save();
            $this->success('解除绑定成功');
        }else{
              
            if($info){
                $this->error('该账号已经绑定了');
            }
            $info = UsersModel::where('id',$uid)->find();
            
            $info->$field = $openid;
            $info->save();
            $this->success('绑定成功');
        }
      
    }
    //第三方登录
    public function Thirdlogins(){
         $type = input('type'); //1 谷歌 2华为 3facebook 4苹果
         $openid = input('openid');
         if($type == 1){
            $info = UsersModel::where('openid',$openid)->find();
        }
        if($type == 2){
            $info = UsersModel::where('huawei_openid',$openid)->find();
            $field = 'huawei_openid';
        }
        if($type == 3){
            $info = UsersModel::where('facebook_openid',$openid)->find();
            $field = 'facebook_openid';
        }
        if($type == 4){
            $info = UsersModel::where('openid',$openid)->find();
        }
        if(!$info){
            $this->error('该账号未绑定,请绑定后登录');
        }
        
        
      

        //unset($info['user_status']);

        unset($info['end_bantime']);

        $info['isreg'] = '0';


        if ($info['last_login_time'] == 0) {
            $info['isreg'] = '1';
        }



        if ($info['birthday']) {
            $info['birthday'] = date('Y-m-d', $info['birthday']);
        } else {
            $info['birthday'] = '';
        }


        $token = md5(md5($info['id'] . $mobile . time()));

        $info['token'] = $token;
        $info['avatar'] = get_upload_path($info['avatar']);
        $info['avatar_thumb'] = get_upload_path($info['avatar_thumb']);

        $this->updateToken($info['id'], $token);
        $dxr[] = $info;
        $this->success('注册成功',$info);
        
        
    }
    
    
    
    public function updateToken($uid, $token, $data = array())
    {
        $nowtime = time();
        $expiretime = $nowtime + 60 * 60 * 24 * 300;
        Db::name('user')
            ->where('id', $uid)
            ->update(array('last_login_time' => $nowtime, "last_login_ip" => $_SERVER['REMOTE_ADDR']));

        $isok = Db::name('user_token')
            ->where('user_id', $uid)
            ->update(array("token" => $token, "expire_time" => $expiretime, 'create_time' => $nowtime));
        if (!$isok) {
            Db::name('user_token')
                ->insert(array("user_id" => $uid, "token" => $token, "expire_time" => $expiretime, 'create_time' => $nowtime,));
        }

        $token_info = array(
            'uid' => $uid,
            'token' => $token,
            'expire_time' => $expiretime,
        );

        setcaches("token_" . $uid, $token_info);

        return 1;
    }







}