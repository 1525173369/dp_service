<?php
/**
 * 个人主页
 */
namespace app\appapi\controller;

use cmf\controller\HomeBaseController;
use think\Db;

class HomeController extends HomebaseController {
	
	function index(){       
        $touid = $this->request->param('touid', 0, 'intval');
        
        if(!$touid){
            $this->assign("reason",'信息错误');
			return $this->fetch(':error');
        }

		$info=getUserInfo($touid);	

        if(!$info){
            $this->assign("reason",'信息错误');
			return $this->fetch(':error');
        }        

		$info['follows']=NumberFormat(getFollownums($touid));
		$info['fans']=NumberFormat(getFansnums($touid));
        
        $this->assign('info',$info);

		/* 贡献榜前三 */

		$contribute=Db::name("user_coinrecord")
				->field("uid,sum(totalcoin) as total")
				->where(["action"=>'1' , "touid"=>$touid])
				->group("uid")
				->order("total desc")
				->limit(0,3)
				->select();
		foreach($contribute as $k=>$v){
			$userinfo=getUserInfo($v['uid']);
			$v['avatar']=$userinfo['avatar'];
			$contribute[$k]=$v;
		}		

        $this->assign('contribute',$contribute);
		
        /* 视频数 */
        $info['videonums']='0';
        /* 直播数 */
        $livenums=Db::name("live_record")
					->where(["uid"=>$touid])
					->count();

        $this->assign('livenums',$livenums);
		/* 直播记录 */
		$record=array();
		$record=Db::name("live_record")
					->field("id,uid,nums,starttime,endtime,title,city")
					->where(["uid"=>$touid])
					->order("id desc")
					->limit(0,20)
					->select()
                    ->toArray();
		foreach($record as $k=>$v){
            if($v['title']==''){
                $record[$k]['title']='无标题';
            }
			$record[$k]['datestarttime']=date("Y.m.d",$v['starttime']);
			$record[$k]['dateendtime']=date("Y.m.d",$v['endtime']);
            $cha=$v['endtime']-$v['starttime'];
            $record[$k]['length']=getSeconds($cha);
		}			

        $this->assign('liverecord',$record);
        
        
        /* 标签 */

        $label=getMyLabel($touid);
        
        $labels=array_slice($label,0,3);
        
		$this->assign('labels',$labels);

		
		return $this->fetch();
	    
	}

	public function getHtml(){
        $id = $this->request->request('id');
        $type =  $this->request->request('type'); //gift  about
        switch ($type){
            case 'gift':
                $this->printGift($id);die;
            case 'about':
                $this->printAbout($id);die;
            case 'dynamic':
                $this->printAbout($id,$type);die;

        }
    }

    public function printAbout($id,$type = 'about'){

        if($type == 'dynamic'){
            $res = Db::name('dynamic')->where('id',$id)->find();
            $content = $res['content'];
        }else{
            $res = Db::name('agreement')->where("id=2")->find();
            $content = $res['content'];
        }
        $content =  htmlspecialchars_decode($content,1);
       
        echo <<<EOT
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>


</style>
<html>
<head>
</head>
<body>
<script src="https://cdn.staticfile.org/vue/2.2.2/vue.min.js"></script>
<div id="app">
<div style="width: 100%" v-html="content"></div>

</div>
<script type="text/javascript">
   

    var vm = new Vue({
        el: '#app',
        data: {
           content:''
        },
        
        created() {
            var content =  '$content'
            content= content.replace(/style="[^"]*?"/g,'');
    
            content= content.replace(/height="[^"]*?"/g,'');
            
            content= content.replace(/width="[^"]*?"/g,'');
            
            content= content.replace(/img/g,'img width="100%" ');
            this.content = content;
        }
        
    
       
       
    })
</script>
</body>


</html>
EOT;

    }
    public function printGift($id){
	   $goods =  Db::name('shop_goods')->where('id',$id)->find();
	   //$content =  htmlspecialchars_decode($goods['goods_content'],1);
	   $content = $goods['goods_content'];
	   $content = str_replace('\'','\\',$content);
	   $content = str_replace("'","\'",$content);
	   $content = json_encode($content);   //字符格式将php转为json格式
      
	   echo <<<EOT
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>


</style>
<html>
<head>
</head>
<body>
<script src="https://cdn.staticfile.org/vue/2.2.2/vue.min.js"></script>
<div id="app">
<p style="width: 100%" v-html="content"></p>

</div>
<script type="text/javascript">
   

    var vm = new Vue({
        el: '#app',
        data: {
           content:''
        },
        
        created() {
            var content =  $content
            console.log(content);
            content= content.replace(/style="[^"]*?"/g,'');
    
            content= content.replace(/height="[^"]*?"/g,'');
            
            content= content.replace(/width="[^"]*?"/g,'');
            
            content= content.replace(/img/g,'img width="100%" ');
            this.content = this.escape2Html(content);
        },
        
        methods:{
            escape2Html(str){
                var arrEntities = { 'lt': '<', 'gt': '>', 'nbsp': ' ', 'amp': '&', 'quot': '"' };
            	return str.replace(/&(lt|gt|nbsp|amp|quot);/ig, function (all, t) {
            		return arrEntities[t];
            	})

            }
        }
        
        

       
       
    })
</script>
</body>


</html>
EOT;

    }

}