<?php

namespace app\portal\controller;
use app\api\model\VideoModel;
use cmf\controller\HomeBaseController;
use app\portal\service\PostService;
use app\portal\model\DynamicModel;
use think\Db;
class PageController extends HomebaseController{
	public function index() {
        
        $postService = new PostService();
        $pageId      = $this->request->param('id', 0, 'intval');
        $page        = $postService->publishedPage($pageId);

        if (empty($page)) {
            abort(404, ' 页面不存在!');
        }

        $this->assign('page', $page);
		
		return $this->fetch();
	}

    /**
     * 帖子
     */
	public function getDynamic(){
        $id  = $this->request->param('id');
        $info = DynamicModel::where('id',$id)->find();

        $img_thumb = array();
        $video_thumb_prefix = "https://ap1.askpert.com/ffmpeg/";

        if(isset($info['thumb']) && !empty($info['thumb'])) {
            foreach($info['thumb'] as $it) {
                if(strpos($it, "https://sn.askpert.com") !== FALSE) {
                    $img_thumb[] = $it."?imageView2/2/w/800/h/800";
                }else
                    if(strpos($it, "https://askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443") !== FALSE)
                    {
                        $img_thumb[] = str_replace("https://askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443", "https://static-ap.askpert.com", $it)."?x-image-process=style/style-400";
                    }else
                        if(strpos($it, "https://static-ap.askpert.com") !== FALSE) {
                            $img_thumb[] = $it."?x-image-process=style/style-400";
                        }else
                        {
                            $img_thumb[] = $it;
                        }
            }
        }

        if(isset($info['video_img']) && !empty($info['video_img'])) {
            $img_urls = explode(",", $info['video_img']);
            if($img_urls && is_array($img_urls)) {
                foreach($img_urls as $it) {
                    if(strpos($it, "http://") === FALSE &&
                        strpos($it, "https://") === FALSE) {
                        $img_thumb[] = $video_thumb_prefix.$it;
                    }else
                        if(strpos($it, "https://askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443") !== FALSE)
                        {
                            $img_thumb[] = str_replace("https://askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443", "https://static-ap.askpert.com", $it)."?x-image-process=style/style-600";
                        }else
                            if(strpos($it, "https://static-ap.askpert.com")  !== FALSE) {
                                $img_thumb[] = $it."?x-image-process=style/style-600";
                            }else
                            {
                                $img_thumb[] = $it;
                            }
                }
            }
        }

        $videos = explode(",", $info['video']);
        $videos_url = [];

        if(isset($videos) && count($videos) > 0) {
            foreach($videos as $v) {
                if(strpos($v, "https://askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443") === FALSE) {
                    $videos_url[] = $v;
                }else
                {
                    $videos_url[] = str_replace("https://askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443", "https://static-ap.askpert.com", $v);
                }
            }
        }

        $this->assign('thumb',$img_thumb);
        $this->assign('videos', $videos_url);
        $this->assign('info', $info);
        $this->assign('url', "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
        return $this->fetch();
    }
    public function getVideo(){
        $id  = $this->request->param('id');
        $info = VideoModel::where('id',$id)->find();

        $img_thumb = array();

        if(isset($info['video_img']) && !empty($info['video_img'])) {
            $img_urls = explode(",", $info['video_img']);
            if($img_urls && is_array($img_urls)) {
                foreach($img_urls as $it) {
                    $img_thumb[] = $it;
                }
            }
        }
       
        $videos = explode(",", $info['video_url']);
        //$img_thumb[0] = 'https://static-ap.askpert.com/qiniu_move/midnight_snack.jpg?x-image-process=style/style-600';
        if(!strstr($img_thumb[0],'?x-image-process=style/style-600')){
            $img_thumb[0].='?x-image-process=style/style-600';
        }
        $this->assign('thumb',$img_thumb);
        $this->assign('videos', $videos);
        $this->assign('info', $info);
        
        $this->assign('url', "https://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
       
        return $this->fetch();
    }
    public function getUser(){
        $id  = $this->request->request('id');
        $info = Db::name('user')->where('id',$id)->find();
        $fans = Db::name('user_attention')->where('touid',$id)->count();
        $guanzhu = Db::name('user_attention')->where('uid',$id)->count();
        
        $tiezi = Db::name('dynamic')->where('uid='.$id.' and isdel = 0 and status = 1')->sum('reading');
       
        $read = $tiezi;
        $likes =  Db::name('dynamic')->where('uid',$id)->where('status',1)->field("id")->select();
        
        $new = [];
        $likesNum = 0;
        if($likes){
          foreach ($likes as $val){
            $new[]=$val['id'];              
          }
         $sIds =implode(',',$new);
         if($sIds){
             $where= " note_id in ({$sIds})" ;
             $likesNum =Db::name('note_like')->where($where)->count();
         }
         
        }
        
      
        $this->assign([
           'fans' => $fans,
           'guanzhu' => $guanzhu,
           'read' => $read,
           'likesNum' => $likesNum,
           'id' =>$id
        ]);
   
        $this->assign('info', $info);
        return $this->fetch();
    }
    public function getDynamics(){
        $uid = input('uid');
        $page =input('page',1);
        $list = Db::name('dynamic')->where('uid='.$uid.' and isdel = 0 and status = 1')->order('id desc')->page($page,20)->select();
       
        foreach($list as $k=>$v){
            if($v['thumb']){
                list($v['thumb']) = explode(',',$v['thumb']);
            }
             
            if($v['video_img']){
               
                list($v['video_img']) = explode(',',$v['video_img']);
            }
            $list[$k]  = $v;
        }
      
        $data = [
           'code' => 200,
           'list' => $list
        ];
        return json($data)->send();
    }
    public function getVideos(){
        $uid = input('uid');
        $page =input('page',1);
        $list = Db::name('video')->where('uid='.$uid.' and is_delete = 0 and status = 1 and draft = 0')->order('id desc')->page($page,20)->select();

        $data = [
           'code' => 200,
           'list' => $list
        ];
        return json($data)->send();
    }
    
  
    


}
