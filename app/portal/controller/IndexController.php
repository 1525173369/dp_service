<?php
// +----------------------------------------------------------------------
// | ThinkCMF [ WE CAN DO IT MORE SIMPLE ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013-2019 http://www.thinkcmf.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 老猫 <thinkcmf@126.com>
// +----------------------------------------------------------------------
namespace app\portal\controller;

use cmf\controller\HomeBaseController;
use think\Db;
class IndexController extends HomeBaseController
{

    // 首页
    public function index()
    {
   
         $this->redirect('admin/index/index');
        exit;
        return $this->fetch();
    }
    
    public function scanqr() {
    	return $this->fetch();
    }

    public function getCustomer(){
       /* $data = [
            'operating' => 'Operating Hourse1',
            'week' => 'Monday - Friday(except public holiday)',
            'datetime' => '9am - 6pm',
            'email' => 'Email',
            'one' => 'For enquiry hello@askpert.com',
            'two' => 'For app issues:support@askpert.com'
        ];*/
        $info =Db::name('about')->where('id',1)->find();
        $lan  = $this->request->get('lan','gb');
        $op = false;
        if($lan == 'en'){
            $op = $info['operating_en'];
            $email = 'Email';
            $op_text = 'Business Hours';
        }
        if($lan == 'ms'){
            $op = $info['operating_ms'];
            $email = 'Mailbox';
            $op_text = 'Operating Hourse';
        }
        if($lan == 'gb'){
            $op = $info['operating_gb'];
            $email = '邮箱';
            $op_text = '营业时间';
        }
        $data = [
            'op' => $op,
            'email_text' => $email,
            'email' => $info['email'],
            'op_text' => $op_text,
            'extra' => $info['extra']
        ];
        return json($data)->send();

        exit(json_encode($data));
    }

}

