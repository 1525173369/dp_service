<?php


namespace app\merch\controller;


use cmf\controller\BaseController;
use Think\Controller;
use Think\Db;

class  LoginController extends MerchController
{
    /**
     * 登录
     */
    public function login(){
        if($this->request->isPost()){
            $shop_account = $this->request->post('user_login');
            $shop_password = $this->request->post('password');
            $ret = Db::name('shop_apply')->where('status',1)->where('shop_account',$shop_account)->where('shop_password',$shop_password)->find();
          
            $is_merch_login = true;
            if(!$ret){
                //看下是不是店主登录后台
                $user = Db::name('user')->where('user_login',$shop_account)/*->where('user_pass',setPass($shop_password))*/->find();
                if($user){
                    $user_pass = $user['user_pass'];
                    if($user_pass == setPass($shop_password)){
                        $ret = Db::name('shop_apply')->where('status',1)->where('uid',$user['id'])->find();
                        if(!$ret){
                             $this->error('不存在该商铺');
                        }
                        $is_merch_login = false;
                        $ret['is_user_login'] = 1;
                    }else{
                        $this->error('密码错误');
                    }
                }else{
                    $this->error('不存在该用户');
                }
            }
            if($ret['uid']){
                
                if($ret['coin'] > 0){
                     Db::name('user')->where('id',$ret['uid'])->setInc('coin',$ret['coin']);
                     Db::name('shop_apply')->where('id',$ret['id'])->setDec('coin',$ret['coin']);
                }
                $user = Db::name('user')->where('id',$ret['uid'])->find();
                $ret['coin'] = $user['coin'];
            }
          
            session('merch_login',[
                'info' => $ret
            ]);
            $this->success('Login successful','/merch/index/order');
        }else{
            $user_login = input('user_login');
            $password = input('password');
            $title = '商家登录';
            if(session('merch_login')){
                header('Location:/merch/index/order');
                exit();
            }
            return view('',[
                'title' => $title,
            ]);
        }

    }

    public function loginout(){
        \think\facade\Session::clear();
        header('Location:/merch/login/login');
        exit();
    }

}