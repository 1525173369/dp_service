<?php


namespace app\merch\controller;


use app\api\model\ScannelCashModel;
use app\api\model\ScannelOrderModel;
use app\api\model\ShopModel;
use think\Collection;
use think\Db;
use think\Exception;
use app\api\model\ShopLogModel;

class IndexController  extends MerchController
{
    public function __construct()
    {
        
        $login = session('merch_login');
        if(!$login){
            header('Location:'.'/merch/login/login');
            exit();
        }
         connectionRedis();

        parent::__construct();
        $site = $this->request->domain();

        $this->assign('site',$site);
    }


    public function order(){
        $shop_id = session('merch_login')['info']['id'];
       
        $key = input('key');
        $list = ScannelOrderModel::with([
            'users1','susers1','shops1'
        ])->where('shopid',$shop_id)->where(function($q)use($key){
            if($key = trim($key)){
                    $q->whereOr('users1.user_nickname','LIKE','%'.$key.'%');
                    $q->whereOr('users1.id',$key);

            }
        })->order('id','desc')->paginate(15,false,[
            'query' => [
                'key'=>$key
            ]
        ]);
        $shopid = session('merch_login')['info']['id'];
        $shop = ShopModel::where('id',$shopid)->where('status',1)->find();
        if($shop['uid']){
             $user = Db::name('user')->where('id',$shop['uid'])->find();
             $shop['coin'] = $user['coin'];
        }
        $this->assign('shop',$shop);
        $config =  getConfigPub();
        $merch_cash_rate = $config['merch_cash_rate'];
        list($a,$b) = explode(':',$merch_cash_rate);
        $rate = number_format($b/$a,2);
       
        return $this->fetch('',[
            'list' => $list,
            'key'  => $key,
            'a' => $a,
            'b' => $b,
            'rate' => $rate
        ]);
    }
    public function cash_list(){
        $shopid = session('merch_login')['info']['id'];
     
        $key = input('key');
        $list = ScannelCashModel::with([
            'shops'
        ])->where('scannel_cash_model.shopid',$shopid)->where('shopid','>',0)->where(function($q)use($key){
            if($key = trim($key)){
                $q->whereOr('scannel_cash_model.ordersn','LIKE',$key);
            }
        })->order('id','desc')->paginate(15,false,[
            'query' => [
                'key'=>$key
            ]
        ]);
        #echo ScannelCashModel::getLastSql();die;
 
        $shop = ShopModel::where('id',$shopid)->where('status',1)->find();
        if($shop['uid']){
             $user = Db::name('user')->where('id',$shop['uid'])->find();
             $shop['coin'] = $user['coin'];
        }
        $this->assign('shop',$shop);
        

        return $this->fetch('',[
            'list' => $list,
            'key'  => $key
        ]);
    }
    public function cash(){
        if($this->request->isPost()){
           
            $shopid = session('merch_login')['info']['id'];
            $request = $this->request->post();
            $coin =  $request['coin'];
            if($coin <= 0){
               $this->error('金币不能小于0');
            }
            $config =  getConfigPub();

            $merch_least_coin  = $config['merch_least_coin'];
            if($coin < $merch_least_coin){
                $this->error('Required minimum amount of 10000 coins to withdraw');
            }
            Db::startTrans();
            try {
                $shop = ShopModel::where('id',$shopid)/*->lock(true)*/->find();  //加锁防止这边有人去改变
                if($shop['uid']){
                    $shopHost = Db::name('user')->where('id',$shop['uid'])->find();
                    $shop['coin'] = $shopHost['coin'];
                }
                if($shop['coin'] < $coin){
                    throw new Exception('Insufficient coins');
                }
                $rate = $config['merch_cash_rate']; //商家提现比例
                list($a,$b) = explode(':',$rate);
                $rate = number_format($b/$a,2);
                $money = round($coin *  $rate,2);

                $data = [
                    'uid' => $shop['uid'],
                    'shopid' => $shop['id'],
                    'money' => $money,
                    'bank_name' =>  $request['bank_name'],
                    'bank_account' => $request['bank_account'],
                    //'bank_username' => $request['bank_username'],
                    //'bank_cardid' => $request['bank_cardid'],
                    'status' => 0,
                    'ordersn' => 'TX'.date('YmdHis').mt_rand(1000,9999),
                    //'mobile' => $this->request->request('mobile'),
                    'coin'  => $coin,
                    'rate' => $rate,
                    'company_name' => $request['company_name'],
                    'ssm_no'  => $request['ssm_no'],
                    'swift_code' => $request['swift_code']
                ];
                $bool = ScannelCashModel::create($data);
                ShopLogModel::addlog($shopid,$coin,'提现支出',2,$data['ordersn']);
                if(!$shop['uid']){
                    $ret = ShopModel::where('id',$shopid)->setDec('coin',$coin);
                }else{
                    $ret = Db::name('user')->where('id',$shop['uid'])->setDec('coin',$coin);
                }
                
                if($bool && $ret){
                    Db::commit();
                }else{
                    throw new Exception('服务器繁忙');
                }
                $code = 1;
                $msg = 'Request sent successful';
            }catch (\Exception $e){
                Db::rollback();
                $msg = $e->getMessage();
                $code = 0;
            }
            if($code){
                $this->success($msg);
            }
            $this->error($msg);


        }else{
            $shopid = session('merch_login')['info']['id'];

            $shop = ShopModel::where('id',$shopid)->find();
            if($shop['uid']){
                 $user = Db::name('user')->where('id',$shop['uid'])->find();
                 $shop['coin'] = $user['coin'];
             }
            $this->assign('shop',$shop);
            return $this->fetch();
        }

    }
}