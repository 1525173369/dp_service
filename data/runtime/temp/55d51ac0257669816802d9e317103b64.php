<?php /*a:2:{s:73:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/admin/video/indexs.html";i:1627376458;s:68:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style>
    .imgtip{
        margin-bottom:5px;
    }
</style>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a >视频列表</a></li>
        <li class=""><a href="<?php echo url('video/adds'); ?>">新增</a></li>
    </ul>

    <form class="well form-inline margin-top-20" method="post" action="<?php echo url('video/indexs'); ?>">




        用户ID：
        <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"
               placeholder="用户ID">

        店铺名搜索：
        <input class="form-control" type="text" name="shopname"  style="width: 200px;" value="<?php echo input('request.shopname'); ?>"
               placeholder="请搜索店铺名">

        关键词：
        <input class="form-control" type="text" name="keyword" style="width: 200px;" value="<?php echo input('request.keyword'); ?>"
               placeholder="关键词">


        <input type="submit" class="btn btn-primary" value="搜索"/>
        <a class="btn btn-danger" href="<?php echo url('video/indexs'); ?>">清空</a>
    </form>

    <form method="post" class="js-ajax-form">
        <div class="table-actions">
            <button class="btn btn-danger btn-sm js-ajax-submit" type="submit" data-action="<?php echo url('video/del'); ?>"
                    data-subcheck="true"><?php echo lang('DELETE'); ?>
            </button>
        </div>
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th width="16">
                    <label>
                        <input type="checkbox" class="js-check-all" data-direction="x" data-checklist="js-check-x">
                    </label>
                </th>
                <th>ID</th>
                <th>用户id/昵称</th>
                <th>店铺id/店铺名称</th>
                <th width="10%">视频简介</th>
                <th>封面</th>
                <th >视频</th>

                <th>点赞数</th>
                <th>评论数</th>
                <th><?php echo lang('阅读数'); ?></th>
                <th>是否删除</th>
                <th>状态</th>
                <th>创建时间</th>
                <th align="center"><?php echo lang('ACTIONS'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <tr>
                    <td>
                        <input type="checkbox" class="js-check" data-yid="js-check-y" data-xid="js-check-x" name="ids[]" value="<?php echo $vo['id']; ?>">
                    </td>
                    <td><?php echo $vo['id']; ?></td>
                    <td><?php echo $vo['uid']; ?>/<?php echo $vo['users']['user_nickname']; ?></td>
                    <td><?php echo $vo['shop_id']; ?>/<?php echo $vo['shops']['name']; ?></td>
                    <td><?php echo htmlspecialchars($vo['desc']); ?></td>
                    <td><img src="<?php echo $vo['video_img']; ?>" width="150" height=""></td>
                    <td>
                        <video width="300" height="300"  controls src="<?php echo $vo['video_url']; ?>" type="video/mp4"></video>
                    </td>

                    <td><?php echo $vo['likes']; ?></td>
                    <td><?php echo $vo['comments']; ?></td>
                    <td><?php echo $vo['visits']; ?></td>
                     <td><?php if($vo['is_delete'] == 1): ?><span class="text-danger">已删除</span><?php else: ?><span class="text-success">未删除</span><?php endif; ?></td>

                    <?php if($vo['status'] == '0'): ?>
                        <td>已下架</td>
                        <?php else: ?>
                        <td>已上架</td>
                    <?php endif; ?>

                    <td><?php echo date('Y-m-d H:i',$vo['createtime']); ?></td>
                    <td>

                        <a class="btn btn-xs btn-primary comment" data-id="<?php echo $vo['id']; ?>">查看评论</a>
                        <?php if($vo['status'] == 1): ?>
                        <a class="btn btn-xs btn-warning  js-ajax-dialog-btn" data-msg="你确定要下架吗?" href="<?php echo url('video/multe',array('id'=>$vo['id'])); ?>">下架</a>
                        <?php endif; if($vo['status'] == 0): ?>
                        <a class="btn btn-xs btn-success  js-ajax-dialog-btn" data-msg="你确定要上架吗?" href="<?php echo url('video/multe',array('id'=>$vo['id'])); ?>">上架</a>
                        <?php endif; ?>
                        <a class="btn btn-xs btn-danger js-ajax-delete" href="<?php echo url('video/del',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a>
                        <a class="btn btn-xs btn-primary" href='<?php echo url("video/edits",array("id"=>$vo["id"])); ?>'>编辑</a>

                    </td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
        <div class="pagination"><?php echo $list; ?></div>

    </form>
</div>
<div id="enlarge_images" style="position:fixed;display:none;z-index:2;"></div>
<script src="/static/js/admin.js"></script>
<script>
        $(function(){
            Wind.use('layer');
        })
         $('.comment').click(function(){
                var _this=$(this);
                var id=_this.data('id');

                layer.open({
                    type: 2,
                    title: '查看评论',
                    shadeClose: true,
                    shade: 0.8,
                    area: ['80%', '100%'],
                    content: '/admin/video/comments?id='+id
                });

            });




    </script>
</body>
</html>