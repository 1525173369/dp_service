<?php /*a:2:{s:71:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/admin/cash/index.html";i:1630725733;s:68:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<style>
    
    
</style>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li class="active"><a>提现列表</a></li>

		</ul>
		<form class="well form-inline margin-top-20" name="form1" method="post" action="">
		  提现状态：
			<select class="form-control" name="status">
                <?php if(is_array($status) || $status instanceof \think\Collection || $status instanceof \think\Paginator): $i = 0; $__LIST__ = $status;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                    <option value="<?php echo $key; ?>" <?php if(input('request.status') == $key): ?>selected<?php endif; ?> ><?php echo $v; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>		
			</select>

			<select  class="form-control" name="time_type">
				<option <?php if(input('request.time_type') == 1): ?>selected<?php endif; ?> value="1">创建时间</option>
				<option  <?php if(input('request.time_type') == 2): ?>selected<?php endif; ?> value="2">处理时间</option>
			</select>
			<input class="form-control js-bootstrap-date" name="start_time" id="start_time" autocomplete="off" value="<?php echo input('request.start_time'); ?>" aria-invalid="false" style="width: 110px;"> - 
            <input class="form-control js-bootstrap-date" name="end_time" id="end_time" autocomplete="off" value="<?php echo input('request.end_time'); ?>" aria-invalid="false" style="width: 110px;">
            
            用户ID：
            <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"
                   placeholder="请输入用户ID">

			店铺ID：
			<input class="form-control" type="text" name="shopid" style="width: 200px;" value="<?php echo input('request.shopid'); ?>"
				   placeholder="请输入店铺ID">
                   
			银行名称：
            <input class="form-control" type="text" name="bank_name" style="width: 200px;" value="<?php echo input('request.bank_name'); ?>"
                   placeholder="银行名称">
			银行卡号：
			<input class="form-control" type="text" name="bank_account" style="width: 210px;" value="<?php echo input('request.bank_account'); ?>"
				   placeholder="请输入银行卡号">
			关键词:
			<input class="form-control" type="text" name="key" style="width: 200px;" value="<?php echo input('request.key'); ?>"
				   placeholder="关键词">

			<input type="button" class="btn btn-primary" value="搜索" onclick="form1.action='<?php echo url('Cash/index'); ?>';form1.submit();"/>

			<!--<div class="admin_main" style="margin-top:10px">
				当前提现总金额：12312313

					成功提现金额：1231
					待处理金额：123

			</div>-->
		</form>	
		
		<form method="post" class="js-ajax-form" >
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>用户id/店铺id</th>
						<th>提现金额/金币/比例值</th>
						<th>银行名称</th>
						<th>银行卡号</th>
						<th>银行用户名称/公司信息</th>
						<th>银行预留电话</th>
						<th>状态</th>
						<th>提交时间</th>
						<th>处理时间</th>
						<th><?php echo lang('ACTIONS'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
					<tr>
						<?php 
							if($vo['shopid'] == 0){
							   $vo['shopid'] = '无';
							}
						 ?>
						<td align="center"><?php echo $vo['id']; ?></td>
						<td><?php echo $vo['uid']; ?>/<?php echo $vo['shopid']; ?></td>
						<td>金额:<?php echo $vo['money']; ?><br>
							金币:<?php echo $vo['coin']; ?><br>
							比例值:<?php echo $vo['rate']; ?>
						</td>
						<td><?php echo $vo['bank_name']; ?></td>
						<td>
                            <?php echo $vo['bank_account']; ?>
                        </td>				
						<td><?php echo $vo['bank_username']; if($vo['company_name']): ?>
						        <br> <label class="label-success">COMPANY INFO</label>
						        <br>----------
                                <br>COMPANY NAME: <span class="s"><?php echo $vo['company_name']; ?></span>
                                <br>SSM No: <span class="s"><?php echo $vo['ssm_no']; ?></span>
                                <br>SWIFT CODE: <span class="s"><?php echo $vo['swift_code']; ?></span>
                                <br> --------
                               
                                
                            <?php endif; ?>    
						</td>
						<td><?php echo $vo['mobile']; ?></td>
						<td>
							<?php if($vo['status'] == 0): ?><span class="btn btn-xs btn-warning">申请中</span><?php endif; if($vo['status'] == 1): ?><span class="btn btn-xs btn-success">已通过</span><?php endif; if($vo['status'] == 2): ?><span class="btn btn-xs btn-danger">已拒绝</span><?php endif; if($vo['status'] == 3): ?><span class="btn btn-xs btn-primary">已完成</span><?php endif; ?>

						</td>
						<td><?php echo date('Y-m-d H:i:s',$vo['createtime']); ?></td>
						<td>
							<?php if($vo['handle_time'] == 0): ?>
								无
								<?php else: ?>
								<?php echo date('Y-m-d H:i:s',$vo['handle_time']); ?>
							<?php endif; ?>

						 </td>
						<td>	
						<?php if($vo['status'] == '0'): ?>
                        <a class="btn btn-xs btn-primary" href='<?php echo url("Cash/verify",array("id"=>$vo["id"])); ?>'><?php echo lang('审核'); ?></a>
						<?php else: ?>
							<a class="btn btn-xs btn-green" href='<?php echo url("Cash/edit",array("id"=>$vo["id"])); ?>'><?php echo lang('详情'); ?></a>
						<?php endif; ?>
                         
                        <!-- <a class="btn btn-xs btn-danger js-ajax-delete" href="<?php echo url('Cash/del',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a> -->
						</td>
					</tr>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</tbody>
			</table>
			<div class="pagination"><?php echo $list; ?></div>


		</form>
	</div>
	<script src="/static/js/admin.js"></script>
</body>
</html>