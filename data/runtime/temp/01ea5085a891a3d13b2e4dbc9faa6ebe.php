<?php /*a:2:{s:95:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/setting/configpri.html";i:1623124595;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style>
.cdnhide{
	display:none;
}
</style>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#A" data-toggle="tab">基本设置</a></li>
        <li><a href="#B" data-toggle="tab">登录配置</a></li>
        <!--<li><a href="#D" data-toggle="tab">映票提现配置</a></li>-->
        <li><a href="#E" data-toggle="tab">推送配置</a></li>
        <!--<li><a href="#F" data-toggle="tab">支付配置</a></li>-->
        <!--<li><a href="#G" data-toggle="tab">邀请奖励</a></li>-->
        <!--<li><a href="#H" data-toggle="tab">统计配置</a></li>-->
        <!--<li><a href="#I" data-toggle="tab">视频配置</a></li>-->
        <!--<li><a href="#J" data-toggle="tab">店铺/商品配置</a></li>-->
        <!--<li><a href="#K" data-toggle="tab">动态配置</a></li>-->
        <!--<li><a href="#L" data-toggle="tab">游戏配置</a></li>-->
        <!--<li><a href="#M" data-toggle="tab">物流配置</a></li>-->
    </ul>
    <form class="form-horizontal js-ajax-form margin-top-20" role="form" action="<?php echo url('setting/configpriPost'); ?>" method="post">
        <fieldset>
            <div class="tabbable">
                <div class="tab-content">
                    <div class="tab-pane active" id="A">
                    
                        <div class="form-group">
                            <label for="input-service_switch" class="col-sm-2 control-label">客服</label>
                            <div class="col-md-6 col-sm-10">
                                <select class="form-control" name="options[service_switch]">
                                    <option value="0">关闭</option>
                                    <option value="1" <?php if($config['service_switch'] == '1'): ?>selected<?php endif; ?>>开启</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-service_url" class="col-sm-2 control-label">客服链接</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-service_url"
                                       name="options[service_url]" value="<?php echo (isset($config['service_url']) && ($config['service_url'] !== '')?$config['service_url']:''); ?>">
                                       <p class="help-block">注册链接：http://www.53kf.com/reg/index?yx_from=210260</p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-sensitive_words" class="col-sm-2 control-label">敏感词</label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-sensitive_words" name="options[sensitive_words]" ><?php echo (isset($config['sensitive_words']) && ($config['sensitive_words'] !== '')?$config['sensitive_words']:''); ?></textarea><p class="help-block">设置多个敏感字，请用英文状态下逗号隔开</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="1">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="B">
                        <div class="form-group">
                            <label for="input-reg_reward" class="col-sm-2 control-label">注册奖励</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-reg_reward"
                                       name="options[reg_reward]" value="<?php echo (isset($config['reg_reward']) && ($config['reg_reward'] !== '')?$config['reg_reward']:''); ?>">
                                       <p class="help-block">新用户注册奖励（整数）</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-bonus_switch" class="col-sm-2 control-label">登录奖励开关</label>
                            <div class="col-md-6 col-sm-10">
                                <select class="form-control" name="options[bonus_switch]">
                                    <option value="0">关闭</option>
                                    <option value="1" <?php if($config['bonus_switch'] == '1'): ?>selected<?php endif; ?>>开启</option>
                                </select>
                            </div>
                        </div>
                        
                        
                        
                        <div class="form-group">
                            <label for="input-login_wx_appid" class="col-sm-2 control-label">微信公众平台Appid</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-login_wx_appid" name="options[login_wx_appid]" value="<?php echo (isset($config['login_wx_appid']) && ($config['login_wx_appid'] !== '')?$config['login_wx_appid']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-login_wx_appsecret" class="col-sm-2 control-label">微信公众平台AppSecret</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-login_wx_appsecret" name="options[login_wx_appsecret]" value="<?php echo (isset($config['login_wx_appsecret']) && ($config['login_wx_appsecret'] !== '')?$config['login_wx_appsecret']:''); ?>"> 
                            </div>
                        </div>
                        <!-- <div class="form-group">
                            <label for="input-ihuyi_account" class="col-sm-2 control-label">互亿无线APIID</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ihuyi_account" name="options[ihuyi_account]" value="<?php echo (isset($config['ihuyi_account']) && ($config['ihuyi_account'] !== '')?$config['ihuyi_account']:''); ?>"> 短信验证码   http://www.ihuyi.com/  互亿无线后台-》验证码、短信通知-》账号及签名->APIID
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-ihuyi_ps" class="col-sm-2 control-label">互亿无线key</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ihuyi_ps" name="options[ihuyi_ps]" value="<?php echo (isset($config['ihuyi_ps']) && ($config['ihuyi_ps'] !== '')?$config['ihuyi_ps']:''); ?>">  短信验证码 互亿无线后台-》验证码、短信通知-》账号及签名->APIKEY
                            </div>
                        </div> -->
                        
                        <div class="form-group">
                            <label for="input-sendcode_switch" class="col-sm-2 control-label">短信验证码开关</label>
                            <div class="col-md-6 col-sm-10">
                                <select class="form-control" name="options[sendcode_switch]">
                                    <option value="0">关闭</option>
                                    <option value="1" <?php if($config['sendcode_switch'] == '1'): ?>selected<?php endif; ?>>开启</option>
                                </select>
                                <p class="help-block">短信验证码开关,关闭后不再发送真实验证码，采用默认验证码123456</p>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-ccp_sid" class="col-sm-2 control-label">容联云ACCOUNT SID</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ccp_sid" name="options[ccp_sid]" value="<?php echo (isset($config['ccp_sid']) && ($config['ccp_sid'] !== '')?$config['ccp_sid']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-ccp_token" class="col-sm-2 control-label">容联云AUTH TOKEN</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ccp_token" name="options[ccp_token]" value="<?php echo (isset($config['ccp_token']) && ($config['ccp_token'] !== '')?$config['ccp_token']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-ccp_appid" class="col-sm-2 control-label">容联云应用APPID</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ccp_appid" name="options[ccp_appid]" value="<?php echo (isset($config['ccp_appid']) && ($config['ccp_appid'] !== '')?$config['ccp_appid']:''); ?>"> 
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-ccp_tempid" class="col-sm-2 control-label">容联云短信模板ID</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ccp_tempid" name="options[ccp_tempid]" value="<?php echo (isset($config['ccp_tempid']) && ($config['ccp_tempid'] !== '')?$config['ccp_tempid']:''); ?>"> 
                            </div>
                        </div>
                        
                        
                        
                        <div class="form-group">
                            <label for="input-iplimit_switch" class="col-sm-2 control-label">短信验证码IP限制开关</label>
                            <div class="col-md-6 col-sm-10">
								<select class="form-control" name="options[iplimit_switch]">
                                    <option value="0">关闭</option>
                                    <option value="1" <?php if($config['iplimit_switch'] == '1'): ?>selected<?php endif; ?>>开启</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-iplimit_times" class="col-sm-2 control-label">短信验证码IP限制次数</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-iplimit_times" name="options[iplimit_times]" value="<?php echo (isset($config['iplimit_times']) && ($config['iplimit_times'] !== '')?$config['iplimit_times']:''); ?>"> 
                                <p class="help-block">同一IP每天可以发送验证码的最大次数</p>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="E">
                        <div class="form-group">
                            <label for="input-TPNS_service" class="col-sm-2 control-label">移动推送 TPNS 服务地址</label>
                            <div class="col-md-6 col-sm-10">
								<select class="form-control" name="options[TPNS_service]">
                                    <option value="https://api.tpns.tencent.com/" <?php if($config['TPNS_service'] == 'https://api.tpns.tencent.com/'): ?>selected<?php endif; ?>>广州服务接入点</option>
                                    <option value="https://api.tpns.sh.tencent.com/" <?php if($config['TPNS_service'] == 'https://api.tpns.sh.tencent.com/'): ?>selected<?php endif; ?>>上海服务接入点</option>
                                    <option value="https://api.tpns.hk.tencent.com/" <?php if($config['TPNS_service'] == 'https://api.tpns.hk.tencent.com/'): ?>selected<?php endif; ?>>中国香港服务接入点</option>
                                    <option value="https://api.tpns.sgp.tencent.com/" <?php if($config['TPNS_service'] == 'https://api.tpns.sgp.tencent.com/'): ?>selected<?php endif; ?>>新加坡服务接入点</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-TPNS_sandbox" class="col-sm-2 control-label">腾讯推送模式</label>
                            <div class="col-md-6 col-sm-10">
								<select class="form-control" name="options[TPNS_sandbox]">
                                    <option value="dev">开发</option>
                                    <option value="product" <?php if($config['TPNS_sandbox'] == 'product'): ?>selected<?php endif; ?>>生产</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-TPNS_android_id" class="col-sm-2 control-label">安卓AccessId</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-TPNS_android_id" name="options[TPNS_android_id]" value="<?php echo (isset($config['TPNS_android_id']) && ($config['TPNS_android_id'] !== '')?$config['TPNS_android_id']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-TPNS_android_key" class="col-sm-2 control-label">安卓AccessKey</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-TPNS_android_key" name="options[TPNS_android_key]" value="<?php echo (isset($config['TPNS_android_key']) && ($config['TPNS_android_key'] !== '')?$config['TPNS_android_key']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-TPNS_android_secret" class="col-sm-2 control-label">安卓SecretKey</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-TPNS_android_secret" name="options[TPNS_android_secret]" value="<?php echo (isset($config['TPNS_android_secret']) && ($config['TPNS_android_secret'] !== '')?$config['TPNS_android_secret']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-TPNS_ios_id" class="col-sm-2 control-label">IOS_AccessId</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-TPNS_ios_id" name="options[TPNS_ios_id]" value="<?php echo (isset($config['TPNS_ios_id']) && ($config['TPNS_ios_id'] !== '')?$config['TPNS_ios_id']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-TPNS_ios_key" class="col-sm-2 control-label">IOS_AccessKey</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-TPNS_ios_key" name="options[TPNS_ios_key]" value="<?php echo (isset($config['TPNS_ios_key']) && ($config['TPNS_ios_key'] !== '')?$config['TPNS_ios_key']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-TPNS_ios_secret" class="col-sm-2 control-label">IOS_SecretKey</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-TPNS_ios_secret" name="options[TPNS_ios_secret]" value="<?php echo (isset($config['TPNS_ios_secret']) && ($config['TPNS_ios_secret'] !== '')?$config['TPNS_ios_secret']:''); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="M">
                        
                        <div class="form-group">
                            <label for="input-express_type" class="col-sm-2 control-label">物流模式</label>
                            <div class="col-md-6 col-sm-10">
                                <label class="radio-inline"><input type="radio" name="options[express_type]" value="0" <?php if($config['express_type'] == '0'): ?>checked<?php endif; ?> >开发版</label>
                                <label class="radio-inline"><input type="radio" name="options[express_type]" value="1" <?php if($config['express_type'] == '1'): ?>checked<?php endif; ?> >正式版</label>
                                <p class="help-block">开发版仅适用于程序调试,每天查询最多500次,且只支持申通、圆通、中通;</p>
                                <p class="help-block">正式运营后,请购买三方套餐,将正式版电商EBusinessID和AppKey填写,并且将此处的物流模式切换为正式版；<a href="http://www.kdniao.com" target="_blank">立即购买</a></p>

                            </div>

                        </div>

                        <div class="form-group">
                            <label for="input-express_id_dev" class="col-sm-2 control-label">用户ID（开发版）</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-express_id_dev" name="options[express_id_dev]" value="<?php echo (isset($config['express_id_dev']) && ($config['express_id_dev'] !== '')?$config['express_id_dev']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-express_appkey_dev" class="col-sm-2 control-label">Api Key（开发版）</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-express_appkey_dev" name="options[express_appkey_dev]" value="<?php echo (isset($config['express_appkey_dev']) && ($config['express_appkey_dev'] !== '')?$config['express_appkey_dev']:''); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-express_id" class="col-sm-2 control-label">用户ID (正式版)</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-express_id" name="options[express_id]" value="<?php echo (isset($config['express_id']) && ($config['express_id'] !== '')?$config['express_id']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <label for="input-express_appkey" class="col-sm-2 control-label">Api Key (正式版)</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-express_appkey" name="options[express_appkey]" value="<?php echo (isset($config['express_appkey']) && ($config['express_appkey'] !== '')?$config['express_appkey']:''); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </fieldset>
    </form>

</div>
<script type="text/javascript" src="/static/js/admin.js"></script>
<script>
(function(){

    $('.btn-cancel-thumbnail2').click(function () {
        $('#thumbnail2-preview').attr('src', '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png');
        $('#thumbnail2').val('');
    });
    

    $("#sdk label").on('click',function(){
        var v=$("input",this).val();
        if(v==1){
            $("#cdn label input[type=radio]").attr('disabled','disabled');
            $("#cdn label input[type=radio][value=2]").removeAttr('disabled');
            $("#cdn label").eq(1).click();
        }else{
            $("#cdn label input[type=radio]").removeAttr('disabled');
        }
    })
    
    $("#cdn label").on('click',function(){
        var v_d=$("input",this).attr('disabled');
        if(v_d=='disabled'){
            return !1;
        }
        var v=$("input",this).val();
        var b=$("#cdn_switch_"+v);
        $(".cdn_bd").hide();
        b.show();
    })
    
    $("#cloudtype label").on('click',function(){
        var v=$("input",this).val();
        var b=$("#cloudtype_"+v);
        $(".cloudtype_bd").siblings('.cloudtype_bd').hide();
        b.show();
    })

       
    
})()
</script>
</body>
</html>
