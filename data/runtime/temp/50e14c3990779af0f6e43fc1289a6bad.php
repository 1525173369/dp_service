<?php /*a:2:{s:87:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/goods/add.html";i:1623124595;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<script type="text/html" id="photos-item-tpl">
    <li id="saved-image{id}">
        <input id="photo-{id}" type="hidden" name="photo_urls[]" value="{filepath}">
        <input class="form-control" id="photo-{id}-name" type="text" name="photo_names[]" value="{name}"
               style="width: 200px;" title="图片名称">
        <img id="photo-{id}-preview" src="{url}" style="height:36px;width: 36px;"
             onclick="imagePreviewDialog(this.src);">
        <a href="javascript:uploadOneImage('图片上传','#photo-{id}');">替换</a>
        <a href="javascript:(function(){$('#saved-image{id}').remove();})();">移除</a>
    </li>
</script>
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li ><a href="<?php echo url('Goods/index'); ?>">商品列表</a></li>
			<li class="active"><a ><?php echo lang('ADD'); ?></a></li>
		</ul>
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">通用信息</a></li>
          <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">商品相册</a></li>
        </ul>
		<form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('goods/addPost'); ?>">
            
          <div class="tab-content">
            
            <div role="tabpanel" class="tab-pane fade in active" id="home">
            <div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>商品名称:</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-name" value="" name="goods_name">
				</div>
			</div>

		
			<!--<div class="form-group">-->
			<!--	<label for="input-one_class" class="col-sm-2 control-label"><span class="form-required"></span>商品分类:</label>-->
   <!--                <div class="col-md-6 col-sm-10">-->
   <!--                     <select class="form-control" name="cat_id">-->
   <!--                         <?php if(is_array($type) || $type instanceof \think\Collection || $type instanceof \think\Paginator): $i = 0; $__LIST__ = $type;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>-->
   <!--                             <option value="<?php echo $v['id']; ?>">-->
   <!--                             <?php echo $v['name']; ?>-->
   <!--                             </option>-->
   <!--                         <?php endforeach; endif; else: echo "" ;endif; ?>-->
   <!--                     </select>-->
   <!--                     <p class="help-block">商品分类可不填</p>-->
   <!--                 </div>-->
			<!--</div>-->
				
			<div class="form-group">
				<label class="col-sm-2 control-label"><span class="form-required"></span>商品规格库存:</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" value="" name="store_count">
				</div>
			</div>
				
			<div class="form-group">
				<label class="col-sm-2 control-label"><span class="form-required"></span>商品市场价:</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" value="" name="market_price">
				</div>
			</div>
				
			<div class="form-group">
				<label class="col-sm-2 control-label"><span class="form-required"></span>商品成本价:</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" value="" name="cost_price">
				</div>
			</div>
				
			<div class="form-group">
				<label class="col-sm-2 control-label"><span class="form-required"></span>商品金币价:</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" value="" name="gold">
				</div>
			</div>

	

	        <div class="form-group">
				<label for="input-content" class="col-sm-2 control-label"><span class="form-required"></span>商品简单描述:</label>
				<div class="col-md-6 col-sm-10">
					<textarea class="form-control" id="input-content" name="goods_remark"></textarea> 
				</div>
			</div>
                
            <div class="form-group">
                <label for="input-ipa_ewm" class="col-sm-2 control-label">商品主图</label>
                <div class="col-md-6 col-sm-10">
                    <input type="hidden" name="original_img" id="thumbnail2" value="">
                    <a href="javascript:uploadOneImage('图片上传','#thumbnail2');">
                        <?php if(empty($site_info['ipa_ewm'])): ?>
                        <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                 id="thumbnail2-preview"
                                 style="cursor: pointer;max-width:150px;max-height:150px;"/>
                        <?php else: ?>
                        <img src="<?php echo cmf_get_image_preview_url($site_info['ipa_ewm']); ?>"
                             id="thumbnail2-preview"
                             style="cursor: pointer;max-width:150px;max-height:150px;"/>
                        <?php endif; ?>
                    </a>
                    <input type="button" class="btn btn-sm btn-cancel-thumbnail2" value="取消图片">
                </div>
            </div>

			

			<div class="form-group">
				<label for="input-sale_nums" class="col-sm-2 control-label"><span class="form-required"></span>商品销量:</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-sale_nums" value="" name="sales_sum">
				</div>
			</div>
				
            <div class="form-group">
                <label for="input-title" class="col-sm-2 control-label"><span class="form-required"></span>内容</label>
                <div class="col-md-6 col-sm-12">
                    <script type="text/plain" id="content" name="goods_content"><?php echo htmlspecialchars_decode((isset($template) && ($template !== '')?$template:"")); ?></script>
                </div>
            </div>
            
            </div>
            <div role="tabpanel" class="tab-pane fade" id="profile">
                    <tr>
                        <td>
                            <ul id="photos" class="pic-list list-unstyled form-inline"></ul>
                            <a href="javascript:uploadMultiImage('图片上传','#photos','photos-item-tpl');"
                               class="btn btn-default btn-sm">选择图片</a>
                        </td>
                    </tr>
            </div>
            <div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
				

					<button type="submit" class="btn btn-primary js-ajax-submit"><?php echo lang('ADD'); ?></button>
				
					<a class="btn btn-default" href="javascript:history.back(-1);"><?php echo lang('BACK'); ?></a>
				</div>
			</div>
			
			
		  </div>	

		</form>
	</div>

    <script src="/static/js/admin.js"></script>
    <script type="text/javascript">
    //编辑器路径定义
    var editorURL = GV.WEB_ROOT;
    </script>
    <script type="text/javascript" src="/static/js/ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="/static/js/ueditor/ueditor.all.min.js"></script>
    <script type="text/javascript">
        var editorcontent = new baidu.editor.ui.Editor();
        editorcontent.render('content');
    </script>

</body>
</html>
