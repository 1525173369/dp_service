<?php /*a:2:{s:88:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/order/info.html";i:1623124595;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap" js-check-wrap>
		<ul class="nav nav-tabs">
			<li ><a href="<?php echo url('order/index'); ?>">订单列表</a></li>
			<li class="active"><a >商品订单详情</a></li>
		</ul>
		<form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('order/editPost'); ?>">
		    
		    <div class="panel panel-default">
            	<div class="panel-heading">
            		基本信息
            	</div>
            	<div class="panel-body">
    	           <div class="form-group">
                        <label for="input-id" class="col-sm-2 control-label"><span class="form-required"></span>订单ID</label>
                        <div class="col-md-6 col-sm-10">
                            <input type="text" class="form-control" id="input-id" value="<?php echo $data['order_id']; ?>" readonly="readonly">
                        </div>
                    </div>
            	</div>
	            <div class="form-group">
                    <label for="input-orderno" class="col-sm-2 control-label"><span class="form-required"></span>订单编号</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-orderno" value="<?php echo $data['order_sn']; ?>" readonly="readonly">
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="input-buyer" class="col-sm-2 control-label"><span class="form-required"></span>会员</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-buyer" value="<?php echo $data['user_nickname']; ?>(<?php echo $data['user_id']; ?>)" readonly="readonly">
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="input-addtime" class="col-sm-2 control-label"><span class="form-required"></span>下单时间</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-addtime" value="<?php echo date('Y-m-d H:i',$data['add_time']); ?>" readonly="readonly">
                    </div>
                </div>
                    
               <div class="form-group">
                    <label for="input-paytime" class="col-sm-2 control-label"><span class="form-required"></span>付款时间</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-paytime" readonly="readonly" <?php if($data['pay_time'] > 0): ?>value="<?php echo date('Y-m-d H:i',$data['pay_time']); ?>"<?php else: ?>value=""<?php endif; ?> >
                    </div>
                </div>
                
                
                <div class="form-group">
                    <label for="input-type" class="col-sm-2 control-label"><span class="form-required"></span>支付方式</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-type" value="<?php echo $data['pay_name']; ?>" readonly="readonly">
                    </div>
                </div>
    
    
    
                <div class="form-group">
                    <label for="input-status" class="col-sm-2 control-label"><span class="form-required"></span>订单状态</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-status" value="<?php echo $status[$data['order_status']]; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-price" class="col-sm-2 control-label"><span class="form-required"></span>订单总价</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-price" value="<?php echo $data['total_amount']; ?>" readonly="readonly">
                    </div>
                </div>
                
            </div>
		   <!--      22            -->
            
        <div class="panel panel-default">
        	<div class="panel-heading">
        		收货信息
        	</div>
        	<div class="panel-body">
                <div class="form-group">
                    <label for="input-username" class="col-sm-2 control-label"><span class="form-required"></span>收货人</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-username" value="<?php echo $data['consignee']; ?>" readonly="readonly">
                    </div>
                </div>
    
                <div class="form-group">
                    <label for="input-phone" class="col-sm-2 control-label"><span class="form-required"></span>联系电话</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-phone" value="<?php echo $data['mobile']; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-address" class="col-sm-2 control-label"><span class="form-required"></span>收货地址</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-address" value="<?php echo $data['province']; ?> <?php echo $data['city']; ?> <?php echo $data['district']; ?> <?php echo $data['address']; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-express_name" class="col-sm-2 control-label"><span class="form-required"></span>配送方式</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-express_name" value="<?php echo $data['shipping_name']; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-express_number" class="col-sm-2 control-label"><span class="form-required"></span>物流单号</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-express_number" value="<?php echo $data['shipping_code']; ?>" readonly="readonly">
                    </div>
                </div>
    
                <div class="form-group">
                    <label for="input-message" class="col-sm-2 control-label"><span class="form-required"></span>买家留言</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-message" value="<?php echo $data['user_note']; ?>" readonly="readonly">
                    </div>
                </div>
                
                
        	</div>
        </div>


        <div class="panel panel-default">
        	<div class="panel-heading">
        		商品信息
        	</div>
        	<div class="panel-body">
    	         <div class="form-group">
                    <label for="input-goodsid" class="col-sm-2 control-label"><span class="form-required"></span>商品ID</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-goodsid" value="<?php echo $data['goods_id']; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-price" class="col-sm-2 control-label"><span class="form-required"></span>商品</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-price" value="<?php echo $data['goods_name']; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-price" class="col-sm-2 control-label"><span class="form-required"></span>商品单价</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-price" value="<?php echo $data['unit_price']; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-nums" class="col-sm-2 control-label"><span class="form-required"></span>数量</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-nums" value="<?php echo $data['goods_num']; ?>" readonly="readonly">
                    </div>
                </div>
        	</div>
        </div>



        <div class="panel panel-default">
        	<div class="panel-heading">
        		费用信息
        	</div>
        	<div class="panel-body">
                <div class="form-group">
                    <label for="input-postage" class="col-sm-2 control-label"><span class="form-required"></span>邮费</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-postage" value="￥<?php echo $data['shipping_price']; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-total" class="col-sm-2 control-label"><span class="form-required"></span>商品总价</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-total" value="￥<?php echo $data['goods_price']; ?>" readonly="readonly">
                    </div>
                </div>
            </div>
        </div>
        
        <div class="panel panel-default">
            <div class="panel-heading">
        		操作
        	</div>
        	<div class="panel-body">
        	     <input id="post-id" type="hidden" name="order_id" value="<?php echo $data['order_id']; ?>">
                 <div class="form-group">
                    <label for="input-express_name" class="col-sm-2 control-label"><span class="form-required"></span>操作备注</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-express_name" value="" name="action_note">
                    </div>
                </div>
        	     
        	     
        	     <?php if($data['order_status'] == 1): ?>
        	         
                <div class="form-group">
                    <label for="input-express_name" class="col-sm-2 control-label"><span class="form-required"></span>配送方式</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-express_name" value="" name="shipping_name">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-express_number" class="col-sm-2 control-label"><span class="form-required"></span>物流单号</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-express_number" value="" name="shipping_code">
                    </div>
                </div>  
        	         
                <div class="form-group">
    				<div class="col-sm-offset-2 col-sm-10">
    					<button type="submit" class="btn btn-primary js-ajax-submit" name="order_status" value="2">去发货</button>
    					<a class="btn btn-default" href="javascript:history.back(-1);"><?php echo lang('BACK'); ?></a>
    				</div>
    			</div>
    			 <?php endif; if($data['order_status'] == 2): ?>
                 <div class="form-group">
    				<div class="col-sm-offset-2 col-sm-10">
    					<button type="submit" class="btn btn-primary js-ajax-submit" name="order_status" value="3">收货</button>
    				</div>
    			</div>
    			<?php endif; if($data['order_status'] == 0): ?>
                 <div class="form-group">
    				<div class="col-sm-offset-2 col-sm-10">
    					<button type="submit" class="btn btn-primary js-ajax-submit" name="order_status" value="2">设为已付款</button>
    				</div>
    			</div>
    			<?php endif; ?>
        	<div>    
        </div>
        </div>
    	</div>
		</form>
        <div class="panel panel-default">
        	<div class="panel-heading">
        		日志信息
        	</div>
        	<div class="panel-body">
                <ul>
                	<?php if(is_array($journal) || $journal instanceof \think\Collection || $journal instanceof \think\Paginator): if( count($journal)==0 ) : echo "" ;else: foreach($journal as $key=>$vo): ?>
                    <li>操作备注：<?php echo $vo['action_note']; ?>,操作时间：<?php echo date('Y-m-d H:i:s',$vo['log_time']); ?>,具体操作：<?php echo $vo['status_desc']; ?></li>
                   <?php endforeach; endif; else: echo "" ;endif; ?>
                </ul>
            </div>
        </div>
	</div>
	<script src="/static/js/admin.js"></script>
    
</body>
</html>