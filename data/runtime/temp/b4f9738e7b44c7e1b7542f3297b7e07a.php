<?php /*a:2:{s:89:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/order/index.html";i:1625811605;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap js-check-wrap">
		<ul class="nav nav-tabs">
			<li class="active"><a>订单列表</a></li>
		</ul>

		<form class="well form-inline margin-top-20" method="post" action="<?php echo url('Order/index'); ?>">

			订单状态：
            <select class="form-control" name="status">
				<option value="">默认</option>
                <?php if(is_array($status) || $status instanceof \think\Collection || $status instanceof \think\Paginator): $i = 0; $__LIST__ = $status;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                    <option value="<?php echo $key; ?>" <?php if(input('request.status') != '' && input('request.status') == $key): ?>selected<?php endif; ?>><?php echo $v; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
			</select>
            
			

            收货人：
            <input class="form-control" type="text" name="buyer_uid" style="width: 200px;" value="<?php echo input('request.buyer_uid'); ?>"
                   placeholder="请输入收货人姓名">
            用户ID：
            <input class="form-control" type="text" name="user_id" style="width: 200px;" value="<?php echo input('request.user_id'); ?>"
                   placeholder="请输入用户ID">

      

            订单提交时间：
			<input class="form-control js-bootstrap-date" name="start_time" id="start_time" autocomplete="off" value="<?php echo input('request.start_time'); ?>" aria-invalid="false" style="width: 110px;"> - 
            <input class="form-control js-bootstrap-date" name="end_time" id="end_time" autocomplete="off" value="<?php echo input('request.end_time'); ?>" aria-invalid="false" style="width: 110px;">
            
            订单编号：
            <input class="form-control" type="text" name="orderno" style="width: 200px;" value="<?php echo input('request.orderno'); ?>"
                   placeholder="请输入全部或部分订单编号">
   


            
			<input type="submit" class="btn btn-primary" value="搜索">
			<a class="btn btn-danger" href="<?php echo url('Order/index'); ?>">清空</a>
		</form>

		<form method="post" class="js-ajax-form">
			<table class="table table-hover table-bordered">
				<thead id="bignav">
					<tr>
						<th>ID</th>
						<th>订单编号</th>
						<th>用户ID</th>
						<th>收货人</th>
						<th>订单总金额</th>
						<th>商品总价</th>
						<th>订单状态</th>
						<th>支付状态</th>
						<th>支付方式</th>
						<th>物流方式</th>
						<th>物流单号</th>
						<th>下单时间</th>
						<th align="center"><?php echo lang('ACTIONS'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(is_array($lists) || $lists instanceof \think\Collection || $lists instanceof \think\Paginator): if( count($lists)==0 ) : echo "" ;else: foreach($lists as $key=>$vo): ?>
					<tr>
						<td><?php echo $vo['order_id']; ?></td>
						<td><?php echo $vo['order_sn']; ?></td>
						<td><?php echo $vo['user_id']; ?></td>
						<td><?php echo $vo['consignee']; ?></td>
						<td><?php echo $vo['total_amount']; ?></td>
						<td><?php echo $vo['goods_price']; ?></td>
						<td style="color: rgb(212, 106, 64);"><?php echo $status[$vo['order_status']]; ?></td>
						<td><?php if($vo['pay_status'] == 0): ?>未支付<?php else: ?>已支付<?php endif; ?></td>
						<td><?php echo $vo['pay_name']; ?></td>
						<td><?php echo $vo['shipping_name']; ?></td>
						<td><?php echo $vo['shipping_code']; ?></td>
						<td><?php echo date('Y-m-d H:i',$vo['add_time']); ?></td>
					
						
						<td>
                            
                            
                            <!--<?php if($vo['shipping_code'] != ''): ?>-->
                            <!--	<a class="btn btn-xs btn-primary" onclick="express(<?php echo $vo['order_id']; ?>)">物流信息</a>-->
                            <!--<?php endif; ?>-->
                            <a class="btn btn-xs btn-primary" href='<?php echo url("Order/info",array("id"=>$vo["order_id"])); ?>'>详情</a>
							
						</td>
					</tr>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</tbody>
			</table>
			<div class="pagination"><?php echo $page; ?></div>
		</form>
	</div>
	<script src="/static/js/admin.js"></script>
	<script type="text/javascript">
		$(function(){
			Wind.use('layer');
		});

		function express(orderid){
			layer.open({
			  type: 2,
			  title: '物流信息',
			  shadeClose: true,
			  shade: 0.8,
			  area: ['500px', '90%'],
			  content: '/Appapi/Express/index?user_type=platform&orderid='+orderid //iframe的url
			}); 
		}
	</script>

	
</body>
</html>