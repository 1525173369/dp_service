<?php /*a:2:{s:75:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/admin/shop_log/index.html";i:1628651707;s:68:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a >列表</a></li>
        <li><a href="<?php echo url('shop_log/manual'); ?>"><?php echo lang('ADD'); ?></a></li>
    </ul>
    <form class="well form-inline margin-top-20" method="post" action="<?php echo url('shop_log/index'); ?>">


        店铺ID：
        <input class="form-control" type="text" name="shop_id" style="width: 200px;" value="<?php echo input('request.shop_id'); ?>"
               placeholder="请输入店铺ID">

        <input type="submit" class="btn btn-primary" value="搜索">
    </form>
    <form method="post" class="js-ajax-form" >
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th>ID</th>
                <th>店铺ID / 名称</th>
                <th>Type</th>
                <th>Action</th>
                <th>变化的金币</th>
                <th>变化前金币 --- 变化后金币</th>
                <th>提交时间</th>
            </tr>
            </thead>
            <tbody>
            <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <tr>
                    <td><?php echo $vo['id']; ?></td>
                    <td><?php echo $vo['shop_id']; ?> / <?php echo $vo['shops']['name']; ?></td>
                    <td><?php echo $vo['type'] == 1?'收益':'支出'; ?></td>
                    <td><?php echo $vo['action']; ?></td>
                    <td><?php echo $vo['change_coin']; ?></td>

                    <td><?php echo $vo['before_coin']; ?> --- <?php echo $vo['after_coin']; ?></td>


                    <td><?php echo date('Y-m-d H:i',$vo['createtime']); ?></td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
        <div class="pagination"><?php echo $list->render(); ?></div>

    </form>
</div>
<script src="/static/js/admin.js"></script>
<script>
	 $(function(){
            Wind.use('layer');




    })


	</script>

</body>
</html>