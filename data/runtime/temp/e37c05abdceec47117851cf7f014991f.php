<?php /*a:2:{s:91:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/message/index.html";i:1625821661;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap">
	<ul class="nav nav-tabs">

	</ul>

	<form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('message/send'); ?>">
		<div class="radio">
			<label>
				<input type="radio" name="type" id="optionsRadios1" value="1" > 发送全部会员
			</label>
		</div>
		<div class="radio">
			<label>
				<input type="radio" name="type" id="optionsRadios2" value="0" checked>发送以下会员
			</label>
		</div>
		<div class="form-group">
			<label for="input-user_login" class="col-sm-2 control-label"><span class="form-required">*</span>会员列表</label>
			<div class="col-md-6 col-sm-10">

				<?php if(is_array($users) || $users instanceof \think\Collection || $users instanceof \think\Paginator): if( count($users)==0 ) : echo "" ;else: foreach($users as $key=>$vo): ?>
					<input type="hidden" name="user[]" value="<?php echo $vo['id']; ?>" />
					<p><span><?php echo $vo['id']; ?></span>&nbsp;<span><?php echo $vo['user_nickname']; ?></span></p>
				<?php endforeach; endif; else: echo "" ;endif; ?>

			</div>
		</div>


		<div class="form-group">
			<label for="input-content" class="col-sm-2 control-label"><span class="form-required"></span>消息标题:</label>
			<div class="col-md-6 col-sm-10">
				<input type="text" class="form-control" id="input-name" name="title" >
			</div>
		</div>


		<!--      <div class="form-group">-->
		<!--	<label for="input-content" class="col-sm-2 control-label"><span class="form-required"></span>消息描述:</label>-->
		<!--	<div class="col-md-6 col-sm-10">-->
		<!--		<textarea class="form-control" id="input-content" name="message"></textarea> -->
		<!--	</div>-->
		<!--</div>-->

		<div class="form-group">
			<label for="input-title" class="col-sm-2 control-label"><span class="form-required"></span>消息描述:</label><span class="form-required"></span>(注意：不支持推送图片和视频!!!)
			<div class="col-md-6 col-sm-12">
				<script type="text/plain" id="content" name="message"><?php echo (isset($template) && ($template !== '')?$template:""); ?></script>
				<!--<textarea class="textarea form-control" rows="5" name="message"></textarea>-->
			</div>
		</div>



		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-primary js-ajax-submit">发送</button>
			</div>
		</div>
	</form>
</div>
<script src="/static/js/admin.js"></script>
<script type="text/javascript">
    //编辑器路径定义
    var editorURL = GV.WEB_ROOT;
    </script>
<script type="text/javascript" src="/static/js/ueditor/ueditor.config.js"></script>
<script type="text/javascript" src="/static/js/ueditor/ueditor.all.min.js"></script>
<script type="text/javascript">
        var editorcontent = new baidu.editor.ui.Editor();
        editorcontent.render('content');
    </script>


</body>
</html>