<?php /*a:0:{}*/ ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vant@2.12/lib/index.css" />
    <style>
        html,
        body,
        div,
        span,
        ul,
        ol,
        form,
        header,
        nav,
        section,
        footer,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }

        img {
            vertical-align: top;
        }

        ul,
        ol {
            list-style: none;
        }

        input,
        button {
            border: 0;
            outline: none;
            box-sizing: border-box;
        }

        a,
        a:hover,
        a:focus {
            color: #333333;
            text-decoration: none;
            /* 针对移动端点击高亮 */
            -webkit-tap-highlight-color: transparent;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
            background-color: #f5f5f5;
            background-color: transparent;
            position: relative;
            /* background-color: #001b20; */
        }

        .clearfix::after {
            content: "";
            display: block;
            clear: both;
            height: 0;
            opacity: 0;
            overflow: hidden;
        }

        .fl {
            float: left;
        }

        .fr {
            float: right;
        }

        .content {
            padding: 0.5rem 0.5rem 1rem;
            background-color: #001b20;
            background-image: url(./image/bg.png);
            background-repeat: no-repeat;
            background-size: 100% 100%;
        }

        .user {
            width: 100%;
            margin-top: 1rem;
            margin-bottom: 0.8rem;
            display: flex;
        }

        .user-head {
            width: 2.5rem;
            height: 2.5rem;
            margin-right: 0.5rem;
            border-radius: 50%;
        }

        .user-head img {
            width: 100%;
            height: 100%;
            border-radius: 50%;
            object-fit: cover;
        }

        .user-information {
            width: 60%;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            align-items: flex-start;
        }

        .user-name {
            width: 10rem;
            font-size: 0.7rem;
            color: #fff;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .user-id {
            font-size: 0.6rem;
            /* color: #666; */
            color: #fff;
            zoom: 0.7;
        }

        /* .information-box {
            display: flex;
        }

        .user-gender {
            display: flex;
            margin-right: 0.3rem;
            justify-content: center;
            align-items: center;
            width: 1rem;
            height: 0.8rem;
            border-radius: 1rem;
            background-color: rgba(255, 255, 255, .5);
        }

        .gender-img {
            width: 0.8rem;
            height: 0.8rem;
        }

        .gender-img img {
            width: 100%;
            height: 100%;
        }

        .medal {
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 0.2rem;
            height: 0.8rem;
            border-radius: 1rem;
            background-color: rgba(255, 255, 255, .5);
        }

        .medal-img {
            width: 0.6rem;
            height: 0.6rem;
        }

        .medal-img img {
            width: 100%;
            height: 100%;
        }

        .medal-text {
            font-size: 0.6rem;
            zoom: 0.7;
            color: #fff;
        } */

        .introduce {
            font-size: 0.6rem;
            color: #e0e0e0;
            zoom: 0.9;
            margin-bottom: 0.8rem;
        }

        .user-data {
            /* width: 55%; */
            display: flex;
            align-items: center;
            color: #fff;
            justify-content: space-around;
        }

        .user-data .num {
            font-size: 0.6rem;
            text-align: center;
            margin-bottom: 0.1rem;
        }

        .user-data .text {
            font-size: 0.6rem;
            zoom: 0.8;
            color: #e0e0e0;
        }

        .publish-content {
            background-color: #fff;
            border-top-left-radius: 0.5rem;
            border-top-right-radius: 0.5rem;
            padding: 0.5rem 0;
            margin-top: -0.5rem;
        }

        .van-tabs--line .van-tabs__wrap {
            height: 1.7rem;
        }

        /* .van-tabs__nav--line{
            padding-bottom: 0.75rem;
        } */
        .van-tab--active {
            color: #323233 !important;
            font-size: 0.7rem !important;
        }

        .van-tab {
            color: #c1c1c1;
            font-size: 0.6rem;
            font-weight: bold;
        }

        .van-tabs__line {
            display: none;
        }

        .release {
            display: flex;
            /* justify-content: space-evenly; */
            padding: 0.2rem 0.8rem;
        }

        .release-content {
            width: 7rem;
            height: 9rem;
            border-radius: 0.6rem;
            overflow: hidden;
        }

        .release-content:nth-child(odd) {
            margin-right: 0.5rem;
        }

        .release-content img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
    </style>
</head>

<body>
    <div id="app">
        <div class="content">
            <div class="user">
                <div class="user-head">
                    <img src="https://sn.askpert.com/file_4144320210603_122628_2501550_android_.jpg">
                </div>
                <div class="user-information">
                    <div class="user-name">太可怜了我看下</div>
                    <div class="user-id">ID: 41443</div>
                    <!-- <div class="information-box">
                        <div class="user-gender">
                            <div class="gender-img">
                                <img src="./image/man.png">
                            </div>
                        </div>
                        <div class="medal">
                            <div class="medal-img">
                                <img src="./image/奖牌.png">
                            </div>
                            <div class="medal-text">金冠薯</div>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="introduce">小红书官方账号 | 一颗暴饮暴食却依然帅气的红薯，立志吃遍全世界！</div>
            <div class="user-data">
                <div class="follow">
                    <div class="num">3</div>
                    <div class="text">关注</div>
                </div>
                <div class="fans">
                    <div class="num">2</div>
                    <div class="text">粉丝</div>
                </div>
                <div class="up">
                    <div class="num">2</div>
                    <div class="text">点赞</div>
                </div>
                <div class="reading">
                    <div class="num">121</div>
                    <div class="text">阅读量</div>
                </div>
            </div>
        </div>
        <div class="publish-content">
            <van-tabs v-model="active">
                <van-tab title="TA的发布">
                    <div class="release">
                        <div class="release-content">
                            <img src="./image/头像.png">
                        </div>
                        <div class="release-content">
                            <img src="./image/bg.png">
                        </div>
                    </div>
                </van-tab>
                <van-tab title="TA的视频">
                    <div class="release">
                        <div class="release-content">
                            <img src="./image/头像.png">
                        </div>
                    </div>
                </van-tab>
                <van-tab title="TA的点评">
                    <div class="release">
                        <div class="release-content">
                            <img src="./image/bg.png">
                        </div>
                    </div>
                </van-tab>
            </van-tabs>
        </div>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6/dist/vue.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vant@2.12/lib/vant.min.js"></script>
<!-- 手机适配 -->
<script>
    (function (doc, win) {
        var docEl = doc.documentElement;
        var resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';
        var recalc = function () {
            var clientWidth = docEl.clientWidth;
            if (!clientWidth) return;
            if (clientWidth >= 640) clientWidth = 640;
            if (clientWidth <= 320) clientWidth = 320;
            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
        };
        if (!doc.addEventListener) return;
        win.addEventListener(resizeEvt, recalc, false);
        doc.addEventListener('DOMContentLoaded', recalc, false);
    })(document, window);
</script>

<script>
    new Vue({
        el: '#app',
        data() {
            return {
                // list: [],
                active: 0,
            };
        },
        methods: {

        },
    })
</script>

</html>