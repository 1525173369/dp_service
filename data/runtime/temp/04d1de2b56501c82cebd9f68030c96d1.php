<?php /*a:2:{s:73:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/admin/report/index.html";i:1628909568;s:68:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li class="active"><a >举报列表</a></li>

		</ul>
		<form class="well form-inline margin-top-20" method="post" action="<?php echo url('Report/index'); ?>">
 

			<!--关键字： -->
   <!--         <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"-->
   <!--                placeholder="请输入会员ID">-->
			<!--<input type="submit" class="btn btn-primary" value="搜索">-->
		</form>				
		<form method="post" class="js-ajax-form" >
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>ID</th>
						<th>举报人</th>						
						<th>被举报人</th>
						<th>举报标题</th>
						<th>举报内容</th>
						<th>举报图片</th>
						<th>被举报ID</th>
						<th>状态</th>
						<th>提交时间</th>
						<th><?php echo lang('ACTIONS'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
					<tr>
						<td><?php echo $vo['id']; ?></td>
						<td> <?php echo $vo['userinfo']['user_nickname']; ?>(<?php echo $vo['uid']; ?>)</td>	
						<td><?php echo $vo['touserinfo']['user_nickname']; ?> (<?php echo $vo['touid']; ?>)</td>		
						<td><?php echo nl2br($vo['description']);?></td>
						<td><?php echo nl2br($vo['content']);?></td>
						<td>
						    <?php if(is_array($vo['thumb']) || $vo['thumb'] instanceof \think\Collection || $vo['thumb'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['thumb'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
						       <img class="imgtip" src="<?php echo $v; ?>" style="max-width:100px;max-height:100px;">
                            <?php endforeach; endif; else: echo "" ;endif; ?>
						    
						</td>
						<?php 
						    if($vo['type'] == 0){
						      $str = '<label class="text-warning">店铺</label>';
						    }else if($vo['type'] == 1){
						      $str = '<label class="text-success">贴子</label>';
						    }else if($vo['type'] == 2){
						      $str = '<label class="text-danger">视频</label>';
						    }else{
						      $str = '<label class="text-default">用户</label>';
						      $vo['report_id'] = $vo['touid'];
						    }
						    
						 ?>
						<td><?php echo $str; ?> id:<?php echo $vo['report_id']; ?></td>
						<td><?php echo $status[$vo['status']]; ?></td>
						<td><?php echo date('Y-m-d H:i:s',$vo['add_time']); ?></td>						

						<td align="center">	
							<?php if($vo['status'] == '0'): ?>
                            <a class="btn btn-xs btn-info js-ajax-dialog-btn" href="<?php echo url('Report/setstatus',array('id'=>$vo['id'],'status'=>'1')); ?>" >标记处理</a>
                             <a class="btn btn-xs btn-info js-ajax-dialog-btn" href="<?php echo url('Report/setstatus',array('id'=>$vo['id'],'status'=>'2')); ?>" >不受理</a>
							<?php endif; ?>
	
                            
							<a class="btn btn-xs btn-danger js-ajax-delete" href="<?php echo url('Report/del',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a>
							
							<!--<a class="btn btn-xs btn-primary" href='<?php echo url("report/edit",array("id"=>$vo["id"])); ?>'>详情</a>-->
						</td>
					</tr>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</tbody>
			</table>
			<div class="pagination"><?php echo $page; ?></div>

		</form>
	</div>
	<script src="/static/js/admin.js"></script>
</body>
</html>