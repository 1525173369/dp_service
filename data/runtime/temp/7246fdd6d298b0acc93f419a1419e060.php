<?php /*a:2:{s:91:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/publish/index.html";i:1627461774;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style>
    .imgtip{
        margin-bottom:5px;
    }
</style>
</head>
<body>
	<div class="wrap js-check-wrap">
		<ul class="nav nav-tabs">
			<li class="active"><a >帖子列表</a></li>
			<li class=""><a href="<?php echo url('publish/add'); ?>">新增</a></li>
		</ul>
        
        <form class="well form-inline margin-top-20" method="post" action="<?php echo url('publish/index'); ?>">

            类别：
            <select class="form-control" name="type" style="width: 100px;">
                <option value=''>全部</option>
                <?php if(is_array($type) || $type instanceof \think\Collection || $type instanceof \think\Paginator): $i = 0; $__LIST__ = $type;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                    <option value="<?php echo $v['id']; ?>">
                    <?php echo $v['name']; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </select>

            用户ID：
            <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"
                   placeholder="用户ID">
            
            店铺名搜索：
            <input class="form-control" type="text" name="name" style="width: 200px;" value=""
                   placeholder="关联帖子">

            关键词：
            <input class="form-control" type="text" name="keyword" style="width: 200px;" value=""
                    placeholder="关键词">
            <input type="hidden" name="status" value="<?php echo $status2; ?>"/>
            <input type="hidden" name="isdel" value="<?php echo $isdel2; ?>"/>
            <input type="submit" class="btn btn-primary" value="搜索"/>
            <a class="btn btn-danger" href="<?php echo url('publish/index',array('status'=>$status2,'isdel'=>$isdel2)); ?>">清空</a>
        </form>
    
		<form method="post" class="js-ajax-form">
            <div class="table-actions">
                <button class="btn btn-danger btn-sm js-ajax-submit" type="submit" data-action="<?php echo url('publish/del'); ?>"
                        data-subcheck="true"><?php echo lang('DELETE'); ?>
                </button>
            </div>
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
                        <th width="16">
                            <label>
                                <input type="checkbox" class="js-check-all" data-direction="x" data-checklist="js-check-x">
                            </label>
                        </th>
						<th>ID</th>
						<th>店铺名字</th>
						<th>类型</th>
						<th>用户id/昵称</th>
						<th width="10%">标题</th>
						<th width="30%">图片</th>
                        <th width="30%">视频</th>
						<th>位置</th>
						<th>点赞数</th>
						<th>评论数</th>
                        <th><?php echo lang('阅读数'); ?></th>
						<th>状态</th>
						<th>提交时间</th>
						<th align="center"><?php echo lang('ACTIONS'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
					<tr>
                        <td>
                            <input type="checkbox" class="js-check" data-yid="js-check-y" data-xid="js-check-x" name="ids[]" value="<?php echo $vo['id']; ?>">
                        </td>
						<td><?php echo $vo['id']; ?></td>
						<td><?php echo $vo['sName']; ?></td>
                        <td><?php echo $vo['cName']; ?></td>
                        <td><?php echo $vo['uid']; ?>/<?php echo $vo['user_nickname']; ?></td>
                        <td><?php echo $vo['title']; ?></td>
						<td>
                            <?php if($vo['type'] == 0): if(is_array($vo['thumb']) || $vo['thumb'] instanceof \think\Collection || $vo['thumb'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['thumb'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                                <img class="imgtip" src="<?php echo $v; ?>" style="max-width:100px;max-height:100px;">
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                            <?php endif; if($vo['type'] == 2): if($vo['video_thumb']): ?><img class="imgtip" src="<?php echo $vo['video_thumb']; ?>" style="max-width:100px;max-height:100px;"><?php endif; ?>
                            <?php endif; ?>
                        
                        </td>
                        <td>
                            <?php if(is_array($vo['video']) || $vo['video'] instanceof \think\Collection || $vo['video'] instanceof \think\Paginator): $i = 0; $__LIST__ = $vo['video'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                                <video width="320" height="240" controls>
                                    <source src="<?php echo $v; ?>" type="video/mp4">
                                </video>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </td>
						<td><?php echo $vo['address']; ?></td>
						<td><?php echo $vo['likes']; ?></td>
			            <td><?php echo $vo['comments']; ?></td>
			            <td><?php echo $vo['reading']; ?></td>

                        <?php if($vo['isdel'] == '1'): ?>
                            <td>已下架</td>
                        <?php else: ?>
                            <td><?php echo $status[$vo['status']]; ?></td>
                        <?php endif; ?>
						
                        <td><?php echo date('Y-m-d H:i',$vo['addtime']); ?></td>
						<td>
                             <a class="btn btn-xs btn-primary" href='<?php echo url("publish/edit",array("id"=>$vo["id"])); ?>'>详情</a>
                            <?php if($vo['is_recommend'] == '0'): ?>
                            <a class="btn btn-xs btn-success js-ajax-dialog-btn" style="background:red;border:1px solid red;" href="<?php echo url('publish/is_recommend',array('id'=>$vo['id'],'is_recommend'=>1)); ?>">非推荐</a>
                            <?php endif; if($vo['is_recommend'] == '1'): ?>
                            <a class="btn btn-xs btn-success js-ajax-dialog-btn" href="<?php echo url('publish/is_recommend',array('id'=>$vo['id'],'is_recommend'=>0)); ?>">推荐中</a>
                            <?php endif; if($vo['is_top'] == '1'): ?>
                            <a class="btn btn-xs btn-success js-ajax-dialog-btn" href="<?php echo url('publish/is_top',array('id'=>$vo['id'],'is_top'=>0)); ?>">置顶中</a>
                            <?php endif; if($vo['is_top'] == '0'): ?>
                            <a class="btn btn-xs btn-danger js-ajax-dialog-btn" href="<?php echo url('publish/is_top',array('id'=>$vo['id'],'is_top'=>1)); ?>">非置顶</a>
                            <?php endif; if($vo['status'] == 0): ?>
                                <a class="btn btn-xs btn-success js-ajax-dialog-btn" href="<?php echo url('publish/setstatus',array('id'=>$vo['id'],'status'=>1)); ?>">同意</a>
                                <a class="btn btn-xs btn-danger js-ajax-dialog-btn" href="<?php echo url('publish/setstatus',array('id'=>$vo['id'],'status'=>-1)); ?>">拒绝</a>
                            <?php endif; if($vo['isdel'] == '0'): ?>
                            <a class="btn btn-xs btn-warning xiajia" data-id="<?php echo $vo['id']; ?>">下架</a>
                            <?php endif; if($vo['isdel'] == '1'): ?>
                            <a class="btn btn-xs btn-success js-ajax-dialog-btn" href="<?php echo url('publish/setDel',array('id'=>$vo['id'],'isdel'=>0,'reason'=>'')); ?>">上架</a>
                            <?php endif; if($vo['status'] == 1): ?>
                                <!-- <a class="btn btn-xs btn-danger js-ajax-dialog-btn" href="<?php echo url('publish/setstatus',array('id'=>$vo['id'],'status'=>-1)); ?>">拒绝</a> -->
                            <?php endif; if($vo['status'] == -1): ?>
                                <!-- <a class="btn btn-xs btn-success js-ajax-dialog-btn" href="<?php echo url('publish/setstatus',array('id'=>$vo['id'],'status'=>1)); ?>">同意</a> -->
                            <?php endif; ?>
                            
                            <!--<?php if($vo['status'] == 1 && $vo['isdel'] == 0): ?>-->
                            <!--<a class="btn btn-xs btn-info setrecom" data-id="<?php echo $vo['id']; ?>" data-recoms="<?php echo $vo['recommend_val']; ?>">设置推荐值</a>-->
                            <!--<?php endif; ?>-->
                            
                            <?php if($vo['status'] != 0): ?>
                            <a class="btn btn-xs btn-primary comment" data-id="<?php echo $vo['id']; ?>">查看评论</a>
                            <?php endif; ?>
                            <!-- <a class="btn btn-xs btn-primary" href='<?php echo url("publishcom/index",array("did"=>$vo["id"])); ?>'>查看评论</a> -->
                            
                            <a class="btn btn-xs btn-danger js-ajax-delete" href="<?php echo url('publish/del',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a>
						</td>
					</tr>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</tbody>
			</table>
			<div class="pagination"><?php echo $page; ?></div>

		</form>
	</div>
    <div id="enlarge_images" style="position:fixed;display:none;z-index:2;"></div>
	<script src="/static/js/admin.js"></script>
    <script>
        $(function(){
            
            Wind.use('layer');
            
            $('.xiajia').click(function(){
                var _this=$(this);
                var id=_this.data('id');
                
                layer.prompt({
                    formType: 2,
                    title: '请输入下架原因',
                    area: ['800px', '100px'] //自定义文本域宽高
                }, function(value, index, elem){
                    $.ajax({
                        url:'<?php echo url('publish/setDel'); ?>',
                        type:'POST',
                        data:{id:id,isdel:1,reason:value},
                        dataType:'json',
                        success:function(data){
                            var code=data.code;
                            if(code==0){
                                layer.msg(data.msg);
                                return !1;
                            }
                            layer.msg(data.msg,{},function(){
                                layer.closeAll();
                                reloadPage(window);
                            });
                            
                        },
                        error:function(){
                            layer.msg('操作失败，请重试')
                        }
                    });
                    
                });
                
            })
            
            $('.setrecom').click(function(){
                var _this=$(this);
                var id=_this.data('id');
                var recoms=_this.data('recoms');
                
                layer.prompt({
                    formType: 0,
                    title: '推荐值',
                    value: recoms,
                    area: ['800px', '100px'] //自定义文本域宽高
                }, function(value, index, elem){
                    $.ajax({
                        url:'<?php echo url('publish/setrecom'); ?>',
                        type:'POST',
                        data:{id:id,recoms:value},
                        dataType:'json',
                        success:function(data){
                            var code=data.code;
                            if(code==0){
                                layer.msg(data.msg);
                                return !1;
                            }
                            layer.msg(data.msg,{},function(){
                                layer.closeAll();
                                reloadPage(window);
                            });
                            
                        },
                        error:function(){
                            layer.msg('操作失败，请重试')
                        }
                    });
                    
                });
                
            })
            
            $('.comment').click(function(){
                var _this=$(this);
                var id=_this.data('id');

                layer.open({
                    type: 2,
                    title: '查看评论',
                    shadeClose: true,
                    shade: 0.8,
                    area: ['80%', '100%'],
                    content: '/admin/comment/index?dynamicid='+id
                }); 
                
            });
            
            $('.view').click(function(){
                var _this=$(this);
                var id=_this.data('id');

                layer.open({
                    type: 2,
                    title: '查看',
                    shadeClose: true,
                    shade: 0.8,
                    area: ['500px', '100%'],
                    content: '/admin/publish/see?id='+id
                }); 
                
            });
        })
    </script>
</body>
</html>