<?php /*a:1:{s:68:"/var/www/ap1.askpert.com/themes/default/portal/page/get_dynamic.html";i:1631788079;}*/ ?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>中医中药_药食同源_薄荷</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />

    <meta property="og:title" content="中医中药_药食同源_薄荷">
    <meta property="og:description" content="相信大家在生活中见到/用到含有薄荷(Mint)的日用品是非常熟悉了。
炎炎长夏，用含薄荷的洗发露、沐浴露洗澡，出来时一身清爽，感觉轻了几斤似的。长时间开车犯困，来一颗薄荷糖，立马醒神精神抖擞。
薄荷是什么？

🙋🏻‍♀️来听有关薄荷的希腊神话故事：
话说，冥界之王海地斯（Hades）是拥有一副黝黑身躯的凶猛之神，
某日，这神到地面上闲逛，突然看到美丽 的精灵曼西(Menthe)，
便深深地爱上了她，但这件事却被妻子波塞风妮(Persephone)发现了。
波塞风妮想要对自己所受的侮辱讨回公道，
于是运用法术把曼西变成无用的杂草🌿；
而曼西虽然变成了草，却依然美丽如昔，
不失原本艳丽的姿态与芳香，
这个出现在希腊神话中的植物，便是我们所谓的欧薄荷(Mentha)。 
薄荷的传说给世人以警示，爱上不该爱的人，很可能带来毁灭性的后果。
所以希腊人有一种说法，放两片叶子在眼中间，可以让人头脑清醒，保持眼清目明。
薄荷是香草植物中的大家族中品种众多的一族。在很多国家的饮食文化里，薄荷简直是烹饪界的福星。薄荷一下，马上提升佳肴的质量。过百的品种，这里为大家带出几种比较常见的薄荷。
同时也分类出【可食用】和【不可食用】的薄荷种类供参考。
【食用薄荷：】
留兰香薄荷- 最知名、普遍的品种。中药用为此品种。
胡椒薄荷- 用来泡茶、烹饪。
柠檬薄荷- 被用制作柠檬的茶饮，可入药。
苹果薄荷- 用来泡茶、凉拌沙拉、制作果冻、餐品装饰物。
巧克力薄荷- 用来泡茶、凉拌沙拉、制作甜点、烹调或咖啡、。
香槟薄荷- 用来泡茶、凉拌沙拉。

【不能食用的薄荷：】
普列薄荷- 专门用来提炼薄荷脑。
猫薄荷- 猫食用后会引起暂时性的行为变化，引起幻觉。
日本薄荷- 樟脑元素过多，常用作防潮、驱蚊虫。
【顶着”薄荷“的名字，但它不是薄荷：】
科西嘉薄荷- 少用于烹饪，不能生吃，最出名是作为薄荷甜酒的调料。

【马来西亚常见：】
绿薄荷（民间称：到手香/左手香）- 用来泡茶、入药。
注意！绿薄荷“气”属于温，和其他薄荷是完全相反的。
所以用法是针对寒证。

🔔从中医基础理解，薄荷的气属凉，味属辛。
📕复习一下，辛味能发散、能行气；凉性能治疗热证，使体内恢复平衡。
⭕因热头痛、感冒、发热、眼睛红、喉咙肿痛，在轻微程度上，薄荷茶可以帮助你减轻不适。另外，有没有发现，有一部分人特别喜欢有事没事就泡薄荷茶喝，说，喝了身体比较舒服~ 难道意味着他们体内有多余的热，薄荷散热所以他们才感到舒服？❌不是的。薄荷的辛，能行气，推动体内气之循环，特别是”肝气”。肝气不顺畅的人，有一个共同点，容易郁闷、闷闷不乐、胡思乱想、胸闷、胁肋(身体两侧腋下的”排骨“)容易胀痛不舒服。当他们服用了一些能行肝气的食物/药，气通则气顺，郁闷自然减轻。
薄荷茶是人人都能喝的保健茶？
不是。长期摄入辛凉容易伤阳气。
凡事要有度，所以建议不要每天薄荷茶当水喝。

中医师提醒：
因为肺气弱而自汗(静静坐着也汗出）的人，不适合用薄荷。
如果你每天都喝薄荷茶，不喝不爽，那也建议你尽快寻求中医诊断，以尽早获得治疗。

以上资料仅供参考。不作为诊断依据。
病从浅中医。如果你也遇到类似问题，不妨联络中医师为你诊断。

">
 
    <meta property="og:image" itemprop="image" content="https://static-ap.askpert.com/qiniu_move/file_4150320210629_231544_4257379_android_.jpg?imageView2/2/w/800/h/800">

    <meta property="og:video" content="">
    <meta property="og:type" content="video">
    <meta property="og:source" content="Askpert Review">
    <meta property="og:url" content="https://ap1.askpert.com/portal/page/getDynamic?uid=41503&id=4077&lan=en">

    <script src="/app/static/js/common/lib-flex/flexible.js"></script>
    <link rel="stylesheet" href="/app/static/css/common/reset.css">
    <link rel="stylesheet" href="/app/static/css/mui.min.css">
    <link rel="stylesheet" href="/app/static/css/share/index.min.css">
    <style>
        .mui-toast-container {
            bottom: 50%;
            /* opacity: 0.6; */
            opacity: 0.6;
            color: #fff;
            width: 180px;
            line-height: 50px;
            padding: 70px 5px 10px 5px;
        }

        .mui-card-footer:before,
        .mui-card-header:after {
            content: none !important;
        }

        /*----------------mui.showLoading---------------*/
        .mui-show-loading {
            position: fixed;
            padding: 5px;
            width: 120px;
            min-height: 120px;
            top: 45%;
            left: 50%;
            margin-left: -60px;
            background: rgba(0, 0, 0, 0.6);
            text-align: center;
            border-radius: 5px;
            color: #FFFFFF;
            visibility: hidden;
            margin: 0;
            z-index: 2000;

            -webkit-transition-duration: .2s;
            transition-duration: .2s;
            opacity: 0;
            -webkit-transform: scale(0.9) translate(-50%, -50%);
            transform: scale(0.9) translate(-50%, -50%);
            -webkit-transform-origin: 0 0;
            transform-origin: 0 0;
        }

        .mui-show-loading.loading-visible {
            opacity: 1;
            visibility: visible;
            -webkit-transform: scale(1) translate(-50%, -50%);
            transform: scale(1) translate(-50%, -50%);
        }

        .mui-show-loading .mui-spinner {
            margin-top: 24px;
            width: 36px;
            height: 36px;
        }

        .mui-show-loading .text {
            line-height: 1.6;
            font-family: -apple-system-font, "Helvetica Neue", sans-serif;
            font-size: 14px;
            margin: 10px 0 0;
            color: #fff;
        }

        .mui-show-loading-mask {
            position: fixed;
            z-index: 1000;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
        }

        .mui-show-loading-mask-hidden {
            display: none !important;
        }

        .shop-score {
            height: 5vh;
            width: 75%;
            margin-left: 30%;
            margin-top: 2%;
            /* position: relative; */
        }

        .shop-score .score-hide {
            overflow: hidden;
            width: 100%;
            position: relative;
        }

        .shop-score .score-hide img {
            float: left;
            width: 5vw;
            height: 5vw;
            margin-top: 10%;
        }

        .shop-score .score-show {
            overflow: hidden;
            width: 100%;
            position: absolute;
            left: 0;
            top: 0;
        }

        .shop-score .score-show img {
            float: left;
            width: 5vw;
            height: 5vw;
            margin-top: 10%;
        }
    </style>
</head>

<body>
<div id="app" v-on:click="url($event)">
    <div class="mui-card">
        <!--页眉，放置标题-->
        <div class="mui-card-header mui-card-media">
            <img :src="note.avatar" />
            <div class="mui-media-body" style="font-size: 0.5rem; border-bottom: 1px solid #ddd;">
                {{note.user_nickname}}
                <p style="font-size: 0.3rem;">{{text[lan].box7}} {{note.addtime | format}}</p>
            </div>
        </div>
        <!--内容区-->
        <div class="mui-card-content">
            <!-- <img v-if="noteImg[0]" :src="noteImg[0]" /> -->
            <div class="mui-slider">
                <div class="mui-slider-group" onClick="event.cancelBubble = true">
                    <!--第一个内容区容器-->
                    <div v-if="noteImg.length != 0" class="mui-slider-item" v-for="(item, index) in noteImg" :key="index">
                        <img :src="item">
                    </div>
                    <div v-if="note.video.length != 0" class="mui-slider-item" v-for="(item, index) in note.video" :key="index">
                        <video class="width" controls="controls" :src="item"></video>
                    </div>
                </div>
            </div>

            <div class="parameter">
                <div class="start">
                    <div class="grey-img1" v-for="(item,index) in start2" :key="index">
                        <img :src="item">
                    </div>
                    <span>{{note.points}}</span>
                    <div class="start1">
                        <div v-if="note.points >= 1.0" class="grey-img1">
                            <img src="/app/static/images/start1.png">
                        </div>
                        <div v-if="note.points >= 2.0" class="grey-img1">
                            <img src="/app/static/images/start1.png">
                        </div>
                        <div v-if="note.points >= 3.0" class="grey-img1">
                            <img src="/app/static/images/start1.png">
                        </div>
                        <div v-if="note.points >= 4.0" class="grey-img1">
                            <img src="/app/static/images/start1.png">
                        </div>
                        <div v-if="note.points >= 5.0" class="grey-img1">
                            <img src="/app/static/images/start1.png">
                        </div>
                    </div>
                </div>
                <div class="score">
                    <div class="heart">
                        <img src="/app/static/images/heart.png">
                        <span>{{note.likes}}</span>
                    </div>
                    <div class="eye">
                        <img src="/app/static/images/eye.png">
                        <span>{{note.reading}}</span>
                    </div>
                </div>
            </div>
            <div class="topic" v-if="note.topic">
                <div v-for="(item,index) in note.topic" :key="index">
                    #{{item.topic_name}}
                </div>
            </div>
            <div class="title">
                {{note.title}}
            </div>
           
            <el-row>
                <el-col style="white-space: pre-wrap;">{{note.content}}</el-col>
            </el-row>
            <div v-if="note.shopId != null" class="shop mui-card-header mui-card-media">
                <img style="border-radius: 0.2rem;width: 2rem;height: 2rem;" :src="note.shopThumb" />
                <div class="mui-media-body" style="height: 2rem;line-height: 1rem;font-size: 0.4rem;margin-left: 2.3rem;">
                    <div class="shop-title">
                        <span>{{note.shopName}}</span>
                        <img v-if="note.shopClaim == 1" src="/app/static/images/renzhen.png">
                        <img v-else src="/app/static/images/weirenzhen.png">
                    </div>
                    <div class="start">
                        <div class="grey-img1" v-for="(item,index) in start2" :key="index">
                            <img :src="item">
                        </div>
                        <span>{{note.shopPoints}}</span>
                        <div class="start1">
                            <div v-if="note.points >= 1.0" class="grey-img1">
                                <img src="/app/static/images/start1.png">
                            </div>
                            <div v-if="note.points >= 2.0" class="grey-img1">
                                <img src="/app/static/images/start1.png">
                            </div>
                            <div v-if="note.points >= 3.0" class="grey-img1">
                                <img src="/app/static/images/start1.png">
                            </div>
                            <div v-if="note.points >= 4.0" class="grey-img1">
                                <img src="/app/static/images/start1.png">
                            </div>
                            <div v-if="note.points >= 5.0" class="grey-img1">
                                <img src="/app/static/images/start1.png">
                            </div>
                        </div>
                        <span>| {{note.comments}} {{text[lan].box5}}</span>
                    </div>
                </div>
            </div>
        </div>
        <!--页脚，放置补充信息或支持的操作-->
        <div class="mui-card-footer">
            {{text[lan].box4}}({{count}})
        </div>
    </div>
    <div class="comment" v-if="noteComment.length !=0">

        <div class="comment-item" v-for="(item,index) in noteComment" :key="index">

            <div class="list mui-card-header mui-card-media">
                <img :src="item.avatar" />
                <div class="mui-media-body">
                    {{item.username == ''? 	text[lan].box6 : item.username}}
                    <div class="text">
                        <img class="heart" src="/app/static/images/heart.png" style="width: 0.4rem;height: 0.4rem;">
                        <span>{{item.zan_num}}</span>
                    </div>
                    <p>{{item.content}} {{text[lan].box7}} {{item.create_time}}</p>
                </div>
            </div>

        </div>

    </div>
    <div  v-if="noteComment == 0" class="wu">
        <img src="/app/static/images/zanwuxinxi.png">
        {{text[lan].box8}}
    </div>
    <script src="/app/static/js/common/mui.min.js"></script>
    <script src="/app/static/js/common/jquery-1.10.1.min.js"></script>
    <script src="/app/static/js/common/vue.js"></script>
    <script>
        //扩展mui.showLoading
        (function ($, window) {
            //显示加载框
            $.showLoading = function (message, type) {
                if ($.os.plus && type !== 'div') {
                    $.plusReady(function () {
                        plus.nativeUI.showWaiting(message);
                    });
                } else {
                    var html = '';
                    html += '<i class="mui-spinner mui-spinner-white"></i>';
                    html += '<p class="text">' + (message || "数据加载中") + '</p>';

                    //遮罩层
                    var mask = document.getElementsByClassName("mui-show-loading-mask");
                    if (mask.length == 0) {
                        mask = document.createElement('div');
                        mask.classList.add("mui-show-loading-mask");
                        document.body.appendChild(mask);
                        mask.addEventListener("touchmove", function (e) {
                            e.stopPropagation();
                            e.preventDefault();
                        });
                    } else {
                        mask[0].classList.remove("mui-show-loading-mask-hidden");
                    }
                    //加载框
                    var toast = document.getElementsByClassName("mui-show-loading");
                    if (toast.length == 0) {
                        toast = document.createElement('div');
                        toast.classList.add("mui-show-loading");
                        toast.classList.add('loading-visible');
                        document.body.appendChild(toast);
                        toast.innerHTML = html;
                        toast.addEventListener("touchmove", function (e) {
                            e.stopPropagation();
                            e.preventDefault();
                        });
                    } else {
                        toast[0].innerHTML = html;
                        toast[0].classList.add("loading-visible");
                    }
                }
            };

            //隐藏加载框
            $.hideLoading = function (callback) {
                if ($.os.plus) {
                    $.plusReady(function () {
                        plus.nativeUI.closeWaiting();
                    });
                }
                var mask = document.getElementsByClassName("mui-show-loading-mask");
                var toast = document.getElementsByClassName("mui-show-loading");
                if (mask.length > 0) {
                    mask[0].classList.add("mui-show-loading-mask-hidden");
                }
                if (toast.length > 0) {
                    toast[0].classList.remove("loading-visible");
                    callback && callback();
                }
            }
        })(mui, window);

        var host = window.location.host == '127.0.0.1:5500' ? 'http://dp.zwrjkf.cn' : 'https://' + window.location.host;

        // var host = 'https://ap1.askpert.com'

        var app = new Vue({
            el: '#app',
            data: {
                uid: '',
                id: '',
                p: 1,
                note: {}, //帖子详情
                noteComment: '', //评论详情
                noteImg: [], //内容图片
                count: 0,
                // 			classObject:{
                // 				active: true,
                // 'text-danger': false
                // 			},
                start1: [
                    '/app/static/images/start1.png',
                    '/app/static/images/start1.png',
                    '/app/static/images/start1.png',
                    '/app/static/images/start1.png',
                    '/app/static/images/start1.png',
                ],
                start2: [
                    '/app/static/images/start2.png',
                    '/app/static/images/start2.png',
                    '/app/static/images/start2.png',
                    '/app/static/images/start2.png',
                    '/app/static/images/start2.png',
                ],
                text: {
                    'gb': {
                        'box1': '分享',
                        'box2': '正在加载...',
                        'box3': '访问错误',
                        'box4': '评论',
                        'box5': '人',
                        'box6': '游客',
                        'box7': '发表于',
                        'box8': '暂无评论',
                    },
                    'en': {
                        'box1': 'Share',
                        'box2': 'Loading...',
                        'box3': 'Access error',
                        'box4': 'comment',
                        'box5': 'people',
                        'box6': 'tourist',
                        'box7': 'Published in',
                        'box8': 'No comment',
                    },
                    'ms': {
                        'box1': 'Kongsi',
                        'box2': 'Memuatkan',
                        'box3': 'Ralat akses',
                        'box4': 'Komen',
                        'box5': 'people',
                        'box6': 'pelancong',
                        'box7': 'Dipublik dalam',
                        'box8': 'Tiada komen',
                    }
                },
                lan: ''
            },
            created() {
                this.init()
                this.getDetailsNote().then(res => {
                    console.log(res)
                    this.note = res[0].details
                    this.noteImg = res[0].details.thumb
                    this.topic = res[0].details.topic
                    console.log(this.noteImg[0])
                    console.log(this.topic[0].topic_name)
                    this.setStart()
                })
                this.getNoteComment().then(res => {
                    console.log(res);
                    this.noteComment = res.data.info[0].list
                    console.log(this.noteComment);
                    this.count = res.data.info[0].count
                })
            },

            mounted() {
                
                
            },
            updated: function() {
                this.$nextTick(()=>{
                    var gallery = mui('.mui-slider');
                    gallery.slider();
                })
            },

            methods: {
                //-------------------------网络请求相关-----------------------------------
                //获取帖子详情
                getDetailsNote() {

                    return new Promise((resolve, reject) => {
                        mui.showLoading(this.text[this.lan].box2, "div"); //加载文字和类型，plus环境中类型为div时强制以div方式显示
                        $.ajax(host + '/appapi/?service=publish.detailsNote', {
                            data: {
                                uid: this.uid,
                                id: this.id,
                                token:"askpert"
                                // id: 1,
                            },
                            dataType: 'json', //服务器返回json格式数据
                            type: 'get', //HTTP请求类型
                            success: function (data) {

                                mui.hideLoading(); //隐藏后的回调函数
                                if (data.data.code !== 0) {
                                    mui.toast('请求失败', {
                                        duration: 'long',
                                        type: 'div'
                                    })
                                } else if (!data.data.msg) {

                                    return resolve(data.data.info)
                                } else {
                                    mui.toast(data.data.msg, {
                                        duration: 'long',
                                        type: 'div'
                                    })
                                }
                            },
                            error: function (xhr, type, errorThrown) {
                                //异常处理；
                                console.log(errorThrown);
                            }
                        });
                    });
                },
                //获取帖子评论
                getNoteComment() {
                    return new Promise((resolve, reject) => {
                        mui.showLoading(this.text[this.lan].box2, "div"); //加载文字和类型，plus环境中类型为div时强制以div方式显示
                        $.ajax(host + '/appapi/?service=comment.getNoteComment', {
                            data: {
                                id: this.id,
                                p: this.p,
                                uid: this.uid
                            },
                            dataType: 'json', //服务器返回json格式数据
                            type: 'get', //HTTP请求类型
                            success: function (data) {
                                mui.hideLoading(); //隐藏后的回调函数
                                if (data.data.code !== 0) {
                                    mui.toast(this.text[this.lan].box3, {
                                        duration: 'long',
                                        type: 'div'
                                    })
                                } else {
                                    return resolve(data)
                                }
                            },
                            error: function (xhr, type, errorThrown) {
                                //异常处理；
                                console.log(errorThrown);
                            }
                        });
                    });
                },

                //-------------------------网络请求相关结束-----------------------------------
                //-------------------------点击事件相关-----------------------------------
                //初始化
                init() {
                    this.uid = this.getQueryString('uid')
                    this.token = this.getQueryString('token')
                    this.id = this.getQueryString('id')
                    this.lan = this.getQueryString('lan')
                    if(this.lan == 'undefined'){
                        this.lan = 'ms';
                    }
                    console.log(this.lan)
                    //document.title = this.text[this.lan].box1
                    mui.init()
                },
                getQueryString(name) {
                    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
                    var r = window.location.search.substr(1).match(reg);
                    if (r != null)
                        return unescape(r[2]);
                    return 'undefined';
                },
                //设置样式
                setStart() {
                    if (this.note.points == '1.0') {
                        this.isShow = true
                    } else if (this.note.points == '2.0') {
                        this.isShow = true
                        this.isShow1 = true
                    } else if (this.note.points == '3.0') {
                        this.isShow = true
                        this.isShow1 = true
                        this.isShow2 = true
                    } else if (this.note.points == '4.0') {
                        this.isShow = true
                        this.isShow1 = true
                        this.isShow2 = true
                        this.isShow3 = true
                    } else {
                        this.isShow = true
                        this.isShow1 = true
                        this.isShow2 = true
                        this.isShow3 = true
                        this.isShow4 = true
                    }
                },
                getQueryString(name) {
                    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
                    var r = window.location.search.substr(1).match(reg);
                    if (r != null)
                        return unescape(r[2]);
                    return 'undefined';
                },
                //点击跳转
                url(event) {
                    var el = event.currentTarget;
                    // window.location.href= host + https://ap1.askpert.com/appapi/down/index
                    window.location.href = host + '/appapi/down/index'
                }
                //-------------------------点击事件相关结束-----------------------------------

            },
            filters: {
                format(data, fmt) {
                    if (!data) return "";
                    var timeStr = new Date(parseInt(data * 1000));
                    var fmt = fmt || "yyyy-MM-dd hh:mm:ss";
                    var o = {
                        "M+": timeStr.getMonth() + 1, //月份
                        "d+": timeStr.getDate(), //日
                        "h+": timeStr.getHours(), //小时
                        "m+": timeStr.getMinutes(), //分
                        "s+": timeStr.getSeconds(), //秒
                        "q+": Math.floor((timeStr.getMonth() + 3) / 3), //季度
                        "S": timeStr.getMilliseconds() //毫秒
                    };
                    // 如果 fmt 中有y,fmt中y替换为timeStr.getFullYear()，例：
                    // yyyy-MM-dd hh:mm:ss 替换为 2018-MM-dd hh:mm:ss
                    // yy-MM-dd hh:mm:ss 替换为 18-MM-dd hh:mm:ss
                    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (timeStr.getFullYear() + "").substr(4 - RegExp.$1
                        .length));
                    // 下面循环原理同上
                    for (var k in o)
                        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) :
                            (("00" + o[k]).substr(("" + o[k]).length)));
                    return fmt;
                }
            }
        })
    </script>
</body>

</html>