<?php /*a:2:{s:89:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/publish/add.html";i:1623124595;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<script type="text/html" id="photos-item-tpl">
	<li id="saved-image{id}">
        <input id="photo-{id}" type="hidden" name="video[]" value="{filepath}">
		<video  id="photo-{id}-preview" width="320" height="240" controls>
			<source src="{url}" type="video/mp4">
		</video>	 
        <a href="javascript:uploadMultiFile('视频上传','#photo-{id}');">替换</a>
        <a href="javascript:(function(){$('#saved-image{id}').remove();})();">移除</a>
    </li>
</script>
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li><a href="<?php echo url('publish/index'); ?>">帖子列表</a></li>
			<li class="active"><a href="<?php echo url('publish/add'); ?>">新增</a></li>
		</ul>
		<form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('publish/addPost'); ?>">
			<div class="form-group">
				<label for="input-ipa_ewm" class="col-sm-2 control-label">发帖人ID</label>
				<div class="col-md-3 col-sm-10">
					<input type="text" class="form-control" id="uid" name="uid" placeholder="请输入发帖人ID" readonly value="">
					<span class="help-block">
						<a onclick="selectGoods()"><i class="fa fa-search"></i>选择</a>
					</span>
				</div>
			</div>
			<div class="form-group">
				<label for="input-title" class="col-sm-2 control-label">帖子标题<span class="form-required">*</span></label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-title" name="title">
				</div>
			</div>
			<div class="form-group">
				<label for="input-content" class="col-sm-2 control-label">帖子内容</label>
				<div class="col-md-6 col-sm-10">
					<textarea class="form-control" name="content" id="input-content"></textarea>
				</div>
			</div>
			<div class="form-group">
				<label for="input-shop_id" class="col-sm-2 control-label">帖子所属店铺</label>
				<div class="col-md-3 col-sm-10">
					<input type="text" class="form-control" id="shop_id" name="shop_id" placeholder="店铺ID" readonly value="">
					<span class="help-block">
						<a onclick="selectShop()"><i class="fa fa-search"></i>选择</a>
					</span>
				</div>
			</div>
			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>话题</label>
				<div class="col-md-6 col-sm-10" style="height:500px;overflow-y:auto;border:1px solid #eeeeee;border-radius: 10px;">
					<?php if(is_array($topicData) || $topicData instanceof \think\Collection || $topicData instanceof \think\Paginator): $i = 0; $__LIST__ = $topicData;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
						<label class="checkbox-inline">
							<input type="checkbox" id="inlineCheckbox1" value="<?php echo $v['topic_id']; ?>" name="custom[]"> <?php echo $v['topic_name']; ?>
						</label>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</div>
			</div>
			<div class="form-group">
				<label for="input-user_login" class="col-sm-2 control-label">视频内容图集</label>
				<div class="col-md-6 col-sm-10">
					<tr>
						<td>
							<ul id="photos" class="pic-list list-unstyled form-inline"></ul>
							<a href="javascript:uploadMultiFile('视频上传','#photos','photos-item-tpl');" class="btn btn-default btn-sm">选择视频</a>
						</td>
					</tr>
				</div>
			</div>
			<div class="form-group">
				<label for="input-user_login" class="col-sm-2 control-label">帖子图片一</label>
				<div class="col-md-6 col-sm-10">
					<input type="hidden" name="thumb[]" id="thumbnail1" value="">
					<a href="javascript:uploadOneImage('图片上传','#thumbnail1');">
						<img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png" id="thumbnail1-preview" style="cursor: pointer;max-width:100px;max-height:100px;" />
					</a>
					<input type="button" class="btn btn-sm btn-cancel-thumbnail1" value="取消图片">
				</div>
			</div>
			<div class="form-group">
				<label for="input-user_login" class="col-sm-2 control-label">帖子图片二</label>
				<div class="col-md-6 col-sm-10">
					<input type="hidden" name="thumb[]" id="thumbnail2" value="">
					<a href="javascript:uploadOneImage('图片上传','#thumbnail2');">
						<img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png" id="thumbnail2-preview" style="cursor: pointer;max-width:100px;max-height:100px;" />
					</a>
					<input type="button" class="btn btn-sm btn-cancel-thumbnail2" value="取消图片">
				</div>
			</div>
			<div class="form-group">
				<label for="input-user_login" class="col-sm-2 control-label">帖子图片三</label>
				<div class="col-md-6 col-sm-10">
					<input type="hidden" name="thumb[]" id="thumbnail3" value="">
					<a href="javascript:uploadOneImage('图片上传','#thumbnail3');">
						<img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png" id="thumbnail3-preview" style="cursor: pointer;max-width:100px;max-height:100px;" />
					</a>
					<input type="button" class="btn btn-sm btn-cancel-thumbnail3" value="取消图片">
				</div>
			</div>
			<div class="form-group">
				<label for="input-user_login" class="col-sm-2 control-label">帖子图片四</label>
				<div class="col-md-6 col-sm-10">
					<input type="hidden" name="thumb[]" id="thumbnail4" value="">
					<a href="javascript:uploadOneImage('图片上传','#thumbnail4');">
						<img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png" id="thumbnail4-preview" style="cursor: pointer;max-width:100px;max-height:100px;" />
					</a>
					<input type="button" class="btn btn-sm btn-cancel-thumbnail4" value="取消图片">
				</div>
			</div>
			<div class="form-group">
				<label for="input-description" class="col-sm-2 control-label">选择分类</label>
				<div class="col-md-6 col-sm-10">
					<select class="form-control" name="cat_id">
						<?php if(is_array($class) || $class instanceof \think\Collection || $class instanceof \think\Paginator): $i = 0; $__LIST__ = $class;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
							<option value="<?php echo $v['id']; ?>"><?php echo $v['name']; ?></option>
						<?php endforeach; endif; else: echo "" ;endif; ?>
					</select>

				</div>
			</div>
			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>省(洲)/市</label>
				<div class="row">
					<div class="col-sm-2">
						<input type="text" class="form-control" name="state" placeholder="洲">
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control" name="city" placeholder="市">
					</div>
				</div>
			</div>
			<div class="box-map" id="googleMap" style="width:600px;height:500px;margin-left:20%;">谷歌地图正在加载中......，如果您没有爬上围墙，还是关闭此网页吧！
			</div>


			<div class="form-group">
				<label for="input-ipa_ewm" class="col-sm-2 control-label">请输入地点</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="address" name="address">
					<span class="help-block">
						<a id="search"><i class="fa fa-search"></i>搜索</a>
					</span>
				</div>
			</div>
			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>经纬度</label>
				<div class="row">
					<div class="col-sm-2">
						<input type="text" class="form-control" id="lat" name="lat" placeholder="纬度">
					</div>
					<div class="col-sm-3">
						<input type="text" class="form-control" id="lng" name="lng" placeholder="经度">
					</div>
				</div>
			</div>


			<script type="text/javascript">
				var lat = -33.8688197,
					lng = 151.2092955,
					address = '悉尼';
				var map, mapProp, chicago, marker, infowindow;
				var keys = 'AIzaSyATCKWBnPvW_cyuUexKqiJsjw-lFu389mA';
				var mapid = document.getElementById("googleMap");

				function initMap(callback) {
					chicago = {
						lat: lat,
						lng: lng
					};
					mapProp = {
						center: chicago,
						zoom: 15,
						gestureHandling: 'greedy'
					};
					map = new google.maps.Map(mapid, mapProp);
					//添加标记
					marker = new google.maps.Marker({
						//添加标记
						position: mapProp.center
						//自定义图标 icon:'pinkball.png'
					});
					marker.setMap(map);
					infowindow = new google.maps.InfoWindow({
						content: address
					});
					infowindow.open(map, marker);
				};
				window.onload = function() {
					var gooMapJs = 'https://maps.googleapis.com/maps/api/js?key=' + keys + '&callback=initMap&sensor=false';
					//谷歌地图
					var script = document.createElement('script');
					script.src = gooMapJs;
					document.body.appendChild(script);
					script.onload = function() {
						map.addListener('click', function(e) {
							placeMarkerAndPanTo(e.latLng, map);
						});

						function placeMarkerAndPanTo(latLng, map) {
							console.log(latLng);
							/*var marker = new google.maps.Marker({ position: latLng, map: map});map.panTo(latLng);*/
						};
					};
					script.onerror = function() {
						if (typeof google === 'undefined') {
							$(mapid).html('因地域限制，google地图无法显示');
						}
					};

				};

				$('#search').click(function() {
					var address = $('#address').val();
					if (!address) return;
					var newuri = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + address + '&key=' + keys;
					$.ajax({
						url: newuri,
						success: function(data) {
							if (data.status == "OK") {
								lat = data.results[0].geometry.location.lat;
								lng = data.results[0].geometry.location.lng;
								$('#lat').val(lat);
								$('#lng').val(lng);
								initMap();
								var infowindow = new google.maps.InfoWindow({
									content: address
								});
								infowindow.open(map, marker);
							} else {
								console.log(data.error_message);
							}
						}
					});
				});
			</script>

			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary js-ajax-submit"><?php echo lang('ADD'); ?></button>
					<a class="btn btn-default" href="javascript:history.back(-1);"><?php echo lang('BACK'); ?></a>
				</div>
			</div>
		</form>
	</div>
	<script src="/static/js/admin.js"></script>
	<script>
		$(function() {
			Wind.use('layer');
		});

		function selectGoods() {
			var url = "/admin/users/index";
			layer.open({
				type: 2,
				title: '选择会员',
				shadeClose: true,
				shade: 0.2,
				area: ['55%', '55%'],
				content: url,
			});
		}

		function selectShop() {
			var url = "/admin/users/shop";
			layer.open({
				type: 2,
				title: '选择店铺',
				shadeClose: true,
				shade: 0.2,
				area: ['55%', '55%'],
				content: url,
			});
		}


		function call_back(uid, id) {
			if (id == 2) {
				$('input[name=shop_id]').val(uid)
			} else {
				$('input[name=uid]').val(uid)
			}
			layer.closeAll('iframe');
		}
	</script>

</body>
</html>
