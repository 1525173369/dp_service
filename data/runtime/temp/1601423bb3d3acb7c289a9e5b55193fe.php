<?php /*a:0:{}*/ ?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>个人主页</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/vant@2.12/lib/index.css" />
    <style>
        html,
        body,
        div,
        span,
        ul,
        ol,
        form,
        header,
        nav,
        section,
        footer,
        h1,
        h2,
        h3,
        h4,
        h5,
        h6,
        p {
            padding: 0;
            margin: 0;
            box-sizing: border-box;
        }

        img {
            vertical-align: top;
        }

        ul,
        ol {
            list-style: none;
        }

        input,
        button {
            border: 0;
            outline: none;
            box-sizing: border-box;
        }

        a,
        a:hover,
        a:focus {
            color: #333333;
            text-decoration: none;
            /* 针对移动端点击高亮 */
            -webkit-tap-highlight-color: transparent;
        }

        body {
            font-family: Arial, Helvetica, sans-serif;
            position: relative;
        }

        .clearfix::after {
            content: "";
            display: block;
            clear: both;
            height: 0;
            opacity: 0;
            overflow: hidden;
        }

        .fl {
            float: left;
        }

        .fr {
            float: right;
        }

        .content {
            padding: 0.5rem 0.5rem 1rem;
            background-image: url(https://static-ap.askpert.com/android1628835318843Screenshot_20210731-164132.png);
            background-size: 100% 372%;
            background-position: 0 -15.6rem;
            /*background-color: #001b20;*/
            /*background-image: url();*/
            /*background-repeat: no-repeat;*/
            /*background-size: 100% 100%;*/
        }

        .user {
            width: 100%;
            margin-top: 1rem;
            margin-bottom: 0.8rem;
            display: flex;
        }

        .user-head {
            width: 2.5rem;
            height: 2.5rem;
            margin-right: 0.5rem;
            border-radius: 50%;
        }

        .user-head img {
            width: 100%;
            height: 100%;
            border-radius: 50%;
            object-fit: cover;
        }

        .user-information {
            width: 60%;
            display: flex;
            flex-direction: column;
            justify-content: space-around;
            align-items: flex-start;
        }

        .user-name {
            width: 10rem;
            font-size: 0.7rem;
            color: #fff;
            overflow: hidden;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        .user-id {
            font-size: 0.6rem;
            /* color: #666; */
            color: #fff;
            zoom: 0.7;
        }

        /* .information-box {
            display: flex;
        }

        .user-gender {
            display: flex;
            margin-right: 0.3rem;
            justify-content: center;
            align-items: center;
            width: 1rem;
            height: 0.8rem;
            border-radius: 1rem;
            background-color: rgba(255, 255, 255, .5);
        }

        .gender-img {
            width: 0.8rem;
            height: 0.8rem;
        }

        .gender-img img {
            width: 100%;
            height: 100%;
        }

        .medal {
            display: flex;
            justify-content: center;
            align-items: center;
            padding: 0.2rem;
            height: 0.8rem;
            border-radius: 1rem;
            background-color: rgba(255, 255, 255, .5);
        }

        .medal-img {
            width: 0.6rem;
            height: 0.6rem;
        }

        .medal-img img {
            width: 100%;
            height: 100%;
        }

        .medal-text {
            font-size: 0.6rem;
            zoom: 0.7;
            color: #fff;
        } */

        .introduce {
            font-size: 0.6rem;
            color: #e0e0e0;
            zoom: 0.9;
            margin-bottom: 0.8rem;
        }

        .user-data {
            /* width: 55%; */
            display: flex;
            align-items: center;
            color: #fff;
            justify-content: space-around;
        }

        .user-data .num {
            font-size: 0.6rem;
            text-align: center;
            margin-bottom: 0.1rem;
        }

        .user-data .text {
            font-size: 0.6rem;
            zoom: 0.8;
            color: #e0e0e0;
        }

        .publish-content {
            background-color: #fff;
            border-top-left-radius: 0.5rem;
            border-top-right-radius: 0.5rem;
            padding: 0.5rem 0;
            margin-top: -0.5rem;
        }

        .van-tabs--line .van-tabs__wrap {
            height: 1.7rem;
        }

        /* .van-tabs__nav--line{
            padding-bottom: 0.75rem;
        } */
        .van-tab--active {
            color: #323233 !important;
            font-size: 0.7rem !important;
        }

        .van-tab {
            color: #c1c1c1;
            font-size: 0.6rem;
            font-weight: bold;
        }

        .van-tabs__line {
            display: none;
        }

        .release {
            display: flex;
            flex-wrap: wrap;
            padding: 0.2rem 0.8rem;
        }

        .release-content {
            width: 7rem;
            height: 9rem;
            border-radius: 0.6rem;
            overflow: hidden;
            margin-bottom: 0.5rem;
        }

        .release-content:nth-child(odd) {
            margin-right: 0.4rem;
        }

        .release-content img {
            width: 100%;
            height: 100%;
            object-fit: cover;
        }
        .empty{
            width: 5rem;
            height: 5rem;
            margin: 2rem auto;
        }
        .empty img{
            width: 100%;
            height: 100%;
            object-fit: cover;
        }

        .empty-text{
            width: 100%;
            text-align: center;
            font-size: 0.7rem;
        }
        .empty-title{
            margin-bottom: 0.5rem;
        }
        .empty-tips{
            color: #a8a8a8;
        }
    </style>
</head>

<body>
    <div id="app">
        <div class="content" v-on:click="url($event)">
            <div class="user">
                <div class="user-head">
                    <img src="https://ap1.askpert.com/default.jpg">
                </div>
                <div class="user-information">
                    <div class="user-name"></div>
                    <div class="user-id">ID: 42125</div>
                    <!-- <div class="information-box">
                        <div class="user-gender">
                            <div class="gender-img">
                                <img src="./image/man.png">
                            </div>
                        </div>
                        <div class="medal">
                            <div class="medal-img">
                                <img src="./image/奖牌.png">
                            </div>
                            <div class="medal-text">金冠薯</div>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="introduce">这家伙很懒，什么都没留下</div>
            <div class="user-data">
                <div class="follow">
                    <div class="num">0</div>
                    <div class="text">关注</div>
                </div>
                <div class="fans">
                    <div class="num">0</div>
                    <div class="text">粉丝</div>
                </div>
                <div class="up">
                    <div class="num">0</div>
                    <div class="text">点赞</div>
                </div>
                <div class="reading">
                    <div class="num">0</div>
                    <div class="text">阅读量</div>
                </div>
            </div>
        </div>
        <div class="publish-content">
            <van-tabs v-model="active">
                <van-tab title="TA的发布" >
                    <div class="release">
                        <div v-on:click="url($event)"  v-for ="i in dy_list"  class="release-content">
                            <img :src="i.thumb == ''?i.video_img:i.thumb ">
                        </div>
                        <div v-if="dy_list==0" style="margin: 0 auto;">
                            <div class="empty">
                                <img src="https://static-ap.askpert.com/1287377ccbac88c1588eace875a28bb4.jpg">
                            </div>
                            <div class="empty-text">
                                <div class="empty-title">暂无数据</div>
                                <div class="empty-tips">赶紧去发布一条吧 ~</div>
                            </div>
                        </div>
                    </div>
                </van-tab>
                <van-tab title="TA的视频" >
                    <div class="release">
                        <div v-on:click="url($event)"  v-for ="i in vi_list"  class="release-content">
                            <img :src="i.video_img ">
                        </div>
                        <div v-if="vi_list==0" style="margin: 0 auto;">
                            <div class="empty">
                                <img src="https://static-ap.askpert.com/1287377ccbac88c1588eace875a28bb4.jpg">
                            </div>
                            <div class="empty-text">
                                <div class="empty-title">暂无数据</div>
                                <div class="empty-tips">赶紧去发布一条吧 ~</div>
                            </div>
                        </div>
                    </div>
                </van-tab>
          
            </van-tabs>
        </div>
    </div>
</body>
<script src="https://cdn.jsdelivr.net/npm/vue@2.6/dist/vue.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/vant@2.12/lib/vant.min.js"></script>
<script src="https://cdn.staticfile.org/vue-resource/1.5.1/vue-resource.min.js"></script>
<!-- 手机适配 -->
<script>
    (function (doc, win) {
        var docEl = doc.documentElement;
        var resizeEvt = 'orientationchange' in window ? 'orientationchange' : 'resize';
        var recalc = function () {
            var clientWidth = docEl.clientWidth;
            if (!clientWidth) return;
            if (clientWidth >= 640) clientWidth = 640;
            if (clientWidth <= 320) clientWidth = 320;
            docEl.style.fontSize = 20 * (clientWidth / 320) + 'px';
        };
        if (!doc.addEventListener) return;
        win.addEventListener(resizeEvt, recalc, false);
        doc.addEventListener('DOMContentLoaded', recalc, false);
    })(document, window);
</script>

<script>
    new Vue({
        el: '#app',
        data() {
            return {
                // list: [],
                active: 0,
                page:1,
                token:'',
                uid:'42125',
                next:true,
                dy_list:[],
                vpage:1,
                vnext:true,
                vi_list:[],
                vi_empty:false,
                dy_empty:false
            };
        },
        created:function(){
           this.getDynamic();
           this.getVideo();
        },
        methods: {
            getDynamic:function(){
                var _that = this;
                if(!_that.next){
                    return false;
                }
                var page = this.page
                var uid = this.uid
                var token = this.token
                
                this.$http.get('/portal/page/getDynamics?uid='+uid+'&token='+token+'&page='+page).then(function(res){
                   var list = res.body.list;
                   
                    if(list.length == 0 && page == 1){
                        _that.vi_empty = true;
                   }
                   
                   if(list.length == 0){
                       return false;
                   }
                   for(var i in list){
                       _that.dy_list.push(list[i]);
                   }
                   _that.page ++ ;
                },function(){
                    console.log('请求失败处理');
                });
                
            },
            
             getVideo:function(){
                var _that = this;
                if(!_that.vnext){
                    return false;
                }
                var page = this.vpage
                var uid = this.uid
                var token = this.token
                
                this.$http.get('/portal/page/getVideos?uid='+uid+'&token='+token+'&page='+page).then(function(res){
                   var list = res.body.list;
                   
                   if(list.length == 0 && page == 1){
                        _that.vi_empty = true;
                   }
                   
                   
                   if(list.length == 0){
                       _that.vnext = false;
                       return false;
                   }
                   
                   
                   for(var i in list){
                       _that.vi_list.push(list[i]);
                   }
                   _that.vpage ++ ;
                },function(){
                    console.log('请求失败处理');
                });
                
            },
          url:function(event) {
                var el = event.currentTarget;
                console.log(event)
               
                // window.location.href= host + https://ap1.askpert.com/appapi/down/index
                window.location.href = '/appapi/down/index'
            }
            
            
        },
    })
</script>

</html>