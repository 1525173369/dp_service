<?php /*a:2:{s:95:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/scannelorder/info.html";i:1627435623;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
    <div class="wrap" js-check-wrap>
        <ul class="nav nav-tabs">
            <li ><a href="<?php echo url('scannelorder/index'); ?>">流水列表</a></li>
            <li class="active"><a >流水详情</a></li>
        </ul>
        <form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('order/editPost'); ?>">
            
            <div class="panel panel-default">
                <div class="panel-heading">
                    基本信息
                </div>
                <div class="panel-body">
                   <div class="form-group">
                        <label for="input-id" class="col-sm-2 control-label"><span class="form-required"></span>订单ID</label>
                        <div class="col-md-6 col-sm-10">
                            <input type="text" class="form-control" id="input-id" value="<?php echo $data['id']; ?>" readonly="readonly">
                        </div>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="input-orderno" class="col-sm-2 control-label"><span class="form-required"></span>订单类型</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-orderno" value="<?php echo $data['type']==0 ? "扫码" : "转账"; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-orderno" class="col-sm-2 control-label"><span class="form-required"></span>订单编号</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-orderno" value="<?php echo $data['ordersn']; ?>" readonly="readonly">
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="input-buyer" class="col-sm-2 control-label"><span class="form-required"></span>金额</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-buyer" value="<?php echo $data['money']; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-buyer" class="col-sm-2 control-label"><span class="form-required"></span>店铺ID</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-buyer" value="<?php echo $data['shopid']; ?>" readonly="readonly">
                    </div>
                </div>
                <div class="form-group">
                    <label for="input-buyer" class="col-sm-2 control-label"><span class="form-required"></span>用户ID</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-buyer" value="<?php echo $data['uid']; ?>" readonly="readonly">
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="input-addtime" class="col-sm-2 control-label"><span class="form-required"></span>下单时间</label>
                    <div class="col-md-6 col-sm-10">
                        <input type="text" class="form-control" id="input-addtime" value="<?php echo date('Y-m-d H:i',$data['createtime']); ?>" readonly="readonly">
                    </div>
                </div>
                    
              
            </div>

        </form>
    </div>
    <script src="/static/js/admin.js"></script>
    
</body>
</html>