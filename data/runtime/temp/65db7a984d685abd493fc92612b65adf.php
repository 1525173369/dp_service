<?php /*a:2:{s:79:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/admin/scannelorder/index.html";i:1628652321;s:68:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap js-check-wrap">
		<ul class="nav nav-tabs">
			<li class="active"><a>扫码订单记录</a></li>
		</ul>
		
		<form class="well form-inline margin-top-20" method="post" action="<?php echo url('scannelorder/index'); ?>">
            
            名称： 
            <input class="form-control" type="text" name="keyword" style="width: 200px;" value="<?php echo input('request.keyword'); ?>"
                   placeholder="请输入订单号">
            用户ID：
            <input class="form-control" type="text" name="user_id" style="width: 200px;" value="<?php echo input('request.user_id'); ?>"
                   placeholder="请输入用户ID">
			<input type="submit" class="btn btn-primary" value="搜索">
			<a class="btn btn-danger" href="<?php echo url('scannelorder/index'); ?>">清空</a>
		</form>	
		<form method="post" class="js-ajax-form">
			<table class="table table-hover table-bordered">
				<thead id="bignav">
					<tr>
						<th>ID</th>
						<th>订单编号</th>
						<th>订单类型</th>
						<th>用户ID</th>
						
						<th>店铺ID</th>
						<th>金额</th>
						<th>下单时间</th>
						<th align="center"><?php echo lang('ACTIONS'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
					<tr>
						<td><?php echo $vo['id']; ?></td>
						<td><?php echo $vo['ordersn']; ?></td>
						<td><?php echo $vo['type']==0 ? "扫码" : "转账"; ?></td>
						<td>
						    <?php if($vo['type'] == 1): ?>
						    <?php echo $vo['uid']; ?> 转账给: <span style="color:red"><?php echo $vo['touid']; ?></span>
						    <?php else: ?>
						     <?php echo $vo['uid']; ?>
						    <?php endif; ?>
						    </td>
					
						<td><?php echo $vo['shopid']; ?></td>
						<td><?php echo $vo['money']; ?></td>
						<td><?php echo date('Y-m-d H:i',$vo['createtime']); ?></td>
						<td>
                            <a class="btn btn-xs btn-primary" href='<?php echo url("scannelorder/info",array("id"=>$vo["id"])); ?>'>详情</a>
							
						</td>
					</tr>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</tbody>
			</table>
			<div class="pagination"><?php echo $page; ?></div>
		</form>
	</div>
	<script src="/static/js/admin.js"></script>
	<script type="text/javascript">
		$(function(){
			Wind.use('layer');
		});
	</script>

	
</body>
</html>