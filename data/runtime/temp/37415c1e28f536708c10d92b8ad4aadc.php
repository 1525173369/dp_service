<?php /*a:2:{s:88:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/ad/note_ad.html";i:1623124595;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style>
    .imgtip{
        margin-bottom:5px;
    }
</style>
</head>
<body>
	<div class="wrap js-check-wrap">
		<ul class="nav nav-tabs">
			<li class="active"><a >帖子列表</a></li>
		</ul>
        
        <form class="well form-inline margin-top-20" method="post" action="<?php echo url('ad/noteAd'); ?>">
            帖子ID：
            <input class="form-control" type="text" name="id" style="width: 200px;" value="<?php echo input('request.id'); ?>"
                   placeholder="帖子ID">
            帖子标题：
            <input class="form-control" type="text" name="title" style="width: 200px;" value=""
                   placeholder="帖子标题">       
            <input type="submit" class="btn btn-primary" value="搜索"/>
        </form>
    
		<form method="post" class="js-ajax-form">
            <div class="table-actions">
     
            </div>
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
                        <th width="16">
                            <label>
                                <input type="checkbox" class="js-check-all" data-direction="x" data-checklist="js-check-x">
                            </label>
                        </th>
						<th>ID</th>
					
						<th width="10%">标题</th>
							<th>用户id/昵称</th>
							<th>广告图</th>
						<th align="center"><?php echo lang('ACTIONS'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
					<tr>
                        <td>
                            <input type="checkbox" class="js-check" data-yid="js-check-y" data-xid="js-check-x" name="ids[]" value="<?php echo $vo['id']; ?>">
                        </td>
						<td><?php echo $vo['id']; ?></td>
                      
                        <td><?php echo $vo['title']; ?></td>
                        <td><?php echo $vo['uid']; ?>/<?php echo $vo['user_nickname']; ?></td>
                         <td>
                             <img class="imgtip" src="<?php echo $vo['thumb1']; ?>" style="max-width:100px;max-height:100px;">
                             <img class="imgtip" src="<?php echo $vo['thumb2']; ?>" style="max-width:100px;max-height:100px;">
                             <img class="imgtip" src="<?php echo $vo['thumb3']; ?>" style="max-width:100px;max-height:100px;">
                             <img class="imgtip" src="<?php echo $vo['thumb4']; ?>" style="max-width:100px;max-height:100px;">
                        </td>
						<td>
                		       <a class="btn btn-xs btn-primary" href='<?php echo url("Ad/edit",array("id"=>$vo["id"])); ?>'>添加广告</a>
						</td>
					</tr>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</tbody>
			</table>
			<div class="pagination"><?php echo $page; ?></div>

		</form>
	</div>
    <div id="enlarge_images" style="position:fixed;display:none;z-index:2;"></div>
	<script src="/static/js/admin.js"></script>
    <script>
        $(function(){
            
            Wind.use('layer');
            
            $('.xiajia').click(function(){
                var _this=$(this);
                var id=_this.data('id');
                
                layer.prompt({
                    formType: 2,
                    title: '请输入下架原因',
                    area: ['800px', '100px'] //自定义文本域宽高
                }, function(value, index, elem){
                    $.ajax({
                        url:'<?php echo url('publish/setDel'); ?>',
                        type:'POST',
                        data:{id:id,isdel:1,reason:value},
                        dataType:'json',
                        success:function(data){
                            var code=data.code;
                            if(code==0){
                                layer.msg(data.msg);
                                return !1;
                            }
                            layer.msg(data.msg,{},function(){
                                layer.closeAll();
                                reloadPage(window);
                            });
                            
                        },
                        error:function(){
                            layer.msg('操作失败，请重试')
                        }
                    });
                    
                });
                
            })
            
            $('.setrecom').click(function(){
                var _this=$(this);
                var id=_this.data('id');
                var recoms=_this.data('recoms');
                
                layer.prompt({
                    formType: 0,
                    title: '推荐值',
                    value: recoms,
                    area: ['800px', '100px'] //自定义文本域宽高
                }, function(value, index, elem){
                    $.ajax({
                        url:'<?php echo url('publish/setrecom'); ?>',
                        type:'POST',
                        data:{id:id,recoms:value},
                        dataType:'json',
                        success:function(data){
                            var code=data.code;
                            if(code==0){
                                layer.msg(data.msg);
                                return !1;
                            }
                            layer.msg(data.msg,{},function(){
                                layer.closeAll();
                                reloadPage(window);
                            });
                            
                        },
                        error:function(){
                            layer.msg('操作失败，请重试')
                        }
                    });
                    
                });
                
            })
            
            $('.comment').click(function(){
                var _this=$(this);
                var id=_this.data('id');

                layer.open({
                    type: 2,
                    title: '查看评论',
                    shadeClose: true,
                    shade: 0.8,
                    area: ['80%', '100%'],
                    content: '/admin/comment/index?dynamicid='+id
                }); 
                
            });
            
            $('.view').click(function(){
                var _this=$(this);
                var id=_this.data('id');

                layer.open({
                    type: 2,
                    title: '查看',
                    shadeClose: true,
                    shade: 0.8,
                    area: ['500px', '100%'],
                    content: '/admin/publish/see?id='+id
                }); 
                
            });
        })
    </script>
</body>
</html>