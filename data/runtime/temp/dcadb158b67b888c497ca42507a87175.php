<?php /*a:2:{s:89:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/video/edits.html";i:1628913724;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap">
    <ul class="nav nav-tabs">
        <li ><a href="<?php echo url('Video/indexs'); ?>">视频列表</a></li>
        <li class="active"><a ><?php echo lang('编辑'); ?></a></li>
    </ul>
    <form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('Video/edits',['id'=>$id]); ?>">

        <div class="form-group">
            <label for="input-uid" class="col-sm-2 control-label"><span class="form-required">*</span>用户ID</label>
            <div class="col-md-3 col-sm-10">
                <input type="text" class="form-control" id="uid" name="uid" placeholder="请输入发帖人ID" readonly value="<?php echo $info['uid']; ?>">
                <span class="help-block">
						<a onclick="selectGoods()"><i class="fa fa-search"></i>选择</a>
					</span>
            </div>
        </div>

        <div class="form-group">
            <label for="input-score" class="col-sm-2 control-label"><span class="form-required">*</span>评分</label>
            <div class="col-md-6 col-sm-10">
                <input type="radio" value="1" <?php if($info['score'] == 1): ?>checked<?php endif; ?> name="score">1星
                <input type="radio" value="2" <?php if($info['score'] == 2): ?>checked<?php endif; ?>  name="score">2星
                <input type="radio" value="3" <?php if($info['score'] == 3): ?>checked<?php endif; ?>  name="score">3星
                <input type="radio" value="4" <?php if($info['score'] == 4): ?>checked<?php endif; ?>  name="score">4星
                <input type="radio" value="5" <?php if($info['score'] == 5): ?>checked<?php endif; ?>  name="score">5星
            </div>
        </div>


        <div class="form-group">
            <label for="input-desc" class="col-sm-2 control-label"><span class="form-required">*</span>简介</label>
            <div class="col-md-6 col-sm-10">
                <textarea name="desc" cols="50" rows="10"><?php echo $info['desc']; ?></textarea>
            </div>
        </div>

        <div class="form-group">
            <label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>话题</label>
            <div class="col-md-6 col-sm-10" style="height:500px;overflow-y:auto;border:1px solid #eeeeee;border-radius: 10px;">
                <?php if(is_array($topicData) || $topicData instanceof \think\Collection || $topicData instanceof \think\Paginator): $i = 0; $__LIST__ = $topicData;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                    <label class="checkbox-inline">
                        <input type="checkbox" <?php if(in_array($v['topic_id'],$info['topic'])): ?>checked<?php endif; ?> id="inlineCheckbox1" value="<?php echo $v['topic_id']; ?>" name="topic[]"> <?php echo $v['topic_name']; ?>
                    </label>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>
        </div>

        <div class="form-group">
            <label for="input-shop_id" class="col-sm-2 control-label"><span class="form-required">*</span>所属店铺</label>
            <div class="col-md-3 col-sm-10">
                <input type="text" class="form-control" id="shop_id" name="shop_id" placeholder="店铺ID" readonly value="<?php echo $info['shop_id']; ?>">
                <span class="help-block">
						<a onclick="selectShop()"><i class="fa fa-search"></i>选择</a>
					</span>
            </div>
        </div>

        <div class="form-group">
            <label for="input-video_img" class="col-sm-2 control-label"><span class="form-required"></span>上传视频封面</label>
            <div class="col-md-6 col-sm-10">
                <input class="form-control" id="js-file-video_img" type="text" value="<?php echo $info['video_img']; ?>" name="video_img" style="width: 300px;display: inline-block;" title="视频文件地址" >
                <a href="javascript:uploadOne('文件上传','#js-file-video_img','image');">上传文件</a>图片格式
                <p>不传入,系统自动截取视频第一秒成为封面!</p>
            </div>

        </div>


        <div class="form-group">
            <label for="input-video" class="col-sm-2 control-label"><span class="form-required">*</span>上传视频</label>
            <div class="col-md-6 col-sm-10">
                <input class="form-control" value="<?php echo $info['video_url']; ?>" id="js-file-input" type="text" name="video_url" style="width: 300px;display: inline-block;" title="视频文件地址" >

                <a href="javascript:uploadOne('文件上传','#js-file-input','video');">上传文件</a>MP4格式
                <p class="help-block">可填写视频链接 [只能包含字母,数字,下划线,不能包含*#$等特殊字符]，可直接上传视频获取链接</p>
            </div>

        </div>

        <div class="form-group">
            <label for="input-likes" class="col-sm-2 control-label"><span class="form-required">*</span>点赞数</label>
            <div class="col-md-6 col-sm-10">
                <input type="number" readonly="readonly" value="<?php echo $info['likes']; ?>" class="form-control" id="input-likes" name="likes">
            </div>
        </div>

        <div class="form-group">
            <label for="input-comments" class="col-sm-2 control-label"><span class="form-required">*</span>评论数</label>
            <div class="col-md-6 col-sm-10">
                <input type="number" readonly="readonly" value="<?php echo $info['comments']; ?>" class="form-control" id="input-comments" name="comments">
            </div>
        </div>

        <div class="form-group">
            <label for="input-collects" class="col-sm-2 control-label"><span class="form-required">*</span>收藏数</label>
            <div class="col-md-6 col-sm-10">
                <input type="number" readonly="readonly" value="<?php echo $info['collects']; ?>" class="form-control" id="input-collects" name="collects">
            </div>
        </div>

        <div class="form-group">
            <label for="input-shares" class="col-sm-2 control-label"><span class="form-required">*</span>分享数</label>
            <div class="col-md-6 col-sm-10">
                <input type="number" readonly="readonly" value="<?php echo $info['shares']; ?>" class="form-control" id="input-shares" name="shares">
            </div>
        </div>

        <div class="form-group">
          <!--  <label for="input-hot" class="col-sm-2 control-label"><span class="form-required">*</span>热度</label>-->
            <div class="col-md-6 col-sm-10">
                <input type="hidden" value="<?php echo $info['hot']; ?>" class="form-control" id="input-hot" name="hot">
            </div>
        </div>

        <div class="form-group">
            <label for="input-visits" class="col-sm-2 control-label"><span class="form-required">*</span>浏览数</label>
            <div class="col-md-6 col-sm-10">
                <input type="number" readonly="readonly" value="<?php echo $info['visits']; ?>" class="form-control" id="input-visits" name="visits">
            </div>
        </div>

        <div class="form-group">
            <label for="input-address" class="col-sm-2 control-label"><span class="form-required">*</span>地址</label>
            <div class="col-md-6 col-sm-10">
                <input type="text" value="<?php echo $info['address']; ?>" class="form-control" id="input-address" name="address">
            </div>
        </div>


        <div class="form-group">
            <label for="input-score" class="col-sm-2 control-label"><span class="form-required">*</span>状态</label>
            <div class="col-md-6 col-sm-10">
                <input type="radio" value="1" <?php if($info['status'] == 1): ?>checked<?php endif; ?> name="status">上架
                <input type="radio" value="0" <?php if($info['status'] == 0): ?>checked<?php endif; ?>  name="status">下架

            </div>
        </div>


        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <input type="hidden" name="status" value="1">
                <button type="submit" class="btn btn-primary js-ajax-submit"><?php echo lang('编辑'); ?></button>
                <a class="btn btn-default" href="javascript:history.back(-1);"><?php echo lang('BACK'); ?></a>
            </div>
        </div>

    </form>
</div>
<script src="/static/js/admin.js"></script>
<script>
        (function(){
            $('.btn-cancel-thumbnail').click(function () {
                $('#thumbnail-preview').attr('src', '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png');
                $('#thumbnail').val('');
            });
            Wind.use('layer');
        })()

        function selectGoods() {
			var url = "/admin/users/index";
			layer.open({
				type: 2,
				title: '选择会员',
				shadeClose: true,
				shade: 0.2,
				area: ['55%', '55%'],
				content: url,
			});
		}

        function selectShop() {
			var url = "/admin/users/shop";
			layer.open({
				type: 2,
				title: '选择店铺',
				shadeClose: true,
				shade: 0.2,
				area: ['55%', '55%'],
				content: url,
			});
		}

		function call_back(uid, id) {
			if (id == 2) {
				$('input[name=shop_id]').val(uid)
			} else {
				$('input[name=uid]').val(uid)
			}
			layer.closeAll('iframe');
		}

    </script>
</body>
</html>
