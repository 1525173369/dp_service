<?php /*a:2:{s:95:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/attestation/index.html";i:1623124595;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap js-check-wrap">
		<ul class="nav nav-tabs">
			<li class="active"><a>认领列表</a></li>
		</ul>
		<form class="well form-inline margin-top-20" method="post" action="<?php echo url('Attestation/index'); ?>">
		     审核状态：
			<select class="form-control" name="status">
				<option value="">全部</option>
                <?php if(is_array($status) || $status instanceof \think\Collection || $status instanceof \think\Paginator): $i = 0; $__LIST__ = $status;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                    <option value="<?php echo $key; ?>" <?php if(input('request.status') != '' && input('request.status') == $key): ?>selected<?php endif; ?> ><?php echo $v; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>    
			</select>
            
			用户： 
            <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"
                   placeholder="请输入会员ID号">
            姓名： 
            <input class="form-control" type="text" name="keyword" style="width: 200px;" value="<?php echo input('request.keyword'); ?>"
                   placeholder="请输入名称">
			<input type="submit" class="btn btn-primary" value="搜索">
			<a class="btn btn-danger" href="<?php echo url('Attestation/index'); ?>">清空</a>
		</form>				
		<form method="post" class="js-ajax-form" >
			<div class="table-actions">
                <!-- <button class="btn btn-danger btn-sm js-ajax-submit" type="submit" data-action="<?php echo url('Shopgoods/del'); ?>"
                        data-subcheck="true">批量删除
                </button> -->



            </div>
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th width="16">
							
                            <label>
                                <input type="checkbox" class="js-check-all" data-direction="x" data-checklist="js-check-x">
                            </label>
                        	
                        </th>
						<th>ID</th>
						<th>认领店铺ID</th>
						<th>认领店铺名称</th>
						<th>认领人id/昵称</th>
						<th>申请人</th>
						<th>状态</th>
						<th>申请时间</th>
						<th><?php echo lang('ACTIONS'); ?></th>
					</tr>
				</thead>
				<tbody>
					<?php if(is_array($lists) || $lists instanceof \think\Collection || $lists instanceof \think\Paginator): if( count($lists)==0 ) : echo "" ;else: foreach($lists as $key=>$vo): ?>
					<tr>
						<td>
					
                            	<input type="checkbox" class="js-check" data-yid="js-check-y" data-xid="js-check-x" name="ids[]" value="<?php echo $vo['id']; ?>">
                      
                        </td>
					    <td><?php echo $vo['id']; ?></td>
					    <td><?php echo $vo['shop_id']; ?></td>
					    <td><?php echo $vo['sName']; ?> </td>
					    <td><?php echo $vo['uid']; ?>/<?php echo $vo['user_nickname']; ?></td>
					    <td><?php echo $vo['username']; ?></td>
					    <td><?php if($vo['status'] == '0'): ?>审核中<?php elseif($vo['status'] == 1): ?>通过<?php else: ?>驳回<?php endif; ?>
					    </td>
					    <td><?php echo date('Y-m-d H:i:s',$vo['addtime']); ?></td>
						<td>
						
                           <a class="btn btn-xs btn-primary" href='<?php echo url("Attestation/edit",array("id"=>$vo["id"])); ?>'>详情</a>
                           <a class="btn btn-xs btn-danger js-ajax-delete" href="<?php echo url('Attestation/del',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a>
						</td>
					</tr>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</tbody>
			</table>
			<div class="table-actions">
                <!-- <button class="btn btn-danger btn-sm js-ajax-submit" type="submit" data-action="<?php echo url('Shopgoods/del'); ?>"
                        data-subcheck="true">批量删除
                </button> -->


           
            </div>
			<div class="pagination"><?php echo $page; ?></div>
		</form>
	</div>
	<script src="/static/js/admin.js"></script>

</body>
</html>