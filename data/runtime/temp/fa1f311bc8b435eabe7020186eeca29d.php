<?php /*a:2:{s:88:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/branch/add.html";i:1627033058;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li ><a href="<?php echo url('branch/index'); ?>">列表</a></li>
			<li class="active"><a ><?php echo lang('ADD'); ?></a></li>
		</ul>
		<form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('branch/addPost'); ?>">
            
            <div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>分店名称</label>
				<div class="col-md-6 col-sm-10">
                    <input type="text" class="form-control" name="shop_name">
				</div>
			</div>
            
            
            <div class="box-map" id="googleMap" style="width:600px;height:500px;margin-left:20%;">谷歌地图正在加载中......，显示不出来则翻墙失败！
    		</div>
	

        	<div class="form-group" style="margin-top:15px;">
        		<label for="input-ipa_ewm" class="col-sm-2 control-label">请输入地点</label>
		  		<div class="col-md-6 col-sm-10">
        		<input type="text" class="form-control" id="address" name="address">
    		     <span class="help-block">
                    <a id="search"><i class="fa fa-search"></i>搜索</a>
                </span>
        		</div>	   
        	</div>
			<div class="form-group">
    			<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>经纬度</label>    
                <div class="row">
                    <div class="col-sm-2">
                        <input type="text" class="form-control" id="lat" name="lat" placeholder="纬度">
                    </div>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="lng" name="lng" placeholder="经度">
                    </div>
                </div>
			</div>
	   <script type="text/javascript">
	   console.log("进来");
        // var lat=parseFloat($('#lat').val());
        //  var lng=parseFloat($('#lng').val());
        //  var address=$('#address').val();
        
        var lat = 41.8781136;
        var lng = -87.6297982;
        var address = '芝加哥';
        var map,mapProp,chicago,marker,infowindow;
        var keys='AIzaSyATCKWBnPvW_cyuUexKqiJsjw-lFu389mA';
        var mapid=document.getElementById("googleMap");
        function initMap(callback){
        	chicago ={lat: lat, lng:lng};
        	 mapProp = {
                center:chicago,
                zoom:15,
                gestureHandling: 'greedy'
            };
        	map=new google.maps.Map(mapid, mapProp);
            //添加标记
        	marker=new google.maps.Marker({
        		//添加标记
        	    position:mapProp.center
        	    //自定义图标 icon:'pinkball.png'
        	});
        	marker.setMap(map);
        	infowindow = new google.maps.InfoWindow({
                  content:address
            });
            infowindow.open(map,marker);
        };	
        window.onload=function(){
            console.log("加载");
        	var gooMapJs='https://maps.googleapis.com/maps/api/js?key='+keys+'&callback=initMap&sensor=false';
        	//谷歌地图
        	var script=document.createElement('script');
        		script.src=gooMapJs;
        		document.body.appendChild(script);
        		script.onload=function(){
        			map.addListener('click', function(e) {
        				placeMarkerAndPanTo(e.latLng, map);
        			});
        			function placeMarkerAndPanTo(latLng, map){
        				console.log(latLng);
        				/*var marker = new google.maps.Marker({ position: latLng, map: map});map.panTo(latLng);*/
        			};
        		};
        		script.onerror=function(){
        			if(typeof google==='undefined'){
        				$(mapid).html('因地域限制，google地图无法显示');
        			}
        		};
        		
        };
        
        $('#search').click(function () {
            console.log("搜索");
          	var address = $('#address').val();
          	console.log("sdfsds");
          	console.log(address);
          	console.log("sdfsdf");
          	if(!address) return;
        	var newuri='https://maps.googleapis.com/maps/api/geocode/json?address='+address+'&key='+keys;
        	$.ajax({
        	url:newuri,
        	success:function(data){
        	if(data.status=="OK"){
        		  lat = data.results[0].geometry.location.lat;
        		  lng = data.results[0].geometry.location.lng;
        		  $('#lat').val(lat);
        		  $('#lng').val(lng);
        			initMap();
        		var infowindow = new google.maps.InfoWindow({
        			  content:address
        		});
        		infowindow.open(map,marker);
        				  }else{
        					console.log(data.error_message);
        		}
        		}
        	});
        });
        
        
        </script> 



			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary js-ajax-submit"><?php echo lang('ADD'); ?></button>
					<a class="btn btn-default" href="javascript:history.back(-1);"><?php echo lang('BACK'); ?></a>
				</div>
			</div>
            
		</form>
	</div>
	<script src="/static/js/admin.js"></script>
    <script>
        (function(){
            $('.btn-cancel-thumbnail').click(function () {
                $('#thumbnail-preview').attr('src', '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png');
                $('#thumbnail').val('');
            });
            
        })()
    </script>
</body>
</html>