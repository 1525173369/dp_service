<?php /*a:1:{s:90:"/www/wwwroot/test01.zwrjkf.com/dp_service/app/merch/controller/../view/public/success.html";i:1628648272;}*/ ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Vito - Responsive Bootstrap 4 Admin Dashboard Template</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="/merch/images/favicon.ico" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/merch/css/bootstrap.min.css">
    <!-- Typography CSS -->
    <link rel="stylesheet" href="/merch/css/typography.css">
    <!-- Style CSS -->
    <link rel="stylesheet" href="/merch/css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="/merch/css/responsive.css">
</head>
<body>
<!-- loader Start -->
<div id="loading">
    <div id="loading-center">
    </div>
</div>
<!-- loader END -->
<!-- Sign in Start -->
<section class="sign-in-page">
    <div class="container mt-5 p-0 bg-white">
        <div class="row no-gutters">
            <div class="col-sm-6 align-self-center">
                <div class="sign-in-from">
                    <img src="/merch/images/login/mail.png" width="80"  alt="">
                    <h1 class="mt-3 mb-0"><?php echo $msg; ?></h1>
                    <p>Will redirect in:<font id="wait"><?php echo $wait; ?></font>sec</p>
                    <div class="d-inline-block w-100">
                        <button type="button" id="re" class="btn btn-primary mt-3">Redirect now</button>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 text-center">
                <div class="sign-in-detail text-white">
                    <a class="sign-in-logo mb-5" href="#"><img src="/merch/images/logo-white.png" class="img-fluid" alt="logo"></a>
                    <div class="owl-carousel" data-autoplay="true" data-loop="true" data-nav="false" data-dots="true" data-items="1" data-items-laptop="1" data-items-tab="1" data-items-mobile="1" data-items-mobile-sm="1" data-margin="0">
                        <div class="item">
                            <img src="/merch/images/login/1.png" class="img-fluid mb-4" alt="logo">
                            <h4 class="mb-1 text-white">Manage your orders</h4>
                            <p>It is a long established fact that a reader will be distracted by the readable content.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Sign in END -->
<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="/merch/js/jquery.min.js"></script>
<script>
           var r = $("#wait").text();
           var url  = "<?php echo $url; ?>";
           console.log(url)
           setInterval(function(){
                 r--;
                 if(r <= 0){
                       window.location.href= url;
                 }
                 $("#wait").text(r)
            }, r*500);

            $("#re").click(function(){
               window.location.href=url;
            })

        </script>
<script src="/merch/js/popper.min.js"></script>
<script src="/merch/js/bootstrap.min.js"></script>
<!-- Appear JavaScript -->
<script src="/merch/js/jquery.appear.js"></script>
<!-- Countdown JavaScript -->
<script src="/merch/js/countdown.min.js"></script>
<!-- Counterup JavaScript -->
<script src="/merch/js/waypoints.min.js"></script>
<script src="/merch/js/jquery.counterup.min.js"></script>
<!-- Wow JavaScript -->
<script src="/merch/js/wow.min.js"></script>
<!-- Apexcharts JavaScript -->
<script src="/merch/js/apexcharts.js"></script>
<!-- Slick JavaScript -->
<script src="/merch/js/slick.min.js"></script>
<!-- Select2 JavaScript -->
<script src="/merch/js/select2.min.js"></script>
<!-- Owl Carousel JavaScript -->
<script src="/merch/js/owl.carousel.min.js"></script>
<!-- Magnific Popup JavaScript -->
<script src="/merch/js/jquery.magnific-popup.min.js"></script>
<!-- Smooth Scrollbar JavaScript -->
<script src="/merch/js/smooth-scrollbar.js"></script>
<!-- lottie JavaScript -->
<script src="/merch/js/lottie.js"></script>
<!-- Chart Custom JavaScript -->
<script src="/merch/js/chart-custom.js"></script>
<!-- Custom JavaScript -->
<script src="/merch/js/custom.js"></script>
</body>
</html>