<?php /*a:1:{s:72:"/www/wwwroot/test01.zwrjkf.com/dp_service/app/merch/view/index/cash.html";i:1628648468;}*/ ?>
<!doctype html>
<html lang="en">
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>提现</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="/merch/images/favicon.ico" />
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="/merch/css/bootstrap.min.css">
    <!-- Typography CSS -->
    <link rel="stylesheet" href="/merch/css/typography.css">
    <!-- Style CSS -->
    <link rel="stylesheet" href="/merch/css/style.css">
    <!-- Responsive CSS -->
    <link rel="stylesheet" href="/merch/css/responsive.css">
</head>
<style>


</style>
<body>
<!-- loader Start -->
<div id="loading">
    <div id="loading-center">
    </div>
</div>
<!-- loader END -->
<!-- Wrapper Start -->
<div class="wrapper">
    <!-- Sidebar  -->
    <div class="iq-sidebar">
        <div class="iq-sidebar-logo d-flex justify-content-between">
            <a href="#">

                <span style="text-transform: none;">ASKPERT</span>
            </a>
            <div class="iq-menu-bt-sidebar">
                <div class="iq-menu-bt align-self-center">
                    <div class="wrapper-menu">
                        <div class="main-circle"><i class="ri-arrow-left-s-line"></i></div>
                        <!--   <div class="hover-circle"><i class="ri-arrow-right-s-line"></i></div>-->
                    </div>
                </div>
            </div>
        </div>
        <div id="sidebar-scrollbar">
            <nav class="iq-sidebar-menu">
                <ul id="iq-sidebar-toggle" class="iq-menu">

                    <li class="iq-menu-title"><i class="ri-subtract-line"></i><span>Merchant Panel</span></li>
                    <li class="active">
                        <a href="#mailbox" class="iq-waves-effect collapsed"  data-toggle="collapse" aria-expanded="true"><i class="ri-mail-line"></i><span>Order Management</span><i class="ri-arrow-right-s-line iq-arrow-right"></i></a>
                        <ul id="mailbox" class="iq-submenu collapse" data-parent="#iq-sidebar-toggle">
                            <li class=""><a href="/merch/index/order"><i class="ri-inbox-line"></i>Transactions</a></li>
                            <li class=""><a href="/merch/index/cash_list"><i class="ri-inbox-line"></i>Withdrawal Record</a></li>
                            <li class="active"><a href="#"><i class="ri-inbox-line"></i>Withdraw Now</a></li>
                        </ul>
                    </li>


                </ul>
            </nav>
            <div class="p-3"></div>
        </div>
    </div>
    <!-- TOP Nav Bar -->
    <div class="iq-top-navbar">
        <div class="iq-navbar-custom">
            <div class="iq-sidebar-logo">
                <div class="top-logo">
                    <a href="index.html" class="logo">
                        <img src="/merch/images/logo.gif" class="img-fluid" alt="">
                        <span>ASPKERT</span>
                    </a>
                </div>
            </div>
            <nav class="navbar navbar-expand-lg navbar-light p-0">
                <div class="navbar-left">
                </div>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="ri-menu-3-line"></i>
                </button>
                <div class="iq-menu-bt align-self-center">
                    <div class="wrapper-menu">
                        <div class="main-circle"><i class="ri-arrow-left-s-line"></i></div>
                        <div class="hover-circle"><i class="ri-arrow-right-s-line"></i></div>
                    </div>
                </div>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">

                   <span style="width: 20%;text-align: center">Coins Balance:<b><?php echo $shop['coin']; ?>coins</b></span>
                   <span style="width: 20%;text-align: center">Time:<b><?php echo date('Y-m-d  H:i'); ?></b></span>
                    <span style="width: 20%;text-align: center" id="qr_code_check"> </span>
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModalLong">
                      View QR Code
                    </button>
                    <!-- Modal -->
                    <div class="modal fade" id="exampleModalLong" tabindex="-1" style="z-index: 99999" role="dialog" aria-labelledby="exampleModalLong" aria-hidden="true">
                        <div class="modal-dialog" role="dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLongTitle">二维码</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">

                                    <div  id="html_qrcode" style="margin-left: 20%"><div id="qrcode"></div></div>
                                </div>
                                <div class="modal-footer">

                                    <button type="button" data-dismiss="modal" class="btn btn-primary">确定</button>
                                </div>
                            </div>
                        </div>
                    </div>



                    <ul class="navbar-nav ml-auto navbar-list">
                       <!-- <li class="nav-item">
                            <a class="search-toggle iq-waves-effect language-title" href="#"><img src="../images/small/flag-01.png" alt="img-flaf" class="img-fluid mr-1" style="height: 16px; width: 16px;" /> 中文 <i class="ri-arrow-down-s-line"></i></a>
                            <div class="iq-sub-dropdown">
                            </div>
                        </li>-->

                        <li class="nav-item">
                            <!--    <a href="#" class="search-toggle iq-waves-effect">
                                    <div id="lottie-beil"></div>
                                    <span class="bg-danger dots"></span>
                                </a>-->

                        </li>
                        <li class="nav-item dropdown">
                            <!--  <a href="#" class="search-toggle iq-waves-effect">
                                  <div id="lottie-mail"></div>
                                  <span class="bg-primary count-mail"></span>
                              </a>-->
                            <div class="iq-sub-dropdown">
                                <div class="iq-card shadow-none m-0">
                                    <div class="iq-card-body p-0 ">
                                        <div class="bg-primary p-3">
                                            <h5 class="mb-0 text-white">All Messages<small class="badge  badge-light float-right pt-1">5</small></h5>
                                        </div>
                                        <a href="#" class="iq-sub-card" >
                                            <div class="media align-items-center">
                                                <div class="">
                                                    <img class="avatar-40 rounded" src="../images/user/01.jpg" alt="">
                                                </div>
                                                <div class="media-body ml-3">
                                                    <h6 class="mb-0 ">Nik Emma Watson</h6>
                                                    <small class="float-left font-size-12">13 Jun</small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="#" class="iq-sub-card" >
                                            <div class="media align-items-center">
                                                <div class="">
                                                    <img class="avatar-40 rounded" src="../images/user/02.jpg" alt="">
                                                </div>
                                                <div class="media-body ml-3">
                                                    <h6 class="mb-0 ">Lorem Ipsum Watson</h6>
                                                    <small class="float-left font-size-12">20 Apr</small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="#" class="iq-sub-card" >
                                            <div class="media align-items-center">
                                                <div class="">
                                                    <img class="avatar-40 rounded" src="../images/user/03.jpg" alt="">
                                                </div>
                                                <div class="media-body ml-3">
                                                    <h6 class="mb-0 ">Why do we use it?</h6>
                                                    <small class="float-left font-size-12">30 Jun</small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="#" class="iq-sub-card" >
                                            <div class="media align-items-center">
                                                <div class="">
                                                    <img class="avatar-40 rounded" src="../images/user/04.jpg" alt="">
                                                </div>
                                                <div class="media-body ml-3">
                                                    <h6 class="mb-0 ">Variations Passages</h6>
                                                    <small class="float-left font-size-12">12 Sep</small>
                                                </div>
                                            </div>
                                        </a>
                                        <a href="#" class="iq-sub-card" >
                                            <div class="media align-items-center">
                                                <div class="">
                                                    <img class="avatar-40 rounded" src="../images/user/05.jpg" alt="">
                                                </div>
                                                <div class="media-body ml-3">
                                                    <h6 class="mb-0 ">Lorem Ipsum generators</h6>
                                                    <small class="float-left font-size-12">5 Dec</small>
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                </div>
                <?php
                    $header_img = session('merch_login')['info']['thumb'];
                    $id = session('merch_login')['info']['id'];
                    $user_nickname = session('merch_login')['info']['name'];

                ?>


                <ul class="navbar-list">
                    <li>
                        <a href="#" class="search-toggle iq-waves-effect d-flex align-items-center">
                            <img src="<?php echo $header_img; ?>" class="img-fluid rounded mr-3" alt="user">
                            <div class="caption">
                                <h6 class="mb-0 line-height"><?php echo $user_nickname; ?></h6>
                                <span class="font-size-12"><?php echo $id; ?></span>
                            </div>
                        </a>
                        <div class="iq-sub-dropdown iq-user-dropdown">
                            <div class="iq-card shadow-none m-0">
                                <div class="iq-card-body p-0 ">
                                    <div class="d-inline-block w-100 text-center p-3">
                                        <a class="bg-primary iq-sign-btn" href="/merch/login/loginout" role="button">Logout</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </nav>

        </div>
    </div>
    <!-- TOP Nav Bar END -->
    <!-- Page Content  -->
    <div id="content-page" class="content-page">
        <div class="container-fluid">
            <div class="row">
                <div class="col-sm-12 col-lg-12">

                    <div class="iq-card">
                        <div class="iq-card-header d-flex justify-content-between">
                            <div class="iq-header-title">
                                <h4 class="card-title">Withdrawal</h4>
                            </div>
                        </div>
                        <div class="iq-card-body">
                            <?php 
                                connectionRedis();
                                $config  =    getConfigPub();
                             ?>
                            <p>TIPS: <?php echo $config['merch_cash_tips']; ?></p>
                            <form method="post">

                                <div class="form-group">
                                    <label for="bank_name">Coins Withdrawal </label>
                                    <input type="number" required name="coin" class="form-control" id="money" value="" placeholder="Insert coins withdrawal amount">
                                </div>

                                <div class="form-group">
                                    <label for="bank_name">Insert bank name </label>
                                    <input type="text" required name="bank_name" class="form-control" id="bank_name" value="" placeholder="Insert bank name">
                                </div>
                                
                                <div class="form-group">
                                    <label for="bank_account">Bank Account No</label>
                                    <input type="text" required name="bank_account" class="form-control" id="bank_account" placeholder="Bank Account No">
                                </div>
                                
                                
                            <!--    <div class="form-group">
                                    <label for="bank_username">银行卡持有人姓名 </label>
                                    <input type="text" required name="bank_username" class="form-control" id="bank_username" value="" placeholder="银行卡持有人姓名">
                                </div>-->


                                  <div class="form-group">
                                    <label for="company_name">Company  Name </label>
                                    <input type="text" required name="company_name" class="form-control" id="company_name" value="" placeholder="Company  Name">
                                </div>
                                
                                
                                 <div class="form-group">
                                    <label for="ssm_no">SSM No</label>
                                    <input type="text" required name="ssm_no" class="form-control" id="ssm_no" value="" placeholder="SSM No">
                                </div>
                                
                            
                                
                                 <div class="form-group">
                                    <label for="swift_code">Swift Code</label>
                                    <input type="text" required name="swift_code" class="form-control" id="bank_name" value="" placeholder="Swift Code">
                                </div>
                                
                                
                               
                                

<!--
                              

                                <div class="form-group">
                                    <label for="bank_cardid">银行卡持有者身份证号码</label>
                                    <input type="number" name="bank_cardid" required class="form-control" id="bank_cardid" placeholder="银行卡持有者身份证号码">
                                </div>

                                <div class="form-group">
                                    <label for="mobile">银行卡预留电话</label>
                                    <input type="text" name="mobile" required class="form-control" id="mobile" placeholder="银行卡预留电话">
                                </div>-->

                                <button type="submit" class="btn btn-primary">Confirm</button>
                                <button type="reset" class="btn iq-bg-danger">Clear</button>
                            </form>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </div>
</div>
    <!-- Wrapper END -->
    <!-- Footer -->
    <footer class="bg-white iq-footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                   <!-- <ul class="list-inline mb-0">
                        <li class="list-inline-item"><a href="./privacy-policy.html">Privacy Policy</a></li>
                        <li class="list-inline-item"><a href="./terms-of-service.html">Terms of Use</a></li>
                    </ul>-->
                </div>
                <div class="col-lg-6 text-right">
                    Copyright 2020 <a href="#">Vito</a> All Rights Reserved.
                </div>
            </div>
        </div>
    </footer>
    <!-- Footer END -->
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->


    <script src="/merch/js/popper.min.js"></script>
    <script src="/merch/js/jquery.min.js"></script>
    <script src="/merch/js/bootstrap.min.js"></script>
    <!-- Appear JavaScript -->
    <script src="/merch/js/jquery.appear.js"></script>
    <!-- Countdown JavaScript -->
    <script src="/merch/js/countdown.min.js"></script>
    <!-- Counterup JavaScript -->
    <script src="/merch/js/waypoints.min.js"></script>
    <script src="/merch/js/jquery.counterup.min.js"></script>
    <!-- Wow JavaScript -->
    <script src="/merch/js/wow.min.js"></script>
    <!-- Apexcharts JavaScript -->
    <script src="/merch/js/apexcharts.js"></script>
    <!-- Slick JavaScript -->
    <script src="/merch/js/slick.min.js"></script>
    <!-- Select2 JavaScript -->
    <script src="/merch/js/select2.min.js"></script>
    <!-- Owl Carousel JavaScript -->
    <script src="/merch/js/owl.carousel.min.js"></script>
    <!-- Magnific Popup JavaScript -->
    <script src="/merch/js/jquery.magnific-popup.min.js"></script>
    <!-- Smooth S../crollbar JavaScript -->
    <script src="/merch/js/smooth-scrollbar.js"></script>
    <!-- lottie JavaScript -->
    <script src="/merch/js/lottie.js"></script>
    <!-- Chart Custom JavaScript -->
    <script src="/merch/js/chart-custom.js"></script>
    <!-- Custom JavaScript -->
    <script src="/merch/js/custom.js"></script>
    <script type="text/javascript" src="/static/js/qrcode/jeromeetienne-jquery/jquery.qrcode.min.js"></script>

     <script>
       $('#qrcode').qrcode("<?php echo $site; ?>/app/pages/scannel/index.html?shopid=<?php echo $shop['id']; ?>");
     </script>
</body>
</html>