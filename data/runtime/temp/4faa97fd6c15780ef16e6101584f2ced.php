<?php /*a:2:{s:87:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/quest/add.html";i:1625138188;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style type="text/css">
    .red_tips{
        color: #F00;
    }
</style>
</head>
<body>
<div class="wrap">
    <ul class="nav nav-tabs">
        <li ><a href="<?php echo url('quest/index'); ?>">问卷列表</a></li>
        <li class="active"><a ><?php echo lang('ADD'); ?></a></li>
    </ul>
    <form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('quest/addPost'); ?>">
        <div class="form-group">
            <label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>组卷名称</label>
            <div class="col-md-6 col-sm-10">
                <input type="text" class="form-control" id="input-name" name="title" value="">
            </div>
        </div>
        <div id="single">
            <table class="table" id="tableS0">

                <caption>单选题1</caption>
                <thead>
                <tr>
                    <th></th>
                    <th><input type="button" class="btn btn-danger" value="删除" onclick="del('tableS0')" /></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>题目<input type="hidden" name="TestPaperList[0][number]" value="1" /></td>
                    <td><input type="text" class="form-control" name="TestPaperList[0][title]" /></td>
                </tr>
                <tr>
                    <td>A</td>
                    <td>
                        <input type="hidden" name="TestPaperList[0][pid][0][label]" value="A" />
                        <input type="text" class="form-control" name="TestPaperList[0][pid][0][department]" />
                    </td>
                </tr>
                <tr>
                    <td>B</td>
                    <td>
                        <input type="hidden" name="TestPaperList[0][pid][1][label]" value="B" />
                        <input type="text" class="form-control" name="TestPaperList[0][pid][1][department]" />
                    </td>
                </tr>
                <tr>
                    <td>C</td>
                    <td>
                        <input type="hidden" name="TestPaperList[0][pid][2][label]" value="C" />
                        <input type="text" class="form-control" name="TestPaperList[0][pid][2][department]" />
                    </td>
                </tr>
                <tr>
                    <td>D</td>
                    <td>
                        <input type="hidden" name="TestPaperList[0][pid][3][label]" value="D" />
                        <input type="text" class="form-control" name="TestPaperList[0][pid][3][department]" />
                    </td>
                </tr>
                </tbody>
            </table>
        </div>

        <input type="button" class="btn btn-info" onclick="clickText('single')" value="增加单选题" />

        <div id="multiple">
            <table class="table" id="tableM0">

                <caption>多选题1</caption>
                <thead>
                <tr>
                    <th></th>
                    <th><input type="button" class="btn btn-danger" value="删除" onclick="del('tableM0')" /></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>题目<input type="hidden" name="multiple[0][number]" value="1" /></td>
                    <td><input type="text" class="form-control" name="multiple[0][title]" /></td>
                </tr>
                <tr>
                    <td>A</td>
                    <td>
                        <input type="hidden" name="multiple[0][pid][0][label]" value="A" />
                        <input type="text" class="form-control" name="multiple[0][pid][0][department]" />
                    </td>
                </tr>
                <tr>
                    <td>B</td>
                    <td>
                        <input type="hidden" name="multiple[0][pid][1][label]" value="B" />
                        <input type="text" class="form-control" name="multiple[0][pid][1][department]" />
                    </td>
                </tr>
                <tr>
                    <td>C</td>
                    <td>
                        <input type="hidden" name="multiple[0][pid][2][label]" value="C" />
                        <input type="text" class="form-control" name="multiple[0][pid][2][department]" />
                    </td>
                </tr>
                <tr>
                    <td>D</td>
                    <td>
                        <input type="hidden" name="multiple[0][pid][3][label]" value="D" />
                        <input type="text" class="form-control" name="multiple[0][pid][3][department]" />
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
        <input type="button" class="btn btn-info" onclick="clickText('multiple')" value="增加多选题" />
        <div id="questions">
            <table class="table" id="tableQ0">

                <caption>问答题1</caption>
                <thead>
                <tr>
                    <th></th>
                    <th><input type="button" class="btn btn-danger" value="删除" onclick="del('tableQ0')" /></th>
                </tr>
                </thead>
                <tbody>
                <tr>
                    <td>题目 <input type="hidden" name="questions[0][number]" value="1" /></td>
                    <td><textarea class="form-control" name="questions[0][title]"></textarea></td>
                </tr>
                <tr>
                    <td>题目解答</td>
                    <td><textarea class="form-control"  name="questions[0][answer]"></textarea></td>
                </tr>
                </tbody>
            </table>

        </div>
        <input type="button" class="btn btn-info" onclick="clickText('questions')" value="增加问答题" />
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">

                <button type="submit" class="btn btn-primary js-ajax-submit"><?php echo lang('ADD'); ?></button>
                <a class="btn btn-default" href="javascript:history.back(-1);"><?php echo lang('BACK'); ?></a>
            </div>
        </div>

    </form>
</div>
<script src="/static/js/admin.js"></script>
<script>
    $(function(){
        Wind.use('layer');
    });



    function clickText(tab){
        var table =  document.getElementById(tab);
        console.log(table);
        console.log("-->");
        var list = table.querySelectorAll('table');
        console.log(list);
        console.log("---<<<<");
        var i = list.length;
        var j = i+1;
        console.log(i);
        if(tab == 'single') {
            var trHtml = '<table class="table" id="tableS' + i + '">' +
                '<caption>单选题'+j+'</caption>' +
                '<thead>' +
                '<tr>' +
                '<th></th>' +
                '<th><input type="button" class="btn btn-danger" value="删除" onclick="del(\'tableS'+i+'\')" /></th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                '<tr>' +
                '<td>题目<input type="hidden" name="TestPaperList['+i+'][number]" value=\"'+j+'\" /></td>' +
                '<td><input type="text" class="form-control" name="TestPaperList['+i+'][title]" /></td>' +
                '</tr>' +
                '<tr>' +
                ' <td>A</td>' +
                '<input type="hidden" name="TestPaperList['+i+'][pid][0][label]" value="A" />'+
                '<td><input type="text" class="form-control" name="TestPaperList['+i+'][pid][0][department]" /></td>' +
                '</tr>' +
                '<tr>' +
                '<td>B</td>' +
                '<input type="hidden" name="TestPaperList['+i+'][pid][1][label]" value="B" />'+
                '<td><input type="text" class="form-control" name="TestPaperList['+i+'][pid][1][department]" /></td>' +
                '</tr>' +
                '<tr>' +
                '<td>C</td>' +
                '<input type="hidden" name="TestPaperList['+i+'][pid][2][label]" value="C" />'+
                '<td><input type="text" class="form-control" name="TestPaperList['+i+'][pid][2][department]" /></td>' +
                '</tr>' +
                '<tr>' +
                '<td>D</td>' +
                '<input type="hidden" name="TestPaperList['+i+'][pid][3][label]" value="D" />'+
                '<td><input type="text" class="form-control" name="TestPaperList['+i+'][pid][3][department]" /></td>' +
                '</tr>' +
                '</tbody></table>';
        }else if(tab=='multiple'){
            var trHtml = '<table class="table" id="tableM' + i + '">' +
                '<caption>多选'+j+'</caption>' +
                '<thead>' +
                '<tr>' +
                '<th></th>' +
                '<th><input type="button" class="btn btn-danger" value="删除" onclick="del(\'tableM'+i+'\')" /></th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                '<tr>' +
                 '<td>题目<input type="hidden" name="multiple['+i+'][number]" value=\"'+j+'\" /></td>' +
                '<td><input type="text" class="form-control" name="multiple['+i+'][title]" /></td>'+
                '</tr>' +
                '<tr>' +
                ' <td>A</td>' +
                '<input type="hidden" name="multiple['+i+'][pid][0][label]" value="A" />'+
                '<td><input type="text" class="form-control" name="multiple['+i+'][pid][0][department]" /></td>' +
                '</tr>' +
                '<tr>' +
                '<td>B</td>' +
                '<input type="hidden" name="multiple['+i+'][pid][1][label]" value="B" />'+
                '<td><input type="text" class="form-control" name="multiple['+i+'][pid][1][department]" /></td>' +
                '</tr>' +
                '<tr>' +
                '<td>C</td>' +
                '<input type="hidden" name="multiple['+i+'][pid][2][label]" value="C" />'+
                '<td><input type="text" class="form-control" name="multiple['+i+'][pid][2][department]" /></td>' +
                '</tr>' +
                '<tr>' +
                '<td>D</td>' +
                '<input type="hidden" name="multiple['+i+'][pid][3][label]" value="D" />'+
                '<td><input type="text" class="form-control" name="multiple['+i+'][pid][3][department]" /></td>' +
                '</tr>' +
                '</tbody></table>';
        }else{
            var trHtml = '<table class="table" id="tableQ' + i + '">' +
                '<caption>问答'+j+'</caption>' +
                '<thead>' +
                '<tr>' +
                '<th></th>' +
                '<th><input type="button" class="btn btn-danger" value="删除" onclick="del(\'tableQ'+i+'\')" /></th>' +
                '</tr>' +
                '</thead>' +
                '<tbody>' +
                '<tr>' +
                '<td>题目<input type="hidden" name="questions['+i+'][number]" value=\"'+j+'\" /></td>' +
                '<td><input type="text" class="form-control" name="questions['+i+'][title]" /></td>' +
                '</tr>' +
                '<tr>' +
                ' <td>题目解答</td>' +
                '<td><textarea class="form-control"  name="questions['+i+'][answer]"></textarea></td>' +
                '</tr>' +
                '</tbody></table>';

        }
        addHtml(tab,i,trHtml);
    }
    function addHtml(tab,i,html) {
        if(tab=='single'){
            if(i==0){
                $("#single").html(html);
            }else{
                var j = i-1;
                var name = 'tableS'+j;
                $("#"+name+"").after(html);
            }
        }else if(tab=='multiple'){
            if(i==0){
                $("#multiple").html(html);
            }else{
                var j = i-1;
                var name = 'tableM'+j;
                $("#"+name+"").after(html);
            }

        }else{
            if(i==0){
                $("#questions").html(html);
            }else{
                var j = i-1;
                var name = 'tableQ'+j;
                $("#"+name+"").after(html);
            }
        }

    }

    function del(tab){
        $("#"+tab+"").remove();
    }
</script>
</body>
</html>