<?php /*a:2:{s:71:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/admin/coin/index.html";i:1636093284;s:68:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li class="active"><a >列表</a></li>
            <li><a href="<?php echo url('coin/manual'); ?>"><?php echo lang('ADD'); ?></a></li>
		</ul>
		<form class="well form-inline margin-top-20" method="post" action="<?php echo url('coin/index'); ?>">
            Type：
            <select class="form-control" name="trans_type">
				<option value="">全部</option>
                <?php if(is_array($trans_type) || $trans_type instanceof \think\Collection || $trans_type instanceof \think\Paginator): $i = 0; $__LIST__ = $trans_type;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                    <option value="<?php echo $key; ?>" <?php if(input('request.trans_type') != '' && input('request.trans_type') == $key): ?>selected<?php endif; ?>><?php echo $v; ?></option>
                <?php endforeach; endif; else: echo "" ;endif; ?>
			</select>
			提交时间：
			<input class="form-control js-bootstrap-date" name="start_time" id="start_time" value="<?php echo input('request.start_time'); ?>" aria-invalid="false" style="width: 110px;"> - 
            <input class="form-control js-bootstrap-date" name="end_time" id="end_time" value="<?php echo input('request.end_time'); ?>" aria-invalid="false" style="width: 110px;">
            用户ID：
            <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"
                   placeholder="请输入会员ID">
            帖子ID
            <input class="form-control" type="text" name="sid" style="width: 200px;" value="<?php echo input('request.sid'); ?>"
                    placeholder="请输入帖子ID">
			关键字： 
            <input class="form-control" type="text" name="keyword" style="width: 200px;" value="<?php echo input('request.keyword'); ?>"
                   placeholder="请输入Detail 关键字">
			<input type="submit" class="btn btn-primary" value="搜索">
		</form>				
		<form method="post" class="js-ajax-form" >
			<table class="table table-hover table-bordered">
				<thead>
					<tr>
						<th>会员ID</th>
						<th>会员</th>	
						<th>Type</th>
						<th>Detail</th>
						<th>商家名称</th>
                        <th>帖子ID</th>
                        <th>订单号(转账时,和扫码会出现)</th>
                        <th>状态</th>
						<th class="text-right">Coins</th>
						<th class="text-right">提交时间</th>
					</tr>
				</thead>
				<tbody>
					<?php if(is_array($lists) || $lists instanceof \think\Collection || $lists instanceof \think\Paginator): if( count($lists)==0 ) : echo "" ;else: foreach($lists as $key=>$vo): ?>
					<tr>
						<td><?php echo $vo['uid']; ?></td>
						<td><?php echo $vo['userinfo']['user_nickname']; ?> </td>
						<td><?php echo $trans_type[$vo['type']]; ?></td>
						<td><?php echo $vo['title']; ?></td>
						
						<td><?php echo $vo['shop_name']; ?></td>
					
                        <td>
                            <?php if($vo['sid'] > '0'): ?>
                                <?php echo $vo['sid']; ?>
                            <?php endif; ?>
                        </td>
                        	<td><?php echo $vo['ordersn']; ?></td>	
                        	<?php 
						    $str = '';
						    if($vo['is_admin'] == 1){
						       $str =  '<span class="text-success">正常</span>';
						    }else{
						       $str = '<span class="text-danger">失效</span>';
						    }
						 ?>
						<td><?php echo $str; ?></td>	
                        
						<td class="text-right"><?php echo number_format($vo['money'],2); ?></td>				
						<td class="text-right"><?php echo date('Y-m-d H:i',$vo['createtime']); ?></td>						
					</tr>
					<?php endforeach; endif; else: echo "" ;endif; ?>
				</tbody>
			</table>
			<div class="pagination"><?php echo $page; ?></div>

		</form>
	</div>
	<script src="/static/js/admin.js"></script>
	<script>
	 $(function(){
            Wind.use('layer');
           
           
    
	    
    })
   
	    
	</script>
	
</body>
</html>