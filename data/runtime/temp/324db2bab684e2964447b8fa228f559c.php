<?php /*a:2:{s:71:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/admin/main/index.html";i:1623124595;s:68:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<?php 
    if (!function_exists('_get_system_widget')) {
        function _get_system_widget($name){
 switch($name): case "CmfHub": ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">交流方式</h3>
            </div>
            <div class="panel-body home-info">
                <ul class="list-unstyled">
                    <li>
                        <em>官网</em> <span><a href="http://www.thinkcmf.com" target="_blank">www.thinkcmf.com</a></span>
                    </li>
                    <li><em>QQ 群</em> <span>100828313,316669417</span></li>
                    <li><em>联系邮箱</em> <span>catman@thinkcmf.com</span></li>
                </ul>
            </div>
        </div>
    <?php break; case "CmfDocuments": ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">开发手册</h3>
            </div>
            <div class="panel-body home-info">
                <ul class="list-unstyled">
                    <li>
                        <em>完全开发手册</em>
                        <span>
                            <a class="label label-success" href="http://www.thinkcmf.com/doc.html"
                               target="_blank">立即阅读</a>
                        </span>
                    </li>
                    <li>
                        <em>常见问题手册</em>
                        <span>
                            <a class="label label-success" href="http://www.thinkcmf.com/faq.html"
                               target="_blank">立即阅读</a>
                        </span>
                    </li>
                    <li>
                        <em>模板开发教程</em>
                        <span>
                            <a class="label label-success" href="http://www.thinkcmf.com/theme_tutorial.html"
                               target="_blank">立即阅读</a>
                        </span>
                    </li>
                    <li>
                        <em>API开发手册</em>
                        <span>
                            <a class="label label-success" href="http://www.thinkcmf.com/cmf5api.html"
                               target="_blank">立即阅读</a>
                        </span>
                    </li>
                </ul>
            </div>
        </div>
    <?php break; case "MainContributors": ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">主要贡献者</h3>
            </div>
            <div class="panel-body home-info">
                <ul class="list-inline">
                    <li>老猫</li>
                    <li>Sam</li>
                    <li>Tuolaji</li>
                    <li>睡不醒的猪</li>
                    <li>小夏</li>
                    <li>Powerless</li>
                    <li>WelKinVan</li>
                    <li>Kane</li>
                    <li>iYting</li>
                    <li>pl125</li>
                    <li>五五</li>
                </ul>
            </div>
        </div>
    <?php break; case "Contributors": ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">贡献者</h3>
            </div>
            <div class="panel-body home-info">
                <ul class="list-inline">
                    <li>Kin Ho</li>
                    <li><a href="https://www.wzxaini9.cn/" target="_blank">Powerless</a></li>
                    <li>Jess</li>
                    <li>木兰情</li>
                    <li>Labulaka</li>
                    <li><a href="http://www.wzcoder.com/" target="_blank">WelKinVan</a></li>
                    <li><a href="http://blog.sina.com.cn/u/1918098881" target="_blank">Jeson</a></li>
                    <li>Yim</li>
                    <li><a href="http://www.jamlee.cn/" target="_blank">Jamlee</a></li>
                    <li>香香咸蛋黄</li>
                    <li><a href="http://www.xdmeng.cn" target="_blank">小凯</a></li>
                    <li>Co</li>
                    <li>Rainfer</li>
                    <li><a href="http://www.yaoweixiu.cn/" target="_blank">Kane</a></li>
                    <li>北岸的云</li>
                    <li>pl125</li>
                    <li><a href="http://www.wuwuseo.com/" target="_blank">五五</a></li>
                    <li><a href="https://www.qdcrazy.cc/" target="_blank">惠达浪</a></li>
                </ul>
            </div>
        </div>
    <?php break; ?>
<?php endswitch; 
        }
    }
 ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style>
    .home-info li em {
        float: left;
        width: 120px;
        font-style: normal;
        font-weight: bold;
    }

    .home-info ul {
        padding: 0;
        margin: 0;
    }

    .panel {
        margin-bottom: 0;
    }

    .grid-sizer {
        width: 10%;
    }

    .grid-item {
        margin-bottom: 5px;
        padding: 5px;
    }

    .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
        padding-left: 5px;
        padding-right: 5px;
        float: none;
    }

</style>
<?php 
    \think\facade\Hook::listen('admin_before_head_end',null,false);
 ?>
</head>
<body>
<div class="wrap">
    <div class="container-fluid">

        <div class="row">
            <div class="col-sm-6 col-lg-3">
                <div class="card bg-primary">
                    <div class="card-body clearfix">
                        <div class="pull-right">
                            <p class="h6 text-white m-t-0">订单总数</p>
                            <p class="h3 text-white m-b-0"><?php echo $orderNum; ?></p>
                        </div>
                        <div class="pull-left"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="mdi mdi-currency-cny fa-1-5x"></i></span> </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card bg-danger">
                    <div class="card-body clearfix">
                        <div class="pull-right">
                            <p class="h6 text-white m-t-0">用户总数</p>
                            <p class="h3 text-white m-b-0"><?php echo $userNum; ?></p>
                        </div>
                        <div class="pull-left"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="mdi mdi-account fa-1-5x"></i></span> </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card bg-success">
                    <div class="card-body clearfix">
                        <div class="pull-right">
                            <p class="h6 text-white m-t-0">帖子总数</p>
                            <p class="h3 text-white m-b-0"><?php echo $noteNum; ?></p>
                        </div>
                        <div class="pull-left"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="mdi mdi-arrow-down-bold fa-1-5x"></i></span> </div>
                    </div>
                </div>
            </div>

            <div class="col-sm-6 col-lg-3">
                <div class="card bg-purple">
                    <div class="card-body clearfix">
                        <div class="pull-right">
                            <p class="h6 text-white m-t-0">新增订单</p>
                            <p class="h3 text-white m-b-0"><?php echo $newOrderNum; ?>条</p>
                        </div>
                        <div class="pull-left"> <span class="img-avatar img-avatar-48 bg-translucent"><i class="mdi mdi-comment-outline fa-1-5x"></i></span> </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4>每周用户</h4>
                    </div>
                    <div class="card-body">
                        <canvas class="js-chartjs-bars"></canvas>
                    </div>
                </div>
            </div>

            <div class="col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4></h4>
                    </div>
                    <div class="card-body">
                        <canvas class="js-chartjs-lines"></canvas>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>系统信息</h4>
                    </div>
                    <div class="card-body">
                        <!--<div class="table-responsive">-->
                            <!--<table class="table table-hover">-->
                                <!--<thead>-->
                                <!--<tr>-->
                                    <!--<th>#</th>-->
                                    <!--<th>项目名称</th>-->
                                    <!--<th>开始日期</th>-->
                                    <!--<th>截止日期</th>-->
                                    <!--<th>状态</th>-->
                                    <!--<th>进度</th>-->
                                <!--</tr>-->
                                <!--</thead>-->
                                <!--<tbody>-->
                                <!--<tr>-->
                                    <!--<td>1</td>-->
                                    <!--<td>设计新主题</td>-->
                                    <!--<td>10/02/2019</td>-->
                                    <!--<td>12/05/2019</td>-->
                                    <!--<td><span class="label label-warning">待定</span></td>-->
                                    <!--<td>-->
                                        <!--<div class="progress progress-striped progress-sm">-->
                                            <!--<div class="progress-bar progress-bar-warning" style="width: 45%;"></div>-->
                                        <!--</div>-->
                                    <!--</td>-->
                                <!--</tr>-->
                                <!--<tr>-->
                                    <!--<td>2</td>-->
                                    <!--<td>网站重新设计</td>-->
                                    <!--<td>01/03/2019</td>-->
                                    <!--<td>12/04/2019</td>-->
                                    <!--<td><span class="label label-success">进行中</span></td>-->
                                    <!--<td>-->
                                        <!--<div class="progress progress-striped progress-sm">-->
                                            <!--<div class="progress-bar progress-bar-success" style="width: 30%;"></div>-->
                                        <!--</div>-->
                                    <!--</td>-->
                                <!--</tr>-->
                                <!--<tr>-->
                                    <!--<td>3</td>-->
                                    <!--<td>模型设计</td>-->
                                    <!--<td>10/10/2019</td>-->
                                    <!--<td>12/11/2019</td>-->
                                    <!--<td><span class="label label-warning">待定</span></td>-->
                                    <!--<td>-->
                                        <!--<div class="progress progress-striped progress-sm">-->
                                            <!--<div class="progress-bar progress-bar-warning" style="width: 25%;"></div>-->
                                        <!--</div>-->
                                    <!--</td>-->
                                <!--</tr>-->
                                <!--<tr>-->
                                    <!--<td>4</td>-->
                                    <!--<td>后台管理系统模板设计</td>-->
                                    <!--<td>25/01/2019</td>-->
                                    <!--<td>09/05/2019</td>-->
                                    <!--<td><span class="label label-success">进行中</span></td>-->
                                    <!--<td>-->
                                        <!--<div class="progress progress-striped progress-sm">-->
                                            <!--<div class="progress-bar progress-bar-success" style="width: 55%;"></div>-->
                                        <!--</div>-->
                                    <!--</td>-->
                                <!--</tr>-->
                                <!--<tr>-->
                                    <!--<td>5</td>-->
                                    <!--<td>前端设计</td>-->
                                    <!--<td>10/10/2019</td>-->
                                    <!--<td>12/12/2019</td>-->
                                    <!--<td><span class="label label-danger">未开始</span></td>-->
                                    <!--<td>-->
                                        <!--<div class="progress progress-striped progress-sm">-->
                                            <!--<div class="progress-bar progress-bar-danger" style="width: 0%;"></div>-->
                                        <!--</div>-->
                                    <!--</td>-->
                                <!--</tr>-->
                                <!--<tr>-->
                                    <!--<td>6</td>-->
                                    <!--<td>桌面软件测试</td>-->
                                    <!--<td>10/01/2019</td>-->
                                    <!--<td>29/03/2019</td>-->
                                    <!--<td><span class="label label-success">进行中</span></td>-->
                                    <!--<td>-->
                                        <!--<div class="progress progress-striped progress-sm">-->
                                            <!--<div class="progress-bar progress-bar-success" style="width: 75%;"></div>-->
                                        <!--</div>-->
                                    <!--</td>-->
                                <!--</tr>-->
                                <!--<tr>-->
                                    <!--<td>7</td>-->
                                    <!--<td>APP改版开发</td>-->
                                    <!--<td>25/02/2019</td>-->
                                    <!--<td>12/05/2019</td>-->
                                    <!--<td><span class="label label-danger">暂停</span></td>-->
                                    <!--<td>-->
                                        <!--<div class="progress progress-striped progress-sm">-->
                                            <!--<div class="progress-bar progress-bar-danger" style="width: 15%;"></div>-->
                                        <!--</div>-->
                                    <!--</td>-->
                                <!--</tr>-->
                                <!--<tr>-->
                                    <!--<td>8</td>-->
                                    <!--<td>Logo设计</td>-->
                                    <!--<td>10/02/2019</td>-->
                                    <!--<td>01/03/2019</td>-->
                                    <!--<td><span class="label label-warning">完成</span></td>-->
                                    <!--<td>-->
                                        <!--<div class="progress progress-striped progress-sm">-->
                                            <!--<div class="progress-bar progress-bar-success" style="width: 100%;"></div>-->
                                        <!--</div>-->
                                    <!--</td>-->
                                <!--</tr>-->
                                <!--</tbody>-->
                            <!--</table>-->
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>
</div>
<script src="/static/js/admin.js"></script>
<!--图表插件-->
<link href="/static/css/style.css" rel="stylesheet">
<link href="/static/css/bootstrap.min.css" rel="stylesheet">
<script type="text/javascript" src="/static/js/Chart.js"></script>
<script type="text/javascript">
    $(document).ready(function(e) {
        var $dashChartBarsCnt  = jQuery( '.js-chartjs-bars' )[0].getContext( '2d' ),
            $dashChartLinesCnt = jQuery( '.js-chartjs-lines' )[0].getContext( '2d' );
        var aa = <?php echo $newUser; ?>;
        var $dashChartBarsData = {
            labels: ['周一', '周二', '周三', '周四', '周五', '周六', '周日'],
            datasets: [
                {
                    label: '注册用户',
                    borderWidth: 1,
                    borderColor: 'rgba(0,0,0,0)',
                    backgroundColor: 'rgba(51,202,185,0.5)',
                    hoverBackgroundColor: "rgba(51,202,185,0.7)",
                    hoverBorderColor: "rgba(0,0,0,0)",
                    data: aa
                }
            ]
        };
        var $dashChartLinesData = {
            labels: ['2003', '2004', '2005', '2006', '2007', '2008', '2009', '2010', '2011', '2012', '2013', '2014'],
            datasets: [
                {
                    label: '交易资金',
                    data: [20, 25, 40, 30, 45, 40, 55, 40, 48, 40, 42, 50],
                    borderColor: '#358ed7',
                    backgroundColor: 'rgba(53, 142, 215, 0.175)',
                    borderWidth: 1,
                    fill: false,
                    lineTension: 0.5
                }
            ]
        };

        new Chart($dashChartBarsCnt, {
            type: 'bar',
            data: $dashChartBarsData
        });

        // var myLineChart = new Chart($dashChartLinesCnt, {
        //     type: 'line',
        //     data: $dashChartLinesData,
        // });
    });
</script>
<?php 
    $lang_set=defined('LANG_SET')?LANG_SET:'';
    $thinkcmf_version=cmf_version();
 ?>
<script>

    Wind.css('dragula');
    Wind.use('masonry', 'imagesloaded', 'dragula', function () {
        var $homeGrid = $('.home-grid').masonry({
            // set itemSelector so .grid-sizer is not used in layout
            itemSelector: '.grid-item',
            // use element for option
            columnWidth: '.grid-sizer',
            percentPosition: true,
            horizontalOrder: false,
            transitionDuration: 0
        });

        $homeGrid.masonry('on', 'layoutComplete', function (event, laidOutItems) {
        });


        $homeGrid.masonry();

        var containers = [];
        $('.home-grid .grid-item').each(function () {
            containers.push(this);
        });
        dragula(containers, {
            isContainer: function (el) {
                return false; // only elements in drake.containers will be taken into account
            },
            moves: function (el, source, handle, sibling) {
                return true; // elements are always draggable by default
            },
            accepts: function (el, target, source, sibling) {
                return true; // elements can be dropped in any of the `containers` by default
            },
            invalid: function (el, handle) {
                return false; // don't prevent any drags from initiating by default
            },
            direction: 'vertical',             // Y axis is considered when determining where an element would be dropped
            copy: false,                       // elements are moved by default, not copied
            copySortSource: false,             // elements in copy-source containers can be reordered
            revertOnSpill: false,              // spilling will put the element back where it was dragged from, if this is true
            removeOnSpill: false,              // spilling will `.remove` the element, if this is true
            mirrorContainer: document.body,    // set the element that gets mirror elements appended
            ignoreInputTextSelection: true     // allows users to select input text, see details below
        }).on('drop', function (el, target, source, sibling) {
            var $target          = $(target);
            var targetClasses    = $target.attr('class');
            var targetDataWidget = $target.data('widget');
            var targetDataSystem = $target.data('system');
            var $source          = $(source);
            var sourceClasses    = $source.attr('class');
            var sourceDataWidget = $source.data('widget');
            var sourceDataSystem = $source.data('system');
            $(source).append($(target).find('.dashboard-box').not('.gu-transit'));
            $(target).append(el);
            $target.attr('class', sourceClasses);
            $target.data('widget', sourceDataWidget);
            $target.data('system', sourceDataSystem);

            $source.attr('class', targetClasses);
            $source.data('widget', targetDataWidget);
            $source.data('system', targetDataSystem);
            $homeGrid.masonry();

            _widgetSort();
        }).on('shadow', function (el, container, source) {
            $homeGrid.masonry();
        });


    });

    function _widgetSort() {

        var widgets = [];
        $('.home-grid .grid-item').each(function () {
            var $this = $(this);

            widgets.push({
                name: $this.data('widget'),
                is_system: $this.data('system')
            });
        });

        $.ajax({
            url: "<?php echo url('main/dashboardWidget'); ?>",
            type: 'post',
            dataType: 'json',
            data: {widgets: widgets},
            success: function (data) {

            },
            error: function () {

            },
            complete: function () {

            }
        });
    }

    //获取官方通知
    $.getJSON("//www.thinkcmf.com/service/sms_jsonp.php?lang=<?php echo $lang_set; ?>&v=<?php echo $thinkcmf_version; ?>&callback=?",
        function (data) {
            var tpl      = '<li><em class="title"></em><span class="content"></span></li>';
            var $notices = $("#thinkcmf-notices");
            $notices.empty();
            if (data.length > 0) {
                $('#thinkcmf-notices-grid').show();
                $.each(data, function (i, n) {
                    var $tpl = $(tpl);
                    $(".title", $tpl).html(n.title);
                    $(".content", $tpl).html(n.content);
                    $notices.append($tpl);
                });
            } else {
                $notices.append("<li>^_^,<?php echo lang('NO_NOTICE'); ?>~~</li>");
            }

        });
</script>
<?php 
    \think\facade\Hook::listen('admin_before_body_end',null,false);
 ?>
</body>
</html>
