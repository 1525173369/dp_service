<?php /*a:2:{s:94:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/user/admin_index/index.html";i:1627270758;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a><?php echo lang('USER_INDEXADMIN_INDEX'); ?></a></li>
        <li><a href="<?php echo url('adminIndex/add'); ?>"><?php echo lang('ADD'); ?></a></li>
    </ul>
    <form class="well form-inline margin-top-20" method="post" action="<?php echo url('user/adminIndex/index'); ?>">
        用户ID：
        <input class="form-control" type="text" name="uid" style="width: 200px;" value="<?php echo input('request.uid'); ?>"
               placeholder="请输入用户ID">
        关键字：
        <input class="form-control" type="text" name="keyword" style="width: 200px;" value="<?php echo input('request.keyword'); ?>"
               placeholder="用户名/昵称/邮箱/手机">
        <input type="submit" class="btn btn-primary" value="搜索"/>
        <a class="btn btn-danger" href="<?php echo url('user/adminIndex/index'); ?>">清空</a>
    </form>
    <form method="post" class="js-ajax-form">
            <div class="table-actions">
                 <a class="btn btn-xs btn-primary" id="mail">发送站内信</a>
            </div>
            
            <div class="table-actions">
                 <a class="btn btn-xs btn-primary" id="paper">发送问卷调查</a>
            </div>
            
        <table class="table table-hover table-bordered">
            <thead>
            <tr>
                <th></th>
                <th>ID</th>
                <th>认证</th>
                <th><?php echo lang('USERNAME'); ?></th>
             
                <th><?php echo lang('NICENAME'); ?></th>
               
                <th><?php echo lang('AVATAR'); ?></th>
                <th><?php echo lang('EMAIL'); ?></th>
                <th>手机</th>
                <th>生日</th> 
                <th>地址</th>
                <th>邀请人</th>
                <th><?php echo lang('REGISTRATION_TIME'); ?></th>
                <th><?php echo lang('LAST_LOGIN_TIME'); ?></th>
                <th><?php echo lang('LAST_LOGIN_IP'); ?></th>
                <th><?php echo lang('STATUS'); ?></th>

                <th><?php echo lang('ACTIONS'); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php 
                $user_statuses=array("0"=>lang('USER_STATUS_BLOCKED'),"1"=>lang('USER_STATUS_ACTIVATED'),"2"=>lang('USER_STATUS_UNVERIFIED'));
             if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): if( count($list)==0 ) : echo "" ;else: foreach($list as $key=>$vo): ?>
                <tr>
                    <td>
                        <input type="checkbox" class="js-check" data-yid="js-check-y" data-xid="js-check-x" name="ids[]" value="<?php echo $vo['id']; ?>">
                    </td>
                    <td><?php echo $vo['id']; ?></td>
            

                    <td><?php echo $vo['major']==1 ? "专属用户" : ($vo['status']==1?"认证用户":"游客"); ?></td>
                    
                    <td><?php echo !empty($vo['user_login']) ? $vo['user_login'] : ($vo['mobile']?$vo['mobile']:lang('THIRD_PARTY_USER')); ?>
                    </td>

                      
        
                    
                    <td><?php echo !empty($vo['user_nickname']) ? $vo['user_nickname'] : lang('NOT_FILLED'); ?></td>
                    <!--<td><img width="25" height="25" src="<?php echo url('user/public/avatar',array('id'=>$vo['id'])); ?>"/></td>-->
                    <td><img width="25" height="25" src="<?php echo !empty($vo['avatar']) ? $vo['avatar'] : lang('NOT_FILLED'); ?>"/></td>
                    <td><?php echo $vo['user_email']; ?></td>
                    <td><?php echo $vo['mobile']; ?></td>
                     <td><?php echo $vo['birthday']; ?></td> 
                    <td><?php echo $vo['address']; ?></td>
                    <td><?php echo !empty($vo['name']) ? $vo['name'] : '暂无'; ?>/<?php echo $vo['pid']; ?></td>
                    <td><?php echo date('Y-m-d H:i:s',$vo['create_time']); ?></td>
                    <td><?php echo date('Y-m-d H:i:s',$vo['last_login_time']); ?></td>
                    <td><?php echo $vo['last_login_ip']; ?></td>

                    <td>
                        
                        <?php switch($vo['user_status']): case "0": ?>
                                <span class="label label-danger"><?php echo $user_statuses[$vo['user_status']]; ?></span>
                            <?php break; case "1": ?>
                                <span class="label label-success"><?php echo $user_statuses[$vo['user_status']]; ?></span>
                            <?php break; case "2": ?>
                                <span class="label label-warning"><?php echo $user_statuses[$vo['user_status']]; ?></span>
                            <?php break; ?>
                        <?php endswitch; ?>
                    </td>
                    <td>
                        <?php if($vo['id'] != '1'): if(empty($vo['user_status']) || (($vo['user_status'] instanceof \think\Collection || $vo['user_status'] instanceof \think\Paginator ) && $vo['user_status']->isEmpty())): ?>
                                <a class="btn btn-xs btn-success js-ajax-dialog-btn"
                                   href="<?php echo url('adminIndex/cancelban',array('id'=>$vo['id'])); ?>"
                                   data-msg="<?php echo lang('ACTIVATE_USER_CONFIRM_MESSAGE'); ?>"><?php echo lang('ACTIVATE_USER'); ?></a>
                                <?php else: ?>
                                <a class="btn btn-xs btn-warning js-ajax-dialog-btn"
                                   href="<?php echo url('adminIndex/ban',array('id'=>$vo['id'])); ?>"
                                   data-msg="<?php echo lang('BLOCK_USER_CONFIRM_MESSAGE'); ?>"><?php echo lang('BLOCK_USER'); ?></a>
                            <?php endif; else: ?>
                            
                            <a class="btn btn-xs btn-warning disabled"><?php echo lang('BLOCK_USER'); ?></a>
                            
                        <?php endif; ?>
                        
                        
                        
                        
                        <a class="btn btn-xs btn-primary" href='<?php echo url("adminIndex/edit",array("id"=>$vo["id"])); ?>'><?php echo lang('EDIT'); ?></a>
                        <a class="btn btn-xs btn-danger js-ajax-delete" href="<?php echo url('adminIndex/del',array('id'=>$vo['id'])); ?>"><?php echo lang('DELETE'); ?></a>
                    </td>
                </tr>
            <?php endforeach; endif; else: echo "" ;endif; ?>
            </tbody>
        </table>
        <div class="pagination"><?php echo $page; ?></div>
    </form>
</div>
<script src="/static/js/admin.js"></script>
    <script type="text/javascript">
        $(function(){
            Wind.use('layer');
        });

        $("#mail").click(function () {

        var ids = [];
        $("input[name='ids[]']:checked").each(function(i){
        ids.push($(this).val())

        })
         if(ids.length > 0){
            var url = "/admin/message/index?user_id_array="+ids;
            layer.open({
                type: 2,
                title: '站内信',
                shadeClose: true,
                shade: 0.8,
                area: ['680px', '580px'],
                content: url
            });
             
             
         }else{
            layer.msg('请选择会员',{icon:2});
         }



        });
        
        
        $("#paper").click(function () {

        var ids = [];
        $("input[name='ids[]']:checked").each(function(i){
        ids.push($(this).val())

        })
         if(ids.length > 0){
            var url = "/admin/message/paper?user_id_array="+ids;
            layer.open({
                type: 2,
                title: '问卷调查',
                shadeClose: true,
                shade: 0.8,
                area: ['680px', '580px'],
                content: url
            });
             
             
         }else{
            layer.msg('请选择会员',{icon:2});
         }



        });
        
        
        
    </script>
</body>
</html>