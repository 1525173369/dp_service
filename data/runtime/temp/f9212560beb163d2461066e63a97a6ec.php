<?php /*a:2:{s:90:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/setting/site.html";i:1627627404;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
<div class="wrap js-check-wrap">
    <ul class="nav nav-tabs">
        <li class="active"><a href="#A" data-toggle="tab"><?php echo lang('WEB_SITE_INFOS'); ?></a></li>
        <!--<li><a href="#B" data-toggle="tab"><?php echo lang('SEO_SETTING'); ?></a></li>-->
        <li><a href="#C" data-toggle="tab">APP版本管理</a></li>
        <li><a href="#D" data-toggle="tab">登录开关</a></li>
        <!--<li><a href="#C" data-toggle="tab"><?php echo lang('URL_SETTING'); ?></a></li>-->
        <!--<li><a href="#E" data-toggle="tab"><?php echo lang('COMMENT_SETTING'); ?></a></li>-->
        <!--<li><a href="#F" data-toggle="tab">用户注册设置</a></li>-->
        <!--<li><a href="#G" data-toggle="tab">CDN设置</a></li>-->
    </ul>

    <form class="form-horizontal js-ajax-form margin-top-20" role="form" action="<?php echo url('setting/sitePost'); ?>"
          method="post">
        <fieldset>
            <div class="tabbable">
                <div class="tab-content">
                    <div class="tab-pane active" id="A">
                        <!--<div class="form-group">-->
                        <!--    <label for="input-maintain_switch" class="col-sm-2 control-label">网站维护</label>-->
                        <!--    <div class="col-md-6 col-sm-10">-->
                        <!--        <select class="form-control" name="options[maintain_switch]">-->
                        <!--            <option value="0">关闭</option>-->
                        <!--            <option value="1" <?php if($site_info['maintain_switch'] == '1'): ?>selected<?php endif; ?>>开启</option>-->
                        <!--        </select>-->
                        <!--        <p class="help-block">网站维护开启后，无法开启直播，进入直播间</p>-->
                        <!--    </div>-->
                        <!--</div>-->
                        
                        <!--<div class="form-group">-->
                        <!--    <label for="input-maintain_tips" class="col-sm-2 control-label">维护提示</label>-->
                        <!--    <div class="col-md-6 col-sm-10">-->
                        <!--        <textarea class="form-control" id="input-maintain_tips" name="options[maintain_tips]" ><?php echo (isset($site_info['maintain_tips']) && ($site_info['maintain_tips'] !== '')?$site_info['maintain_tips']:''); ?></textarea>-->
                        <!--        <p class="help-block">维护提示信息（200字以内）</p>-->
                        <!--    </div>-->
                        <!--</div>-->

                        <!--<div class="form-group">-->
                        <!--    <label for="input-company_name" class="col-sm-2 control-label">公司名称</label>-->
                        <!--    <div class="col-md-6 col-sm-10">-->
                        <!--        <input type="text" class="form-control" id="input-company_name" name="options[company_name]"-->
                        <!--               value="<?php echo (isset($site_info['company_name']) && ($site_info['company_name'] !== '')?$site_info['company_name']:''); ?>">-->
                        <!--               <p class="help-block">公司名称(网站下载页关于我们使用)</p>-->
                        <!--    </div>-->
                        <!--</div>-->

                        <!--<div class="form-group">-->
                        <!--    <label  for="input-company_desc" class="col-sm-2 control-label">公司简介</label>-->
                        <!--    <div class="col-md-6 col-sm-10">        -->
                        <!--        <textarea  class="form-control" name="options[company_desc]"><?php echo (isset($site_info['company_desc']) && ($site_info['company_desc'] !== '')?$site_info['company_desc']:''); ?></textarea>-->
                        <!--        <p class="help-block">公司简介（网站下载页关于我们使用,字数在200字以内）</p>-->
                        <!--    </div>-->
                        <!--</div>-->
                        
                        <div class="form-group">
                            <label for="input-site-name" class="col-sm-2 control-label"><span
                                    class="form-required"></span><?php echo lang('WEBSITE_NAME'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site-name" name="options[site_name]"
                                       value="<?php echo (isset($site_info['site_name']) && ($site_info['site_name'] !== '')?$site_info['site_name']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-site" class="col-sm-2 control-label"><span
                                    class="form-required"></span>网站域名</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site-name" name="options[site]" value="<?php echo (isset($site_info['site']) && ($site_info['site'] !== '')?$site_info['site']:''); ?>">
                                <p class="help-block">格式： http(s)://xxxx.com(:端口号)</p>
                            </div> 
                        </div>

                        <div class="form-group">
                            <label for="input-site" class="col-sm-2 control-label"><span
                                    class="form-required"></span>商家提现提醒</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site-merch_cash_tips" name="options[merch_cash_tips]" value="<?php echo (isset($site_info['merch_cash_tips']) && ($site_info['merch_cash_tips'] !== '')?$site_info['merch_cash_tips']:''); ?>">
                                <p class="help-block">商家提现时的提醒tips</p>
                            </div>
                        </div>



                        <div class="form-group">
                            <label for="input-site-user_least_coin" class="col-sm-2 control-label"><span
                                    class="form-required"></span>用户最少提现金币</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site-user_least_coin" name="options[user_least_coin]"
                                       value="<?php echo (isset($site_info['user_least_coin']) && ($site_info['user_least_coin'] !== '')?$site_info['user_least_coin']:''); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-site-merch_least_coin" class="col-sm-2 control-label"><span
                                    class="form-required"></span>商家最少提现金币</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site-merch_least_coin" name="options[merch_least_coin]"
                                       value="<?php echo (isset($site_info['merch_least_coin']) && ($site_info['merch_least_coin'] !== '')?$site_info['merch_least_coin']:''); ?>">
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="input-site-user_cash_rate" class="col-sm-2 control-label"><span
                                    class="form-required"></span>用户金币比例</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site-user_cash_rate" name="options[user_cash_rate]"
                                       value="<?php echo (isset($site_info['user_cash_rate']) && ($site_info['user_cash_rate'] !== '')?$site_info['user_cash_rate']:''); ?>">
                                <p class="help-block">(填入100:1表示100金币兑换1马币)!!!!切勿填错,如果填错可能导致整个提现模块瘫痪!!!</p>

                            </div>

                        </div>


                        <div class="form-group">
                            <label for="input-site-merch_cash_rate" class="col-sm-2 control-label"><span
                                    class="form-required"></span>商家金币比例</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site-merch_cash_rate" name="options[merch_cash_rate]"
                                       value="<?php echo (isset($site_info['merch_cash_rate']) && ($site_info['merch_cash_rate'] !== '')?$site_info['merch_cash_rate']:''); ?>">
                                <p class="help-block">(填入100:1表示100金币兑换1马币)!!!!切勿填错,如果填错可能导致整个提现模块瘫痪!!!</p>

                            </div>

                        </div>





                        <div class="form-group">
                            <label for="input-ipa_ewm" class="col-sm-2 control-label">app启动图</label>
                            <div class="col-md-6 col-sm-10">
                        <ul id="photos" class="pic-list list-unstyled form-inline">
                            <?php if(!(empty($site_info['start_up']) || (($site_info['start_up'] instanceof \think\Collection || $site_info['start_up'] instanceof \think\Paginator ) && $site_info['start_up']->isEmpty()))): if(is_array($site_info['start_up']) || $site_info['start_up'] instanceof \think\Collection || $site_info['start_up'] instanceof \think\Paginator): if( count($site_info['start_up'])==0 ) : echo "" ;else: foreach($site_info['start_up'] as $key=>$vo): $img_url=cmf_get_image_preview_url($vo); ?>
                                    <li id="saved-image<?php echo $key; ?>">
                                        <input id="photo-<?php echo $key; ?>" type="text" style="width: 50%;display:none" readonly name="options[start_up][]"
                                               value="<?php echo $img_url; ?>">
                                       <input id="photo-{id}" style="width: 50%;" type="text"  name="options[start_up_url][]" value="<?php echo $site_info['start_up_url'][$key]; ?>" placeholder="请输入跳转地址">
                                        <img id="photo-<?php echo $key; ?>-preview"
                                             src="<?php echo cmf_get_image_preview_url($vo); ?>"
                                             style="height:36px;width: 36px;"
                                             onclick="parent.imagePreviewDialog(this.src);">
                                        <a href="javascript:uploadOneImage('图片上传','#photo-<?php echo $key; ?>');">替换</a>
                                        <a href="javascript:(function(){$('#saved-image<?php echo $key; ?>').remove();})();">移除</a>
                                        <span>(点击图片可预览,输入框为跳转地址)</span>
                                    </li>
                                <?php endforeach; endif; else: echo "" ;endif; ?>
                            <?php endif; ?>


                        </ul>
                        <a href="javascript:uploadMultiImage('图片上传','#photos','photos-item-wrapper');">启动图上传</a>
                            </div>
                        </div>
                        <script type="text/html" id="photos-item-wrapper">
                            <li id="savedimage{id}">
                                <input id="photo-{id}" style="width: 50%;display:none" type="text" readonly name="options[start_up][]" value="{filepath}">
                                <input id="photo-{id}" style="width: 50%;" type="text"  name="options[start_up_url][]" value="" placeholder="请输入跳转地址">
                                <img id="photo-{id}-preview" src="{url}" style="height:36px;width: 36px;" onclick="parent.imagePreviewDialog(this.src);">
                                <a href="javascript:uploadOneImage('图片上传','#photo-{id}');">替换</a>
                                <a href="javascript:(function(){$('#savedimage{id}').remove();})();">移除</a>

                                <span>(点击图片可预览,输入框为跳转地址)</span>
                            </li>
                        </script>


                        <!--  <div class="form-group">-->
                        <!--    <label for="input-site" class="col-sm-2 control-label"><span-->
                        <!--            class="form-required"></span>app启动动画图跳转链接</label>-->
                        <!--    <div class="col-md-6 col-sm-10">-->
                        <!--        <input type="text" class="form-control" id="input-site-name" name="options[url]" value="<?php echo (isset($site_info['url']) && ($site_info['url'] !== '')?$site_info['url']:''); ?>">-->
                        <!--    </div> -->
                        <!--</div>-->
                        
                        <div class="form-group" style="display:none;">
                            <label for="input-admin_url_password" class="col-sm-2 control-label">
                                后台加密码
                                <a href="http://www.thinkcmf.com/faq.html?url=https://www.kancloud.cn/thinkcmf/faq/493509"
                                   title="查看帮助手册"
                                   data-toggle="tooltip"
                                   target="_blank"><i class="fa fa-question-circle"></i></a>
                            </label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-admin_url_password"
                                       name="admin_settings[admin_password]"
                                       value="<?php echo (isset($admin_settings['admin_password']) && ($admin_settings['admin_password'] !== '')?$admin_settings['admin_password']:''); ?>"
                                       id="js-site-admin-url-password">
                                <p class="help-block">英文字母数字，不能为纯数字</p>
                                <p class="help-block" style="color: red;">
                                    设置加密码后必须通过以下地址访问后台,请劳记此地址，为了安全，您也可以定期更换此加密码!</p>
                                <?php 
                                    $root=cmf_get_root();
                                    $root=empty($root)?'':'/'.$root;
                                    $site_domain = cmf_get_domain().$root;
                                 ?>
                                <p class="help-block">后台登录地址：<span id="js-site-admin-url"><?php echo $site_domain; ?>/<?php echo (isset($admin_settings['admin_password']) && ($admin_settings['admin_password'] !== '')?$admin_settings['admin_password']:'admin'); ?></span>
                                </p>
                            </div>
                        </div>

                        <div class="form-group" style="display:none;">
                            <label for="input-site_admin_theme" class="col-sm-2 control-label">后台模板</label>
                            <div class="col-md-6 col-sm-10">
                                <?php 
                                    $site_admin_theme=empty($admin_settings['admin_theme'])?'':$admin_settings['admin_theme'];
                                 ?>
                                <select class="form-control" name="admin_settings[admin_theme]"
                                        id="input-site_admin_theme">
                                    <?php if(is_array($admin_themes) || $admin_themes instanceof \think\Collection || $admin_themes instanceof \think\Paginator): if( count($admin_themes)==0 ) : echo "" ;else: foreach($admin_themes as $key=>$vo): $admin_theme_selected = $site_admin_theme == $vo ? "selected" : ""; ?>
                                        <option value="<?php echo $vo; ?>" <?php echo $admin_theme_selected; ?>><?php echo $vo; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="display:none;">
                            <label for="input-site_adminstyle" class="col-sm-2 control-label"><?php echo lang('WEBSITE_ADMIN_THEME'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <?php 
                                    $site_admin_style=empty($admin_settings['admin_style'])?cmf_get_admin_style():$admin_settings['admin_style'];
                                 ?>
                                <select class="form-control" name="admin_settings[admin_style]"
                                        id="input-site_adminstyle">
                                    <?php if(is_array($admin_styles) || $admin_styles instanceof \think\Collection || $admin_styles instanceof \think\Paginator): if( count($admin_styles)==0 ) : echo "" ;else: foreach($admin_styles as $key=>$vo): $admin_style_selected = $site_admin_style == $vo ? "selected" : ""; ?>
                                        <option value="<?php echo $vo; ?>" <?php echo $admin_style_selected; ?>><?php echo $vo; ?></option>
                                    <?php endforeach; endif; else: echo "" ;endif; ?>
                                </select>
                            </div>
                        </div>
                        <?php if(APP_DEBUG && false): ?>
                            <div class="form-group">
                                <label for="input-default_app" class="col-sm-2 control-label">默认应用</label>
                                <div class="col-md-6 col-sm-10">
                                    <?php 
                                        $site_default_app=empty($cmf_settings['default_app'])?'demo':$cmf_settings['default_app'];
                                     ?>
                                    <select class="form-control" name="cmf_settings[default_app]"
                                            id="input-default_app">
                                        <?php if(is_array($apps) || $apps instanceof \think\Collection || $apps instanceof \think\Paginator): if( count($apps)==0 ) : echo "" ;else: foreach($apps as $key=>$vo): $default_app_selected = $site_default_app == $vo ? "selected" : "";
                                             ?>
                                            <option value="<?php echo $vo; ?>" <?php echo $default_app_selected; ?>><?php echo $vo; ?></option>
                                        <?php endforeach; endif; else: echo "" ;endif; ?>
                                    </select>
                                </div>
                            </div>
                        <?php endif; ?>
                        <!--
                        <div class="form-group">
                            <label for="input-html_cache_on" class="col-sm-2 control-label"><?php echo lang('HTML_CACHE'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <?php $html_cache_on_checked=empty($html_cache_on)?'':'checked'; ?>
                                <div class="checkbox">
                                    <label><input type="checkbox" name="options[mobile_tpl_enabled]" value="1" id="input-html_cache_on" <?php echo $html_cache_on_checked; ?>></label>
                                </div>
                            </div>
                        </div>
                        -->
                        <!--<div class="form-group">-->
                        <!--    <label for="input-copyright" class="col-sm-2 control-label">版权信息</label>-->
                        <!--    <div class="col-md-6 col-sm-10">-->
                        <!--        <textarea class="form-control" id="input-copyright" name="options[copyright]" ><?php echo (isset($site_info['copyright']) && ($site_info['copyright'] !== '')?$site_info['copyright']:''); ?></textarea>-->
                        <!--        <p class="help-block">版权信息（200字以内）</p>-->
                        <!--    </div>-->
                        <!--</div>-->
                        
                        <!-- <div class="form-group">-->
                        <!--    <label for="input-upper" class="col-sm-2 control-label">每天获得金币上限</label>-->
                        <!--    <div class="col-md-6 col-sm-10">-->
                        <!--        <input type="text" class="form-control" id="input-upper" name="comment_app[upper]"-->
                        <!--               value="<?php echo (isset($comment_app['upper']) && ($comment_app['upper'] !== '')?$comment_app['upper']:''); ?>">-->
                        <!--        <p class="help-block">金币上限</p>-->
                        <!--    </div>-->
                        <!--</div>-->
                        
                        
                        
                        
                        <div class="form-group">
                            <label for="input-publish" class="col-sm-2 control-label">发布帖子</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-publish" name="comment_app[publish]"
                                       value="<?php echo (isset($comment_app['publish']) && ($comment_app['publish'] !== '')?$comment_app['publish']:''); ?>">
                                <p class="help-block">发布帖子获得金币</p>
                            </div>
                        </div>

                        <!--<div class="form-group">-->
                        <!--    <label for="input-questionnaire" class="col-sm-2 control-label">参与问卷调查</label>-->
                        <!--    <div class="col-md-6 col-sm-10">-->
                        <!--        <input type="text" class="form-control" id="input-questionnaire" name="comment_app[questionnaire]"-->
                        <!--               value="<?php echo (isset($comment_app['questionnaire']) && ($comment_app['questionnaire'] !== '')?$comment_app['questionnaire']:''); ?>">-->
                        <!--        <p class="help-block">参与问卷调查获得金币</p>-->
                        <!--    </div>-->
                        <!--</div>-->


                        <div class="form-group">
                            <label for="input-material" class="col-sm-2 control-label">完善资料</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-material" name="comment_app[material]"
                                       value="<?php echo (isset($comment_app['material']) && ($comment_app['material'] !== '')?$comment_app['material']:''); ?>">
                                <p class="help-block">完善资料获得金币</p>
                            </div>
                        </div>
                        
     

                        <div class="form-group">
                            <label for="input-num" class="col-sm-2 control-label">阅读量</label>
                            <div class="col-md-6 col-sm-10">
                                 <a class="btn btn-xs btn-primary" href='<?php echo url("read/index"); ?>'>前往设置</a>
                                <!--<input type="text" class="form-control" id="input-num" name="comment_app[num]"-->
                                <!--       value="<?php echo (isset($comment_app['num']) && ($comment_app['num'] !== '')?$comment_app['num']:''); ?>">-->
                                <p class="help-block">多少阅读量获得金币</p>
                            </div>
                        </div>
                   
                        <!-- <div class="form-group">-->
                        <!--    <label for="input-reading" class="col-sm-2 control-label">阅读获得金币</label>-->
                        <!--    <div class="col-md-6 col-sm-10">-->
                        <!--        <input type="text" class="form-control" id="input-reading" name="comment_app[reading]"-->
                        <!--               value="<?php echo (isset($comment_app['reading']) && ($comment_app['reading'] !== '')?$comment_app['reading']:''); ?>">-->
                        <!--        <p class="help-block">阅读量获得金币</p>-->
                        <!--    </div>-->
                        <!--</div>-->
                   
                   
                   
                   
                   
                   
                   
                        <div class="form-group">
                            <label for="input-mobile" class="col-sm-2 control-label">公司电话</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-mobile"
                                       name="options[mobile]"
                                       value="<?php echo (isset($site_info['mobile']) && ($site_info['mobile'] !== '')?$site_info['mobile']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-address" class="col-sm-2 control-label">公司地址</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-address"
                                       name="options[address]"
                                       value="<?php echo (isset($site_info['address']) && ($site_info['address'] !== '')?$site_info['address']:''); ?>">
                            </div>
                        </div>
                        
                        <!--<div class="form-group">-->
                        <!--    <label for="input-apk_ewm" class="col-sm-2 control-label">android版下载二维码</label>-->
                        <!--    <div class="col-md-6 col-sm-10">-->
                        <!--        <input type="hidden" name="options[apk_ewm]" id="thumbnail1" value="<?php echo (isset($site_info['apk_ewm']) && ($site_info['apk_ewm'] !== '')?$site_info['apk_ewm']:''); ?>">-->
                        <!--        <a href="javascript:uploadOneImage('图片上传','#thumbnail1');">-->
                        <!--            <?php if(empty($site_info['apk_ewm'])): ?>-->
                        <!--            <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"-->
                        <!--                     id="thumbnail1-preview"-->
                        <!--                     style="cursor: pointer;max-width:150px;max-height:150px;"/>-->
                        <!--            <?php else: ?>-->
                        <!--            <img src="<?php echo cmf_get_image_preview_url($site_info['apk_ewm']); ?>"-->
                        <!--                 id="thumbnail1-preview"-->
                        <!--                 style="cursor: pointer;max-width:150px;max-height:150px;"/>-->
                        <!--            <?php endif; ?>-->
                        <!--        </a>-->
                        <!--        <input type="button" class="btn btn-sm btn-cancel-thumbnail1" value="取消图片">  PC首页用-->
                        <!--    </div>-->
                        <!--</div>-->
                        
                        <!--<div class="form-group">-->
                        <!--    <label for="input-ipa_ewm" class="col-sm-2 control-label">iPhone版下载二维码</label>-->
                        <!--    <div class="col-md-6 col-sm-10">-->
                        <!--        <input type="hidden" name="options[ipa_ewm]" id="thumbnail2" value="<?php echo (isset($site_info['ipa_ewm']) && ($site_info['ipa_ewm'] !== '')?$site_info['ipa_ewm']:''); ?>">-->
                        <!--        <a href="javascript:uploadOneImage('图片上传','#thumbnail2');">-->
                        <!--            <?php if(empty($site_info['ipa_ewm'])): ?>-->
                        <!--            <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"-->
                        <!--                     id="thumbnail2-preview"-->
                        <!--                     style="cursor: pointer;max-width:150px;max-height:150px;"/>-->
                        <!--            <?php else: ?>-->
                        <!--            <img src="<?php echo cmf_get_image_preview_url($site_info['ipa_ewm']); ?>"-->
                        <!--                 id="thumbnail2-preview"-->
                        <!--                 style="cursor: pointer;max-width:150px;max-height:150px;"/>-->
                        <!--            <?php endif; ?>-->
                        <!--        </a>-->
                        <!--        <input type="button" class="btn btn-sm btn-cancel-thumbnail2" value="取消图片">  PC首页用-->
                        <!--    </div>-->
                        <!--</div>-->
                        
                        
                       <div class="form-group">
                            <label for="input-ipa_ewm" class="col-sm-2 control-label">背景图</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="hidden" name="options[background]" id="thumbnail2" value="<?php echo (isset($site_info['background']) && ($site_info['background'] !== '')?$site_info['background']:''); ?>">
                                <a href="javascript:uploadOneImage('图片上传','#thumbnail2');">
                                    <?php if(empty($site_info['background'])): ?>
                                    <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                             id="thumbnail2-preview"
                                             style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php else: ?>
                                    <img src="<?php echo cmf_get_image_preview_url($site_info['background']); ?>"
                                         id="thumbnail2-preview"
                                         style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php endif; ?>
                                </a>
                                <input type="button" class="btn btn-sm btn-cancel-thumbnail2" value="取消图片">
                            </div>
                        </div>




                        <div class="form-group">
                            <label for="input-af" class="col-sm-2 control-label">实名认证（金币）</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-af"
                                       name="options[coin]"
                                       value="<?php echo (isset($site_info['coin']) && ($site_info['coin'] !== '')?$site_info['coin']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-tow" class="col-sm-2 control-label">实名认证（引荐人金币）</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-tow"
                                       name="options[coin_ref]"
                                       value="<?php echo (isset($site_info['coin_ref']) && ($site_info['coin_ref'] !== '')?$site_info['coin_ref']:''); ?>">
                            </div>
                        </div>


                        <div class="form-group">
                            <label for="input-forward_time" class="col-sm-2 control-label">金币入账等待时长(天)</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-forward_time"
                                       name="options[forward_time]"
                                       value="<?php echo (isset($site_info['forward_time']) && ($site_info['forward_time'] !== '')?$site_info['forward_time']:''); ?>">
                            </div>
                        </div>
                        
                        
                        
                        <div class="form-group">
                            <label for="input-forward_time" class="col-sm-2 control-label">最少转账金币</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="number" class="form-control" id="input-forward_time"
                                       name="options[least_trans]"
                                       value="<?php echo (isset($site_info['least_trans']) && ($site_info['least_trans'] !== '')?$site_info['least_trans']:''); ?>">
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="1">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="B">
                        <div class="form-group">
                            <label for="input-site_seo_title" class="col-sm-2 control-label"><?php echo lang('WEBSITE_SEO_TITLE'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site_seo_title"
                                       name="options[site_seo_title]" value="<?php echo (isset($site_info['site_seo_title']) && ($site_info['site_seo_title'] !== '')?$site_info['site_seo_title']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-site_seo_keywords" class="col-sm-2 control-label"><?php echo lang('WEBSITE_SEO_KEYWORDS'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-site_seo_keywords"
                                       name="options[site_seo_keywords]"
                                       value="<?php echo (isset($site_info['site_seo_keywords']) && ($site_info['site_seo_keywords'] !== '')?$site_info['site_seo_keywords']:''); ?>">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-site_seo_description" class="col-sm-2 control-label"><?php echo lang('WEBSITE_SEO_DESCRIPTION'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-site_seo_description"
                                          name="options[site_seo_description]"><?php echo (isset($site_info['site_seo_description']) && ($site_info['site_seo_description'] !== '')?$site_info['site_seo_description']:''); ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="C">
                        <div class="form-group">
                            <label for="input-isup" class="col-sm-2 control-label">强制更新</label>
                            <div class="col-md-6 col-sm-10">
                                <select class="form-control" name="options[isup]">
                                    <option value="0">关闭</option>
                                    <option value="1" <?php if($site_info['isup'] == '1'): ?>selected<?php endif; ?>>开启</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-apk_ver" class="col-sm-2 control-label">APK版本号</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-apk_ver"
                                       name="options[apk_ver]" value="<?php echo (isset($site_info['apk_ver']) && ($site_info['apk_ver'] !== '')?$site_info['apk_ver']:''); ?>">
                                       <p class="help-block">安卓APP最新的版本号，请勿随意修改</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-apk_url" class="col-sm-2 control-label">APK下载链接</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-apk_url"
                                       name="options[apk_url]" value="<?php echo (isset($site_info['apk_url']) && ($site_info['apk_url'] !== '')?$site_info['apk_url']:''); ?>">
                                       <p class="help-block">安卓最新版APK下载链接</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-apk_des" class="col-sm-2 control-label">APK更新说明</label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-apk_des"
                                          name="options[apk_des]"><?php echo (isset($site_info['apk_des']) && ($site_info['apk_des'] !== '')?$site_info['apk_des']:''); ?></textarea>
                                          <p class="help-block">APK更新说明（200字以内）</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-ipa_ver" class="col-sm-2 control-label">IPA版本号</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ipa_ver"
                                       name="options[ipa_ver]" value="<?php echo (isset($site_info['ipa_ver']) && ($site_info['ipa_ver'] !== '')?$site_info['ipa_ver']:''); ?>">
                                       <p class="help-block">IOS APP最新的版本号，请勿随意修改</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-ios_shelves" class="col-sm-2 control-label">IPA上架版本号</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ios_shelves"
                                       name="options[ios_shelves]" value="<?php echo (isset($site_info['ios_shelves']) && ($site_info['ios_shelves'] !== '')?$site_info['ios_shelves']:''); ?>">
                                       <p class="help-block">IOS上架审核中版本的版本号(用于上架期间隐藏上架版本部分功能,不要和IPA版本号相同)</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-ipa_url" class="col-sm-2 control-label">IPA下载链接</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-ipa_url"
                                       name="options[ipa_url]" value="<?php echo (isset($site_info['ipa_url']) && ($site_info['ipa_url'] !== '')?$site_info['ipa_url']:''); ?>">
                                       <p class="help-block">IOS最新版IPA下载链接</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-ipa_des" class="col-sm-2 control-label">IPA更新说明</label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-ipa_des"
                                          name="options[ipa_des]"><?php echo (isset($site_info['ipa_des']) && ($site_info['ipa_des'] !== '')?$site_info['ipa_des']:''); ?></textarea>
                                          <p class="help-block">IPA更新说明（200字以内）</p>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="input-qr_url" class="col-sm-2 control-label">二维码下载链接</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="hidden" name="options[qr_url]" id="thumbnail6" value="<?php echo (isset($site_info['qr_url']) && ($site_info['qr_url'] !== '')?$site_info['qr_url']:''); ?>">
                                <a href="javascript:uploadOneImage('图片上传','#thumbnail6');">
                                    <?php if(empty($site_info['qr_url'])): ?>
                                    <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                             id="thumbnail6-preview"
                                             style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php else: ?>
                                    <img src="<?php echo cmf_get_image_preview_url($site_info['qr_url']); ?>"
                                         id="thumbnail6-preview"
                                         style="cursor: pointer;max-width:150px;max-height:150px;"/>
                                    <?php endif; ?>
                                </a>
                                <input type="button" class="btn btn-sm btn-cancel-thumbnail6" value="取消图片">
                                <p class="help-block"></p> 
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    
                    <div class="tab-pane" id="D">
                        <div class="form-group">
                            <label for="input-site-name" class="col-sm-2 control-label">登录方式</label>
                            <div class="col-md-6 col-sm-10">
                                <?php 
									$qq='qq';
									$wx='wx';
									$sina='sina';
									$facebook='facebook';
									$twitter='twitter';
                                    $ios='ios';
                                    $google='google';
								 ?>
								<label class="checkbox-inline"><input type="checkbox" value="google" name="login_type[]" <?php if(in_array(($google), is_array($site_info['login_type'])?$site_info['login_type']:explode(',',$site_info['login_type']))): ?>checked="checked"<?php endif; ?>>google</label>
								<!--<label class="checkbox-inline"><input type="checkbox" value="qq" name="login_type[]" <?php if(in_array(($qq), is_array($site_info['login_type'])?$site_info['login_type']:explode(',',$site_info['login_type']))): ?>checked="checked"<?php endif; ?>>QQ</label>-->
								<!--<label class="checkbox-inline"><input type="checkbox" value="wx" name="login_type[]" <?php if(in_array(($wx), is_array($site_info['login_type'])?$site_info['login_type']:explode(',',$site_info['login_type']))): ?>checked="checked"<?php endif; ?>>微信</label>-->
								<!--<label class="checkbox-inline"><input type="checkbox" value="facebook" name="login_type[]" <?php if(in_array(($facebook), is_array($site_info['login_type'])?$site_info['login_type']:explode(',',$site_info['login_type']))): ?>checked="checked"<?php endif; ?>>FaceBook</label>-->
								<!--<label class="checkbox-inline"><input type="checkbox" value="twitter" name="login_type[]" <?php if(in_array(($twitter), is_array($site_info['login_type'])?$site_info['login_type']:explode(',',$site_info['login_type']))): ?>checked="checked"<?php endif; ?>>Twitter</label>-->
        <!--                        <label class="checkbox-inline"><input type="checkbox" value="ios" name="login_type[]" <?php if(in_array(($ios), is_array($site_info['login_type'])?$site_info['login_type']:explode(',',$site_info['login_type']))): ?>checked="checked"<?php endif; ?>>iOS</label>-->
								
                            </div>
                        </div>
                        
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="F">
                        <div class="form-group">
                            <label for="input-banned_usernames" class="col-sm-2 control-label">用户注册验证</label>
                            <div class="col-md-6 col-sm-10">
                                <select class="form-control" name="cmf_settings[open_registration]">
                                    <option value="0">是</option>
                                    <?php 
                                        $open_registration_selected = empty($cmf_settings['open_registration'])?'':'selected';
                                     ?>
                                    <option value="1" <?php echo $open_registration_selected; ?>>否</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group" style="display: none;">
                            <label for="input-banned_usernames" class="col-sm-2 control-label"><?php echo lang('SPECAIL_USERNAME'); ?></label>
                            <div class="col-md-6 col-sm-10">
                                <textarea class="form-control" id="input-banned_usernames"
                                          name="cmf_settings[banned_usernames]"><?php echo (isset($cmf_settings['banned_usernames']) && ($cmf_settings['banned_usernames'] !== '')?$cmf_settings['banned_usernames']:''); ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="G">
                        <div class="form-group">
                            <label for="input-cdn_static_root" class="col-sm-2 control-label">静态资源cdn地址</label>
                            <div class="col-md-6 col-sm-10">
                                <input type="text" class="form-control" id="input-cdn_static_root"
                                       name="cdn_settings[cdn_static_root]"
                                       value="<?php echo (isset($cdn_settings['cdn_static_root']) && ($cdn_settings['cdn_static_root'] !== '')?$cdn_settings['cdn_static_root']:''); ?>">
                                <p class="help-block">
                                    不能以/结尾；设置这个地址后，请将ThinkCMF下的静态资源文件放在其下面；<br>
                                    ThinkCMF下的静态资源文件大致包含以下(如果你自定义后，请自行增加)：<br>
                                    themes/admin_simplebootx/public/assets<br>
                                    static<br>
                                    themes/simplebootx/public/assets<br>
                                    例如未设置cdn前：jquery的访问地址是/static/js/jquery.js, <br>
                                    设置cdn是后它的访问地址就是：静态资源cdn地址/static/js/jquery.js
                                </p>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-primary js-ajax-submit" data-refresh="0">
                                    <?php echo lang('SAVE'); ?>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </fieldset>
    </form>

</div>
<script type="text/javascript" src="/static/js/admin.js"></script>
</body>
</html>
