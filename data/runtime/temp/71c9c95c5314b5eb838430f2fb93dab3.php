<?php /*a:2:{s:74:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/admin/shopapply/add.html";i:1636341371;s:68:"/var/www/ap1.askpert.com/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
<style type="text/css">
	.red_tips{
		color: #F00;
	}
</style>
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li ><a href="<?php echo url('Shopapply/index'); ?>">店铺列表</a></li>
			<li class="active"><a ><?php echo lang('ADD'); ?></a></li>
		</ul>
		<form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('Shopapply/addPost'); ?>">

        	<div class="form-group">
        		<label for="input-ipa_ewm" class="col-sm-2 control-label">店主</label>
    		  	<div class="col-md-3 col-sm-10">
        		<input type="text" class="form-control" id="uid" name="uid" placeholder="请输入店主ID" readonly>
    		     <span class="help-block">
                    <a onclick="selectGoods()"><i class="fa fa-search"></i>选择</a>
                </span>
        		</div>	   
        	</div>
        	
        	
        	<div class="form-group">
        		<label for="input-ipa_ewm" class="col-sm-2 control-label">店铺提供者</label>
    		  	<div class="col-md-3 col-sm-10">
        		<input type="text" class="form-control" id="provider_id" name="provider_id" placeholder="可不填" readonly>
    		     <span class="help-block">
                    <a onclick="selectUser()"><i class="fa fa-search"></i>选择</a>
                </span>
        		</div>	   
        	</div>

           <div class="form-group">
                <label for="input-ipa_ewm" class="col-sm-2 control-label">商品主图</label>
                <div class="col-md-6 col-sm-10">
                    <input type="hidden" name="thumb" id="thumbnail2" value="">
                    <a href="javascript:uploadOneImage('图片上传','#thumbnail2');">
                        <?php if(empty($site_info['ipa_ewm'])): ?>
                        <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                 id="thumbnail2-preview"
                                 style="cursor: pointer;max-width:150px;max-height:150px;"/>
                        <?php else: ?>
                        <img src="<?php echo cmf_get_image_preview_url($site_info['ipa_ewm']); ?>"
                             id="thumbnail2-preview"
                             style="cursor: pointer;max-width:150px;max-height:150px;"/>
                        <?php endif; ?>
                    </a>
                    <input type="button" class="btn btn-sm btn-cancel-thumbnail2" value="取消图片">
                </div>
            </div>
            
            <div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>店铺名称</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="name" >
				</div>
			</div>


			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>店铺账号</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" required="required" value="" name="shop_account" >
				</div>
			</div>

			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>店铺密码</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  required="required" value="" name="shop_password" >
				</div>
			</div>


			<div class="form-group">
				<label for="input-coin" class="col-sm-2 control-label"><span class="form-required">*</span>金币</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" readonly="" class="form-control"  required="required" value="0" name="coin" >
				</div>
			</div>

            
            <div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>简介</label>
				<div class="col-md-6 col-sm-10">
                    <textarea class="form-control" name="des" id="des" ></textarea>
				</div>
			</div>

	
			
			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>经营类目</label>
				<div class="col-md-6 col-sm-10">
					<select class="form-control" name="classid">
                        <?php if(is_array($shop_class) || $shop_class instanceof \think\Collection || $shop_class instanceof \think\Paginator): $i = 0; $__LIST__ = $shop_class;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                        <option value="<?php echo $v['id']; ?>"><?php echo $v['name']; ?></option>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>
				</div>
			</div>
<!--			<div class="form-group">-->
<!--    			<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>地址</label>    -->
<!--                <div class="row">-->
<!--                    <div class="col-sm-2">-->
<!--                        <input type="text" class="form-control" name="province" placeholder="省">-->
<!--                    </div>-->
<!--                    <div class="col-sm-3">-->
<!--                        <input type="text" class="form-control" name="city" placeholder="市">-->
<!--                    </div>-->
<!--                    <div class="col-sm-4">-->
<!--                        <input type="text" class="form-control" name="area" placeholder="区">-->
<!--                    </div>-->
<!--                </div>-->
<!--			</div>-->
			
			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>官网链接</label>
				<div class="col-md-6 col-sm-10">
                    <input type="text" class="form-control"  name="link">
				</div>
			</div>

			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>店铺电话</label>
				<div class="col-md-6 col-sm-10">
                    <input type="text" class="form-control"  value="" name="service_phone">
				</div>
			</div>

            <!-- <div class="form-group">
        	<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>话题</label>
			<div class="col-md-6 col-sm-10">
			    <?php if(is_array($topicData) || $topicData instanceof \think\Collection || $topicData instanceof \think\Paginator): $i = 0; $__LIST__ = $topicData;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
            	<label class="checkbox-inline">
            		<input type="checkbox" id="inlineCheckbox1" value="<?php echo $v['topic_id']; ?>" name="topic[]"> <?php echo $v['topic_name']; ?>
            	</label>
                <?php endforeach; endif; else: echo "" ;endif; ?>
            </div>    
            </div> -->
              
			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>经营人</label>
				<div class="col-md-6 col-sm-10">
                    <input type="text" class="form-control"  value="" name="username">
				</div>
			</div>  
                        
    		<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>经营人手机号码</label>
				<div class="col-md-6 col-sm-10">
                    <input type="text" class="form-control"  value="" name="phone">
				</div>
			</div> 
			<div class="form-group" style="margin-left:165px;">
                <input type="button" class="btn btn-info" onclick="clickText('multiple')" value="添加门店" />
			</div>
			<div class="form-group" id="multiple">
			    <div id="tableM0">
                    <label for="input-needcoin" class="col-sm-2 control-label"><span class="form-required">*</span>请选择门店</label>
    				<div class="col-md-6 col-sm-10" style="margin-bottom:15px;">
    					<select class="form-control" name="branchid[0][option]">
                            <option value="">请选择</option>
                            <?php if(is_array($option) || $option instanceof \think\Collection || $option instanceof \think\Paginator): $i = 0; $__LIST__ = $option;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                            <option value="<?php echo $v['id']; ?>"><?php echo $v['shop_name']; ?></option>
                            <?php endforeach; endif; else: echo "" ;endif; ?>
                        </select>
    				</div>
			        
			    </div>

			</div>
		
			<!--<div class="form-group" id="multiple">-->
			<!--    <div id="tableM0">-->
                    
   <!--                 <label for="input-needcoin" class="col-sm-2 control-label"><span class="form-required">*</span>请选择门店</label>-->
   <!-- 				<div class="col-md-6 col-sm-10">-->
   <!-- 					<select class="form-control" name="classid[0][option]">-->
   <!--                         <?php if(is_array($option) || $option instanceof \think\Collection || $option instanceof \think\Paginator): $i = 0; $__LIST__ = $option;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>-->
   <!--                         <option value="<?php echo $v['id']; ?>"><?php echo $v['shop_name']; ?></option>-->
   <!--                         <?php endforeach; endif; else: echo "" ;endif; ?>-->
   <!--                     </select>-->
   <!-- 				</div>-->
   <!-- 				<input type="button" class="btn btn-info" onclick="clickText('multiple')" value="添加门店" />			        -->
			        
			<!--    </div>-->

			<!--</div>-->
			
            <div class="form-group">
				<label for="input-needcoin" class="col-sm-2 control-label"><span class="form-required">*</span>审核状态</label>
				<div class="col-md-6 col-sm-10">
					<select class="form-control" name="status">
                        <?php if(is_array($status) || $status instanceof \think\Collection || $status instanceof \think\Paginator): $i = 0; $__LIST__ = $status;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                        <option value="<?php echo $key; ?>"><?php echo $v; ?></option>
                        <?php endforeach; endif; else: echo "" ;endif; ?>
                    </select>
				</div>
			</div>

			<div class="form-group">
				<label for="input-claim" class="col-sm-2 control-label"><span class="form-required">*</span>是否认证</label>
				<div class="col-md-6 col-sm-10">
					<select class="form-control" name="claim">
							<option value="1" selected>是</option>
							<option value="0">否</option>
					</select>
				</div>
			</div>




			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>营业执照</label>
				<div class="col-md-6 col-sm-10">
					<input id="b_cert" type="hidden" name="b_cert" >
					<img id="b_cert-preview" style="max-height: 200px;max-width: 200px" src='/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png'>
					<a href="javascript:uploadOneImage('图片上传','#b_cert');">上传</a>
				</div>
			</div>



			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>品牌所在地区</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="region">
				</div>
			</div>


			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>品牌地址</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="brand_address">
				</div>
			</div>


			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>商户类型</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="merch_type">
				</div>
			</div>

			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>主要负责人姓名</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="first_user_name">
				</div>
			</div>

			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>主要负责人电话</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="first_user_mobile">
				</div>
			</div>
			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>主要负责人电邮</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="first_user_email">
				</div>
			</div>


			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>第二负责人姓名</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="second_user_name">
				</div>
			</div>


			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>第二负责人电话</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="second_user_mobile">
				</div>
			</div>


			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>第二负责人电邮</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="second_user_email">
				</div>
			</div>





			<fieldset style="margin-left: 7%;

    margin-bottom: 2%;
    margin-top: 2%;">
				<legend>公司资料</legend>
			</fieldset>



			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>公司名称</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="company_name">
				</div>
			</div>


			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>公司logo</label>
				<div class="col-md-6 col-sm-10">
					<input id="company_logo" type="hidden" name="company_logo" >
				<!--	<input id="company_logo-name" type="hidden" name="company_logo" title="图片名称">-->
					<img id="company_logo-preview" style="max-height: 200px;max-width: 200px" src='/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png'>
				   <a href="javascript:uploadOneImage('图片上传','#company_logo');">上传</a>
				</div>
			</div>

			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>公司营业执照</label>
				<div class="col-md-6 col-sm-10">
					<input id="company_img" type="hidden" name="company_img" >
				<!--	<input id="company_img-name" type="hidden" name="company_img" title="图片名称">-->
					<img id="company_img-preview" style="max-height: 200px;max-width: 200px" src='/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png'>
					<a href="javascript:uploadOneImage('图片上传','#company_img');">上传</a>
				</div>
			</div>

			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>公司地址</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="company_address">
				</div>
			</div>


			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>公司网站</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control"  value="" name="company_link">
				</div>
			</div>

			<div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required"></span>公司资料</label>
				<div class="col-md-6 col-sm-10">
					<textarea class="form-control" name="company_description" id="company_description" ></textarea>
				</div>
			</div>



			<div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<button type="submit" class="btn btn-primary js-ajax-submit"><?php echo lang('EDIT'); ?></button>
					<a class="btn btn-default" href="javascript:history.back(-1);"><?php echo lang('BACK'); ?></a>
				</div>
			</div>

		</form>
	</div>
	<script src="/static/js/admin.js"></script>
	
    <script>
	function clickText(tab){
        var table =  document.getElementById(tab);
        console.log(table);
        console.log("-->");
        var list = table.querySelectorAll('table');
        console.log(list);
        console.log("---<<<<");
        var i = list.length+1;
        console.log(i);
        console.log("---<><>><");
        var j = i+1;
        console.log(i);
        if(tab=='multiple'){
            var trHtml = '<table class="form-group" style="width: 100%;margin-left: 180px;" id="tableM' + i + '">' +
                        
				'<div class="col-md-6 col-sm-10" id="tableMs' + i + '">'+
				'<span onclick="del('+i+')" style="cursor:pointer;margin-top:10px;color:red;float: right;margin-top: 10px;margin-right: -360px;" >删除</span>'+
					'<select class="form-control" name="branchid['+i+'][option]" style="margin-top:0px;width: 100%;margin-left: 282px;">'+
					    '<option value="">请选择</option>'+
                        '<?php if(is_array($option) || $option instanceof \think\Collection || $option instanceof \think\Paginator): $i = 0; $__LIST__ = $option;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>'+
                        '<option value="<?php echo $v['id']; ?>">`'+"<?php echo $v['shop_name']; ?>"+'`</option>'+
                        '<?php endforeach; endif; else: echo "" ;endif; ?>'+
                    '</select>'+
				'</div>'+
			'</table>'
        }
        addHtml(tab,i,trHtml);
    }
    
    function addHtml(tab,i,html) {
        if(tab=='multiple'){
            if(i==0){
                console.log(i);
                console.log("00-->");
                $("#multiple").html(html);
            }
            else{
                console.log("--k");
                console.log(i);
                var j = i-1;
                var name = 'tableM'+j;
                $("#"+name+"").after(html);
            }

        }
    }

    
    function del(id){
        
        $("#tableMs"+id).remove();
        
    }

	    
	</script>
	<script>
	    $(function(){
			Wind.use('layer');
		});	    
        function selectGoods(){
        var url = "/admin/users/index";
        layer.open({
            type: 2,
            title: '选择会员',
            shadeClose: true,
            shade: 0.2,
            area: ['55%', '55%'],
            content: url,
        });
        }
        
        function selectUser(){
        var url = "/admin/users/index&judge=1";
        layer.open({
            type: 2,
            title: '选择会员',
            shadeClose: true,
            shade: 0.2,
            area: ['55%', '55%'],
            content: url,
        });
        }
        function call_back(uid,id){
            if(id==3){
                $('input[name=provider_id]').val(uid)
                 layer.closeAll('iframe');
            }else{
                $('input[name=uid]').val(uid)
                 layer.closeAll('iframe');
            }
             
        }
        
	</script>
</body>

</html>