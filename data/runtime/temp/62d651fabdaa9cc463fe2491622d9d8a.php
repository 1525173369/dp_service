<?php /*a:2:{s:86:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/admin/ad/sedit.html";i:1623124595;s:85:"/www/wwwroot/test01.zwrjkf.com/dp_service/themes/admin_simpleboot3/public/header.html";i:1623124595;}*/ ?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- HTML5 shim for IE8 support of HTML5 elements -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <![endif]-->


    <link href="/themes/admin_simpleboot3/public/assets/themes/<?php echo cmf_get_admin_style(); ?>/bootstrap.min.css" rel="stylesheet">
    <link href="/themes/admin_simpleboot3/public/assets/simpleboot3/css/simplebootadmin.css" rel="stylesheet">
    <link href="/static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <!--[if lt IE 9]>
    <script src="https://cdn.bootcss.com/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style>
        form .input-order {
            margin-bottom: 0px;
            padding: 0 2px;
            width: 42px;
            font-size: 12px;
        }

        form .input-order:focus {
            outline: none;
        }

        .table-actions {
            margin-top: 5px;
            margin-bottom: 5px;
            padding: 0px;
        }

        .table-list {
            margin-bottom: 0px;
        }

        .form-required {
            color: red;
        }
    </style>
    <script type="text/javascript">
        //全局变量
        var GV = {
            ROOT: "/",
            WEB_ROOT: "/",
            JS_ROOT: "static/js/",
            APP: '<?php echo app('request')->module(); ?>'/*当前应用名*/
        };
    </script>
    <script src="/themes/admin_simpleboot3/public/assets/js/jquery-1.10.2.min.js"></script>
    <script src="/static/js/wind.js"></script>
    <script src="/themes/admin_simpleboot3/public/assets/js/bootstrap.min.js"></script>
    <script>
        Wind.css('artDialog');
        Wind.css('layer');
        $(function () {
            $("[data-toggle='tooltip']").tooltip({
                container:'body',
                html:true,
            });
            $("li.dropdown").hover(function () {
                $(this).addClass("open");
            }, function () {
                $(this).removeClass("open");
            });
        });
    </script>
    <?php if(APP_DEBUG): ?>
        <style>
            #think_page_trace_open {
                z-index: 9999;
            }
        </style>
    <?php endif; ?>
</head>
<body>
	<div class="wrap">
		<ul class="nav nav-tabs">
			<li ><a href="<?php echo url('Ad/index'); ?>">列表</a></li>
			<li class="active"><a ><?php echo lang('EDIT'); ?></a></li>
		</ul>
		<form method="post" class="form-horizontal js-ajax-form margin-top-20" action="<?php echo url('Ad/editShop'); ?>">
		    
		    

		    
            <div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>广告1标题</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-name" name="name1" value="<?php echo $data['name1']; ?>">
				</div>
			</div>
            <div class="form-group">
				<label for="input-user_login" class="col-sm-2 control-label"><span class="form-required">*</span>图片</label>
				<div class="col-md-6 col-sm-10">
					<input type="hidden" name="thumb1" id="thumbnail1" value="<?php echo $data['thumb1']; ?>">
                    <a href="javascript:uploadOneImage('图片上传','#thumbnail1');">
                        <?php if(empty($data['thumb1'])): ?>
                        <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                 id="thumbnail1-preview"
                                 style="cursor: pointer;max-width:100px;max-height:100px;"/>
                        <?php else: ?>
                        <img src="<?php echo cmf_get_image_preview_url($data['thumb1']); ?>"
                             id="thumbnail1-preview"
                             style="cursor: pointer;max-width:100px;max-height:100px;"/>
                        <?php endif; ?>
                    </a>
                    <input type="button" class="btn btn-sm btn-cancel-thumbnail1" value="取消图片"> 图片尺寸： 50 X 50
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-des" class="col-sm-2 control-label"><span class="form-required">*</span>跳转链接</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-des" name="url1" value="<?php echo $data['url1']; ?>" maxlength="200">
					<p class="help-block"></p>
				</div>
			</div>
			
            <div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>广告2标题</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-name" name="name2" value="<?php echo $data['name2']; ?>">
				</div>
			</div>
            <div class="form-group">
				<label for="input-user_login" class="col-sm-2 control-label"><span class="form-required">*</span>图片</label>
				<div class="col-md-6 col-sm-10">
					<input type="hidden" name="thumb2" id="thumbnail2" value="<?php echo $data['thumb2']; ?>">
                    <a href="javascript:uploadOneImage('图片上传','#thumbnail2');">
                        <?php if(empty($data['thumb2'])): ?>
                        <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                 id="thumbnail2-preview"
                                 style="cursor: pointer;max-width:100px;max-height:100px;"/>
                        <?php else: ?>
                        <img src="<?php echo cmf_get_image_preview_url($data['thumb2']); ?>"
                             id="thumbnail2-preview"
                             style="cursor: pointer;max-width:100px;max-height:100px;"/>
                        <?php endif; ?>
                    </a>
                    <input type="button" class="btn btn-sm btn-cancel-thumbnail2" value="取消图片"> 图片尺寸： 50 X 50
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-des" class="col-sm-2 control-label"><span class="form-required">*</span>跳转链接</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-des" name="url2" value="<?php echo $data['url2']; ?>" maxlength="200">
					<p class="help-block"></p>
				</div>
			</div>
			
			
            <div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>广告3标题</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-name" name="name3" value="<?php echo $data['name3']; ?>">
				</div>
			</div>
            <div class="form-group">
				<label for="input-user_login" class="col-sm-2 control-label"><span class="form-required">*</span>图片</label>
				<div class="col-md-6 col-sm-10">
					<input type="hidden" name="thumb3" id="thumbnail3" value="<?php echo $data['thumb3']; ?>">
                    <a href="javascript:uploadOneImage('图片上传','#thumbnail3');">
                        <?php if(empty($data['thumb3'])): ?>
                        <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                 id="thumbnail3-preview"
                                 style="cursor: pointer;max-width:100px;max-height:100px;"/>
                        <?php else: ?>
                        <img src="<?php echo cmf_get_image_preview_url($data['thumb3']); ?>"
                             id="thumbnail3-preview"
                             style="cursor: pointer;max-width:100px;max-height:100px;"/>
                        <?php endif; ?>
                    </a>
                    <input type="button" class="btn btn-sm btn-cancel-thumbnail3" value="取消图片"> 图片尺寸： 50 X 50
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-des" class="col-sm-2 control-label"><span class="form-required">*</span>跳转链接</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-des" name="url3" value="<?php echo $data['url3']; ?>" maxlength="200">
					<p class="help-block"></p>
				</div>
			</div>
			
			
			
            <div class="form-group">
				<label for="input-name" class="col-sm-2 control-label"><span class="form-required">*</span>广告4标题</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-name" name="name4" value="<?php echo $data['name4']; ?>">
				</div>
			</div>
            <div class="form-group">
				<label for="input-user_login" class="col-sm-2 control-label"><span class="form-required">*</span>图片</label>
				<div class="col-md-6 col-sm-10">
					<input type="hidden" name="thumb4" id="thumbnail4" value="">
                    <a href="javascript:uploadOneImage('图片上传','#thumbnail4');">
                        <?php if(empty($data['thumb4'])): ?>
                        <img src="/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png"
                                 id="thumbnail4-preview"
                                 style="cursor: pointer;max-width:100px;max-height:100px;"/>
                        <?php else: ?>
                        <img src="<?php echo cmf_get_image_preview_url($data['thumb4']); ?>"
                             id="thumbnail4-preview"
                             style="cursor: pointer;max-width:100px;max-height:100px;"/>
                        <?php endif; ?>
                    </a>
                    <input type="button" class="btn btn-sm btn-cancel-thumbnail4" value="取消图片"> 图片尺寸： 50 X 50
				</div>
			</div>
            
            <div class="form-group">
				<label for="input-des" class="col-sm-2 control-label"><span class="form-required">*</span>跳转链接</label>
				<div class="col-md-6 col-sm-10">
					<input type="text" class="form-control" id="input-des" name="url4" value="<?php echo $data['url4']; ?>" maxlength="200">
					<p class="help-block"></p>
				</div>
			</div>
            <div class="form-group">
				<div class="col-sm-offset-2 col-sm-10">
					<input type="hidden" name="shop_id" value="<?php echo $shop_id; ?>" />
					<button type="submit" class="btn btn-primary js-ajax-submit"><?php echo lang('EDIT'); ?></button>
					<a class="btn btn-default" href="javascript:history.back(-1);"><?php echo lang('BACK'); ?></a>
				</div>
			</div>

		</form>
	</div>
	<script src="/static/js/admin.js"></script>
    <script type="text/javascript">
        $(function(){
			Wind.use('layer');
		});	    
        function selectGoods(){
        var url = "/admin/ad/note";
        layer.open({
            type: 2,
            title: '选择帖子',
            shadeClose: true,
            shade: 0.2,
            area: ['75%', '75%'],
            content: url,
        });
        }
        function call_back(id,$status){
            if($status==2){
                $('input[name=shop_id]').val(id)
                layer.closeAll('iframe'); 
            }else{
               $('input[name=note_id]').val(id)
                layer.closeAll('iframe'); 
            }
        }
         function selectshop(){
        var url = "/admin/ad/shop&judge=2";
        layer.open({
            type: 2,
            title: '选择店铺',
            shadeClose: true,
            shade: 0.2,
            area: ['75%', '75%'],
            content: url,
        });
        }
        
        
        (function(){
            $('.btn-cancel-thumbnail').click(function () {
                $('#thumbnail-preview').attr('src', '/themes/admin_simpleboot3/public/assets/images/default-thumbnail.png');
                $('#thumbnail').val('');
            });
        })()

    </script>
</body>
</html>