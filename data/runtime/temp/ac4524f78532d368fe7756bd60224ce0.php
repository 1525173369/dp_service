<?php /*a:1:{s:88:"/www/wwwroot/test01.zwrjkf.com/dp_service/app/merch/controller/../view/public/error.html";i:1628911879;}*/ ?>
<!doctype html>
<html lang="en">
    <head>
      <!-- Required meta tags -->
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Vito - Responsive Bootstrap 4 Admin Dashboard Template</title>
      <!-- Favicon -->
      <link rel="shortcut icon" href="/merch/images/favicon.ico" />
      <!-- Bootstrap CSS -->
      <link rel="stylesheet" href="/merch/css/bootstrap.min.css">
      <!-- Typography CSS -->
      <link rel="stylesheet" href="/merch/css/typography.css">
      <!-- Style CSS -->
      <link rel="stylesheet" href="/merch/css/style.css">
      <!-- Responsive CSS -->
      <link rel="stylesheet" href="/merch/css/responsive.css">
   </head>
    <body>
        <!-- loader Start -->
        <div id="loading">
            <div id="loading-center">
            </div>
        </div>
        <!-- loader END -->
        <!-- Wrapper Start -->
        <div class="wrapper">
            <div class="container-fluid p-0">
                <div class="row no-gutters">
                    <div class="col-sm-12 text-center">
                        <div class="iq-error error-500">
                            <img src="/merch/images/error/03.png" class="img-fluid iq-error-img" alt="">
                            <h2 class="mb-0"><?php echo $msg; ?></h2>
                            <p>Will redirect in:<font id="wait"><?php echo $wait; ?></font>sec</p>
                            <a class="btn btn-primary mt-3" href="javascript:history.go(-1)"><i class="ri-home-4-line"></i>Back to previous page</a>
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- Wrapper END -->
        <!-- Optional JavaScript -->
      <!-- jQuery first, then Popper.js, then Bootstrap JS -->
      <script src="/merch/js/jquery.min.js"></script>
        <script>
           var r = $("#wait").text();
           setInterval(function(){
                 r--;
                 if(r <= 0){
                       window.location.href="javascript:history.go(-1)";
                 }
                 $("#wait").text(r)
            }, r*500);

        </script>

      <script src="/merch/js/popper.min.js"></script>
      <script src="/merch/js/bootstrap.min.js"></script>
      <!-- Appear JavaScript -->
      <script src="/merch/js/jquery.appear.js"></script>
      <!-- Countdown JavaScript -->
      <script src="/merch/js/countdown.min.js"></script>
      <!-- Counterup JavaScript -->
      <script src="/merch/js/waypoints.min.js"></script>
      <script src="/merch/js/jquery.counterup.min.js"></script>
      <!-- Wow JavaScript -->
      <script src="/merch/js/wow.min.js"></script>
      <!-- Apexcharts JavaScript -->
      <script src="/merch/js/apexcharts.js"></script>
      <!-- Slick JavaScript -->
      <script src="/merch/js/slick.min.js"></script>
      <!-- Select2 JavaScript -->
      <script src="/merch/js/select2.min.js"></script>
      <!-- Owl Carousel JavaScript -->
      <script src="/merch/js/owl.carousel.min.js"></script>
      <!-- Magnific Popup JavaScript -->
      <script src="/merch/js/jquery.magnific-popup.min.js"></script>
      <!-- Smooth Scrollbar JavaScript -->
      <script src="/merch/js/smooth-scrollbar.js"></script>
      <!-- lottie JavaScript -->
      <script src="/merch/js/lottie.js"></script>
      <!-- Chart Custom JavaScript -->
      <script src="/merch/js/chart-custom.js"></script>
      <!-- Custom JavaScript -->
      <script src="/merch/js/custom.js"></script>
    </body>
</html>