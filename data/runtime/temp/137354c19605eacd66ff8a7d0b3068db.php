<?php /*a:1:{s:68:"/var/www/ap1.askpert.com/themes/default/portal/page/get_dynamic.html";i:1636177742;}*/ ?>
<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <title>怎么搭怎么好吃 😋 超推168住家菜脯</title>
    <meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1,maximum-scale=1,user-scalable=no" />

    <meta property="og:title" content="怎么搭怎么好吃 😋 超推168住家菜脯">
    <meta property="og:description" content="168 菜脯---住家式下菜拌料！
厨房小白，家庭主妇都爱的下饭菜~
就像我们在餐厅点的“菜脯鱼”的味道一样咱
.
每天煮到不会煮，要花很多时间和力气去做菜！
但自从家里多了这个【168菜脯】吃什么都要撒一点，真的好吃👍
.
不止省下很多时间，而且小小一罐也可以做很多食物♥️
✅口感脆脆的，像在吃中式版的爆米花
✅味道咸咸甜甜，一口吃下去就想再来一口
✅不会有太重的烧焦味，色泽也非常的好看
✅蒸煮炒每一样菜式都可以搭上这个168菜脯
.
我经常拿来配粥，拌面，夹馒头，炒菜，蒸鱼等，吃起来嘴巴真的没有一刻是想停下的！
.
吃法很简单，鱼蒸好后，才撒在上面，其他的菜也是都一样哦，到最后才撒上去 ~
.
我那时候买的价格是 RM25 两罐 (一罐140gm）
.
推荐指数 ⭐️⭐️⭐️⭐️⭐️
超级好下饭的拌料，味道真的很赞，一口吃下去整个嘴巴满满的菜脯香

方便指数 ⭐️⭐️⭐️⭐️⭐️
完全不花时间，不花力气，就可以吃到跟参观一样的菜式

价格指数 ⭐️⭐️⭐️⭐️⭐️
比起在外头餐厅点一个 RM30 的菜脯鱼，把这两罐放在家里😂 直接可以做出百变的菜脯菜式! 很值得👍
.
我是在 FB 和 OC NICE 买的，他们的菜脯是AlorSetar住家式的！真的超级推荐">

            <meta property="og:image" itemprop="image" content="https://static-ap.askpert.com/img_202108152333199277_IOS_.jpg?x-image-process=style/style-400">
        
    <meta property="og:video" content="">
    <meta property="og:type" content="video">
    <meta property="og:source" content="Askpert Review">
    <meta property="og:url" content="https://ap1.askpert.com/portal/page/getDynamic?&uid=10&id=6099&lan=en">

    <script src="/app/static/js/common/lib-flex/flexible.js"></script>
    <link rel="stylesheet" href="/app/static/css/common/reset.css">
    <link rel="stylesheet" href="/app/static/css/mui.min.css">
    <link rel="stylesheet" href="/app/static/css/share/index.min.css">
    <style>
        .mui-toast-container {
            bottom: 50%;
            /* opacity: 0.6; */
            opacity: 0.6;
            color: #fff;
            width: 180px;
            line-height: 50px;
            padding: 70px 5px 10px 5px;
        }

        .mui-card-footer:before,
        .mui-card-header:after {
            content: none !important;
        }

        /*----------------mui.showLoading---------------*/
        .mui-show-loading {
            position: fixed;
            padding: 5px;
            width: 120px;
            min-height: 120px;
            top: 45%;
            left: 50%;
            margin-left: -60px;
            background: rgba(0, 0, 0, 0.6);
            text-align: center;
            border-radius: 5px;
            color: #FFFFFF;
            visibility: hidden;
            margin: 0;
            z-index: 2000;

            -webkit-transition-duration: .2s;
            transition-duration: .2s;
            opacity: 0;
            -webkit-transform: scale(0.9) translate(-50%, -50%);
            transform: scale(0.9) translate(-50%, -50%);
            -webkit-transform-origin: 0 0;
            transform-origin: 0 0;
        }

        .mui-show-loading.loading-visible {
            opacity: 1;
            visibility: visible;
            -webkit-transform: scale(1) translate(-50%, -50%);
            transform: scale(1) translate(-50%, -50%);
        }

        .mui-show-loading .mui-spinner {
            margin-top: 24px;
            width: 36px;
            height: 36px;
        }

        .mui-show-loading .text {
            line-height: 1.6;
            font-family: -apple-system-font, "Helvetica Neue", sans-serif;
            font-size: 14px;
            margin: 10px 0 0;
            color: #fff;
        }

        .mui-show-loading-mask {
            position: fixed;
            z-index: 1000;
            top: 0;
            right: 0;
            left: 0;
            bottom: 0;
        }

        .mui-show-loading-mask-hidden {
            display: none !important;
        }

        .shop-score {
            height: 5vh;
            width: 75%;
            margin-left: 30%;
            margin-top: 2%;
            /* position: relative; */
        }

        .shop-score .score-hide {
            overflow: hidden;
            width: 100%;
            position: relative;
        }

        .shop-score .score-hide img {
            float: left;
            width: 5vw;
            height: 5vw;
            margin-top: 10%;
        }

        .shop-score .score-show {
            overflow: hidden;
            width: 100%;
            position: absolute;
            left: 0;
            top: 0;
        }

        .shop-score .score-show img {
            float: left;
            width: 5vw;
            height: 5vw;
            margin-top: 10%;
        }
    </style>
</head>

<body>
<div id="app" v-on:click="url($event)">
    <div class="mui-card">
        <!--页眉，放置标题-->
        <div class="mui-card-header mui-card-media">
            <img :src="note.avatar" />
            <div class="mui-media-body" style="font-size: 0.5rem; border-bottom: 1px solid #ddd;">
                {{note.user_nickname}}
                <p style="font-size: 0.3rem;">{{text[lan].box7}} {{note.addtime | format}}</p>
            </div>
        </div>
        <!--内容区-->
        <div class="mui-card-content">
            <!-- <img v-if="noteImg[0]" :src="noteImg[0]" /> -->
            <div class="mui-slider">
                <div class="mui-slider-group" onClick="event.cancelBubble = true">
                    <!--第一个内容区容器-->
                    <div v-if="noteImg.length != 0" class="mui-slider-item" v-for="(item, index) in noteImg" :key="index">
                        <img :src="item">
                    </div>
                    <div v-if="note.video.length != 0" class="mui-slider-item" v-for="(item, index) in note.video" :key="index">
                        <video class="width" controls="controls" :src="item"></video>
                    </div>
                </div>
            </div>

            <div class="parameter">
                <div class="start">
                    <div class="grey-img1" v-for="(item,index) in start2" :key="index">
                        <img :src="item">
                    </div>
                    <span>{{note.points}}</span>
                    <div class="start1">
                        <div v-if="note.points >= 1.0" class="grey-img1">
                            <img src="/app/static/images/start1.png">
                        </div>
                        <div v-if="note.points >= 2.0" class="grey-img1">
                            <img src="/app/static/images/start1.png">
                        </div>
                        <div v-if="note.points >= 3.0" class="grey-img1">
                            <img src="/app/static/images/start1.png">
                        </div>
                        <div v-if="note.points >= 4.0" class="grey-img1">
                            <img src="/app/static/images/start1.png">
                        </div>
                        <div v-if="note.points >= 5.0" class="grey-img1">
                            <img src="/app/static/images/start1.png">
                        </div>
                    </div>
                </div>
                <div class="score">
                    <div class="heart">
                        <img src="/app/static/images/heart.png">
                        <span>{{note.likes}}</span>
                    </div>
                    <div class="eye">
                        <img src="/app/static/images/eye.png">
                        <span>{{note.reading}}</span>
                    </div>
                </div>
            </div>
            <div class="topic" v-if="note.topic">
                <div v-for="(item,index) in note.topic" :key="index">
                    #{{item.topic_name}}
                </div>
            </div>
            <div class="title">
                {{note.title}}
            </div>

            <el-row>
                <el-col style="white-space: pre-wrap;">{{note.content}}</el-col>
            </el-row>
            <div v-if="note.shopId != null" class="shop mui-card-header mui-card-media">
                <img style="border-radius: 0.2rem;width: 2rem;height: 2rem;" :src="note.shopThumb" />
                <div class="mui-media-body" style="height: 2rem;line-height: 1rem;font-size: 0.4rem;margin-left: 2.3rem;">
                    <div class="shop-title">
                        <span>{{note.shopName}}</span>
                        <img v-if="note.shopClaim == 1" src="/app/static/images/renzhen.png">
                        <img v-else src="/app/static/images/weirenzhen.png">
                    </div>
                    <div class="start">
                        <div class="grey-img1" v-for="(item,index) in start2" :key="index">
                            <img :src="item">
                        </div>
                        <span>{{note.shopPoints}}</span>
                        <div class="start1">
                            <div v-if="note.points >= 1.0" class="grey-img1">
                                <img src="/app/static/images/start1.png">
                            </div>
                            <div v-if="note.points >= 2.0" class="grey-img1">
                                <img src="/app/static/images/start1.png">
                            </div>
                            <div v-if="note.points >= 3.0" class="grey-img1">
                                <img src="/app/static/images/start1.png">
                            </div>
                            <div v-if="note.points >= 4.0" class="grey-img1">
                                <img src="/app/static/images/start1.png">
                            </div>
                            <div v-if="note.points >= 5.0" class="grey-img1">
                                <img src="/app/static/images/start1.png">
                            </div>
                        </div>
                        <span>| {{note.comments}} {{text[lan].box5}}</span>
                    </div>
                </div>
            </div>
        </div>
        <!--页脚，放置补充信息或支持的操作-->
        <div class="mui-card-footer">
            {{text[lan].box4}}({{count}})
        </div>
    </div>
    <div class="comment" v-if="noteComment.length !=0">

        <div class="comment-item" v-for="(item,index) in noteComment" :key="index">

            <div class="list mui-card-header mui-card-media">
                <img :src="item.avatar" />
                <div class="mui-media-body">
                    {{item.username == ''? 	text[lan].box6 : item.username}}
                    <div class="text">
                        <img class="heart" src="/app/static/images/heart.png" style="width: 0.4rem;height: 0.4rem;">
                        <span>{{item.zan_num}}</span>
                    </div>
                    <p>{{item.content}} {{text[lan].box7}} {{item.create_time}}</p>
                </div>
            </div>

        </div>

    </div>
    <div  v-if="noteComment == 0" class="wu">
        <img src="/app/static/images/zanwuxinxi.png">
        {{text[lan].box8}}
    </div>
    <script src="/app/static/js/common/mui.min.js"></script>
    <script src="/app/static/js/common/jquery-1.10.1.min.js"></script>
    <script src="/app/static/js/common/vue.js"></script>
    <script>
        //扩展mui.showLoading
        (function ($, window) {
            //显示加载框
            $.showLoading = function (message, type) {
                if ($.os.plus && type !== 'div') {
                    $.plusReady(function () {
                        plus.nativeUI.showWaiting(message);
                    });
                } else {
                    var html = '';
                    html += '<i class="mui-spinner mui-spinner-white"></i>';
                    html += '<p class="text">' + (message || "数据加载中") + '</p>';

                    //遮罩层
                    var mask = document.getElementsByClassName("mui-show-loading-mask");
                    if (mask.length == 0) {
                        mask = document.createElement('div');
                        mask.classList.add("mui-show-loading-mask");
                        document.body.appendChild(mask);
                        mask.addEventListener("touchmove", function (e) {
                            e.stopPropagation();
                            e.preventDefault();
                        });
                    } else {
                        mask[0].classList.remove("mui-show-loading-mask-hidden");
                    }
                    //加载框
                    var toast = document.getElementsByClassName("mui-show-loading");
                    if (toast.length == 0) {
                        toast = document.createElement('div');
                        toast.classList.add("mui-show-loading");
                        toast.classList.add('loading-visible');
                        document.body.appendChild(toast);
                        toast.innerHTML = html;
                        toast.addEventListener("touchmove", function (e) {
                            e.stopPropagation();
                            e.preventDefault();
                        });
                    } else {
                        toast[0].innerHTML = html;
                        toast[0].classList.add("loading-visible");
                    }
                }
            };

            //隐藏加载框
            $.hideLoading = function (callback) {
                if ($.os.plus) {
                    $.plusReady(function () {
                        plus.nativeUI.closeWaiting();
                    });
                }
                var mask = document.getElementsByClassName("mui-show-loading-mask");
                var toast = document.getElementsByClassName("mui-show-loading");
                if (mask.length > 0) {
                    mask[0].classList.add("mui-show-loading-mask-hidden");
                }
                if (toast.length > 0) {
                    toast[0].classList.remove("loading-visible");
                    callback && callback();
                }
            }
        })(mui, window);

        var host = window.location.host == '127.0.0.1:5500' ? 'http://dp.zwrjkf.cn' : 'https://' + window.location.host;

        // var host = 'https://ap1.askpert.com'

        var app = new Vue({
            el: '#app',
            data: {
                uid: '',
                id: '',
                p: 1,
                note: {}, //帖子详情
                noteComment: '', //评论详情
                noteImg: [], //内容图片
                count: 0,
                // 			classObject:{
                // 				active: true,
                // 'text-danger': false
                // 			},
                start1: [
                    '/app/static/images/start1.png',
                    '/app/static/images/start1.png',
                    '/app/static/images/start1.png',
                    '/app/static/images/start1.png',
                    '/app/static/images/start1.png',
                ],
                start2: [
                    '/app/static/images/start2.png',
                    '/app/static/images/start2.png',
                    '/app/static/images/start2.png',
                    '/app/static/images/start2.png',
                    '/app/static/images/start2.png',
                ],
                text: {
                    'gb': {
                        'box1': '分享',
                        'box2': '正在加载...',
                        'box3': '访问错误',
                        'box4': '评论',
                        'box5': '人',
                        'box6': '游客',
                        'box7': '发表于',
                        'box8': '暂无评论',
                    },
                    'en': {
                        'box1': 'Share',
                        'box2': 'Loading...',
                        'box3': 'Access error',
                        'box4': 'comment',
                        'box5': 'people',
                        'box6': 'tourist',
                        'box7': 'Published in',
                        'box8': 'No comment',
                    },
                    'ms': {
                        'box1': 'Kongsi',
                        'box2': 'Memuatkan',
                        'box3': 'Ralat akses',
                        'box4': 'Komen',
                        'box5': 'people',
                        'box6': 'pelancong',
                        'box7': 'Dipublik dalam',
                        'box8': 'Tiada komen',
                    }
                },
                lan: ''
            },
            created() {
                this.init()
                this.getDetailsNote().then(res => {
                    console.log(res)
                    this.note = res[0].details
                    this.noteImg = res[0].details.thumb
                    this.topic = res[0].details.topic
                    console.log(this.noteImg[0])
                    console.log(this.topic[0].topic_name)
                    this.setStart()
                })
                this.getNoteComment().then(res => {
                    console.log(res);
                    this.noteComment = res.data.info[0].list
                    console.log(this.noteComment);
                    this.count = res.data.info[0].count
                })
            },

            mounted() {


            },
            updated: function() {
                this.$nextTick(()=>{
                    var gallery = mui('.mui-slider');
                    gallery.slider();
                })
            },

            methods: {
                //-------------------------网络请求相关-----------------------------------
                //获取帖子详情
                getDetailsNote() {

                    return new Promise((resolve, reject) => {
                        mui.showLoading(this.text[this.lan].box2, "div"); //加载文字和类型，plus环境中类型为div时强制以div方式显示
                        $.ajax(host + '/appapi/?service=publish.detailsNote', {
                            data: {
                                uid: this.uid,
                                id: this.id,
                                token:"askpert"
                                // id: 1,
                            },
                            dataType: 'json', //服务器返回json格式数据
                            type: 'get', //HTTP请求类型
                            success: function (data) {

                                mui.hideLoading(); //隐藏后的回调函数
                                if (data.data.code !== 0) {
                                    mui.toast('请求失败', {
                                        duration: 'long',
                                        type: 'div'
                                    })
                                } else if (!data.data.msg) {

                                    return resolve(data.data.info)
                                } else {
                                    mui.toast(data.data.msg, {
                                        duration: 'long',
                                        type: 'div'
                                    })
                                }
                            },
                            error: function (xhr, type, errorThrown) {
                                //异常处理；
                                console.log(errorThrown);
                            }
                        });
                    });
                },
                //获取帖子评论
                getNoteComment() {
                    return new Promise((resolve, reject) => {
                        mui.showLoading(this.text[this.lan].box2, "div"); //加载文字和类型，plus环境中类型为div时强制以div方式显示
                        $.ajax(host + '/appapi/?service=comment.getNoteComment', {
                            data: {
                                id: this.id,
                                p: this.p,
                                uid: this.uid
                            },
                            dataType: 'json', //服务器返回json格式数据
                            type: 'get', //HTTP请求类型
                            success: function (data) {
                                mui.hideLoading(); //隐藏后的回调函数
                                if (data.data.code !== 0) {
                                    mui.toast(this.text[this.lan].box3, {
                                        duration: 'long',
                                        type: 'div'
                                    })
                                } else {
                                    return resolve(data)
                                }
                            },
                            error: function (xhr, type, errorThrown) {
                                //异常处理；
                                console.log(errorThrown);
                            }
                        });
                    });
                },

                //-------------------------网络请求相关结束-----------------------------------
                //-------------------------点击事件相关-----------------------------------
                //初始化
                init() {
                    this.uid = this.getQueryString('uid')
                    this.token = this.getQueryString('token')
                    this.id = this.getQueryString('id')
                    this.lan = this.getQueryString('lan')
                    if(this.lan == 'undefined'){
                        this.lan = 'ms';
                    }
                    console.log(this.lan)
                    //document.title = this.text[this.lan].box1
                    mui.init()
                },
                getQueryString(name) {
                    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
                    var r = window.location.search.substr(1).match(reg);
                    if (r != null)
                        return unescape(r[2]);
                    return 'undefined';
                },
                //设置样式
                setStart() {
                    if (this.note.points == '1.0') {
                        this.isShow = true
                    } else if (this.note.points == '2.0') {
                        this.isShow = true
                        this.isShow1 = true
                    } else if (this.note.points == '3.0') {
                        this.isShow = true
                        this.isShow1 = true
                        this.isShow2 = true
                    } else if (this.note.points == '4.0') {
                        this.isShow = true
                        this.isShow1 = true
                        this.isShow2 = true
                        this.isShow3 = true
                    } else {
                        this.isShow = true
                        this.isShow1 = true
                        this.isShow2 = true
                        this.isShow3 = true
                        this.isShow4 = true
                    }
                },
                getQueryString(name) {
                    var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
                    var r = window.location.search.substr(1).match(reg);
                    if (r != null)
                        return unescape(r[2]);
                    return 'undefined';
                },
                //点击跳转
                url(event) {
                    var el = event.currentTarget;
                    // window.location.href= host + https://ap1.askpert.com/appapi/down/index
                    window.location.href = host + '/appapi/down/index'
                }
                //-------------------------点击事件相关结束-----------------------------------

            },
            filters: {
                format(data, fmt) {
                    if (!data) return "";
                    var timeStr = new Date(parseInt(data * 1000));
                    var fmt = fmt || "yyyy-MM-dd hh:mm:ss";
                    var o = {
                        "M+": timeStr.getMonth() + 1, //月份
                        "d+": timeStr.getDate(), //日
                        "h+": timeStr.getHours(), //小时
                        "m+": timeStr.getMinutes(), //分
                        "s+": timeStr.getSeconds(), //秒
                        "q+": Math.floor((timeStr.getMonth() + 3) / 3), //季度
                        "S": timeStr.getMilliseconds() //毫秒
                    };
                    // 如果 fmt 中有y,fmt中y替换为timeStr.getFullYear()，例：
                    // yyyy-MM-dd hh:mm:ss 替换为 2018-MM-dd hh:mm:ss
                    // yy-MM-dd hh:mm:ss 替换为 18-MM-dd hh:mm:ss
                    if (/(y+)/.test(fmt)) fmt = fmt.replace(RegExp.$1, (timeStr.getFullYear() + "").substr(4 - RegExp.$1
                        .length));
                    // 下面循环原理同上
                    for (var k in o)
                        if (new RegExp("(" + k + ")").test(fmt)) fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) :
                            (("00" + o[k]).substr(("" + o[k]).length)));
                    return fmt;
                }
            }
        })
    </script>
</body>

</html>