<?php
/**
 * 配置文件
 */

return [
    // 数据库类型
    'type'     => 'mysql',
    // 服务器地址
    //'hostname' => '127.0.0.1',
    'hostname' => env('DB_HOST'),//'192.168.0.41',
    // 数据库名
    'database' => env('DB_DATABASE'),//'askp_dp_app',
    // 用户名
    'username' => env('DB_USERNAME'),//'dpapp_usr2813',
    // 密码
    //'password' => 'uX83,as01Epc42.-Az',
    'password' =>  env('DB_PASSWORD'),//'uX83,as01Epc42-Az',
    // 端口
    'hostport' => env('DB_PORT'),//'3306',
    // 数据库编码默认采用utf8
    'charset'  => env('DB_CHARSET'),//'utf8mb4',
    // 数据库表前缀
    'prefix'   => env('DB_PREFIX'),//'cmf_',
    "authcode" => 'zcdl8iiGDB3zANY9kI',
    //#COOKIE_PREFIX#
    'REDIS_HOST' => env('REDIS_HOST'),//"127.0.0.1",
//    'REDIS_HOST' => "redis-19cf5af7-616a-4841-b912-3bc5e1f3ccbb.dcs.huaweicloud.com",
    'REDIS_AUTH' => env('REDIS_AUTH'),//"akyChN7DmB7mNtLB",
    'REDIS_PORT' => env('REDIS_PORT'),//"6379",
];
