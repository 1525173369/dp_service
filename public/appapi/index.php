<?php
/**
 * Appapi 统一入口
 */
define('BASE_PATH',__DIR__.'/../../');
if(!function_exists('putenv')){
    exit('请开启禁用函数putenv');
}
$env = parse_ini_file(BASE_PATH.'.env', true);  //env简单填入
foreach ($env as $k=>$v){
  $item = strtoupper($k);
  putenv("$item=$v");
}
 
require_once dirname(__FILE__) . '/../../PhalApi/public/init.php';

//装载你的接口
DI()->loader->addDirs('Appapi');

/** ---------------- 响应接口请求 ---------------- **/
define('FWS',__DIR__.'/../ffmpeg/');
ini_set ('get_magic_quotes_gpc', 1);
$r=  $_REQUEST;
file_put_contents('log.log',json_encode($r)."\n",FILE_APPEND);
try{
    $api = new PhalApi();
    $rs = $api->response();
    $rs->output();
}catch (\Exception $e){
    //{"ret":200,"msg":"","data":{"code":0,"info":[],"msg":"Request submitted"}}
    $data = [
        'ret' => 200,
        'msg' => $e->getMessage(),
        'data' => [
            'code' => 101,
            'info' => [],
            'msg' => $e->getMessage()
        ]
    ];
    if($_GET['_debug'] == 1){
        echo "<pre>";
        var_dump($e);
    }else{
        header('content-type:application/json');
        exit(json_encode($data));
    }


}


