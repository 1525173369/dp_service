function SYSDrag(parions) {
	this._initial(parions);
};
//拖拽功能模块方法
SYSDrag.prototype = {
	constructor: this,
	_initial:function(parions){
		var par = {
			DragModule:null,//拖拽区域
			DragData:null,//拖拽数据
			DragMethod:null,//定义各类模块
			DragWidth:null,//拖拽区域宽度 ,不填默认浏览器大小
			DragHeight:null,//拖拽区域高度,不填默认浏览器大小
			DragLevel:null,//拖拽模块层级
			Dragclass:null,//拖拽的类
			DragContent:null,//内容区域
			DragVersion:"1.0",//拖拽版本
			Ylattice:null, //拖拽区域Y轴数量
			Xlattice:null,//拖拽区域x轴数量
			lattice_x:0,//起始位置x轴坐标
            lattice_y:0,//起始位置Y轴坐标
			DragSlide:true,//是否允许拖拽栏目	
			DragOperation:null,
			DrageditMethod:null,
			DragModuleHeight:null,//设置每个模块高度
			DragSort:[],
			DragArray:[],
			EditClass:null,
			SessionName:null,
			SubmitName:null,//提交处理事件
			ResetName:null,
			languageData:null,//语言数据
			CkPrompt:false,//是否启用搜索提示信息
			SubmitEvent:function(){},
			RefactoringEvent:function(){},//删除全部从新编辑
			DragExtension:function(){},//拖拽扩展	
			ModuleExtension:function(){},//模块扩展
			ModuleUpload:function(){}//上传模块方法
		};
		//封装extend合并方法
//	    this.extend=function(o, n, override) {
//			for(var key in n) {
//				if(n.hasOwnProperty(key) && (!o.hasOwnProperty(key) || override)){
//					o[key] = n[key];
//				}
//			}
//			return o;
//		};
		this.extend=function(des, src, override){
		  if(src instanceof Array){
		    for(var i = 0, len = src.length; i < len; i++)
		       extend(des, src[i], override);
		  }
		  for( var i in src){
		    if(override || !(i in des)){
		      des[i] = src[i];
		    }
		  }
		  return des;
		}
		// Id与class方法简写
		this.$=function(name) {
			var expression=/^\#.*$/;
			var v=expression.test(name);
			if(v==true){
				if(name.indexOf("#")!= -1){
					name= document.getElementById(name.slice(1));
				}
			}else{
				if(name.indexOf(".")!= -1){
					name= document.getElementsByClassName(name.slice(1))[0];
				}
			}
			return name;
		};
		//判断是否存在class属性方法
		this.hasClass = function(elements, cName) {
			return !!elements.className.match(new RegExp("(\\s|^)" + cName + "(\\s|$)"));
		};
		//添加class属性方法
		this.addClass = function(elements, cName) {
			var arr=[];
			var result=cName.split(",");
			for(var l=0;l<result.length;l++){
				if(!this.hasClass(elements, cName)) {
					if(l==0){
						elements.className +=result[l];
					}else{
						elements.className += " " + result[l];
					}
			   };
			}
		};
		//删除class属性方法 elements当前结构  cName类名
		this.removeClass = function(elements, cName) {
			var str = cName.search("-")
			if(this.hasClass(elements, cName)) {
				elements.className = elements.className.replace(new RegExp("(\\s|^)" + cName + "(\\s|$)"), " "); // replace方法是替换
			};
		};
		//event方法简写
		this.eve=function(eve){
			var evt = eve || window.event; //指向触发事件的元素
			var obj = evt.target || evt.srcElement || eve.srcElement; //指向触发事件的元素
			return obj;
		},
		//根据class类名条件筛选结构
		this.getByClass = function(oParent, sClass) { //根据class获取元素
			var oReasult = [];
			var oEle = oParent.getElementsByTagName("*");
			for(i = 0; i < oEle.length; i++) {
				if(oEle[i].className == sClass) {
					oReasult.push(oEle[i]);
				}
			};
			return oReasult;
		};
		//指定sysAttributeNode添加方法简写
		this.Attributes=function(parent,name,value){
			var Attributes = document.createAttribute(name);
			Attributes.nodeValue =value;
			return parent.setAttributeNode(Attributes);
		};
		this.addEvent=function (a, b, c, d) {
		//添加函数
		   if (a.attachEvent) a.attachEvent(b[0], c);
		   else a.addEventListener(b[1] || b[0].replace(/^on/, ""), c, d || false);
		   return c;
		};
		this.delEvent=function (a, b, c, d) {
		   if (a.detachEvent) a.detachEvent(b[0], c);
		   else a.removeEventListener(b[1] || b[0].replace(/^on/, ""), c, d || false);
		   return c;
		};
		this.reEvent =function () {
		//获取Event
		   return window.event ? window.event : (function (o) {
		    do {
		     o = o.caller;
		    } while (o && !/^\[object[ A-Za-z]*Event\]$/.test(o.arguments[0]));
		    return o.arguments[0];
		   })(this.reEvent);
       };
		//根据class类名条件筛选结构
		this.getElementsByClassName = function(parent, className) {
			//获取所有父节点下的tag元素　
			var aEls = parent.getElementsByTagName("*");　　
			var arr = [];
			//循环所有tag元素　
			for(var i = 0; i < aEls.length; i++) {
				//将tag元素所包含的className集合（这里指一个元素可能包含多个class）拆分成数组,赋值给aClassName	　　　　
				var aClassName = aEls[i].className.split(' ');　　　　 //遍历每个tag元素所包含的每个className
				for(var j = 0; j < aClassName.length; j++) {　　　　　　 //如果符合所选class，添加到arr数组				　　　　　
					if(aClassName[j] == className) {　　　　　　　　
						arr.push(aEls[i]);　　　　　　　　 //如果className里面包含'box' 则跳出循环						　　　　　　　　
						break; //防止一个元素出现多次相同的class被添加多次						　　　　　　
					}　　　　
				};　　
			};　　
			return arr;
		};
		this.addLoadListener=function(fn) {
			if(typeof window.addEventListener != 'undefined') {
				window.addEventListener('load', fn, false);
			} else if(typeof document.addEventListener != 'undefined') {
				document.addEventListener('load', fn, false);
			} else if(typeof window.attachEvent != 'undefined') {
				window.attachEvent('onload', fn);
			} else {
				var oldfn = window.onload;
				if(typeof window.onload != 'function') {
					window.onload = fn;
				} else {
					window.onload = function() {
						oldfn();
						fn();
					};
				}
			}
		};
		this.Width=function() {
			return self.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
		};
		this.Height=function() {
			return self.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
		};
		this.name=function(i) {
			return document.getElementsByName(i);
		};
		this.closetag=function(parent,tag,dataid){
	        var tags = parent.getElementsByTagName(tag);
	        for(var i=tags.length-1;i>=0;i--){
	            if(dataid==null){
	                tags[i].outerHTML = tags[i].innerHTML;                
	            }else{
	                if(tags[i].getAttribute("sys-tagName")===dataid){
	                    tags[i].outerHTML = tags[i].innerHTML;
	                }
	            }                
	        }
	    };
		//删除指定区域element方法
		this.removeElement = function(_element) {
			var _parentElement = _element.parentNode;
			if(_parentElement) {
				_parentElement.removeChild(_element);
			};
		};
		this.par = this.extend(par, parions, true);
		this.show(this.par,this);
	},
	show:function(par,sys){
		var obj=sys.$(par.DragModule);
		var DragWidth=par.DragWidth;
		var DragHeight=par.DragHeight;
		var Method=par.DragMethod;
		var Dragclass=par.Dragclass;
		var sessionArray=par.DragArray;
		var sessionname=par.SessionName;
		sys.DragMagnitude(sys,par,DragWidth,DragHeight,obj);
		if(Dragclass!=null){
			sys.Moduleclass(sys,par,obj);
		}
		sys.Methodsys(sys,Method,par);
		document.onmousedown=function(eve){
			var Event=sys.eve(eve);
			var content=sys.$("#DragContent");
		    var operate=Event.getAttribute("sys-operate");
		    if(operate=="drag"){
		    	sys.slidemodule(eve,sys,content,par);
		    	return false;
		    }
		    var id=Event.id;
		    if(id=="slider"){
		    	sys.scrollbar(sys,Event,content,'onMouseMove');
		    }
		}
		var Dragedit=sys.$(par.DrageditMethod);
		var list=sys.getElementsByClassName(Dragedit,"sys-type-module");
		list.myMap(function(item,i){
			var t=item.getAttribute("title");
			var y=item.getAttribute("sys-id");
			var html=item.innerHTML;
			sys.sessionArr(html, t, y, sessionArray,sessionname);
		});
	    Dragedit!=null?sys.removeElement(Dragedit):'';
		sys.MethodClick(sys, par,obj);
	},
	//点击事件处理模块
	MethodClick:function(sys, par,obj){
		 var Dragedit=sys.$(par.DrageditMethod);
		 var EditClass=par.EditClass;
		 var submit=sys.$(par.SubmitName);
		 var reset=sys.$(par.ResetName);
		 var MH=par.DragModuleHeight;
		 
		 MH==null?MH=150:MH=MH;
		 var ABC=["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
		 var content=sys.$("#DragContent");
		 sys.addEvent(submit, ["onclick"], sys.submitMethod.bind(sys,par,obj));//提交
		 sys.addEvent(reset, ["onclick"], sys.resetMethod.bind(sys,par,obj));//重置 
		 document.onclick=function(eve){
			var reEvent=sys.eve(eve);
			var operate=reEvent.getAttribute("sys-operate");
			var id=reEvent.getAttribute("sys-id");
			var title=reEvent.getAttribute("title");
			var amount=reEvent.getAttribute("sys-amount");
			var seat=reEvent.getAttribute("sys-seat");
			var cont=content.childNodes;
			var Event=null;
			var arrEvent=function(Event){
				for(var n = 0; n < cont.length; n++) {
			    	var d=cont[n].getAttribute("sys-id");
		            var t=cont[n].getAttribute("title");
			    	if(title==t || id==d){
			    		var mod=sys.getElementsByClassName(cont[n],"sys-edit-content");
			    		if(amount==null){
			    			Event=reEvent.parentNode.nextSibling;
			    		}else{
			    			mod.myMap(function(item,i){
				    			var s=item.getAttribute("sys-amount");
				    			if(s==amount){
				    				Event=item;
				    			}
			    		    })
			    		}
			    	}
				}
				return Event;
			}
			if(operate=="delete"){
				var DragSort=par.DragSort;
				if(DragSort.length!=0){
					for(var i = 0; i < DragSort.length; i++) {
						if(reEvent.getAttribute('title') == DragSort[i].title) {
							DragSort.splice(i, 1);
							for(var n = 0; n < cont.length; n++) {
						    	var d=cont[n].getAttribute("sys-id");
					            var t=cont[n].getAttribute("title");
						    	if(title==t || id==d){
						    		sys.removeElement(cont[n]);
						    	}
							}
						}
					}
				}
			}else if(operate=="add"){
				Event=arrEvent(Event);
				sys.DrageditMethod(sys, par,reEvent,operate,id,title,ABC,Event);
				var conttext=sys.conttext(conttext,null);
				sys.FormdataMethod(sys, par, Event);
				sys.Wordcount(sys, conttext, null);
				var reqlist= sys.getElementsByClassName(Event,'sys-Required');
				reqlist.myMap(function(items){
					var mode=items.getAttribute("sys-mode");
					if(mode=="files"){
						par.ModuleUpload(sys,par,items);
					}
				})				
			}else if(operate=="add-select"){
			    Event=arrEvent(Event);
				sys.DrageditMethod(sys, par,reEvent,operate,id,title,ABC,Event);
			}else if(operate=="delete-select"){
				Event=arrEvent(Event);
				var li=sys.getElementsByClassName(Event,"sys-edit-condition");
				var region= sys.getElementsByClassName(Event,'sys-region');
				var bout=null;
				region.myMap(function(items){
			  	   var t=items.getAttribute("sys-bout");
			  	   t!=null?bout=t:'';
				})
			    var n=li.length;
			    if(bout==n){   
			    	sys.PromptBox('至少保留'+bout+"个选项", 2, false);
			    }else{
			    	var site=li[seat];
			    	var parent=li[seat-1];
			    	var btn=sys.getElementsByClassName(site,"sys-edit-button");
			    	var parbtn=sys.getElementsByClassName(parent,"sys-edit-button");
			    	var lay=parbtn[0].parentNode;
			    	var parop=parbtn[0].getAttribute("sys-operate");
			    	btn.myMap(function(ent){
			    		var op=ent.getAttribute("sys-operate");
			    		sys.Attributes(ent,"sys-seat",seat-1);
			    		if(op!=parop){
			    		 	lay.innerHTML+=ent.outerHTML;
			    		}
			    	})
			    	sys.removeElement(site) ;
			    }
			}else if(operate=="answer-select"){
				//选择方式方法
				Event=arrEvent(Event);
				var mode=reEvent.getAttribute("sys-mode");
				var li=sys.getElementsByClassName(Event,"sys-edit-condition");
				var site=li[seat];
				var btns=EditClass;
				li.myMap(function(ent,i){
					var btn=sys.getElementsByClassName(ent,"sys-edit-button");
					var answer=ent.getAttribute("sys-answer");
					for(var n = 0; n < btn.length; n++) {
						var parmode=btn[n].getAttribute("sys-mode");
						if(parmode=="radio"){
							//单选
							sys.Attributes(ent,"sys-answer",false);
							btn[n].innerHTML="选择";
							sys.Attributes(site,"sys-answer",true);
							sys.addClass(btn[n]," sys-answer");
							reEvent.innerHTML="<i class='iconfont'>&#xe712;<i>";
						}else if(parmode=="checkbox"){
							//复选
							sys.Attributes(site,"sys-answer",true);
						    //单选
							sys.addClass(btn[n]," sys-answer");
							reEvent.innerHTML="<i class='iconfont'>&#xe712;<i>";
						}
					}
				})
			} else if(operate=="delete-topics"){
				for(var n = 0; n < cont.length; n++) {
			    	var d=cont[n].getAttribute("sys-id");
		            var t=cont[n].getAttribute("title");
			    	if(title==t || id==d){
			    		var conth=cont[n].offsetHeight;
			    		var mod=sys.getElementsByClassName(cont[n],"sys-edit-content");
			    		var mh=mod[0].offsetHeight;
			    		sys.removeElement(mod[0]);
			    		var h=conth-mh;
			    		h<MH?h=MH:h=h;
			    		cont[n].style.minHeight =h+'px';
			    	}
				}
			}else if(operate=="stretch"){
					for(var n = 0; n < cont.length; n++) {
				    	var d=cont[n].getAttribute("sys-id");
			            var t=cont[n].getAttribute("title");
				    	if(title==t || id==d){
				    		var child=cont[n].childNodes;
				    		child[1].style.display="none";
				    		cont[n].style.minHeight="";
				    		cont[n].style.overflow="hidden";
				    		sys.Attributes(reEvent,"sys-operate","hide");
				    		reEvent.innerHTML="&#xe602;"
				    	}
					}				
			}else if(operate=="hide"){
				for(var n = 0; n < cont.length; n++) {
			    	var d=cont[n].getAttribute("sys-id");
		            var t=cont[n].getAttribute("title");
			    	if(title==t || id==d){
			    		var child=cont[n].childNodes;
			    		child[1].style.display="block";
			    		cont[n].style.minHeight="";
			    		cont[n].style.overflow="hidden";
			    		sys.Attributes(reEvent,"sys-operate","stretch");
			    		reEvent.innerHTML="&#xe61e;"
			    	}
				}	
			}
			var onfocusMobile = function(eve) {
				var tar =sys.eve(eve);
				var conttext=sys.conttext(conttext,null);
				if(tar.tagName.toLowerCase() == "textarea") {
					sys.addLoadListener(sys.Wordcount(sys, conttext, eve));
				}
				var eventname = tar.type;
				sys.PromptModeMethod(tar, sys, eventname);
			};
			var onblurMobile = function(eve) {
				var textname = "不能为空！";
				var tar =sys.eve(eve);
				if(tar.tagName.toLowerCase() == "input" || tar.tagName.toLowerCase() == "textarea") {
					var conttext=sys.conttext(conttext,null);
					var index = tar.selectedIndex; // 选中索引
					var checked = tar.checked;
					var Hints = tar.getAttribute('sys-name');
					var verify = tar.getAttribute('sys-verify');
					if(index != null) {
						var selectname = tar.options[index].value;
						if(selectname == "0") {
							sys.newprompt(textname, Hints, sys, tar);
						} else {
							sys.prompthtml(tar,sys);
						}
					} else if(tar.value != "") {
						var promptname = tar.getAttribute('sys-prompt');
						sys.Attributes(tar,"value",tar.value);
						for(var i = 0; i < conttext.length; i++) {
							var dataprompt = conttext[i].getAttribute('sys-prompt');
							if(dataprompt == "password") {
								var zhi = conttext[i].value;
							}
						}
						if(checked!=true){
							sys.newprompt(textname, Hints, sys, tar);
						}	
					} else {
						sys.Attributes(tar,"value","");
						if(verify == "verify") {
							sys.newprompt(textname, Hints, sys, tar);
						}
					}
				}
				var eventname = tar.type;
				sys.PromptModeMethod(tar, sys, eventname);
			};
			if(reEvent.tagName.toLowerCase() == "input" || reEvent.tagName.toLowerCase() == "textarea"){
				//事件会在对象失去焦点时发生
				reEvent.onblur = function(eve) {
					onblurMobile(eve);
				}
				reEvent.onfocus = function(eve) {
					onfocusMobile(eve)
				};	
		    }
		}
	},
	//设置提示模式
	PromptModeMethod: function(tar, sys, name) {
		if(name == "focus") {
			sys.removeClass(tar.parentNode, "form_prompt");
			sys.addClass(tar.parentNode, "form_errors");
		} else if(name == "blur" || name == "click") {
			sys.removeClass(tar.parentNode, "form_errors");
			if(tar.value == null) {
				sys.addClass(tar.parentNode, "form_prompt");
			} else {
				sys.removeClass(tar.parentNode, "form_prompt");
			}
		}
	},
	//一个提示方法pc端
	newprompt: function(name, Hints, sys, obj) {
		var v= obj.value,mu=0;
		var prompt = obj.parentNode.getElementsByTagName('span');
		var prompthtml=function(has){
			if(!has){
				mu++;
				var newspan = document.createElement("span");
				sys.addClass(newspan,"sys-prompt iconfont");
				newspan.innerHTML=Hints + name;
				obj.parentNode.appendChild(newspan);
			}	
		}
		if(prompt.length==0){
			prompthtml(false);
		}else{
			for(var p=0; p<prompt.length;p++){
				var has=sys.hasClass(prompt[p],"sys-prompt");
				var wordhas=sys.hasClass(prompt[p],"word_count");
				if(has){
					if(v==""){
						sys.removeElement(prompt[p]);
						prompthtml(has);
					}else{
						mu++;
						sys.removeElement(prompt[p]);
					}
				}
		    }
			if(mu==0){
				
				if(v==""){
				prompthtml(false);
				}
			}
		}
	},
	//清除提示信息
	prompthtml: function(obj,sys) {
		var prompt = obj.parentNode.getElementsByTagName('span');
		if(prompt) {
			var has=sys.hasClass(prompt,"sys-prompt");
			if(has){
				var prompthtml = obj.parentNode.removeChild(prompt);
			}
		}
		return prompthtml;
	},
	//列表重组集合方法
	conttext: function(setlist,module) {
		var sys = this,region="";
		if(module==null){
			region = sys.$("#DragContent");
		}else{
			region = module;
		}
		var setlist = new Array();
		var sets = sys.getElementsByClassName(region, "sys-Required");
		var up = sys.name("files"); //上传
		for(var i = 0; i < sets.length + up.length; i++) {
			var name = "";
			for(var u = 0; u < up.length; u++) {
				up[u] != null ? name = up[0].type : '';
			}
			if(up[i] != null) {
				var ele = up[i];
			} else {
				if(!name) {
					var ele = sets[i];
				} else {
					var ele = sets[i - 1];
				}
			}
			setlist.push(ele); //重组集合数组 
		}
		return setlist;
	},
	submitMethod:function(par,lay,MouseEvent){
		var sys=this;
		var content=sys.$("#DragContent");
		var setvalue = [], mun = 0,prompt="不能为空!",formData = "",topicdata="",topicArray=[];
		var containSpecial = RegExp(/[(\ )(\~)(\!)(\@)(\#)(\$)(\%)(\^)(\&)(\*)(\()(\))(\-)(\_)(\+)(\=)(\[)(\])(\{)(\})(\|)(\\)(\;)(\:)(\')(\")(\,)(\.)(\/)(\<)(\>)(\?)(\)]+/);    
		var conttext=sys.conttext(conttext,null);
		var sort=sys.getElementsByClassName(content,"sort-module");
	    sort.myMap(function(item,i){
	 		var id=item.getAttribute("sys-id");
	 		var title=item.getAttribute("title");
	 		var level=item.getAttribute("sys-level");
	 		var structure={
		 		FormId:id,
		 		FormTitle:title,
		 		Formlevel:level,
		 		FormTopic:topicdata
		 	}
	 		topicArray.push(structure); //添加数组
	 	})
	    
		if(conttext.length!=0){
			 conttext.myMap(function(item,i){
			 	var verify =item.getAttribute('sys-verify');
			 	var Hints =item.getAttribute('sys-name');
				var promptname =item.getAttribute('sys-prompt');
				var mode =item.getAttribute('sys-mode');
				
				if(item.value == "") {
					if(verify == "verify") {
						var eventname = MouseEvent.type;
						sys.PromptModeMethod(item, sys, eventname);
						sys.newprompt(prompt, Hints, sys, item);
						setvalue.push(i);
					}
				}
				var content = item.value;
				if(content!= "") {				
					//sys.prompthtml(item);
				}
			 })
			 if(setvalue.length == 0) {
			 	var am=0;
			 	var traverse=function(list,newarr,group,event,i,fabric){	
			 		var answer=null,amount=null;
			 		if(event!=null){
			 			answer=event.getAttribute("sys-answer"); 
			 			amount=event.getAttribute("sys-amount");
			 			if(answer!=null){
				 			amount!=null?group["id"] = i:'';
				 			amount!=null?group["pid"] = parseInt(amount):'';
				 		if(answer=="true"){
			 			    group["answer"] = answer;	
				 		}else if(answer=="false"){
				 			group["answer"] = answer;
				 		}
				 		}else{
				 			amount!=null?group["id"] =  parseInt(amount):'';
				 			amount!=null?group["pid"] = 0:'';
				 			
				 		}
			 		};
			 		list.myMap(function(item){
			 			var v="";
 						var keyvalue = item.getAttribute("data-key");
						var keypassword = item.getAttribute("data-value");
						var datatype = item.getAttribute("data-type");
						var mode = item.getAttribute("sys-mode");
						var name = item.getAttribute("sys-name");
						var muster = sys.getElementsByClassName(item, 'SelectionBox');
						var verupload = item.type;
						group[keyvalue] = '';
						if(muster.length != 0) {
							if(mode == 'radio') {
								for(var c = 0; c < muster.length; c++) {
									var checkedname=muster[c].childNodes[0];
									if(checkedname.checked == true) {
										var v = checkedname.value;
										group[keyvalue] = v;
									}
								}
							} else if(mode =='checkbox') {
								var checkboxArray = []; //声明一个数组
								var ArrayString = item.getAttribute("data-Array");
								var result = ArrayString.split(",");
								for(var c = 0; c < muster.length; c++) {
									var checkedname=muster[c].childNodes[0];
									if(checkedname.checked == true) {
										var x = checkedname.value;
										var newcheckbox = {
											id: x,
											name: result[c]
										};
											checkboxArray.push(newcheckbox); //从新整合数组
									}
								}
							    v =checkboxArray;
							}
					   }	
 						if(item.value==undefined){
 							if(name!=undefined){
								v = item.innerText.replace(/:/g, "");
							}
						}else{
							if(containSpecial.test(v)){
							    v=v.replace(/\"/g, "&quot;").replace(/\'/g, "&apos;").replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gtl;");
							}else{
							    v = item.value;
							}
						}
 						if(keyvalue != null){
							group[keyvalue] = v;
 						}
 					})
			 		newarr.push(group); //从新整合数组
			 	};
			 	topicArray.myMap(function(data){
			 		for(var i = 0; i < sort.length; i++) {
			 			var title=sort[i].getAttribute("title");
			 			if(data.FormTitle==title){
			 				var topicmete=sys.getElementsByClassName(sort[i],"sys-edit-content");
			 				var list=null,selectArray=[],newarr = [],topic=null,qcon=null,jsonlist="";
			 				topicmete.myMap(function(topicitem,t){
			 					t++;
			 					topic=topicitem;
			 					var condition=sys.getElementsByClassName(topicitem,"sys-edit-condition");
			 					var region=sys.getElementsByClassName(topicitem,"sys-region");
			 					region.myMap(function(regionitem){
			 						var newgroup={};
			 						var fabric = regionitem.getAttribute("sys-mode");
			 						if(fabric=="title"){
			 						    list=sys.conttext(conttext,regionitem);
			 						    traverse(list,newarr,newgroup,topicitem);    
				 					}else if(fabric=="select"){
				 						var an=0;
				 						condition.myMap(function(item,i){
				 							var answer=item.getAttribute("sys-answer"); 
				 							var selectgroup={};
				 							i++;
					 						var list=sys.conttext(conttext,item);
					 						var z=parseInt(String(t)+i);
					 						traverse(list,selectArray,selectgroup,item,z);
						 					if(answer=="true"){
						 						am++;an++;mun=am;
						 						
					 						}
						 					if(an==0){
					 						    am=0; mun=0;
					 						}
					 					});	
				 					}else if(fabric=="content"){
				 						mun++;am++;
				 						var connewgroup={},newextend=[];
				 						list=sys.conttext(conttext,regionitem);
				 						traverse(list,newarr,newgroup);
						 				for(var v = 0; v < newarr.length; v++) {
					 						var item=newarr[v];
											var rows = topic;
											var keyvalue = rows.getAttribute("sys-key");
											var	result = keyvalue.split(",");
											for(var n = 0; n < result.length; n++) {
												var key=result[n];
												var value = item[key];
												if(value!=undefined){
													connewgroup[key] = '';
													if(containSpecial.test(value)){
														   value=value.replace(/\"/g, "&quot;").replace(/\'/g, "&apos;").replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gtl;");
														     connewgroup[key] += value;
														}else{
														    connewgroup[key] += value;
														}
											    }
										    }	
										}
						 				qcon=connewgroup;
				 					}
			 					});
			 				});
			 				if(am==0){
				 				sys.PromptBox("请至少给"+title+"选择一个答案",2);
				 				return false;
				 			}else{
				 				if(qcon==null){
				 					jsonlist= newarr.concat(selectArray); //从新整合数组
				 					topicdata=sys.treedata(jsonlist, 0, topic);
				 				}else{
				 					var arrextend=[]
				 					jsonlist=arrextend.concat(qcon); //从新整合数组
				 					topicdata=jsonlist;
				 				}
				 			    data.FormTopic=topicdata;
			 			   }
			 			}
			 		}
			 	});
                //添加组卷名称方法操作
                mun!=0?sys.combination(sys,par,topicArray):'';
			}		  			 
		}else{
			sys.PromptBox("抱歉,请先添加相关模块,编辑信息在提交!", 2, false);
		}
	},
	//添加组卷名称方法操作
	combination:function(sys,par,topicArray){
		var html="",Testarry=[];
		var title="TestPaperName";
		var div = document.createElement("div");
		sys.addClass(div,"sys-module sort-combination");
		sys.Attributes( div,"sys-id","combination");
		var ul=document.createElement("ul");
		sys.addClass(ul,"sys-addinfo add_style padding15");
		ul.id="combination";
		html+="<li class='topic-info content_look sys-region clearfix' sys-mode='title'>"
		html+="<label class='label_name'>组卷名称：</label>"
		html+="<span class='text_input info_data clearfix'>"
		html+="<input sys-name='组卷名称' sys-verify='verify' data-type='title' data-reveal='value' data-key="+title+" type='text' class='sys-Required text_input col-sm-9 col-xs-9'>"
		html+="</span></li>"	 
		html+="<li class='content_look sys-region clearfix'>"
		html+="<label class='label_name'>默认展示：</label>"
		html+="<span class='sys-Required info_data'>"		        			
		html+="<label class='SelectionBox'><input name='radio' checked='checked' type='radio' value='0' class='ace'><span class='lbl Selectstyle0' data-radio='0'>是</span></label>"
		html+="<label class='SelectionBox'><input name='radio' type='radio' value='1' class='ace'><span class='lbl Selectstyle1' data-radio='1'>否</span></label>"
		html+="</span></li>"
		ul.innerHTML=html;
		div.appendChild(ul);
		new Mysysbox({
			ieventmask: false, //是否允许点击遮罩层关闭窗体
			mask: true, //是否允许显示遮罩层
			lock: true, //是否显示默认按钮事件
			btn: ["确认","取消"], //按钮名称
			winbtn: [false, false, true],
			title: "组卷命名", //栏目标题
			mode: 0, //输入的内容状态(0为文本输入，1为url地址界面传入)
			tm: 1, //提示状态设置（0为提示框，1为文本内容信息）
			width:450, //窗口宽度
			height:250, //窗口高度
			content:div.innerHTML,
			confirm: function(mysysbox) {
				var required=sys.$("#combination");
				var condition=sys.getElementsByClassName(required,"sys-Required");
				var checkedValue="",inputValue="";
				condition.myMap(function(item){
				    var muster = sys.getElementsByClassName(item, 'SelectionBox');
				    if(muster.length!=0){
				    	for(var c = 0; c < muster.length; c++) {
							var checkedname = sys.name("radio")[c];
							if(checkedname.checked == true) {
							    checkedValue = sys.name("radio")[c].value;
							}
						}
				    }else{
				    	inputValue=item.value;
				    }
				})
				if(inputValue!=""){
					var datetime=sys.transferDate(new Date());
					var TestPaper={
						TestPaperName:inputValue,
						TestPapermode:checkedValue,  
						TestPaperTime:datetime,
						TestPaperList:topicArray
					}
					Testarry.push(TestPaper);
					par.SubmitEvent(sys,par,Testarry);
					mysysbox.hide(); //删除当前窗体
				}else{
					
					sys.PromptBox("组卷名称不能为空",2);
				}
			}, //确认处理事件（内置按钮时）
			close:function(mysysbox) {
				 mysysbox.hide(); //删除当前窗体
			}//关闭处理事件（内置按钮
		});	
	},
	//删除全部模块从新编辑方法
	resetMethod:function(par,lay,MouseEvent){
		var sys=this;
		var DragSort=par.DragSort;//获取栏目
		var content=sys.$("#DragContent");
		content.innerHTML="";
		par.DragSort=[];
	},
	/**************时间转换格式方法*************/
	transferDate:function(date) {
		// 年
		var year = date.getFullYear();
		// 月
		var month = date.getMonth() + 1;
		// 日
		var day = date.getDate();
		if (month >= 1 && month <= 9) {
			month = "0" + month;
		}
		if (day >= 0 && day <= 9) {
			day = "0" + day;
		}
		var dateString = year + '' + month + '' + day;
		return dateString;
	},
	//内容编辑处理方法
	DrageditMethod:function(sys, par,eve,operate,id,title,ABC,Event){
		var cont=null,ul=null,status=null,mu=0;
		var Dragedit=sys.$(par.DrageditMethod);
		var sessionname=par.SessionName;
		var parent=Event.parentNode;
		var menu = JSON.parse(window.sessionStorage.getItem(sessionname));
		var BrowserInfo = {
			IsIE: /*@cc_on!@*/ false,
			IsIE9Under: /*@cc_on!@*/ false && (parseInt(s.match(/msie (\d+)/)[1], 10) <= 9),
		};
		var answerlist=function(li,s,items,ui){
			var clonedNode = li[s].cloneNode(true); // 克隆节点
            items.appendChild(clonedNode); //添加克隆的节点
            var condition=sys.getElementsByClassName(items,"sys-edit-condition");
            condition.myMap(function(cond,i){
            	var region= sys.getElementsByClassName(cond,'sys-region');
            	sys.Attributes(cond,"sys-amount",ui);
            	sys.Attributes(cond,"sys-answer",false);
            	for(var n = 0; n < region.length; n++) {
			        var name=region[n].getAttribute("sys-name");
			         if(name=="label"){
			 	       region[n].innerText=ABC[i]+":";
			        }
			        if(name=="button"){
			          	sys.Attributes(region[n],"sys-amount",ui);
			          	sys.Attributes(region[n],"sys-seat",i);
			          	
			          	if(i>=0 && i<s+1){
			          		var ope=region[n].getAttribute("sys-operate");
				          	if(ope=="add-select" || ope=="delete-select"){
				          		sys.removeElement(region[n])
				          	}
			          	}			            
			        }
			    }
            })
		};
		var addhtml=function(status,html,list,sessionname){	
			if(operate=="add-select"){
 				var region= sys.getElementsByClassName(Event,'sys-region');
 				region.myMap(function(items){
	 				var plus=items.getAttribute("sys-plus");
	 				var bout=items.getAttribute("sys-bout");
	 				var amount=Event.getAttribute("sys-amount");
	 				if(plus=="input"){	
	 					var li=sys.getElementsByClassName(items,"sys-edit-condition");
	 					var sl=li.length-1;
					 	answerlist(li,sl,items,amount);
					}
 				})			
			}else{
				var box = document.createElement("div");
				var mh=0;
				sys.Attributes(box,"sys-tagName","delete");
				Event.appendChild(box).innerHTML=html;
				sys.closetag(Event,"div","delete");
				ul=sys.getElementsByClassName(Event,"sys-edit-content");
				ul.myMap(function(u,ui){
					mu++;
					ui++;
					var amount=u.getAttribute("sys-amount");
					if(amount==null){
						sys.Attributes(u,"sys-amount",mu);
						var region= sys.getElementsByClassName(u,'sys-region');
						for(var n = 0; n < region.length; n++) {
							 var name=region[n].getAttribute("sys-name");
							 var plus=region[n].getAttribute("sys-plus");
							 if(name=="title"){
							 	  region[n].innerText=title+mu+":";
							 }
							 if(plus=="input"){
							 	  var bout=region[n].getAttribute("sys-bout");
							 	  for(var b = 0; b < bout-1; b++) {
							 	  	   var li=sys.getElementsByClassName(region[n],"sys-edit-condition");
									   answerlist(li,b,region[n],mu);
							 	  }
							 }
						}
					}
					mh=u.offsetHeight*ui;
			   });
			   parent.style.minHeight =mh+'px';
		   }		
		}
		if(menu!=null){
			menu.myMap(function(item,i){
				if(item.title==title || item.type==id ) {	
					addhtml(operate,item.html)
				}
			})
		}else{
			var sessionname=Dragedit.getAttribute("sys-session");
			var list=sys.getElementsByClassName(Dragedit,"sys-type-module");
			addhtml(status,html,list,sessionname);
		}
        
	},
	//编辑模板信息框的方法
	FormdataMethod:function(sys,par,event){
		var list=sys.conttext(list,event);
		var value = '';
		list.myMap(function(item,i){
			var stint = item.getAttribute("sys-stint");
			var keyname = item.getAttribute("sys-method");
			var ArrayString = item.getAttribute("sys-Array");
			var checkboxid = item.getAttribute("sys-checkboxid");
			if(keyname=="SelectionBox"){
				var mode = item.getAttribute("sys-mode");
				var muster = sys.getByClass(item, 'SelectionBox');
				if(muster.length == 0) {
					var uid = '',
					    initialid=0,
						result = ArrayString.split(",");
					checkboxid != null ? uid = checkboxid.split(",") : '';
					var initial=item.getAttribute("data-initial");//初始值,用于单选复选框中
					initial!=null?initialid=parseInt(initial):'';
					var newarr = []; //声明一个数组
					for(var n = 0; n < result.length; n++) {
						if(checkboxid != null) {
							id = uid[n];
						} else {
							id = n+initialid;
						}
						var newgroup = {
							id: id,
							name: result[n]
						};
						newarr.push(newgroup); //从新整合数组 
					};
					for(var r = 0; r < newarr.length; r++) {
						item.innerHTML += "<label class='SelectionBox'><input name='radio" + i + "' type='" + mode + "' value='" + newarr[r].id + "' class='ace'><span class='lbl Selectstyle"+r+"' data-radio='" + r + "'>" + newarr[r].name + "</span></label>";
					}
					var musterlist=function(values){						
				    	muster = sys.getByClass(item, 'SelectionBox');
						var un="";
						for(var r = 0; r < muster.length; r++) {
							var d = muster[r].childNodes[0];//当前方法
							var v = muster[r].childNodes[0].value;  //当前的value
							if(v.indexOf(",") != -1){
								un=values.lastIndexOf(v);
							}else{
								un=v;
							}
							var va=values.indexOf(un);
							if(va!=-1){
								d.parentNode.innerHTML = "<input name='radio" + i + "' type='" + mode + "' checked='checked' value='" + newarr[r].id + "' class='ace'><span class='lbl Selectstyle"+r+"' data-radio='"+r+"'>" + newarr[r].name + "</span>";
								var cp=par.CkPrompt;
								if(cp==true){
									var verify = item[1].getAttribute('data-verify');
									if(verify=="verify"){
										var prompts = document.createAttribute("data-name");
										prompts.nodeValue = newarr[r].name;
										item[1].setAttributeNode(prompts);
									}else{
									}
								}else{	
								}
							}else{
								d.parentNode.innerHTML = "<input name='radio" + i + "' type='" + mode + "' value='" + newarr[r].id + "' class='ace'><span class='lbl Selectstyle"+r+"' data-radio='"+r+"'>" + newarr[r].name + "</span>";
							};
						};
					};
					if(!value) {
					    values = item.getAttribute("data-value");
					    musterlist(values);

					} else {
						var isArray= Array.isArray(value);
						var z='';
						if(isArray==true){
							for(var n = 0; n < value.length; n++) {
							    var values =value[n].id;
							    if(value.length-1==n){
							    	z+=values;
							    }else{
							    	z+=values+',';
							    } 
						    }
							musterlist(z);
						}else{
							values = value;
							musterlist(values);
						}
					}
				}	
			}
		})
	},
	//制定模块
	Methodsys:function(sys,Method,par){
		var newarr=[];
		if(Method!=null){
			for(var n = 0; n < Method.length; n++) {
		   	    var newgroup = {
					id: n,
					module:Method[n].module,
					title:Method[n].title,
					event: Method[n].event
				};
				newarr.push(newgroup); //从新整合数组 
			}
			sys.Operatemethod(sys,par,newarr);
		}
	},
	Operatemethod:function(sys,par,newarr){
		for(var n= 0; n < newarr.length; n++) {
			var name=newarr[n].module;
			var obj=sys.$(name);
			var prompts = document.createAttribute("sys-module");
			prompts.nodeValue =newarr[n].title;
			obj.setAttributeNode(prompts);
			newarr[n].event(sys,par,obj);//编辑方法
		}
	},
	//记录保存当前打开的页面(存储到本地json格式数据)
	sessionArr: function(content, title, type, sessionArray,sessionname) {
		var menu = JSON.parse(window.sessionStorage.getItem(sessionname));		
		var json=function(content,title,type){
			var group = {
				html: content,
				title: title,
				type: type
			}; //编辑数组
			sessionArray.push(group); //添加数组
			var jsonStr = JSON.stringify(sessionArray); //json数组转换为JSON字符串
			window.sessionStorage.setItem(sessionname, jsonStr); //声明存储到名称为session的json中
		}
		if(menu != null) {
			sessionArray=menu;
			var Array = sessionArray.some(function(ele, index, array){
			    if(ele.title == title) {
			        return true;
			    }else {
			        return false;
			    }
		    });
		    if(!Array){
		    	json(content,title,type);
		    }
		} else {
			json(content,title,type);
		}
	},
	//声明ajax方法,用于判断浏览器是否支持ajax
	ajaxObject: function(sys) {
		var xmlHttp;
		try {
			// Firefox, Opera 8.0+, Safari
			xmlHttp = new XMLHttpRequest();
		} catch(e) {
			// Internet Explorer
			try {
				xmlHttp = new ActiveXObject("Msxml2.XMLHTTP");
			} catch(e) {
				try {
					xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
				} catch(e) {
					sys.PromptBox('您的浏览器不支持AJAX', 2, false);
					return false;
				}
			}
		}
		return xmlHttp;
	},
	ajaxGet: function(url,reply,prompt,event) {
		//url地址,reply异步或同步,prompt提示信息,event方法
		var sys = this;
		var ajax = sys.ajaxObject(sys);
		ajax.open("GET", url, reply);
		if(ajax) {
			sys.PromptBox(prompt, 0, true);
		}
		ajax.onreadystatechange = function(eve) {
			if(ajax.readyState == 4) {
				if(ajax.status == 200) {
					var json = ajax.responseText; //获取到json字符串，还需解析
					var jsonStr = JSON.parse(json); //将字符串转换为json数组
					event(jsonStr);
				} else {
					sys.PromptBox("HTTP请求错误！错误码：" + ajax.status, 2);
				}
				sys.PromptBox(null, 0, true);
			}
		};
		ajax.send();
	},
	ajaxPost:function(url,reply,prompt,header,event){
		//url地址,reply异步或同步,header请求头,prompt提示信息,event方法
		var sys = this;
		var ajax = sys.ajaxObject();
		ajax.open("post", url, reply);			
		ajax.sysRequestHeader("Content-Type",header);
		if(ajax) {
			sys.PromptBox(prompt, 0, true);
		}
		ajax.onreadystatechange = function(eve) {
			if(ajax.readyState == 4) {
				if(ajax.status == 200) {
					var json = ajax.responseText; //获取到json字符串，还需解析
					var jsonStr = JSON.parse(json); //将字符串转换为json数组
					event(jsonStr);
				}else{
					sys.PromptBox("HTTP请求错误！错误码：" + ajax.status, 2);
			   }
				sys.PromptBox(null, 0, true);
			}
		}
		ajax.send();
	},
	//设置一个提示框，编辑提示框，texts为提示文本 ，time为显示时间秒单位
	PromptBox: function(texts, time, status) {
		var sys = this;
		var b = document.body.querySelector(".box_Bullet");
		if(!b) {
			var box = document.createElement("div");
			document.body.appendChild(box).className = "box_Bullet";
			var boxcss = document.querySelector(".box_Bullet");
			var winWidth = window.innerWidth;
			document.body.appendChild(box).innerHTML = texts;
			var wblank = winWidth - boxcss.offsetWidth;
			box.style.cssText = "width:" + boxcss.offsetWidth + "px" + "; left:" + (wblank / 2) + "px" + ";" +"margin-top:" + (-boxcss.offsetHeight / 2) + "px";
			var int = setInterval(function() {
				time--;
				sys.endclearInterval(time, box, int);
			}, 1000);

		} else if(status == true) {
			document.body.removeChild(b);
			return;
		}
	},
	endclearInterval: function(time, box, int) {
		time > 0 ? time-- : clearInterval(int);
		if(time == 0) {
			clearInterval(int);
			document.body.removeChild(box);
			return;
		}
	},
		//文本框字数限制设置
	Wordcount: function(sys, conttext, e) {
		var size="",prompt="";
		for(var i = 0; i < conttext.length; i++) {
			var stint = conttext[i].getAttribute('data-stint');
			if(stint == "Wordcount") {
				var obj = conttext[i];
				var S = obj.value.length;
				var span = document.createElement("span");
				var sl = conttext[i].getAttribute('size');
				if(sl) {
					size = parseInt(conttext[i].getAttribute('size'));
				} else {
					size = 20;
				}
				var classname = sys.getByClass(obj.parentNode, 'word_count')[0];
				if(classname) {
					if(S == 0) {
						classname.innerHTML = "剩余字数 :<em class='number'>" + size + "</em>字符";
					}
					prompt = classname.getElementsByTagName('em')[0];
				} else {
					obj.parentNode.appendChild(span).className = "word_count";
					span.innerHTML = "剩余字数 :<em class='number'>" + size + "</em>字符";
					prompt = span.getElementsByTagName('em')[0];
				}
				obj.onkeyup = function(event) {
					var eve=sys.eve(event);
					prompt = eve.parentNode.getElementsByTagName('em')[0];
					size = parseInt(eve.getAttribute('size'));
					sys.Wordonkeyup(eve, size, sys, prompt);
				}
				obj.onblur = function(event) {
					var expression = /^[A-Za-z0-9]+$/;
					if(expression.test(obj.value)) {
						var Hints = obj.getAttribute('data-name');
						var textname = "文本内容不能只包括数字字母。";
						sys.newprompt(textname, Hints, sys, obj);
						//sys.PromptBox("文本内容不能只包括数字字母", 2);
					}
				};
				!e ? sys.Wordonkeyup(obj, size, sys, prompt) : '';
			}
		}
	},
	Wordonkeyup: function(obj, size, sys, prompt) {
		if(obj.value.length > size) {
			sys.PromptBox("您输入的字数超过限制", 2);
			obj.value = obj.value.substring(0, size);
			prompt.innerHTML = 0;
			return false;
		} else {
			var curr = size - obj.value.length; //减去 当前输入的	
			prompt.innerHTML = curr.toString();
			return true;
		}
	},
	//滚动方法
	scrollbar:function(sys,wrapDiv,contentDiv,sty){
		if(sty=="onMouseMove"){

		}else{
			var sliderWrap = document.createElement("div");
		sys.addClass(sliderWrap,'sliderWrap' );
		sliderWrap.id="sliderWrap";
		wrapDiv.appendChild(sliderWrap);
		var slider = document.createElement("div");
		sys.addClass(slider,'slider' );
		slider.id="slider";
		sliderWrap.appendChild(slider);
		//设置比例
		//clientHeight - 不包括border
		var scale = wrapDiv.clientHeight / contentDiv.clientHeight;
		//设置滑块的高度
		var h1 = sliderWrap.clientHeight * scale;
		//为了合理设置高度，设置滑块的最小高度
		if (h1 < 50) { 
			h1 = 50;
		}else if (scale >= 1) {
			//说明当前内容能过完全显示在可视区域内，不需要滚动条
			sliderWrap.style.display = "none";
		}else{
			sliderWrap.style.display = "none";
		}
		//设置滑块的高度
		slider.style.height = h1 +"px";
		//设置y轴的增量
		var y = 0;
		//为wrap添加滚轮事件
		wrapDiv.onmousewheel = function(e){
			var event1 = event || e;
			var conh=contentDiv.clientHeight;
			var wraph=wrapDiv.clientHeight;
			var scale = wraph / conh;
		    //设置滑块的高度
		   var h1 = sliderWrap.clientHeight * scale;
		   //设置滑块的高度
		   slider.style.height = h1 +"px";
			if(conh>wraph){
				sliderWrap.style.display = "block";
			}else{
				sliderWrap.style.display = "none";
			}
			if (event.wheelDelta < 0) {
				//滑动条向下滚动
				y += 10;
			}else if (event.wheelDelta > 0) {
				//滑动条向上滚动
				y -= 10;
			}
			//y变化时说明在滚动，此时使滚动条发生滚动，以及设置content内容部分滚动
			//判断极端情况，滑块不能划出屏幕
			if (y <= 0) {
				//滑块最多滑到顶部
				y = 0;
			}
			if(y >= sliderWrap.clientHeight - slider.clientHeight){
				//滑块最多滑到最底部
				y = sliderWrap.clientHeight - slider.clientHeight;
			}
			//更新滑块的位置
			slider.style.top = y +"px";
			scale = wrapDiv.clientHeight / contentDiv.clientHeight;
			contentDiv.style.top = - y / scale +"px";
		}
			
			
		}
		
	},
	//拖拽整体样式方法编辑
	DragMagnitude:function(sys,par,w,h,obj){
		if (window.getComputedStyle) {
	        var computedStyle = getComputedStyle( document.body, null)
	    } else {
	       var computedStyle = p.currentStyle;//兼容IE的写法
	    }
		var Ylattice=par.Ylattice;
		var Xlattice=par.Xlattice;
		var mh="",mw="",mx="",my="";
		if(w==null|| h==null){
			 mh=sys.Height()-parseInt(computedStyle.paddingTop)*2 ;
			 mw=sys.Width()-parseInt(computedStyle.paddingLeft)*2;
		}else{
			mh=h;
			mw=w;
		}
		var div=sys.$(par.DragContent);
		var btn=sys.$(par.DragOperation);
		var bh=btn.offsetHeight;
		var section=sys.$(".sys-section");
		sys.addClass(div,"sys-Editarea");
		obj.style.width=mw+'px';
		obj.style.height=mh+'px';
		obj.style.position="relative";
		obj.style.overflow="hidden";
		obj.style.zIndex="0";
		var div1=document.createElement("div");
		sys.addClass(div1,"sys-scrollbar");
		div1.id="DragContent";
		div1.style.width=(mw-section.offsetWidth-2)+"px";
		div1.style.position="relative";
		div1.style.zIndex="1";
		div.appendChild(div1);
		div.style.position="relative";
		div.style.zIndex="1";
		div.style.overflow="hidden";
		div.style.height=(mh-2-bh)+"px";
		div.style.width=(mw-section.offsetWidth-2)+"px";
		sys.scrollbar(sys,div,div1);//滚动
	},
	//模块类方法编辑
	Moduleclass:function(sys,par,obj,title,id,Level,i){
		var Dragclass=par.Dragclass;
		var DragSort=par.DragSort;
		var Slide=par.DragSlide;
		var MH=par.DragModuleHeight;
		MH==null?MH=150:MH=MH;
		var content=sys.$("#DragContent");
		var html=function(title,i){
			var group = {
				title: title,
			}; //编辑数组
			DragSort.push(group); //添加数组
			var div = document.createElement("div");
			sys.addClass(div,"sys-module sort-module");
			sys.Attributes( div,"sys-id",id);
			sys.Attributes( div,"sys-Level",Level);
			sys.Attributes( div,"title",title);
			div.style.minHeight=MH+'px';
			var div1 = document.createElement("div");
			sys.addClass(div1,"title-name,sort-title");
			sys.Attributes(div1,"sys-operate","drag");
			sys.Attributes(div1,"sys-slide",Slide);
			sys.Attributes(div1,"sys-sort",i);
			div1.innerText=title;
			var button1=document.createElement("botton");
			var button2=document.createElement("botton");
			var button3=document.createElement("botton");
			sys.addClass(button1,"btn,sys-operate btn-blue sys-type-btn");
			sys.Attributes( button1,"sys-operate","delete");
			button1.innerText="删除";
			sys.addClass(button2,"btn,sys-operate btn-danger sys-type-btn");
			sys.Attributes( button2,"sys-operate","add");
			button2.innerText="添加";
			sys.addClass(button3,"btn,sys-operate btn-change sys-type-btn iconfont");
			sys.Attributes( button3,"sys-operate","stretch");
			button3.innerHTML="&#xe61e;";
			//内容区域
			var con = document.createElement("div");
			sys.addClass(con,"sys-module sort-content");
			sys.Attributes(con,"sys-content","content");
			content.appendChild(div);
			div.appendChild(div1);
			div.appendChild(con);
			div1.appendChild(button1);	
			div1.appendChild(button2);	
			div1.appendChild(button3);
			var btn=sys.getElementsByClassName(div1,"sys-operate");
			btn.myMap(function(item,i){
				sys.Attributes( item,"sys-id",id);
			    sys.Attributes( item,"sys-Level",Level);
			    sys.Attributes( item,"title",title);  
			})
		}
		if(Dragclass!=null){
			for(var i=0;i<Dragclass.length;i++){
				var title=Dragclass[i];
				var Array = DragSort.some(function(ele, index, array){
				    if(ele.title == title) {
				        return true;
				    }else {
				        return false;
				    }
			    });
			    if(!Array){
			    	html(title,i);
			    }else{
			    	sys.PromptBox(title+"已存在,请从新选择", 2);
			    }	
			}
		}else{
			i++;
			var sort=sys.getElementsByClassName(content,"sort-module");
			if(DragSort.length==0){
				html(title,i);
			}else{
				var Array = DragSort.some(function(ele, index, array){
				    if(ele.title == title) {
				        return true;
				    }else {
				        return false;
				    }
			    });
			    if(!Array){
			    	html(title,i);
			    }else{
			    	sys.PromptBox(title+"已存在,请从新选择", 2);
			    }				
			}	
		}
	},
	getStyle: function(element, attr) {
		if(element.currentStyle) {
			return element.currentStyle[attr];
		} else {
			return getComputedStyle(element, false)[attr];
		}
	},
	getX:function(obj){
		var parObj=obj;  
		var left=obj.offsetLeft;  
	 	while(parObj=parObj.offsetParent){  
	  		left+=parObj.offsetLeft;  
		}  
 		return left;  
	},  
	getY:function(obj){  
		var parObj=obj;  
		var top=obj.offsetTop;  
		while(parObj = parObj.offsetParent){  
	 		top+=parObj.offsetTop;  
	 	}  
	 return top;  
	},
	slidemodule: function(e,sys,region,par) {
		var obj = sys.eve(e);
		var nm = obj.attributes.length;
		var sortlist=sys.getElementsByClassName(region,"sort-module");
		var slide=null;
		var reg = /\d+/g;
		var disX = 0,
		    t=0,
			disY = 0,
			copy=null,
			draggable=false,
			target=false,
			active=true,
			clone=false,
			p = obj.parentNode,
			f = obj.parentNode.parentNode;
			r= sys.$("#Editarea");
		if (window.getComputedStyle) {
	        var computedStyle = getComputedStyle(p, null)
	    } else {
	       var computedStyle = p.currentStyle;//兼容IE的写法
	    }
		if(sortlist.length==1){
			sys.PromptBox("请至少添加两个以上才能拖拽", 2);
		}else{
			for(var i = 0; i < nm; i++){
			    var name=obj.attributes[i];	 
			    if(name.name=="sys-slide"){
		 	       slide=name.value;
		        }
			}
			if(slide == "true") {		
				obj.style.cursor = "move";
				var ml = sys.getStyle(p,'marginLeft');
				var mr = sys.getStyle(p,'marginRight');
				var ch=region.offsetHeight;
				disX = e.clientX - p.offsetLeft;
				disY = e.clientY - p.offsetTop;
				var title=p.getAttribute("title");
				var pw=p.offsetWidth/2+par.lattice_x;
				var ph=p.offsetHeight/2+par.lattice_y;
				var sl=p.offsetLeft;
				var st=p.offsetTop;	
				var top=Math.abs(sys.getY(region));  
				var left=Math.abs(sys.getX(region)) ;  
				var shtml=p.outerHTML;
				var div = document.createElement("div");
		        var span = document.createElement("span");
		        sys.addClass(div,"sys-sort-content");
		        sys.addClass(span,"sys-sort-margin,sys-dragregion");
		        p.style.visibility="hidden";
		        div.style.cssText ="left:"+sl + "px;position:absolute;z-index:5;top:"+st+"px;width:"+p.offsetWidth + "px";
		        div.innerHTML=shtml;
		        f.appendChild(div);
		        f.appendChild(span);
		        span.style.left = sl + "px";
		        span.style.width = p.offsetWidth + "px"; 
				span.style.top = st + "px";
				span.style.position = "absolute";
			    span.style.zIndex = "1";
			    copy=div;
				var cutoverhtml=function(method,iT,leastmum){ 
					var id=method.getAttribute("sys-id");
                    var Level=method.getAttribute("sys-Level");
                    var title=method.getAttribute("title");
                    var pid=p.getAttribute("sys-id");
                    var pLevel=p.getAttribute("sys-Level");
                    var ptitle=p.getAttribute("title");
                    var pshtml=method.innerHTML;
                    var phtml=p.innerHTML;
                    sys.Attributes( p,"sys-id",id);
					sys.Attributes( p,"sys-Level",Level);
					sys.Attributes( p,"title",title);
                    sys.Attributes( method,"sys-id",pid);
					sys.Attributes( method,"sys-Level",pLevel);
					sys.Attributes( method,"title",ptitle);					
                    if(iT>leastmum){
                    	p.style.visibility ="";
                    	p.innerHTML=pshtml;
                    }
                    method.innerHTML=phtml; 
                    var pe=sys.getElementsByClassName(p,'sort-title')[0];
                    var psort=pe.getAttribute("sys-sort");
                    var pse=sys.getElementsByClassName(method,'sort-title')[0];
                    var pssort=pse.getAttribute("sys-sort");
                    sys.Attributes(pse,"sys-sort",psort);
                    sys.Attributes(pe,"sys-sort",pssort);
                    pse.style.cursor = "";
                    if(sys.hasClass(f,"sys-dragregion")){
                    	sys.removeElement(sys.$(".sys-dragregion"));
                    }
				}
				var rea=function(iT){
					var mT=parseInt(computedStyle.marginTop);
					var mst=p.offsetHeight+mT;
					var ave=mst/2;
					var mi=st-ave;
					sortlist.myMap(function(item,i){
						var leastmum=parseInt(ave*i);
						if(leastmum!=0){
							leastmum=parseInt(mst*i);
						}
						var maximum=parseInt(mst*i+mst);
						if(iT>leastmum && iT<maximum){
							if(item!=p){
								cutoverhtml(item,iT,leastmum);
								return false
							}										
					    }
					 })
				}
				document.onmousemove = function(e) {	
					var iL = e.clientX - disX-left;
					var iT = e.clientY - disY-top+parseInt(computedStyle.marginTop);
					var maxL = region.clientWidth - obj.offsetWidth+(parseInt(computedStyle.marginLeft)*2) ;
					var maxT = region.clientHeight - obj.offsetHeight+(parseInt(computedStyle.marginTop)*2);
					iL <= 0 && (iL = 0);
					iT <= 0 && (iT = 0);
				    iL >= maxL && (iL = maxL);
					iT >= maxT && (iT = maxT);
					if(iT!=0){
						copy.style.width = p.offsetWidth+'px';
						copy.style.left = iL + "px";
						copy.style.top = iT + "px";
						copy.style.position = "absolute";
					    copy.style.zIndex = "10";
					}
					rea(iT);
					return false
				};
				document.onmouseup = function(e) {
					var amount=0,i=0,html="";
					var event=sys.eve(e);
					var list=sys.getElementsByClassName(region,'sort-module');
					var layf=event.parentNode.parentNode;
					var pl=p.offsetLeft;
					var pt=p.offsetTop;
					var l=event.parentNode.parentNode.offsetLeft;
					var t=event.parentNode.parentNode.offsetTop;
					var rangel=l-pl;//
					var ranget=t-pt;//差值
					p.style.position = "relative";
				    p.style.zIndex = "";
				    p.style.left ="";
					p.style.top ="";
					p.style.position ="";
					p.style.visibility ="";
					obj.style.cursor = "";
					sortlist.myMap(function(item,i){
						item.style.visibility ="";
					})
					sys.removeElement(sys.$(".sys-dragregion"));
					sys.removeElement(sys.$(".sys-sort-content"));				   
					document.onmousemove = null;
					document.onmouseup = null;
					this.releaseCapture && this.releaseCapture();
					return false
				}; 
				this.setCapture && this.setCapture();
				return false
			}
		}
	},
	//界面方法调用
	htmltemplate:function(sys,par,json,nameedit,template,structure){
		var pid = 0, total=null; //默认值
		var number = json.length;//数量
		if(number != null && number!=0) {
			var s = navigator.userAgent.toLowerCase();
			if(template=='html'){
				if(structure=='menu'){
				    sys.treedata(json, pid, nameedit);
		            var datalist = sys.treedata(json, pid, nameedit);
					sys.ModuleHtml(sys,par,datalist,nameedit);
					var column=sys.getElementsByClassName(nameedit,"sys-column");
					for(var i=0;i<column.length;i++){
						column[i].removeAttribute('sys-key');
						column[i].removeAttribute('sys-title-value');
						column[i].removeAttribute('sys-url-value');
						column[i].removeAttribute('sys-window-value');
						column[i].removeAttribute('sys-size-value');
						column[i].removeAttribute('sys-direction-value');
					}	
				}else if(structure=='tree'){
					
					
				}else if(structure=='cycle'){
					sys.ModuleHtml(sys,par,json,nameedit);
					var cycle=sys.getElementsByClassName(nameedit,"sys-html");
					for(var i=0;i<cycle.length;i++){
						cycle[i].removeAttribute('sys-key');
						cycle[i].removeAttribute('sys-hex');
						cycle[i].removeAttribute('sys-hex-value');
						cycle[i].removeAttribute('sys-hex-background');
						cycle[i].removeAttribute('sys-title-value');
						cycle[i].removeAttribute('sys-url-value');
					}
				}	
			}else if(template=='div'){
				if(structure=='menu'){
					
				}else if(structure=='tree'){
					
				}else{
					
				}
			}else{
				
			}
		}
	},
	//模块页面处理
	ModuleHtml:function(sys,par,oldArr,nameedit){
		var size=oldArr.length;
		var mouIconClose=sys.par.mouIconClose; //箭头
		var mouIconOpen=sys.par.mouIconOpen;// 箭头
		var contenthtml=function(list,item){
			for(var l=0;l<list.length;l++){
				var m=list[l].parentNode.parentNode;
				if(sys.hasClass(m,'sys-childmenu')){
				   sys.removeClass(list[l].parentNode,'sys-level');
				}
			  	if(sys.hasClass(list[l],'arrow')){
	    	 		sys.removeElement(list[l],'arrow');
	    	 	}
				var keysys=list[l].getAttribute("sys-key");
				var type=list[l].getAttribute("sys-type");
				var level=list[l].getAttribute("sys-level-value");
				var Child=list[l].getAttribute("sys-child-value");
				var title=list[l].getAttribute("sys-title-value");
				var url=list[l].getAttribute("sys-url-value");
				var window=list[l].getAttribute('sys-window-value');
				var size=list[l].getAttribute('sys-size-value');
				var direction=list[l].getAttribute('sys-direction-value');
				var hex=list[l].getAttribute('sys-hex');
				var hexname=list[l].getAttribute('sys-hex-value');
				var background=list[l].getAttribute('sys-hex-background');
				if(keysys!=null){
					var	name = keysys.split(",");
					for(var k=0;k<name.length;k++){
						var key=name[k];
						var value = item[key];
						if(type=="text"){
							list[l].innerHTML=value;
						}else if(type=="label"){
							var s = document.createAttribute("sys-"+name[k]);
							s.nodeValue =value;
							list[l].sysAttributeNode(s);
						}else if(type=="icon"){
							list[l].innerHTML=value;
						}else if(type=="img"){
							var img = document.createElement("img");
							sys.addClass(img,'icon-img');
							img.src=value;
							list[l].appendChild(img);
						}else{
							var s = document.createAttribute("sys-"+name[k]);
							s.nodeValue =value;
							list[l].setAttributeNode(s);
						}
						if(name[k]==title){
							var t = document.createAttribute('title');
							t.nodeValue =value;
							list[l].sysAttributeNode(t);
							list[l].removeAttribute('sys-'+name[k]+"-value");
						}else if(name[k]==url){
							var u = document.createAttribute('sys-href');
							u.nodeValue =value;
							list[l].sysAttributeNode(u);
							list[l].removeAttribute('sys-'+name[k]+"-value");
						}else if(name[k]==window){
							var w = document.createAttribute('sys-window');
							w.nodeValue =value;
							list[l].sysAttributeNode(w);
							list[l].removeAttribute('sys-'+name[k]+"-value");
						}else if(name[k]==size){
							var s = document.createAttribute('sys-windowsize');
							s.nodeValue =value;
							list[l].sysAttributeNode(s);
							list[l].removeAttribute('sys-'+name[k]+"-value");
						}else if(name[k]==direction){
							var d= document.createAttribute('sys-direction');
							d.nodeValue =value;
							list[l].sysAttributeNode(d);
							list[l].removeAttribute('sys-'+name[k]+"-value");
						}else if(name[k]==level){
							var d= document.createAttribute('sys-level');
							d.nodeValue =value;
							list[l].sysAttributeNode(d);
							list[l].removeAttribute('sys-'+name[k]+"-value")
						}else if(name[k]==hexname){
							var d= document.createAttribute('sys-title');
							d.nodeValue =value;
							list[l].sysAttributeNode(d);
							list[l].removeAttribute('sys-'+name[k]+"-value");
						}else if(name[k]==background){
							list[l].style.backgroundColor=value;
							list[l].removeAttribute('sys-'+name[k]+"-value");
						}
					}
					list[l].removeAttribute('sys-key');
					list[l].parentNode.removeAttribute('sys-key');
			    }
			}
		};
		oldArr.myMap(function(item,i){
			var rows = nameedit.children[i];
			var clonedNode = rows.cloneNode(true); // 克隆节点
			nameedit.appendChild(clonedNode); //添加克隆的节点
			if(item.pid && item.pid.length > 0){
			    var list= sys.getElementsByClassName(rows,'sys-region');
			    var arrow = document.createElement("i");
			    sys.addClass(arrow,'arrow iconfont sys-region' );
			    list[0].appendChild(arrow);
			    arrow.innerHTML=mouIconOpen;
			    var ischek = document.createAttribute("ischek");
			    ischek.nodeValue =false;
			    arrow.sysAttributeNode(ischek);
			    var level = document.createAttribute("sys-level");
			    level.nodeValue ="Parent";
			    rows.sysAttributeNode(level);
			    sys.addClass(rows,'sys-level');
				contenthtml(list,item);
				var lay = document.createElement("ul");
		    	sys.addClass(lay,'sys-childmenu clearfix' );
		    	lay.style.display = "none";
		    	lay.style.overflow = "hidden";
		    	var child = nameedit.children[i+1];
		    	var clonedNode = child.cloneNode(true); // 克隆节点
		    	lay.appendChild(clonedNode); //添加克隆的节点
			    rows.appendChild(lay);
				sys.contenthtml(item.pid,lay);
		    }else{
		    	i++;
		    	var list= sys.getElementsByClassName(rows,'sys-region');
	    	 	contenthtml(list,item);
	    	 	if(i==size){
	    	 		sys.removeElement(nameedit.children[size]);
	    	 	}
		    }
      })
	},
	//树状图数据结构重组方法
	treedata: function(oldArr, pid, nameedit) {
		var _this = this;
		var newArr = [];
		var size = oldArr.length;
		oldArr.myMap(function(item) {
			if(item.pid == pid) {
				var newgroup = {};
				for(var i = 0; i < size; i++) {
					var rows = nameedit;
					var keyvalue = rows.getAttribute("sys-key");
					var	result = keyvalue.split(",");
					for(var n = 0; n < result.length; n++) {
						var key=result[n];
						var value = item[key];
						if(value!=undefined){
							newgroup[key] = '';
						    newgroup[key] += value;
					   }
					}	
				}
				var child = _this.treedata(oldArr, item.id, nameedit);
				if(child.length > 0) {
					newgroup.pid = child;
				}
				newArr.push(newgroup);
			}
		});
		return newArr;
	}
};
(function(w){
	/** 
	 * map遍历数组 
	 * @param fn [function] 回调函数； 
	 * @param context [object] 上下文； 
	 */
	Array.prototype.myMap = function(fn, context) {
		context = context || window;
		var ary = [];
		if(Array.prototype.map) {
			ary = this.map(fn, context);
		} else {
			for(var i = 0; i < this.length; i++) {
				ary[i] = fn.apply(context, [this[i], i, this]);
			}
		}
		return ary;
	};
	if(!Array.prototype.some) {
	    Array.prototype.some = function(callback) {
		    // 获取数组长度
		    var len = this.length;
		    if(typeof callback != "function") {
		        throw new TypeError();
		    }
		    // thisArg为callback 函数的执行上下文环境
		    var thisArg = arguments[1];
		    for(var i = 0; i < len; i++) {
		        if(i in this && callback.call(thisArg, this[i], i, this)) {
		            return true;
		        }
		    }
		    return false;
	    }
	}
	var $A = function (a) {
	//转换数组
	    return a ? Array.apply(null, a) : new Array;
	};
	Function.prototype.bind = function () {
	//绑定事件
		var wc = this, a = $A(arguments), o = a.shift();
		return function () {
		   wc.apply(o, a.concat($A(arguments)));
		};
	};
})(window);
