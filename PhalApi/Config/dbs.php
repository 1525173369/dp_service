<?php
/**
 * 分库分表的自定义数据库路由配置
 */
function env($name,$defaut=''){
   $value = getenv($name,$defaut);
   if(!$value){
       return $defaut;
   }
   return $value;
}
return array(
    /**
     * DB数据库服务器集群
     */
    'servers' => array(
        'db_appapi_old' => array(                         //服务器标记
//            'host'      => '127.0.0.1',             //数据库域名
            'host'      => '192.168.0.41',             //数据库域名
            'name'      => 'askp_dp_app',               //数据库名字
            'user'      => 'dpapp_usr2813',                  //数据库用户名
//            'password'  => 'uX83,as01Epc42.-Az',	                    //数据库密码
            'password'  => 'uX83,as01Epc42-Az',                          //数据库密码
            'port'      => '3306',                  //数据库端口
            'charset'   => 'utf8mb4',                  //数据库字符集
        ),
        'db_appapi' => array(                         //服务器标记
//            'host'      => '127.0.0.1',             //数据库域名
            'host'      => env('DB_HOST'),             //数据库域名
            'name'      => env('DB_DATABASE'),              //数据库名字
            'user'      => env('DB_USERNAME'),              //数据库用户名
//            'password'  => 'uX83,as01Epc42.-Az',	                    //数据库密码
            'password'  => env('DB_PASSWORD'),                           //数据库密码
            'port'      => env('DB_PORT'),         //数据库端口
            'charset'   => env('DB_CHARSET'),             //数据库字符集
        ),

    ),

    /**
     * 自定义路由表
     */
    'tables' => array(
        //通用路由
        '__default__' => array(
            'prefix' => 'cmf_',
            'key' => 'id',
            'map' => array(
                array('db' => 'db_appapi'),
            ),
        ),

        /**
        'demo' => array(                                                //表名
            'prefix' => 'pa_',                                         //表名前缀
            'key' => 'id',                                              //表主键名
            'map' => array(                                             //表路由配置
                array('db' => 'db_appapi'),                               //单表配置：array('db' => 服务器标记)
                array('start' => 0, 'end' => 2, 'db' => 'db_appapi'),     //分表配置：array('start' => 开始下标, 'end' => 结束下标, 'db' => 服务器标记)
            ),
        ),
         */
    ),
);
