<?php
/**
 * 请在下面放置任何您需要的应用配置
 */

return array(

    /**
     * 应用接口层的统一参数
     */
    'apiCommonRules' => array(
        //'sign' => array('name' => 'sign', 'require' => true),
    ),
    'REDIS_HOST' => env('REDIS_HOST'),//"127.0.0.1",
//    'REDIS_HOST' => "redis-19cf5af7-616a-4841-b912-3bc5e1f3ccbb.dcs.huaweicloud.com",
    'REDIS_AUTH' => env('REDIS_AUTH'),//"akyChN7DmB7mNtLB",
    'REDIS_PORT' => env('REDIS_PORT'),
    
    'sign_key' => '76576076c1f5f657b634e966c8836a06',
		
	'uptype'=>1,//上传方式：1表示 七牛，2表示 本地
		
	/**
     * 七牛相关配置
     */
    // 'Qiniu' =>  array(
    //     //ak
    //     'accessKey' => 's4Y1QruR70msxJTK8LKUAxm2MJFHk7-q8cbvAV4q',
    //     //sk
    //     'secretKey' => 'PFfkz-rTvt8gZzp_NLTyzjTelI7BDEQYrmK2TMYR',
    //     //存储空间
    //     'space_bucket' => 'zwrk',
    //     //cdn加速域名 格式：http(s)://a.com
    //     'space_host' => 'http://qiniu.zwrjkf.cn',  
    //     //区域上传域名(服务端)  参考文档：https://developer.qiniu.com/kodo/manual/1671/region-endpoint
    //     'uphost' => 'http://up.qiniup.com', 
    // ),

    'Qiniu_old' =>  array(
        //ak
        'accessKey' => '-Gvse4W7T0DQ0PNptCArWnsdOgYqJAB4Tme6EZbo',
        //sk
        'secretKey' => 'cDaixo2dAJMrhrFWGGPIMZl16OiOoJ4SFhqToWT8',
        //存储空间
        'space_bucket' => 'askpert',
        //cdn加速域名 格式：http(s)://a.com
        'space_host' => 'https://sn.askpert.com',  
        //区域上传域名(服务端)  参考文档：https://developer.qiniu.com/kodo/manual/1671/region-endpoint
        'uphost' => 'https://up-as0.qiniup.com', 
    ),
    'Qiniu' =>  array(
        //ak
        'accessKey' => env('ACCESSKEY'),
        //sk
        'secretKey' => env('SECRETKEY'),
        //存储空间
        'space_bucket' => env('SPACEBUCKET'),
        //cdn加速域名 格式：http(s)://a.com
        'space_host' => env('SPACEHOST'),
        //区域上传域名(服务端)  参考文档：https://developer.qiniu.com/kodo/manual/1671/region-endpoint
        'uphost' => env('UPHOST'),
    ),
		
		 /**
     * 本地上传
     */
    'UCloudEngine' => 'local',

    /**
     * 本地存储相关配置（UCloudEngine为local时的配置）
     */
    'UCloud' => array(
        //对应的文件路径  站点域名
        'host' => env('HOSTUPLOAD')
    ),
		
		/**
     * 云上传引擎,支持local,oss,upyun
     */
    //'UCloudEngine' => 'oss',

    /**
     * 云上传对应引擎相关配置
     * 如果UCloudEngine不为local,则需要按以下配置
     */
   /*  'UCloud' => array(
        //上传的API地址,不带http://,以下api为阿里云OSS杭州节点
        'api' => 'oss-cn-hangzhou.aliyuncs.com',

        //统一的key
        'accessKey' => '',
        'secretKey' => '',

        //自定义配置的空间
        'bucket' => '',
        'host' => 'http://image.xxx.com', //必带http:// 末尾不带/

        'timeout' => 90
    ), */
		

		
);
