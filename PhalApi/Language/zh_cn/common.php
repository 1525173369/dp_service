<?php
/**
 * 翻译说明：
 *
 * 1、带大括号的保留原来写法，如：{name}，会被系统动态替换
 * 2、没在以下出现的，可以自行追加
 *
 * @author dogstar <chanzonghuang@gmail.com> 2015-02-09
 */

return array(
	'Hi {name}, welcome to use PhalApi!' => '{name}您好，欢迎使用PhalApi！',
    'user not exists' => '用户不存在',
    'personal information'=>'个人信息',
    'Contributing businesses'=>'贡献商家',
    'Business claim'=>'商家认领',
    'eal name authentication'=>'实名认证',
    'erchant authentication'=>'商家认证',
    'nalysis report'=>'分析报告',
    'questionnaire'=>'调查问卷',
    'My draft box'=>'我的草稿箱',
    'Contact customer service'=>'联系客服',
);
