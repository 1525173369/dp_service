<?php

return array(
	'Hello {name}, Welcome to use PhalApi!' => 'Hello {name}, Welcome to use PhalApi!',
    'user not exists' => 'user not exists',
    'personal information'=>'personal information',
    'Contributing businesses'=>'Contributing businesses',
    'Business claim'=>'Business claim',
    'eal name authentication'=>'eal name authentication',
    'erchant authentication'=>'erchant authentication',
    'nalysis report'=>'nalysis report',
    'questionnaire'=>'questionnaire',
    'My draft box'=>'My draft box',
    'Contact customer service'=>'Contact customer service',
);
