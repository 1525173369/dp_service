<?php

class Domain_Message {
	public function getList($uid,$p) {
		$rs = array();

		$model = new Model_Message();
		$rs = $model->getList($uid,$p);

		return $rs;
	}

	public function getShopOrderList($uid,$p){
		$rs = array();

		$model = new Model_Message();
		$rs = $model->getShopOrderList($uid,$p);

		return $rs;
	}
	
    public function information($uid,$p) {
        $rs = array();

        $model = new Model_Message();
        $rs = $model->information($uid,$p);

        return $rs;
    }
    
    public function getCollect($uid,$p){
        $rs = array();

        $model = new Model_Remind();
        $rs = $model->getList($uid,$p);

        return $rs;

    }
    public function getComment($uid,$p){
        $rs = array();

        $model = new Model_Remind();
        $rs = $model->getCommentList($uid,$p);

        return $rs;
    }
    public function getLikes($uid,$p){
        $rs = array();

        $model = new Model_Remind();
        $rs = $model->getLikes($uid,$p);

        return $rs;
    }
    
    public function getSubscribe($uid,$p){
        $rs = array();

        $model = new Model_Remind();
        $rs = $model->getSubscribe($uid,$p);

        return $rs;
    }
    
    public function getNew($uid){
        $rs = array();
        $model = new Model_Message();
        $rs = $model->getNew($uid);

        return $rs;
    }
	
}
