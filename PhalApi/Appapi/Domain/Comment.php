<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/3 0003
 * Time: 15:49
 */
class Domain_Comment{

    public function addComment($data,$lan){

        $rs = array();

        $model = new Model_Comment();
        $rs = $model->addComment($data,$lan);

        return $rs;


    }
    
    public function getShopComment($id,$uid,$p){
        $rs = array();

        $model = new Model_Comment();
        $rs = $model->getShopComment($id,$uid,$p);
        return $rs;

    }
    
    public function addNoteComment($data,$lan,$at,$touid){

        $rs = array();

        $model = new Model_Comment();
        $rs = $model->addNoteComment($data,$lan,$at,$touid);
        return $rs;
    }
    
    public function getNoteComment($id,$uid,$p){
        $rs = array();

        $model = new Model_NoteComment();
        $rs = $model->getShopComment($id,$uid,$p);
        return $rs;
    }
    
    public function addLike($uid,$tid,$status,$lan){
        $rs = array();
        $model = new Model_Comment();
        $rs = $model->addLike($uid,$tid,$status,$lan);

        return $rs;
    }
    
    public function getMyComment($uid,$p){
        $rs = array();
        $model = new Model_Comment();
        $rs = $model->getMyComment($uid,$p);

        return $rs;
    }
    
    public function delComment($uid,$id,$type){
        $rs = array();
        $model = new Model_Comment();
        $rs = $model->delComment($uid,$id,$type);

        return $rs;
    }

}