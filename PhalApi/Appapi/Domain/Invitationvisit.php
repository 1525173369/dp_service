<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/8 0008
 * Time: 16:22
 */
class Domain_Invitationvisit{

    public function addVisitLog($uid,$note){
        
        $rs = array();
        $model = new Model_InvitationVisit();
        $rs = $model->addVisitLog($uid,$note);
        return $rs;

    }
    
    public function getHistory($uid,$p){
        $rs = array();
        $model = new Model_InvitationVisit();
        $rs = $model->getHistory($uid,$p);
        return $rs;
        
    }

}