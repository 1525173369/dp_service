<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 18:04
 */
class Domain_Order {

    protected $dataArray; //商品数据
    protected $submitArray;//提交过来的数据
    //下单
    public function place($uid,$data){
        $rs = array();
        $this->submitArray = $data;
        //检查库存
        $this->dataArray = $this->checkStock($data);
        if(isset($this->dataArray['status'])  && $this->dataArray['status']==1004){
            return $this->dataArray;
        }
        //检查是否够钱付款
        $r =  $this->checkMoney($uid);
        if($r['status']==1003 || $r['status'] == 1002){
            return $r;
        }


        $id = $data['id'];
        $number = $data['number'];
        //生成订单快照
        $data =  $this->sanpOrder();
        //创建订单
        $rs= $this->createOrder($uid,$data);
        if($rs['status'] == 1){
            DI()->notorm->shop_goods->where('id=?',$id)
                ->update( array('store_count' => new NotORM_Literal("store_count - {$number}") ) );
        }
        return $rs;
    }

    //创建订单号
    public static function makeOrderNo(){
        $yCode = array('A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J');
        $orderSn =
            $yCode[intval(date('Y')) - 2020] .
            strtoupper(dechex(date('m'))) .
            date('d') .
            substr(time(), -5) .
            substr(microtime(), 2, 5) .
            sprintf('%02d', rand(0, 99));
        return $orderSn;
    }
    private function checkMoney($uid){
        $model = new Model_User();
        $res = $model->getUserOrderInfo($uid);
        #var_dump((int)$this->dataArray['gold'],$res['coin'],(int)$this->dataArray['gold']>(int)$res['coin']);die;
        if((int)$this->dataArray['gold']>(int)$res['coin']){
            return array('status'=>1003,'msg'=>'金币不足','result'=>array());
        }
        if($res['user_status'] != 1){
            return [
                'status' => 1002,
                'msg' => '认证后才能提现',
                'result' => []
            ];
        }
        return array('status'=>1,'msg'=>'','result'=>array());
    }

    private function checkStock($data){
        $model = new Model_Goods();
        $res = $model->getDetails($data['id']);
        if($res['store_count']<$data['number']){
            return array('status'=>1004,'msg'=>'库存不足','result'=>array());
        }
        return $res;
    }

    private function sanpOrder(){
        $data = [];
        $data['goods_price']=$this->dataArray['gold'];//商品总价
        $data['total_amount']=$this->dataArray['gold'];
        $model = new Model_Address();
        $rs = $model->getInfo($this->submitArray['address_id']);
        $data['consignee']=$rs['consignee'];
        $data['country']=$rs['country'];
        $data['province']=$rs['province'];
        $data['city']=$rs['city'];
        $data['district']=$rs['district'];
        $data['address']=$rs['address'];
        $data['mobile']=$rs['mobile'];
        return $data;

    }
    private function createOrder($uid,$data){
        $data['order_sn']=$this->makeOrderNo();
        $data['user_id']=$uid;
        $data['order_status']=1;
        $data['pay_status']=1;
        $data['pay_code']='goldPay';
        $data['pay_name']='金币兑换';
        $data['add_time']=time();
        $data['pay_time']=time();
        $model = new Model_Order();
        $order_id = $model->addOrder($data);
        $orderGoods=[];
        $orderGoods['order_id']=$order_id;
        $orderGoods['goods_id']=$this->dataArray['id'];
        $orderGoods['goods_name']=$this->dataArray['goods_name'];
        $orderGoods['goods_num']=1;
        $orderGoods['goods_price']=$data['goods_price'];
        $model->addOrderGoods($orderGoods);//添加子订单
        $money = $model->reduceGold($uid,$orderGoods['goods_price']);//减用户余额
        $model->addSaleNum($orderGoods['goods_id'],$orderGoods['goods_num']);//增加销量
        $model->addLog([
                'uid'=>$uid,
                'type'=>0,
                'title'=>'兑换'.$orderGoods['goods_name'],
                'createtime'=>time(),
                'money'=>$orderGoods['goods_price']
            
            ]);
        return array('status'=>1,'msg'=>'成功','result'=>array(
            'order_id'=>$order_id,
            'order_sn'=>$data['order_sn'],
            'pay_time'=>$data['pay_time'],
            'money'=>$money,
        ));
    }

}