<?php

class Domain_Quest{
    
    public function getQuestExamList() {
        $rs = array();

        $model = new Model_Quest();
        $rs = $model->getQuestExamList();

        return $rs;
    }
    
    public function getQuestExam($exam_id,$p) {
        $rs = array();

        $model = new Model_Quest();
        $rs = $model->getQuestExam($exam_id,$p);

        return $rs;
    }
    
    public function userQuestExam($uid, $exam_id, $exam) {
        $rs = array();

        $model = new Model_Quest();
        $rs = $model->userQuestExam($uid, $exam_id, $exam);

        return $rs;
    }
    
    public function addAnswer($uid,$data){
        $rs = array();

        $model = new Model_Quest();
        $rs = $model->addAnswer($uid,$data);

        return $rs;
        
    }
    
}