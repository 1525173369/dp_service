<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/30 0030
 * Time: 18:04
 */
class Domain_Publish{
    public function setDynamic($data) {
        $rs = array();

        $model = new Model_Publish();
        $rs = $model->setDynamic($data);

        return $rs;
    }
    
    public function getListByShop($shop_id,$p){
        
        $rs = [];
        $model = new Model_Publish();
        
        $rs = $model->getListByShop($shop_id,$p);

        return $rs;
        
    }
    
    public function getStatusShopList($keywords,$p,$lat,$lng){
        $rs = [];
        $model = new Model_Publish();
        
        $rs = $model->getShopLists($keywords,$p,$lat,$lng);

        return $rs;
    }
    
    /**
     * 获取推荐的帖子列表
     */
    public function getRecommend($p, $uid, $type, $lat, $lng, $classid, $keyword,$city){
        $rs = [];
        $model = new Model_Publish();
        $rs = $model->getRecommend($p, $uid, $type, $lat, $lng, $classid, $keyword,$city);
        return $rs;
    }
    
    public function getNote($id,$uid){
        $rs = [];
        $model = new Model_Publish();
        $rs = $model->getNote($id,$uid);
        return $rs;
    }
    
    public function getShop($id){
        $rs = [];
        $model = new Model_Publish();
        $rs = $model->getShop($id);
        return $rs;

    }
    
    public function addNoteLike($data,$lan){
        $rs = [];
        $model = new Model_Publish();
        $rs = $model->addNoteLike($data,$lan);
        return $rs;
        
    }
    
    public function getDraftList($uid,$p){
        $rs = [];
        $model = new Model_Publish();
        $rs = $model->getDraftList($uid,$p);
        return $rs;
    }
    
    public function draftNote($uid, $id){
        $rs = [];
        $model = new Model_Publish();
        $rs = $model->draftNote($uid, $id);
        return $rs;
    }
    
    public function draftDel($uid, $id){
        $rs = [];
        $model = new Model_Publish();
        $rs = $model->draftDel($uid, $id);
        return $rs;
        
    }
    
    public function getMyNote($uid,$touid,$p){
        $rs = [];
        $model = new Model_Publish();
        $rs = $model->getMyNote($uid,$touid,$p);
        return $rs;
        
    }
    
    public function addTopic($custom){
        $rs = [];
        $model = new Model_ShopTopic();
        $rs = $model->addTopic($custom);
        return $rs;
    }
    
    public function censusLike($uid,$start,$end){
        $rs = array();
        $daysNum = date('t',$start);
        $rs['title']=date("Y-m",$start);
        $rs['xAxis'] =[6,12,18,24,(int)$daysNum];//ECharts X轴数据
       foreach ($rs['xAxis'] as &$val){
            $val = date("m",$start)."-".$val;
            
        }
        $model = new Model_Publish();
        //6
        $rs['series'][] = $model->censusLike($uid,$start,$start+86400*6);
        //12
        $rs['series'][] = $model->censusLike($uid,$start+86400*6,$start+86400*12);
        //18
        $rs['series'][] = $model->censusLike($uid,$start+86400*12,$start+86400*24);
        //24
        $rs['series'][] = $model->censusLike($uid,$start+86400*18,$start+86400*24);
        //31
        $rs['series'][] = $model->censusLike($uid,$start+86400*24,$end);
        return $rs;
    }
    public function getWeekLike($uid,$time){
         $rs = array();
         $rs['title']=date("Y-m-d",$time);
         $rs['xAxis'] =[1,2,3,4,5,6,7];//ECharts X轴数据
        foreach($rs['xAxis'] as &$val){
             $newTime = $time+($val-1)*86400;
             $val=date("m-d",$newTime);
         }
         $model = new Model_Publish();
        $rs['series'][] = $model->censusLike($uid,$time,$time+86400);
        //12
        $rs['series'][] = $model->censusLike($uid,$time+86400,$time+86400*2);
        //18
        $rs['series'][] = $model->censusLike($uid,$time+86400*2,$time+86400*3);
        //24
        $rs['series'][] = $model->censusLike($uid,$time+86400*3,$time+86400*4);
        //31
        $rs['series'][] = $model->censusLike($uid,$time+86400*4,$time+86400*5);
        $rs['series'][] = $model->censusLike($uid,$time+86400*5,$time+86400*6);
        $rs['series'][] = $model->censusLike($uid,$time+86400*6,$time+86400*7);
        return $rs;
    }
    
    public function censusRead($uid,$start,$end){
        $rs = array();
        $daysNum = date('t',$start);
        $rs['title']=date("Y-m",$start);
        $rs['xAxis'] =[6,12,18,24,(int)$daysNum];//ECharts X轴数据
        
          foreach ($rs['xAxis'] as &$val){
            $val = date("m",$start)."-".$val;
            
        }
        $model = new Model_InvitationVisit();
        //6
        $rs['series'][] = $model->censusRead($uid,$start,$start+86400*6);
        //12
        $rs['series'][] = $model->censusRead($uid,$start+86400*6,$start+86400*12);
        //18
        $rs['series'][] = $model->censusRead($uid,$start+86400*12,$start+86400*24);
        //24
        $rs['series'][] = $model->censusRead($uid,$start+86400*18,$start+86400*24);
        //31
        $rs['series'][] = $model->censusRead($uid,$start+86400*24,$end);
        return $rs;
    }
    
    public function getWeekRead($uid,$time){
         $rs = array();
         $rs['title']=date("Y-m-d",$time);
         $rs['xAxis'] =[1,2,3,4,5,6,7];//ECharts X轴数据
       foreach($rs['xAxis'] as &$val){
             $newTime = $time+($val-1)*86400;
             $val=date("m-d",$newTime);
         }
         $model = new Model_InvitationVisit();
        $rs['series'][] = $model->censusRead($uid,$time,$time+86400);
        //12
        $rs['series'][] = $model->censusRead($uid,$time+86400,$time+86400*2);
        //18
        $rs['series'][] = $model->censusRead($uid,$time+86400*2,$time+86400*3);
        //24
        $rs['series'][] = $model->censusRead($uid,$time+86400*3,$time+86400*4);
        //31
        $rs['series'][] = $model->censusRead($uid,$time+86400*4,$time+86400*5);
        $rs['series'][] = $model->censusRead($uid,$time+86400*5,$time+86400*6);
        $rs['series'][] = $model->censusRead($uid,$time+86400*6,$time+86400*7);
        return $rs;
    }
    
    public function screen($uid,$data,$p){
        $rs = [];
        $model = new Model_Publish();
        $rs = $model->screen($uid,$data,$p);
        return $rs;
    }
    public function topicNote($uid,$topic,$lat,$lng,$p){
        $rs = [];
        $model = new Model_Publish();
        $rs = $model->topicNote($uid,$topic,$lat,$lng,$p);
        return $rs;
        
    }
    
    public function hidePost($uid, $post_id, $type){
        $rs = [];
        $model = new Model_Publish();
        $rs = $model->hidePost($uid, $post_id, $type);
        return $rs;
    }
}