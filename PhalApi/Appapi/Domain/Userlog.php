<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/29 0029
 * Time: 14:50
 */
class Domain_Userlog {

    public function getInfo($uid,$status,$p,$lan){
        $rs = array();
        $model = new Model_Userlog();
        $rs = $model->getLists($uid,$status,$p,$lan);
        return $rs;

    }
    
    public function addLog($uid){
        $rs = array();
        $model = new Model_Userlog($uid);
        $rs = $model->addLog($uid);
        return $rs;

    }
    

}