<?php

class Domain_Login {

    public function userLogin($user_login,$code, $device_id = "", $as_id = "", $device = "",$lan = 'en') {
        $rs = array();

        $model = new Model_Login();
        $rs = $model->userLogin($user_login,$code, $device_id, $as_id, $device,$lan);

        return $rs;
    }

    public function userReg($user_login,$code, $device_id = "", $as_id = "", $device = "",$lan = 'en') {
        $rs = array();
        $model = new Model_Login();
        $rs = $model->userReg($user_login,$code, $device_id, $as_id, $device,$lan);

        return $rs;
    }	
	
    public function userFindPass($user_login,$user_pass) {
        $rs = array();
        $model = new Model_Login();
        $rs = $model->userFindPass($user_login,$user_pass);

        return $rs;
    }	

    public function userLoginByThird($openid,$type,$nickname,$avatar,$source, $device_id = "", $as_id = "", $device = "") {
        $rs = array();

        $model = new Model_Login();
        $rs = $model->userLoginByThird($openid,$type,$nickname,$avatar,$source, $device_id, $as_id, $device);

        return $rs;
    }
    
    public function userLoginThird($openid,$nickname,$avatar,$email, $device_id = "", $as_id = "", $device = "") {
        $rs = array();

        $model = new Model_Login();
        $rs = $model->userLoginThird($openid,$nickname,$avatar,$email, $device_id, $as_id, $device);

        return $rs;
    }
    public function userLoginThirdAndroid($openid,$nickname,$avatar,$email, $device_id = "", $as_id = "", $device = "") {
        $rs = array();

        $model = new Model_Login();
        $rs = $model->userLoginThirdAndroid($openid,$nickname,$avatar,$email, $device_id, $as_id, $device);

        return $rs;
    }
    public function userLoginHuaWei($openid,$nickname,$avatar,$email, $device_id = "", $as_id = "", $device = "") {
        $rs = array();

        $model = new Model_Login();
        $rs = $model->userLoginHuaWei($openid,$nickname,$avatar,$email, $device_id, $as_id, $device);

        return $rs;
    }
    
    

    public function upUserPush($uid,$pushid,$terminal) {
        $rs = array();

        $model = new Model_Login();
        $rs = $model->upUserPush($uid,$pushid,$terminal);

        return $rs;
    }			
	
	public function getUserban($user_login) {
        $rs = array();

        $model = new Model_Login();
        $rs = $model->getUserban($user_login);

        return $rs;
    }
	public function getThirdUserban($openid,$type) {
        $rs = array();

        $model = new Model_Login();
        $rs = $model->getThirdUserban($openid,$type);

        return $rs;
    }

    public function getCancelCondition($uid){
        $rs = array();

        $model = new Model_Login();
        $rs = $model->getCancelCondition($uid);

        return $rs;
    }

    public function cancelAccount($uid){
        $rs = array();

        $model = new Model_Login();
        $rs = $model->cancelAccount($uid);

        return $rs;
    }
    
    public function getUserRecom($uid) {
        $rs = array();

        $model = new Model_Login();
        $rs = $model->getUserRecom($uid);

        return $rs;
    }
    
    public function userLogout($uid){
        $rs = array();

        $model = new Model_Login();
        $rs = $model->userLogout($uid);

        return $rs;
        
    }
    
    public function loginAccount($user_login,$user_pass, $device_id = "", $as_id = "", $device = ""){
        $rs = array();

        $model = new Model_Login();
        $rs = $model->loginAccount($user_login,$user_pass, $device_id, $as_id, $device);
        
        return $rs;
    }
    
    public function updatePass($uid,$pass){
        $rs = array();
        
        $model = new Model_Login();
        $rs = $model->updatePass($uid,$pass);
        
        return $rs;
    }
    
    public function updateEmptyAgentCode() {
        $rs = array();
        
        $model = new Model_Login();
        $rs = $model->updateEmptyAgentCode($uid,$pass);
        
        return $rs;
    }
}
