<?php

class Domain_Home {


    public function agreement(){
       $rs = array();
        $model = new Model_Home();
        $rs = $model->agreement();
        return $rs;
    }
    public function agreementTwo(){
        $rs = array();
         $model = new Model_Home();
         $rs = $model->agreementTwo();
         return $rs;
     }
     public function agreementThree(){
        $rs = array();
         $model = new Model_Home();
         $rs = $model->agreementThree();
         return $rs;
     }
    public function about(){
       $rs = array();
        $model = new Model_Home();
        $rs = $model->about();
        return $rs;
    }
    
    public function getSlide() {
        $rs = array();
        $model = new Model_Home();
        $rs = $model->getSlide();
        return $rs;
    }

    public function getReportClassifiers($lan) {
        $rs = array();
        $model = new Model_Home();
        $rs = $model->getReportClassifiers($lan);
        return $rs;
    }
		
	public function getHot($p,$search) {
        $rs = array();

        $model = new Model_Home();
        $rs = $model->getHot($p,$search);
				
        return $rs;
    }
		
	public function getFollow($uid,$p) {
        $rs = array();
				
        $model = new Model_Home();
        $rs = $model->getFollow($uid,$p);
				
        return $rs;
    }
		
	public function getNew($lng,$lat,$p) {
        $rs = array();

        $model = new Model_Home();
        $rs = $model->getNew($lng,$lat,$p);
				
        return $rs;
    }
		
	public function search($uid,$key,$p) {
        $rs = array();

        $model = new Model_Home();
        $rs = $model->search($uid,$key,$p);
				
        return $rs;
    }
	
	public function getNearby($uid,$lng,$lat,$p) {
        $rs = array();

        $model = new Model_Home();
        $rs = $model->getNearby($uid,$lng,$lat,$p);
				
        return $rs;
    }
	
	public function getRecommend($uid) {
        $rs = array();

        $model = new Model_Home();
        $rs = $model->getRecommend($uid);
				
        return $rs;
    }
	
	public function attentRecommend($uid,$touid) {
        $rs = array();

        $model = new Model_Home();
        $rs = $model->attentRecommend($uid,$touid);
				
        return $rs;
    }

    public function profitList($uid,$type,$p){
        $rs = array();

        $model = new Model_Home();
        $rs = $model->profitList($uid,$type,$p);
                
        return $rs;
    }

    public function consumeList($uid,$type,$p){
        $rs = array();

        $model = new Model_Home();
        $rs = $model->consumeList($uid,$type,$p);
                
        return $rs;
    }

    public function getClassLive($liveclassid,$p){
        $rs = array();

        $model = new Model_Home();
        $rs = $model->getClassLive($liveclassid,$p);
                
        return $rs;
    }
    
    public function getHotShop($uid, $p) {
        $rs = array();
        
        $model = new Model_Home();
        $rs = $model->getHotShop($uid, $p);
        
        return $rs;
    }
    public function searchHome($uid,$keyword,$p,$lat,$lng,$catId){
        $rs = array();
        
        $model = new Model_Home();
        $rs = $model->searchHome($uid,$keyword,$p,$lat,$lng,$catId);
        
        return $rs;
        
    }
    
    public function qiniuConfig() {
        $rs = array();
        
        $model = new Model_Home();
        $rs = $model->qiniuConfig();
        
        return $rs;
    }

}
