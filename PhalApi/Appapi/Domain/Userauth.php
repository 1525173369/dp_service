<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/28 0028
 * Time: 10:41
 */
class Domain_Userauth {

    public function getInfo($uid){
        $rs = array();

        $model = new Model_Userauth();
        $rs = $model->getInfo($uid);

        return $rs;
    }

    public function update($uid,$data){
        $rs = array();

        $model = new Model_Userauth();
        $rs = $model->updateInfo($uid,$data);

        return $rs;

    }

    public function add($uid,$data){
        $rs = array();

        $model = new Model_Userauth();
        $rs = $model->addInfo($uid,$data);

        return $rs;


    }
    
    public function isAuth($uid){
        $rs = array();

        $model = new Model_Userauth();
        $rs = $model->isAuth($uid);

        return $rs;
        
    }
    public function shopAuth($uid,$id){
        $rs = array();

        $model = new Model_Userauth();
        $rs = $model->shopAuth($uid,$id);

        return $rs;
        
    }
    

}