<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 15:34
 */
class Domain_Ad{

    public function getList(){
        $rs = array();
        $model = new Model_Ad();
        $rs = $model->getLists();
        return $rs;

    }
    public function noteAd($id){
        $rs = array();
        $model = new Model_Ad();
        $rs = $model->noteAd($id);
        return $rs;

    }
    
    public function shopAd($id){
        $rs = array();
        $model = new Model_Ad();
        $rs = $model->shopAd($id);
        return $rs;

    }
    
}