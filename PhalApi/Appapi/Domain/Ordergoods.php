<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/8 0008
 * Time: 16:22
 */
class Domain_Ordergoods {

    public function getList($uid,$p){

        $rs = array();
        $model = new Model_Order();
        $rs = $model->getLists($uid,$p);
        return $rs;

    }
    
    public function getInfo($id){
        $rs = array();
        $model = new Model_Order();
        $rs = $model->getInfo($id);
        return $rs;

    }
    public function confirm($id){
        $rs = array();
        $model = new Model_Order();
        $rs = $model->confirm($id);
        return $rs;
    }

}