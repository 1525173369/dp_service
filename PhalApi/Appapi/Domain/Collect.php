<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/4 0004
 * Time: 13:48
 */
class Domain_Collect{

    public function collect($data,$lan){

        $rs = array();
        $model = new Model_Collect();
        $rs = $model->collect($data,$lan);
        return $rs;
    }
    
    public function getCollect($id,$status,$p){
        $rs = array();
        $model = new Model_Collect();
        $rs = $model->getCollect($id,$status,$p);
        return $rs;

    }
    
    public function delCollect($id){
        $rs = array();
        $model = new Model_Collect();
        $rs = $model->delCollect($id);
        return $rs;
    }
    public function isCollect($id,$uid,$status){
        $rs = array();
        $model = new Model_Collect();
        $rs = $model->isCollect($id,$uid,$status);
        return $rs;

    }
    
    public function censusLike($uid,$start,$end){
        $rs = array();
        $daysNum = date('t',$start);
        $rs['title']=date("Y-m",$start);
        $rs['xAxis'] =[6,12,18,24,(int)$daysNum];//ECharts X轴数据
        foreach ($rs['xAxis'] as &$val){
            $val = date("m",$start)."-".$val;
            
        }
        $model = new Model_Collect();
        //6
        $rs['series'][] = $model->censusLike($uid,$start,$start+86400*6);
        //12
        $rs['series'][] = $model->censusLike($uid,$start+86400*6,$start+86400*12);
        //18
        $rs['series'][] = $model->censusLike($uid,$start+86400*12,$start+86400*24);
        //24
        $rs['series'][] = $model->censusLike($uid,$start+86400*18,$start+86400*24);
        //31
        $rs['series'][] = $model->censusLike($uid,$start+86400*24,$end);
        return $rs;

    }
    public function getWeekTime($uid,$time){
         $rs = array();
         $rs['title']=date("Y-m-d",$time);
         $rs['xAxis'] =[1,2,3,4,5,6,7];//ECharts X轴数据
          foreach($rs['xAxis'] as &$val){
             $newTime = $time+($val-1)*86400;
             $val=date("m-d",$newTime);
         }
         $model = new Model_Collect();
        $rs['series'][] = $model->censusLike($uid,$time,$time+86400);
        //12
        $rs['series'][] = $model->censusLike($uid,$time+86400,$time+86400*2);
        //18
        $rs['series'][] = $model->censusLike($uid,$time+86400*2,$time+86400*3);
        //24
        $rs['series'][] = $model->censusLike($uid,$time+86400*3,$time+86400*4);
        //31
        $rs['series'][] = $model->censusLike($uid,$time+86400*4,$time+86400*5);
        $rs['series'][] = $model->censusLike($uid,$time+86400*5,$time+86400*6);
        $rs['series'][] = $model->censusLike($uid,$time+86400*6,$time+86400*7);
        return $rs;
    }
    
    public function getWeekFollow($uid,$time){
         $rs = array();
         $rs['title']=date("Y-m-d",$time);
         $rs['xAxis'] =[1,2,3,4,5,6,7];//ECharts X轴数据
        foreach($rs['xAxis'] as &$val){
             $newTime = $time+($val-1)*86400;
             $val=date("m-d",$newTime);
         }
         $model = new Model_Collect();
        $rs['series'][] = $model->censusFollow($uid,$time,$time+86400);
        //12
        $rs['series'][] = $model->censusFollow($uid,$time+86400,$time+86400*2);
        //18
        $rs['series'][] = $model->censusFollow($uid,$time+86400*2,$time+86400*3);
        //24
        $rs['series'][] = $model->censusFollow($uid,$time+86400*3,$time+86400*4);
        //31
        $rs['series'][] = $model->censusFollow($uid,$time+86400*4,$time+86400*5);
        $rs['series'][] = $model->censusFollow($uid,$time+86400*5,$time+86400*6);
        $rs['series'][] = $model->censusFollow($uid,$time+86400*6,$time+86400*7);
        return $rs;
    }
    
    
    
    public function censusFollow($uid,$start,$end){
        
        $rs = array();
        $daysNum = date('t',$start);
        $rs['title']=date("Y-m",$start);
        $rs['xAxis'] =[6,12,18,24,(int)$daysNum];//ECharts X轴数据
         foreach ($rs['xAxis'] as &$val){
            $val = date("m",$start)."-".$val;
            
        }
        
        $model = new Model_Collect();
        //6
        $rs['series'][] = $model->censusFollow($uid,$start,$start+86400*6);
        //12
        $rs['series'][] = $model->censusFollow($uid,$start+86400*6,$start+86400*12);
        //18
        $rs['series'][] = $model->censusFollow($uid,$start+86400*12,$start+86400*24);
        //24
        $rs['series'][] = $model->censusFollow($uid,$start+86400*18,$start+86400*24);
        //31
        $rs['series'][] = $model->censusFollow($uid,$start+86400*24,$end);
        return $rs;

    }
    



}