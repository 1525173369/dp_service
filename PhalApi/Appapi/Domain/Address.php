<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 15:34
 */
class Domain_Address {

    public function getList($id){
        $rs = array();
        $model = new Model_Address();
        $rs = $model->getLists($id);
        return $rs;

    }
    public function updateAddress($data,$address_id){
        $rs = array();
        $model = new Model_Address();
        $rs = $model->updateAddress($data,$address_id);
        return $rs;

    }

    public function addAddress($data,$uid){
        $rs = array();
        $model = new Model_Address();
        $rs = $model->addAddress($data,$uid);
        return $rs;

    }

    public function setDefault($uid,$address_id){
        $rs = array();
        $model = new Model_Address();
        $rs = $model->setDefault($uid,$address_id);
        return $rs;
    }
    
    public function getInfo($id){
        $rs = array();
        $model = new Model_Address();
        $rs = $model->getInfo($id);
        return $rs;
    }
    
    public function del($uid,$address_id){
        $rs = array();
        $model = new Model_Address();
        $rs = $model->del($uid,$address_id);
        return $rs;
    }

}