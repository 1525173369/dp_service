<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 10:48
 */
class Domain_Goods {

    public function getList($p,$keywords){
        $rs = array();
        $model = new Model_Goods();
        $rs = $model->getLists($p,$keywords);
        return $rs;
    }
    
    public function getDetails($id){
        $rs = [];
        $model = new Model_Goods();
        $rs = $model->getDetails($id);
        return $rs;
    }

    public function getBanner($id){
        $rs = [];
        $model = new Model_Goods();
        $rs = $model->getBanner($id);
        return $rs;
    }
    
    public function checkStatus($uid,$id){
        $rs = [];
        $modelGoods = new Model_Goods();
        $result =  $modelGoods->getUserFlashOrderGoods($uid,$id);
        if($result==1){
            return ['status'=>2];
        }
        $res = $modelGoods->getDetails($id);
        $model = new Model_User();
        $r = $model->getUserOrderInfo($uid);
        if($res['gold']<=$r['coin']){
            return ['status'=>1];
        }else{
            return ['status'=>0];
        }


    }

}