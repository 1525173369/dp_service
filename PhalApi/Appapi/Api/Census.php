<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/15 0015
 * Time: 11:38
 */
class Api_Census extends PhalApi_Api {

    public function getRules() {
        return array(
            'getCollect'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'time' => array('name' => 'time', 'type' => 'string','desc' => '格式：2020-12'),
            ),
            'follow'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'time' => array('name' => 'time', 'type' => 'string','desc' => '格式：2020-12'),
            ),
             'getLikeVital'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'time' => array('name' => 'time', 'type' => 'string','desc' => '格式：2020-12'),
            ),
             'getRead'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'time' => array('name' => 'time', 'type' => 'string','desc' => '格式：2020-12'),
            ),
            
            
        );

    }
    
    function getWeekMyActionAndEnd($time = '', $first = 1){
      //当前日期
      if (!$time) $time = time();
      $sdefaultDate = date("Y-m-d", $time);
      //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
      //获取当前周的第几天 周日是 0 周一到周六是 1 - 6
      $w = date('w', strtotime($sdefaultDate));
      //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
      $week_start = date('Y-m-d', strtotime("$sdefaultDate -" . ($w ? $w - $first : 6) . ' days'));
      //本周结束日期
      $week_end = date('Y-m-d', strtotime("$week_start +6 days"));
      //return array("week_start" => $week_start, "week_end" => $week_end);
      return strtotime($week_start);
    }
    
    
    
    //收藏统计
    public function getCollect(){

        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $timeParameter=checkNull($this->time);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        if(!$timeParameter){
            $start_time=mktime(0,0,0,date('m'),1,date('Y'));
            $end_time=mktime(23,59,59,date('m'),date('t'),date('Y'));
        }else{
            $timestamp = strtotime($timeParameter);
            $start_time = strtotime(date( 'Y-m-1 00:00:00', $timestamp ));
            $mDays = date( 't', $timestamp );
            $end_time = strtotime(date( 'Y-m-' . $mDays . ' 23:59:59', $timestamp ));

        }
        $week_start = $this->getWeekMyActionAndEnd(strtotime($timeParameter));
        $domain = new Domain_Collect();
        $info = $domain->censusLike($uid,$start_time,$end_time);
        $infoTwo= $domain->getWeekTime($uid,$week_start);
        $rs['info'][0]['list']=$info;
        $rs['info'][1]['week']=$infoTwo;
        return $rs;
    }
    //关注统计
    public function follow(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $timeParameter=checkNull($this->time);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        if(!$timeParameter){
            $start_time=mktime(0,0,0,date('m'),1,date('Y'));
            $end_time=mktime(23,59,59,date('m'),date('t'),date('Y'));
        }else{
            $timestamp = strtotime($timeParameter);
            $start_time = strtotime(date( 'Y-m-1 00:00:00', $timestamp ));
            $mDays = date( 't', $timestamp );
            $end_time = strtotime(date( 'Y-m-' . $mDays . ' 23:59:59', $timestamp ));

        }
        $week_start = $this->getWeekMyActionAndEnd(strtotime($timeParameter));
        $domain = new Domain_Collect();
        $info = $domain->censusFollow($uid,$start_time,$end_time);
        $infoTwo= $domain->getWeekFollow($uid,$week_start);
        $rs['info'][0]['list']=$info;
        $rs['info'][1]['week']=$infoTwo;
        return $rs;
        
    }
    //点赞统计
    public function  getLikeVital(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $timeParameter=checkNull($this->time);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        if(!$timeParameter){
            $start_time=mktime(0,0,0,date('m'),1,date('Y'));
            $end_time=mktime(23,59,59,date('m'),date('t'),date('Y'));
        }else{
            $timestamp = strtotime($timeParameter);
            $start_time = strtotime(date( 'Y-m-1 00:00:00', $timestamp ));
            $mDays = date( 't', $timestamp );
            $end_time = strtotime(date( 'Y-m-' . $mDays . ' 23:59:59', $timestamp ));

        }
        $week_start = $this->getWeekMyActionAndEnd(strtotime($timeParameter));
        $domain = new Domain_Publish();
        $info = $domain->censusLike($uid,$start_time,$end_time);
        $infoTwo= $domain->getWeekLike($uid,$week_start);
        $rs['info'][0]['list']=$info;
        $rs['info'][1]['week']=$infoTwo;
        return $rs;
        
        
    }
    
    public function getRead(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $timeParameter=checkNull($this->time);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        if(!$timeParameter){
            $start_time=mktime(0,0,0,date('m'),1,date('Y'));
            $end_time=mktime(23,59,59,date('m'),date('t'),date('Y'));
        }else{
            $timestamp = strtotime($timeParameter);
            $start_time = strtotime(date( 'Y-m-1 00:00:00', $timestamp ));
            $mDays = date( 't', $timestamp );
            $end_time = strtotime(date( 'Y-m-' . $mDays . ' 23:59:59', $timestamp ));

        }
        $week_start = $this->getWeekMyActionAndEnd(strtotime($timeParameter));
        $domain = new Domain_Publish();
        $info = $domain->censusRead($uid,$start_time,$end_time);
        $infoTwo= $domain->getWeekRead($uid,$week_start);
        $rs['info'][0]['list']=$info;
        $rs['info'][1]['week']=$infoTwo;
        return $rs;
        
    }
    
    

}