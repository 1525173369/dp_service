<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/15 0015
 * Time: 11:38
 */
class Api_Ad extends PhalApi_Api {

    public function getRules() {
        return array(
            'noteAd'=>array(
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '帖子ID'),

            ),
           'shopAd'=>array(
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '店铺ID'),

            ),
            
        );

     
   

    }
    
 
    public function adIndex(){
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $domain = new Domain_Ad();
        $r = $domain->getList();
        $rs['info'][0]['list'] = $r;
        return $rs;
    }
    
    public function noteAd(){
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $domain = new Domain_Ad();
        $r = $domain->noteAd($this->id);
        $rs['info'][0]['list'] = $r;
        return $rs;
        
        
    }
    public function shopAd(){
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $domain = new Domain_Ad();
        $r = $domain->shopAd($this->id);
      
        $rs['info'][0]['list'] = $r;
        return $rs;
        
        
    }

}