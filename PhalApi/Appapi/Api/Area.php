<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/15 0015
 * Time: 11:38
 */
class Api_Area extends PhalApi_Api {

    public function getRules() {
        return array(
            'getCollect'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'time' => array('name' => 'time', 'type' => 'string','desc' => '格式：2020-12'),
            ),
            
        );

    }
    
 
    public function index(){
        $rs = array('code' => 0, 'msg' => '获取成功', 'info' => array());
        $domain = new Domain_Area();
        $r = $domain->getList();
        if(!$r){
            return $rs;
        }
        
        $rs['info'][0]['list'] = $r;
        return $rs;
    }
    

}