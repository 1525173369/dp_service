<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/30 0030
 * Time: 17:41
 */
header('Access-Control-Allow-Origin: *');

class Api_Publish extends PhalApi_Api
{

    public function getRules()
    {
        return array(
            'setDynamic' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'title' => array('name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '标题'),
                'thumb' => array('name' => 'thumb', 'type' => 'string', 'desc' => '图片地址集合'),
                'video' => array('name' => 'video', 'type' => 'string', 'desc' => '视频链接'),
                'shop_id' => array('name' => 'shop_id', 'type' => 'string', 'desc' => '归属店铺ID'),
                'cat_id' => array('name' => 'cat_id', 'type' => 'string', 'desc' => '所属分类ID'),
                'topic' => array('name' => 'topic', 'type' => 'string', 'desc' => '话题ID，以逗号分隔'),
                'lat' => array('name' => 'lat', 'type' => 'string', 'desc' => '维度'),
                'lng' => array('name' => 'lng', 'type' => 'string', 'desc' => '经度'),
                'city' => array('name' => 'city', 'type' => 'string', 'desc' => '城市'),
                'address' => array('name' => 'address', 'type' => 'string', 'desc' => '详细地理位置'),
                'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '内容'),
                'custom' => array('name' => 'custom', 'type' => 'string', 'desc' => '自定义话题,多个话题以,分隔'),
                'points' => array('name' => 'points', 'type' => 'string', 'desc' => '店铺评分'),
                'area' => array('name' => 'area', 'type' => 'string', 'desc' => 'Area'),
                'state' => array('name' => 'state', 'type' => 'string', 'desc' => '洲'),
                'country' => array('name' => 'country', 'type' => 'string', 'desc' => '国家'),
                'postal_code' => array('name' => 'postal_code', 'type' => 'string', 'desc' => '邮政'),
            ),
            'eidtDynamic' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '帖子ID'),
                'title' => array('name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '标题'),
                'thumb' => array('name' => 'thumb', 'type' => 'string', 'desc' => '图片地址集合'),
                'video' => array('name' => 'video', 'type' => 'string', 'desc' => '视频链接'),
                'shop_id' => array('name' => 'shop_id', 'type' => 'string', 'desc' => '归属店铺ID'),
                'cat_id' => array('name' => 'cat_id', 'type' => 'string', 'desc' => '所属分类ID'),
                'topic' => array('name' => 'topic', 'type' => 'string', 'desc' => '话题ID，以逗号分隔'),
                'lat' => array('name' => 'lat', 'type' => 'string', 'desc' => '维度'),
                'lng' => array('name' => 'lng', 'type' => 'string', 'desc' => '经度'),
                'city' => array('name' => 'city', 'type' => 'string', 'desc' => '城市'),
                'address' => array('name' => 'address', 'type' => 'string', 'desc' => '详细地理位置'),
                'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '内容'),
                'custom' => array('name' => 'custom', 'type' => 'string', 'desc' => '自定义话题,多个话题以,分隔'),
                'points' => array('name' => 'points', 'type' => 'string', 'desc' => '店铺评分'),
                'area' => array('name' => 'area', 'type' => 'string', 'desc' => 'Area'),
                'state' => array('name' => 'state', 'type' => 'string', 'desc' => '洲'),
                'country' => array('name' => 'country', 'type' => 'string', 'desc' => '国家'),
                'postal_code' => array('name' => 'postal_code', 'type' => 'string', 'desc' => '邮政'),
            ),
            
            'getListByShop' => array(
                'shop_id' => array('name' => 'shop_id', 'type' => 'string', 'desc' => '归属店铺ID'),
                'p' => array('name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'),
            ),
            'getShop' => array(
                'keywords' => array('name' => 'keywords', 'type' => 'string', 'desc' => '关键词'),
                'p' => array('name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'),
                'lat' => array('name' => 'lat', 'type' => 'string', 'desc' => '纬度'),
                'lng' => array('name' => 'lng', 'type' => 'string', 'desc' => '经度'),
            ),
            'recommendList' => array(
                'p' => array('name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'),
                'uid' => array('name' => 'uid', 'type' => 'string', 'desc' => '用户ID'),
                'type' => array('name' => 'type', 'type' => 'string', 'desc' => '类型'),
                'lat' => array('name' => 'lat', 'type' => 'string', 'desc' => '纬度', 'require' => false),
                'lng' => array('name' => 'lng', 'type' => 'string', 'desc' => '经度', 'require' => false),
                'classid' => array('name' => 'classid', 'type' => 'string', 'desc' => '分类id', 'require' => false),
                'keyword' => array('name' => 'keyword', 'type' => 'string', 'desc' => '分类id', 'require' => false),
                'city' => array('name' => 'city', 'type' => 'string', 'desc' => '城市', 'require' => false),
            ),
            'detailsNote' => array(
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '帖子id'),
                'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID,用来记录阅读量'),
            ),
            'detailsShop' => array(
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => 'id'),
            ),
            'draft' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id', 'type' => 'int', 'desc' => '修改id'),
                'title' => array('name' => 'title', 'type' => 'string', 'require' => true, 'desc' => '标题'),
                'thumb' => array('name' => 'thumb', 'type' => 'string', 'desc' => '图片地址集合'),
                'shop_id' => array('name' => 'shop_id', 'default' => '0', 'type' => 'int', 'desc' => '归属店铺ID'),
                'cat_id' => array('name' => 'cat_id', 'default' => '0', 'type' => 'int', 'desc' => '所属分类ID'),
                'topic' => array('name' => 'topic', 'type' => 'string', 'desc' => '话题ID，以逗号分隔'),
                'lat' => array('name' => 'lat', 'type' => 'string', 'desc' => '维度'),
                'lng' => array('name' => 'lng', 'type' => 'string', 'desc' => '经度'),
                'city' => array('name' => 'city', 'type' => 'string', 'desc' => '城市'),
                'address' => array('name' => 'address', 'type' => 'string', 'desc' => '详细地理位置'),
                'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '内容'),
                'custom' => array('name' => 'custom', 'type' => 'string', 'desc' => '自定义话题,多个话题以,分隔'),
                'points' => array('name' => 'points', 'type' => 'string', 'desc' => '店铺评分'),
                'area' => array('name' => 'area', 'type' => 'string', 'desc' => 'Area'),
                'state' => array('name' => 'state', 'type' => 'string', 'desc' => 'state'),
                'country' => array('name' => 'country', 'type' => 'string', 'desc' => 'country'),
                'postal_code' => array('name' => 'postal_code', 'type' => 'string', 'desc' => 'postal_code'),
                'video' => array('name' => 'video', 'type' => 'string', 'desc' => '视频链接'),
            ),
            'draftList' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => 'uid'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => 'token'),
                'p' => array('name' => 'p', 'default' => '1', 'type' => 'int', 'desc' => '页数'),
            ),
            'draftNote' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '帖子id'),
            ),
            'draftDel' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '帖子id'),
            ),
            'screen' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => ''),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => ''),
                'reading' => array('name' => 'reading', 'default' => '0', 'type' => 'int', 'desc' => ''),
                'likes' => array('name' => 'likes', 'default' => '0', 'type' => 'int', 'desc' => ''),
                'collect_sum' => array('name' => 'collect_sum', 'default' => '0', 'type' => 'int', 'desc' => ''),
                'points' => array('name' => 'points', 'type' => 'string', 'desc' => ''),
                'topic' => array('name' => 'topic', 'type' => 'string', 'desc' => ''),
                'address' => array('name' => 'address', 'type' => 'string', 'desc' => ''),
                'p' => array('name' => 'p', 'default' => '1', 'type' => 'int', 'desc' => '页数'),

            ),

            'hidePost' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => ''),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => ''),
                'post_id' => array('name' => 'post_id', 'type' => 'int', 'require' => true, 'desc' => ''),
                'type' => array('name' => 'type', 'type' => 'int', 'require' => true, 'desc' => ''),
            ),
            'dyDiname' => array(
//                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
//                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '帖子ID'),
//                'day' => array('name' => 'day', 'type' => 'int', 'default' => '7', 'require' => true, 'desc' => '时间段'),
//                'time' => array('name' => 'time', 'type' => 'int',   'desc' => '时间'),
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'time' => array('name' => 'time', 'type' => 'string', 'desc' => '格式：2020-12'),
            ),
            'dyDinameList' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '帖子ID'),
                'p' => array('name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '帖子ID'),
            ),
        );
    }

    public function dyDinameList()
    {
        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $checkToken = checkToken($uid, $token);
        $id = checkNull($this->id);
        $p = checkNull($this->p);
        $limit = 20;
        $start = ($p - 1) * $limit;
        if ($checkToken == 700 && 0) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $list = DI()->notorm->record_all->where('sid=?', $id)->limit($start, $limit)->fetchAll();
        $count = DI()->notorm->record_all->where('sid=?', $id)->count();
        $res = [];
        foreach ($list as $k => $v) {
            $user = DI()->notorm->user->where('id=?', $v['uid'])->fetchOne();
            if ($user) {
                $res[$k]['avatar'] = get_upload_path($user['avatar']);
                $res[$k]['user_nickname'] = $user['user_nickname'];
                $res[$k]['addtime'] = date('Y-m-d H:i:s', $v['addtime']);
            }
        }
        $rs = array('code' => 0, 'msg' => '获取成功', 'info' => array());
        $info['info'] = $res;
        $info['count'] = $count;
      
       
        $rs['info'] = $info;
        return $rs;
    }

    //查询单个帖子近七天的访问量
    public function dyDiname()
    {
//        $uid = checkNull($this->uid);
//        $token = checkNull($this->token);
//        $checkToken = checkToken($uid, $token);
//        $id = checkNull($this->id);
//        $day = checkNull($this->day);
//        $time = $this->time;
//        if ($checkToken == 700) {
//            $rs['code'] = $checkToken;
//            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
//            return $rs;
//        }
//        $where = '';
//        if($time){
//            $where = " and addtime >= strtotime({$time})";
//        }
//        $list = DI()->notorm->record_all->select('date(FROM_UNIXTIME(addtime,"%m%d")) as ctime,count(sid) as total')
//            ->group("date_format(from_unixtime(addtime),'%Y%m%d')")
//            ->where("DATE_SUB(CURDATE(), INTERVAL {$day} DAY) <= date(from_unixtime(addtime)) and sid = ? {$where}", $id)
//            ->fetchAll();
//        for ($i = $day - 1; 0 <= $i; $i--) {
//            $result[] = date('Y-m-d', strtotime('-' . $i . ' day'));
//            $total[] = 0;
//        }
//        array_walk($list, function ($value, $key) use ($result, &$total) {
//            $index = array_search($value['ctime'], $result);
//            $total[$index] = intval($value['total']);
//        });
//        $data = [
//            'day' => $result,
//            'total' => $total
//        ];
//        $rs = array('code' => 0, 'msg' => '获取成功', 'info' => array());
//        $rs['info'] = $data;
//        return $rs;
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $timeParameter = checkNull($this->time);
        $checkToken = checkToken($uid, $token);
        $id = checkNull($this->id);
//        if ($checkToken == 700) {
//            $rs['code'] = $checkToken;
//            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
//            return $rs;
//        }
        if (!$timeParameter) {
            $start_time = mktime(0, 0, 0, date('m'), 1, date('Y'));
            $end_time = mktime(23, 59, 59, date('m'), date('t'), date('Y'));
        } else {
            $timestamp = strtotime($timeParameter);
            $start_time = strtotime(date('Y-m-1 00:00:00', $timestamp));
            $mDays = date('t', $timestamp);
            $end_time = strtotime(date('Y-m-' . $mDays . ' 23:59:59', $timestamp));

        }
        $week_start = $this->getWeekMyActionAndEnd(strtotime($timeParameter));
        $info = $this->censusLike($uid, $start_time, $end_time,$id);
        $infoTwo = $this->getWeekTime($uid, $week_start,$id);
        $rs['info'][0]['list'] = $info;
        $rs['info'][1]['week'] = $infoTwo;
        return $rs;
    }
    public function getWeekTime($uid,$time,$id){
        $rs = array();
        $rs['title']=date("Y-m-d",$time);
        $rs['xAxis'] =[1,2,3,4,5,6,7];//ECharts X轴数据
        foreach($rs['xAxis'] as &$val){
            $newTime = $time+($val-1)*86400;
            $val=date("m-d",$newTime);
        }
        $rs['series'][] = $this->censusLike1($uid,$time,$time+86400,$id);
        //12
        $rs['series'][] = $this->censusLike1($uid,$time+86400,$time+86400*2,$id);
        //18
        $rs['series'][] = $this->censusLike1($uid,$time+86400*2,$time+86400*3,$id);
        //24
        $rs['series'][] = $this->censusLike1($uid,$time+86400*3,$time+86400*4,$id);
        //31
        $rs['series'][] = $this->censusLike1($uid,$time+86400*4,$time+86400*5,$id);
        $rs['series'][] = $this->censusLike1($uid,$time+86400*5,$time+86400*6,$id);
        $rs['series'][] = $this->censusLike1($uid,$time+86400*6,$time+86400*7,$id);
        return $rs;
    }
    public function censusLike($uid,$start,$end,$id){
        $rs = array();
        $daysNum = date('t',$start);
        $rs['title']=date("Y-m",$start);
        $rs['xAxis'] =[6,12,18,24,(int)$daysNum];//ECharts X轴数据
        foreach ($rs['xAxis'] as &$val){
            $val = date("m",$start)."-".$val;
        }
        //6
        $rs['series'][] = $this->censusLike1($uid,$start,$start+86400*6,$id);
        //12
        $rs['series'][] = $this->censusLike1($uid,$start+86400*6,$start+86400*12,$id);
        //18
        $rs['series'][] = $this->censusLike1($uid,$start+86400*12,$start+86400*24,$id);
        //24
        $rs['series'][] = $this->censusLike1($uid,$start+86400*18,$start+86400*24,$id);
        //31
        $rs['series'][] = $this->censusLike1($uid,$start+86400*24,$end,$id);
        return $rs;

    }

    public function censusLike1($uid,$start,$end,$id){
        $where1 = " sid= {$id} and addtime>=".$start." and addtime<=".$end ;
        $count1 = DI()->notorm->record_all->where($where1)->count();
        $count =  $count1;
        return $count;

    }
    function getWeekMyActionAndEnd($time = '', $first = 1){
        //当前日期
        if (!$time) $time = time();
        $sdefaultDate = date("Y-m-d", $time);
        //$first =1 表示每周星期一为开始日期 0表示每周日为开始日期
        //获取当前周的第几天 周日是 0 周一到周六是 1 - 6
        $w = date('w', strtotime($sdefaultDate));
        //获取本周开始日期，如果$w是0，则表示周日，减去 6 天
        $week_start = date('Y-m-d', strtotime("$sdefaultDate -" . ($w ? $w - $first : 6) . ' days'));
        //本周结束日期
        $week_end = date('Y-m-d', strtotime("$week_start +6 days"));
        //return array("week_start" => $week_start, "week_end" => $week_end);
        return strtotime($week_start);
    }
    public function setDynamic()
    {
        $rs = array('code' => 0, 'msg' => '发布成功', 'info' => array());
        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $title = $this->title;
        $video = $this->video;
        $video_img  = $_REQUEST['video_img'];
        $thumb = checkNull($this->thumb);
        $content = $this->content;
        $cat_id = checkNull($this->cat_id);
        $lat = checkNull($this->lat);
        $lng = checkNull($this->lng);
        $city = checkNull($this->city);
        $address = checkNull($this->address);
        $topic = checkNull($this->topic);
        $custom = checkNull($this->custom);
        $shop_id = checkNull($this->shop_id);
        $state = checkNull($this->state);
        $country = checkNull($this->country);
        $postal_code = checkNull($this->postal_code);
        $area = checkNull($this->area);
        $checkToken = checkToken($uid, $token);
        $points = $this->points;
        /*if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }*/

        $sensitivewords = sensitiveField($title);

        if ($sensitivewords == 1001) {
            $rs['code'] = 10011;
            $rs['msg'] = '动态标题输入非法，请重新输入';
            return $rs;
        }
        $domain = new Domain_Publish();
        $topicRes = '';
        if ($custom) {
            $custom = explode(',', $custom);
            $topicRes = $domain->addTopic($custom);
        }
        //  print_r($topic);exit;
        if ($topic && $custom) {
            $topic = trim($topic, ',');
            $topic = $topic . ',' . $topicRes;
        }

        if (!isset($shop_id) || empty($shop_id)) {
            $shop_id = 0;
        }

        $data = array(
            'uid' => $uid,
            'title' => $title,
            'thumb' => $thumb,
            'content' => $content,
            'cat_id' => $cat_id,
            'topic' => $topic ? $topic : $topicRes,
            'shop_id' => $shop_id,
            'lat' => $lat,
            'lng' => $lng,
            'city' => $city,
            'address' => $address,
            'addtime' => time(),
            "likes" => 0,
            "comments" => 0,
            'custom' => $topicRes,
            'points' => $points,
            'status' => 1,
            'state' => $state,
            'country' => $country,
            'postal_code' => $postal_code,
            'area' => $area,
            'video' => $video,
            'video_img' => $video_img
        );
        
     

        $info = $domain->setDynamic($data);
       
        if ($info == 1004) {
            $rs['code'] = 1004;
            $rs['msg'] = '发布失败，请重试';
            return $rs;
        }
        
        
           

        
        
        $thumbs = explode(',',$video);
        foreach ($thumbs as $k=>$v){
            if($v){
                 DI()->notorm->obs->insert(['url'=>$v,'createtime'=>time(),'updatetime'=>time(),'dynamic_id'=>$info]);
            }
        }
        
        

        
        
        
        if ($info == 1) {
            $rs['msg'] = "发布成功";
        }
        /* $rs['info'][0]['id']=$info['id'];
        $rs['info'][0]['thumb_s']=get_upload_path($thumb_s); */
        $rs['info'][0]['msg'] = '';
        return $rs;
    }

    public function eidtDynamic()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $id = checkNull($this->id);
        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $title = $this->title;
        $thumb = checkNull($this->thumb);
        $video = $this->video;
        $video_img  = $_REQUEST['video_img'];
        $content = $_REQUEST['content'];
        $cat_id = checkNull($this->cat_id);
        $lat = checkNull($this->lat);
        $lng = checkNull($this->lng);
        $city = checkNull($this->city);
        $address = checkNull($this->address);
        $topic = checkNull($this->topic);
        $custom = checkNull($this->custom);
        $shop_id = checkNull($this->shop_id);
        $state = checkNull($this->state);
        $country = checkNull($this->country);
        $postal_code = checkNull($this->postal_code);
        $area = checkNull($this->area);
        $checkToken = checkToken($uid, $token);
        $points = $this->points;
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        $sensitivewords = sensitiveField($title);

        if ($sensitivewords == 1001) {
            $rs['code'] = 10011;
            $rs['msg'] = '动态标题输入非法，请重新输入';
            return $rs;
        }
        $domain = new Domain_Publish();
        $topicRes = '';
        if ($custom) {
            $custom = explode(',', $custom);
            $topicRes = $domain->addTopic($custom);
        }
        //  print_r($topic);exit;
        if ($topic && $custom) {
            $topic = trim($topic, ',');
            // $topicRes= trim($topicRes);
            $topic = $topic . ',' . $topicRes;
        }

        if (!isset($shop_id) || empty($shop_id)) {
            // $rs['code'] = 10011;
            // $rs['msg'] = '请选择商家';
            // return $rs;
            $shop_id = 0;
        }

        $data = array(
            'id' => $id,
            'uid' => $uid,
            'title' => $title,
            'thumb' => $thumb,
            'content' => $content,
            'cat_id' => $cat_id,
            'topic' => $topic ? $topic : $topicRes,
            'shop_id' => $shop_id,
            'lat' => $lat,
            'lng' => $lng,
            'city' => $city,
            'address' => $address,
            //'addtime' => time(),
            "likes" => 0,
            "comments" => 0,
            'custom' => $topicRes,
            'points' => $points,
            'status' => 1,
            'state' => $state,
            'country' => $country,
            'postal_code' => $postal_code,
            'area' => $area,
            'video' => $video,
            'video_img' => $video_img
        );

        $info = $domain->setDynamic($data);
        if ($info == 1004) {
            $rs['code'] = 1004;
            $rs['msg'] = '修改失败，请重试';
            return $rs;
        }
        if ($info == 1) {
            $rs['msg'] = "修改成功";
        }
        /* $rs['info'][0]['id']=$info['id'];
        $rs['info'][0]['thumb_s']=get_upload_path($thumb_s); */
        $rs['info'][0]['msg'] = '';
        return $rs;
    }
    
    
    /**
     * 获取店铺相关的帖子
     */
    public function getListByShop()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $shop_id= $this->shop_id;
        $p = $this->p;
        $domain = new Domain_Publish();
        $r = $domain->getListByShop($shop_id,$p);
        $rs['info'][0]['list'] = $r;
        //$rs['info'][] = $r;
        return $rs;

    }

    /**
     * 获取店铺列表
     */
    public function getShop()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $keywords = $this->keywords;
        $domain = new Domain_Publish();
        $r = $domain->getStatusShopList($keywords, $this->p, $this->lat, $this->lng);
        $rs['info'][0]['list'] = $r;
        //$rs['info'][] = $r;
        return $rs;

    }

    /**
     * 获取帖子推荐
     */
    public function recommendList()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $p = checkNull($this->p);
        $uid = checkNull($this->uid);
        $type = checkNull($this->type);
        $lat = checkNull($this->lat);
        $lng = checkNull($this->lng);
        $classid = checkNull($this->classid);
        $keyword = checkNull($this->keyword);
        $city = checkNull($this->city);


        $domain = new Domain_Publish();
        $r = $domain->getRecommend($p, $uid, $type, $lat, $lng, $classid, $keyword, $city);
        $rs['info'][0]['list'] = $r;
        return $rs;
    }

    /**
     * 帖子详情
     */
    public function detailsNote()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid = $_REQUEST['uid'];
        $token = $_REQUEST['token'];
        if($token != 'askpert'){  //h5过来不需要验证
            $checkToken = checkToken($uid, $token);
            if ($checkToken == 700) {
                $rs['code'] = $checkToken;
                $rs['msg'] = '您的登陆状态失效，请重新登陆！';
                return $rs;
            }
        }
       
        $domain = new Domain_Publish();
        $r = $domain->getNote($this->id, $this->uid);

        if ($r == 404) {
            $rs['msg'] = '帖子已被删除';
            $rs['info'] = [];
            return $rs;
        }
        if ($this->uid) {
            $addLog = new Domain_Userlog();
            $addLog->addLog($this->uid);
        }
        if ($this->uid && !empty($r['id'])) {
            $record = new Domain_Invitationvisit();
            $record->addVisitLog($this->uid, $r);
            $isLike = getUserNoteAtte($this->uid, $this->id);
            $record->addVisitLog($this->uid, $r);
            $r['isCollect'] = getUserNoteColl($this->uid, $this->id) ? 1 : 0;
            $r['is_like'] = $isLike;

        }
        $shop_id = $r['shopId'];
       
        $goods = DI()->notorm->ad->where('shop_id =?',$shop_id)->fetchOne();
     
        $goods =  [
            [
                'name'=>$goods['name']===null?'':$goods['name1'],
                'thumb' => $goods['thumb1']===null?'':get_upload_path($goods['thumb1']),
                'url' => $goods['url1']===null?'':$goods['url1'],
            ], 
             [
                'name'=>$goods['name2']===null?'':$goods['name2'],
                'thumb' => $goods['thumb2']===null?'':get_upload_path($goods['thumb2']),
                'url' => $goods['url2']===null?'':$goods['url2'],
            ],   
             [
                'name'=>$goods['name3']===null?'':$goods['name3'],
                'thumb' => $goods['thumb3']===null?'':get_upload_path($goods['thumb3']),
                'url' => $goods['url3']===null?'':$goods['url3'],
            ],   
             [
                'name'=>$goods['thumb4']===null?'':$goods['name4'],
                'thumb' => $goods['thumb4']===null?'':get_upload_path($goods['thumb4']),
                'url' => $goods['url4']===null?'':$goods['url4'],
            ]   
        ];
        $r['goods'] = $goods;
        $rs['info'][0]['details'] = $r;
        return $rs;
    }

    /**
     * 店铺详情
     */
    public function detailsShop()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $request = json_encode($_REQUEST);
        file_put_contents('axun.log',$request);
        $domain = new Domain_Publish();
        $r = $domain->getShop($this->id);
        if ($r == 404) {
            $rs['msg'] = '店铺不存在';
            $rs['info'] = [];
            return $rs;
        }
        $rs['info'][0]['details'] = $r;
        return $rs;

    }

    /**
     * 加入草稿
     */
    public function draft()
    {
        $rs = array('code' => 0, 'msg' => '加入成功', 'info' => array());

        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $title = checkNull($this->title);
        $thumb = checkNull($this->thumb);
        $content = checkNull($this->content);
        $cat_id = checkNull($this->cat_id);
        $video = $this->video;
        $video_img  = $_REQUEST['video_img'];
        $lat = checkNull($this->lat);
        $lng = checkNull($this->lng);
        $city = checkNull($this->city);
        $address = checkNull($this->address);
        $topic = checkNull($this->topic);
        $custom = checkNull($this->custom);
        $shop_id = checkNull($this->shop_id);
        $state = checkNull($this->state);
        $country = checkNull($this->country);
        $postal_code = checkNull($this->postal_code);
        $area = checkNull($this->area);
        $checkToken = checkToken($uid, $token);
        $points = $this->points;
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        $sensitivewords = sensitiveField($title);

        if ($sensitivewords == 1001) {
            $rs['code'] = 10011;
            $rs['msg'] = '动态标题输入非法，请重新输入';
            return $rs;
        }
        $data = array(
            'uid' => $uid,
            'title' => $title,
            'thumb' => $thumb,
            'content' => stripcslashes($content),
            'cat_id' => $cat_id,
            'topic' => $topic,
            'shop_id' => $shop_id,
            'lat' => $lat,
            'lng' => $lng,
            'city' => $city,
            'address' => $address,
            'addtime' => time(),
            "likes" => 0,
            "comments" => 0,
            'custom' => $custom,
            'draft' => 1,
            'status' => 0,
            'points' => $points,
            'state' => $state,
            'country' => $country,
            'postal_code' => $postal_code,
            'area' => $area,
            'id' => $this->id,
            'video' => $video,
            'video_img' => $video_img
        );
        
        $domain = new Domain_Publish();
        $info = $domain->setDynamic($data);
        if ($info == 1004) {
            $rs['code'] = 1004;
            $rs['msg'] = '加入失败，请重试';
            return $rs;
        }
        if ($info == 1) {
            $rs['msg'] = "加入成功";
        }
        return $rs;

    }

    public function draftList()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $p = checkNull($this->p);
        $checkToken = checkToken($uid, $token);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Publish();
        $r = $domain->getDraftList($uid, $p);
        $rs['info'][0]['list'] = $r;
        return $rs;
    }

    public function draftNote()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $id = checkNull($this->id);
        $checkToken = checkToken($uid, $token);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        if (!isset($id) || empty($id)) {
            $rs['code'] = -1;
            $rs['msg'] = 'Invalid Id！';
            return $rs;
        }

        $domain = new Domain_Publish();
        $r = $domain->draftNote($uid, $id);
        $rs['info'][0]['details'] = $r;
        return $rs;
    }

    public function draftDel()
    {
        $rs = array('code' => 0, 'msg' => '删除成功', 'info' => array());
        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $id = checkNull($this->id);
        $checkToken=checkToken($uid,$token);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        if (!isset($id) || empty($id)) {
            $rs['code'] = -1;
            $rs['msg'] = 'Invalid Id！';
            return $rs;
        }

        $domain = new Domain_Publish();
        $r = $domain->draftDel($uid, $id);
        $rs['info'][0]['msg'] = '';
        return $rs;
    }

    public function screen()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $checkToken = checkToken($uid, $token);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $data = [
            'reading' => $this->reading,
            'likes' => $this->likes,
            'collect_sum' => $this->collect_sum,
            'points' => $this->points,
            'topic' => $this->topic,
            'address' => $this->address,
        ];
        $domain = new Domain_Publish();
        $r = $domain->screen($uid, $data, $this->p);
        $rs['info'][0]['list'] = $r;
        return $rs;
    }

    public function hidePost()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $id = checkNull($this->post_id);
        $type = checkNull($this->type);
        $checkToken = checkToken($uid, $token);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        if (!isset($id) || empty($id)) {
            $rs['code'] = -1;
            $rs['msg'] = 'Invalid Id!';
            return $rs;
        }

        if (!isset($type) || ($type != 0 && $type != 1)) {
            $rs['code'] = -1;
            $rs['msg'] = 'Invalid Type!';
            return $rs;
        }

        $domain = new Domain_Publish();
        $r = $domain->hidePost($uid, $id, $type);
        $rs['info'][0]['msg'] = '';
        return $rs;
    }
}