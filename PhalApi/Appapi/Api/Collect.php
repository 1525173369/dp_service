<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/4 0004
 * Time: 13:31
 */
class Api_Collect extends PhalApi_Api {

    public function getRules() {
        return array(
            'collect'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'collect_id' => array('name' => 'collect_id', 'type' => 'int', 'require' => true, 'desc' => '帖子ID/店铺ID'),
                'type' => array('name' => 'type', 'type' => 'int', 'default' => '0', 'desc' => '0店铺1帖子收藏'),
                'lan'=>array('name' => 'lan', 'type' => 'int', 'default' => '0', 'desc' => '0中文1英文2马来语'),
            ),
           'collectList'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'status' => array('name' => 'status', 'type' => 'int', 'require' => true, 'desc' => '帖子/店铺'),
                'p' => array('name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '0店铺1帖子收藏'),
            ),
           'delCollect'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'collect_id' => array('name' => 'collect_id', 'type' => 'int', 'require' => true, 'desc' => '收藏id'),
            ),
            'isCollect'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'status' => array('name' => 'status', 'type' => 'int', 'require' => true, 'desc' => '帖子/店铺'),
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '帖子id'),
            ),
            
        );
    }

    /**
     * 用户收藏
     */
    public function collect(){
        $rs = array('code' => 0, 'msg' => '收藏成功', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $collect_id=checkNull($this->collect_id);
        $type=checkNull($this->type);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        //根据帖子的ID查询用户，查询当前用户和帖子用户是否存在拉黑
        $dynamic = new Domain_Dynamic();
        $result = $dynamic->getDynamicById($collect_id);
        $user = new Domain_User();
        $self = $user->isBlack($uid, $result['uid']);
        // $oth = $user->isBlack($result['uid'], $uid);
        // if($self == '1'){
            
        //     $rs['code'] = 200;
        //     $rs['msg'] = '您已经拉黑对方！';
        //     return $rs;
        // }
        
        if($self == '1'){
            
            $rs['code'] = 200;
            $rs['msg'] = '对方暂时拒绝了您的收藏！';
            return $rs;
        }
        
        
        $data = [
            'user_id'=>$uid,
            'type'=>$type,
            'collect_id'=>$collect_id,
            'add_time'=>time(),
        ];
        $domain = new Domain_Collect();
        $info = $domain->collect($data,$this->lan);
        if($info['status']==1004){
            //$rs['code']=1004;
            $rs['msg']='已取消收藏';
            return $rs;

        }
        return $rs;
    }
    
    /**
     * 收藏列表
     */
    public function collectList(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $domain = new Domain_Collect();
        $r  = $domain->getCollect($this->uid,$this->status,$this->p);
        $rs['info'][0]['favorite'] = $r;
        return $rs;

    }
    
    /**
     * 删除收藏
     */
    public function delCollect(){
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $collect_id = checkNull($this->collect_id);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Collect();
        $r  = $domain->delCollect($collect_id);
        if($r){
            return $rs;
        }else{
            $rs['code'] = 1004;
            $rs['msg'] = '操作失败';
            return $rs;

        }
        return $rs;

    }
    
    /**
     * 是否收藏帖子/店铺
     */
    public function isCollect(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $status=checkNull($this->status);
        $id=checkNull($this->id);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Collect();
        $r  = $domain->isCollect($id,$uid,$status);
        $rs['info'][0]['list'] = $r;
        return $rs;

    }

}