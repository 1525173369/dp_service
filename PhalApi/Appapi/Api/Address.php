<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 15:25
 */
class Api_Address extends PhalApi_Api {

    public function getRules() {
        return array(
            'getList' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
            ),
            'addAddress' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'consignee' => array('name' => 'consignee', 'type' => 'string','require' => true, 'desc' => '收货人'),
                'mobile' => array('name' => 'mobile', 'type' => 'string','require' => true, 'desc' => '号码'),
                'country' => array('name' => 'country', 'type' => 'string', 'desc' => '国家'),
                'province' => array('name' => 'province', 'type' => 'string', 'desc' => '省份'),
                'city' => array('name' => 'city', 'type' => 'string', 'desc' => '城市'),
                'district' => array('name' => 'district', 'type' => 'string', 'desc' => '区'),
                'address' => array('name' => 'address', 'type' => 'string','require' => true, 'desc' => '详细地址'),
                'address_id' => array('name' => 'address_id', 'type' => 'int', 'desc' => '地址ID'),
            ),
            
           'setDefault' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'address_id' => array('name' => 'address_id', 'type' => 'string','require' => true, 'desc' => 'id'),
            ),
            
            'getInfo' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'address_id' => array('name' => 'address_id', 'type' => 'string','require' => true, 'desc' => 'id'),
            ),
           'del' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'address_id' => array('name' => 'address_id', 'type' => 'string','require' => true, 'desc' => 'id'),
            ),
            
            
        );
    }

    /**
     * 获取用户地址列表
     */
    public function getList(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Address();
        $r  = $domain->getList($uid);
        $rs['info'][0]['list'] = $r;
        return $rs;
    }
    /**
     * 添加地址
     */
    public function addAddress(){
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());

        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $consignee=checkNull($this->consignee);
        $mobile=checkNull($this->mobile);
        $country=checkNull($this->country);
        $province=checkNull($this->province);
        $city=checkNull($this->city);
        $district=checkNull($this->district);
        $address=checkNull($this->address);
        $address_id=checkNull($this->address_id);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $data=array(
            'consignee'=>$consignee,
            'country'=>$country,
            'province'=>$province,
            'city'=>$city,
            'district'=>$district,
            'address'=>$address,
            'mobile'=>$mobile,
        );
        $domain = new Domain_Address();
        $infos = [];
        if(!empty($address_id)){
            //修改
            $info = $domain->updateAddress($data,$address_id);
            $infos['address_id'] = $address_id;
        }else{
            $info = $domain->addAddress($data,$uid);
            $infos['address_id'] = $info['id'];
        }
        if($info){
            $rs['info'][0] =$infos; 
            return $rs;
        }else{
            $rs['code']=1004;
            $rs['msg']='操作失败，请重试';
            return $rs;

        }
    }

    public function setDefault(){
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $address_id=checkNull($this->address_id);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Address();
        $info = $domain->setDefault($uid,$address_id);
        return $rs;

    }
    
    public function getInfo(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $address_id=checkNull($this->address_id);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Address();
        $r = $domain->getInfo($address_id);
        $rs['info'][0]['details'] = $r;
        return $rs;
    }
    
    public function del(){
        $rs = array('code' => 0, 'msg' => '删除成功', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $address_id=checkNull($this->address_id);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Address();
        $r = $domain->del($uid,$address_id);
        return $rs;
        
    }

}