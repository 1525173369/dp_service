<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 14:37
 */
class Api_Order extends PhalApi_Api {

    public function getRules() {
        return array(
            'addOrder'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id','require' => true, 'type' => 'int', 'desc' => '商品ID'),
                'number' => array('name' => 'number','default'=>'1', 'type' => 'int', 'desc' => '数量'),
                'address_id' => array('name' => 'address_id','require' => true, 'type' => 'int', 'desc' => '地址ID'),
                'lan' => ['name'=>'lan','require'=>true,'desc'=>'语言']

            ),
            'getOrder'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'p' => array('name' => 'id','default'=>'1', 'type' => 'int', 'desc' => '页数'),

            ),
             'orderInfo'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'order_id' => array('name' => 'order_id','require' => true, 'type' => 'int', 'desc' => '订单ID'),

            ),
            'confirm'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'order_id' => array('name' => 'order_id','require' => true, 'type' => 'int', 'desc' => '订单ID'),

            ),

        );

    }

    /**
     * 下单
     */
    public function addOrder(){
        $rs = array('code' => 0, 'msg' => '下单成功', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $id = checkNull($this->id);
        $number = checkNull($this->number);
        $address_id = checkNull($this->address_id);
        $checkToken=checkToken($uid,$token);
        $lan = checkNull($this->lan);
        if($lan != 'en' && $lan!='ms'){
            $lan = 'gb';
        }
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $data=[
            'id'=>$id,
            'number'=>$number,
            'address_id'=>$address_id
        ];
        $goods  = DI()->notorm->shop_goods->where('id = ?',$id)->fetchOne();
        if($goods['start']  &&  $goods['end']){
            if($goods['start'] > time()){
                $rs['code'] = 1003;
                $rs['msg'] = '抢购时间还未开始';
                return $rs;
            }
            if($goods['end'] < time()){
                $rs['code'] = 1003;
                $rs['msg'] = '抢购时间已经结束';
                return $rs;
            }
        }


        $domain=new Domain_Order();
        $r = $domain->place($uid,$data);
        if($r['status']==1003){
            $rs['code']=1003;
            if($lan == 'en'){
                $msg = 'Insufficient coin';
            }else if($lan == 'ms'){
                $msg = 'Koin tidak mencukupi';
            }else{
                $msg = '金币不足';
            }
            $rs['msg']=$msg;
            return $rs;
        }elseif ($r['status']==1004){
            $rs['code']=1004;
            if($lan == 'en'){
                $msg = 'Fully redeemed';
            }else if($lan == 'ms'){
                $msg = 'Habis ditebus';
            }else{
                $msg = '库存不足';
            }
            $rs['msg']=$msg;
            return $rs;
        }else if($r['status'] == 1002){
            $rs['code'] = 1002;
            if($lan == 'en'){ //语言操作 统一gb中文 en英文 ms马来西亚
                $rs['msg'] = 'Please verify your identity to continue the redemption';
            }
            if($lan == 'ms'){
                $rs['msg'] = 'Sila sahkan identiti anda untuk menebuskan hadiah';
            }
            if($lan == 'gb'){
                $rs['msg'] = '请实名认证后再兑换礼物';
            }

            return $rs;
        }
        $rs['info'][0]=$r['result'];
        return $rs;
    }
    
    public function getOrder(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain=new Domain_Ordergoods();
        $r = $domain->getList($uid,$this->p);
        $rs['info'][0]['list']=$r;
        return $rs;

    }
    
    public function orderInfo(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $order_id=checkNull($this->order_id);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain=new Domain_Ordergoods();
        $r = $domain->getInfo($order_id);
        $rs['info'][0]['list']=$r;
        return $rs;
    }
    
    public function confirm(){
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $order_id=checkNull($this->order_id);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain=new Domain_Ordergoods();
        $r = $domain->confirm($order_id);
        return $rs;

    }

}