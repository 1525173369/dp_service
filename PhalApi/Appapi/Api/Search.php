<?php
/**
 * Created by PhpStorm.
 * User: liaohongyun
 * Date: 2021/2/26
 * Time: 11:03
 */
class Api_Search extends PhalApi_Api{
    public function getRules() {
        return array(
            'search'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                // 'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'keyword' => array('name' => 'keyword', 'type' => 'string', 'require' => true, 'desc' => '模糊搜索内容'),
            ),
            'searchV2'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'keyword' => array('name' => 'keyword', 'type' => 'string', 'require' => true, 'desc' => '模糊搜索内容'),
            ),
        );
    }
    	/**
     * showdoc
     * @catalog 搜索V2
     * @title 搜索V2
     * @description 搜索V2
     * @method POST
     * @url {domian}/appapi/?service=serach.serachV2
     * @param id 必选 int id
     * @param uid 必选 int 用户uid
     * @return {"ret":200,"data":{"code":0,"msg":"\u83b7\u53d6\u6210\u529f","info":[[{"title":"\u51e4\u51f0\u51e4\u51f02","count":"1"},{"title":"\u51e4\u51f0\u51e4\u51f0","count":"2"}]]},"msg":""}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @return_param info[title] string 搜索出的标题
     * @return_param info[count] int 搜索出的结果
     * @remark
     * @number 1
     */
    public function searchV2(){
        $rs = array('code' => 0, 'msg' => '获取成功', 'info' => array());
        $uid=checkNull($this->uid);
        $isblack = DI()->notorm->user_black->select('touid')->where('uid = ? and type=1',$uid)->fetchAll();
        $result = [];
        array_walk_recursive($isblack, function($value) use (&$result) {
            array_push($result, $value);
        });
        $isblack_str =implode(',',$result);
        $keyword=checkNull($this->keyword);
      
        $sql = 'select * from (select `name` as title,des as content,address,1 as type FROM  cmf_shop_apply UNION select title,content,address,2 as type from cmf_dynamic where isdel=0 and draft = 0 UNION select id as title,user_nickname as content,address,3 as type from cmf_user UNION select  title,`desc` as content,address,4 as type from cmf_video where is_delete = 0 and draft = 0) as a';
        $where = ' where  (1=1 and title like "%'.$keyword.'%" or content like "%'.$keyword.'%" or address like "%'.$keyword.'%")';
        $sql.= $where;
        $sql.=' limit 0,10';

       
       
        $data = DI()->notorm->dynamic->queryall($sql);
        $relax =   [];
        foreach ($data as $k=>$v){
           
            $result_count =  0;
            $content = $v['content'];
            $title  = $v['title'];
            $address = $v['address'];
            $type = $v['type'];
            $black_rx = false;
              
            if($type == 1){
                $balck_rx = 'shop_apply';
                $title_field = 'name';
                $content_field = 'des';
                $model = DI()->notorm->shop_apply;
                if($isblack_str){
                    $model = $model->where("uid NOT IN (".$isblack_str.")");
                }
               
               
            }
            if($type == 2){
                $balck_rx = 'dynamic';
                $title_field = 'title';
                $content_field = 'content';
                $model = DI()->notorm->dynamic;
                if($isblack_str){
                   $model = $model->where("uid NOT IN (".$isblack_str.") and isdel = 0 and draft =0");
                }
            }
            if($type ==  3){
                $balck_rx = 'user';
                $title_field = 'id';
                $content_field = 'user_nickname';
                $model = $model = DI()->notorm->user;
                if($isblack_str){
                    $model = $model->where("id NOT IN (".$isblack_str.")");
                }
                
            }
            if($type == 4){
                $balck_rx = 'video';
                $title_field = 'title';
                $content_field = 'desc';
                $model = DI()->notorm->video;
                if($isblack_str){
                    $model = $model->where("uid NOT IN (".$isblack_str.") and is_delete = 0 and draft =0");
                }
            }
          
           
            
            $keyword = strtolower($keyword);
            if(strpos(strtolower($title),$keyword) !== false){
                $title = $title;
                $type = 'title';
            }elseif(strpos(strtolower($content),$keyword) !== false){
                $title = $content;
                $type = 'content';
            }else{
                $title = $address;
                $type = 'address';
            }
            
            $rp = explode($keyword,strtolower($title));
            $str1 = '';
            $str2 = '';
            $indexof = mb_strpos($title,$keyword);
            $len  = mb_strlen($title);
            $is_strop = 4;
            $klen = mb_strlen($keyword);
            if($klen > 7){
               $is_strop =  0;   
            }
            if(mb_strlen($rp[0]) > 4){
                  if($is_strop){
                      $str1 =  mb_substr($rp[0], -4, $is_strop);
                  }else{
                      $str1 = '';
                  }
                  
                  
            }else{
                $str1 = $rp[0];
            }
            if(mb_strlen($rp[1]) > 4 ){
                   if($is_strop){
                         $str2 = mb_substr($rp[1], 0, $is_strop);
                   }else{
                       $str2 = '';
                   }
                 
            }else{
                $str2 = $rp[1];
            }
            
            #var_dump($str1,$str2,$indexof,$len,$title,$keyword);die;
            $title = $str1.$keyword.$str2;
         
           /* if($type == 'title'){
                 
                 $count =  $model->where('lower('.$title_field.') like ?',"%$title%")->count();
            }else  if($type == 'content'){
                 $count =  $model->where('lower(`'.$content_field.'`) like ?',"%$title%")->count();
            }else{
                 $count =  $model->where('lower(address) like ?',"%$title%")->count();
            }*/
       
            
             $count = $model->where('lower('.$title_field.') like ? or lower(`'.$content_field.'`) like ? or lower(address) like ?',"%$title%","%$title%","%$title%")->count();
        
            
            
            $result_count = $count;
            $result_title = $title;
          
           
            $relax[] = [
                'title' => $result_title,
                'count' => $result_count,
                'type' => $type,
            ];
            
            
        

        }
        $i= [];
        foreach($relax as $k=>$v){
            $i[$v['title']] = $v;
        }
        $relax =[];
        foreach ($i as $k=>$v){
            $relax[] = $v;
        }
        $ios = $_GET['ios'];
        if($ios == 1){
            $rs['info'][0]['list'] = $relax;
        }else{
            $rs['info'][0] = $relax;
        }
        
        return $rs;
    }

    //获取http协议
    function get_http_type($urls =false)
    {
        if(!$urls){
            return false;
        }
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $url = $http_type.$_SERVER['HTTP_HOST'].'/ffmpeg/';
        if(strpos($urls,'http') === false){  //是否包含
            $url = $url.$urls;
        }else{
            $url = $urls;
        }
        return $url;
    }
    public function search(){
        $rs = array('code' => 0, 'msg' => '获取成功', 'info' => array());
        $uid=checkNull($this->uid);
        // $token=checkNull($this->token);
        $keyword=checkNull($this->keyword);
        // $checkToken=checkToken($uid,$token);
//        if($checkToken==700){
//            $rs['code'] = $checkToken;
//            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
//            return $rs;
//        }

        $isblack = DI()->notorm->user_black->select('touid')->where('uid = ? and type=1',$uid)->fetchAll();
        $result = ['99999999999999']; //防止下面为空时报错
        array_walk_recursive($isblack, function($value) use (&$result) {
            array_push($result, $value);
        });
        $isblack_str =implode(',',$result);
        $where = " LIKE '%{$keyword}%'";
       // $dynamic = DI()->notorm->dynamic->where("title" . $where)->select('*')->fetchAll();//帖子
        $ks = str_replace(' ','',$keyword);
        $ks = strtolower($ks);
        $where2021414 = " LIKE '%{$ks}%'";  //都去掉空格后 来进行搜索,更加精准
        
        
        $dynamic = DI()->notorm->dynamic->where("uid NOT IN (".$isblack_str.")and lower(REPLACE(title,' ',''))".$where2021414.' or lower(REPLACE(content," ","")) '.$where2021414)->where('draft = 0')->where('isdel = 0')->select('*')->fetchAll();
        
        // echo "lower(REPLACE(title,' ',''))".$where2021414.' or lower(REPLACE(content," ","")) '.$where2021414." and uid NOT IN (".$isblack_str.")";
        // exit;
     
        #var_dump($dynamic,"REPLACE(title,' ','')".$where2021414.' or lower(REPLACE(content," ","")) '.$where2021414);die;
        

        //$shop_apply = DI()->notorm->shop_apply->where('name'.$where)->select('*')->fetchAll();//商户
        $shop_apply = DI()->notorm->shop_apply->where("REPLACE(name,' ','')".$where2021414." and uid NOT IN (".$isblack_str.")")->select('*')->fetchAll();//商户

        //$shop_topic = DI()->notorm->shop_topic->where('topic_name'.$where)->group('topic_name')->select('topic_id,topic_name')->fetchAll();//话题
        $shop_topic = DI()->notorm->shop_topic->where("REPLACE(topic_name,' ','')".$where2021414)->group('trim(topic_name)')->select('topic_id,topic_name')->fetchAll();//话题

        //$user = DI()->notorm->user->where('user_nickname'.$where .' and id != 1')->select('*')->fetchAll();//用户名 ->where(" 'and id NOT IN (SELECT touid FROM cmf_user_black WHERE type=1 and uid=".$uid.")'")
        
        $user = DI()->notorm->user->where("id NOT IN (".$isblack_str.") and user_nickname ".$where2021414.' and id != 1 or  id = "'.$ks.'"')->select('*')->fetchAll();//用户名
        // echo "id NOT IN (".$isblack_str.") and user_nickname ".$where2021414.' and id != 1 or  id = "'.$ks.'"';exit;
        
        $topic = DI()->notorm->shop_topic->where('topic_name '.$where2021414)->fetchAll();
        $topic_str = [];
        foreach ($topic as $k=>$v){
            $topic_str[] = $v['topic_id'];
        }
        $topic_where = '';
        if($topic_str){
            $topic_str = implode(',',$topic_str);
            $topic_where=' or topic in ('.$topic_str.')';
        }
       
        $videos = DI()->notorm->video->where("REPLACE(title,' ','')".$where2021414.' or lower(REPLACE(`desc`," ","")) '.$where2021414.$topic_where." and uid NOT IN (".$isblack_str.") and REPLACE(title,' ','') != ''")->where('draft = 0')->where('is_delete = 0')->fetchAll();
        
      
        foreach ($user as $k=>$v){
            if($v['major'] == 1){
                $v['type'] = 'green';
            }else if($v['user_status'] == 1){
                $v['type'] = 'blue';
            }else{
                $v['type'] = '';
            }
            $v['avatar'] = get_upload_path($v['avatar']);
            $user[$k] = $v;
        }
        

        //$shop_goods = DI()->notorm->shop_goods->where('goods_name'.$where)->select('*')->fetchAll();//礼物
        $shop_goods = DI()->notorm->shop_goods->where("REPLACE(goods_name,' ','')".$where2021414)->select('*')->fetchAll();//礼物
        if($shop_goods){
            $domainStatus = new Domain_Goods();
            foreach ($shop_goods as $k => $v) {
                $res = $domainStatus->checkStatus($uid,$v['id']);
                $shop_goods[$k]['button']=$res['status'];
                if($v['store_count'] == 0){
                    $shop_goods[$k]['button']= 3;
                }
                $shop_goods[$k]['original_img'] = get_upload_path($v['original_img']);
            }
        }
        foreach ($dynamic as &$val){ //帖子
            if($val['thumb']){
                $thumb_arr=explode(',',trim($val['thumb'],','));
                foreach ($thumb_arr as $k => $v) {
                    $thumb_arr[$k]=get_upload_path($v);
                }
                $val['thumb']= $thumb_arr;
            }else{
                $val['thumb'] = [];
            }
            if($val['video']){
                $video=explode(',',trim($val['video'],','));
                foreach ($video as $k => $v) {
                    $video[$k]=get_upload_path($v);
                }
                $val['video']= $video;
            }else{
                $val['video'] = [];
            }
            if($val['video_img']){
                $video_img=explode(',',trim($val['video_img'],','));
                foreach ($video_img as $k => $v) {
                    $video[$k]=$this->get_http_type($v);
                }
                $val['video_img']= $video;
            }else{
                $val['video_img'] = [];
            }

            if($val['uid']){
                $val['info']=getUserInfo($val['uid']);
                $val['info']['avatar'] = get_upload_path($val['info']['avatar']);
                $val['info']['avatar_thumb'] = get_upload_path($val['info']['avatar_thumb']);
            }
            $val['custom'] = getShopTopic($val['custom']) ?: [];
            $val['topic'] = getShopTopic($val['topic']) ?: [];
            $val['topic'] = array_merge($val['custom'],$val['topic']);
            $val['topic'] = array_unique($val['topic'],SORT_REGULAR);
            $val['topic'] = array_values($val['topic']);
            $val['isCollect']  = getUserNoteColl($uid, $val['id']) ? 1 : 0;
            $val['isAttn']  = getUserNoteAtte($uid, $val['id']);
        }
        
        
        
        foreach ($videos as &$val){ //帖子
           
            if($val['video_url']){
                $video=explode(',',trim($val['video_url'],','));
               
                foreach ($video as $k => $v) {
                    $video[$k]=get_upload_path($v);
                }
                $val['video_url']= $video[0];
            }else{
                $val['video_url'] = '';//[];
            }
            if($val['video_img']){
                $video_img=explode(',',trim($val['video_img'],','));
                foreach ($video_img as $k => $v) {
                    $video[$k]=$this->get_http_type($v);
                }
                $val['video_img']= $video[0];
            }else{
                $val['video_img'] ='';// [];
            }

            if($val['uid']){
                $val['info']=getUserInfo($val['uid']);
                $val['info']['avatar'] = get_upload_path($val['info']['avatar']);
                $val['info']['avatar_thumb'] = get_upload_path($val['info']['avatar_thumb']);
            }
            $val['custom'] = getShopTopic($val['custom']) ?: [];
            $val['topic'] = getShopTopic($val['topic']) ?: [];
            $val['topic'] = array_merge($val['custom'],$val['topic']);
            $val['topic'] = array_unique($val['topic'],SORT_REGULAR);
            $val['topic'] = array_values($val['topic']);
           /* $val['isCollect']  = getUserNoteColl($uid, $val['id']) ? 1 : 0;
            $val['isAttn']  = getUserNoteAtte($uid, $val['id']);*/
            $val['is_collect'] = DI()->notorm->user_video_collect->where('video_id = ? and uid = ?',$val['id'],$uid)->count();
            $val['is_like'] = DI()->notorm->user_video_like->where('video_id = ? and uid = ?',$val['id'],$uid)->count();
            $val['newTopic_ios'] = $val['topic'];
            $val['users'] = getUserInfo($val['uid']);
            $val['id'] = (int)$val['id'];
            $val['uid'] = (int)$val['uid'];
            $val['shops'] = DI()->notorm->shop_apply->where('id = ?',$val['shop_id'])->fetchOne();
            $val['score'] = (int)$val['score'];
            $val['shares'] = (int)$val['shares'];
            $val['likes'] = (int)$val['likes'];
            $val['comments'] = (int)$val['comments'];
            $val['draft'] = (int)$val['draft'];
        }
        
        
        
        
        
        foreach($shop_apply as $k =>$res){
            //判断用户是否关注了店铺主播
            $isattention=isAttention($uid,$res[$k]['uid']);
            $shop_apply[$k]['isattention']=$isattention;
            $userinfo = getUserInfo($res[$k]['uid']);

            $shop_apply[$k]['user_nickname'] = $userinfo['user_nickname'];
            $shop_apply[$k]['avatar'] = get_upload_path($userinfo['avatar']);
            $shop_apply[$k]['avatar_thumb'] = get_upload_path($userinfo['avatar_thumb']);
            $shop_apply[$k]['thumb'] = get_upload_path($res['thumb']);
            //获取店铺点赞信息
            $shop_fabulous = getShopFabulous($res[$k]['uid'], $uid);
            $shop_apply[$k]['fabulous'] = $shop_fabulous['fabulous'];
            $shop_apply[$k]['isfabulous'] = $shop_fabulous['isfabulous'];
            $v1 = $v2 = 0;
            $v1 = DI()->notorm->video->where('is_delete =? and draft =? and shop_id =?',0,0,$res['id'])->count();
            $v2 = DI()->notorm->dynamic->where('isdel=? and draft=? and shop_id =?',0,0,$res['id'])->count();
            $v = $v1 +$v2;
            $shop_apply[$k]['consumption'] = (string)$v;

            //获取店铺话题
            $shop_apply[$k]['topic'] = getShopTopic($res[$k]['topic']);
        }
        foreach ($user as &$v){
            $v['follow'] = DI()->notorm->user_attention
                ->where('touid=?',$v['id'])
                ->count();
        }
        //插入搜索记录
        $history = DI()->notorm->history_search->where([
            'uid'=>$uid,
            'keyword'=>$keyword
        ])->fetch();
        if($history){
            DI()->notorm->history_search->where([
                'uid'=>$uid,
                'keyword'=>$keyword
            ])->update(
                ['num'=>$history['num']+1]);

        }else{
            DI()->notorm->history_search->insert([
                'uid'=>$uid,
                'keyword'=>$keyword,
                'add_time'=>time(),
            ]);
        }
        $info['dynamic'] = $dynamic ?: [];
        $info['shop_apply'] = $shop_apply ?: [];
        $info['shop_topic'] = $shop_topic ?: [];
        $info['user'] = $user ?: [];
        $info['shop_goods'] = $shop_goods ?: [];
        $info['video'] = $videos ?: [];
        $rs['info'][0] = $info;
        return $rs;
    }
}