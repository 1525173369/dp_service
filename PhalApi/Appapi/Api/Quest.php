<?php
header('Access-Control-Allow-Origin: *');
class Api_Quest extends PhalApi_Api {

	public function getRules() {
		return array(
		    'getQuestExamList'=>array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
			),
			'getQuestExam'=>array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'exam_id' => array('name' => 'exam_id', 'type' => 'string', 'require' => true, 'desc' => '试卷ID'),
                 'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1', 'desc' => '页数'),
			),
			'userQuestExam'=>array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'exam_id' => array('name' => 'exam_id', 'type' => 'string', 'require' => true, 'desc' => '试卷ID'),
                'exam' => array('name' => 'exam', 'type' => 'string', 'require' => true, 'desc' => '试题答案, 传递序列化的数组'),
			),
			'answer'=>array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'type' => array('name' => 'type', 'type' => 'int', 'require' => true, 'desc' => '类型'),
                'answer' => array('name' => 'answer', 'type' => 'string', 'require' => true, 'desc' => '试题答案'),
                'paper_id' => array('name' => 'paper_id', 'type' => 'int', 'require' => true, 'desc' => '试卷ID'),
                'number' => array('name' => 'number', 'type' => 'int', 'require' => true, 'desc' => '试题编号'),
			),
			
		);
	}
	
	
	
	public function getQuestExamList() {
	    $rs = array('code' => 0, 'msg' => '', 'info' => array());

		$uid=checkNull($this->uid);
        $token=checkNull($this->token);
        
// 	    $checkToken=checkToken($uid,$token);
// 		if($checkToken==700){
// 			$rs['code'] = $checkToken;
// 			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
// 			return $rs;
// 		}
		
		$domain=new Domain_Quest();
		$res=$domain->getQuestExamList();
		
		if($res) {
		    $rs['info'][0]['questExamList'] = $res; 
		}else {
		    $rs['msg'] = '暂无问卷调查';
		}
		
		return $rs;
		
		
		
	}
	
	
	public function getQuestExam() {
	    $rs = array('code' => 0, 'msg' => '', 'info' => array());

		$uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $exam_id=checkNull($this->exam_id);

// 	    $checkToken=checkToken($uid,$token);
// 		if($checkToken==700){
// 			$rs['code'] = $checkToken;
// 			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
// 			return $rs;
// 		}
		$list = DI()->notorm->user_answer
            	->where("user_id={$uid} and paper_id={$exam_id} and status=1")
                ->fetch();
		if($list){
		    $rs['code'] = 1002;
			$rs['msg'] = '您已答题，请勿重复答题！';
			return $rs;
		}
		$domain=new Domain_Quest();
		$res=$domain->getQuestExam($exam_id,$this->p);
		
		if($res) {
		    $rs['info'][0]['questExam'] = $res; 
		}else {
		    DI()->notorm->user_answer
            	->where("user_id={$uid} and paper_id={$exam_id} and status=0")
            	->update(['status'=>1]);
		    $rs['msg'] = '暂无问卷调查';
		}
		
		return $rs;
		
		
		
	}
	
	
	public function answer(){
	    $rs = array('code' => 0, 'msg' => '', 'info' => array());
	    $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $number=checkNull($this->number);   //题号
        $type=checkNull($this->type);       //类型
        $answer=checkNull($this->answer);   //答案
        $paper_id =checkNull($this->paper_id);
        $data = [
            'number'=>$number,        
            'type'=>$type,        
            'answer'=>$answer,
            'paper_id'=>$paper_id
        ];
		$domain=new Domain_Quest();
		$res=$domain->addAnswer($uid,$data);
		return $rs;
	}
	
	public function userQuestExam() {
	    $rs = array('code' => 0, 'msg' => '', 'info' => array());

		$uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $exam_id=checkNull($this->exam_id);
        $exam=checkNull($this->exam);

// 	    $checkToken=checkToken($uid,$token);
// 		if($checkToken==700){
// 			$rs['code'] = $checkToken;
// 			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
// 			return $rs;
// 		}

        // $rs['msg'] = '用户答卷成功!';
        // return $rs;



        $exam = array(
            "id" => "4", //试卷ID
            "title" => "qweqwe", // 试卷标题
            'quest' => array(
                "0" => array(
                    "FormId" => "1", //类型ID  1: 单选题 2: 多选题 3: 判断题 4:问答题
                    "FormTitle" => "单选题",
                    "FormTopic" => array(
                        "0" => array(
                            "id" => "1", // 单选题ID
                            "title" => "单选题标题", // 单选题标题
                            "pid" => array( // 仅记录用户选择的
                                "0" => array(
                                        "id" => "11", // 选项ID
                                        "label" => "A", // 选项标签
                                        "department" => "12313", // 选项内容
                                    )
                                )
                            ),
                        "1" => array(
                            "id" => "2", // 单选题ID
                            "title" => "单选题标题", // 单选题标题
                            "pid" => array( // 仅记录用户选择的
                                "0" => array(
                                        "id" => "16", // 选项ID
                                        "label" => "B", // 选项标签
                                        "department" => "345345", // 选项内容
                                    )
                                )
                            )
                        )
                    ),
                "1" => array(
                    "FormId" => "2", //类型ID  1: 单选题 2: 多选题 3: 判断题 4: 问答题
                    "FormTitle" => "多选题",
                    "FormTopic" => array(
                        "0" => array( // 仅记录用户选择的
                            "id" => "1", // 多选题ID
                            "title" => "多选题标题", // 多选题标题
                            "pid" => array( // 仅记录用户选择的
                                "0" => array(
                                        "id" => "4", // 选项ID
                                        "label" => "A", // 选项标签
                                        "checkbox-topics" => "多选题选项A", // 选项内容
                                    ),
                                "1" => array(
                                        "id" => "5", // 选项ID
                                        "label" => "B", // 选项标签
                                        "checkbox-topics" => "多选题选项B", // 选项内容
                                    ),
                                "2" => array(
                                        "id" => "6", // 选项ID
                                        "label" => "C", // 选项标签
                                        "checkbox-topics" => "多选题选项C", // 选项内容
                                    ),
                                )
                            ),
                        )
                    ),
                "2" => array(
                    "FormId" => "3", //类型ID  1: 单选题 2: 多选题 3: 判断题 4:问答题
                    "FormTitle" => "判断题",
                    "FormTopic" => array(
                        "0" => array(
                            "id" => "1", // 判断题ID
                            "title" => "判断题标题", // 判断题标题
                            "answer" => "1" // 用户选择 0 为选择本题错误  1 为选择本题正确
                            ),
                        "1" => array(
                            "id" => "2", // 判断题ID
                            "title" => "判断题标题", // 判断题标题
                            "answer" => "0" // 用户选择 0 为选择本题错误  1 为选择本题正确
                            )
                        )
                    ),
                "3" => array(
                    "FormId" => "4", //类型ID  1: 单选题 2: 多选题 3: 判断题 4:问答题
                    "FormTitle" => "问答题",
                    "FormTopic" => array(
                        "0" => array(
                            "id" => "1", // 问答题ID
                            "title" => "问答题标题", // 问答题标题
                            "answer" => "用户问答题解答" // 用户问答题解答
                            ),
                        "1" => array(
                            "id" => "2", // 问答题ID
                            "title" => "问答题标题", //问答题标题
                            "answer" => "用户问答题解答" // 用户问答题解答
                            )
                        )
                    )
                )
            );
            
        $exam = json_encode($exam);
		
		$domain=new Domain_Quest();
		$res=$domain->userQuestExam($uid, $exam_id, $exam);
		
		if($res) {
		    $rs['msg'] = '答题成功';
		} else {
		    $rs['msg'] = '答题失败请重试';
		}
		
// 		if($res) {
// 		    $rs['info'][0]['questExamList'] = $res; 
// 		}else {
// 		    $rs['msg'] = '暂无问卷调查';
// 		}
		
		return $rs;
		
		
		
	}
	
	
	
}