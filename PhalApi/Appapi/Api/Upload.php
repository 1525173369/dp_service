<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/27 0027
 * Time: 13:02
 */
header('Access-Control-Allow-Origin: *');
class Api_Upload extends PhalApi_Api {


    public function getRules() {

    }

    /**
     * 通用的图片上传
     */
    public function upload(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $thumb='';
		if($_FILES){
			if ($_FILES["file"]["error"] > 0) {
				$rs['code'] = 1003;
				$rs['msg'] = T('failed to upload file with error: {error}', array('error' => $_FILES['file']['error']));
				DI()->logger->debug('failed to upload file with error: ' . $_FILES['file']['error']);
				return $rs;
			}
			
			if(!checkExt($_FILES["file"]['name'])){
				$rs['code']=1004;
				$rs['msg']='图片仅能上传 jpg,png,jpeg';
				return $rs;
			}

			$uptype=DI()->config->get('app.uptype');
			if($uptype==1){
				//七牛
				$url = DI()->qiniu->uploadFile($_FILES['file']['tmp_name']);

				if (!empty($url)) {
					$thumb=  $url.'?imageView2/2/w/600/h/600'; //600 X 600
				}
			}else if($uptype==2){
				//本地上传
				//设置上传路径 设置方法参考3.2
				DI()->ucloud->set('save_path','thumb/'.date("Ymd"));

				//新增修改文件名设置上传的文件名称
			   // DI()->ucloud->set('file_name', $this->uid);

				//上传表单名
				$res = DI()->ucloud->upfile($_FILES['file']);
				
				$files='../upload/'.$res['file'];
				$PhalApi_Image = new Image_Lite();
				//打开图片
				$PhalApi_Image->open($files);
				/**
				 * 可以支持其他类型的缩略图生成，设置包括下列常量或者对应的数字：
				 * IMAGE_THUMB_SCALING      //常量，标识缩略图等比例缩放类型
				 * IMAGE_THUMB_FILLED       //常量，标识缩略图缩放后填充类型
				 * IMAGE_THUMB_CENTER       //常量，标识缩略图居中裁剪类型
				 * IMAGE_THUMB_NORTHWEST    //常量，标识缩略图左上角裁剪类型
				 * IMAGE_THUMB_SOUTHEAST    //常量，标识缩略图右下角裁剪类型
				 * IMAGE_THUMB_FIXED        //常量，标识缩略图固定尺寸缩放类型
				 */
				$PhalApi_Image->thumb(660, 660, IMAGE_THUMB_SCALING);
				$PhalApi_Image->save($files);							
				
				$thumb=$res['url'];
			}
			
			@unlink($_FILES['file']['tmp_name']);	
			
			$rs['info'][0]['thumb'] = $thumb;
			
			return $rs;
		}
        
        
        
        
//         $file=$_FILES['file'];
//         //设置上传路径
//         DI()->ucloud->set('save_path','thumb/'.date("Ymd"));
//         $rs = DI()->ucloud->upfile($file);
//         $errorRes = DI()->ucloud->get('error');
//         if(!empty($errorRes)){
//         	$rs['code'] = 1000;
// 			$rs['msg'] = $errorRes;
// 			return $rs;
//         }
//         return $rs;

    }

    public function uploading(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $thumb='';
		if($_FILES && is_array($_FILES['file']['tmp_name'])){

			$uptype=DI()->config->get('app.uptype');
			if($uptype==1){
				//七牛
				$va = [];
				foreach ($_FILES['file']['tmp_name'] as $val){

        			$url = DI()->qiniu->uploadFile($val);
                    
    				if (!empty($url)) {
    					$va[]=  $url.'?imageView2/2/w/600/h/600'; //600 X 600
    				}
				}

			}else if($uptype==2){
			    //本地
			}

			
			$rs['info'][0]['thumb'] = $va;
			
			return $rs;
		}elseif($_FILES){
    		$url = DI()->qiniu->uploadFile($_FILES['file']['tmp_name']);

			if (!empty($url)) {
				$thumb=  $url.'?imageView2/2/w/600/h/600'; //600 X 600
			}
    		$rs['info'][0]['thumb'] = $thumb;
			
			return $rs;    
		}
		
    }

}
