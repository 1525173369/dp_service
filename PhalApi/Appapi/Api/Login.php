<?php
/**
 * 登录、注册
 */
if (!session_id()) session_start();
class Api_Login extends PhalApi_Api { 
    public function getRules() {
        return array(
            'appLogin' => array(
                'phone' => array('name' => 'phone', 'type' => 'string', 'require' => true,  'min' => '6',  'max'=>'30', 'desc' => '手机号码'),
                'code' => array('name' => 'code', 'type' => 'string','require' => true,  'min' => '1',  'max'=>'30', 'desc' => '验证码'),
                'lan' => array('name' => 'lan', 'type' => 'string', 'default'=>'', 'desc'=>'language'),
                'device_id' => array('name' => 'device_id', 'type' => 'string', 'default'=>'', 'desc'=>'device_id'),
                'as_id' => array('name' => 'as_id', 'type' => 'string', 'default'=>'', 'desc'=>'ad id'),
                'device' => array('name' => 'device', 'type' => 'string', 'default'=>'', 'desc'=>'device'),
            ),
            'loginAccount' => array(
                'user_login' => array('name' => 'user_login', 'type' => 'string', 'require' => true,  'min' => '6',  'max'=>'30', 'desc' => '手机号码'),
                'user_pass' => array('name' => 'user_pass', 'type' => 'string','require' => true,  'min' => '1',  'max'=>'30', 'desc' => '密码'),
                'lan' => array('name' => 'lan', 'type' => 'string', 'default'=>'', 'desc'=>'language'),
                'device_id' => array('name' => 'device_id', 'type' => 'string', 'default'=>'', 'desc'=>'device_id'),
                'as_id' => array('name' => 'as_id', 'type' => 'string', 'default'=>'', 'desc'=>'ad id'),
                'device' => array('name' => 'device', 'type' => 'string', 'default'=>'', 'desc'=>'device'),
            ),
            'userReg' => array(
                'user_login' => array('name' => 'user_login', 'type' => 'string','require' => true,  'min' => '6',  'max'=>'30', 'desc' => '账号'),
                'user_pass' => array('name' => 'user_pass', 'type' => 'string','require' => true,  'min' => '1',  'max'=>'30', 'desc' => '密码'),
                'user_pass2' => array('name' => 'user_pass2', 'type' => 'string',  'require' => true,  'min' => '1',  'max'=>'30', 'desc' => '确认密码'),
                'code' => array('name' => 'code', 'type' => 'string', 'min' => 1, 'require' => true,   'desc' => '验证码'),
                'source' => array('name' => 'source', 'type' => 'string',  'default'=>'pc', 'desc' => '来源设备'),
                'device_id' => array('name' => 'device_id', 'type' => 'string', 'default'=>'', 'desc'=>'device_id'),
                'as_id' => array('name' => 'as_id', 'type' => 'string', 'default'=>'', 'desc'=>'ad id'),
                'device' => array('name' => 'device', 'type' => 'string', 'default'=>'', 'desc'=>'device'),
            ),
            'userFindPass' => array(
                'user_login' => array('name' => 'user_login', 'type' => 'string', 'require' => true,  'min' => '6',  'max'=>'30', 'desc' => '账号'),
                'user_pass' => array('name' => 'user_pass', 'type' => 'string', 'require' => true,  'min' => '1',  'max'=>'30', 'desc' => '密码'),
                'user_pass2' => array('name' => 'user_pass2', 'type' => 'string', 'require' => true,  'min' => '1',  'max'=>'30', 'desc' => '确认密码'),
                'code' => array('name' => 'code', 'type' => 'string', 'min' => 1, 'require' => true,   'desc' => '验证码'),
            ),  
            'userLoginByThird' => array(
                'openid' => array('name' => 'openid', 'type' => 'string', 'min' => 1, 'require' => true,   'desc' => '第三方openid'),
                'email' => array('name' => 'email', 'type' => 'string',  'default'=>'',   'desc' => '邮箱'),
                'nicename' => array('name' => 'nicename', 'type' => 'string',   'default'=>'',  'desc' => '第三方昵称'),
                'avatar' => array('name' => 'avatar', 'type' => 'string',  'default'=>'', 'desc' => '第三方头像'),
                'mark' => array('name' => 'mark', 'type' => 'int',  'default'=>'0', 'desc' => '安卓无法获得GOOGLE身份标识UId根据邮箱验证'),
                'device_id' => array('name' => 'device_id', 'type' => 'string', 'default'=>'', 'desc'=>'device_id'),
                'as_id' => array('name' => 'as_id', 'type' => 'string', 'default'=>'', 'desc'=>'ad id'),
                'device' => array('name' => 'device', 'type' => 'string', 'default'=>'', 'desc'=>'device'),
               // 'source' => array('name' => 'source', 'type' => 'string',  'default'=>'pc', 'desc' => '来源设备'),
            ),
            
            'getCode' => array(
                'phone' => array('name' => 'phone', 'type' => 'string', 'min' => 1, 'require' => true,  'desc' => '手机号'),
                'sign' => array('name' => 'sign', 'type' => 'string',  'default'=>'', 'desc' => '签名'),
                'lan' => array('name' => 'lan', 'type' => 'string',  'default'=>'', 'desc' => ''),
            ),
            
            'getForgetCode' => array(
                'mobile' => array('name' => 'mobile', 'type' => 'string', 'min' => 1, 'require' => true,  'desc' => '手机号'),
                'sign' => array('name' => 'sign', 'type' => 'string',  'default'=>'', 'desc' => '签名'),
            ),
            'getUnionid' => array(
                'code' => array('name' => 'code', 'type' => 'string','desc' => '微信code'),
            ),

            'logout' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户Token'),
            ),

            'upUserPush'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
                'pushid' => array('name' => 'pushid', 'type' => 'string', 'desc' => '腾讯Token'),
                'terminal' => array('name' => 'terminal', 'type' => 'string', 'desc' => '设备类型'),
            ),

            'getCancelCondition'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户Token'),
            ),

            'cancelAccount'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户Token'),
                'time' => array('name' => 'time', 'type' => 'string', 'desc' => '时间戳'),
                'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名'),
            ),
            
            'getUserRecom'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
            ),
            'setPass'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户Token'),
                'pass' => array('name' => 'pass', 'type' => 'string', 'require' => true, 'desc' => '密码'),
            ),
            'sendEmail' => array(
                'email' => array('name'=>'emai','type'=>'string','require'=>true,'desc'=>'邮箱号码')
            )
        );
    }
    
    /**
     * 会员登陆 需要密码
     * @desc 用于用户登陆信息
     * @return int code 操作码，0表示成功
     * @return array info 用户信息
     * @return string info[0].id 用户ID
     * @return string info[0].user_nicename 昵称
     * @return string info[0].avatar 头像
     * @return string info[0].avatar_thumb 头像缩略图
     * @return string info[0].sex 性别
     * @return string info[0].signature 签名
     * @return string info[0].coin 用户余额
     * @return string info[0].login_type 注册类型
     * @return string info[0].level 等级
     * @return string info[0].province 省份
     * @return string info[0].city 城市
     * @return string info[0].birthday 生日
     * @return string info[0].token 用户Token
     * @return string msg 提示信息
     */
    public function appLogin() {
        $rs = array('code' => 0, 'msg' => '登录成功', 'info' => array());
        $user_login=checkNull($this->phone);
        $code=checkNull($this->code);
        $device_id=checkNull($this->device_id);
        $as_id=checkNull($this->as_id);
        $device=checkNull($this->device);
        $config = getConfigPri();
        if((!$_SESSION['reg_mobile'] || !$_SESSION['reg_mobile_code']) && $config['sendcode_switch'] == 1){
            $rs['code'] = 1001;
            switch($this->lan) {
                case 'gb':
                case 'zh':
                case 'zh-Hans':
                    $rs['msg']='请先获取验证码';
                    break;

                case 'ms':
                    $rs['msg']='Sila dapatkan Kod.';
                    break;

                default:
                    $rs['msg']='Please Request Code.';
            }
            return $rs;
        }

        if($user_login!=$_SESSION['reg_mobile']&& $config['sendcode_switch'] == 1){
            $rs['code'] = 1001;
            switch($this->lan) {
                case 'gb':
                    $rs['msg'] = '手机号码不一致';
                    break;
                case 'zh':
                case 'zh-Hans':
                    $rs['msg']='手机号码不一致';
                    break;

                case 'ms':
                    $rs['msg']='Mobile No. tidak sama.';
                    break;

                default:
                    $rs['msg']='Different mobile No.';
            }
            return $rs;
        }

        if($code!=$_SESSION['reg_mobile_code'] && $config['sendcode_switch'] == 1){
            $rs['code'] = 1002;
            switch($this->lan) {
                case 'gb':
                case 'zh':
                case 'zh-Hans':
                    $rs['msg']='验证码错误';
                    break;

                case 'ms':
                    $rs['msg']='Kod Tidak Sah.';
                    break;

                default:
                    $rs['msg']='Invalid Code.';
            }
            return $rs;
        }


        // var_dump($user_login);die;

        $domain = new Domain_Login();
        $info = $domain->userLogin($user_login,$code, $device_id, $as_id,$device,$this->lan);

        if ($info == '1001') {
            $rs['code'] = 10086;
                //禁用信息
                
             switch($this->lan) {
                case 'gb':
                case 'zh':
                case 'zh-Hans':
                    $rs['msg']='该账号未注册';
                    break;

                case 'ms':
                    $rs['msg']='Akaun ini belum didaftar';
                    break;

                default:
                    $rs['msg']='This account is not registered';
            }    
           
            return $rs;
            
            
            $info = $domain->userReg($user_login,$code, $device_id, $as_id,$device,$this->lan);
            if($info == '1') {
                $info = $domain->userLogin($user_login,$code);
            }
        }else if($info=='1002'){
                $rs['code'] = 1002;
                //禁用信息
                $baninfo=$domain->getUserban($user_login);
                $rs['info'][0] =$baninfo;
                return $rs;
        }else if($info=='1003'){
            $rs['code'] = 1003;
            $rs['msg'] = '该账号已被禁用';
            return $rs;
        }else if($info==1004){
                $rs['code'] = 1004;
                $rs['msg'] = '该账号已注销';
                return $rs;
        }
        
        
        
        
        // else {
            // if($info==1001){
            //     $rs['code'] = 1001;
            //     $rs['msg'] = '账号或密码错误';
            //     return $rs;
            // }else if($info==1002){
            //     $rs['code'] = 1002;
            //     //禁用信息
            //     $baninfo=$domain->getUserban($user_login);
            //     $rs['info'][0] =$baninfo;
            //     return $rs;
            // }else if($info==1003){
            //     $rs['code'] = 1003;
            //     $rs['msg'] = '该账号已被禁用';
            //     return $rs;
            // }else if($info==1004){
            //     $rs['code'] = 1004;
            //     $rs['msg'] = '该账号已注销';
            //     return $rs;
            // }
        // }
        // var_dump($info);
        $friLogin = $domain->getUserRecom($info['id']);
        // var_dump($friLogin);die;

        $rs['info'][0] = $info;
        $rs['info'][0]['friLogin'] = $friLogin?'0' : '1';

        return $rs;
    }
    //账号密码登录
    public function loginAccount(){
        $rs = array('code' => 0, 'msg' => '登录成功', 'info' => array());
        $user_login=checkNull($this->user_login);
        $user_pass=checkNull($this->user_pass);
        $device_id=checkNull($this->device_id);
        $as_id=checkNull($this->as_id);
        $device=checkNull($this->device);

        $domain = new Domain_Login();
        $info = $domain->loginAccount($user_login,$user_pass, $device_id, $as_id, $device);
        if($info==1001){
            $rs['code'] = 1001;
            switch($this->lan) {
                case 'gb':
                case 'zh':
                case 'zh-Hans':
                    $rs['msg']='账号或密码错误!';
                    break;

                case 'ms':
                    $rs['msg']='Username / Kata laluan Tidak Sah.';
                    break;

                default:
                    $rs['msg']='Invalid Username / Password';
            }
            return $rs;
        }


        if($info==1002){
            $rs['code'] = 1002;
            switch($this->lan) {
                case 'gb':
                case 'zh':
                case 'zh-Hans':
                    //禁用信息
                    $baninfo=$domain->getUserban($user_login);
                    $rs['info'][0] =$baninfo;
                    break;

                case 'ms':
                    $rs['msg']='Username / Akaun ini telah dilumpuhkan';
                    break;

                default:
                    $rs['msg']='Your account has been suspended';
            }
            return $rs;
        }

        if($info==1003){
            $rs['code'] = 1003;
            switch($this->lan) {
                case 'gb':
                case 'zh':
                case 'zh-Hans':
                    //禁用信息
                    $rs['msg'] = '该账号已被禁用';
                    break;

                case 'ms':
                    $rs['msg']='Username / Akaun ini telah dilumpuhkan';
                    break;

                default:
                    $rs['msg']='Your account has been suspended';
            }
            return $rs;
        }




         $rs['info'][0] = $info;
         return $rs;
    } 
    //设置密码
    public function setPass(){
        $rs = array('code' => 0, 'msg' => '设置成功', 'info' => array());
        $uid=$this->uid;
        $token=$this->token;
        $pass=$this->pass;
        $checkToken=checkToken($uid,$token);
//      if($checkToken==700){
//          $rs['code'] = $checkToken;
//          $rs['msg'] = '您的登陆状态失效，请重新登陆！';
//          return $rs;
//      }
        $check = passcheck($pass);
        if(!$check ){
            $rs['code'] = 1004;
            $rs['msg'] = '密码为6-20位字母数字组合';
            return $rs;                                     
        }
        $domain = new Domain_Login();
        $info = $domain->updatePass($uid,$pass);
        if($info==1003){
            $rs['code'] = 1003;
            $rs['msg'] = '该账户已设置了密码！';
            return $rs;
        }
        $rs['info'][0]['msg']='设置成功';
        return $rs;
    
    }
   /**
     * 会员注册
     * @desc 用于用户注册信息
     * @return int code 操作码，0表示成功
     * @return array info 用户信息
     * @return string info[0].id 用户ID
     * @return string info[0].user_nicename 昵称
     * @return string info[0].avatar 头像
     * @return string info[0].avatar_thumb 头像缩略图
     * @return string info[0].sex 性别
     * @return string info[0].signature 签名
     * @return string info[0].coin 用户余额
     * @return string info[0].login_type 注册类型
     * @return string info[0].level 等级
     * @return string info[0].province 省份
     * @return string info[0].city 城市
     * @return string info[0].birthday 生日
     * @return string info[0].token 用户Token
     * @return string msg 提示信息
     */
    public function userReg() {

        $rs = array('code' => 0, 'msg' => '注册成功', 'info' => array());
    
        $user_login=checkNull($this->user_login);
        $user_pass=checkNull($this->user_pass);
        $user_pass2=checkNull($this->user_pass2);
        $source=checkNull($this->source);
        $code=checkNull($this->code);
        $device_id=checkNull($this->device_id);
        $as_id=checkNull($this->as_id);
        $device=checkNull($this->device);
        
        if(!$_SESSION['reg_mobile'] || !$_SESSION['reg_mobile_code']){
            $rs['code'] = 1001;
            $rs['msg'] = '请先获取验证码';
            return $rs;     
        }
    
        if($user_login!=$_SESSION['reg_mobile']){
            $rs['code'] = 1001;
            $rs['msg'] = '手机号码不一致';
            return $rs;                 
        }

        if($code!=$_SESSION['reg_mobile_code']){
            $rs['code'] = 1002;
            $rs['msg'] = '验证码错误';
            return $rs;                 
        }   

        if($user_pass!=$user_pass2){
            $rs['code'] = 1003;
            $rs['msg'] = '两次输入的密码不一致';
            return $rs;                 
        }   
        
        $check = passcheck($user_pass);

        if(!$check){
            $rs['code'] = 1004;
            $rs['msg'] = '密码为6-20位字母数字组合';
            return $rs;                                     
        }
        
        $domain = new Domain_Login();
        $info = $domain->userReg($user_login,$user_pass,$source,$device_id,$as_id,$device);

        if($info==1006){
            $rs['code'] = 1006;
            $rs['msg'] = '该手机号已被注册！';
            return $rs; 
        }else if($info==1007){
            $rs['code'] = 1007;
            $rs['msg'] = '注册失败，请重试';
            return $rs; 
        }

        //$rs['info'][0] = $info;
        
        $_SESSION['reg_mobile'] = '';
        $_SESSION['reg_mobile_code'] = '';
        $_SESSION['reg_mobile_expiretime'] = '';
            
        return $rs;
    }       
    /**
     * 会员找回密码
     * @desc 用于会员找回密码
     * @return int code 操作码，0表示成功，1表示验证码错误，2表示用户密码不一致,3短信手机和登录手机不一致 4、用户不存在 801 密码6-12位数字与字母
     * @return array info 
     * @return string msg 提示信息
     */
    public function userFindPass() {
        
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $user_login=checkNull($this->user_login);
        $user_pass=checkNull($this->user_pass);
        $user_pass2=checkNull($this->user_pass2);
        $code=checkNull($this->code);
        
        if(!$_SESSION['forget_mobile'] || !$_SESSION['forget_mobile_code']){
            $rs['code'] = 1001;
            $rs['msg'] = '请先获取验证码';
            return $rs;     
        }
        
        if($user_login!=$_SESSION['forget_mobile']){
            $rs['code'] = 1000;
            $rs['msg'] = '手机号码不一致';
            return $rs;                 
        }

        if($code!=$_SESSION['forget_mobile_code']){
            $rs['code'] = 1002;
            $rs['msg'] = '验证码错误';
            return $rs;                 
        }   
        

        if($user_pass!=$user_pass2){
            $rs['code'] = 1003;
            $rs['msg'] = '两次输入的密码不一致';
            return $rs;                 
        }   

        $check = passcheck($user_pass);
        if(!$check){
            $rs['code'] = 1004;
            $rs['msg'] = '密码为6-20位字母数字组合';
            return $rs;                                     
        }   

        $domain = new Domain_Login();
        $info = $domain->userFindPass($user_login,$user_pass);  
        
        if($info==1006){
            $rs['code'] = 1006;
            $rs['msg'] = '该帐号不存在';
            return $rs; 
        }else if($info===false){
            $rs['code'] = 1007;
            $rs['msg'] = '重置失败，请重试';
            return $rs; 
        }
        
        $_SESSION['forget_mobile'] = '';
        $_SESSION['forget_mobile_code'] = '';
        $_SESSION['forget_mobile_expiretime'] = '';

        return $rs;
    }

     /**
     * 华为ID登录
     * @desc 用于用户登陆信息
     * @return int code 操作码，0表示成功
     * @return array info 用户信息
     * @return string info[0].id 用户ID
     * @return string info[0].user_nicename 昵称
     * @return string info[0].avatar 头像
     * @return string info[0].avatar_thumb 头像缩略图
     * @return string info[0].sex 性别
     * @return string info[0].signature 签名
     * @return string info[0].coin 用户余额
     * @return string info[0].login_type 注册类型
     * @return string info[0].level 等级
     * @return string info[0].province 省份
     * @return string info[0].city 城市
     * @return string info[0].birthday 生日
     * @return string info[0].token 用户Token
     * @return string msg 提示信息
     */
    public function userLoginByHuaWei() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $openid=checkNull($this->openid);
        $email=checkNull($this->email);
        $nicename=checkNull($this->nicename);
        $avatar=checkNull($this->avatar);
        $device_id=checkNull($this->device_id);
        $as_id=checkNull($this->as_id);
        $device=checkNull($this->device);
    //  $source=checkNull($this->source);
    //  $sign=checkNull($this->sign);
        
        
//         $checkdata=array(
//             'openid'=>$openid
//         );
        
//         $issign=checkSign($checkdata,$sign);
//         if(!$issign){
//             $rs['code']=1001;
//          $rs['msg']='签名错误';
//          return $rs; 
//         }
        
        
        
        $domain = new Domain_Login();
       // $info = $domain->userLoginThird($openid,$nicename,$avatar,$email);
        if($this->mark==1){//安卓
            $info = $domain->userLoginHuaWei($openid,$nicename,$avatar,$email,$device_id,$as_id,$device);
        }
        if($info==1002){
            $rs['code'] = 1002;
            //禁用信息
            $baninfo=$domain->getThirdUserban($openid,$type);
            $rs['info'][0] =$baninfo;
            return $rs;                 
        }else if($info==1003){
            $rs['code'] = 1003;
            $rs['msg'] = '该账号已被禁用';
            return $rs; 
        }else if($info==1004){
            $rs['code'] = 1004;
            $rs['msg'] = '该账号已注销';
            return $rs; 
        }

        $rs['info'][0] = $info;
        

        return $rs;
    }
    
    /**
     * 第三方登录
     * @desc 用于用户登陆信息
     * @return int code 操作码，0表示成功
     * @return array info 用户信息
     * @return string info[0].id 用户ID
     * @return string info[0].user_nicename 昵称
     * @return string info[0].avatar 头像
     * @return string info[0].avatar_thumb 头像缩略图
     * @return string info[0].sex 性别
     * @return string info[0].signature 签名
     * @return string info[0].coin 用户余额
     * @return string info[0].login_type 注册类型
     * @return string info[0].level 等级
     * @return string info[0].province 省份
     * @return string info[0].city 城市
     * @return string info[0].birthday 生日
     * @return string info[0].token 用户Token
     * @return string msg 提示信息
     */
    public function userLoginByThird() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $openid=checkNull($this->openid);
        $email=checkNull($this->email);
        $nicename=checkNull($this->nicename);
        $avatar=checkNull($this->avatar);
        $device_id=checkNull($this->device_id);
        $as_id=checkNull($this->as_id);
        $device=checkNull($this->device);
    //  $source=checkNull($this->source);
    //  $sign=checkNull($this->sign);
        
        
//         $checkdata=array(
//             'openid'=>$openid
//         );
        
//         $issign=checkSign($checkdata,$sign);
//         if(!$issign){
//             $rs['code']=1001;
//          $rs['msg']='签名错误';
//          return $rs; 
//         }
        
        
        
        $domain = new Domain_Login();
       // $info = $domain->userLoginThird($openid,$nicename,$avatar,$email);
        if($this->mark==1){//安卓
            $info = $domain->userLoginThirdAndroid($openid,$nicename,$avatar,$email,$device_id,$as_id,$device);
        }else{
            $info = $domain->userLoginThird($openid,$nicename,$avatar,$email,$device_id,$as_id,$device);
            $info['user_pass'] = 1;
        }
        if($info==1002){
            $rs['code'] = 1002;
            //禁用信息
            $baninfo=$domain->getThirdUserban($openid,$type);
            $rs['info'][0] =$baninfo;
            return $rs;                 
        }else if($info==1003){
            $rs['code'] = 1003;
            $rs['msg'] = '该账号已被禁用';
            return $rs; 
        }else if($info==1004){
            $rs['code'] = 1004;
            $rs['msg'] = '该账号已注销';
            return $rs; 
        }

        $rs['info'][0] = $info;
        

        return $rs;
    }
    
    /**
     * 获取注册短信验证码
     * @desc 用于注册获取短信验证码
     * @return int code 操作码，0表示成功,2发送失败
     * @return array info 
     * @return string msg 提示信息
     */
     
    public function getCode() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $phone = checkNull($this->phone);
        $sign = checkNull($this->sign);
        
//      $ismobile=checkMobile($mobile);
//      if(!$ismobile){
//          $rs['code']=1001;
//          $rs['msg']='请输入正确的手机号';
//          return $rs; 
//      }
        
        $checkdata=array(
            'phone'=>$phone
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
            $rs['msg']='签名错误';
            return $rs; 
        }
        
//      return $issign;die;
        
        $where="user_login='{$phone}'";
        
//      $checkuser = checkUser($where);
        
//        if($checkuser){
//            $rs['code']=1004;
//          $rs['msg']='该手机号已注册';
//          return $rs;
//        }

       /* if($_SESSION['reg_mobile']==$phone && $_SESSION['reg_mobile_expiretime']> time() ){
            $rs['code']=1002;
            switch($this->lan) {
                case 'gb':
                case 'zh':
                case 'zh-Hans':
                    $rs['msg']='验证码5分钟有效，请勿多次发送';
                    break;

                case 'ms':
                    $rs['msg']='SMS Kod sah dalam 5mins.';
                    break;

                default:
                    $rs['msg']='Code is valid for 5mins.';
            }
            return $rs;
        }*/
    
        $limit = ip_limit();    
        if( $limit == 1){
            $rs['code']=1003;
            switch($this->lan) {
                case 'gb':
                case 'zh':
                case 'zh-Hans':
                    $rs['msg']='您已当日发送次数过多';
                    break;

                case 'ms':
                    $rs['msg']='Anda telah mencapai percubaan maksimum yang dibenarkan.';
                    break;

                default:
                    $rs['msg']="You've reached your max attempt allowed.";
            }
            return $rs;
        }       
        
        if(isset($_SESSION['reg_mobile_code']) && !empty($_SESSION['reg_mobile_code']) && $_SESSION['reg_mobile']==$phone) {
            $mobile_code = $_SESSION['reg_mobile_code'];
        }else{
            $mobile_code = random(6,1);
        }
        if(!DI()->notorm->user->where('mobile =? or user_login =?',$phone,$phone)->count()){
            $rs['code'] = 10086;
                //禁用信息
                
             switch($this->lan) {
                case 'gb':
                case 'zh':
                case 'zh-Hans':
                    $rs['msg']='该账号未注册';
                    break;

                case 'ms':
                    $rs['msg']='Akaun ini belum didaftar';
                    break;

                default:
                    $rs['msg']='This account is not registered';
            }    
           
            return $rs;
        }
        /* 发送验证码 */
        $result=sendCode($phone,$mobile_code);

        if($result['code']==0){
            $_SESSION['reg_mobile'] = $phone;
            $_SESSION['reg_mobile_code'] = $mobile_code;
            $_SESSION['reg_mobile_expiretime'] = time() +60*5;
        }else if($result['code']==667){
            $_SESSION['reg_mobile'] = $phone;
            $_SESSION['reg_mobile_code'] = $result['msg'];
            $_SESSION['reg_mobile_expiretime'] = time() +60*5;
            
            $rs['code']=0;
            $rs['msg']='验证码为：'.$result['msg'];
        }else{
            $rs['code']=1002;
            $rs['msg']=$result['msg']?$result['msg']:'发送失败,未知错误';
            $_SESSION['reg_mobile'] = '';
            $_SESSION['reg_mobile_expiretime'] = time();
        } 
        
        return $rs;
    }       

    /**
     * 获取找回密码短信验证码
     * @desc 用于找回密码获取短信验证码
     * @return int code 操作码，0表示成功,2发送失败
     * @return array info 
     * @return string msg 提示信息
     */
     
    public function getForgetCode() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $mobile = checkNull($this->mobile);
        $sign = checkNull($this->sign);
        
//      $ismobile=checkMobile($mobile);
//      if(!$ismobile){
//          $rs['code']=1001;
//          $rs['msg']='请输入正确的手机号';
//          return $rs; 
//      }
        
        $checkdata=array(
            'mobile'=>$mobile
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
            $rs['msg']='签名错误';
            return $rs; 
        }
        
        $where="user_login='{$mobile}'";
        $checkuser = checkUser($where); 
        
        if(!$checkuser){
            $rs['code']=1004;
            $rs['msg']='该手机号未注册';
            return $rs;
        }

        //判断手机号是否注销
        $is_destroy=checkIsDestroyByLogin($mobile);
        if($is_destroy){
            $rs['code']=1005;
            $rs['msg']='该手机号已注销';
            return $rs;
        }

        if($_SESSION['forget_mobile']==$mobile && $_SESSION['forget_mobile_expiretime']> time() ){
            $rs['code']=1002;
            $rs['msg']='验证码5分钟有效，请勿多次发送';
            return $rs;
        }

        $limit = ip_limit();    
        if( $limit == 1){
            $rs['code']=1003;
            $rs['msg']='您已当日发送次数过多';
            return $rs;
        }   
        $mobile_code = random(6,1);
        
        /* 发送验证码 */
        $result=sendCode($mobile,$mobile_code);
        if($result['code']==0){
            $_SESSION['forget_mobile'] = $mobile;
            $_SESSION['forget_mobile_code'] = $mobile_code;
            $_SESSION['forget_mobile_expiretime'] = time() +60*5;   
        }else if($result['code']==667){
            $_SESSION['forget_mobile'] = $mobile;
            $_SESSION['forget_mobile_code'] = $result['msg'];
            $_SESSION['forget_mobile_expiretime'] = time() +60*5;
            
            $rs['code']=1002;
            $rs['msg']='验证码为：'.$result['msg'];
        }else{
            $rs['code']=1002;
            $rs['msg']=$result['msg'];
        } 
        
        return $rs;
    }   
    
    /**
     * 获取微信登录unionid
     * @desc 用于获取微信登录unionid
     * @return int code 操作码，0表示成功,2发送失败
     * @return array info 
     * @return string info[0].unionid 微信unionid
     * @return string msg 提示信息
     */    
    public function getUnionid(){
        
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $code=checkNull($this->code);
        
        if($code==''){
            $rs['code']=1001;
            $rs['msg']='参数错误';
            return $rs;
            
        }

        //$configpri=getConfigPri();
    
        //$AppID = $configpri['login_wx_appid'];
        //$AppSecret = $configpri['login_wx_appsecret'];
        $AppID = 'wxbee8d98b9852d612';
        $AppSecret = 'f9d4f74d9412691eeb271dc7632f24b6';
        /* 获取token */
        //$url="https://api.weixin.qq.com/sns/oauth2/access_token?appid={$AppID}&secret={$AppSecret}&code={$code}&grant_type=authorization_code";
        $url="https://api.weixin.qq.com/sns/jscode2session?appid={$AppID}&secret={$AppSecret}&js_code={$code}&grant_type=authorization_code";
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_URL, $url);
        $json =  curl_exec($ch);
        curl_close($ch);
        $arr=json_decode($json,1);
        //file_put_contents('./getUnionid.txt',date('Y-m-d H:i:s').' 提交参数信息 code:'.json_encode($code)."\r\n",FILE_APPEND);
        //file_put_contents('./getUnionid.txt',date('Y-m-d H:i:s').' 提交参数信息 arr:'.json_encode($arr)."\r\n",FILE_APPEND);
        if($arr['errcode']){
            $rs['code']=1003;
            $rs['msg']='配置错误';
            //file_put_contents('./getUnionid.txt',date('Y-m-d H:i:s').' 提交参数信息 arr:'.json_encode($arr)."\r\n",FILE_APPEND);
            return $rs;
        }
        
        

        /* 小程序 绑定到 开放平台 才有 unionid  否则 用 openid  */
        $unionid=$arr['unionid'];

        if(!$unionid){
            //$rs['code']=1002;
            //$rs['msg']='公众号未绑定到开放平台';
            //return $rs;
            
            $unionid=$arr['openid'];
        }
        
        $rs['info'][0]['unionid'] = $unionid;
        return $rs;
    }
    
    /**
     * 退出
     * @desc 用于用户退出 注销极光
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string msg 提示信息
     */
    public function logout() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid = $this->uid;
        $token=checkNull($this->token);
        
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        $domain = new Domain_Login(); 
        $res = $domain->userLogout($uid);
        //$info = userLogout($uid);
        $rs['info'][0]['msg'] = '退出成功';

        return $rs;         
    }


    /**
     * 更新极光pushid
     * @desc 用于更新极光pushid
     * @return int code 状态码，0表示成功
     * @return string msg 提示信息
     * @return array info 返回信息
     */
    public function upUserPush(){

        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $pushid=checkNull($this->pushid);
        $terminal=checkNull($this->terminal);
        
        
        $domain=new Domain_Login();
        $res = $domain->upUserPush($uid,$pushid,$terminal);
        
        if($res == 1) {
            $rs['msg'] = '更新成功!';
        }else {
            $rs['msg'] = '更新失败!';
        }
        
        return $rs;
        
    }

    /**
     * 获取注销账号的条件
     * @desc 用于获取注销账号的条件
     * @return int code 状态码，0表示成功
     * @return string msg 提示信息
     * @return array info 返回信息
     * @return array info[0]['list'] 条件数组
     * @return string info[0]['list'][]['title'] 标题
     * @return string info[0]['list'][]['content'] 内容
     * @return string info[0]['list'][]['is_ok'] 是否满足条件 0 否 1 是
     * @return string info[0]['can_cancel'] 是否可以注销账号 0 否 1 是
     */
    public function getCancelCondition(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        $domain=new Domain_Login();
        $res=$domain->getCancelCondition($uid);

        $rs['info'][0]=$res;

        return $rs;
    }

    /**
     * 用户注销账号
     * @desc 用于用户注销账号
     * @return int code 状态码,0表示成功
     * @return string msg 返回提示信息
     * @return array info 返回信息
     */
    public function cancelAccount(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $time=checkNull($this->time);
        $sign=checkNull($this->sign);

        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }

        if(!$time||!$sign){
            $rs['code'] = 1001;
            $rs['msg'] = '参数错误';
            return $rs;
        }

        $now=time();
        if($now-$time>300){
            $rs['code']=1001;
            $rs['msg']='参数错误';
            return $rs;
        }

        
        $checkdata=array(
            'uid'=>$uid,
            'token'=>$token,
            'time'=>$time
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1001;
            $rs['msg']='签名错误';
            return $rs; 
        }

        $domain=new Domain_Login();
        $res=$domain->cancelAccount($uid);

        if($res==1001){
            $rs['code']=1001;
            $rs['msg']='相关内容不符合注销账号条件';
            return $rs;
        }

        $rs['msg']='注销成功,手机号、身份证号等信息已解除';
        return $rs;
    }


    public function updateEmptyAgentCode() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain=new Domain_Login();
        $res=$domain->updateEmptyAgentCode();
        
        return $rs;
    }

}
