<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/27 0027
 * Time: 11:45
 */
class Api_Test extends PhalApi_Api {
    public function getRules() {
           return array(
            //   'test'=>array(
            //     'custom' => array('name' => 'custom', 'type' => 'string', 'require' => true,'desc' => '帖子id'),
               
            //  ),
             'push'=>array(
                'title' => array('name' => 'title', 'type' => 'string', 'require' => true,'desc' => ''),
                'content' => array('name' => 'content', 'type' => 'string', 'require' => true,'desc' => ''),
                'uid' => array('name' => 'uid', 'type' => 'string', 'require' => true,'desc' => ''),
                'type' => array('name' => 'type', 'type' => 'string', 'require' => true,'desc' => ''),
                'body' => array('name' => 'body', 'type' => 'string', 'desc' => ''),
              ),
              
              'push2'=>array(
                'title' => array('name' => 'title', 'type' => 'string', 'require' => true,'desc' => ''),
                'content' => array('name' => 'content', 'type' => 'string', 'require' => true,'desc' => ''),
                'uid' => array('name' => 'uid', 'type' => 'string', 'require' => true,'desc' => ''),
                'type' => array('name' => 'type', 'type' => 'string', 'require' => true,'desc' => ''),
                'body' => array('name' => 'body', 'type' => 'string', 'desc' => ''),
              ),
               'faker_user_visit' => [
                   'uid' => ['name'=>'uid','type'=>'int','require'=>true,'desc'=>'帖子主人id'],
                   'num' => ['name'=>'num','type'=>'int','require'=>true,'desc'=>'模拟多少人来读帖子数量']
               ]
            );
        
    }

    public function faker_user_visit(){
        $i = 0; //最终有效读帖子访问量
        $uid  = $this->uid;
        $num = $this->num;
        $list = DI()->notorm->dynamic->where('uid',$uid)->fetchAll(); //帖主 所有的帖子
        foreach ($list as $k=>$v){
            $users = DI()->notorm->user->where('user_status = 1')->order('id desc')->limit(0,$num)->fetchAll();
            foreach ($users as $uk=>$uv){
                //https://ap1.askpert.com/appapi/?service=publish.detailsNote&id=1808&uid=41159
                $ret = DI()->notorm->invitation_visit->where('user_id = '.$uv['id'])->where('note_id = '.$v['id'])->count();
                if($ret){
                    continue;
                }
                $url = 'https://ap1.askpert.com/appapi/?service=publish.detailsNote&id='.$v['id'].'&uid='.$uv['id'];
                $data = file_get_contents($url);
                $data = json_decode($data,1);
                if($data['ret'] == 200)
                $i++;
            }
        }
        echo $i;die;
    }

    /**
     * test
     */
    public function getUser(){
        
        
     		addMoney(40521,[
    		    'title'=>'回答问卷',
    		    'money'=>100,
    		    ]);
             $list = getShopClass();
             print_r($list);exit;
       	$config = sendCode('13580412556','115042');
       	print_r($config);exit;
    	$res = DI()->ucloud->get('config');
        return $res;


    }
    public function test(){
        $rs = array('code' => 0, 'msg' => '获取成功', 'info' => array());
        $domain = new Domain_Area();
        $r = $domain->getListTwo();
        if(!$r){
            return $rs;
        }
        $rs['info'][0]['list'] = $r;
        return $rs;
    }
    
    
    
    //推送测试
    public function push(){
        $res = TPNSPush($this->title, $this->content,$this->uid, $this->type, $this->body); 
        file_put_contents(API_ROOT.'/../record/yun.txt',json_encode($res)."\r\n",FILE_APPEND);
        return $res;
    }
    
    public function push2(){
        $res = TPNSPush($this->title, $this->content,$this->uid, $this->type, $this->body); 
        file_put_contents(API_ROOT.'/../record/yun2.txt',json_encode($res)."\r\n",FILE_APPEND);
        return $res;
    }
    

}