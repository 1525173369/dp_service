<?php
/**
 * 系统消息
 */
class Api_Message extends PhalApi_Api {

	public function getRules() {
		return array(
			'getList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户Token'),
				'p' => array('name' => 'p', 'type' => 'int','default'=>1, 'desc' => '页码'),
			),
			'getNewMsg' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户Token'),
			),

			'getShopOrderList' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户Token'),
				'p' => array('name' => 'p', 'type' => 'int','default'=>1, 'desc' => '页码'),
			),
			'getCollect' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户Token'),
				'p' => array('name' => 'p', 'type' => 'int','default'=>1, 'desc' => '页码'),
			),
			'getComment' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户Token'),
				'p' => array('name' => 'p', 'type' => 'int','default'=>1, 'desc' => '页码'),
			),
			'getLikes' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户Token'),
				'p' => array('name' => 'p', 'type' => 'int','default'=>1, 'desc' => '页码'),
			),
			
			'getSubscribe' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户Token'),
				'p' => array('name' => 'p', 'type' => 'int','default'=>1, 'desc' => '页码'),
			),

		);
	}
	
	/**
	 * 系统消息
	 * @desc 用于 获取系统消息
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0] 支付信息
	 * @return string msg 提示信息
	 */
	public function getList() {
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$token=checkNull($this->token);
		$p=checkNull($this->p);
        
        if($p<1){
			$p=1;
		}
        
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
		
		$domain = new Domain_Message();
		$list = $domain->information($uid,$p);
		//$rs['info'][0]['new']=$domain->getNew($uid);
		$rs['info'][0]['list']=$list;
		return $rs;			
	}

    public function getNewMsg(){
    	$rs = array('code' => 0, 'msg' => '', 'info' => array());
		$token=checkNull($this->token);
		$uid=$this->uid;
        $checkToken=checkToken($uid,$token);
	/*	if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}*/
		$domain = new Domain_Message();
	    $rs['info'][0]['new']=$domain->getNew($uid);
        return $rs;	
    }
	/**
	 * 获取店铺订单列表
	 * @desc 用于 获取店铺订单列表
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0] 支付信息
	 * @return string msg 提示信息
	 */
	public function getShopOrderList(){
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		
		$uid=$this->uid;
		$token=checkNull($this->token);
		$p=checkNull($this->p);
        
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
		
		$domain = new Domain_Message();
		$res = $domain->getShopOrderList($uid,$p);
		$rs['info']=$res;
		return $rs;
	}
	
	/**
     * 获取收藏提醒消息
     */
	public function getCollect(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=$this->uid;
        $token=checkNull($this->token);
        $p=checkNull($this->p);
        if($p<1){
            $p=1;
        }
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Message();
        $res = $domain->getCollect($uid,$p);
        $rs['info'][0]['list']=$res;
        return $rs;

    }
    
    /**
     * 获取评论消息提醒
     */
    public function getComment(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=$this->uid;
        $token=checkNull($this->token);
        $p=checkNull($this->p);
        if($p<1){
            $p=1;
        }
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Message();
        $res = $domain->getComment($uid,$p);
        $rs['info'][0]['list']=$res;
        return $rs;

    }
    
    /**
     * 获取评论消息提醒
     */
    public function getLikes(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=$this->uid;
        $token=checkNull($this->token);
        $p=checkNull($this->p);
        if($p<1){
            $p=1;
        }
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Message();
        $res = $domain->getLikes($uid,$p);
        $rs['info'][0]['list']=$res;
        return $rs;

    }
    
    /**
     * 获取订阅消息提醒
     */
    public function getSubscribe(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=$this->uid;
        $token=checkNull($this->token);
        $p=checkNull($this->p);
        if($p<1){
            $p=1;
        }
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Message();
        $res = $domain->getSubscribe($uid,$p);
        $rs['info'][0]['list']=$res;
        return $rs;

    }
}
