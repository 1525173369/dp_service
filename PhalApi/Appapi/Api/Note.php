<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/15 0015
 * Time: 11:38
 */
class Api_Note extends PhalApi_Api {

    public function getRules() {
        return array(
            'addLike'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                't_id' => array('name' => 't_id', 'type' => 'string','require' => true,'desc' => '帖子ID'),
                 'lan'=>array('name' => 'lan', 'type' => 'int', 'default' => '0', 'desc' => '0中文1英文2马来语'),
            ),
            'historySearch'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
            ),
            'clearHistory'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
            ),
    
        );



    }

	public function addLike() {
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$t_id=checkNull($this->t_id);
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        $checkdata=array(
            'uid'=>$uid,
            'note_id'=>$t_id,
            'add_time'=>time(),
        );
        $domain = new Domain_Publish();
        $result = $domain->addNoteLike($checkdata,$this->lan);
		if($result==1001){
			$rs['code'] = 1001;
			$rs['msg'] = "帖子不存在";
			return $rs;
		}else if($result==1002){
			$rs['code'] = 1002;
			$rs['msg'] = "不能给自己点赞";
			return $rs;
		}
		$rs['info'][0]['num']=$result;
        return $rs;
    }	
    //热门搜索
    public function hotSearch(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $domain = new Domain_Search();
        $r = $domain->getHot();
        $rs['info'][0]['list']=$r;
        return $rs;
    }
    
    public function historySearch(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
		// echo $uid;exit;
        $domain = new Domain_Search();
        //echo 11;exit;
        $r = $domain->getHistory($uid);
        $rs['info'][0]['list']=$r;
        return $rs;
    }
    
    public function clearHistory(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        $domain = new Domain_Search();
        $r = $domain->clearHistory($uid);
        $rs['info'][0]['msg']='删除成功';
        return $rs;
    }
    
    
}