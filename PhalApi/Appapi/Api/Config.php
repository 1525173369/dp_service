<?php
/**
 * 公共配置
 */

class Api_Config extends PhalApi_Api {

	public function getRules() {
		return array(
			'getConfig' => array(
				
			),
		);
	}
	
	/*
	获取配置
	*/
	public function getConfig(){
	    $rs = array('code' => 0, 'msg' => '', 'config' => array());
        $configpub=DI()->notorm->options
	        ->where("option_name='configpub'")
            ->select('option_value')
            ->fetchOne();
        $configpub_notive = json_decode($configpub['option_value'],true);
        $config_list = [
                'region'=>explode(',',trim($configpub_notive['region'],',')),//国家地区
            ];
        $rs['config'] = $config_list;
        return $rs;
	}
	
	public function phone_prefix(){
	    $rs = array('code' => 0, 'msg' => '获取成功', 'region' => array());
        $configpub=DI()->notorm->phone_prefix
            ->select('country,prefix')
            ->fetchAll();
        $rs['region'] = $configpub;
        return $rs;    
	}
	
// 	public function vipLevel(){
// 	    $key = "60134958501";
// 	    $secret = "66300";
// 	    self::sendCode($key,$secret);
// 	}
	
	public function sendCode($mobile,$code){
        $rs=array();
		$config = getConfigPri();
		
        if(!$config['sendcode_switch']){
            $rs['code']=667;
			$rs['msg']='123456';
            return $rs;
        }
        
		/* 短信宝 */
		$statusStr = array(
		"0" => "短信发送成功",
		"-1" => "参数不全",
		"-2" => "服务器空间不支持,请确认支持curl或者fsocket，联系您的空间商解决或者更换空间！",
		"30" => "密码错误",
		"40" => "账号不存在",
		"41" => "余额不足",
		"42" => "帐户已过期",
		"43" => "IP地址限制",
		"50" => "内容含有敏感词"
		);
// 		$smsapi = "http://api.smsbao.com/";
// 		$user = "shocklive888"; //短信平台帐号javascript:;
// 		$pass = md5("Aa066512"); //短信平台密码
		$content="您的验证码是" . $code ;//要发送的短信内容
// 		$phone = $mobile;//要发送短信的手机号码
// 		$sendurl = $smsapi."sms?u=".$user."&p=".$pass."&m=".$phone."&c=".urlencode($content);
		
		
		$destination = $mobile;
        $message = $content;
        // $message = html_entity_decode($message, ENT_QUOTES, 'utf-8'); 
        $message = $message;
        
        $username = "mylive_mike88";
        $password = "XUAe6c8Rew9yGwZ";
        $type = 2;
        $sender_id = "66300";
        $fp = "https://www.isms.com.my/isms_send.php";
        $fp .= "?un=$username&pwd=$password&dstno=$destination&msg=$message&type=$type&sendid=$sender_id&agreedterm=YES";
        // echo $fp;die;
        $result = self::ismscURL($fp);
// 		$result =file_get_contents($fp);
		if($result == 0){
            setSendcode(array('type'=>'1','account'=>$mobile,'content'=>$content));
			$rs['code']=0;
		}else{
			$rs['code']=1002;
			$rs['msg']="$statusStr[$result]";
		}
		return $rs;
	}
	
	public function ismscURL($link){                            //显示获得的数据
        $http = curl_init($link);
        curl_setopt($http, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($http, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($http, CURLOPT_SSL_VERIFYHOST, FALSE);
        $http_result = curl_exec($http);
        $http_status = curl_getinfo($http, CURLINFO_HTTP_CODE);
        curl_close($http);
        return $http_result;
    }
}	