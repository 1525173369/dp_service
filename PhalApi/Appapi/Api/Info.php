<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/27 0027
 * Time: 16:30
 */
header('Access-Control-Allow-Origin: *');
if($_SERVER['REQUEST_METHOD']=='OPTIONS' or $_SERVER['REQUEST_METHOD']=='options'){
     exit();
}
session_start();
class Api_Info extends PhalApi_Api {
    public function getRules() {
        return array(
            'getInfo'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
            ),
            'attestation'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'real_name' => array('name' => 'real_name', 'type' => 'string', 'require' => true, 'desc' => '身份号码'),
                'cer_no' => array('name' => 'cer_no', 'type' => 'string', 'require' => true, 'desc' => '姓名'),
                'front_view' => array('name' => 'front_view', 'type' => 'string', 'require' => true, 'desc' => '身份证正面'),
                'back_view' => array('name' => 'back_view', 'type' => 'string', 'require' => true, 'desc' => '身份证反面'),
            ),
            'getList' => array(
                'keywords' => array('name' => 'keywords', 'type' => 'string', 'desc' => '关键词'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
            ),
            'updateUser' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'avatar' => array('name' => 'avatar', 'type' => 'string',  'desc' => '头像'),
                'user_nickname' => array('name' => 'user_nickname','require' => true, 'type' => 'string',  'desc' => '昵称'),
                'sex' => array('name' => 'sex', 'type' => 'string',  'desc' => '01性别'),
                'mobile' => array('name' => 'mobile', 'type' => 'string',  'desc' => '手机'),
                'user_email' => array('name' => 'user_email', 'type' => 'string', 'require' => true, 'desc' => '邮箱'),
                'signature' => array('name' => 'signature', 'type' => 'string', 'require' => true, 'desc' => '个人介绍'),
                'address' => array('name' => 'address', 'type' => 'string',  'desc' => '地址'),
                'birthday' => array('name' => 'birthday', 'type' => 'string', 'require' => true, 'desc' => '生日日期'),
            ),
            'getProfit' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'status' => array('name' => 'status', 'type' => 'string','default'=>'0', 'desc' => '0支出1收入，状态'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
                'lan' =>array('name' => 'lan', 'type' => 'string', 'default'=>'gb' ,'desc' => '语言'),
            ),
            'postContribution'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true,'desc' => '用户token'),
                'name' => array('name' => 'name', 'type' => 'string','require' => true, 'desc' => '店铺名'),
                'thumb' => array('name' => 'thumb', 'type' => 'string', 'require' => true,'desc' => '店铺主图'),
                'classid' => array('name' => 'classid', 'type' => 'string', 'desc' => '经营类目'),
                'link' => array('name' => 'link', 'type' => 'string', 'default'=>'','desc' => '官网链接'),
//                'province' => array('name' => 'province', 'type' => 'string', 'desc' => '省份'),
//                'city' => array('name' => 'city', 'type' => 'string', 'desc' => '市'),
//                'area' => array('name' => 'area', 'type' => 'string', 'desc' => '地区'),
//                'address' => array('name' => 'address', 'type' => 'string', 'desc' => '详细地址'),
//                'service_phone' => array('name' => 'service_phone', 'type' => 'string', 'desc' => '电话'),
//                'open_time' => array('name' => 'open_time', 'type' => 'string', 'desc' => '营业开始时间'),
//                'end_time' => array('name' => 'end_time', 'type' => 'string', 'desc' => '营业结束时间'),
//                'business_status' => array('name' => 'business_status', 'type' => 'int', 'desc' => '营业状态'),
//                'lng' => array('name' => 'lng', 'type' => 'string', 'desc' => '经度'),
//                'lat' => array('name' => 'lat', 'type' => 'string', 'desc' => '纬度'),

            ),
           'dedicateList' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' ,'desc' => '页数'),
            ),
            'isAuth'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
            ),
           'shopAuth'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id', 'type' => 'string', 'require' => true, 'desc' => '店铺ID'),
            ),
            'authentication' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'shop_id' => array('name' => 'shop_id', 'type' => 'int','require' => true,'desc' => '店铺ID'),
                'name' => array('name' => 'name', 'type' => 'string','require' => true, 'desc' => '店铺名字'),
                'username' => array('name' => 'username', 'type' => 'string','require' => true,'desc' => '法人姓名'),
                'phone' => array('name' => 'phone', 'type' => 'string','require' => true,'desc' => '法人号码'),
                'code' => array('name' => 'code', 'type' => 'string','require' => true,'desc' => '验证码'),
                'cardno' => array('name' => 'cardno', 'type' => 'string','require' => true, 'desc' => '法人身份证号码'),
                'province' => array('name' => 'province', 'type' => 'string','desc' => '省'),
                'city' => array('name' => 'city', 'type' => 'string','desc' => '市'),
                'area' => array('name' => 'area', 'type' => 'string','desc' => '区'),
                'address' => array('name' => 'address', 'type' => 'string', 'desc' => '详细地址'),
                'card_front' => array('name' => 'card_front', 'type' => 'string','require' => true, 'desc' => '身份证正面'),
                'card_back' => array('name' => 'card_back', 'type' => 'string','require' => true, 'desc' => '身份证反面'),
                'certificate' => array('name' => 'certificate', 'type' => 'string','require' => true, 'desc' => '营业执照'),
                // 'lng' => array('name' => 'lng', 'type' => 'string','require' => true, 'desc' => '经度'),
                // 'lat' => array('name' => 'lat', 'type' => 'string','require' => true, 'desc' => '纬度'),
            ),
            'index'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
            ),
            'visit'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string',  'desc' => '用户token'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' , 'desc' => '页数'),
                'touid'=>array('name' => 'touid', 'type' => 'int',  'desc' => '查看别人信息'),
            ),
            'myNote'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int',  'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' , 'desc' => '页数'),
                'touid'=>array('name' => 'touid', 'type' => 'int', 'default'=>'0', 'desc' => '查看别人信息'),
            ),
           'getMyComment'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int',  'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string',  'desc' => '用户token'),
                'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' , 'desc' => '页数'),
                 'touid'=>array('name' => 'touid', 'type' => 'int',  'desc' => '查看别人信息'),
            ),
            'myRelated'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int',  'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string',  'desc' => '用户token'),
                 'touid'=>array('name' => 'touid', 'type' => 'int',  'desc' => '查看别人信息'),
            ),
			'smsCode' => array(
				'mobile' => array('name' => 'mobile', 'type' => 'string', 'min' => 1, 'require' => true,  'desc' => '手机号'),
			),
			'report' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'type'=>array('name' => 'type', 'type' => 'int', 'default'=>'0' ,'desc' => '0店铺举报1帖子举报'),
                'report_id'=>array('name' => 'report_id', 'type' => 'int','require' => true, 'desc' => '帖子ID或者店铺ID'),
                'content'=>array('name' => 'content', 'type' => 'string','require' => true, 'desc' => '举报内容'),
                'description'=>array('name' => 'description', 'type' => 'string','require' => true, 'desc' => '举报详情'),
                'thumb'=>array('name' => 'thumb', 'type' => 'string', 'desc' => '图片地址集合'),
			),
			'updateBackground' => array(
			    'uid' => array('name' => 'uid', 'type' => 'int',  'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户token'),
                'background'=>array('name' => 'background', 'type' => 'string',  'require' => true, 'desc' => '背景图片'),
			),
			'getFollow'=>array(
			    'uid' => array('name' => 'uid', 'type' => 'int',  'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string',  'require' => true, 'desc' => '用户token'),
                 'p' => array('name' => 'p', 'type' => 'int', 'default'=>'1' , 'desc' => '页数'),
                 'type'=>array('name' => 'type', 'type' => 'int', 'default'=>'1' , 'desc' => '0我的粉丝1我的关注'),
                'touid'=>array('name' => 'touid', 'type' => 'int', 'require' => true,  'desc' => '查看别人信息'),
			),
			

        );

    }
    
    //获取我的粉丝
    public function getFollow(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $touid = checkNull($this->touid);
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_User();
        $r  = $domain->getFollow($touid,$this->type,$this->p);
        $rs['info'][0]['list'] = $r;
        return $rs;
        
    }
    
    

    /**
     * 获取个人主页信息
     */
    public function getInfo(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);

        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain=new Domain_User();
        $res=$domain->getInfo($uid);
        $rs['info']=$res;

        return $rs;

    }
    /**
     * 用户修改背景图
    */
    public function updateBackground(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $data = [
            'background'=>$this->background,    
        ];
        $domain=new Domain_User();
        $res=$domain->updateBackground($uid,$data);
        if($res==1002){
            $rs['code'] = 1002;
            $rs['msg'] = '修改失败';
            return $rs;
        }
        $rs['info'][0]['msg']='修改成功';

        return $rs;
    }
    //判断用户是否实名认证
    public function isAuth(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain=new Domain_Userauth();
        $r  = $domain->isAuth($uid);
        $rs['info'][0]['status']=$r;
        return $rs;
        
    }
    /**
     * 实名认证
     */
     public function attestation(){
 	    header('Access-Control-Allow-Origin: *');
         $rs = array('code' => 0, 'msg' => '提交成功', 'info' => array());
         $uid=checkNull($this->uid);
         $token=checkNull($this->token);
         $real_name=checkNull($this->real_name);
         $cer_no=checkNull($this->cer_no);
         $front_view=checkNull($this->front_view);
         $back_view=checkNull($this->back_view);
         $checkToken=checkToken($uid,$token);
        //  if($checkToken==700){
        //      $rs['code'] = $checkToken;
        //      $rs['msg'] = '您的登陆状态失效，请重新登陆！';
        //      return $rs;
        //  }
         //判断用户是否实名认证
         $isAuth=isAuth($uid);
         if($isAuth){
             $rs['code']=1001;
             $rs['msg']="你已经实名认证成功,不需提交";
             return $rs;
         }
         $domain=new Domain_Userauth();
         $r  = $domain->getInfo($uid);
         $data = [
             'real_name'=>$real_name,
             'cer_no' =>$cer_no,
             'front_view'=>$front_view,
             'back_view'=>$back_view,
             'addtime'=>time(),
         ];
         if($r){
             $domain->update($uid,$data);
         }else{
             $domain->add($uid,$data);
         }
         return $rs;
    }
    public function index(){
        $imageDefault = array(
            'left'=>180,
            'top'=>500,
            'right'=>0,
            'bottom'=>0,
            'width'=>380,
            'height'=>380,
            'opacity'=>100
        );
        $textDefault = array(
            'text'=>'',
            'left'=>0,
            'top'=>0,
            'fontSize'=>32,       //字号
            'fontColor'=>'255,255,255', //字体颜色
            'angle'=>0,
        );
        $uid = $this->uid;
        $url=get_upload_path('/appapi/down/index');
        // var_dump($url);die;
        // $url = 'http://www.baidu.com';
        $img = 'public/static/images/haibao/qrcode_'.$uid.'.jpg';
        if(!file_exists(API_ROOT.'/../'.$img)){
            include API_ROOT.'/../simplewind/Lib/Util/phpqrcode.php';
            
            $value = $url;					//二维码内容
            
            $errorCorrectionLevel = 'L';	//容错级别 
            $matrixPointSize = 6;			//生成图片大小
            //生成二维码图片
            $filename = API_ROOT.'/../'.$img;
            \QRcode::png($value,$filename , $errorCorrectionLevel, $matrixPointSize, 2); 
        }
        $background = API_ROOT.'/../public/static/images/bg.jpg';//海报最底层得背景
        $config['image'][]['url'] = API_ROOT.'/../public/static/images/haibao/qrcode_'.$uid.'.jpg';
        $filename = API_ROOT.'/../public/static/images/haibao/qrcode_bg_'.$uid.'.jpg';
        $this -> getbgqrcode($imageDefault,$textDefault,$background,$filename,$config);
        $rs = array('code' => 0, 'msg' => '提交成功', 'info' => array());
        $info =[];
        $info['img'] = get_upload_path('/'."static/images/haibao/qrcode_bg_{$uid}.jpg");
        $userinfo = getUserInfo($uid);
        $info['code'] = $userinfo['code'] ?: rand(000000,999999);
        $rs['info'] = $info;
        return $rs;
        
    }
    public function getbgqrcode($imageDefault,$textDefault,$background,$filename="",$config=array()){
        //如果要看报什么错，可以先注释调这个header
        if(empty($filename)) header("content-type: image/png");
        //背景方法
        $backgroundInfo = getimagesize($background);
        $ext = image_type_to_extension($backgroundInfo[2], false);
        $backgroundFun = 'imagecreatefrom'.$ext;
        $background = $backgroundFun($background);
        $backgroundWidth = imagesx($background);  //背景宽度
        $backgroundHeight = imagesy($background);  //背景高度
        $imageRes = imageCreatetruecolor($backgroundWidth,$backgroundHeight);
        $color = imagecolorallocate($imageRes, 0, 0, 0);
        imagefill($imageRes, 0, 0, $color);
        imagecopyresampled($imageRes,$background,0,0,0,0,imagesx($background),imagesy($background),imagesx($background),imagesy($background));
        //处理了图片
        if(!empty($config['image'])){
            foreach ($config['image'] as $key => $val) {
                $val = array_merge($imageDefault,$val);
                $info = getimagesize($val['url']);
                $function = 'imagecreatefrom'.image_type_to_extension($info[2], false);
                if($val['stream']){   
                    //如果传的是字符串图像流
                    $info = getimagesizefromstring($val['url']);
                    $function = 'imagecreatefromstring';
                }
                $res = $function($val['url']);
                $resWidth = $info[0];
                $resHeight = $info[1];
                //建立画板 ，缩放图片至指定尺寸
                $canvas=imagecreatetruecolor($val['width'], $val['height']);
                imagefill($canvas, 0, 0, $color);
                //关键函数，参数（目标资源，源，目标资源的开始坐标x,y, 源资源的开始坐标x,y,目标资源的宽高w,h,源资源的宽高w,h）
                imagecopyresampled($canvas, $res, 0, 0, 0, 0, $val['width'], $val['height'],$resWidth,$resHeight);
                $val['left'] = $val['left']<0?$backgroundWidth- abs($val['left']) - $val['width']:$val['left'];
                $val['top'] = $val['top']<0?$backgroundHeight- abs($val['top']) - $val['height']:$val['top'];
                //放置图像
                imagecopymerge($imageRes,$canvas, $val['left'],$val['top'],$val['right'],$val['bottom'],$val['width'],$val['height'],$val['opacity']);//左，上，右，下，宽度，高度，透明度
            }
        }
        //处理文字
        if(!empty($config['text'])){
            foreach ($config['text'] as $key => $val) {
                $val = array_merge($textDefault,$val);
                list($R,$G,$B) = explode(',', $val['fontColor']);
                $fontColor = imagecolorallocate($imageRes, $R, $G, $B);
                $val['left'] = $val['left']<0?$backgroundWidth- abs($val['left']):$val['left'];
                $val['top'] = $val['top']<0?$backgroundHeight- abs($val['top']):$val['top'];
                imagettftext($imageRes,$val['fontSize'],$val['angle'],$val['left'],$val['top'],$fontColor,$val['fontPath'],$val['text']);
            }
        }
        //生成图片
        if(!empty($filename)){
            $res = imagejpeg ($imageRes,$filename,90); 
            //保存到本地
            imagedestroy($imageRes);
        }else{
            imagejpeg ($imageRes);     
            //在浏览器上显示
            imagedestroy($imageRes);
        }
    }
     
      /**
      *  商家认领列表(分页)
      */
     public function getList(){
         $rs = array('code' => 0, 'msg' => '', 'info' => array());
         $keywords = $this->keywords;
         $domain = new Domain_Shop();
         $r  = $domain->getStatusShopList($keywords,$this->p);
         $rs['info'][0]['claim'] = $r;
         return $rs;
     }
     
         /**
     * 用户修改个人信息
     */
    public function updateUser(){
        $rs = array('code' => 0, 'msg' => '修改成功', 'info' => array());

        $checkToken=checkToken($this->uid,$this->token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $avatar=checkNull($this->avatar);
        $user_nickname=checkNull($this->user_nickname);
        $sex=checkNull($this->sex);
        $mobile=checkNull($this->mobile);
        $user_emaile=checkNull($this->user_email);
        $signature=checkNull($this->signature);
        $address=checkNull($this->address);
        $birthday = checkNull($this->birthday);
        $domain = new Domain_User();
        if(!empty($birthday)){
            $birthday = strtotime($birthday);
        }
        $data=[
            'avatar'=>$avatar,
            'user_nickname'=>$user_nickname,
            'sex'=>$sex,
            'mobile'=>$mobile,
            'user_email'=>$user_emaile,  
            'signature'=>$signature,
            'address'=>$address,
            'birthday' => $birthday,
            'avatar_thumb'=>$avatar,
            
        ];
        $info = $domain->userUpdate($this->uid,$data);

        if($info===false){
            $rs['code'] = 1001;
            $rs['msg'] = '修改失败';
            return $rs;
        }
        $config= DI()->notorm->option
            ->select('option_value')
            ->where("option_name='comment_app'")
            ->fetchOne();
        $config=json_decode($config['option_value'],true);
        $user = DI()->notorm->user->where('id=?',$this->uid)->fetchOne();
        if(trim($user['user_email']) && trim($user['address'])){  //完善城市后 才会进入的钱
            addMoney($this->uid,[
                'title'=>'完善资料',
                'money'=>$config['material'],
            ]);
        }
        $rs['info'][0]['msg']='修改成功';
        return $rs;



    }
    
    /**
     * 收入支出记录
     */
    public function getProfit(){
        $rs = array('code' => 0, 'msg' => '查询成功', 'info' => array());
        $checkToken=checkToken($this->uid,$this->token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
           // return $rs;
        }
        $status=checkNull($this->status);
        $domain=new Domain_Userlog();
        $res=$domain->getInfo($this->uid,$status,$this->p,$this->lan);
        $rs['info'][0]['record']=$res;

        return $rs;

    }
    
    /**
     * 贡献店铺
     */
    public function postContribution(){
        $rs = array('code' => 0, 'msg' => '提交成功', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $name=checkNull($this->name);
        $thumb=checkNull($this->thumb);
        $classid=checkNull($this->classid);
        $link=checkNull($this->link);
//        $city=checkNull($this->city);
//        $area=checkNull($this->area);
//        $address=checkNull($this->address);
//        $open_time=checkNull($this->open_time);
//        $end_time=checkNull($this->end_time);
//        $service_phone = checkNull($this->service_phone);
//        $business_status = checkNull($this->business_status);
//        $lng = checkNull($this->lng);
//        $lat = checkNull($this->lat);
        $checkToken=checkToken($uid,$token);
       // print_r($thumb);exit;
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        //判断用户是否实名认证
        // $isauth=isAuth($uid);
        // if(!$isauth){
        //     $rs['code']=1001;
        //     $rs['msg']="请先进行实名认证";
        //     return $rs;
        // }
        if(!$thumb){
            $rs['code']=1001;
            $rs['msg']="请上传图片";
            return $rs;
        }
        if(!$name){
            $rs['code']=1001;
            $rs['msg']="请填写名称";
            return $rs;
        }
        if(!$classid){
            $rs['code']=1001;
            $rs['msg']="请选择经营类目";
            return $rs;
        }
        $data=array(
            'name'=>$name,
            'thumb'=>$thumb,
            'link'=>$link,
//            'city'=>$city,
//            'area'=>$area,
//            'address'=>$address,
            'classid'=>$classid,
//            'service_phone'=>$service_phone,
            'status'=>1,
//            'open_time'=>$open_time,
//            'end_time'=>$end_time,
//            'business_status'=>$business_status,
            'provider_id'=>$uid,
            'addtime'=>time(),
//            'lng'=>$lng,
//            'lat'=>$lat,
        );
        $domain=new Domain_Shop();
        $res=$domain->shopDevote($uid,$data);

        if($res==1001){
            $rs['code']=1002;
            $rs['msg']='店铺提交失败';
            return $rs;
        }

        return $rs;
    }
    
    /**
     * 我的贡献列表
     */
    public function dedicateList(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $checkToken=checkToken($this->uid,$this->token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Shop();
        $r  = $domain->getUserShopList($this->uid,$this->p);
        $rs['info'][0]['contribution'] = $r;
        return $rs;


    }
    
    public function smsCode(){
		$rs = array('code' => 0, 'msg' => '', 'info' => array());
		$mobile = $this->mobile;
		$ismobile=checkMobile($mobile);
		if(!$ismobile){
			$rs['code']=1001;
			$rs['msg']='请输入正确的手机号';
			return $rs;	
		}
		if($_SESSION['set_mobile']==$mobile && $_SESSION['set_mobile_expiretime']> time() ){
			$rs['code']=1002;
			$rs['msg']='验证码5分钟有效，请勿多次发送';
			return $rs;
		}
        $mobile_code = random(6,1);
		/* 发送验证码 */
		$result=sendCode($mobile,$mobile_code);
		if($result['code']===0){
			$_SESSION['set_mobile'] = $mobile;
			$_SESSION['set_mobile_code'] = $mobile_code;
			$_SESSION['set_mobile_expiretime'] = time() +60*5;
			$rs['info']=$mobile_code;
		}else if($result['code']==667){
			$_SESSION['set_mobile'] = $mobile;
            $_SESSION['set_mobile_code'] = $result['msg'];
            $_SESSION['set_mobile_expiretime'] = time() +60*5;
            
            $rs['code']=1002;
			$rs['msg']='验证码为：'.$result['msg'];
            
		}else{
			$rs['code']=1002;
			$rs['msg']=$result['msg'];
		}

		
		return $rs;
        
    }
    public function shopAuth(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $id=checkNull($this->id);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain=new Domain_Userauth();
        $r  = $domain->shopAuth($uid,$id);
        $status=DI()->notorm->attestation
					->where('uid=? and shop_id=?',$uid,$id)
					->fetchOne();
        $rs['info'][0]['status']=$r;
        $rs['info'][0]['content']=$status['reason'];
        return $rs;
        
        
    }
    /**
     * 商家认证
     */
    public function authentication(){
        $rs = array('code' => 0, 'msg' => '提交成功', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
        // if($checkToken==700){
        //     $rs['code'] = $checkToken;
        //     $rs['msg'] = '您的登陆状态失效，请重新登陆！';
        //     return $rs;
        // }
        $shop_id=checkNull($this->shop_id);
        $name=checkNull($this->name);
        $username=checkNull($this->username);
        $phone=checkNull($this->phone);
// 		if($phone!=$_SESSION['reg_mobile']){
// 			$rs['code'] = 1001;
// 			$rs['msg'] = '手机号码不一致';
// 			return $rs;					
// 		}

// 		if($this->code!=$_SESSION['reg_mobile_code']){
// 			$rs['code'] = 1002;
// 			$rs['msg'] = '验证码错误';
// 			return $rs;					
// 		}	
        
        
        
        
        $cardno=checkNull($this->cardno);
        $province=checkNull($this->province);
        $city=checkNull($this->city);
        $area=checkNull($this->area);
        $address=checkNull($this->address);
        $card_front=checkNull($this->card_front);
        $card_back=checkNull($this->card_back);
        $certificate=checkNull($this->certificate);
        // $lng=checkNull($this->lng);
        // $lat=checkNull($this->lat);
        $data=array(
            'shop_id'=>$shop_id,
            'uid'=>$uid,
            'name'=>$name,
            'username'=>$username,
            'phone'=>$phone,
            'cardno'=>$cardno,
            'province'=>$province,
            'city'=>$city,
            'area'=>$area,
            'address'=>$address,
            'card_front'=>$card_front,
            'card_back'=>$card_back,
            'certificate'=>$certificate,
            // 'lng'=>$lng,
            // 'lat'=>$lat,
            'addtime'=>time(),
        );
        $domain=new Domain_Shop();
        $res=$domain->identification($data);
        if($res==1001){
            $rs['code']=1002;
            $rs['msg']='认证店铺提交失败';
            return $rs;
        }
        if($res==1002){
            $rs['code']=1001;
            $rs['msg']="认证已提交,请耐心等待";
            return $rs;
        }
        return $rs;

    }
    //浏览记录
    public function visit(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
        // if($checkToken==700){
        //     $rs['code'] = $checkToken;
        //     $rs['msg'] = '您的登陆状态失效，请重新登陆！';
        //     return $rs;
        // }
        $record = new Domain_Invitationvisit();
        
        if(checkNull($this->touid)){
            $r = $record->getHistory($this->touid,$this->p); 
        }else{
            $r = $record->getHistory($uid,$this->p); 
            
        }
        
      //  $r = $record->getHistory($uid,$this->p);
        $rs['info'][0]['list'] = $r;
        return $rs;
    }
    
    //我发布帖子
    public function myNote(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        // $checkToken=checkToken($uid,$token);
        // if($checkToken==700){
        //     $rs['code'] = $checkToken;
        //     $rs['msg'] = '您的登陆状态失效，请重新登陆！';
        //     return $rs;
        // }
        $domain = new Domain_Publish();
   
        $r = $domain->getMyNote($uid,$this->touid,$this->p); 


        $rs['info'][0]['list'] = $r;
        return $rs;
    }
    //我发布的评论
    public function getMyComment(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
        $domain = new Domain_Comment();
        
        
        if(checkNull($this->touid)){
            $r = $domain->getMyComment($this->touid,$this->p); 
        }else{
            $r = $domain->getMyComment($uid,$this->p); 
            
        }
        
        
       // $r = $domain->getMyComment($uid,$p);
        $rs['info'][0]['list'] = $r;
        return $rs;
    }
    //我的关联
    public function myRelated(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_User();
                
        if(checkNull($this->touid)){
            $r = $domain->myRelated($this->touid); 
        }else{
            $r = $domain->myRelated($uid); 
            
        }
        if(!$r){
            $rs['msg'] = '用户不存在';
        }
        $rs['info'][0]['list'] = $r;
        return $rs;
    }
    
    public function report(){
        $rs = array('code' => 0, 'msg' => '举报成功', 'info' => array());
        $uid=$this->uid;
		$token=checkNull($this->token);
	    $report_id=$this->report_id;
	    $type=$this->type;
		$content=checkNull($this->content);
		$description=checkNull($this->description);
		$thumb=checkNull($this->thumb);
// 		$checkToken=checkToken($uid,$token);
// 		if($checkToken==700){
// 			$rs['code'] = $checkToken;
// 			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
// 			return $rs;
// 		}
		$data=array(
			'uid'=>$uid,
			'report_id'=>$report_id,
			'content'=>$content,
			'description'=>$description,
			'thumb'=>$thumb,
			'type'=>$type,
			'add_time'=>time(),
		);
	
        $domain = new Domain_Dynamic();
        $info = $domain->report($data);
		
		if($info==1000){
			$rs['code'] = 1000;
			$rs['msg'] = '动态不存在';
			return $rs;
		}
        $rs['info'][0]['msg']='操作成功';
        return $rs;
		
        
    }
    
}