<?php
/**
 * 动态管理
 */
class Api_Dynamic extends PhalApi_Api {

	public function getRules() {
		return array(
            'setDynamic' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'title' => array('name' => 'title', 'type' => 'string', 'desc' => '标题'),
				'thumb' => array('name' => 'thumb', 'type' => 'string', 'desc' => '图片地址集合'),
				'video_thumb' => array('name' => 'video_thumb', 'type' => 'string', 'desc' => '视频封面'),
				'href' => array('name' => 'href', 'type' => 'string', 'desc' => '视频地址'),
				'voice' => array('name' => 'voice', 'type' => 'string', 'desc' => '语音地址'),
				'length' => array('name' => 'length', 'type' => 'int', 'default'=>0, 'desc' => '语音长度'),
				'lat' => array('name' => 'lat', 'type' => 'string',  'desc' => '维度'),
				'lng' => array('name' => 'lng', 'type' => 'string',  'desc' => '经度'),
				'city' => array('name' => 'city', 'type' => 'string',  'desc' => '城市'),
				'address' => array('name' => 'address', 'type' => 'string',  'desc' => '详细地理位置'),
				'type' => array('name' => 'type', 'type' => 'int','default'=>0, 'desc' => '动态类型：0：纯文字；1：文字+图片；2：文字+视频；3：文字+音频'),
				'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名 uid   type'),
			),
			
           'setComment' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1,  'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户Token'),
				'dynamicid' => array('name' => 'dynamicid', 'type' => 'int', 'min' => 1,'desc' => '动态ID'),
				'touid' => array('name' => 'touid', 'type' => 'int', 'default'=>0, 'desc' => '回复的评论UID'),
                'commentid' => array('name' => 'commentid', 'type' => 'int',  'default'=>0,  'desc' => '回复的评论commentid'),
                'parentid' => array('name' => 'parentid', 'type' => 'int',  'default'=>0,  'desc' => '回复的评论ID'),
                'content' => array('name' => 'content', 'type' => 'string',  'default'=>'', 'desc' => '内容'),
                'type'=>array('name'=>'type','type'=>'int','default'=>'0','desc'=>'类型，0文字，1语音'),
                'voice'=>array('name'=>'voice','type'=>'string','desc'=>'语音'),
                'length'=>array('name'=>'length','type'=>'int','desc'=>'时长'),
            ),
			'addLike' => array(
				'uid' => array('name' => 'uid', 'type' => 'int', 'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
				'dynamicid' => array('name' => 'dynamicid', 'type' => 'int', 'desc' => '动态ID'),
                'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名 uid  dynamicid'),
			),
		
			'addCommentLike' => array(
            	'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1,  'desc' => '用户ID'),
            	'token' => array('name' => 'token', 'type' => 'string','desc' => '用户Token'),
                'commentid' => array('name' => 'commentid', 'type' => 'int', 'min' => 1, 'desc' => '评论/回复 ID'),
				'sign' => array('name' => 'sign', 'type' => 'string', 'desc' => '签名 uid  commentid'),
            ),
			
			 'getAttentionDynamic' => array(
            	'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1,'desc' => '用户ID'),
            	'token' => array('name' => 'token', 'type' => 'string','desc' => '用户Token'),
            	'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1, 'desc' => '页数'),
            ),
			 'getNewDynamic' => array(
            	'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1,'desc' => '用户ID'),
            	'lng' => array('name' => 'lng', 'type' => 'string', 'desc' => '经度值'),
                'lat' => array('name' => 'lat', 'type' => 'string','desc' => '纬度值'),
            	'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1, 'desc' => '页数'),
            ),
			
			'getHomeDynamic' => array(
                'uid' => array('name' => 'uid', 'type' => 'int',  'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string','desc' => '用户Token'),
                'touid' => array('name' => 'touid', 'type' => 'int', 'require' => true, 'desc' => '对方ID'),
				'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1, 'desc' => '页数'),
            ),
			
         
            'getDynamic' => array(
            	'uid' => array('name' => 'uid', 'type' => 'int','desc' => '用户ID'),
                'dynamicid' => array('name' => 'dynamicid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '动态ID'),
            ),
			
            'getComments' => array(
                'uid' => array('name' => 'uid', 'type' => 'int','desc' => '用户ID'),
                'dynamicid' => array('name' => 'dynamicid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '动态ID'),
                'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1, 'desc' => '页数'),
            ),
			
			'getReplys' => array(
				'uid' => array('name' => 'uid', 'type' => 'int',  'require' => true, 'desc' => '用户ID'),
                'commentid' => array('name' => 'commentid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '评论ID'),
                'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1, 'desc' => '页数'),
            ),
		
            'del' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => 'token'),
                'id' => array('name' => 'id', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '动态ID'),
            ),
			
			
			'report' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => 'token'),
                'dynamicid' => array('name' => 'dynamicid', 'type' => 'int', 'min' => 1, 'require' => true, 'desc' => '动态ID'),
                'content' => array('name' => 'content', 'type' => 'string', 'min' => 1, 'require' => true, 'desc' => '举报内容'),
            ),
			
			
			'getCreateNonreusableSignature' => array(
                'imgname' => array('name' => 'imgname', 'type' => 'string', 'desc' => '图片名称'),
                'videoname' => array('name' => 'videoname', 'type' => 'string', 'desc' => '视频名称'),
				'folderimg' => array('name' => 'folderimg', 'type' => 'string','desc' => '图片文件夹'),
				'foldervideo' => array('name' => 'foldervideo', 'type' => 'string', 'desc' => '视频文件夹'),
            ),


            'getRecommendDynamics'=>array(
            	'uid' => array('name' => 'uid', 'type' => 'int',  'desc' => '用户ID'),
            	'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1, 'desc' => '页数'),
            ),
            'topicNote'=>array(
				'uid'=> array('name' => 'uid', 'require' => true,'type' => 'string',  'desc' => '用户ID'),
				'token' => array('name' => 'token', 'type' => 'string', 'desc' => '用户token'),
                'topic_id'=> array('name' => 'topic_id', 'require' => true,'type' => 'string',  'desc' => '话题ID'),
				'lat' => array('name' => 'lat', 'type' => 'string','require' => true,  'desc' => '维度'),
				'lng' => array('name' => 'lng', 'type' => 'string','require' => true,  'desc' => '经度'),
            	'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1, 'desc' => '页数'), 

            ),
            
            'delComment' => array(
               	'uid'=> array('name' => 'uid', 'require' => true,'type' => 'int',  'desc' => '用户ID'),
               	'id'=> array('name' => 'id', 'require' => true,'type' => 'int',  'desc' => '评论id'),
               	'token'=> array('name' => 'token', 'require' => true,'type' => 'string',  'desc' => 'token'),
            ),
            'getShopDynamic' => array(
               	'id'=> array('name' => 'id', 'require' => true,'type' => 'int',  'desc' => '店铺ID'),
               	'p' => array('name' => 'p', 'type' => 'int', 'min' => 1, 'default'=>1, 'desc' => '页数'),
               	'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true,  'desc' => '用户id'),
            )
		);
	}
	/**
     * showdoc
     * @catalog 获取商铺的短视频和帖子
     * @title 获取商铺的短视频和帖子
     * @description 获取商铺的短视频和帖子
     * @method POST
     * @url {domian}/appapi/?service=Dynamic.getShopDynamic
     * @param id 必选 int id
     * @param uid 必选 int 用户uid
     * @return {"ret":200,"data":{"code":0,"msg":"\u83b7\u53d6\u6210\u529f","info":[[{"fid":"3144","video_img":[],"shop_id":"191","thumb":["https:\/\/static-ap.askpert.com\/android1626518465753wx_camera_1626243132987.jpg"],"title":"\u51e4\u51f0\u51e4\u51f0","type":"1","draft":"0","status":"1","isdel":"0","addtime":"1626595058"},{"fid":"3143","video_img":[],"shop_id":"191","thumb":["https:\/\/static-ap.askpert.com\/android16265179023601626517867189.jpg","https:\/\/static-ap.askpert.com\/android16265179034111626517869461.jpg","https:\/\/static-ap.askpert.com\/android16265179037211626517872347.jpg"],"title":"\u963f\u897f\u5427","type":"1","draft":"0","status":"1","isdel":"0","addtime":"1626517904"},{"fid":"3142","video_img":[],"shop_id":"191","thumb":["https:\/\/static-ap.askpert.com\/android1626517559482wx_camera_1626243132987.jpg"],"title":"\u597d\u60f3\u597d\u60f3\u5bb6","type":"1","draft":"0","status":"1","isdel":"0","addtime":"1626517561"},{"fid":"3141","video_img":[],"shop_id":"197","thumb":["https:\/\/static-ap.askpert.com\/android1626517227848wx_camera_1626321393676.jpg"],"title":"DHC\u597d\u8bcd\u4f73\u53e5\u8be5\u653e\u5c31\u653e\u9152\u9b3c\u9152\u56fd\u9645\u7ecf\u5e38\u5403\u996d","type":"1","draft":"0","status":"1","isdel":"0","addtime":"1626517229"},{"fid":"94","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265162680171626516267965.jpg"],"shop_id":"191","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265162680171626516267965.jpg"],"title":"ghhh","type":"0","draft":"0","status":"1","isdel":"0","addtime":"1626516278"},{"fid":"93","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265147962651626514796188.jpg"],"shop_id":"197","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265147962651626514796188.jpg"],"title":"\u5173\u7cfb\u8fd8\u884c\u5462\u5c0f\u9e21\u5c0f\u9e21","type":"0","draft":"0","status":"1","isdel":"0","addtime":"1626514845"},{"fid":"92","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265136177361626513610082.jpg"],"shop_id":"191","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265136177361626513610082.jpg"],"title":"\u4e0d\u559c\u6b22\u559c\u5267\u5c0f\u6d63\u718a\u5c31\u662f\u65b0\u534e\u5927\u8857","type":"0","draft":"0","status":"1","isdel":"0","addtime":"1626513626"},{"fid":"90","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265134653741626513465289.jpg"],"shop_id":"197","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265134653741626513465289.jpg"],"title":"\u8106\u9aa8\u80a0\u7c97\u7cd9fycuvj\u59d1\u59d1","type":"0","draft":"0","status":"1","isdel":"0","addtime":"1626513477"},{"fid":"89","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265134049881626513404902.jpg"],"shop_id":"191","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265134049881626513404902.jpg"],"title":"\u7c97\u7c97\u53e4\u5e01","type":"0","draft":"0","status":"1","isdel":"0","addtime":"1626513416"},{"fid":"88","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265130230391626513022954.jpg"],"shop_id":"191","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16265130230391626513022954.jpg"],"title":"\u5403v\u597dv\u5c40","type":"0","draft":"0","status":"1","isdel":"0","addtime":"1626513041"},{"fid":"84","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16264083848221626408384752.jpg"],"shop_id":"260","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16264083848221626408384752.jpg"],"title":"\u8fd9\u4ed6\u55b5\u7684","type":"0","draft":"0","status":"1","isdel":"0","addtime":"1626408396"},{"fid":"3140","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16264003004711626400300408.jpg","https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16264003016621626400300672.jpg","https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16264003022221626400300961.jpg"],"shop_id":"0","thumb":[],"title":"\u54c8\u55bd","type":"1","draft":"0","status":"1","isdel":"0","addtime":"1626400306"},{"fid":"3135","video_img":[],"shop_id":"260","thumb":["https:\/\/sn.askpert.com\/file_4144320210712_151113_9978733_android_.jpg"],"title":"chc","type":"1","draft":"0","status":"1","isdel":"0","addtime":"1626073874"},{"fid":"3131","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16260711514361626071151237.jpg","https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16260711530601626071151998.jpg"],"shop_id":"262","thumb":["https:\/\/static-ap.askpert.com\/android16260704949061626070484976.jpg"],"title":"634y4y","type":"1","draft":"0","status":"1","isdel":"0","addtime":"1626070497"},{"fid":"3130","video_img":[],"shop_id":"262","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16260704031551626070392880.jpg"],"title":"Tggf","type":"1","draft":"0","status":"1","isdel":"0","addtime":"1626070404"},{"fid":"3129","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16260700408601626070038334.jpg"],"shop_id":"0","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16260697812141626069771445.jpg","https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16260700378591626070037506.png"],"title":"Vnfb","type":"1","draft":"0","status":"1","isdel":"0","addtime":"1626069782"},{"fid":"3125","video_img":[],"shop_id":"204","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16258993173621625899310404.png","https:\/\/static-ap.askpert.com\/c709fee03ca25e5e56f1beeb8cd92c65.jpg"],"title":"\u9e45\u9e45\u9e45","type":"1","draft":"0","status":"1","isdel":"0","addtime":"1626060927"},{"fid":"82","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16258251483841625825148214.jpg"],"shop_id":"191","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16258251483841625825148214.jpg"],"title":"\u5403\u996d\u996d","type":"0","draft":"0","status":"1","isdel":"0","addtime":"1625825191"},{"fid":"81","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16258246911421625824690963.jpg"],"shop_id":"197","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16258246911421625824690963.jpg"],"title":"\u7092\u83dc\u53cd\u53cd\u590d\u590d","type":"0","draft":"0","status":"1","isdel":"0","addtime":"1625824711"},{"fid":"77","video_img":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16258018240041625801823910.jpg"],"shop_id":"262","thumb":["https:\/\/askpert-oss.obs.ap-southeast-3.myhuaweicloud.com:443\/android16258018240041625801823910.jpg"],"title":"giggles","type":"0","draft":"0","status":"1","isdel":"0","addtime":"1625801786"}]]},"msg":""}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @return_param info[fid] int 为资源id,根据type的不同来分别代表视频id或者帖子ID
     * @return_param info[type] int 0为视频,1为帖子
     * @return_param info[thumb] array 为封面图片,应该和下方video_img合并,权重为先thumb再video_img
     * @return_param info[video_img] array 为封面图片
     * @return_param info[shop_id] int 店铺ID
     * @return_param info[title] string 标题
     * @return_param info[is_like] string 是否喜欢
     * @remark
     * @number 1
     */
	public function getShopDynamic(){
	     $rs = array('code' => 0, 'msg' => '获取成功', 'info' => array());
	     $p = $this->p;
	     $shop_id = $this->id;
	     $uid  = $this->uid;
	     $nums=20;
	   	 $start=($p-1)*$nums;
	     $list = DI()->notorm->dynamic->where('shop_id=?',$shop_id)->where('draft = 0 and isdel = 0 and status = 1')->limit($start,$nums)->fetchAll();
	     #var_dump($list);die;
	     $sql = 'select * from (select id as fid,points,reading,uid,likes,topic,video_img,shop_id,thumb,title,1 as type,draft,status,isdel,addtime   from cmf_dynamic where status = 1 and draft = 0 and isdel = 0
UNION select id as fid,score as points,visits as reading,uid,likes,topic, video_img,shop_id,"" as thumb,`desc` as title,0 as type,draft,status,is_delete as isdel,createtime as addtime from cmf_video where status = 1 and draft = 0 and is_delete = 0) as a where a.shop_id = '.$shop_id.'  ORDER BY addtime desc limit '.$start.','.$nums;

        $list = DI()->notorm->dynamic->queryall($sql);
       
        foreach ($list as $k=>$v){
	         $thumb = $v['thumb'];
	         $thumb = explode(',',$thumb);
	         $new_thub = [];
	         foreach ($thumb as $tk=>$tv){
	             if($tv){
	                 $tv = get_upload_path($tv);
	                 $new_thub[] = $tv;
	             }
	         }
	         $v['thumb'] = $new_thub;
	         $video_img = $v['video_img'];
	         $video_img = explode(',',$video_img);
	         $new_video_img = [];
	         foreach ($video_img as $tk=>$tv){
	             if($tv){
	                 $tv = get_upload_path($tv);
	                 $new_video_img[] = $tv;
	             }
	         }
	         $v['video_img'] = $new_video_img;
	         $v['type'] = (int)$v['type'];
	         $topic =  $this->getShopTopic($v['topic']) ?: [];
	         $custom = $this->getShopTopic($v['custom']) ?: [];
	         $v['topic'] = array_merge($topic,$custom);
	         $v['is_like'] = $this->getisLike($v['fid'],$uid,$v['type']);
	         $v['is_collect'] = $this->getIsCollect($id,$uid,$v['type']);
	         $v['collects'] = $this->getCollectNumber($v['fid'],$v['type']);
	         $v['user'] = getUserInfo($v['uid']);
	         $v['user']['avatar'] = get_upload_path($v['user']['avatar']);
	         $list[$k] = $v;
	     }
	     if($_REQUEST['ios'] == 1){
	         $rs['info'][0]['list'] = $list;
	     }else{
	         $rs['info'][0] = $list;
	     }
	     
	     return $rs;
	}
	
	public function getCollectNumber($id,$type = 0){
	     if($type == 1){
	        $attention = DI()->notorm->collect
            ->select("*")
            ->where("collect_id=?  and type = 1", $id)
            ->count();
	    }else{
	         $attention = DI()->notorm->user_video_collect
            ->select("*")
            ->where(" video_id = ?",  $id)
            ->count();
	    }
	    return $attention;
	}
	
	public function getIsCollect($id,$uid,$type= 0){
	     if($type == 1){
	        $attention = DI()->notorm->collect
            ->select("*")
            ->where("collect_id=? and user_id = ? and type = 1", $id, $uid)
            ->count();
	    }else{
	         $attention = DI()->notorm->user_video_collect
            ->select("*")
            ->where("uid=? and video_id = ?", $uid, $id)
            ->count();
	    }
	    return $attention;
	}
	
	
	public function getisLike($id,$uid,$type = 0){
	    if($type == 1){
	         $attention = DI()->notorm->note_like
            ->select("*")
            ->where("note_id=? and uid = ?", $id, $uid)
            ->count();
	    }else{
	         $attention = DI()->notorm->user_video_like
            ->select("*")
            ->where("uid=? and video_id = ?", $uid, $id)
            ->count();
	    }
	    return $attention;
	}
	
	
	 public function getShopTopic($topic) {
        if(!$topic) {
            return 0;
        }
        $topics = explode(',', trim($topic,','));
        $res = [];
        $info =  DI()->notorm->shop_topic->where('topic_id', $topics)->fetchAll();
        foreach ($topics as $v){
            if(is_numeric($v)){  //该地方在2021/4/19 因为app端新增标签时,通知服务端未改,导致后期发现未适应数据库,所以修改未适应
                $s = DI()->notorm->shop_topic->where('topic_id', $v)->fetchOne();
                if($s){
                    $res[] = $s;
                }
            }

        }
        return $res;
    }
	
	/**
	 * 发布动态
	 * @desc 用于 发布动态
	 * @return int code 操作码，0表示成功
	 * @return array info 
	 * @return string info[0].id 记录ID
	 * @return string msg 提示信息
	 */
	public function setDynamic() {
		$rs = array('code' => 0, 'msg' => '发布成功', 'info' => array());
		
		$uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$title=checkNull($this->title);
		$thumb=checkNull($this->thumb);
		$video_thumb=checkNull($this->video_thumb);
		$href=checkNull($this->href);
		$lat=checkNull($this->lat);
		$lng=checkNull($this->lng);
		$city=checkNull($this->city);
		$address=checkNull($this->address);
		$type=checkNull($this->type);
		$sign=checkNull($this->sign);
		$voice=checkNull($this->voice);
		$length=checkNull($this->length);
		
		if($uid<1 || $token==''){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
     
        $checkdata=array(
            'uid'=>$uid,
            'type'=>$type,
        );
        
       $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1002;
			$rs['msg']='签名错误';
			return $rs;	
        }
		
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
		
		$sensitivewords=sensitiveField($title);
	
		if($sensitivewords==1001){
			$rs['code'] = 10011;
			$rs['msg'] = '动态标题输入非法，请重新输入';
			return $rs;
		}
		
		
		$data=array(
            'uid'=>$uid,
            'title'=>$title,
            'thumb'=>$thumb,
            'video_thumb'=>$video_thumb,
            'href'=>$href,
            'voice'=>$voice,
            'length'=>$length,
            'lat'=>$lat,
            'lng'=>$lng,
            'city'=>$city,
            'address'=>$address,
            'type'=>$type,
            "likes"=>0,
			"comments"=>0,
        );

		
		$domain = new Domain_Dynamic();
		$info = $domain->setDynamic($data);
		if($info==1007){
			$rs['code']=1007;
			$rs['msg']='视频分类不存在';
			return $rs;
		}else if($info==1003){
			$rs['code']=1003;
			$rs['msg']='您还未认证或认证还未通过';
			return $rs;
		}else if($info==1004){
			$rs['code']=1004;
			$rs['msg']='发布失败，请重试';
			return $rs;
		}else if(!$info){
			$rs['code']=1001;
			$rs['msg']='发布失败';
			return $rs;
		}
		if($info['status']=='0'){
			$rs['msg']="发布成功，请等待审核";
		}
		$rs['info'][0]=$info;
		/* $rs['info'][0]['id']=$info['id'];
		$rs['info'][0]['thumb_s']=get_upload_path($thumb_s); */
		return $rs;
	}		
	
   	/**
     * 评论/回复
     * @desc 用于用户评论/回复 别人视频
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return int info[0].isattent 对方是否关注我
     * @return int info[0].u2t 我是否拉黑对方
     * @return int info[0].t2u 对方是否拉黑我
     * @return int info[0].comments 评论总数
     * @return int info[0].replys 回复总数
     * @return string msg 提示信息
     */
	public function setComment() {
        $rs = array('code' => 0, 'msg' => '评论成功', 'info' => array());
		
		$uid=$this->uid;
		$token=checkNull($this->token);
		$touid=checkNull($this->touid);
		$dynamicid=$this->dynamicid;
		$commentid=$this->commentid;
		$parentid=$this->parentid;
		$content=checkNull($this->content);
		$type=checkNull($this->type);
		$voice=checkNull($this->voice);
		$length=checkNull($this->length);

		
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
		$info=array(
			'isattent'=>'0',
			'u2t'=>'0',
			't2u'=>'0',
			'comments'=>0,
			'replys'=>0,
		);
		if($touid>0){
			$isattent=isAttention($touid,$uid);
			$u2t = isBlack($uid,$touid);
			$t2u = isBlack($touid,$uid);
			$info['isattent']=(string)$isattent;
			$info['u2t']=(string)$u2t;
			$info['t2u']=(string)$t2u;
			if($t2u==1){
				$rs['code'] = 1000;
				$rs['msg'] = '对方暂时拒绝接收您的消息';
				return $rs;
			}
		}
		
		if($type==1){
            if($voice==''){
                $rs['code'] = 1001;
				$rs['msg'] = '请录入语音';
				return $rs;
            }
        }else{
            if($content==''){
                $rs['code'] = 1002;
				$rs['msg'] = '请输入内容';
				return $rs;
            }
        }
		
		$sensitivewords=sensitiveField($content);
		if($sensitivewords==1001){
			$rs['code'] = 10011;
			$rs['msg'] = '评论内容输入非法，请重新输入';
			return $rs;
		}
		
		if($commentid==0 && $commentid!=$parentid){
			$commentid=$parentid;
		}
		
		$data=array(
			'uid'=>$uid,
			'touid'=>$touid,
			'dynamicid'=>$dynamicid,
			'commentid'=>$commentid,
			'parentid'=>$parentid,
			'content'=>$content,
			'addtime'=>time(),
			'type'=>$type,
			'voice'=>$voice,
			'length'=>$length,
		);

        $domain = new Domain_Dynamic();
        $result = $domain->setComment($data);
		
		$info['comments']=$result['comments'];
		$info['replys']=$result['replys'];
	
		$rs['info'][0]=$info;
		
		if($parentid!=0){
			 $rs['msg']='回复成功';			
		}
        return $rs;
    }	
	
  
   	/**
     * 点赞
     * @desc 用于动态点赞数累计
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[0].islike 是否点赞 
     * @return string info[0].likes 点赞数量
     * @return string msg 提示信息
     */
	public function addLike() {
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $uid=checkNull($this->uid);
		$token=checkNull($this->token);
		$dynamicid=checkNull($this->dynamicid);
		$sign=checkNull($this->sign);
        
        if($uid<1 || $token=='' || $dynamicid <1 ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
        
        $checkdata=array(
            'uid'=>$uid,
            'dynamicid'=>$dynamicid,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1002;
			$rs['msg']='签名错误';
			return $rs;	
        }
      
        $checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
		
		
        $domain = new Domain_Dynamic();
        $result = $domain->addLike($uid,$dynamicid);
		if($result==1001){
			$rs['code'] = 1001;
			$rs['msg'] = "动态不存在";
			return $rs;
		}else if($result==1002){
			$rs['code'] = 1002;
			$rs['msg'] = "不能给自己点赞";
			return $rs;
		}else if($result == 1003){
		    $rs['code'] == 1003;
		    $rs['msg'] ="服务器繁忙";
        }
		$rs['info'][0]=$result;
        return $rs;
    }	

   	
	
   	/**
     * 评论/回复 点赞
     * @desc 用于评论/回复 点赞数累计
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[0].islike 是否点赞 
     * @return string info[0].likes 点赞数量
     * @return string msg 提示信息
     */
	public function addCommentLike() {
        $rs = array('code' => 0, 'msg' => '点赞成功', 'info' => array());

        $uid=$this->uid;
        $token=checkNull($this->token);
        $commentid=$this->commentid;
		$sign=$this->sign;

		if($uid<1 || $token=='' || $commentid <1 ){
            $rs['code'] = 1001;
			$rs['msg'] = '信息错误';
			return $rs;
        }
		$checkdata=array(
            'uid'=>$uid,
            'commentid'=>$commentid,
        );
        
        $issign=checkSign($checkdata,$sign);
        if(!$issign){
            $rs['code']=1002;
			$rs['msg']='签名错误';
			return $rs;	
        }
		

		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}

        $domain = new Domain_Dynamic();
         $res= $domain->addCommentLike($uid,$commentid);
         if($res==1001){
         	$rs['code']=1003;
         	$rs['msg']='评论信息不存在';
         	return $rs;
         }
         $rs['info'][0]=$res;

        return $rs;
    }	
	
	/**
     * 获取关注动态
     * @desc 用于获取关注动态
     * @return int code 操作码，0表示成功
     * @return array info 动态列表
     * @return array info[].userinfo 用户信息
     * @return string info[].datetime 格式后的发布时间
	 * @return string info[].islike 是否点赞 
	 * @return string info[].comments 评论总数
     * @return string info[].likes 点赞数
     * @return string msg 提示信息
     */
	public function getAttentionDynamic() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $uid=$this->uid;
		$token=checkNull($this->token);
		$p=$this->p;
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}

		$key='attention_dynamicLists_'.$uid.'_'.$p;
        $info=getcaches($key);

        if(!$info){
        	$domain = new Domain_Dynamic();
        	$info=$domain->getAttentionDynamic($uid,$p);
        	if(!$info){
        		 $rs['code']=0;
                $rs['msg']="暂无动态列表";
                return $rs;
        	}
			setCaches($key,$info,2);
        }
        
        $rs['info'] = $info;

        return $rs;
    }
	/**
     * 获取最新动态
     * @desc 用于 获取最新动态
     * @return int code 操作码，0表示成功
     * @return array info 动态列表
     * @return array info[].userinfo 用户信息
     * @return string info[].datetime 格式后的发布时间
	 * @return string info[].islike 是否点赞 
	 * @return string info[].comments 评论总数
     * @return string info[].likes 点赞数
     * @return string msg 提示信息
     */
	public function getNewDynamic() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $uid=checkNull($this->uid);
		$lng=checkNull($this->lng);
		$lat=checkNull($this->lat);
		$p=checkNull($this->p);
		
		if(!$p){
			$p=1;
		}
		
		$key='new_dynamicLists_'.$p;
        $info=getcaches($key);

        if(!$info){
        	$domain = new Domain_Dynamic();
        	$info=$domain->getNewDynamic($uid,$lng,$lat,$p);
			
        	if(!$info){
        		$rs['code']=0;
                $rs['msg']="暂无动态列表";
                return $rs;
        	}
			setCaches($key,$info,2);
        }
        
        $rs['info'] = $info;

        return $rs;
    }
	
	/**
     * 个人主页动态
     * @desc 用于获取个人主页动态
     * @return int code 操作码，0表示成功
     * @return string msg 提示信息
     */
	public function getHomeDynamic() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		$isBan=isBan($this->uid);
		 if($isBan=='0'){
			$rs['code'] = 700;
			$rs['msg'] = '该账号已被禁用';
			return $rs;
		}
		$uid=checkNull($this->uid);
		$touid=checkNull($this->touid);
		$p=checkNull($this->p);
		$token=checkNull($this->token);
		
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
		

        $domain = new Domain_Dynamic();
        $info = $domain->getHomeDynamic($uid,$touid,$p);
		
		
		$rs['info']=$info;

        return $rs;
    }	
	 /**
     * 获取推荐动态
     * @desc 用户获取推荐动态
     * @return int code 状态码，0表示成功
     * @return string msg 提示信息
     * @return array info 返回信息
     */
    public function getRecommendDynamics(){
    	$rs = array('code' => 0, 'msg' => '', 'info' => array());
    	$uid=$this->uid;
    	if($uid>0){ //非游客

    		$isBan=isBan($this->uid);
			if($isBan=='0'){
				$rs['code'] = 700;
				$rs['msg'] = '该账号已被禁用';
				return $rs;
			}
    	}

		$p=$this->p;
		$key='dynamicRecommend_'.$p;

		$info=getcaches($key);

		if(!$info){

			$domain=new Domain_Dynamic();
			$info=$domain->getRecommendDynamics($uid,$p);

			if($info==1001){
				$rs['code']=1001;
				$rs['msg']="暂无动态列表";
				return $rs;
			}
			setcaches($key,$info,2);
		}
		$rs['info']=$info;

		return $rs;
    }
	
	
	/**
     * 动态详情
     * @desc 用于获取动态详情
     * @return int code 操作码，0表示成功，1000表示视频不存在
     * @return array info[0] 视频详情
     * @return object info[0].userinfo 用户信息
     * @return string info[0].datetime 格式后的时间差
     * @return string info[0].isattent 是否关注
     * @return string info[0].likes 点赞数
     * @return string info[0].comments 评论数
     * @return string info[0].views 阅读数
     * @return string info[0].steps 踩一踩数量
     * @return string info[0].shares 分享数量
     * @return string info[0].islike 是否点赞
     * @return string info[0].isstep 是否踩
     * @return string msg 提示信息
     */
	public function getDynamic() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Dynamic();
        $result = $domain->getDynamic($this->uid,$this->dynamicid);
		if(!$result){
			$rs['code'] = 1000;
			$rs['msg'] = "动态已删除";
			return $rs;
			
		}
		$rs['info'][0]=$result;

        return $rs;
    }
	/**
     * 动态评论列表
     * @desc 用于获取 动态评论列表
     * @return int code 操作码，0表示成功
     * @return array info 
     * @return string info[0].comments 评论总数
     * @return array info[0].commentlist 评论列表
     * @return object info[0].commentlist[].userinfo 用户信息
	 * @return string info[0].commentlist[].datetime 格式后的时间差
	 * @return string info[0].commentlist[].replys 回复总数
	 * @return string info[0].commentlist[].likes 点赞数
	 * @return string info[0].commentlist[].islike 是否点赞
	 * @return array info[0].commentlist[].replylist 回复列表
     * @return string msg 提示信息
     */
	public function getComments() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		$isBan=isBan($this->uid);
		 if($isBan=='0'){
			$rs['code'] = 700;
			$rs['msg'] = '该账号已被禁用';
			return $rs;
		}

        $domain = new Domain_Dynamic();
        $rs['info'][0] = $domain->getComments($this->uid,$this->dynamicid,$this->p);

        return $rs;
    }	
	
	/**
     * 回复列表
     * @desc 用于获取动态回复列表
     * @return int code 操作码，0表示成功
     * @return array info 评论列表
     * @return object info[].userinfo 用户信息
	 * @return string info[].datetime 格式后的时间差
	 * @return object info[].tocommentinfo 回复的评论的信息
	 * @return object info[].tocommentinfo.content 评论内容
	 * @return string info[].likes 点赞数
	 * @return string info[].islike 是否点赞
     * @return string msg 提示信息
     */
	public function getReplys() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
		$isBan=isBan($this->uid);
		 if($isBan=='0'){
			$rs['code'] = 700;
			$rs['msg'] = '该账号已被禁用';
			return $rs;
		}

        $domain = new Domain_Dynamic();
        $rs['info'] = $domain->getReplys($this->uid,$this->commentid,$this->p);

        return $rs;
    }	
	
	
	/**
     * 删除动态
     * @desc 用于删除动态以及相关信息
     * @return int code 操作码，0表示成功
     * @return string msg 提示信息
     */
	public function del() {
        $rs = array('code' => 0, 'msg' => '删除成功', 'info' => array());
		
		$uid=$this->uid;
		$token=checkNull($this->token);
		$dynamicid=$this->id;

		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
		
        $domain = new Domain_Dynamic();
        $info = $domain->del($uid,$dynamicid);
        if($info==1004){
        	$rs['code'] = 1004;
			$rs['msg'] = '你暂无此帖子！';
			return $rs;
        }
        return $rs;
    }	

	/**
     * 举报动态
     * @desc 用于举报动态
     * @return int code 操作码，0表示成功
     * @return string msg 提示信息
     */
	public function report() {
        $rs = array('code' => 0, 'msg' => '举报成功', 'info' => array());
		
		$uid=$this->uid;
		$token=checkNull($this->token);
		$dynamicid=$this->dynamicid;
		$content=checkNull($this->content);
		$description = $_REQUEST['description'];
		$checkToken=checkToken($uid,$token);
		if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}
        
		$data=array(
			'uid'=>$uid,
			'dynamicid'=>$dynamicid,
			'content'=>$content,
			'addtime'=>time(),
			'description' => $description
		);
        $domain = new Domain_Dynamic();
        $info = $domain->report($data);
		
		if($info==1000){
			$rs['code'] = 1000;
			$rs['msg'] = '动态不存在';
			return $rs;
		}

        return $rs;
    }	
    //从话题点进帖子
    public function topicNote(){
        $rs = array('code' => 0, 'msg' => '', 'info' =>array());
		$topic_id=$this->topic_id;
		$lat=checkNull($this->lat);
		$lng=checkNull($this->lng);
        $domain = new Domain_Publish();
        //echo(11);exit;
        $r = $domain->topicNote($this->uid,$topic_id,$lat,$lng,$this->p);
        $rs['info'][0]['list']=$r; 
		return $rs; 
    }
	
	
	/* 检测文件后缀 */
	protected function checkExt($filename){
		$config=array("jpg","png","jpeg");
		$ext   =   pathinfo(strip_tags($filename), PATHINFO_EXTENSION);
		 
		return empty($config) ? true : in_array(strtolower($ext), $config);
	}	
	
	/**
     * 获取七牛上传Token
     * @desc 用于获取七牛上传Token
     * @return int code 操作码，0表示成功
     * @return int code 操作码，0表示成功
     * @return string msg 提示信息
     */
	public function getQiniuToken(){
	
	   	$rs = array('code' => 0, 'msg' => '', 'info' =>array());

	   	//获取后台配置的腾讯云存储信息
		//$configPri=getConfigPri();
		//$token = DI()->qiniu->getQiniuToken2($configPri['qiniu_accesskey'],$configPri['qiniu_secretkey'],$configPri['qiniu_bucket']);
		$token = DI()->qiniu->getQiniuToken();
		$rs['info'][0]['token']=$token ; 
		return $rs; 
		
	}
   

	/**
     * 获取动态举报分类列表
     * @desc 获取动态举报分类列表
     * @return int code 操作码，0表示成功
     * @return string msg 提示信息
     * @return array info 返回信息
     */
	public function getReportlist() {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());

        $domain = new Domain_Dynamic();
        $res = $domain->getReportlist();

        if($res==1001){
        	$rs['code']=1001;
        	$rs['msg']='暂无举报分类列表';
        	return $rs;
        }
        $rs['info']=$res;
        return $rs;
    }
     /**
     * showdoc
     * @catalog 帖子
     * @title 删除帖子评论
     * @description 删除帖子评论接口
     * @method Get
     * @url {dimain}/appapi/?service=Dynamic.delComment
     * @param token 必选 string 用户token
     * @param uid 必选 int 用户id
     * @param id 必选 int 评论id
     * @return {"ret":200,"data":{"code":0,"msg":"\u5220\u9664\u6210\u529f","info":[]},"msg":""}
     * @return_param ret int http状态,可忽略
     * @return_param data[code]  int 状态码,0表示成功,其他表示失败
     * @return_param data[msg] string 提示信息,当code为异常时,msg为提示信息
     * @remark
     * @number 1
     */
    public function delComment(){
        $rs = array('code' => 0, 'msg' => '删除成功', 'info' => array());
        $id = $this->id;
        $uid = $this->uid;
        $token=checkNull($this->token);
        /*if($checkToken==700){
			$rs['code'] = $checkToken;
			$rs['msg'] = '您的登陆状态失效，请重新登陆！';
			return $rs;
		}*/
		$fox = Di()->notorm->note_comment->where('comment_id = '.$id.' and user_id = '.$uid)->count();
		if(!$fox){
		    $rs['code'] = 101;
		    $rs['msg'] = '你没有该权限删除';
		    return $rs;
		}else{
		    Di()->notorm->note_comment->where("comment_id = $id")->delete();
		}
		return $rs;
    }
    
	


}
