<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/10 0010
 * Time: 13:38
 */
class Api_Follow extends PhalApi_Api {

    public function getRules() {
        return array(

            'solicitude'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true,'desc' => 'id'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'to_user_id' => array('name' => 'to_user_id', 'type' => 'int','require' => true, 'desc' => '接收者ID'),
            ),
            

        );


    }

    /**
     *  关注
     */
    public function solicitude(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=$this->uid;
        $token=checkNull($this->token);
        $to_user_id=checkNull($this->to_user_id);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $data=array(
            'from_user_id'=>$uid,
            'to_user_id'=>$to_user_id,
            'add_time'=>time(),
        );

        $domain = new Domain_Follow();
        $info = $domain->addData($data);
        if($info==1004){
            $rs['code']=1004;
            $rs['msg']='关注失败，请重试';
            return $rs;
        }
        return $rs;
    }


}