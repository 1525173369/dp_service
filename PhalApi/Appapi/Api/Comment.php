<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/3 0003
 * Time: 15:33
 */
class Api_Comment extends PhalApi_Api
{
    public function getRules()
    {
        return array(
            'addComment' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => 'id'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'shop_id' => array('name' => 'shop_id', 'type' => 'int', 'require' => true, 'desc' => '店铺ID'),
                'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '内容'),
                'service_rank' => array('name' => 'service_rank', 'type' => 'string', 'desc' => '服务等级评分'),
                'img' => array('name' => 'img', 'type' => 'string', 'desc' => '图片集,以,分隔'),
                'lan' => array('name' => 'lan', 'type' => 'int', 'default' => '0', 'desc' => '0中文1英文2马来语'),
            ),
            'getShopComment' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => 'id'),
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '店铺id'),
                'p' => array('name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'),
            ),
            'addNoteComment' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => 'id'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'pid' => array('name' => 'pid', 'type' => 'int', 'desc' => '评论ID'),
                'note_id' => array('name' => 'note_id', 'type' => 'int', 'require' => true, 'desc' => '帖子ID'),
                'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '内容'),
                'service_rank' => array('name' => 'service_rank', 'default' => '0', 'type' => 'int', 'desc' => '服务等级评分'),
                'img' => array('name' => 'img', 'type' => 'string', 'desc' => '图片集,以,分隔'),
                'lan' => array('name' => 'lan', 'type' => 'int', 'default' => '0', 'desc' => '0中文1英文2马来语'),
            ),
            'getNoteComment' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => 'id'),
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '帖子id'),
                'p' => array('name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'),
            ),
            'addLike' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => 'id'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '评论ID'),
                'status' => array('name' => 'status', 'type' => 'int', 'default' => '1', 'desc' => '0帖子评论1店铺评论点赞'),
                'lan' => array('name' => 'lan', 'type' => 'int', 'default' => '0', 'desc' => '0中文1英文2马来语'),
            ),
            'delComment' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => 'id'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id', 'type' => 'int', 'require' => true, 'desc' => '评论ID'),
                'type' => array('name' => 'type', 'type' => 'int', 'default' => '0', 'desc' => '0帖子评论1店铺评论'),
            ),
            'shield' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'note_id' => array('name' => 'note_id', 'type' => 'int', 'require' => true, 'desc' => '帖子ID'),
            ),
            'shieldList' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'p' => array('name' => 'p', 'type' => 'int', 'default' => '1', 'desc' => '页数'),
            ),
            'shieldByUser' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'touid' => array('name' => 'touid', 'type' => 'int', 'require' => true, 'desc' => '被屏蔽用户ID'),
            ),
            'callUserComent' => array(
                'uid' => array('name' => 'uid', 'type' => 'int', 'require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string', 'require' => true, 'desc' => '用户token'),
                'touid' => array('name' => 'touid', 'type' => 'string', 'require' => true, 'desc' => '对方uid,多个用,隔开'),
                'content' => array('name' => 'content', 'type' => 'string', 'require' => true, 'desc' => '评论内容'),
            )
            
            
        );
    }
    
    public function callUserComent(){
         $uid = $this->uid;
         $token = checkNull($this->token);
         $checkToken = checkToken($uid, $token);
         if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
         }
         $content  = checkNull($this->content); //内容
         $touid = checkNull($this->touid);  //对方id 
    }
    
    
    
    

    //删除评论
    public function delComment()
    {
        $rs = array('code' => 0, 'msg' => '删除评论成功', 'info' => array());
        $uid = $this->uid;
        $token = checkNull($this->token);
        $id = checkNull($this->id);
        $type = checkNull($this->type);
        $checkToken = checkToken($uid, $token);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Comment();
        $r = $domain->delComment($uid, $id, $type);
        if ($r == 1004) {
            $rs['code'] = 1004;
            $rs['msg'] = '删除失败';
            return $rs;
        } elseif ($r == 1005) {
            $rs['code'] = 1005;
            $rs['msg'] = '只能删除自己评论';
            return $rs;
        } elseif ($r == 1006) {
            $rs['code'] = 1006;
            $rs['msg'] = '该评论已删除';
            return $rs;
        }
        $rs['info'][0]['msg'] = '删除成功';
        return $rs;
    }

    /**
     *
     * 添加店铺评论
     */

    public function addComment()
    {
        $rs = array('code' => 0, 'msg' => '评论成功', 'info' => array());
        $uid = $this->uid;
        $token = checkNull($this->token);
        $shop_id = checkNull($this->shop_id);
        $content = $this->content;
        $img = $this->img;
        $pid = $_GET['pid'];
        if(!isset($pid)){
            $pid = 0;
        }
        $service_rank = checkNull($this->service_rank);
        $checkToken = checkToken($uid, $token);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $data = array(
            'user_id' => $uid,
            'shop_id' => $shop_id,
            'img' => $img,
            'service_rank' => $service_rank,
            'content' => $content,
            'create_time' => time(),
            'cover_id'=>$pid
        );

        $domain = new Domain_Comment();
        $info = $domain->addComment($data, $this->lan);
        if ($info == 1004) {
            $rs['code'] = 1004;
            $rs['msg'] = '发布失败，请重试';
            return $rs;
        }
        return $rs;

    }

    /**
     * 获取店铺评论列表
     */
    public function getShopComment()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $domain = new Domain_Comment();
        //  echo(11221);
        $r = $domain->getShopComment($this->id, $this->uid, $this->p);
        $rs['info'][0]['shop_comment'] = $r['list'];
        $rs['info'][0]['shop_count'] = $r['count'];
        return $rs;


    }

    /**
     * 添加帖子评论
     */
    public function addNoteComment()
    {

        $rs = array('code' => 0, 'msg' => '评论成功', 'info' => array());
        $uid = $this->uid;
        $token = checkNull($this->token);
        $note_id = checkNull($this->note_id);
        $pid = $_REQUEST['pid'];
        $at = isset($_REQUEST['at'])?$_REQUEST['at']:0;
       
        $touid = isset($_REQUEST['to_uid'])?$_REQUEST['to_uid']:0;
        if(!isset($pid) || !$pid){
            $pid = 0;
        }
        $content = urldecode($this->content);
        $img = $this->img;
        $service_rank = checkNull($this->service_rank);
        $checkToken = checkToken($uid, $token);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $dynamic = new Domain_Dynamic();
        $result = $dynamic->getDynamicById($note_id);
        // var_dump($result['uid']);exit;
        $user = new Domain_User();
        $self = $user->isBlack($result['uid'],$uid);
        // return $self;exit;
        if($self == '1'){
            
            $rs['code'] = 200;
            $rs['msg'] = '对方暂时拒绝您的评论！';
            return $rs;
        }
        
        $data = array(
            'user_id' => $uid,
            'note_id' => $note_id,
            'img' => $img,
            'service_rank' => $service_rank,
            'content' => $content,
            'create_time' => time(),
            'cover_id'=>$pid
        );


        $domain = new Domain_Comment();
        
        $info = $domain->addNoteComment($data, $this->lan,$at,$touid);
        if ($info == 1004) {
            $rs['code'] = 1004;
            $rs['msg'] = '发布失败，请重试';
            return $rs;
        }
        return $rs;


    }

    /**
     * 获取帖子评论列表
     */
    public function getNoteComment()
    {
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $domain = new Domain_Comment();
        $r = $domain->getNoteComment($this->id, $this->uid, $this->p);
        $rs['info'][0]['list'] = $r['list'];
        $rs['info'][0]['count'] = $r['count'];
        return $rs;

    }

    /**
     * 评论点赞
     */
    public function addLike()
    {
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $checkToken = checkToken($uid, $token);
        $id = checkNull($this->id);
        $status = checkNull($this->status);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        
        $user = new Domain_User();
        $self = $user->isBlack($pid, $uid);
        if($self == '1'){
            
            $rs['code'] = 200;
            $rs['msg'] = '对方暂时拒绝您的点赞！';
            return $rs;
        }
        $domain = new Domain_Comment();
        $r = $domain->addLike($uid, $id, $status, $this->lan);
        $rs['info'][0]['num'] = $r;
        return $rs;

    }
    /**
     * 屏蔽帖子
     */
    public function shield()
    {
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $checkToken = checkToken($uid, $token);
        $note_id = checkNull($this->note_id);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        
        $domain = new Domain_Shield();
        $r = $domain->addShield($uid, $note_id);
        $rs['info'][0]['num'] = $r;
        return $rs;

    }
    
    /**
     * 屏蔽帖子列表
     */
    public function shieldList()
    {
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $uid = checkNull($this->uid);
        $token = checkNull($this->token);
        $p = checkNull($this->p);
        $checkToken = checkToken($uid, $token);
        if ($checkToken == 700) {
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        
        $domain = new Domain_Shield();
        $r = $domain->shieldList($uid,$p);
        $rs['info'][0]['list'] = $r;
        return $rs;

    }
    
    
    /**
     * 屏蔽某个用户的所有帖子
     */
    public function shieldByUser()
    {
        $rs = array('code' => 0, 'msg' => '操作成功', 'info' => array());
        $uid = checkNull($this->uid);
        $touid = checkNull($this->touid);
        $token = checkNull($this->token);
        $checkToken = checkToken($uid, $token);
        // if ($checkToken == 700) {
        //     $rs['code'] = $checkToken;
        //     $rs['msg'] = '您的登陆状态失效，请重新登陆！';
        //     return $rs;
        // }
        
        $domain = new Domain_Shielduser();
        $r = $domain->addShieldByUser($uid,$touid);
        $rs['info'][0]['num'] = $r;
        return $rs;

    }
    
    
    

}