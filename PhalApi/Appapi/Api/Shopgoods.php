<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 10:33
 */
class Api_Shopgoods extends PhalApi_Api {

    public function getRules() {
        return array(

            'getShop'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'keywords' => array('name' => 'keywords', 'type' => 'string', 'desc' => '关键字'),
                'p' => array('name' => 'p', 'type' => 'string','default'=>'1',  'desc' => '页数'),
            ),
            'details'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id','require' => true, 'type' => 'string', 'desc' => '关键字'),
            ),
            'getBanner'=>array(
                'id' => array('name' => 'id','require' => true, 'type' => 'string', 'desc' => '关键字'),
            ),
            'buttonStatus'=>array(
                'uid' => array('name' => 'uid', 'type' => 'int','require' => true, 'desc' => '用户ID'),
                'token' => array('name' => 'token', 'type' => 'string','require' => true, 'desc' => '用户token'),
                'id' => array('name' => 'id','require' => true, 'type' => 'string', 'desc' => '商品ID'),
            ),

        );
    }

    public function getShop(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $keywords = $this->keywords;
        $domain = new Domain_Goods();
        $r  = $domain->getList($this->p,$keywords);
        if($r){
            $domainStatus = new Domain_Goods();
            foreach ($r as $k => $v) {
                $res = $domainStatus->checkStatus($uid,$v['id']);
                $r[$k]['button']=$res['status'];
                if($v['store_count'] == 0){
                    $r[$k]['button']= 3;
                }
                $r[$k]['gold'] = intval($v['gold']);
                $r[$k]['gold'] = (string)$r[$k]['gold'];
                $jishi_status = 0;  // -2表示未开启 -1 为预备开始  0表示正在进行 1表示已经结束
                $jishi_number = 0;
                if($v['start'] && $v['end']){
                    if($v['start'] > time()){
                         $jishi_status = -1;
                         $jishi_number = $v['start'] - time();
                         $r[$k]['button']= 4;
                         
                    }elseif($v['start'] < time() && $v['end'] > time()){
                        $jishi_status = 0;
                        $jishi_number = $v['end'] - time();
                        
                    }else{
                        $r[$k]['button']= 5;
                        $jishi_status = 1;
                    }
                }else{
                    $jishi_status = -2;
                }
            }
        }
        $rs['info'][0]['shop_list'] = $r;
        return $rs;

    }
    
    /**
     * 获取礼物详情
     */
    public function details(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Goods();
        $r = $domain->getDetails($this->id);
        $banner = $domain->getBanner($this->id);
        $button = $domain->checkStatus($uid,$this->id);
        if($r['store_count'] == 0){
            $button['status'] = 3;
        }
        $jishi_status = 0;  // -2表示未开启 -1 为预备开始  0表示正在进行 1表示已经结束
        $jishi_number = 0;
        //button 4为准备开始 5为已经结束
        if($r['start'] && $r['end']){
            if($r['start'] > time()){
                 $jishi_status = -1;
                $jishi_number = $r['start'] - time();
                $button['status'] = 4;
            }elseif($r['start'] < time() && $r['end'] > time()){
                $jishi_status = 0;
                $jishi_number = $r['end'] - time();
            }else{
                 $button['status'] = 5;
                 $jishi_status = 1;
            }
        }else{
            $jishi_status = -2;
        }
        $r['jishi'] = [
            'jishi_status' => $jishi_status,
            'jishi_number' => $jishi_number
            ];
        $rs['info'][0]['banner'] = $banner;
        $rs['info'][0]['shop_details'] = $r;
        $rs['info'][0]['button']=$button;
        return $rs;

    }
    /**
     * 礼物详情轮播图
     */
    public function getBanner(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $domain = new Domain_Goods();
        $r = $domain->getBanner($this->id);
        $rs['info'][0]['banner'] = $r;
        return $rs;

    }
    
    public function buttonStatus(){
        $rs = array('code' => 0, 'msg' => '', 'info' => array());
        $uid=checkNull($this->uid);
        $token=checkNull($this->token);
        $id = checkNull($this->id);
        $checkToken=checkToken($uid,$token);
        if($checkToken==700){
            $rs['code'] = $checkToken;
            $rs['msg'] = '您的登陆状态失效，请重新登陆！';
            return $rs;
        }
        $domain = new Domain_Goods();
        $res = $domain->checkStatus($uid,$id);
        $rs['info'][0]['list']=$res;
        return $res;
    }


}