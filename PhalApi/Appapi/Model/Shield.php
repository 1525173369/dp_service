<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 15:38
 */
class Model_Shield extends PhalApi_Model_NotORM {


    public function shieldList($uid,$p){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$res=DI()->notorm->shield
					->select("*")
					->where('uid=?',$uid)
					->limit($start,$pnum)
					->fetchAll();
		foreach($res as $k=>$v){
			$noteinfo=DI()->notorm->dynamic
					->select("*")
					->where('id=?',$v['note_id'])
					->fetchOne();
			if($noteinfo){
				$res[$k]=$noteinfo;
			}else{
				DI()->notorm->shield->where('uid=? or note_id=?',$v['uid'],$v['note_id'])->delete();
				unset($res[$k]);
			}
		}
		$res=array_values($res);
		return $res;
    }

    public function addShield($uid,$note_id)
    {
        DI()->notorm->shield
				->insert(array("uid"=>$uid,"note_id"=>$note_id));
		return 1;
    }

}