<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/4 0004
 * Time: 13:54
 */
class Model_Collect extends PhalApi_Model_NotORM {

    public function collect($data,$lan){

        $where=[
            'user_id'=>$data['user_id'],
            'collect_id'=>$data['collect_id'],
            'type'=>$data['type'],
        ];
        $count = $this->getORM()->where($where)->count();
        if($count>0){
            if($data['type']==1){
                DI()->notorm->dynamic
                    ->where("id = '{$data['collect_id']}'")
                    ->update( array('collect_sum' => new NotORM_Literal("collect_sum - 1") ) );
            }else{
                DI()->notorm->shop_apply
                    ->where("id = '{$data['collect_id']}' ")
                    ->update( array('collect_sum' => new NotORM_Literal("collect_sum - 1") ) );
            }
            $this->getORM()->where($where)->delete();
            return array('status'=>1004,'msg'=>'已取消收藏','result'=>array());
        }
        $users = getUserInfo($data['user_id']);
        if($data['type']==1){




            //帖子收藏
            DI()->notorm->dynamic
                ->where("id=?",$data['collect_id'])
                ->update( array('collect_sum' => new NotORM_Literal("collect_sum + 1") ) );
            DI()->notorm->collect
                ->insert($data);

            $user =  DI()->notorm->dynamic->where("id =?",$data['collect_id'])->fetch();
            if($data['user_id']!=$user['uid']){
                $lang = getUserInfo($user['uid']);
                if($lang['lang']=='en'){
                    $res = TPNSPush('Notification', $users['user_nickname'].' saved your post', $user['uid'], 0, '');
                    $language = 'saved your post';
                }elseif($lang['lang']=='ms'){
                    $res = TPNSPush('Notifikasi', $users['user_nickname'].' telah menyimpankan siaran anda', $user['uid'], 0, '');
                    $language = 'telah menyimpankan siaran anda';
                }else{
                    $res = TPNSPush('您有新的帖子收藏提醒', $users['user_nickname'].'用户收藏了你的帖子', $user['uid'], 0, '');
                    $language = '用户收藏了你的帖子';
                }



                $remindData=[
                    'type'=>0,
                    'target'=>'note',
                    'target_id'=>$data['collect_id'],
                    'target_owner'=>$data['user_id'],
                    'add_time'=>time(),
                    'action'=>'用户收藏了你的帖子',
                    'user_id'=>$user['uid']?$user['uid']:0,
                ];
                DI()->notorm->remind
                    ->insert($remindData);

            }



        }else{
            //店铺收藏
            DI()->notorm->shop_apply
                ->where("id = '{$data['collect_id']}' ")
                ->update( array('collect_sum' => new NotORM_Literal("collect_sum + 1") ) );
            DI()->notorm->collect
                ->insert($data);
            $user =  DI()->notorm->shop_apply->where("id=?",$data['collect_id'])->fetch();
            if($data['user_id']!=$user['uid'] and !empty($user['uid'])){
                $lang = getUserInfo($user['uid']);
                if($lang['lang']=='en'){
                    $res = TPNSPush('Notification', $users['user_nickname'].' saved your store', $user['uid'], 0, '');
                    $language = 'saved your store';
                }elseif($lang['lang']=='ms'){
                    $res = TPNSPush('Notifikasi', $users['user_nickname'].' mengumpulkan kedai anda', $user['uid'], 0, '');
                    $language = 'mengumpulkan kedai anda';
                }else{
                    $res = TPNSPush('您有新的店铺收藏消息', $users['user_nickname'].'用户收藏了你店铺', $user['uid'], 0, '');
                    $language = '用户收藏了你店铺';
                }
                $remindData=[
                    'type'=>0,
                    'target'=>'shop',
                    'target_id'=>$data['collect_id'],
                    'target_owner'=>$data['user_id'],
                    'add_time'=>time(),
                    'action'=>'用户收藏了你店铺',
                    'user_id'=>$user['uid']?$user['uid']:0,
                ];
                DI()->notorm->remind
                    ->insert($remindData);
            }elseif(empty($user['uid']) and $data['user_id']!=$user['provider_id']){
                //$user['provider_id'] = 41443;
                $lang = getUserInfo($user['provider_id']);

                if($lang['lang']=='en'){
                    $res = TPNSPush('Notification', $users['user_nickname'].' saved your store', $user['provider_id'], 0, '');
                    $language = 'saved your store';
                }elseif($lang['lang']=='ms'){
                    $res = TPNSPush('Notifikasi', $users['user_nickname'].' mengumpulkan kedai anda', $user['provider_id'], 0, '');
                    $language = 'mengumpulkan kedai anda';
                }else{
                    $res = TPNSPush('您有新的店铺收藏消息', $users['user_nickname'].'用户收藏了你店铺', $user['provider_id'], 0, '');
                    $language = '用户收藏了你店铺';
                }
                $remindData=[
                    'type'=>0,
                    'target'=>'shop',
                    'target_id'=>$data['collect_id'],
                    'target_owner'=>$data['user_id'],
                    'add_time'=>time(),
                    'action'=>'用户收藏了你店铺',
                    'user_id'=>$user['uid']?$user['uid']:0,
                ];
                DI()->notorm->remind
                    ->insert($remindData);

            }


        }
        return array('status'=>1,'msg'=>'收藏成功!请到个人中心查看','result'=>array());

    }
    //获取http协议
    //获取http协议
    function get_http_type($urls =false)
    {
        if(!$urls){
            return false;
        }
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $url = $http_type.$_SERVER['HTTP_HOST'].'/ffmpeg/';
        if(strpos($urls,'http') === false){  //是否包含
            $url = $url.$urls;
        }else{
            $url = $urls;
        }
        return $url;
    }
    public function getCollect($id,$status,$p){
        if($p<1){
            $p=1;
        }
        $pnums=10;
        $start=($p-1)*$pnums;
        $where="A.user_id=".$id. " and A.type=".$status;
        if($status==1){
            $list = $this->getORM()
                ->select('A.id AS collectID, B.*') // 获取字段
                ->alias('A')
                ->leftJoin('dynamic', 'B', 'A.collect_id = B.id')
                ->where($where)->where("B.id !='' ")
                ->limit($start,$pnums)->order('A.add_time desc')
                ->fetchAll();

            foreach($list as &$val){
                if(!empty($val['thumb'])){
                    $thumb_arr=explode(',',trim($val['thumb'],','));
                    foreach ($thumb_arr as $k => $v) {
                        $thumb_arr[$k]=get_upload_path($v);
                    }
                    $val['thumb']= $thumb_arr;
                }else{
                    $val['thumb']=[];
                }

                if(!empty($val['video'])){
                    $video=explode(',',trim($val['video'],','));
                    foreach ($video as $k => $v) {
                        $video[$k]=get_upload_path($v);
                    }
                    $val['video']= $video;
                }else{
                    $val['video']=[];
                }

                if(!empty($val['video_img'])){
                    $video_img=explode(',',trim($val['video_img'],','));
                    foreach ($video as $k => $v) {
                        $video_img[$k]= $this->get_http_type($v);
                    }
                    $val['video_img']= $video_img;
                }else{
                    $val['video_img']=[];
                }


                $val['topic'] = getShopTopic($val['topic']) ?: [];
                $val['custom'] = getShopTopic($val['custom']) ?: [];
                if($val['uid']){
                    $val['info']=getUserInfo($val['uid']);
                    $val['info']['avatar'] = get_upload_path($val['info']['avatar']);
                    $val['info']['avatar_thumb'] = get_upload_path($val['info']['avatar_thumb']);
                }
                $val['isCollect']=getUserNoteColl($id, $val['id'])?1:0;
                $val['isLike'] =getUserNoteAtte($id,$val['id']);


            }


            return $list;
        }else{
            $list = $this->getORM()
                ->select('A.id AS collectID, B.*') // 获取字段
                ->alias('A')
                ->leftJoin('shop_apply', 'B', 'A.collect_id = B.id')
                ->where($where)->where("B.id !='' ")
                ->limit($start,$pnums)->order('A.add_time desc')
                ->fetchAll();
            foreach($list as &$val){
                $val['topic'] = getShopTopic($val['topic'])?: [];
                $val['custom'] = getShopTopic($val['custom'])?: [];
                if($val['thumb']){
                    $val['thumb']= get_upload_path($val['thumb']);
                }

            }
            return $list;

        }


    }

    public function delCollect($id){
        $list = $this->getORM()->where("id=?",$id)->delete();
        return $list;
    }

    public function isCollect($id,$uid,$status){
        $where = "user_id=".$uid. " and type=".$status." and collect_id=".$id;
        $list = $this->getORM()
            ->where($where)->fetch();
        return $list ? ['isCollect'=>1,'collect_id'=>$list['id']] : ['isCollect'=>0,'collect_id'=>$list['id']];
    }

    public function censusLike($uid,$start,$end){
        $ids = DI()->notorm->shop_apply->where("uid = '{$uid}' ")->select('id')->fetch();
        $sIds = DI()->notorm->dynamic->where("uid = '{$uid}' ")->select('id')->fetch();
        $count1=0;
        $count2=0;
        if($sIds){
            $sIds=implode(",",$sIds);
            $where1 = "collect_id in ({$sIds}) and type=1 and add_time>=".$start." and add_time<=".$end ;
            $count1 = $this->getORM()
                ->where($where1)->count();
        }
        if($ids){
            $ids=implode(",",$ids);
            $where2 = "collect_id in ({$ids}) and type=0 and add_time>=".$start." and add_time<=".$end ;
            $count2 = $this->getORM()
                ->where($where2)->count();
        }
        $count =  $count1+$count2;
        return $count;

    }

    public function censusFollow($uid,$start,$end){
        $where = "touid =".$uid." and add_time>=".$start." and add_time<=".$end ;
        $count = DI()->notorm->user_attention
            ->where($where)->count();
        return $count;

    }


}