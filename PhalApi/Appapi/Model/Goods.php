<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 11:09
 */
class Model_Goods extends PhalApi_Model_NotORM {

    protected function getTableName($id) {
        return 'shop_goods';  // 手动设置表名为
    }

    public function getLists($p,$keywords){
        if($p<1){
            $p=1;
        }
        $pnums=10;
        $start=($p-1)*$pnums;
        $where="exchange_gold=1  and is_on_sale = 1";//兑换的商品
        if($keywords){
           // echo 111;exit;
            $where.=" and goods_name like '%".$keywords."%'";
        }
       // print_r($where);exit;
        $list = $this->getORM()
            ->where($where)
            ->limit($start,$pnums)->order('id desc')
            ->fetchAll();
        foreach($list as $k=>$v){
            $list[$k]['goods_content']=htmlspecialchars_decode($v['goods_content']);
            $list[$k]['original_img']=get_upload_path($v['original_img']);
        }    
        return $list;

    }
    
    public function getDetails($id){
        $where= "id=".$id;
        $list =  $this->getORM()
            ->where($where)
            ->fetch();
        $list['goods_content']= htmlspecialchars_decode($list['goods_content']);
        $list['original_img']= get_upload_path($list['original_img']);   
        return $list;

    }

    public function getBanner($id){
        $where= "goods_id=".$id;
        $list=DI()->notorm->goods_images
            ->where($where)->fetchAll();
        foreach($list as $k=>$v){
            $list[$k]['image_url']=get_upload_path($v['image_url']);
        }   
        return $list;
    }
    
    /**
     * 获取用户今天是否已经购买
     * @param $user_id
     * @return float|int
     */
    public function getUserFlashOrderGoods($uid,$goods_id){
        //今日起始
        $beginToday=mktime(0,0,0,date('m'),date('d'),date('Y'));
        $endToday=mktime(0,0,0,date('m'),date('d')+1,date('Y'))-1;
        $list=DI()->notorm->order
            ->where('user_id=? and pay_time > ? and pay_time < ?',$uid,$beginToday,$endToday)
            ->select('order_id')
            ->fetchAll();
        $newAraay=[];    
        foreach ($list as $val){
           $newAraay[]= $val['order_id']; 
        }    
        $newAraay = implode(",",$newAraay);
        if($list){
            $where = "goods_id= {$goods_id}";
            $where.=" and order_id in ({$newAraay})  ";
           //   print_r($where);exit; 
            $res = DI()->notorm->order_goods->where($where)->fetchAll();
            if($res){
                return 1;
            }else{
                return 0;
            }
        }else{
            return 0;
        }  
            
        

    }


}