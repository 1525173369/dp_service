<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 15:38
 */
class Model_Area extends PhalApi_Model_NotORM {


    public function getLists(){
        $res = DI()->notorm->area->order('pid asc')->fetchAll();
        if(!$res){
            return $res;
        }
        $ms = [];
        $hw = [];
        foreach ($res as $k=>$v){
            if($v['is_hw']){
                $hw[] = $v;
            }else{
                $ms[] = $v;
            }
        }
        
        $list1 = self::xmsb_getDataTree($ms,'id','pid','name','first');
        $list2 = self::xmsb_getDataTree($hw,'id','pid','name','first');
        $list['list1'] = $list1;
        $list['list2'] = $list2;
        return $list;
    }
    /**
     * @desc   xmsb_getDataTree         无限级分类函数
     * @param  array     $datas         查询出的数据
     * @param  string    $primaryKey    数据表的主键
     * @param  string    $parentKey     父节点字段名
     * @param  string    $nameKey       需要用到的名称字段
     * @param  integer   $firstParent   根节点主键值
     * @return array                    返回树状结果集
     */
    public function xmsb_getDataTree($datas, $primaryKey, $parentKey, $nameKey,$first, $firstParent = 0)
    {
        if(!is_array($datas) || !$primaryKey || !$parentKey || !$nameKey || !$first) return false;
        
        $tree = array($primaryKey => 0, $parentKey => 0, $nameKey => 'xmsb',$first=>0);
        $tmpMap = array($firstParent => & $tree);
        
        foreach($datas as $rk => $rv)
        {
            $tmpMap[$rv[$primaryKey]] = array
            (
                $primaryKey => $rv[$primaryKey],
                $parentKey => $rv[$parentKey],
                $nameKey => $rv[$nameKey],
                $first =>$rv[$first]
            );
            
            $parentObj = & $tmpMap[$rv[$parentKey]];
            if(!$parentObj['children']) $parentObj['children'] = array();
            $parentObj['children'][] = & $tmpMap[$rv[$primaryKey]];
        }
        
        return $tree['children'];
}


}