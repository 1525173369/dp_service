<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/29 0029
 * Time: 14:59
 */
class Model_InvitationVisit extends PhalApi_Model_NotORM
{

    protected function getTableName($id)
    {
        return 'invitation_visit';  // 手动设置表名为 
    }

    public function addVisitLog($uid, $note)
    {
        //验证用户是否是实名认证通过
        $domain = new Domain_Userauth();
        $r = $domain->isAuth($uid);
        if ($r['status'] != 1) {
            return;
        }
        if($note['uid'] == $uid){
            return;
        }
        //增加浏览记录start
        $where = "user_id=" . $uid . " and note_id=" . $note['id'];
        $record = DI()->notorm->invitation_visit
            ->where($where)->fetch();

        if ($record) {
            DI()->notorm->invitation_visit
                ->where($where)
                ->update(array('visittime' => time()));
        } else {
            DI()->notorm->invitation_visit
                ->where($where)
                ->insert([
                    'note_id' => $note['id'],
                    'user_id' => $uid,
                    'visittime' => time(),
                    'cat_id' => $note['cat_id']
                ]);
            //增加阅读量
            DI()->notorm->dynamic
                ->where("id=?", $note['id'])
                ->update(array('reading' => new NotORM_Literal("reading + 1")));

        }
        //增加浏览记录end
        //话题增加热度start
        if ($note['custom']) {
            $topic = [];
            foreach ($note['custom'] as $k=>$v){
                $topic[] = $v['id'];
            }
            foreach ($topic as $val) {
                //echo $val;
                DI()->notorm->shop_topic
                    ->where("topic_id=?", $val)
                    ->update(array('topic_num' => new NotORM_Literal("topic_num + 1")));
            }
        }
        //end
        return;

    }

    //获取http协议
    function get_http_type($urls =false)
    {
        if(!$urls){
            return false;
        }
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $url = $http_type.$_SERVER['HTTP_HOST'].'/ffmpeg/';
        if(strpos($urls,'http') === false){  //是否包含
            $url = $url.$urls;
        }else{
            $url = $urls;
        }
        return $url;
    }

    //浏览记录
    public function getHistory($uid, $p)
    {
        if ($p < 1) {
            $p = 1;
        }
        $pnums = 10;
        $start = ($p - 1) * $pnums;
        // $res =  $this->getORM()->select('B.*') // 获取字段
        //         ->alias('A') 
        //         ->leftJoin('dynamic', 'B', 'A.note_id = B.id')
        //       ->limit($start,$pnums)
        //  ->where("A.user_id=?",$uid)->fetchAll();
        /*$sql = 'SELECT  b.*,a.user_id as sid '
            . 'FROM cmf_dynamic AS b LEFT JOIN cmf_invitation_visit AS a '
            . 'ON b.id = a.note_id where a.user_id=' . $uid
            . " order by visittime desc limit {$start},$pnums ";*/

        $sql='select * from cmf_invitation_visit AS v LEFT JOIN cmf_dynamic AS d on d.id = v.note_id where v.user_id='.$uid.' order by visittime desc limit '.$start.','.$pnums;

        $res = $this->getORM()->queryAll($sql, array());

        foreach ($res as $key => $value) {
            /*if($value['id'] == 1677){
                echo "<pre>";
                var_dump($value);
            }*/
            if (!empty($value['thumb'])) {
                $value['thumb'] = explode(",", trim($value['thumb'],','));
                // 帖子图片
                foreach ($value['thumb'] as $k => $v) {
                    $value['thumb'][$k] = get_upload_path($v);
                }
                $res[$key]['thumb'] = $value['thumb'];
            } else {

                $res[$key]['thumb'] = [];
            }
            if (!empty($value['video'])) {
                $value['video'] = explode(",", $value['video']);
                // 帖子图片
                foreach ($value['video'] as $k => $v) {
                    $value['video'][$k] = get_upload_path($v);
                }
                $res[$key]['video'] = $value['video'];
            } else {
                $res[$key]['video'] = [];
            }

            if(!empty($value['video_img'])){
                $value['video_img'] = explode(",",$value['video_img']);
                foreach ($value['video_img'] as $k => $v) {
                    $value['video_img'][$k] =$this->get_http_type($v);
                }
                $res[$key]['video_img'] = $value['video_img'];
            } else {
                $res[$key]['video_img'] = [];
            }


            $res[$key]['topic'] = getShopTopic($value['topic']) ?: [];
            $user = getUserInfo($value['uid']);
            //print_r($user);
            $res[$key]['user_nickname'] = $user['user_nickname'];
            $res[$key]['avatar'] = get_upload_path($user['avatar']);
            $res[$key]['isCollect'] = getUserNoteColl($uid, $value['id']) ? 1 : 0;
            $res[$key]['isLike'] = getUserNoteAtte($uid, $value['id']);

        }
      
        return $res;
    }

    public function censusRead($uid, $start, $end)
    {
        $count = 0;
        $where = "user_id=" . $uid . " and visittime>=" . $start . " and visittime<=" . $end;
        //print_r($where);exit;
        $count = DI()->notorm->invitation_visit->where($where)->count();


        return $count;

    }


}