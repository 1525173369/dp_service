<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/3 0003
 * Time: 17:37
 */
class Model_NoteComment extends PhalApi_Model_NotORM
{


    protected function getTableName($id)
    {
        return 'note_comment';  // 手动设置表名为
    }

    public function getShopComment($id, $uid, $p)
    {
        if ($p < 1) {
            $p = 1;
        }
        $dynamic = DI()->notorm->dynamic->where('id = ?',$id)->fetchOne();
        $hoster = $dynamic['uid'] ; //发帖者
        $pnums = 10;
        $start = ($p - 1) * $pnums;
        $where = "A.is_show=0 and A.note_id=" . $id;
        $list = $this->getORM()
            ->select('A.*, B.avatar,B.user_nickname') // 获取字段
            ->alias('A')
            ->leftJoin('user', 'B', 'A.user_id = B.id')
            ->where($where)
            ->limit($start, $pnums)->order('A.create_time desc')
            ->fetchAll();


        foreach ($list as $k => $v) {

            $list[$k]['create_time'] = datetime($v['create_time']);
            $list[$k]['avatar'] = get_upload_path($v['avatar']);
            $list[$k]['is_like'] = 0;
            $list[$k]['content'] = $v['content'];
            if ($v['zan_userid']) {
                if (in_array($uid, explode(',', $v['zan_userid']))) {
                    $list[$k]['is_like'] = 1;
                }
            }
            $list[$k]['is_delete'] = 0;
            if($hoster == $uid){
                $list[$k]['is_delete'] = 1;
            }
            if($v['user_id'] == $uid){
                $list[$k]['is_delete'] = 1;
            }

        }
        if($list){
            $tree = $this->xmsb_getDataTree($list,'comment_id','cover_id','user_nickname','note_id','username','content','create_time','is_show','label','user_id','img','service_rank','zan_num','zan_userid','is_anonymous','avatar','user_nickname','is_like');
        }
        foreach($list as $k => $v){
            if($v['id'] == $tree[$k]['id']){
                $tree[$k]['avatar'] = get_upload_path($tree[$k]['avatar']);
                $list[$k]['children'] = $tree[$k]['children'];
            }
        }
        array_multisort($list,SORT_DESC);
        $count = $this->getORM()
            ->select('A.*, B.avatar,B.user_nickname') // 获取字段
            ->alias('A')
            ->leftJoin('user', 'B', 'A.user_id = B.id')
            ->where($where)
            ->order('A.create_time desc')
            ->count();
        $info['count'] = $count;
        $info['list'] = $list;
        return $info;

    }

    /**
     * @desc   xmsb_getDataTree         无限级分类函数
     * @param array $datas 查询出的数据
     * @param string $primaryKey 数据表的主键
     * @param string $parentKey 父节点字段名
     * @param string $nameKey 需要用到的名称字段
     * @param integer $firstParent 根节点主键值
     * @return array                    返回树状结果集
     */
    public function xmsb_getDataTree($datas, $primaryKey, $parentKey, $nameKey,$note_id,$username,$content,$create_time,$is_show,$label,$user_id,$img,$service_rank,$zan_num,$zan_userid,$is_anonymous,$avatar,$user_nickname,$is_like ,$firstParent = 0)
    {
        if (!is_array($datas) || !$primaryKey || !$parentKey || !$nameKey) return false;

        $tree = array($primaryKey => 0, $parentKey => 0, $nameKey => 'xmsb');
        $tmpMap = array($firstParent => & $tree);

        foreach ($datas as $rk => $rv) {
            $tmpMap[$rv[$primaryKey]] = array
            (
                $primaryKey => $rv[$primaryKey],
                $parentKey => $rv[$parentKey],
                $nameKey => $rv[$nameKey],
                $note_id => $rv[$note_id],
                $username => $rv[$username],
                $content => $rv[$content],
                $create_time => $rv[$create_time],
                $is_show => $rv[$is_show],
                $label => $rv[$label],
                $user_id => $rv[$user_id],
                $img => $rv[$img],
                $service_rank => $rv[$service_rank],
                $zan_num => $rv[$zan_num],
                $zan_userid => $rv[$zan_userid],
                $is_anonymous => $rv[$is_anonymous],
                $avatar => $rv[$avatar],
                $user_nickname => $rv[$user_nickname],
                $is_like => $rv[$is_like],
            );

            $parentObj = &$tmpMap[$rv[$parentKey]];
            if (!$parentObj['children']) $parentObj['children'] = array();
            $parentObj['children'][] = &$tmpMap[$rv[$primaryKey]];
        }

        return $tree['children'];
    }

    public function getMyComment($uid, $p)
    {
        if ($p < 1) {
            $p = 1;
        }
        $pnums = 10;
        $start = ($p - 1) * $pnums;
        $list = $this->getORM()->where('user_id=?', $uid)
            ->limit($start, $pnums)->order('create_time desc')
            ->fetchAll();
        foreach ($list as $k => $v) {
            $list[$k]['create_time'] = date("Y-m-d H:i", $v['create_time']);

            $name = getUserInfo($v['user_id']);
            $list[$k]['avatar'] = get_upload_path($name['avatar']) ?: '';
            $list[$k]['img'] = get_upload_path($v['img']) ?: '';
            $list[$k]['user_nickname'] = $name['user_nickname'];
            $list[$k]['content'] = base64_decode($v['content']);
        }
        return $list;
    }

}