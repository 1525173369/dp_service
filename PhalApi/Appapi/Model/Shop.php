<?php
session_start();
class Model_Shop extends PhalApi_Model_NotORM {

    //检测用户是否缴纳保证金
    public function getBond($uid){
        // $info=DI()->notorm->shop_bond->where("uid=?",$uid)->fetchOne();
        // if(!$info){
        //     return -1;
        // }

        // if($info['status']==0){
        //     return 1;
        // }

        return 2;
    }

    //缴纳保证金
    public function deductBond($uid,$shop_bond){

        //检测用户是否已经缴纳
        $info=DI()->notorm->shop_bond->where("uid=? and status !=0",$uid)->fetchOne();
        if($info){
            return 1001;
        }

        $isok=DI()->notorm->user->where("id=? and coin>=?",$uid,$shop_bond)->update(array('coin' => new NotORM_Literal("coin - {$shop_bond}")) );
        if(!$isok){
            return 1002;
        }

        //判断是否存在保证金已退回的情况
        $info2=DI()->notorm->shop_bond->where("uid=? and status =0",$uid)->fetchOne();
        if($info2){

            //更新保证金记录
            $data=array(
                "bond"=>$shop_bond,
                "status"=>1,
                "addtime"=>time(),
                "uptime"=>0 
            );

            $res=DI()->notorm->shop_bond->where("uid=?",$uid)->update($data);

        }else{
            $data=array(
                "uid"=>$uid,
                "bond"=>$shop_bond,
                "status"=>1,
                "addtime"=>time(),
                "uptime"=>time() 
            );

            $res=DI()->notorm->shop_bond->insert($data);
        }

        if(!$res){
            return 1003;
        }

        //写入消费记录
        $data1=array(
            "type"=>'0',
            "action"=>'14',
            "uid"=>$uid,
            "touid"=>$uid,
            "giftid"=>0,
            "giftcount"=>1,
            "totalcoin"=>$shop_bond,
            "addtime"=>time()
        );

        DI()->notorm->user_coinrecord->insert($data1);

        return 1;
    }

    //获取一级商品分类
    public function getOneGoodsClass(){
        $list=[];

        $list1=DI()->notorm->shop_goods_class->select("gc_one_id")->where("gc_grade=3")->fetchAll();

        if(!$list1){
            return [];
        }

        $gc_one_ids=array_column($list1,"gc_one_id");

        $ids=array_unique($gc_one_ids);

        $list=DI()->notorm->shop_goods_class->select("gc_id,gc_name,gc_isshow")->where("gc_id",$ids)->where(" gc_isshow=1")->order("gc_sort")->fetchAll();
        return $list;
    }

    /*获取店铺认证信息*/
    public function getShopApplyInfo($uid){

        $res=array(
            'apply_status'=>'0',
            'apply_info'=>[]
        );

        $info=DI()->notorm->shop_apply
                ->where("uid=?",$uid)
                ->fetchOne();

        if(!$info){
            $res['apply_status']='-1';
            return $res;
        }

        unset($info['name'],$info['thumb'],$info['des'],$info['license']);

        $info['certificate_format']=get_upload_path($info['certificate']); //营业执照
        $info['other_format']=get_upload_path($info['other']); //其他证件

        //获取用户的经营类目
        // $goods_classid=DI()->notorm->seller_goods_class->select("goods_classid as gc_id")->where("uid=? and status=1",$uid)->fetchAll();
        
        // $info['goods_classid']=$goods_classid;
        
        $shop_classid = DI()->notorm->shop_class->select('*')->where('id=?',$classid)->fetchOne();

        $status=$info['status'];

        if($status==0){ //审核中
            $res['apply_status']='0';
            return $res;
        }else if($status==1){ //审核通过
            $res['apply_status']='1';
            $res['apply_info']=$info;
            return $res;
        }else if($status==2){ //审核拒绝
            $res['apply_status']='2';
            $res['apply_info']=$info;
            return $res;
        }

        

        return $res;
    }

    /*店铺申请*/
    public function shopApply($uid,$data,$apply_status,$classid){

        if($apply_status==-1){ //无申请记录
            $res=DI()->notorm->shop_apply->insert($data);
        }

        if($apply_status==2){
            $res=DI()->notorm->shop_apply->where("uid={$uid}")->update($data);
        }

        if(!$res){
            return 1001;
        }

        if($apply_status=1){

            //写入店铺总评分记录
            // $data1=array(
            //     'shop_uid'=>$uid
            // );

            // DI()->notorm->shop_points->insert($data1);
        }

        //更新商家经营类目
        // DI()->notorm->seller_goods_class->where("uid=?",$uid)->delete();
        // foreach ($classid_arr as $k => $v) {
        //     if($v){
        //         $data1=array(
        //             'uid'=>$uid,
        //             'goods_classid'=>$v,
        //             'status'=>1
        //         );
        //         DI()->notorm->seller_goods_class->insert($data1);
        //     }
        // }
        

        return 1;
    }


	/* 商铺信息 */
	public function getShop($uid,$fields='') {

        if(!$fields){
            // $fields='uid,name,thumb,des,username,phone,cardno,country_code,province,city,area';
            $fields = '*';
        }

        $shop_info=DI()->notorm->shop_apply
                    ->select($fields)
                    ->where('uid=?',$uid)
                    ->fetchOne();

        if(!$shop_info){
            return [];
        }

        //获取用户信息
        $userinfo=getUserInfo($uid);
        $shop_info['user_nickname']=$userinfo['user_nickname']; //用于进入私信聊天顶部显示昵称
        // $shop_info['name']=$userinfo['user_nickname'].'的小店';

        // if($shop_info['certificate']){
        //   $shop_info['certificate']=get_upload_path($shop_info['certificate']);
        // }

        // if($shop_info['other']){
        //     $shop_info['other']=get_upload_path($shop_info['other']);
        // }
        
        // $shop_info['sale_nums']=NumberFormat($shop_info['sale_nums']);
        // $shop_info['avatar']=get_upload_path($userinfo['avatar']);
        // $shop_info['composite_points']=(string)number_format(($shop_info['quality_points']+$shop_info['service_points']+$shop_info['express_points'])/3,'1');
        // $shop_info['composite_points']=$shop_info['composite_points']==0?'0.0':$shop_info['composite_points'];
        // $shop_info['quality_points']=$shop_info['quality_points']>0?(string)$shop_info['quality_points']:'暂无评分';
        // $shop_info['service_points']=$shop_info['service_points']>0?(string)$shop_info['service_points']:'暂无评分';
        // $shop_info['express_points']=$shop_info['express_points']>0?(string)$shop_info['express_points']:'暂无评分';

        // //获取店铺的上架产品总数
        // $where=[];
        // $where['uid']=$uid;
        // $where['status']=1;

        // $count=$this->countGoods($where);
        // $shop_info['goods_nums']=$count;
        // $shop_info['address_format']=$shop_info['city'].$shop_info['area'];

        // //获取后台配置的店铺资质说明
        // $configpri=getConfigPri();
        // $shop_info['certificate_desc']=$configpri['shop_certificate_desc'];
		return $shop_info;
	}

     
    /* 商品总数 */
    public function countGoods($where=[]){

        $nums=DI()->notorm->shop_goods
                ->where($where)
                ->count();

        
        return (string)$nums;
    }

    /* 获取商品信息 */
    public function getGoods($where=[]){
        
        $info=[];
        
        if($where){
            $info=DI()->notorm->shop_goods
                    ->where($where)
                    ->fetchOne();
        }

        return $info;
    }

    /* 更新商品信息 */
    public function upGoods($where=[],$data=[]){
        $result=false;
        
        if($data){
            $result=DI()->notorm->shop_goods
                    ->where($where)
                    ->update($data);
        }

        return $result;
    }



    //获取商品列表
    public function getGoodsList($where,$p){
        

        $list=handleGoodsList($where,$p);
        foreach ($list as $k => $v) {
           unset($list[$k]['specs']);
        }

        return $list;
    }

    //获取商品评价总数
    public function getGoodsCommentNums($goodsid){
        $count=DI()->notorm->shop_order_comments->where("goodsid=? and is_append=0",$goodsid)->count();
        return $count;
    }

    //获取商品最新的三条评价
    public function getTopThreeGoodsComments($goodsid){
        $list=DI()->notorm->shop_order_comments
                ->where("goodsid=? and is_append=0",$goodsid)
                ->order("addtime desc")
                ->limit(0,3)
                ->fetchAll();


        if($list){
            foreach ($list as $k => $v) {
                $list[$k]=handleGoodsComments($v);
                //获取评论的追评信息
                //$append_comment=getGoodsAppendComment($v['uid'],$v['orderid']);
                $list[$k]['append_comment']=(object)[];
                /*if($append_comment){
                    $list[$k]['append_comment']=handleGoodsComments($append_comment); 
                }*/
                
            }
        }

        return $list;
    }


    //获取商品评论列表
    public function getGoodsCommentList($uid,$goodsid,$type,$p){

        if($p<1){
            $p=1;
        }

        $pnums=50;

        $where="goodsid={$goodsid} and is_append=0";

        switch ($type) {

            case 'all':
                //$where="goodsid={$goodsid}";
                break;
            case 'img':
                $where="goodsid={$goodsid} and is_append=0 and thumbs !=''";
                break;

            case 'video':
                $where="goodsid={$goodsid} and is_append=0 and video_url !=''";
                break;

            case 'append':

                //获取有追评的评论订单ID
                $orderids=DI()->notorm->shop_order_comments->where("goodsid={$goodsid} and is_append=1")->select("orderid")->fetchAll();
                if($orderids){
                    $orderid_arr=array_column($orderids, 'orderid');

                }else{

                    return [];
                }

                
                break;
            
        }

        if($p>1){
            $goodscomment_endtime=$_SESSION['goodscomment_endtime'];
            if($goodscomment_endtime){
                $where.=" and addtime<".$goodscomment_endtime;
            }
            
        }


        if($type=='append'){

            $list=DI()->notorm->shop_order_comments
                ->where($where)
                ->where('orderid',$orderid_arr)
                ->order("addtime desc")
                ->limit(0,$pnums)
                ->fetchAll();


        }else{

            $list=DI()->notorm->shop_order_comments
                ->where($where)
                ->order("addtime desc")
                ->limit(0,$pnums)
                ->fetchAll();
        }

        foreach ($list as $k => $v) {
            $v=handleGoodsComments($v);
            $list[$k]=$v;
            $list[$k]['has_append_comment']='0';
            //获取评论的追评信息
            $append_comment=getGoodsAppendComment($v['uid'],$v['orderid']);

            $list[$k]['append_comment']=(object)[];

            if($append_comment){

                $list[$k]['has_append_comment']='1';
                $cha=$append_comment['addtime']-$v['addtime'];

                if($cha<24*60*60){
                    $append_comment['date_tips']='当日评论';
                }else{
                    
                    $append_comment['date_tips']=floor($cha/(24*60*60)).'天后评论';
                }

                $list[$k]['append_comment']=handleGoodsComments($append_comment);
            }
            
        }

        $end=end($list);
        if($end){
            $_SESSION['goodscomment_endtime']=$end['addtime'];
        }

        return $list;

    }


    //获取商品评论不同类型下的评论总数
    public function getGoodsCommentsTypeNums($goodsid){

        $data=array();

        $data['all_nums']='0';
        $data['img_nums']='0';
        $data['video_nums']='0';
        $data['append_nums']='0';

        
        $all_nums=DI()->notorm->shop_order_comments->where("goodsid=? and is_append=0",$goodsid)->count();

        $img_nums=DI()->notorm->shop_order_comments->where("goodsid=? and is_append=0 and thumbs !=''",$goodsid)->count();

        $video_nums=DI()->notorm->shop_order_comments->where("goodsid=? and is_append=0 and video_url !=''",$goodsid)->count();
        
        $append_nums=DI()->notorm->shop_order_comments->where("goodsid=? and is_append=1 ",$goodsid)->count();
                

        $data['all_nums']=$all_nums;
        $data['img_nums']=$img_nums;
        $data['video_nums']=$video_nums;
        $data['append_nums']=$append_nums;

        return $data;

    }

    public function searchShopGoods($uid,$keywords,$p){
        if($p<1){
            $p=1;
        }

        $pnums=50;
        $start=($p-1)*$pnums;

        $where="uid={$uid} and status=1";

        if($keywords!=''){
            $where.=" and name like '%".$keywords."%'";
        }

        $list=DI()->notorm->shop_goods
                ->select("id,specs,name,addtime")
                ->where($where)
                ->order("addtime desc")
                ->limit($start,$pnums)
                ->fetchAll();

        foreach ($list as $k => $v) {
            $goods_info=handleGoods($v);
            $list[$k]['price']=$goods_info['specs_format'][0]['price'];
            $list[$k]['thumb']=$goods_info['specs_format'][0]['thumb'];
            unset($list[$k]['addtime']);
            unset($list[$k]['specs']);
        }

        return $list;
    }
    
    
    
    //获取店铺列表
    public function getShopList($uid,$classid,$p) {
        
        if($p<1){
            $p=1;
        }

        $pnums=20;
        $start=($p-1)*$pnums;

        // if(!$fields){
        //     $fields='uid,sale_nums,quality_points,service_points,express_points,certificate,other,service_phone,province,city,area';
        // }

        $shop_list_info=DI()->notorm->shop_apply
                    ->select($fields)
                    ->where('status = 1 and classid=?',$classid)
                    ->limit($start,$pnums)
                    ->fetchAll();

        

        if(!$shop_list_info){
            return [];
        }

        //获取用户信息
        // $userinfo=getUserInfo($uid);
        // $shop_list_info['user_nickname']=$userinfo['user_nickname']; //用于进入私信聊天顶部显示昵称
        // $shop_list_info['name']=$userinfo['user_nickname'].'的小店';


        
        // var_dump($shop_list_info);die;
        
        foreach ($shop_list_info as $k => $v) {
            // var_dump($k['certificate']);
            // var_dump($v['certificate']);
            // if($shop_list_info[$k]['certificate']){
            //     $shop_list_info[$k]['certificate']=get_upload_path($v['certificate']);
            // }
            // if($v['other']){
            //     $v['other']=get_upload_path($v['other']);
            // }
            // fabulous
            
            //判断用户是否关注了店铺主播
            $isattention=isAttention($uid,$shop_list_info[$k]['uid']);
            $shop_list_info[$k]['isattention']=$isattention;
            $userinfo = getUserInfo($shop_list_info[$k]['uid']);
            // var_dump($shop_list_info)
            // var_dump($userinfo);die;
            $shop_list_info[$k]['user_nickname'] = $userinfo['user_nickname'];
            $shop_list_info[$k]['avatar'] = get_upload_path($userinfo['avatar']);
            $shop_list_info[$k]['avatar_thumb'] = get_upload_path($userinfo['avatar_thumb']);
            
            
            //获取店铺点赞信息
            $shop_fabulous = getShopFabulous($shop_list_info[$k]['uid'], $uid);
            $shop_list_info[$k]['fabulous'] = $shop_fabulous['fabulous'];
            $shop_list_info[$k]['isfabulous'] = $shop_fabulous['isfabulous'];
            
            
            //获取店铺话题
            $shop_list_info[$k]['topic'] = getShopTopic($shop_list_info[$k]['topic']);
            
            
            // $topic=array_column($list,'id');
        
            // $uids_s=implode(',',$uids);
            
            // $topic = explode(',', $shop_list_info[$k]['topic']);
            // var_dump($topic);die;
            
            // $where="uid!={$uid} and uid in ({$uids_s})";
    
        }
        
        // var_dump($shop_list_info);die;

        if($shop_list_info['certificate']){
           $shop_list_info['certificate']=get_upload_path($shop_list_info['certificate']);
        }

        if($shop_list_info['other']){
            $shop_list_info['other']=get_upload_path($shop_list_info['other']);
        }
        
        // $shop_list_info['sale_nums']=NumberFormat($shop_list_info['sale_nums']);
        // $shop_list_info['avatar']=get_upload_path($shop_list_info['avatar']);
        // $shop_list_info['composite_points']=(string)number_format(($shop_list_info['quality_points']+$shop_list_info['service_points']+$shop_list_info['express_points'])/3,'1');
        // $shop_list_info['composite_points']=$shop_list_info['composite_points']==0?'0.0':$shop_list_info['composite_points'];
        // $shop_list_info['quality_points']=$shop_list_info['quality_points']>0?(string)$shop_list_info['quality_points']:'暂无评分';
        // $shop_list_info['service_points']=$shop_list_info['service_points']>0?(string)$shop_list_info['service_points']:'暂无评分';
        // $shop_list_info['express_points']=$shop_list_info['express_points']>0?(string)$shop_list_info['express_points']:'暂无评分';

        // unset($shop_list_info['quality_points']);
        // unset($shop_list_info['service_points']);
        // unset($shop_list_info['express_points']);

        //获取后台配置的店铺资质说明
        // $configpri=getConfigPri();
        // $shop_list_info['certificate_desc']=$configpri['shop_certificate_desc'];
        
        // var_dump($shop_list_info);die;
            
		return $shop_list_info;
	}
	
	
	/* 关注用户 商家列表*/
	public function getAttentionShop($uid,$p){
        if($p<1){
            $p=1;
        }
		$nums=20;
		$start=($p-1)*$nums;
	
		$AttShop=array();
		$attention=DI()->notorm->user_attention
				->select("touid")
				->where("uid='{$uid}'")
				->fetchAll();
		
		if($attention){
			$uids=array_column($attention,'touid');
			$touids=implode(",",$uids);
// 			$where="uid in ({$touids}) and  isdel=0 and status=1";
            $where="uid in ({$touids}) and status=1";
			
			$AttShop=DI()->notorm->shop_apply
					->select("*")
					->where($where)
					->order("addtime desc")
					->limit($start,$nums)
					->fetchAll();
			if(!$AttShop){
				return array();
			}
			
			foreach ($AttShop as $k => $v) {
    			//获取店铺点赞信息
                $shop_fabulous = getShopFabulous($AttShop[$k]['uid'], $uid);
                $AttShop[$k]['fabulous'] = $shop_fabulous['fabulous'];
                $AttShop[$k]['isfabulous'] = $shop_fabulous['isfabulous'];
                
                //获取店铺话题
                $AttShop[$k]['topic'] = getShopTopic($AttShop[$k]['topic']);
			}
		}
	
		return $AttShop;		
	} 		
	
	/* 附近商铺 */
    public function getNearbyShop($lng,$lat,$p) {
        if($p<1){
            $p=1;
        }
		$pnum=20;
		$start=($p-1)*$pnum;
// 		$where=" islive='1' and lng!='' and lat!='' ";
        $where=" status='1' and lng!='' and lat!='' ";
		
		$result=DI()->notorm->shop_apply
				->select("*,getDistance('{$lat}','{$lng}',lat,lng) as distance,province")
				->where($where)
                ->order("distance asc")
                ->limit($start,$pnum)
				->fetchAll();
				
				
// 		var_dump($result);die;
		
		foreach($result as $k=>$v){
            
// 			$v=handleLive($v);
            
            if($v['distance']>1000){
                $v['distance']=1000;
            }
            $v['distance']=$v['distance'].'km';

            $result[$k]=$v;
            
            $shop_fabulous = getShopFabulous($result[$k]['uid'], $uid);
            $result[$k]['fabulous'] = $shop_fabulous['fabulous'];
            $result[$k]['isfabulous'] = $shop_fabulous['isfabulous'];
            
            //获取店铺话题
            $result[$k]['topic'] = getShopTopic($result[$k]['topic']);
		}
		
		return $result;
    }
    
    
    /* 用户点赞商铺 */
	public function addFabulous($uid,$touid){
		$rs=array(
			'isfabulous'=>'0',
			'fabulous'=>'0',
		);
  
		$shop_info = $this->getShop($touid);
        if(!$shop_info){
			return 1001;
        }
        
		if($shop_info['uid']==$uid){
			return 1002;//不能给自己点赞
		}
		
        $isFabulous = getShopFabulous($touid,$uid);
        
        if($isFabulous['isfabulous'] == '1'){
            /* 已点赞 - 取消 */
            $this->reduceFabulous($uid,$touid);
            $nums=$isFabulous['fabulous']-1;
        }else{
            /* 未点赞 - 添加 */
            $this->addtoFabulous($uid,$touid);
            $nums=$isFabulous['fabulous']+1;
        }
        
        $rs['isfabulous']=$isFabulous['isfabulous']?'0':'1';
        
        $rs['fabulous']=NumberFormat($nums);
		
		return $rs; 		
	}
	
	
	/* 用户点赞商铺 点赞+ */
	public function addtoFabulous($uid,$touid) {
        $rs = DI()->notorm->shop_thumbs
                    ->insert(array("uid"=>$uid,"touid"=>$touid,"addtime"=>time() ));
		return $rs;
	}
    
    /* 用户点赞商铺 点赞- */
	public function reduceFabulous($uid,$touid) {
        $rs = DI()->notorm->shop_thumbs
                    ->where("uid='{$uid}' and touid='{$touid}'")
                    ->delete();
                            
		return $rs;
	}
	
	
	/* 用户搜索话题相关商铺和帖子 */
    public function shopTopicSearch($uid,$topic_id,$p) {
        if($p<1){
            $p=1;
        }
		$pnum=20;
		$start=($p-1)*$pnum;
        $where = 'status = "1"';
		
		$shop_topic = DI()->notorm
	                    ->shop_topic
		                ->select('*')
		                ->where('topic_id = ?', $topic_id)
		                ->fetchOne();
		
		$shop_topic = DI()->notorm
                ->shop_topic
                ->select('*')
                ->where('topic_name = ? or topic_name like ?', $shop_topic['topic_name'], '%'. $shop_topic['topic_name'] . '%')
                ->fetchOne();
                
        $shop_apply_topic = DI()->notorm->shop_apply
                ->select('*')
                ->where('status = 1')
                // ->order("uid desc")
                ->limit($start,$pnum)
                ->fetchAll();
        
        
        $info = [];
        
        foreach ($shop_apply_topic as $key => $value) {
            $topics = explode(',', $value['topic']);
            
                foreach ($topics as $x => $y) {
                    if($y == $topic_id) {
                        
                        $info[] = $value;
                        
                        $shop_fabulous = getShopFabulous($shop_apply_topic[$key]['uid'], $uid);
                        $info[]['fabulous'] = $shop_fabulous['fabulous'];
                        $info[]['isfabulous'] = $shop_fabulous['isfabulous'];
                        
                        //获取店铺话题
                        $info[]['topic'] = getShopTopic($shop_apply_topic[$key]['topic']);
                    }
                }
                
        }
		return $info;
    }
	
	
	//获取热门话题
	public function getHotTopic() {
	    $info  = DI()->notorm->shop_topic
	            ->select('topic_id, topic_name, topic_num, addtime')
	            ->order('topic_num desc')
	            ->limit(0, 6)
	            ->fetchAll();
	            
        return $info;
	            
	    
	    
	}
    
//     /* 获取获取商铺信息 */
// 	public function getShopInfo($uid,$touid,$where='status=1') {
// 		$info=DI()->notorm->shop_apply
//                 ->select('id,uid,title,thumb,video_thumb,href,voice,length,likes,comments,type')
// 				->where('id = ?',$touid)
//                 ->where($where)
// 				->fetchOne();
// 		if($info){
// 			$info=handleDynamic($uid,$info);
// 		}
// 		return $info;
// 	}

    //获取未认证列表
    public function getShopLists($keywords,$p){
        if($p<1){
            $p=1;
        }
        $pnums=10;
        $start=($p-1)*$pnums;
        $where="claim !=1 ";
        if($keywords!=''){
            $where.=" and name like '%".$keywords."%'";
        }
        $list=DI()->notorm->shop_apply
            ->where($where)
            ->limit($start,$pnums)->order('id desc')
            ->fetchAll();
            
        foreach ($list as $key => $value) {
            $list[$key]['thumb'] = get_upload_path($value['thumb']);
            $list[$key]['topic'] = getShopTopic($value['topic']) ?: [];  
        }    
        
        
        return $list;
    }
    
    //贡献店铺
    public function devote($uid,$data){
        if($data['thumb']){
            $res = explode(',',$data['thumb']);
            if(count($res)>1){
                $data['thumb'] = array_shift($res);
                $data['photo']=implode(',',$res);
            }
        }
        $data['uid'] = 0;
        $res=DI()->notorm->shop_apply->insert($data);
        if(!$res){
            return 1001;
        }
        return 1;
        
    }
    
   //获取自己贡献的列表
    public function getUserShopLists($uid,$p){
        if($p<1){
            $p=1;
        }
        $pnums=10;
        $start=($p-1)*$pnums;
        $where="provider_id=".$uid;
        $list=DI()->notorm->shop_apply
            ->where($where)
            ->limit($start,$pnums)->order('id desc')
            ->fetchAll();
        foreach($list as &$v){
            $v['thumb'] = $v['thumb'] ? get_upload_path($v['thumb']) : '';
            $v['topic'] = getShopTopic($v['topic']) ?: [];
            // $v['custom'] = getShopTopic($v['custom']) ?: [];
        }    
        return $list;
    }
    
    //添加/修改商家认领验证
    public function addProving($data){
        $info=DI()->notorm->attestation
            ->where('uid=?',$data['uid'])
            ->where('shop_id=?',$data['shop_id'])
            ->fetchOne();
        if($info && $info['status'] == 0){
            return 1002;
        }
        if($info && $info['status']==2){
            $data['status']=0;
            $res = DI()->notorm->attestation
            ->where('uid=?',$data['uid'])
            ->where('shop_id=?',$data['shop_id'])
            ->update($data);
        }else{
             $res=DI()->notorm->attestation->insert($data);
        }
       
        if(!$res){
            return 1001;
        }
        return 1;
    }
    
}
