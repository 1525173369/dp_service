<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 15:38
 */
class Model_Ad extends PhalApi_Model_NotORM {


    public function getLists(){
        $res = DI()->notorm->ad->fetchAll();
        foreach ($res as &$val){
            
             $val['thumb']= get_upload_path($val['thumb']);  
            
        }
        return $res;
    }

    public function noteAd($id){
        $res = DI()->notorm->ad->where('note_id=?',$id)->fetch();
        $new=[];
        if($res){
            if($res['name1']){
                $new[]=['name'=>$res['name1'],'thumb'=>get_upload_path($res['thumb1']),url=>$res['url1']];
            }
            if($res['name2']){
                $new[]=['name'=>$res['name2'],'thumb'=>get_upload_path($res['thumb2']),url=>$res['url2']];
            }
            if($res['name3']){
                $new[]=['name'=>$res['name3'],'thumb'=>get_upload_path($res['thumb3']),url=>$res['url3']];
            }
            if($res['name4']){
                $new[]=['name'=>$res['name4'],'thumb'=>get_upload_path($res['thumb4']),url=>$res['url4']];
            }
        }
        return $new;
    }

    public function shopAd($id){
        $res = DI()->notorm->ad->where('shop_id=?',$id)->fetch();
        $new=[];
        if($res){
            if($res['name1']){
                $new[]=['name'=>$res['name1'],'thumb'=>get_upload_path($res['thumb1']),'url'=>$res['url1']];
            
            }
            if($res['name2']){
                $new[]=['name'=>$res['name2'],'thumb'=>get_upload_path($res['thumb2']),'url'=>$res['url2']];
            }
            if($res['name3']){
                $new[]=['name'=>$res['name3'],'thumb'=>get_upload_path($res['thumb3']),'url'=>$res['url3']];
            }
            if($res['name4']){
                $new[]=['name'=>$res['name4'],'thumb'=>get_upload_path($res['thumb4']),'url'=>$res['url4']];
            }
        }
        return $new;
    }


}