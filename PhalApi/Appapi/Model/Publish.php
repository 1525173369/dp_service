<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/2 0002
 * Time: 9:46
 */
class Model_Publish extends PhalApi_Model_NotORM
{

    protected function getTableName($id)
    {
        return 'dynamic';  // 手动设置表名为
    }




    public function setDynamic($data)
    {
        //审核开关
//        $configpri=getConfigPri();
//        $dynamic_auth=$configpri['dynamic_auth'];
//        if($dynamic_auth=='1'){//动态发布认证关闭
//            $isauth=isAuth($uid);
//            if(!$isauth){
//                return 1003;
//            }
//        }
        if ($data['id'] != null) {
            //$data['addtime'] = time();
            $id = $data['id'];
            unset($data['id']);
            $data['content'] = stripcslashes($data['content']);
            $result = DI()->notorm->dynamic->where("id = '{$id}'")->update($data);
        } else {
            unset($data['id']);
            $data['cat_id'] = $data['cat_id'] ?: 0;
//            var_dump($data);die;
            $data['content'] = stripcslashes($data['content']);

            $result = DI()->notorm->dynamic->insert($data);



            $config = DI()->notorm->option
                ->select('option_value')
                ->where("option_name='comment_app'")
                ->fetchOne();
            $config = json_decode($config['option_value'], true);
            if ($config['publish'] > 0) {
                addMoney($data['uid'], [
                    'title' => '发布帖子',
                    'money' => $config['publish'],
                ]);
            }


            if ($data['status'] == 1) {
                if ($data['shop_id'] && $data['points']) {
                    $shop = DI()->notorm->shop_apply->where("id = '{$data['shop_id']}'")->fetch();
                    if ($shop['points'] > 0) {
                        $newPoints = ($shop['points'] + $data['points']) / 2;
                        $newPoints = number_format($newPoints, 1);
                        DI()->notorm->shop_apply->where("id=?", $data['shop_id'])->update(['points' => $newPoints]);
                    } else {
                        DI()->notorm->shop_apply->where("id=?", $data['shop_id'])->update(['points' => $data['points']]);
                    }
                }

            }
        }

        if (!$result) {
            return 1004;
        }
        return $result;
    }
    public function getListByShop($shop_id,$p){
        $pnums = 10;
        $start = ($p - 1) * $pnums;
        $where = "status=1 and isdel = 0 and shop_id = ".$shop_id;
        $list = DI()->notorm->dynamic
            ->select("*")
            ->where($where)->order("id asc")
            ->limit($start, $pnums)
            ->fetchAll();
        return $list;
        
    }

    public function getShopLists($keywords, $p, $lat, $lng)
    {
        if ($p < 1) {
            $p = 1;
        }
        $pnums = 10;
        $start = ($p - 1) * $pnums;
        $where = "status=1";
        if ($keywords != '') {
            $where .= " and name like '%" . $keywords . "%'";
        }
        //if(!empty($lat) and !empty($lng)){
        //$range =5*1000;//默认5公里
        // $where .= " AND (6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - ".$lat." * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(".$lat." * pi() / 180) * pow(sin((lng * pi()/ 180 - ".$lng." * pi() / 180) / 2),2))) * 1000)  < $range ";

        // }
        // $list=DI()->notorm->shop_apply
        // 	->select("*,getDistance('{$lat}','{$lng}',lat,lng) as distance,province")
        //     ->where($where)->order("distance asc")
        //     ->limit($start,$pnums)
        //     ->fetchAll();
        $list = DI()->notorm->shop_apply
            ->select("*,round((6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - " . $lat . " * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(" . $lat . " * pi() / 180) * pow(sin((lng * pi()/ 180 - " . $lng . " * pi() / 180) / 2),2))) * 1),2) as distance,province")
            ->where($where)->order("distance asc")
            ->limit($start, $pnums)
            ->fetchAll();
        foreach ($list as &$val) {
            if ($val['thumb']) {
                $val['thumb'] = get_upload_path($val['thumb']);
            }
            $val['topic'] = $this->getShopTopic($val['topic']) ?: [];
        }

        return $list;
    }

    public function getRecommend($p, $uid, $type, $lat, $lng, $classid, $keyword, $city)
    {
        $uri = $this->get_http_type();

        if ($p < 1) {
            $p = 1;
        }
        $pnums = 10;
        $start = ($p - 1) * $pnums;
        $isblack = DI()->notorm->user_black->select('touid')->where('uid = ? and type=1',$uid)->fetchAll();
        $result = [];
        array_walk_recursive($isblack, function($value) use (&$result) {
            array_push($result, $value);
        });
        $isblack_str =implode(',',$result);
 
        // var_dump(implode(',',$result));exit;
        if ($type == '1') { //推荐帖子

            $recom = DI()->notorm->user_recom->where('uid = ?', $uid)->fetchOne();
           
            $orderby = '';
            $user_recom = '';
            if($recom){
                $user_recom = $recom['recom'];
            }
            if($user_recom){
                $user_recom = explode(',',$user_recom);
                $str = '';
                $i = 1;
                foreach ($user_recom as $k){
                    if($k){
                        $str.=' WHEN '.$k.' THEN 0';
                    }
                }
              
                if($str){
                    $orderby = ' ,(CASE  A.cat_id '.$str.'  else 20 END),addtime desc';
                }
                
            }
           
            $where = "A.is_recommend=1 and A.status=1 AND A.id NOT IN (SELECT dynamic_id FROM cmf_user_hide_dynamic WHERE uid=?) AND A.uid NOT IN (SELECT dynamic_uid FROM cmf_user_hide_user_dynamic WHERE uid=?)";
            if ($city) {
                
                   $city = strtolower($city);
                  // $where .= " and (LOWER(A.city) LIKE '%{$city}%' OR LOWER(A.state) LIKE '%{$city}%')";
                    //$where .= " or A.city LIKE '%{$city}%' or A.state LIKE '%{$city}%' or A.address LIKE '%{$city}%'";
   
                $data = [];
                $data['address'] = $city;
                $search_address = $data['address']; //这个位置覆盖掉
                $wheres="name LIKE '%".$data['address']."%' or chinese like '%".$data['address']."%' or english LIKE '%".$data['address']."'";
                $areaList = DI()->notorm->area->where($wheres)->where('pid','>',0)->fetchOne();  //获取到的area

                $where.=' and (';
                if($areaList['name']){
                    $data['address'] = $areaList['name'];  //跟随arealist的地址库走
                    $where .= " A.city LIKE '%{$data['address']}%' or A.state LIKE '%{$data['address']}%' or A.address LIKE '%{$data['address']}%' or ";
                }
                if($areaList['chinese']){
                    $data['address'] = $areaList['chinese'];  //跟随arealist的地址库走
                    $where .= " A.city LIKE '%{$data['address']}%' or A.state LIKE '%{$data['address']}%' or A.address LIKE '%{$data['address']}%' or ";
                }
                if($areaList['english']){
                    $data['address'] = $areaList['english'];  //跟随arealist的地址库走
                    $where .= "  A.city LIKE '%{$data['address']}%' or A.state LIKE '%{$data['address']}%' or A.address LIKE '%{$data['address']}%' or ";
                }
                $data['address'] = $search_address;
                $where .= " A.city LIKE '%{$data['address']}%' or A.state LIKE '%{$data['address']}%' or A.address LIKE '%{$data['address']}%'";
                $where .=" )";
               // $where.=' and address <> ""';
                $where.=' and (A.city <> "" or A.state <> "" or A.address <> "")';
              
            }else{
            /*    $english = 'Malaysia';
                $chinese = '马来西亚';
                $ms = 'Malaysia';
                $where  .=" and (A.country = '{$english}' or A.country = '{$chinese}' or A.country = '{$ms}' or A.country ='')";*/

            }
            file_put_contents('lp.log',$city);
            //不显示拉黑人的帖子
            $where .= " and A.uid NOT IN (SELECT touid FROM cmf_user_black WHERE type=1 and uid=".$uid.")";
            //不显示被拉黑人的帖子
            $where .= " and A.uid NOT IN (SELECT uid FROM cmf_user_black WHERE type=1 and touid=".$uid.")";
            
            $list = $this->getORM()
                ->select('A.*, B.avatar,B.user_nickname') // 获取字段
                ->alias('A')
                ->leftJoin('user', 'B', 'A.uid = B.id')
                ->where($where, $uid, $uid)
                ->order('is_top desc'.$orderby)
                ->limit($start, $pnums)
                ->fetchAll();
                
     

        } elseif ($type == '2') { // 关注帖子

            $list = array();
            $attention = DI()->notorm->user_attention
                ->select("*")
                ->where("uid='{$uid}'")
                ->fetchAll();
            //$uids=array_column($attention,'touid');
            if ($attention) {
                $uids = array_column($attention, 'touid');

                $touids = implode(",", $uids);
                //	print_r($touids);exit;
                $where = "A.uid in ({$touids}) and A.status=1";

                $list = $this->getORM()
                    ->select('A.*, B.avatar,B.user_nickname') // 获取字段
                    ->alias('A')
                    ->leftJoin('user', 'B', 'A.uid = B.id')
                    ->where($where)
                    ->limit($start, $pnums)
                    ->order('A.is_top desc,A.addtime desc')
                    ->fetchAll();

            }

        } elseif ($type == '3') {
            /* $where = " status='1' and lng!='' and lat!=''";
             $list = DI()->notorm->dynamic
                 //->select("*,getDistance('{$lat}','{$lng}',lat,lng) as distance,province")
                 ->select("*,round((6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - " . $lat . " * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(" . $lat . " * pi() / 180) * pow(sin((lng * pi()/ 180 - " . $lng . " * pi() / 180) / 2),2))) * 1),2) as distance,province")
                 ->where($where)
                 ->order("distance asc")
                 ->limit($start, $pnums)
                 ->fetchAll();*/
            //sql优化
            $where = " t1.status='1' and t1.lng!='' and t1.lat!='' and t1.distance < 10";
            $sql = "select *,round((6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - " . $lat . " * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(" . $lat . " * pi() / 180) * pow(sin((lng * pi()/ 180 - " . $lng . " * pi() / 180) / 2),2))) * 1),2) as distance from cmf_dynamic";
            $str = "select * from (".$sql.") as t1 where".$where." order by is_top desc, distance limit ".$start.",".$pnums;

            $list = $this->getORM()->queryAll($str);

            foreach ($list as $key => $value) {

                $userinfo = getUserInfo($value['uid']);

                $list[$key]['avatar'] = get_upload_path($userinfo['avatar']);
                $list[$key]['user_nickname'] = $userinfo['user_nickname'];
                // 用户是否点赞帖子
                $list[$key]['isAttn'] = getUserNoteAtte($uid, $value['id']);
                // 帖子点赞总数
                $list[$key]['attns'] = getNoteAttes($value['id']);
                // 用户是否收藏帖子
                $list[$key]['isCollect'] = getUserNoteColl($uid, $value['id']) ? 1 : 0;

                // 帖子图片
                if ($value['thumb']) {
                    $value['thumb'] = explode(",", $value['thumb']);
                    foreach ($value['thumb'] as $k => $v) {
                        $value['thumb'][$k] = get_upload_path($v);
                    }
                    $list[$key]['thumb'] = $value['thumb'];
                } else {
                    $list[$key]['thumb'] = [];
                }
                if (!empty($value['video'])) {
                    $value['video'] = explode(",", $value['video']);
                    foreach ($value['video'] as $k => $v) {
                        $value['video'][$k] = get_upload_path($v);
                    }
                    $list[$key]['video'] = $value['video'];
                } else {
                    $list[$key]['video'] = [];
                }

                if(!empty($value['video_img'])){
                    $value['video_img'] = explode(",",$value['video_img']);
                    foreach ($value['video_img'] as $k=>$v){
                        $value['video_img'][$k] = $this->get_http_type($v);
                    }
                    $list[$key]['video_img'] = $value['video_img'];
                }else{
                    $list[$key]['video_img'] = [];
                }

                // 距离
                if ($value['distance'] > 1000) {
                    $value['distance'] = 1000;
                }
                $value['distance'] = $value['distance'] . 'km';
                $list[$key]['distance'] = $value['distance'];
                //获取帖子话题
                $list[$key]['topic'] = $this->getShopTopic($value['topic']) ?: [];

            }
        } elseif ($type == '4') {
            $where = "A.cat_id={$classid} and A.status=1";
            $list = $this->getORM()
                ->select('A.*, B.avatar,B.user_nickname') // 获取字段
                ->alias('A')
                ->leftJoin('user', 'B', 'A.uid = B.id')
                ->where($where)
                ->limit($start, $pnums)
                ->order('A.is_top desc,A.addtime desc')
                ->fetchAll();

        } elseif ($type == '0') {
            $where = " A.cat_id={$classid} and A.status=1 ";
                        //不显示拉黑人的帖子
            $where .= " and A.uid NOT IN (SELECT touid FROM cmf_user_black WHERE type=1 and uid=".$uid.")";
            //不显示被拉黑人的帖子
            $where .= " and A.uid NOT IN (SELECT uid FROM cmf_user_black WHERE type=1 and touid=".$uid.")";
            if ($keyword) {
                $where .= " and A.title like '%{$keyword}%' ";
            }
            $list = $this->getORM()
                ->select('A.*, B.avatar,B.user_nickname') // 获取字段
                ->alias('A')
                ->leftJoin('user', 'B', 'A.uid = B.id')
                ->where($where)
                ->limit($start, $pnums)
                ->order('A.is_top desc,A.addtime desc')
                ->fetchAll();
        }
//        var_dump($list);die;
        if ($list != '' && $type != '3') {
            foreach ($list as $key => $value) {
                // 用户是否点赞帖子
                $list[$key]['isAttn'] = getUserNoteAtte($uid, $value['id']);
                // 帖子点赞总数
                $list[$key]['attns'] = getNoteAttes($value['id']);
                // 用户是否收藏帖子
                $isCollect = getUserNoteColl($uid, $value['id']);
                $list[$key]['isCollect'] = $isCollect ? 1 : 0;
                $list[$key]['isCollect_id'] = $isCollect['id'];
                $list[$key]['avatar'] = get_upload_path($value['avatar']);
                if (!empty($value['thumb'])) {
                    $thumb = explode(",", trim($value['thumb'], ','));
                    foreach ($thumb as $k => $v) {
                        if((bool)$v){
                            $thumb[$k] = get_upload_path($v);
                        }else{
                            unset($thumb[$k]);
                        }
                    }
                    $list[$key]['thumb'] = $thumb ?: [];
                }else{
                    $list[$key]['thumb'] = [];
                }

                if(!empty($list[$key]['video'])){
                    $list[$key]['video'] = explode(',',$list[$key]['video']);
                }else{
                    $list[$key]['video'] = [];
                }

                if(!empty($value['video_img'])){
                    $value['video_img'] = explode(",",$value['video_img']);
                    foreach ($value['video_img'] as $k=>$v){
                        $value['video_img'][$k] = $this->get_http_type($v);
                    }
                    $list[$key]['video_img'] = $value['video_img'];
                }else{
                    $list[$key]['video_img'] = [];
                }

                //获取帖子话题
                $list[$key]['custom'] = $this->getShopTopic($value['custom']) ?: [];
                $list[$key]['topic'] = $this->getShopTopic($value['topic']) ?: [];
                $list[$key]['topic'] = array_merge($list[$key]['custom'],$list[$key]['topic']);
                $list[$key]['topic'] = array_unique($list[$key]['topic'],SORT_REGULAR);
                $list[$key]['topic'] = array_values($list[$key]['topic']);
                $coin = DI()->notorm->user_log->where('sid = ?', $value['id'])->select("sum(money) as money")->fetchAll();
                $list[$key]['coin'] = $coin[0]['money'] != null ? $coin[0]['money'] : 0;
            }
        }

        return $list;

    }
    //获取店铺所选话题
    public function getShopTopic($topic) {
        if(!$topic) {
            return 0;
        }
        $topics = explode(',', trim($topic,','));
        $res = [];
        $info =  DI()->notorm->shop_topic->where('topic_id', $topics)->fetchAll();
        foreach ($topics as $v){
            if(is_numeric($v)){  //该地方在2021/4/19 因为app端新增标签时,通知服务端未改,导致后期发现未适应数据库,所以修改未适应
                $s = DI()->notorm->shop_topic->where('topic_id', $v)->fetchOne();
                if($s){
                    $res[] = $s;
                }
            }

        }
        return $res;
    }
    public function getNote($id, $uid)
    {
        $where = "A.id=" . $id;
        $list = $this->getORM()
            ->select('A.*, B.name AS shopName,B.thumb AS shopThumb,B.des AS shopDes,B.points AS shopPoints,B.id AS shopId,consumption as consumption,B.claim AS shopClaim')
            ->alias('A')
            ->leftJoin('shop_apply', 'B', 'A.shop_id = B.id')->where($where)
            ->fetch();

        if (!$list) {
            return 404;
        }
        // $newReading = $list['reading']+1;
        // $this->getORM()->where('id=?',$id)->update(['reading'=>$newReading]);
        //全部帖子阅读量
        //$tiezi = DI()->notorm->dynamic->where('uid=? and id=?', $list['uid'], $id)->select("sum(reading) as reading")->fetchAll();
        $tiezi = DI()->notorm->dynamic->where('uid=? ', $list['uid'])->where('status = 1 and isdel = 0')->select("sum(reading) as reading")->fetchAll();
        $map = " num<=" . $tiezi[0]['reading'];
        $readCoin = DI()->notorm->read->where($map)->order('num desc')->limit(0, 1)->fetchAll();

        if ($readCoin) {
            $logMap = "type=1 and uid =" . $list['uid'] . " and money=" . $readCoin[0]['percentage'];

            $logMap .= " and readid = '{$readCoin[0]['id']}' and is_admin = 1";

            $userLog = DI()->notorm->user_log
                ->where($logMap)->fetchOne();
            //print_r($readCoin[0]['num']);exit;
            if (!$userLog) {

                addMoney($list['uid'], [
                    'title' => '阅读量',
                    'money' => $readCoin[0]['percentage'],
                    'readid' => $readCoin[0]['id'],
                    'dyId' => $id
                ]);
                
            }
        }


        if (!DI()->notorm->record_all->where('uid=? and sid = ?', $uid, $id)->fetchOne()) {
            DI()->notorm->record_all->insert(['uid' => $uid, 'sid' => $id, 'addtime' => time()]);
        }
        $user = getUserInfo($list['uid']);
        $list['user_nickname'] = $user['user_nickname'];
        $shopclass = DI()->notorm->shop_class->where('id', $list['cat_id'])->select('id,name,english,malay')->fetchOne();
        $list['shop_class'] = $shopclass ?: '';
        $list['avatar'] = get_upload_path($user['avatar']);
        $list['avatar_thumb'] = get_upload_path($user['avatar_thumb']);
        $list['shopThumb'] = get_upload_path($list['shopThumb']);
//        var_dump($list['topic']);die;
        $list['topic'] = $this->getShopTopic($list['topic']) ?: [];
        $list['custom'] = $this->getShopTopic($list['custom']) ?: [];
        $list['topic'] = array_merge($list['topic'],$list['custom']);
        $list['topic'] =  array_unique( $list['topic'], SORT_REGULAR);
        $list['topic'] = array_values($list['topic']);
        $coin = DI()->notorm->user_log->where('sid = ?', $id)->select("sum(money) as money")->fetchAll();
        $list['coin'] = $coin[0]['money'] != null ? $coin[0]['money'] : 0;
        if ($list['thumb']) {
            $thumb_arr = explode(',', trim($list['thumb'], ','));
            foreach ($thumb_arr as $k => $v) {
                if($v){
                    $thumb_arr[$k] = get_upload_path($v);
                }else{
                    unset($thumb_arr[$k]);
                }

            }
            $list['thumb'] = $thumb_arr;
        } else {
            $list['thumb'] = [];
        }
        if ($list['video']) {
            $video = explode(',', $list['video']);
            foreach ($video as $k => $v) {
                $video[$k] = get_upload_path($v);
            }
            $list['video'] = $video;
        } else {
            $list['video'] = [];
        }

        if($list['video_img']){
            $video_img = explode(',', $list['video_img']);
            foreach ($video_img as $k => $v) {
                $video_img[$k] = $this->get_http_type($v);
            }
            $list['video_img'] = $video_img;
        }else{
            $list['video_img'] = [];
        }

        return $list;

    }

    public function getShop($id)
    {
        $where = "id=" . $id;
        $list = DI()->notorm->shop_apply
            ->where($where)->fetch();
        if (!$list['id']) {
            return 404;
        }
        $branch_arr = array();
        foreach ($list as $k=>$v){
            if($k == 'company_description' || $k == 'des'){
                $v = htmlspecialchars_decode($v);
            }
            if($k == 'consumption'){
                #$v = DI()->notorm->shop_comment->where('shop_id = '.$id)->count(); //评论数
                $v1 = $v2 = 0;
                $v1 = DI()->notorm->video->where('is_delete =? and draft =? and  shop_id =?',0,0,$id)->count();
                $v2 = DI()->notorm->dynamic->where('isdel=? and draft=? and shop_id=?',0,0,$id)->count();
                $v = $v1 +$v2;
            }
            $list[$k] = $v;
            if($k == 'branch' && $v){
                #$branch_arr = DI()->notorm->branch->where('FIND_IN_SET("id",'.$v.')')->fetchAll();
                $sql  = 'select * from cmf_branch where FIND_IN_SET(id,"'.$v.'")';

                $branch_arr = DI()->notorm->branch->queryAll($sql);
                foreach($branch_arr as $key=>$val){
                    $branch_arr[$key]['address'] = htmlspecialchars_decode($val['address']);
                }
                

            }
        }
       # $res = DI()->notorm->shop_apply->where('id = '.$id)->select("branch")->fetchAll();


       /* foreach ($res as $key => $value){
            
            if($value['branch']){
                $ress = explode(',', $value['branch']);
                foreach($ress as $val){
                    
                    $branch_arr[] = DI()->notorm->branch->where('id = '.$val)->fetchAll();
                    
                }

            }else{
                $branch_arr[] = '';
            }
        }*/
        $list['thumb'] = $list['thumb'] ? get_upload_path($list['thumb']) : '';
        $list['branch'] = $branch_arr;
        return $list;
    }

    public function getDraftList($uid, $p)
    {
        if ($p < 1) {
            $p = 1;
        }
        $pnums = 10;
        $start = ($p - 1) * $pnums;
        $where = "A.uid=" . $uid . " and A.draft=1";
        $list = $this->getORM()->select('A.*, B.name AS shopName')
            ->alias('A')
            ->leftJoin('shop_apply', 'B', 'A.shop_id = B.id')
            ->where($where)
            ->order('addtime desc')
            ->limit($start, $pnums)
            ->fetchAll();
        foreach ($list as &$val) {
            if ($val['thumb']) {
                $thumb_arr = explode(',', $val['thumb']);
                foreach ($thumb_arr as $k => $v) {
                    $thumb_arr[$k] = get_upload_path($v);
                }
                $val['thumb'] = $thumb_arr;
            } else {
                $val['thumb'] = [];
            }
            if ($val['video']) {
                $video = explode(',', $val['video']);
                foreach ($video as $k => $v) {
                    $video[$k] = get_upload_path($v);
                }
                $val['video'] = $video;
            } else {
                $val['video'] = [];
            }

            if ($val['video_img']) {
                $video = explode(',', $val['video_img']);
                foreach ($video as $k => $v) {
                    $video[$k] = get_upload_path($v);
                }
                $val['video_img'] = $video;
            } else {
                $val['video_img'] = [];
            }

            $val['topicName'] = $this->getShopTopic($val['topic']) ?: [];
        }
        return $list;
    }

    public function draftNote($uid, $id)
    {
        $where = "A.id=? AND A.uid=?";
        $list = $this->getORM()->select('A.*, B.name AS shopName,C.name AS cateName,C.english as englishCateName,C.malay as malayCateName')
            ->alias('A')
            ->leftJoin('shop_apply', 'B', 'A.shop_id = B.id')
            ->leftJoin('shop_class', 'C', 'A.cat_id = C.id')
            ->where($where, $id, $uid)->fetch();
        if ($list['thumb']) {
            $thumb_arr = explode(',', $list['thumb']);
            foreach ($thumb_arr as $k => $v) {
                $thumb_arr[$k] = get_upload_path($v);
            }
            $list['thumb'] = $thumb_arr;
            $video = explode(',', $list['video']);
            foreach ($video as $k => $v) {
                $video[$k] = get_upload_path($v);
            }
            $list['video'] = $video;
            $video_img = explode(',',$list['video_img']);
            foreach ($video_img as $k=>$v){
                $video_img[$k] = $this->get_http_type($v);
            }
            $list['video_img'] = $video_img;

        }
        $list['englishCateName'] = htmlspecialchars_decode($list['englishCateName']);
        $list['malayCateName'] = htmlspecialchars_decode($list['malayCateName']);
        if ($list['topic']) {
            $list['topicName'] = $this->getShopTopic($list['topic']);
        }
        return $list;
    }


    public function draftDel($uid, $id)
    {

        $where = "id=? AND uid=?";
        $list = $this->getORM()->where($where, $id, $uid)
            ->delete();

        return 1;

    }

    public function addNoteLike($data, $lan)
    {
        $users = getUserInfo($data['uid']);
        $res = DI()->notorm->dynamic->where([
            'id' => $data['note_id']
        ])
            ->fetch();
        if (!$res) {
            return 1001;
        }
        if ($res['uid'] == $data['uid']) {
           // return 1002;//不能给自己点赞
        }
        $r = DI()->notorm->note_like->where([
            'note_id' => $data['note_id'],
            'uid' => $data['uid']
        ])->fetch();
        if ($r) {
            DI()->notorm->note_like->where([
                'note_id' => $data['note_id'],
                'uid' => $data['uid']
            ])->delete();
            DI()->notorm->dynamic->where([
                'id' => $data['note_id']
            ])->update([
                'likes' => $res['likes'] - 1,
            ]);

        } else {
            DI()->notorm->note_like->insert([
                'note_id' => $data['note_id'],
                'uid' => $data['uid'],
                'add_time' => time(),
            ]);
            DI()->notorm->dynamic->where([
                'id' => $data['note_id']
            ])->update([
                'likes' => $res['likes'] + 1,
            ]);

            $lang = getUserInfo($res['uid'])['lang'];

            if ($lang == 'en') {
                $tuisong = TPNSPush('Notification', $users['user_nickname'] . ' liked your post', $res['uid'], 1, '');
                $language = 'liked your post';
            } elseif ($lang == 'ms') {
                $tuisong = TPNSPush('Notifikasi', $users['user_nickname'] . ' suka siaran anda', $res['uid'], 1, '');
                $language = 'telah komen siaran anda';
            } else {
                $tuisong = TPNSPush('您有新的帖子点赞提醒', $users['user_nickname'] . ' 用户点赞了你的帖子', $res['uid'], 1, '');
                $language = '用户点赞了你的帖子';
            }
            //  $tuisong = TPNSPush('您有新的帖子点赞提醒','用户点赞了你的帖子', $res['uid'], 1, '');
            $remindData = [
                'type' => 1,
                'target' => 'note',
                'target_id' => $res['id'],//帖子ID
                'target_owner' => $data['uid'],//触发人ID
                'add_time' => time(),
                'action' => '用户点赞了你的帖子',
                'user_id' => $res['uid'] ? $res['uid'] : 0,
            ];
            DI()->notorm->remind
                ->insert($remindData);

        }
        $nums = DI()->notorm->dynamic->where([
            'id' => $data['note_id']
        ])->select('likes')->fetch();
        // print_r($nums);
        return $nums['likes'];
    }

    //我发布的帖子
    public function getMyNote($uid, $touid, $p)
    {
        if ($p < 1) {
            $p = 1;
        }
        $pnums = 10;
        $start = ($p - 1) * $pnums;
        if ($touid > 0) {
            $res = $this->getORM()
                ->where("uid=? and status=?", $touid, 1)->order('addtime desc')
                ->limit($start, $pnums)->fetchAll();
        } else {
            $res = $this->getORM()
                ->where("uid=? and status=?", $uid, 1)->order('addtime desc')
                ->limit($start, $pnums)->fetchAll();
        }

        foreach ($res as $key => $value) {
            if(!empty($value['thumb'])){
                $value['thumb'] = explode(",", trim($value['thumb'],','));
                // 帖子图片
                foreach ($value['thumb'] as $k => $v) {
                    $value['thumb'][$k] = get_upload_path($v);
                }
                $res[$key]['thumb'] = $value['thumb'];
            }else{
                $res[$key]['thumb'] = [];
            }
            if(!empty($value['video'])){
                $value['video'] = explode(",", $value['video']);
                foreach ($value['video'] as $k => $v) {
                    $value['video'][$k] = get_upload_path($v);
                }
                $res[$key]['video'] = $value['video'];
            }else{
                $res[$key]['video'] = [];
            }
            $uri = $this->get_http_type();
            if(!empty($value['video_img'])){
                $value['video_img'] = explode(",", $value['video_img']);
                foreach ($value['video_img'] as $k => $v) {
                    $value['video_img'][$k] =  $this->get_http_type($v);
                }
                $res[$key]['video_img'] = $value['video_img'];
            }else{
                $res[$key]['video_img'] = [];
            }

            $res[$key]['topic'] = $this->getShopTopic($value['topic']) ?: [];
            $name = getUserInfo($value['uid']);
            $res[$key]['avatar'] = get_upload_path($name['avatar']) ?: '';
            $res[$key]['user_nickname'] = $name['user_nickname'];
            $res[$key]['isCollect'] = getUserNoteColl($uid, $value['id']) ? 1 : 0;
            $res[$key]['isLike'] = getUserNoteAtte($uid, $value['id']);
            $collectNum = DI()->notorm->collect->where('type=? and collect_id=?', 1, $value['id'])->count();
            $res[$key]['collectNum'] = $collectNum ? $collectNum : 0;
            $coin = DI()->notorm->user_log->where('sid = ?', $value['id'])->select("sum(money) as money")->fetchAll();
            $res[$key]['coin'] = $coin[0]['money'] != null ? $coin[0]['money'] : 0;
        }
        return $res;
    }


    public function censusLike($uid, $start, $end)
    {
        $sIds = DI()->notorm->dynamic->where("uid = '{$uid}' ")->select('id')->fetch();

        $count1 = 0;
        if ($sIds) {
            $sIds = implode(",", $sIds);

            $where1 = "note_id in ({$sIds})  and add_time>=" . $start . " and add_time<=" . $end;
            //  print_r($where1);exit;
            $count1 = DI()->notorm->note_like
                ->where($where1)->count();

        }
        return $count1;

    }
    //获取http协议
    //获取http协议
    function get_http_type($urls =false)
    {
        if(!$urls){
            return false;
        }
        $http_type = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') || (isset($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https')) ? 'https://' : 'http://';
        $url = $http_type.$_SERVER['HTTP_HOST'].'/ffmpeg/';
        if(strpos($urls,'http') === false){  //是否包含
            $url = $url.$urls;
        }else{
            $url = $urls;
        }
        return $url;
    }


    public function screen($uid, $data, $p)
    {
        if ($p < 1) {
            $p = 1;
        }
        $pnums = 8;
        $start = ($p - 1) * $pnums;
        $where = "status='1' ";
        if ($data['topic']) {
            $data['topic'] = trim($data['topic'], '#');
            $where1 = "topic_name LIKE '%{$data['topic']}%'";
            $sIds = DI()->notorm->shop_topic->where($where1)->select('topic_id')->fetchAll();
            if ($sIds) {
                $new = array_column($sIds, 'topic_id');
//                $sIds=implode(",",$new);
                $where .= ' and ';
                foreach ($new as $v) {
                    $where .= " custom LIKE '%{$v}%' or";
                }
                $where = trim($where, ' or ');
            }
        }
        if ($data['address']) {
            $search_address = $data['address']; //这个位置覆盖掉
            $wheres="name LIKE '%".$data['address']."%' or chinese like '%".$data['address']."%' or english LIKE '%".$data['address']."'";
            $areaList = DI()->notorm->area->where($wheres)->where('pid','>',0)->fetchOne();  //获取到的area

            $where.=' and (';
            if($areaList['name']){
                $data['address'] = $areaList['name'];  //跟随arealist的地址库走
                $where .= " city LIKE '%{$data['address']}%' or state LIKE '%{$data['address']}%' or address LIKE '%{$data['address']}%' or ";
            }
            if($areaList['chinese']){
                $data['address'] = $areaList['chinese'];  //跟随arealist的地址库走
                $where .= " city LIKE '%{$data['address']}%' or state LIKE '%{$data['address']}%' or address LIKE '%{$data['address']}%' or ";
            }
            if($areaList['english']){
                $data['address'] = $areaList['english'];  //跟随arealist的地址库走
                $where .= "  city LIKE '%{$data['address']}%' or state LIKE '%{$data['address']}%' or address LIKE '%{$data['address']}%' or ";
            }
            $data['address'] = $search_address;
            $where .= " city LIKE '%{$data['address']}%' or state LIKE '%{$data['address']}%' or address LIKE '%{$data['address']}%'";
            $where .=" )";

        }
        if ($data['points']) {
            $where .= " and points = {$data['points']}";
        }
        $order = 'is_top desc';
        if ($data['reading'] != 0) {
            if ($data['reading'] == 1) {
                $order .= ',reading desc';
                // $res = DI()->notorm->dynamic->where($where)->order('reading desc')->limit($start,$pnums)->fetchAll();
            } elseif ($data['reading'] == 2) {
                $order .= ',reading asc';
                // $res = DI()->notorm->dynamic->where($where)->order('reading ASC')->limit($start,$pnums)->fetchAll();
            }

        }
        if ($data['likes'] != 0) {
            if ($data['likes'] == 1) {
                if (!empty($order)) {
                    $order .= ',likes desc';
                } else {
                    $order .= ',likes desc';
                }
                // $res = DI()->notorm->dynamic->where($where)->order('likes desc')->limit($start,$pnums)->fetchAll();
            } elseif ($data['likes'] == 2) {
                //   $res = DI()->notorm->dynamic->where($where)->order('likes asc')->limit($start,$pnums)->fetchAll();
                if (!empty($order)) {
                    $order .= ',likes asc';
                } else {
                    $order .= ',likes asc';
                }
            }
        }
        if ($data['collect_sum'] != 0) {
            if ($data['collect_sum'] == 1) {
                if (!empty($order)) {
                    $order .= ',collect_sum desc';
                } else {
                    $order .= ',collect_sum desc';
                }
                // $res = DI()->notorm->dynamic->where($where)->order('collect_sum desc')->limit($start,$pnums)->fetchAll();
            } elseif ($data['collect_sum'] == 2) {
                // $res = DI()->notorm->dynamic->where($where)->order('collect_sum asc')->limit($start,$pnums)->fetchAll();
                if (!empty($order)) {
                    $order .= ',collect_sum asc';
                } else {
                    $order .= ',collect_sum asc';
                }
            }
        }

        if ($data['collect_sum'] == 0 && $data['reading'] == 0 && $data['likes'] == 0) {
            $order = 'is_top desc,id desc';
        }


        $res = DI()->notorm->dynamic->where($where)->order($order)->limit($start, $pnums)->fetchAll();
        foreach ($res as $key => $value) {
            if(!empty($value['thumb'])){
                $value['thumb'] = explode(",", trim($value['thumb'],','));
                // 帖子图片
                foreach ($value['thumb'] as $k => $v) {
                    $value['thumb'][$k] = get_upload_path($v);
                }
            }else{
                $value['thumb'] = [];
            }
            if(!empty($value['video'])){
                $value['video'] = explode(",", trim($value['video'],','));

                // 帖子图片
                foreach ($value['video'] as $k => $v) {
                    $value['video'][$k] = get_upload_path($v);
                }
            }else{
                $value['video'] = [];
            }
            $uri = $this->get_http_type();
            if(!empty($value['video_img'])){
                $value['video_img'] = explode(",", trim($value['video_img'],','));
                foreach ($value['video_img'] as $k => $v) {
                    $value['video_img'][$k] =$this->get_http_type($v);
                }
            }else{
                $value['video_img'] = [];
            }

            $res[$key]['thumb'] = $value['thumb'];
            $res[$key]['video'] = $value['video'];
            $res[$key]['video_img'] = $value['video_img'];
            $res[$key]['topic'] = $this->getShopTopic($value['topic']) ?: [];
            $name = getUserInfo($value['uid']);
            $res[$key]['avatar'] = get_upload_path($name['avatar']) ?: '';
            $res[$key]['user_nickname'] = $name['user_nickname'];
            $res[$key]['isCollect'] = getUserNoteColl($uid, $value['id']) ? 1 : 0;
            $res[$key]['isAttn'] = getUserNoteAtte($uid, $value['id']);
        }
        return $res;
    }

    public function topicNote($uid, $topic, $lat, $lng, $p)
    {
        if ($p < 1) {
            $p = 1;
        }
        $pnums = 10;
        $start = ($p - 1) * $pnums;

        $where = "status='1' and draft = 0 ";
       // $where .= " and custom LIKE '%{$topic}%'";
        //由于那边自定义标签问题

        $topic = DI()->notorm->shop_topic->where('topic_id = '.$topic)->fetchOne();
        $topic_name = trim($topic['topic_name']);

        $topicList = DI()->notorm->shop_topic->where('TRIM(topic_name) = "'.$topic_name.'"')->fetchAll();

        $dks = [];
        $where.= 'and ( ';
        foreach ($topicList as $vs){
            $dks[] = $vs['topic_id'];
            $where.= " FIND_IN_SET('{$vs['topic_id']}',topic) or  FIND_IN_SET('{$vs['topic_id']}',custom) or ";
        }
        $where .=' 1=2 ';
        $where.=')';



        //$topic = implode(',',$dks);

       // $where .= " and  ( FIND_IN_SET(topic,'{$topic}') or ( FIND_IN_SET(custom,'{$topic}') ))";

        $res = DI()->notorm->dynamic
            ->select("*,round((6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - " . $lat . " * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(" . $lat . " * pi() / 180) * pow(sin((lng * pi()/ 180 - " . $lng . " * pi() / 180) / 2),2))) * 1),2) as distance,province")
            ->where($where)
            ->order("distance,id asc") //因为位置可能相同 导致第二页时 有重复数据 所以用2个字段排序方式
            ->limit($start, $pnums)
            ->fetchAll();

        $sql = 'select * from cmf_dynamic union select  *  from cmf_video';

        $sql = "select custom,draft,status,uid,thumb,topic,title,content,id,addtime,likes,reading,points,video_img,shop_id,round((6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - " . $lat . " * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(" . $lat . " * pi() / 180) * pow(sin((lng * pi()/ 180 - " . $lng . " * pi() / 180) / 2),2))) * 1),2) as distance,1 as type from cmf_dynamic union select custom,draft,status,uid,video_img as thumb,topic,`desc` as title,title as content,id,createtime,likes,visits,score,video_img,shop_id,round((6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - " . $lat . " * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(" . $lat . " * pi() / 180) * pow(sin((lng * pi()/ 180 - " . $lng . " * pi() / 180) / 2),2))) * 1),2) as distance,2 as type from cmf_video";
       # var_dump($where);die;
       // $sql .= " where $where order  by type desc,distance asc,id desc";
        $sql = 'select *  from ('.$sql.') s1  where '. $where.' order  by type desc,distance asc,id desc limit '.$start.','.$pnums;
       # echo $sql;die;
        //echo $sql;die;
        $res = DI()->notorm->dynamic->queryAll($sql);
    
        foreach ($res as $key => $value) {
           // var_dump($value['id']);
            // 帖子图片
            if($value['thumb']){
                $value['thumb'] = explode(",", $value['thumb']);
                foreach ($value['thumb'] as $k => $v) {
                    $value['thumb'][$k] = get_upload_path($v);
                }
            }else{
                $value['thumb'] = [];
            }

            $res[$key]['thumb'] = $value['thumb'];
            $res[$key]['topic'] = $this->getShopTopic($value['custom']) ?: [];
            $rw = $this->getShopTopic($value['topic']) ?: [];
            $res[$key]['topic'] = array_merge($res[$key]['topic'],$rw);
            $topics = [];
            foreach ($res[$key]['topic'] as $kt=>$vt){   //去除重复
                $topics[$vt['topic_id']] = $vt;
            }
            $new = [];
            foreach ($topics as $k=>$v){
                $new[] = $v;
            }
            $res[$key]['topic'] = $new;

            $name = getUserInfo($value['uid']);
            $res[$key]['avatar'] = get_upload_path($name['avatar']) ?: '';
            $res[$key]['user_nickname'] = $name['user_nickname'];
            $res[$key]['isCollect'] = getUserNoteColl($uid, $value['id']) ? 1 : 0;
            $res[$key]['isAttn'] = getUserNoteAtte($uid, $value['id']);
            if($res[$key]['video']){
                $res[$key]['video'] = explode(',',$res[$key]['video']);
            }else{
                $res[$key]['video'] = [];
            }
            if($res[$key]['video_img']){
                $res[$key]['video_img'] = explode(',',$res[$key]['video_img']);
                foreach ($res[$key]['video_img'] as $k=>$v){
                    $res[$key]['video_img'][$k] = $this->get_http_type($v);
                }
            }else{
                $res[$key]['video_img'] = [];
            }


        }

        return $res;

    }

    public function hidePost($uid, $post_id, $type)
    {
        if ($type == 1) {
            // Find user id from post
            $res = DI()->notorm->dynamic
                ->select("uid")
                ->where("id=?", $post_id)
                ->fetch();

            if ($res && isset($res['uid']) && intval($res['uid']) > 0) {
                $data = [
                    "uid" => $uid,
                    "dynamic_uid" => $res['uid'],
                    "add_time" => time()
                ];

                $result = DI()->notorm->user_hide_user_dynamic->insert($data);
            }
        }

        if ($type == 0) {
            $data = [
                "uid" => $uid,
                "dynamic_id" => $post_id,
                "add_time" => time()
            ];

            $result = DI()->notorm->user_hide_dynamic->insert($data);
        }

        if ($result) {
            return $result;
        } else {
            return false;
        }
    }
}