<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/28 0028
 * Time: 9:48
 */
class Model_Userauth extends PhalApi_Model_NotORM {

        public function getInfo($uid){
            $info=DI()->notorm->user_auth->where("uid=?",$uid)->fetchOne();
            if(!$info){
                return 0;
            }
            return 1;

        }

        public function updateInfo($uid,$fields){
            if(!$fields){
                return false;
            }
            $fields['status']=0;
            return DI()->notorm->user_auth
                ->where('uid=?',$uid)
                ->update($fields);

        }

        public function addInfo($uid,$data){
            $data['uid']=$uid;
            $rs=DI()->notorm->user_auth->insert($data);
            if(!$rs){
                return 1002;
            }
            return $rs;

        }
        
        public function isAuth($uid){
            $status=DI()->notorm->user_auth
					->select("status,reason")
					->where('uid=?',$uid)
					->fetchOne();
            if($status){
                if($status['status']==1){
                    return ['status'=>1];
                }elseif($status['status']==2){
                    return ['status'=>2,'reason'=>$status['reason']];
                }else{
                    return ['status'=>0];
                }
            }else{
               return ['status'=>3];
            }
    		
        }
        
        public function shopAuth($uid,$id){
             $status=DI()->notorm->attestation
					->select("status")
					->where('uid=? and shop_id=?',$uid,$id)
					->fetchOne();	
            if($status){
                if($status['status']==1){
                    return 1;
                }elseif($status['status']==2){
                    return 2;
                }else{
                    return 0; 
                }
            }else{
                return 3;    
            }
    		
            
        }

}