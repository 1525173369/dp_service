<?php

class Model_User extends PhalApi_Model_NotORM {
	/* 用户全部信息 */
	public function getBaseInfo($uid,$v=0) {
		$info=DI()->notorm->user
				->select("id,mobile,user_nickname,avatar,avatar_thumb,sex,signature,coin,consumption,province,city,birthday,user_status,login_type,lang,user_email,address,user_pass,pid,major,openid,huawei_openid,facebook_openid,apple_openid")
				->where('id=?  and user_type="2"',$uid)
				->fetchOne();
        if($info){
            $info['avatar']=get_upload_path($info['avatar']);
            $info['avatar_thumb']=get_upload_path($info['avatar_thumb']);						
            // $info['level']=getLevel($info['consumption']);
            // $info['level_anchor']=getLevelAnchor($info['votestotal']);
            // $info['lives']=getLives($uid);
            // $info['follows']=getFollows($uid);
            // $info['fans']=getFans($uid);
            
            // $info['vip']=getUserVip($uid);
            // $info['liang']=getUserLiang($uid);
           
            $info['user_pass']=$info['user_pass']?1:0;
            if($v){
                $info['user_pass'] = 1;
            }
            if($info['birthday']){
                $info['birthday']=date('Y-m-d',$info['birthday']);      
            }else{
                $info['birthday']='';
            }
            $info['type'] = ''; //最低,未认证
            if($info['user_status'] == 1){
                $info['type'] = 'blue'; //认证过的
            }
            if($info['major'] == 1){
                $info['type'] = 'green';
            }
        }

					
		return $info;
	}
	
	public function updateBackground($uid,$data){
    	$info=DI()->notorm->user->where('id=?',$uid)->update($data);
    	if(!$info){
    	    return 1002;
    	}else{
    	    return 1;
    	}
	    
	}
			
	/* 判断昵称是否重复 */
	public function checkName($uid,$name){
		$isexist=DI()->notorm->user
					->select('id')
					->where('id!=? and user_nickname=?',$uid,$name)
					->fetchOne();
		if($isexist){
			return 0;
		}else{
			return 1;
		}
	}
	
	//切换语言
	public function switchingLanguage($uid,$data){
    	$info=DI()->notorm->user->where('id=?',$uid)->update($data);
    	$key = 'lang:'.$uid;
    	DI()->redis->set($key,$data['lang']); //设置每个用户的语言
    	if(!$info){
    	    return 1002;
    	}else{
    	    return 1;
    	}
				
    }
	
	
	
	/* 修改信息 */
	public function userUpdate($uid,$fields){
		/* 清除缓存 */
		delCache("userinfo_".$uid);
        
        if(!$fields){
            return false;
        }
        $config= DI()->notorm->option
					->select('option_value')
					->where("option_name='comment_app'")
					->fetchOne();
		$config=json_decode($config['option_value'],true);
		$user = DI()->notorm->user->where('id=?',$uid)->fetchOne();

		if(trim($user['user_email']) && trim($user['address'])){  //完善城市后 才会进入的钱
            addMoney($uid,[
                'title'=>'完善资料',
                'money'=>$config['material'],
            ]);
        }

		return DI()->notorm->user
					->where('id=?',$uid)
					->update($fields);
					
	}

	/* 修改密码 */
	public function updatePass($uid,$oldpass,$pass){
		$userinfo=DI()->notorm->user
					->select("user_pass")
					->where('id=?',$uid)
					->fetchOne();
		$oldpass=setPass($oldpass);
		if($userinfo['user_pass']!=$oldpass){
			return 1003;
		}							
		$newpass=setPass($pass);
		return DI()->notorm->user
					->where('id=?',$uid)
					->update( array( "user_pass"=>$newpass ) );
	}
	
	/* 我的钻石 */
	public function getBalance($uid){
		return DI()->notorm->user
				->select("coin,score")
				->where('id=?',$uid)
				->fetchOne();
	}
	
	/* 充值规则 */
	public function getChargeRules(){

		$rules= DI()->notorm->charge_rules
				->select('id,coin,coin_ios,money,product_id,give')
				->order('list_order asc')
				->fetchAll();

		return 	$rules;
	}
    
	/* 我的收益 */
	public function getProfit($uid){
		$info= DI()->notorm->user
				->select("votes,votestotal")
				->where('id=?',$uid)
				->fetchOne();

		$config=getConfigPri();
		
		//提现比例
		$cash_rate=$config['cash_rate'];
        $cash_start=$config['cash_start'];
		$cash_end=$config['cash_end'];
		$cash_max_times=$config['cash_max_times'];
		//剩余票数
		$votes=$info['votes'];
        
		//总可提现数
		$total=(string)floor($votes/$cash_rate);

        if($cash_max_times){
            //$tips='每月'.$cash_start.'-'.$cash_end.'号可进行提现申请，收益将在'.($cash_end+1).'-'.($cash_end+5).'号统一发放，每月只可提现'.$cash_max_times.'次';
            $tips='每月'.$cash_start.'-'.$cash_end.'号可进行提现申请，每月只可提现'.$cash_max_times.'次';
        }else{
            //$tips='每月'.$cash_start.'-'.$cash_end.'号可进行提现申请，收益将在'.($cash_end+1).'-'.($cash_end+5).'号统一发放';
            $tips='每月'.$cash_start.'-'.$cash_end.'号可进行提现申请';
        }
        
		$rs=array(
			"votes"=>$votes,
			"votestotal"=>$info['votestotal'],
			"total"=>$total,
			"cash_rate"=>$cash_rate,
			"tips"=>$tips,
		);
		return $rs;
	}	
	/* 提现  */
	public function setCash($data){
        
        $nowtime=time();
        
        $uid=$data['uid'];
        $accountid=$data['accountid'];
        $cashvote=$data['cashvote'];
        
        $config=getConfigPri();
        $cash_start=$config['cash_start'];
        $cash_end=$config['cash_end'];
        $cash_max_times=$config['cash_max_times'];
        
        $day=(int)date("d",$nowtime);
        
        if($day < $cash_start || $day > $cash_end){
            return 1005;
        }
        
        //本月第一天
        $month=date('Y-m-d',strtotime(date("Ym",$nowtime).'01'));
        $month_start=strtotime(date("Ym",$nowtime).'01');

        //本月最后一天
        $month_end=strtotime("{$month} +1 month");
        
        if($cash_max_times){
            $isexist=DI()->notorm->cash_record
                    ->where('uid=? and addtime > ? and addtime < ?',$uid,$month_start,$month_end)
                    ->count();
            if($isexist >= $cash_max_times){
                return 1006;
            }
        }
        
		$isrz=DI()->notorm->user_auth
				->select("status")
				->where('uid=?',$uid)
				->fetchOne();
		if(!$isrz || $isrz['status']!=1){
			return 1003;
		}
        
        /* 钱包信息 */
		$accountinfo=DI()->notorm->cash_account
				->select("*")
				->where('id=? and uid=?',$accountid,$uid)
				->fetchOne();

        if(!$accountinfo){

            return 1007;
        }
        

		//提现比例
		$cash_rate=$config['cash_rate'];
		/* 最低额度 */
		$cash_min=$config['cash_min'];
		
		//提现钱数
        $money=floor($cashvote/$cash_rate);
		
		if($money < $cash_min){
			return 1004;
		}
		
		$cashvotes=$money*$cash_rate;
        
        
        $ifok=DI()->notorm->user
            ->where('id = ? and votes>=?', $uid,$cashvotes)
            ->update(array('votes' => new NotORM_Literal("votes - {$cashvotes}")) );
        if(!$ifok){
            return 1001;
        }
		
		
		
		$data=array(
			"uid"=>$uid,
			"money"=>$money,
			"votes"=>$cashvotes,
			"orderno"=>$uid.'_'.$nowtime.rand(100,999),
			"status"=>0,
			"addtime"=>$nowtime,
			"uptime"=>$nowtime,
			"type"=>$accountinfo['type'],
			"account_bank"=>$accountinfo['account_bank'],
			"account"=>$accountinfo['account'],
			"name"=>$accountinfo['name'],
		);
		
		$rs=DI()->notorm->cash_record->insert($data);
		if(!$rs){
            return 1002;
		}	        
            
        
						
		
		return $rs;
	}
	
	/* 关注 */
	public function setAttent($uid,$touid,$lan){
		$isexist=DI()->notorm->user_attention
					->select("*")
					->where('uid=? and touid=?',$uid,$touid)
					->fetchOne();
		if($isexist){
			DI()->notorm->user_attention
				->where('uid=? and touid=?',$uid,$touid)
				->delete();
			return 0;
		}else{
		    $users = getUserInfo($uid); 
// 			DI()->notorm->user_black
				// ->where('uid=? and touid=?',$uid,$touid)
				// ->delete();
			DI()->notorm->user_attention
				->insert(array("uid"=>$uid,"touid"=>$touid,'add_time'=>time()));
		   //  $push = TPNSPush('您有新的关注信息', '‘'.$users['user_nickname'].'’'.'用户关注了你！', $touid, 3, ''); 

		   $lang = getUserInfo($touid);
                if($lang['lang']=='en'){

                   $push = TPNSPush('Notification', $users['user_nickname'].' is following you', $touid, 3, '');
                  $language = 'is following you';
                }elseif($lang['lang']=='ms'){
          
                   $push = TPNSPush('Notifikasi', $users['user_nickname'].' mengikuti anda', $touid, 3, '');
                  $language = 'mengikuti anda';
                }else{
                  $push = TPNSPush('您有新的关注信息',$users['user_nickname'].' 用户关注了你', $touid, 3, '');
                   $language = '用户关注了你';
                }
		     
		     
        	//file_put_contents(API_ROOT.'/../record/yun.txt',json_encode($push)."\r\n",FILE_APPEND);		
            $remindData=[
                'type'=>3,
                'target'=>'subscribe',
                //'target_id'=>'',
                'target_owner'=>$uid,
                'add_time'=>time(),
                'action'=>'用户关注了你',
                'user_id'=>$touid,
            ];
             DI()->notorm->remind
            ->insert($remindData);	
			return 1;
		}			 
	}
	
		/* 判断是否拉黑 */
	public function isBlack($uid,$touid){
	    
	   // return $uid."---".$touid;exit;
		$isexist=DI()->notorm->user_black
					->select("*")
					->where('uid=? and touid=? and type = 1',$uid,$touid)
					->fetchOne();
		if($isexist){
			return 1;
		}else{

			return 0;
		}			 
	}
	
	/* 拉黑 */
	public function setBlack($uid,$touid,$type){
	    
	    $isexist=DI()->notorm->user_black
					->select("*")
					->where('uid=? and touid=?',$uid,$touid)
					->fetchOne();
					
					
	    if($isexist){
	        if($type == 1){
	            DI()->notorm->user_black
                    ->where('uid=? and touid=?',$uid,$touid)
                    ->update(array("type"=>1 ));
	            return 1;
	            
	        }else if($type == 0){
	            DI()->notorm->user_black
                    ->where('uid=? and touid=?',$uid,$touid)
                    ->update(array("type"=>0 ));
	            return 0;
	            
	        }
	        

	    }else{
	        
	       DI()->notorm->user_black
        			->insert(array("uid"=>$uid,"touid"=>$touid,"type"=>1));
        	        return 1;
	        
	    }
	   // if($isexist){
	        
	   //     if($type == 1){
	        
    // 		    DI()->notorm->user_black
    //     			->insert(array("uid"=>$uid,"touid"=>$touid,"type"=>0));
    //     	        return 0;
    // 	    }

	   // }
	    

	    
	    
	   // else if($type == 0){
	        
	        
	   // }

	   // if($type == 0){
	        
	   //     DI()->notorm->user_black
    //                 ->where('uid=? and touid=?',$uid,$touid)
    //                 ->update(array("type"=>1 ));
	   //     return 1;
	        
	   // }
	   
	   
	   
// 		$isexist=DI()->notorm->user_black
// 					->select("*")
// 					->where('uid=? and touid=?',$uid,$touid)
// 					->fetchOne();
// 		if($isexist){
// 			DI()->notorm->user_black
// 				->where('uid=? and touid=?',$uid,$touid)
// 				->delete();
// 			return 0;
// 		}else{
// 			DI()->notorm->user_attention
// 				->where('uid=? and touid=?',$uid,$touid)
// 				->delete();
// 			DI()->notorm->user_black
// 				->insert(array("uid"=>$uid,"touid"=>$touid,"type"=>1));

// 			return 1;
// 		}			 
	}
	
	/* 关注列表 */
	public function getFollowsList($uid,$touid,$p){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$touids=DI()->notorm->user_attention
					->select("touid")
					->where('uid=?',$touid)
					->limit($start,$pnum)
					->fetchAll();
		foreach($touids as $k=>$v){
			$userinfo=getUserInfo($v['touid']);
			if($userinfo){
				if($uid==$touid){
					$isattent='1';
				}else{
					$isattent=isAttention($uid,$v['touid']);
				}
				$userinfo['isattention']=$isattent;
				$touids[$k]=$userinfo;
			}else{
				DI()->notorm->user_attention->where('uid=? or touid=?',$v['touid'],$v['touid'])->delete();
				unset($touids[$k]);
			}
		}		
		$touids=array_values($touids);
		return $touids;
	}
	
	/* 粉丝列表 */
	public function getFansList($uid,$touid,$p){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$touids=DI()->notorm->user_attention
					->select("uid")
					->where('touid=?',$touid)
					->limit($start,$pnum)
					->fetchAll();
		foreach($touids as $k=>$v){
			$userinfo=getUserInfo($v['uid']);
			if($userinfo){
				$userinfo['isattention']=isAttention($uid,$v['uid']);
				$touids[$k]=$userinfo;
			}else{
				DI()->notorm->user_attention->where('uid=? or touid=?',$v['uid'],$v['uid'])->delete();
				unset($touids[$k]);
			}
			
		}		
		$touids=array_values($touids);
		return $touids;
	}	

	/* 黑名单列表 */
	public function getBlackList($uid,$p){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$touids=DI()->notorm->user_black
					->select("touid")
					->where('uid=? and type=?',$uid,1)
					->limit($start,$pnum)
					->fetchAll();
		foreach($touids as $k=>$v){
			$userinfo=getUserInfo($v['touid']);
			if($userinfo){
				$touids[$k]=$userinfo;
			}else{
				DI()->notorm->user_black->where('uid=? or touid=?',$v['touid'],$v['touid'])->delete();
				unset($touids[$k]);
			}
		}
		$touids=array_values($touids);
		return $touids;
	}
	
	/* 直播记录 */
	public function getLiverecord($touid,$p){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$record=DI()->notorm->live_record
					->select("id,uid,nums,starttime,endtime,title,city")
					->where('uid=?',$touid)
					->order("id desc")
					->limit($start,$pnum)
					->fetchAll();
		foreach($record as $k=>$v){
			$record[$k]['datestarttime']=date("Y.m.d",$v['starttime']);
			$record[$k]['dateendtime']=date("Y.m.d",$v['endtime']);
            $cha=$v['endtime']-$v['starttime'];
			$record[$k]['length']=getSeconds($cha);
		}						
		return $record;						
	}	
	
		/* 个人主页 */
	public function getUserHome($uid,$touid){
		$info=getUserInfo($touid);

		$info['follows']=(string)getFollows($touid);
		$info['fans']=(string)getFans($touid);
		$info['isattention']=(string)isAttention($uid,$touid);
		$info['isblack']=(string)isBlack($uid,$touid);
		$info['isblack2']=(string)isBlack($touid,$uid);
        
        /* 直播状态 */
        $islive='0';
        $isexist=DI()->notorm->live
                    ->select('uid')
					->where('uid=? and islive=1',$touid)
					->fetchOne();
        if($isexist){
            $islive='1';
        }
		$info['islive']=$islive;	        
		
		/* 贡献榜前三 */
		$rs=array();
		$rs=DI()->notorm->user_coinrecord
				->select("uid,sum(totalcoin) as total")
				->where('action=1 and touid=?',$touid)
				->group("uid")
				->order("total desc")
				->limit(0,3)
				->fetchAll();
		foreach($rs as $k=>$v){
			$userinfo=getUserInfo($v['uid']);
			$rs[$k]['avatar']=$userinfo['avatar'];
		}		
		$info['contribute']=$rs;	
		
        /* 视频数 */

		if($uid==$touid){  //自己的视频（需要返回视频的状态前台显示）
			$where=" uid={$uid} and isdel='0' and status=1  and is_ad=0";
		}else{  //访问其他人的主页视频
            $videoids_s=getVideoBlack($uid);
			$where="id not in ({$videoids_s}) and uid={$touid} and isdel='0' and status=1  and is_ad=0";
		}
        
		$videonums=DI()->notorm->video
				->where($where)
				->count();
        if(!$videonums){
            $videonums=0;
        }

        $info['videonums']=(string)$videonums;
		  /* 动态数 */

		if($uid==$touid){  //自己的动态（需要返回动态的状态前台显示）
			$whered=" uid={$uid} and isdel='0' and status=1";
		}else{  //访问其他人的主页动态
			$whered=" uid={$touid} and isdel='0' and status=1  ";
		}
        
		$dynamicnums=DI()->notorm->dynamic
				->where($whered)
				->count();
        if(!$dynamicnums){
            $dynamicnums=0;
        }

        $info['dynamicnums']=(string)$dynamicnums;
        /* 直播数 */
        $livenums=DI()->notorm->live_record
					->where('uid=?',$touid)
					->count();
                    
        $info['livenums']=$livenums;        
		/* 直播记录 */
		$record=array();
		$record=DI()->notorm->live_record
					->select("id,uid,nums,starttime,endtime,title,city")
					->where('uid=?',$touid)
					->order("id desc")
					->limit(0,50)
					->fetchAll();
		foreach($record as $k=>$v){
			$record[$k]['datestarttime']=date("Y.m.d",$v['starttime']);
			$record[$k]['dateendtime']=date("Y.m.d",$v['endtime']);
            $cha=$v['endtime']-$v['starttime'];
            $record[$k]['length']=getSeconds($cha);
		}		
		$info['liverecord']=$record;	
		return $info;
	}
	
	/* 贡献榜 */
	public function getContributeList($touid,$p){
		if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;

		$rs=array();
		$rs=DI()->notorm->user_coinrecord
				->select("uid,sum(totalcoin) as total")
				->where('touid=?',$touid)
				->group("uid")
				->order("total desc")
				->limit($start,$pnum)
				->fetchAll();
				
		foreach($rs as $k=>$v){
			$rs[$k]['userinfo']=getUserInfo($v['uid']);
		}		
		
		return $rs;
	}
	
	/* 设置分销 */
	public function setDistribut($uid,$code){
        
        $isexist=DI()->notorm->agent
				->select("*")
				->where('uid=?',$uid)
				->fetchOne();
        if($isexist){
            return 1004;
        }
        
        //获取邀请码用户信息
		$oneinfo=DI()->notorm->agent_code
				->select("uid")
				->where('code=? and uid!=?',$code,$uid)
				->fetchOne();
		if(!$oneinfo){
			return 1002;
		}
		
		//获取邀请码用户的邀请信息
		$agentinfo=DI()->notorm->agent
				->select("*")
				->where('uid=?',$oneinfo['uid'])
				->fetchOne();
		if(!$agentinfo){
			$agentinfo=array(
				'uid'=>$oneinfo['uid'],
				'one_uid'=>0,
			);
		}
        // 判断对方是否自己下级
        if($agentinfo['one_uid']==$uid ){
            return 1003;
        }
		
		$data=array(
			'uid'=>$uid,
			'one_uid'=>$agentinfo['uid'],
			'addtime'=>time(),
		);
		DI()->notorm->agent->insert($data);
		return 0;
	}
    
    
    /* 印象标签 */
    public function getImpressionLabel(){
        
        $key="getImpressionLabel";
		$list=getcaches($key);
		if(!$list){
            $list=DI()->notorm->label
				->select("*")
				->order("list_order asc,id desc")
				->fetchAll();
            if($list){
                setcaches($key,$list); 
            }
			
		}

        return $list;
    }       
    /* 用户标签 */
    public function getUserLabel($uid,$touid){
        $list=DI()->notorm->label_user
				->select("label")
                ->where('uid=? and touid=?',$uid,$touid)
				->fetchOne();
                
        return $list;
        
    }    

    /* 设置用户标签 */
    public function setUserLabel($uid,$touid,$labels){
        $nowtime=time();
        $isexist=DI()->notorm->label_user
				->select("*")
                ->where('uid=? and touid=?',$uid,$touid)
				->fetchOne();
        if($isexist){
            $rs=DI()->notorm->label_user
                ->where('uid=? and touid=?',$uid,$touid)
				->update(array( 'label'=>$labels,'uptime'=>$nowtime ) );
            
        }else{
            $data=array(
                'uid'=>$uid,
                'touid'=>$touid,
                'label'=>$labels,
                'addtime'=>$nowtime,
                'uptime'=>$nowtime,
            );
            $rs=DI()->notorm->label_user->insert($data);
        }
                
        return $rs;
        
    }    
    
    /* 获取我的标签 */
    public function getMyLabel($uid){
        $rs=array();
        $list=DI()->notorm->label_user
				->select("label")
                ->where('touid=?',$uid)
				->fetchAll();
        $label=array();
        foreach($list as $k=>$v){
            $v_a=preg_split('/,|，/',$v['label']);
            $v_a=array_filter($v_a);
            if($v_a){
                $label=array_merge($label,$v_a);
            }
            
        }

        if(!$label){
            return $rs;
        }
        
        
        $label_nums=array_count_values($label);
        
        $label_key=array_keys($label_nums);
        
        $labels=$this->getImpressionLabel();
        
        $order_nums=array();
        foreach($labels as $k=>$v){
            if(in_array($v['id'],$label_key)){
                $v['nums']=(string)$label_nums[$v['id']];
                $order_nums[]=$v['nums'];
                $rs[]=$v;
            }
        }
        
        array_multisort($order_nums,SORT_DESC,$rs);
        
        return $rs;
        
    }   
    
    /* 获取关于我们列表 */
    public function getPerSetting(){
        $rs=array();
        
        $list=DI()->notorm->portal_post
				->select("id,post_title")
                ->where("type='2'")
                ->order('list_order asc')
				->fetchAll();
        foreach($list as $k=>$v){
            
            $rs[]=array('id'=>'0','name'=>$v['post_title'],'thumb'=>'' ,'href'=>get_upload_path("/portal/page/index?id={$v['id']}"));
        }
        
        return $rs;
    }
    
    /* 提现账号列表 */
    public function getUserAccountList($uid){
        
        $list=DI()->notorm->cash_account
                ->select("*")
                ->where('uid=?',$uid)
                ->order("addtime desc")
                ->fetchAll();
                
        return $list;
    }

    /* 账号信息 */
    public function getUserAccount($where){
        
        $list=DI()->notorm->cash_account
                ->select("*")
                ->where($where)
                ->order("addtime desc")
                ->fetchAll();
                
        return $list;
    }
    /* 设置提账号 */
    public function setUserAccount($data){
        
        $rs=DI()->notorm->cash_account
                ->insert($data);
                
        return $rs;
    }

    /* 删除提账号 */
    public function delUserAccount($data){
        
        $rs=DI()->notorm->cash_account
                ->where($data)
                ->delete();
                
        return $rs;
    }
    
	/* 登录奖励信息 */
	public function LoginBonus($uid){
		$rs=array(
			'bonus_switch'=>'0',
			'bonus_day'=>'0',
			'count_day'=>'0',
			'bonus_list'=>array(),
		);
        
        //file_put_contents(API_ROOT.'/Runtime/LoginBonus_'.date('Y-m-d').'.txt',date('Y-m-d H:i:s').' 提交参数信息 uid:'.json_encode($uid)."\r\n",FILE_APPEND);
		$configpri=getConfigPri();
		if(!$configpri['bonus_switch']){
			return $rs;
		}
		$rs['bonus_switch']=$configpri['bonus_switch'];

		//file_put_contents(API_ROOT.'/Runtime/LoginBonus_'.date('Y-m-d').'.txt',date('Y-m-d H:i:s').' 提交参数信息 bonus_switch:'."\r\n",FILE_APPEND);
		/* 获取登录设置 */
        $key='loginbonus';
		$list=getcaches($key);
		if(!$list){
            $list=DI()->notorm->loginbonus
					->select("day,coin")
					->fetchAll();
			if($list){
				setcaches($key,$list);
			}
		}
        
        //file_put_contents(API_ROOT.'/Runtime/LoginBonus_'.date('Y-m-d').'.txt',date('Y-m-d H:i:s').' 提交参数信息 list:'."\r\n",FILE_APPEND);
		$rs['bonus_list']=$list;
		$bonus_coin=array();
		foreach($list as $k=>$v){
			$bonus_coin[$v['day']]=$v['coin'];
		}

		/* 登录奖励 */
		$signinfo=DI()->notorm->user_sign
					->select("bonus_day,bonus_time,count_day")
					->where('uid=?',$uid)
					->fetchOne();
        //file_put_contents(API_ROOT.'/Runtime/LoginBonus_'.date('Y-m-d').'.txt',date('Y-m-d H:i:s').' 提交参数信息 signinfo:'."\r\n",FILE_APPEND);
		if(!$signinfo){
			$signinfo=array(
				'bonus_day'=>'0',
				'bonus_time'=>'0',
				'count_day'=>'0',
			);
        }
        $nowtime=time();
        if($nowtime - $signinfo['bonus_time'] > 60*60*24){
            $signinfo['count_day']=0;
        }
        $rs['count_day']=(string)$signinfo['count_day'];
		
		if($nowtime>$signinfo['bonus_time']){
			//更新
			$bonus_time=strtotime(date("Ymd",$nowtime))+60*60*24;
			$bonus_day=$signinfo['bonus_day'];
			if($bonus_day>6){
				$bonus_day=0;
			}
			$bonus_day++;
            $coin=$bonus_coin[$bonus_day];
            
			if($coin){
                $rs['bonus_day']=(string)$bonus_day;
            }
			
		}
        //file_put_contents(API_ROOT.'/Runtime/LoginBonus_'.date('Y-m-d').'.txt',date('Y-m-d H:i:s').' 提交参数信息 rs:'."\r\n",FILE_APPEND);
		return $rs;
	}
    
	/* 获取登录奖励 */
	public function getLoginBonus($uid){
		$rs=0;
		$configpri=getConfigPri();
		if(!$configpri['bonus_switch']){
			return $rs;
		}
		
		/* 获取登录设置 */
        $key='loginbonus';
		$list=getcaches($key);
		if(!$list){
            $list=DI()->notorm->loginbonus
					->select("day,coin")
					->fetchAll();
			if($list){
				setcaches($key,$list);
			}
		}

		$bonus_coin=array();
		foreach($list as $k=>$v){
			$bonus_coin[$v['day']]=$v['coin'];
		}
		
		$isadd=0;
		/* 登录奖励 */
		$signinfo=DI()->notorm->user_sign
					->select("bonus_day,bonus_time,count_day")
					->where('uid=?',$uid)
					->fetchOne();
		if(!$signinfo){
			$isadd=1;
			$signinfo=array(
				'bonus_day'=>'0',
				'bonus_time'=>'0',
				'count_day'=>'0',
			);
        }
		$nowtime=time();
		if($nowtime>$signinfo['bonus_time']){
			//更新
			$bonus_time=strtotime(date("Ymd",$nowtime))+60*60*24;
			$bonus_day=$signinfo['bonus_day'];
			$count_day=$signinfo['count_day'];
			if($bonus_day>6){
				$bonus_day=0;
			}
            if($nowtime - $signinfo['bonus_time'] > 60*60*24){
                $count_day=0;
            }
			$bonus_day++;
			$count_day++;
            
 
            if($isadd){
                DI()->notorm->user_sign
                    ->insert(array("uid"=>$uid,"bonus_time"=>$bonus_time,"bonus_day"=>$bonus_day,"count_day"=>$count_day ));
            }else{
                DI()->notorm->user_sign
                    ->where('uid=?',$uid)
                    ->update(array("bonus_time"=>$bonus_time,"bonus_day"=>$bonus_day,"count_day"=>$count_day ));
            }
            
            $coin=$bonus_coin[$bonus_day];
            
			if($coin){
                DI()->notorm->user
                    ->where('id=?',$uid)
                    ->update(array( "coin"=>new NotORM_Literal("coin + {$coin}") ));
				

                /* 记录 */
                $insert=array("type"=>'1',"action"=>'3',"uid"=>$uid,"touid"=>$uid,"giftid"=>$bonus_day,"giftcount"=>'0',"totalcoin"=>$coin,"showid"=>'0',"addtime"=>$nowtime );
                DI()->notorm->user_coinrecord->insert($insert);
            }
            $rs=1;
		}
		
		return $rs;
		
	}

	//检测用户是否填写了邀请码
	public function checkIsAgent($uid){
		$info=DI()->notorm->agent->where("uid=?",$uid)->fetchOne();
		if(!$info){
			return 0;
		}

		return 1;
	}

	//用户商城提现
    public function setShopCash($data){
        
        $nowtime=time();
        
        $uid=$data['uid'];
        $accountid=$data['accountid'];
        $money=$data['money'];
        
        $configpri=getConfigPri();
        $balance_cash_start=$configpri['balance_cash_start'];
        $balance_cash_end=$configpri['balance_cash_end'];
        $balance_cash_max_times=$configpri['balance_cash_max_times'];
        
        $day=(int)date("d",$nowtime);
        
        if($day < $balance_cash_start || $day > $balance_cash_end){
            return 1005;
        }
        
        //本月第一天
        $month=date('Y-m-d',strtotime(date("Ym",$nowtime).'01'));
        $month_start=strtotime(date("Ym",$nowtime).'01');

        //本月最后一天
        $month_end=strtotime("{$month} +1 month");
        
        if($balance_cash_max_times){
            $count=DI()->notorm->user_balance_cashrecord
                    ->where('uid=? and addtime > ? and addtime < ?',$uid,$month_start,$month_end)
                    ->count();
            if($count >= $balance_cash_max_times){
                return 1006;
            }
        }
        
        
        /* 钱包信息 */
        $accountinfo=DI()->notorm->cash_account
                ->select("*")
                ->where('id=? and uid=?',$accountid,$uid)
                ->fetchOne();

        if(!$accountinfo){
            return 1007;
        }
        

        /* 最低额度 */
        $balance_cash_min=$configpri['balance_cash_min'];
        
        if($money < $balance_cash_min){
            return 1004;
        }
        

        $ifok=DI()->notorm->user
            ->where('id = ? and balance>=?', $uid,$money)
            ->update(array('balance' => new NotORM_Literal("balance - {$money}")) );

        if(!$ifok){
            return 1001;
        }
        
        
        
        $data=array(
            "uid"=>$uid,
            "money"=>$money,
            "orderno"=>$uid.'_'.$nowtime.rand(100,999),
            "status"=>0,
            "addtime"=>$nowtime,
            "type"=>$accountinfo['type'],
            "account_bank"=>$accountinfo['account_bank'],
            "account"=>$accountinfo['account'],
            "name"=>$accountinfo['name'],
        );
        
        $rs=DI()->notorm->user_balance_cashrecord->insert($data);
        if(!$rs){
            return 1002;
        }           
            
        return $rs;
    }

    //获取认证信息
    public function getAuthInfo($uid){
    	$info=DI()->notorm->user_auth
    			->where("uid=? and status=1",$uid)
    			->select("real_name,cer_no")
    			->fetchOne();
    	return $info;
    }
    
    //用户首次登录设置推荐分类
    public function setRecomClass($uid, $recomClass) {
        
        $info = '0';
        
        if(DI()->notorm->user_recom->where('uid = ?', $uid)->fetchOne() == '') {
            DI()->notorm->user_recom
                ->insert(array('uid' => $uid, 'recom' => $recomClass));
            return '1';
        } else {
            $info = DI()->notorm->user_recom
                ->where('uid = ?', $uid)
                ->update(array('recom' => $recomClass));
                return '1';
        }
        
        return $info;
        
    }
    
    //用户修改推荐分类
    public function updateRecomClass($uid, $recomClass) {
        
        if(DI()->notorm->user_recom->where('uid = ?', $uid)->fetchOne() == '') {
            
            return '0';
        }
        
        $info = DI()->notorm->user_recom
                ->where('uid = ?', $uid)
                ->update(array('recom' => $recomClass));
        
        return $info;
        
    }
    
    public function getUserOrderInfo($id){
        $info=DI()->notorm->user
            ->where("id=?",$id)
            //->select("real_name,cer_no")
            ->fetch();
        return $info;
    }
    public function myRelated($id){
            $info=$this->getORM()
            ->where("id=?",$id)
            //->select("real_name,cer_no")
			->fetch();
		  if(!$info){
			return [];
		  }
          $info['avatar']=get_upload_path($info['avatar']);
          $info['background']=get_upload_path($info['background']);
          $info['followNum']=  DI()->notorm->user_attention->where("uid=?",$id)->count();
          $info['fansNum']=  DI()->notorm->user_attention->where("touid=?",$id)->count();
          $tiezi = DI()->notorm->dynamic->where('uid=? and isdel = 0 and status = 1',$id)->select("sum(reading) as reading")->fetchAll();
       
          $info['read'] = $tiezi[0]['reading'] ?:0;
          $likes = DI()->notorm->dynamic->where('uid=?',$id)->where('status=?',1)->select("id")->fetchAll();
          $new = [];
          if($likes){
            foreach ($likes as $val){
                $new[]=$val['id'];              
            }
            $sIds =implode(',',$new);
            $where= " note_id in ({$sIds})" ;
            $likesNum = DI()->notorm->note_like->where($where)->count();
          }
          $info['like_num']= $likesNum?$likesNum:0;
          //查看
          $info['type'] = ''; //最低,未认证
          if($info['user_status'] == 1){
                $info['type'] = 'blue'; //认证过的
          }
          if($info['major'] == 1){
                $info['type'] = 'green';
          }
          
          return $info;
    }
    public function getFollow($uid,$type,$p){
         if($p<1){
            $p=1;
        }
		$pnum=10;
		$start=($p-1)*$pnum;
		if($type==1){
    		$touids=DI()->notorm->user_attention
			->select("touid")
			->where('uid=?',$uid)
			->limit($start,$pnum)
			->fetchAll();
		foreach($touids as $k=>$v){
			$userinfo=getUserInfo($v['touid']);
			$userinfo['avatar'] =get_upload_path($userinfo['avatar']);
			$touids[$k]=$userinfo;
		}		
		return $touids;
		}else{
    		$touids=DI()->notorm->user_attention
					->select("uid")
					->where('touid=?',$uid)
					->limit($start,$pnum)
					->fetchAll();
		foreach($touids as $k=>$v){
			$userinfo=getUserInfo($v['uid']);
			$touids[$k]=$userinfo;

		}		
		return $touids;
		}

    
    }
    
}
