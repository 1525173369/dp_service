<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 15:38
 */
class Model_Address extends PhalApi_Model_NotORM {
    protected function getTableName($id) {
        return 'user_address';  // 手动设置表名为
    }

    public function getLists($id){
        $where= "user_id=".$id;
        $list=$this->getORM()
            ->where($where)->fetchAll();
        return $list;
    }

    public function addAddress($data,$uid){
        $data['user_id']=$uid;
        $rs=DI()->notorm->user_address->insert($data);
        if(!$rs){
            return 1002;
        }
        return $rs;

    }
    public function updateAddress($data,$id){
        DI()->notorm->user_address
            ->where('address_id=?',$id)
            ->update($data);
        return 1;
                

    }
    public function setDefault($uid,$address_id){
        DI()->notorm->user_address
            ->where('is_default=?',1)
            ->update([
                'is_default'=>0,
            ]);
        DI()->notorm->user_address
            ->where('address_id=?',$address_id)
            ->update([
                'is_default'=>1,
            ]);
        return 1;

    }
    
    public function getInfo($id){
        $rs  = DI()->notorm->user_address
            ->where('address_id=?',$id)
            ->fetch();
        return $rs;
    }
    
    public function del($uid,$address_id){
        DI()->notorm->user_address
            ->where('address_id=?',$address_id)
            ->where('user_id=?',$uid)
            ->delete();
        return 1;
        
        
    }


}