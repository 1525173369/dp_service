<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/10 0010
 * Time: 9:38
 */
class Model_Remind extends PhalApi_Model_NotORM {
    protected function getTableName($id) {
        return 'remind';  // 手动设置表名为
    }
    public function getList($uid,$p){
        $pnum=10;
        $start=($p-1)*$pnum;
        $where="A.type=0 and A.user_id=".$uid;
        $list= $this->getORM()->alias('A')
            ->leftJoin('user', 'B', 'A.target_owner = B.id')
            ->where($where)
            ->select('A.*,B.user_nickname,B.avatar')->order('A.add_time desc')
            ->limit($start,$pnum)
            ->fetchAll();
        foreach($list as $k=>$v){
            $list[$k]['add_time']=date('Y-m-d H:i',$v['add_time']);
            $list[$k]['avatar']=get_upload_path($v['avatar']);
        }    
        return $list;

    }
    public function getCommentList($uid,$p){
        $pnum=10;
        $start=($p-1)*$pnum;
        $where="A.type=2 and A.user_id=".$uid;
        $list= $this->getORM()->alias('A')
            ->leftJoin('user', 'B', 'A.target_owner = B.id')
            ->where($where)
            ->select('A.*,B.user_nickname,B.avatar')->order('A.add_time desc')
            ->limit($start,$pnum)
            ->fetchAll();
        foreach($list as $k=>$v){
            $list[$k]['add_time']=date('Y-m-d H:i',$v['add_time']);
            $list[$k]['avatar']=$v['avatar']?get_upload_path($v['avatar']):'';
            $list[$k]['user_nickname']=$v['user_nickname']?$v['user_nickname']:'';
        }    
        return $list;

    }
    public function getLikes($uid,$p){
        $pnum=10;
        $start=($p-1)*$pnum;
        $where="A.type=1 and A.user_id=".$uid;
        $list= $this->getORM()->alias('A')
            ->leftJoin('user', 'B', 'A.target_owner = B.id')
            ->where($where)
            ->select('A.*,B.user_nickname,B.avatar')->order('A.add_time desc')
            ->limit($start,$pnum)
            ->fetchAll();
        foreach($list as $k=>$v){
            $list[$k]['add_time']=date('Y-m-d H:i',$v['add_time']);
            $list[$k]['avatar']=get_upload_path($v['avatar']);
        }    
        return $list;

    }
    
    public function getSubscribe($uid,$p){
        $pnum=10;
        $start=($p-1)*$pnum;
        $where="A.type=3 and A.user_id=".$uid;
        $list= $this->getORM()->alias('A')
            ->leftJoin('user', 'B', 'A.target_owner = B.id')
            ->where($where)
            ->select('A.*,B.user_nickname,B.avatar')->order('A.add_time desc')
            ->limit($start,$pnum)
            ->fetchAll();
        foreach($list as $k=>$v){
            $list[$k]['add_time']=date('Y-m-d H:i',$v['add_time']);
            $list[$k]['avatar']=get_upload_path($v['avatar']);
        }    
        return $list;

    }



}