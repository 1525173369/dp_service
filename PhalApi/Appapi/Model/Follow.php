<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/10 0010
 * Time: 13:55
 */
class Model_Follow extends PhalApi_Model_NotORM {

    public function addData($data){

        $res=DI()->notorm->follow
            ->insert($data);

        if(!$res){
            return 1004;
        }
        $remindData=[
            'type'=>3,
            'target'=>'subscribe',
            //'target_id'=>'',
            'target_owner'=>$data['from_user_id'],
            'add_time'=>time(),
            'action'=>'用户关注了你',
            'user_id'=>$data['to_user_id'],
        ];
        DI()->notorm->remind
            ->insert($remindData);

        return 1;

    }




}