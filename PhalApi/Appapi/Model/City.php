<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/7 0007
 * Time: 15:38
 */
class Model_City extends PhalApi_Model_NotORM {


    public function getLists(){
        $res = DI()->notorm->area->order('first asc')->fetchAll();
        $res= array2level($res);
        return $res;
    }
    public function vTree($arr,$pid=0){
        foreach($arr as $k => $v){
            if($v['pid']==$pid){
                $data[$v['id']]=$v;
                $data[$v['id']]['sub']=self::vTree($arr,$v['id']) ?: "" ;
            }
        }
        return isset($data)?$data:array();
    }


}