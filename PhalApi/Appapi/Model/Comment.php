<?php

/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/3 0003
 * Time: 15:51
 */
class Model_Comment extends PhalApi_Model_NotORM
{

    protected function getTableName($id)
    {
        return 'shop_comment';  // 手动设置表名为
    }

    public function addComment($data, $lan)
    {
        $users = getUserInfo($data['user_id']);
        $data['content'] = urldecode($data['content']);
        $res = DI()->notorm->shop_comment
            ->insert($data);
        if (!$res) {
            return 1004;
        }

        $user = DI()->notorm->shop_apply->where("id = '{$data['shop_id']}'")->fetch();
        $lang = getUserInfo($user['user_id']);
        DI()->notorm->shop_apply->where("id=?", $data['shop_id'])
            ->update(array('consumption' => new NotORM_Literal("consumption + 1")));
        if ($data['user_id'] != $user['uid'] && !empty($user['uid']) && $data['cover_id'] == '') {
            $lang = getUserInfo($user['uid']);
            if ($lang['lang'] == 'en') {
                $push = TPNSPush('Notification', $users['user_nickname'] . ' commented on your store', $user['uid'], 2, '');
                $language = 'commented on your store';
            } elseif ($lang['lang'] == 'ms') {
                $push = TPNSPush('Notifikasi', $users['user_nickname'] . ' Komen di kedai anda', $user['uid'], 2, '');
                $language = 'Komen di kedai anda';
            } else {
                $push = TPNSPush('您有新的店铺评论消息', $users['user_nickname'] . '用户评论了你的店铺', $user['uid'], 2, '');
                $language = '用户评论了你的店铺';
            }
            $remindData = [
                'type' => 2,
                'target' => 'shop',
                'target_id' => $data['shop_id'],
                'target_owner' => $data['user_id'],
                'add_time' => time(),
                'action' => '用户评论了你的店铺',
                'user_id' => $user['uid'] ? $user['uid'] : 0,
            ];
            DI()->notorm->remind
                ->insert($remindData);


        } elseif (empty($user['uid']) && $data['user_id'] != $user['provider_id'] && $data['cover_id'] == '') {

            $lang = getUserInfo($user['provider_id']);
            if ($lang['lang'] == 'en') {
                $language = 'commented on your store';
                TPNSPush('Notification', $users['user_nickname'] . ' commented on your store', $user['provider_id'], 2, '');
            } elseif ($lang['lang'] == 'ms') {
                $language = 'Komen di kedai anda';
                TPNSPush('Notifikasi', $users['user_nickname'] . ' Komen di kedai anda', $user['provider_id'], 2, '');
            } else {
                $language = '用户评论了你的店铺';
                TPNSPush('您有新的店铺评论消息', $users['user_nickname'] . '用户评论了你的店铺', $user['provider_id'], 2, '');
            }
            $remindData = [
                'type' => 2,
                'target' => 'shop',
                'target_id' => $data['shop_id'],
                'target_owner' => $data['user_id'],
                'add_time' => time(),
                'action' => '用户评论了你的店铺',
                'user_id' => $user['uid'] ? $user['uid'] : 0,
            ];
            DI()->notorm->remind
                ->insert($remindData);
        }else{
            $data['note_id'] = $data['cover_id'];
            $user = DI()->notorm->shop_comment->where("comment_id = '{$data['note_id']}'")->select('user_id as uid')->fetchOne();
            $lang = getUserInfo($user['uid']);
            if ($lang['lang'] == 'en') {
                $language = 'replied your comment';
                TPNSPush('Notification', $users['user_nickname'] .' '. $language, $user['uid'], 2, '');
            } elseif ($lang['lang'] == 'ms') {
                $language = 'telah membalas komen anda';
                TPNSPush('Notifikasi', $users['user_nickname'] .' '. $language, $user['uid'], 2, '');
            } else {
                $language = '回复了您的评论';
                $ret = TPNSPush('评论消息', $users['user_nickname'] .' '. $language, $user['uid'], 2, '');

            }
            $remindData = [
                'type' => 2,
                'target' => 'shop',
                'target_id' => $data['shop_id'],
                'target_owner' => $data['user_id'],
                'add_time' => time(),
                'action' => '回复了您的评论',
                'user_id' => $user['uid'] ? $user['uid'] : 0,
            ];
            DI()->notorm->remind
                ->insert($remindData);
        }

        //店铺评分start
        if ($data['service_rank']) {
            if ($user['points'] > 0) {
                $newPoints = ($user['points'] + $data['service_rank']) / 2;
                $newPoints = number_format($newPoints, 1);
                DI()->notorm->shop_apply->where("id=?", $data['shop_id'])->update(['points' => $newPoints]);
            } else {
                DI()->notorm->shop_apply->where("id=?", $data['shop_id'])->update(['points' => $data['service_rank']]);
            }

        }
        //end
        return 1;

    }

    public function delComment($uid, $id, $type)
    {
        if ($type == 1) {
            $shopComment = DI()->notorm->shop_comment->where("comment_id = '{$id}'")->fetch();
            if (!$shopComment) {
                return 1006;
            }
            if ($shopComment['user_id'] != $uid) {
                return 1005;
            }
            if (DI()->notorm->shop_comment->where("comment_id = '{$id}'")->delete()) {
                DI()->notorm->shop_apply->where('id=?', $shopComment['shop_id'])
                    ->update(array('consumption' => new NotORM_Literal("consumption - 1")));
                return;
            } else {
                return 1004;
            }
        } else {
            $noteComment = DI()->notorm->note_comment->where("comment_id = '{$id}'")->fetch();
            if (!$noteComment) {
                return 1006;
            }
          /*  if ($noteComment['user_id'] != $uid) {
                return 1005;
            }*/
            if (DI()->notorm->note_comment->where("comment_id = '{$id}'")->delete()) {
                DI()->notorm->dynamic->where('id=?', $noteComment['note_id'])
                    ->update(array('comments' => new NotORM_Literal("comments - 1")));
                return;
            } else {
                return 1004;
            }
        }
    }


    public function addNoteComment($data, $lan,$at = 0,$to_uid = null)
    {
        $res = DI()->notorm->note_comment
            ->insert($data);
        if (!$res) {
            return 1004;
        }
        $users = getUserInfo($data['user_id']);
        $user = $newuser = DI()->notorm->dynamic->where("id = '{$data['note_id']}'")->fetch();
      
        $lang = getUserInfo($user['uid']);
      
        if ($data['user_id'] != $user['uid'] && $data['cover_id'] ==0) {

            if ($lang['lang'] == 'en') {
                TPNSPush('Notification', $users['user_nickname'] . ' commented on your post', $user['uid'], 2, '');
                $language = '用户点评了你的帖子';
            } elseif ($lang['lang'] == 'ms') {

                TPNSPush('Notifikasi', $users['user_nickname'] . ' telah komen siaran anda', $user['uid'], 2, '');
                $language = '用户点评了你的帖子';
            } else {
                TPNSPush('评论消息', $users['user_nickname'] . '用户点评了你的帖子', $user['uid'], 2, '');
                $language = '用户点评了你的帖子';
            }
        }else{
            $user = DI()->notorm->note_comment->where("comment_id = '{$data['cover_id']}'")->select('user_id as uid')->fetchOne();
            $lang = getUserInfo($user['uid']);
            if ($lang['lang'] == 'en') {
                $language = '回复了您的评论';
                TPNSPush('Notification', $users['user_nickname'] . ' replied to your comment', $user['uid'], 2, '');
            } elseif ($lang['lang'] == 'ms') {
                $language = '回复了您的评论';
                TPNSPush('Notifikasi', $users['user_nickname'] . ' membalas komen anda', $user['uid'], 2, '');
            } else {
                $language = ' 回复了您的评论';
                TPNSPush('评论消息', $users['user_nickname'] . $language, $user['uid'], 2, '');
            }
        }
      
        
        if($at){  //有@的情况下处理的
            $to_uids = explode(',',$to_uid);
           
            foreach ($to_uids as $v){
                 $vuser = getUserInfo($v);
               
                 //var_dump($vuser);die;
                  if ($vuser['lang'] == 'en') {
                       $language =  ' mentioned you';
                       $r= TPNSPush('Notification', $users['user_nickname'] . ' mentioned you', $v, 2, '');
                     
                  } elseif ($vuser['lang'] == 'ms') {
                        $language = ' mention anda';
                        $r = TPNSPush('Notifikasi', $users['user_nickname'] . ' mention anda', $v, 2, '');
                  } else {
                        $language =  '回复了您的评论';
                        $r = TPNSPush('评论消息', $users['user_nickname'] . $language, $v, 2, '');
                  }
                  
                   $remindData = [
                        'type' => 2,
                        'target' => 'note',
                        'target_id' => $data['note_id'],
                        'target_owner' => $data['user_id'],
                        'add_time' => time(),
                        'action' => '在评论中提及你',
                        'user_id' => $vuser['id'] ? $vuser['id'] : 0
                    ];
                  
                    DI()->notorm->remind
                        ->insert($remindData);
                  
                  
            }
          
        }
       
     
        $remindData = [
            'type' => 2,
            'target' => 'note',
            'target_id' => $data['note_id'],
            'target_owner' => $data['user_id'],
            'add_time' => time(),
            'action' => $language,
            'user_id' => $newuser['uid'] ? $newuser['uid'] : 0
        ];
        
        DI()->notorm->remind
            ->insert($remindData);


        //增加评论数
        $comment_data['comments'] = $user['comments'] + 1;  //点赞数量+1
        DI()->notorm->dynamic
            ->where("id = '{$data['note_id']}'")
            ->update($comment_data);
        return 1;
    }

    public function getShopComment($id, $uid, $p)
    {
        //echo(11);
        if ($p < 1) {
            $p = 1;
        }
        $pnums = 10;
        $start = ($p - 1) * $pnums;
        $where = "A.is_show=0 and A.shop_id=" . $id;
        $list = $this->getORM()
            ->select('A.*,C.views, B.avatar,B.user_nickname') // 获取字段
            ->alias('A')
            ->leftJoin('user', 'B', 'A.user_id = B.id')
            ->leftJoin('shop_apply', 'C', 'A.shop_id = C.id')
            ->where($where)
            ->limit($start, $pnums)->order('A.create_time desc')
            ->fetchAll();
        foreach ($list as $k => $v) {
            $list[$k]['create_time'] = datetime($v['create_time']);
            $list[$k]['avatar'] = get_upload_path($v['avatar']) ? get_upload_path($v['avatar']) : '';
            $list[$k]['user_nickname'] = $v['user_nickname'] ? $v['user_nickname'] : '';
            $list[$k]['content'] = $v['content'];
            if (!$v['img']) {
                $list[$k]['img'] = '';
            }
            $list[$k]['is_like'] = 0;
            if ($v['zan_userid']) {
                if (in_array($uid, explode(',', $v['zan_userid']))) {
                    $list[$k]['is_like'] = 1;
                }
            }
        }
        if($list){
            $tree = $this->xmsb_getDataTree($list,'comment_id','cover_id','user_nickname','note_id','username','content','create_time','is_show','label','user_id','img','service_rank','zan_num','zan_userid','is_anonymous','avatar','user_nickname','is_like');
        }
        foreach($list as $k => $v){
            if($v['id'] == $tree[$k]['id']){
                $tree[$k]['avatar'] = get_upload_path($tree[$k]['avatar']);
                $list[$k]['children'] = $tree[$k]['children'];
            }
        }
        array_multisort($list,SORT_DESC);
        $count = $this->getORM()
            ->select('A.*,C.views, B.avatar,B.user_nickname') // 获取字段
            ->alias('A')
            ->leftJoin('user', 'B', 'A.user_id = B.id')
            ->leftJoin('shop_apply', 'C', 'A.shop_id = C.id')
            ->where($where)
            ->order('A.create_time desc')
            ->count();
        $info['count'] = $count;
        $info['list'] = $list;
       
        
        return $info;

    }
    /**
     * @desc   xmsb_getDataTree         无限级分类函数
     * @param array $datas 查询出的数据
     * @param string $primaryKey 数据表的主键
     * @param string $parentKey 父节点字段名
     * @param string $nameKey 需要用到的名称字段
     * @param integer $firstParent 根节点主键值
     * @return array                    返回树状结果集
     */
    public function xmsb_getDataTree($datas, $primaryKey, $parentKey, $nameKey,$note_id,$username,$content,$create_time,$is_show,$label,$user_id,$img,$service_rank,$zan_num,$zan_userid,$is_anonymous,$avatar,$user_nickname,$is_like ,$firstParent = 0)
    {
        if (!is_array($datas) || !$primaryKey || !$parentKey || !$nameKey) return false;

        $tree = array($primaryKey => 0, $parentKey => 0, $nameKey => 'xmsb');
        $tmpMap = array($firstParent => & $tree);

        foreach ($datas as $rk => $rv) {
            $tmpMap[$rv[$primaryKey]] = array
            (
                $primaryKey => $rv[$primaryKey],
                $parentKey => $rv[$parentKey],
                $nameKey => $rv[$nameKey],
                $note_id => $rv[$note_id],
                $username => $rv[$username],
                $content => $rv[$content],
                $create_time => $rv[$create_time],
                $is_show => $rv[$is_show],
                $label => $rv[$label],
                $user_id => $rv[$user_id],
                $img => $rv[$img],
                $service_rank => $rv[$service_rank],
                $zan_num => $rv[$zan_num],
                $zan_userid => $rv[$zan_userid],
                $is_anonymous => $rv[$is_anonymous],
                $avatar => $rv[$avatar],
                $user_nickname => $rv[$user_nickname],
                $is_like => $rv[$is_like],
            );

            $parentObj = &$tmpMap[$rv[$parentKey]];
            if (!$parentObj['children']) $parentObj['children'] = array();
            $parentObj['children'][] = &$tmpMap[$rv[$primaryKey]];
        }

        return $tree['children'];
    }


    public function addLike($uid, $id, $status, $lan)
    {
        $users = getUserInfo($uid);
        if ($status == 1) {  //店铺
            $rs = DI()->notorm->shop_comment->where("comment_id=?", $id)->fetch();
            $comment_user_id_array = explode(',', $rs['zan_userid']);
            if (in_array($uid, $comment_user_id_array)) {  //判断用户有没点赞过
                $key = array_search($uid, $comment_user_id_array);
                if (isset($key)) {
                    unset($comment_user_id_array[$key]);
                }
                $comment_user_id_string = implode(',', $comment_user_id_array);
                $comment_data['zan_num'] = $rs['zan_num'] - 1;  //点赞数量减1
                $comment_data['zan_userid'] = $comment_user_id_string;
                $rs = DI()->notorm->shop_comment
                    ->where("comment_id = '{$id}'")
                    ->update($comment_data);

            } else {
                array_push($comment_user_id_array, $uid);
                $comment_user_id_string = implode(',', $comment_user_id_array);
                $comment_data['zan_num'] = $rs['zan_num'] + 1;  //点赞数量加1
                $comment_data['zan_userid'] = $comment_user_id_string;
                $rs = DI()->notorm->shop_comment
                    ->where("comment_id = '{$id}'")
                    ->update($comment_data);
                //消息提醒
                $user = DI()->notorm->shop_comment
                    ->where("comment_id = '{$id}'")->fetch();
                if ($user['user_id'] != $users['id']) {
                    $lang = getUserInfo($user['user_id']);  //4.21修改语言原因:评论无法!!封!
                    if ($lang['lang'] == 'en') {
                        $push = TPNSPush('Notification', $users['user_nickname'] . ' likes your comment', $user['user_id'], 1, '');
                        $language = 'likes your comment';
                    } elseif ($lang['lang'] == 'ms') {
                        $push = TPNSPush('Notifikasi', $users['user_nickname'] . ' suka komen anda', $user['user_id'], 1, '');
                        $language = 'suka komen anda';
                    } else {
                        $push = TPNSPush('您有新的消息', $users['user_nickname'] . '用户点赞了你的评论', $user['user_id'], 1, '');
                        $language = '用户点赞了你的评论';
                    }

                    //file_put_contents(API_ROOT.'/../record/yun.txt',json_encode($push)."\r\n",FILE_APPEND);
                    $remindData = [
                        'type' => 1,
                        'target' => 'shopComment',
                        'target_id' => $user['shop_id'],
                        'target_owner' => $uid,
                        'add_time' => time(),
                        'action' => '用户点赞了你的评论',
                        'user_id' => $user['user_id'] ? $user['user_id'] : 0,
                    ];
                    DI()->notorm->remind
                        ->insert($remindData);


                }

            }
            $commentNum = DI()->notorm->shop_comment
                ->where("comment_id = '{$id}'")
                ->fetch();
            return $commentNum['zan_num'];
        } else { //帖子
            $rs = DI()->notorm->note_comment->where("comment_id=?", $id)->fetch();
            $comment_user_id_array = explode(',', $rs['zan_userid']);
            if (in_array($uid, $comment_user_id_array)) {  //判断用户有没点赞过
                $key = array_search($uid, $comment_user_id_array);
                if (isset($key)) {
                    unset($comment_user_id_array[$key]);
                }
                $comment_user_id_string = implode(',', $comment_user_id_array);
                $comment_data['zan_num'] = $rs['zan_num'] - 1;  //点赞数量减1
                $comment_data['zan_userid'] = $comment_user_id_string;
                $rs = DI()->notorm->note_comment
                    ->where("comment_id = '{$id}'")
                    ->update($comment_data);

            } else {
                array_push($comment_user_id_array, $uid);
                $comment_user_id_string = implode(',', $comment_user_id_array);
                $comment_data['zan_num'] = $rs['zan_num'] + 1;  //点赞数量加1
                $comment_data['zan_userid'] = $comment_user_id_string;
                $rs = DI()->notorm->note_comment
                    ->where("comment_id = '{$id}'")
                    ->update($comment_data);
                //消息提醒
                $user = DI()->notorm->note_comment
                    ->where("comment_id = '{$id}'")->fetch();

                if ($user['user_id'] != $users['id']) {
                    $lang = getUserInfo($user['user_id']);
                    if ($lang['lang'] == 'en') {
                        $push = TPNSPush('Notification', $users['user_nickname'] . ' likes your comment', $user['user_id'], 1, '');
                        $language = 'likes your comment';
                    } elseif ($lang['lang'] == 'ms') {
                        $push = TPNSPush('Notifikasi', $users['user_nickname'] . ' suka komen anda', $user['user_id'], 1, '');
                        $language = 'suka komen anda';
                    } else {
                        $push = TPNSPush('您有新的帖子点赞消息', $users['user_nickname'] . '用户点赞了你的评论', $user['user_id'], 1, '');
                        $language = '用户点赞了你的评论';
                    }
                    $remindData = [
                        'type' => 1,
                        'target' => 'noteComment',
                        'target_id' => $user['note_id'],
                        'target_owner' => $uid,
                        'add_time' => time(),
                        'action' => '用户点赞了你的评论',
                        'user_id' => $user['user_id'] ? $user['user_id'] : 0,
                    ];
                    DI()->notorm->remind
                        ->insert($remindData);
                }

            }
            $commentNum = DI()->notorm->note_comment->where("comment_id = '{$id}'")->fetch();
            return $commentNum['zan_num'];
        }

    }


    public function getMyComment($uid, $p)
    {
        if ($p < 1) {
            $p = 1;
        }
        $pnums = 10;
        $start = ($p - 1) * $pnums;
        $list = $this->getORM()->where('user_id=?', $uid)
            ->limit($start, $pnums)->order('create_time desc')
            ->fetchAll();
        $list = DI()->notorm->note_comment->where('user_id=?',$uid)
             ->limit($start,$pnums)->order('create_time desc')
              ->fetchAll();
        // if($list && $listTwo){
        //     $list = array_merge($list,$listTwo);
        // }
        foreach ($list as $k => $v) {
            $list[$k]['create_time'] = date("Y-m-d H:i", $v['create_time']);

            $name = getUserInfo($v['user_id']);
            $list[$k]['avatar'] = get_upload_path($name['avatar']) ?: '';
            $list[$k]['img'] = trim($v['img'], ',');
            $list[$k]['user_nickname'] = $name['user_nickname'];
            $zan_userid = explode(',', trim($v['zan_userid'], ','));
            $list[$k]['zan_userid'] = implode(',', $zan_userid);
            $list[$k]['is_like'] = 0;
            if (in_array($uid, $zan_userid)) {
                $list[$k]['is_like'] = 1;
            }
        }
        return $list;
    }

    public function MyComment($sql){
        $list = $this->getORM()->queryAll($sql);
        return $list;
    }


}