<?php

class Model_Quest extends PhalApi_Model_NotORM {

    protected function getTableName($id) {
        return 'user_quest';
    }
    
    public function getQuestExamList() {
        
        $info = DI()->notorm->user_quest
            ->select('id, list_order, title')
            ->where('status = 1')
            ->order('list_order desc')
            ->fetchAll();
        
        return $info;

    }
    
    public function getQuestExam($exam_id,$p) {
        
        if($p<1){
            $p=1;
        }
        $pnums=1;
        $start=($p-1)*$pnums;
        
        $res = DI()->notorm->question
            ->where('paper_id = ?', $exam_id)
            ->limit($start,$pnums)
            ->order('id asc')
            ->fetchAll();
        foreach ($res as &$val){
            $val['content'] = unserialize($val['content']);
          
        }
        return $res;

    }
    
    public function userQuestExam($uid, $exam_id, $exam) {
        
        // var_dump($exam);
        
        $res = $exam;
        
        $exam = json_decode($exam, true);
        
        $data = [];
        $data['uid'] = $uid;
        $data['qid'] = $exam_id;
        $data['title'] = $exam['title'];
        $data['add_time'] = time();
        $data['quest'] = $res;
        
        
        $info = DI()->notorm->user_quest_exam->insert($data);
        
        // $info = DI()->notorm->user_quest_exam
        //     ->where('status = 1')
        //     ->order('list_order desc')
        //     ->fetchAll();
            
        
        return $info;

    }
    
    public function addAnswer($uid,$data){
        $res = DI()->notorm->user_answer
            	->where("user_id='{$uid}' and paper_id='{$data['paper_id']}'")
                ->fetch();
        if($data['type']==0){
            $name='q'.$data['number'];
            
        }elseif ($data['type']==1) {
           $name='d'.$data['number'];
        }elseif($data['type']==3){
           $name='a'.$data['number']; 
        }
        $newData[$name]=$data['answer'];
        if($res){
            DI()->notorm->user_answer
            	->where("user_id='{$uid}' and paper_id='{$data['paper_id']}'")
                ->update($newData);
        }else{
             $newData['paper_id']=$data['paper_id'];
             $newData['user_id']=$uid;
             DI()->notorm->user_answer->insert($newData);
             $config= DI()->notorm->option
					->select('option_value')
					->where("option_name='comment_app'")
					->fetchOne();
    		$config=json_decode($config['option_value'],true);
    	
	     		addMoney($uid,[
    		    'title'=>'回答问卷',
    		    'money'=>$config['questionnaire'],
    		    ]);
    		
   
        }
        return $res;     
    }
    
    
}