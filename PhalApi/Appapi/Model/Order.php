<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/8 0008
 * Time: 13:20
 */
class Model_Order extends PhalApi_Model_NotORM
{

//    protected function getTableName($id)
//    {
//        return 'shop_goods';  // 手动设置表名为
//    }

    public function addOrder($data){
        $this->getORM()->insert($data);
        return $this->getORM()->insert_id();
    }

    public function addOrderGoods($data){
        $res=DI()->notorm->order_goods
            ->insert($data);

        return 1;
    }

    public function reduceGold($uid,$number){
        $rs=DI()->notorm->user
            ->where("id = '{$uid}' and coin>0")
            ->update( array('coin' => new NotORM_Literal("coin - " .$number) ) );
         $money = DI()->notorm->user
            ->where("id=?",$uid)
            ->fetch();    
         return $money['coin'];    

    }
    
    public function addSaleNum($goods_id,$num){
          $rs=DI()->notorm->shop_goods
            ->where('id=?',$goods_id)
            ->update( array('sales_sum' => new NotORM_Literal("sales_sum + " .$num) ) );
         return 1;    
        
    }
    
    public function getLists($uid,$p){
        if($p<1){
            $p=1;
        }
        $pnums=10;
        $start=($p-1)*$pnums;
        $where="A.user_id= ".$uid;//金币兑换的商品
        $list = $this->getORM()
            ->select('A.order_status,A.order_id,A.order_sn,A.total_amount, B.goods_id,B.goods_name,B.goods_price,B.goods_num') // 获取字段
            ->alias('A')
            ->leftJoin('order_goods', 'B', 'A.order_id = B.order_id')
            ->where($where)
            ->limit($start,$pnums)->order('A.order_id desc')
            ->fetchAll();
        return $list;

    }
    
    public function getInfo($id){
        $where="A.order_id= ".$id;//金币兑换的商品
        $list = $this->getORM()
            ->select('A.*, B.goods_id,B.goods_name,B.goods_price,B.goods_num') // 获取字段
            ->alias('A')
            ->leftJoin('order_goods', 'B', 'A.order_id = B.order_id')
            ->where($where)
            ->fetchAll();
        return $list;
    }
    
    public function addLog($data){
        
        $res = DI()->notorm->user_log->insert($data);
        return $res;
    }
    
    public function confirm($id){
        $where="order_id= ".$id;//金币兑换的商品
        $this->getORM()
            ->where($where)->update([
                'order_status'=>3
            ]);
        return;
    }

}