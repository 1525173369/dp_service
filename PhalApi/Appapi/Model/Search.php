<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/12/10 0010
 * Time: 13:55
 */
class Model_Search extends PhalApi_Model_NotORM {

    public function getHot(){


        $res= DI()->notorm->search_word
            ->fetchAll();

        return $res;

    }

    public function getHistory($uid){
     //   echo 11;exit;
     
        $res= DI()->notorm->history_search
            ->where('uid=?',$uid)->order("add_time desc")
            ->fetchAll();

        return $res;

    }
    
    public function clearHistory($uid){


        $res= DI()->notorm->history_search
            ->where('uid=?',$uid)
            ->delete();

        return $res;

    }

}