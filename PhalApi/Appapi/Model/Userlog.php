<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2020/11/29 0029
 * Time: 14:59
 */
class Model_Userlog extends PhalApi_Model_NotORM {


    public function getLists($uid,$status,$p,$lan){

        if($p<1){
            $p=1;
        }
        $pnums=10;
        $start=($p-1)*$pnums;
        $where="uid =".$uid." and type= ".$status." and is_admin = 1 and is_hidden = 0";
        $shop = DI()->notorm->shop_apply->where('uid =?',$uid)->fetchOne();
        if($shop){ //说明有shop_log信息
           $type = 1;
           $wheres = '1=1';
           if($status != 1){
               $type = 2;
               $wheres = '0=1';
           }
         
           $sql = 'select * from (select ordersn,id,title,createtime,money,0 as is_shop from cmf_user_log where is_hidden = 0  and uid='.$uid.' and  is_admin = 1 and type = '.$status.' union select ordersn,id,action as title,createtime,change_coin as money,1  as is_shop  from cmf_shop_log  where type = '.$type.' and shop_id = '.$shop['id'].' union select 998 as ordersn,id,(select title from cmf_user_log where id = cmf_user_forward_coin.user_log_id) as title,createtime,coin as money,0 as is_shop from cmf_user_forward_coin where uid = '.$uid.' and status = 0 and '.$wheres.') s1 order by createtime desc limit '.$start.','.$pnums;
         
           $list = DI()->notorm->user_log->queryall($sql);   
        }else{
            if($status == 0){
                $list=DI()->notorm->user_log
                    ->where($where)
                    ->limit($start,$pnums)
                    ->order('createtime desc')
                    ->fetchAll();
            }else{
                $sql = 'select * from (select ordersn,id,title,createtime,money,0 as is_shop from cmf_user_log where is_hidden = 0 and  uid='.$uid.' and  is_admin = 1 and type = '.$status.' union select 998 as ordersn,id,(select title from cmf_user_log where id = cmf_user_forward_coin.user_log_id) as title,createtime,coin as money,0 as is_shop from cmf_user_forward_coin where uid = '.$uid.' and status = 0) s1 order by createtime desc limit '.$start.','.$pnums; 
                
                
                $list = DI()->notorm->user_log->queryall($sql); 
            }
             
                    
        }
        
       $host = getMyhost();
     
             
		foreach($list as $k=>$v){
		    if(!$v['title']){
		        $list[$k]['title'] =  '新阅读量待入账';
		    }
			$list[$k]['createtime']=date("Y/m/d",$v['createtime']);
		/*	if($lan == 'bm'){
			    if($v['title'] == '阅读帖子'){
                    $v['title'] = 'Jumlah bacaan';
                }
            }
            if($lan == 'en'){
                if($v['title'] == '阅读帖子'){
                    $v['title'] = 'Total views';
                }
            }
            if($lan == 'gb'){
                if($v['title'] == '阅读帖子'){
                    $v['title'] = '阅读量';
                }
            }*/
            $list[$k]['url'] = '';
            if($ordersn = $v['ordersn']){
                // window.location.href='/app/pages/scannel/details.html?ordersn='+data.data.info.ordersn+'&shopid='+data.data.info.shopid+'&value='+data.data.info.money+'&createtime='+data.data.info.createtime+"&remark="+data.data.info.remarks+"&touid="+data.data.info.touid;
                $ordersninfo = DI()->notorm->scannel_order->where('ordersn = ?',$ordersn)->fetchOne();
                if($ordersninfo){
                     #var_dump($ordersninfo,$ordersn);die;
                   
                    $ordersninfo['remarks'] = ($ordersninfo['remarks']);
                    if(!$ordersninfo['touid']){ //分转账和扫码支付店铺,touid为转账给的对象,因为扫码支付没有,所以优先判断
                        $ordersninfo['touid'] = $ordersninfo['shop_uid'];
                    }
                    $url = $host."/app/pages/scannel/details.html?ordersn={$ordersninfo['ordersn']}&shopid={$ordersninfo['shopid']}&value={$ordersninfo['money']}&createtime={$ordersninfo['createtime']}&remark={$ordersninfo['remarks']}&touid={$ordersninfo['touid']}&uids={$ordersninfo['uid']}&shop_uid=".$ordersninfo['shop_uid'];
                   
                    $list[$k]['url'] = $url;
                }
               
            }
            $list[$k]['money'] = number_format($list[$k]['money'],2);
            
           
            
		}    
        return $list;
    }
    
    public function addLog($uid){

        $rs = DI()->notorm->user
            ->where('id=?',$uid)->fetch();
        $readNum =$rs['read']+1;
        DI()->notorm->user
        ->where('id=?',$uid)->update(array('read'=>$readNum));
//         $config= DI()->notorm->option
// 			->select('option_value')
// 			->where("option_name='comment_app'")
// 			->fetchOne();
// 		$config=json_decode($config['option_value'],true);
// 		if($config['num']==$readNum){
//     		   addMoney($data['uid'],[
//     		    'title'=>'阅读帖子',
//     		    'money'=>$config['reading'],
//     		    ]); 
// 		}

        return 1;

    }
    
    





}