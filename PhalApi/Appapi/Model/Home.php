<?php
if (!session_id()) session_start();
class Model_Home extends PhalApi_Model_NotORM {
    protected $live_fields='uid,title,city,stream,pull,thumb,isvideo,type,type_val,goodnum,anyway,starttime,isshop,game_action';
     
    
	/* 轮播 */
	public function getSlide(){

		$rs=DI()->notorm->slide_item
			->select("image as slide_pic,url as slide_url")
			->where("status='1' and slide_id='1' ")
			->order("list_order asc")
			->fetchAll();
		foreach($rs as $k=>$v){
			$rs[$k]['slide_pic']=get_upload_path($v['slide_pic']);
// 			var_dump($v['slide_pic']);
		}				

		return $rs;
    }
    
    public function getReportClassifiers($lan) {
        $select = "id, reason_en as reason";
        switch($lan) {
            case 'gb':
            case 'zh':
            case 'zh-Hans':
                $select = "id, reason";
                break;

            case 'ms':
                $select = "id, reason_ms as reason";
                break;

            default:
                $select = "id, reason_en as reason";
                break;
        }
        $rs=DI()->notorm->report_reason
                ->select($select)
                ->fetchAll();
        return $rs;
    }
	
	
	public function agreement(){
	    
	    $rs=DI()->notorm->agreement->where("id=?",1)->fetch();
	    return $rs;
	    
    }
    public function agreementTwo(){
	    
	    $rs=DI()->notorm->agreement->where("id=?",3)->fetch();
	    return $rs;
	    
    }
    public function agreementThree(){
	    
	    $rs=DI()->notorm->agreement->where("id=?",4)->fetch();
	    return $rs;
	    
	}
	
	
	public function about(){
	    
	    $rs=DI()->notorm->agreement->where("id=?",2)->fetch();
	    if($rs){
	        return $rs;
	    }
	    return [];
	    
	}
	/* 热门 */
    public function getHot($p,$search) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$where=" islive= '1' and ishot='1' ";
        
        if($p==1){
			$_SESSION['hot_starttime']=time();
		}
        
		if($p!=0){
			$endtime=$_SESSION['hot_starttime'];
            if($endtime){
                $where.=" and starttime < {$endtime}";
            }
			
		}
        
        if($p!=1){
			$hotvotes=$_SESSION['hot_hotvotes'];
            if($hotvotes){
                $where.=" and hotvotes < {$hotvotes}";
            }else{
                $where.=" and hotvotes < 0";
            }
			
		}
		
		$result=DI()->notorm->live
                    ->select($this->live_fields.',hotvotes')
                    ->where($where)
                    ->order('isrecommend desc,hotvotes desc,starttime desc')
                    ->limit(0,$pnum)
                    ->fetchAll();
        $arr = [];
		foreach($result as $k=>$v){
            
			$v=handleLives($v,$search);
            
            if(!empty($v)){
                $arr[]=$v;
            }
			
		}
		$result = $arr;
		if(!empty($result)){
			$last=end($result);
			//$_SESSION['hot_starttime']=$last['starttime'];
			$_SESSION['hot_hotvotes']=$last['hotvotes'];
		}

		return $result;
    }
	
		/* 关注列表 */
    public function getFollow($uid,$p) {
        $rs=array(
            'title'=>'你关注的主播没有开播',
            'des'=>'赶快去看看其他主播的直播吧',
            'list'=>array(),
        );
        if($p<1){
            $p=1;
        }
		$result=array();
		$pnum=50;
		$start=($p-1)*$pnum;
		
		$touid=DI()->notorm->user_attention
				->select("touid")
				->where('uid=?',$uid)
				->fetchAll();
				
		if(!$touid){
            return $rs;
        }
        
        $rs['title']='你关注的主播没有开播';
        $rs['des']='赶快去看看其他主播的直播吧';
        $where=" islive='1' ";					
        if($p!=1){
            $endtime=$_SESSION['follow_starttime'];
            if($endtime){
                $start=0;
                $where.=" and starttime < {$endtime}";
            }
            
        }	
    
        $touids=array_column($touid,"touid");
        $touidss=implode(",",$touids);
        $where.=" and uid in ({$touidss})";
        $result=DI()->notorm->live
                ->select($this->live_fields)
                ->where($where)
                ->order("starttime desc")
                ->limit(0,$pnum)
                ->fetchAll();
	
		foreach($result as $k=>$v){
            
			$v=handleLive($v);
            
            $result[$k]=$v;
		}	

		if($result){
			$last=end($result);
			$_SESSION['follow_starttime']=$last['starttime'];
		}
        
        $rs['list']=$result;

		return $rs;					
    }
		
		/* 最新 */
    public function getNew($lng,$lat,$p) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$where=" islive='1' ";

		if($p!=1){
			$endtime=$_SESSION['new_starttime'];
            if($endtime){
                $where.=" and starttime < {$endtime}";
            }
		}
		
		$result=DI()->notorm->live
				->select($this->live_fields.',lng,lat')
				->where($where)
				->order("starttime desc")
				->limit(0,$pnum)
				->fetchAll();	
		foreach($result as $k=>$v){
            
			$v=handleLive($v);
			
			$distance='好像在火星';
			if($lng!='' && $lat!='' && $v['lat']!='' && $v['lng']!=''){
				$distance=getDistance($lat,$lng,$v['lat'],$v['lng']);
			}else if($v['city']){
				$distance=$v['city'];	
			}
			
			$v['distance']=$distance;
			unset($v['lng']);
			unset($v['lat']);
            
            $result[$k]=$v;
			
		}		
		if($result){
			$last=end($result);
			$_SESSION['new_starttime']=$last['starttime'];
		}

		return $result;
    }
		
		/* 搜索 */
    public function search($uid,$key,$p) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;
		$where=' user_type="2" and ( id=? or user_nicename like ?  or goodnum like ? ) and id!=?';
		if($p!=1){
			$id=$_SESSION['search'];
            if($id){
                $where.=" and id < {$id}";
            }
		}
		
		$result=DI()->notorm->user
				->select("id,user_nicename,avatar,sex,signature,consumption,votestotal")
				->where($where,$key,'%'.$key.'%','%'.$key.'%',$uid)
				->order("id desc")
				->limit($start,$pnum)
				->fetchAll();
		foreach($result as $k=>$v){
			$v['level']=(string)getLevel($v['consumption']);
			$v['level_anchor']=(string)getLevelAnchor($v['votestotal']);
			$v['isattention']=(string)isAttention($uid,$v['id']);
			$v['avatar']=get_upload_path($v['avatar']);
			unset($v['consumption']);
            
            $result[$k]=$v;
		}				
		
		if($result){
			$last=end($result);
			$_SESSION['search']=$last['id'];
		}
		
		return $result;
    }
	
	/* 附近 */
    public function getNearby($uid,$lng,$lat,$p) {
        if($p<1){
            $p=1;
        }
		$pnum=10;
		$start=($p-1)*$pnum;
		$where="status='1' ";
		$list=DI()->notorm->dynamic
				//->select($this->live_fields.",getDistance('{$lat}','{$lng}',lat,lng) as distance,province")
		        ->select("*,round((6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - ".$lat." * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(".$lat." * pi() / 180) * pow(sin((lng * pi()/ 180 - ".$lng." * pi() / 180) / 2),2))) * 1),2) as distance,province")
				->where($where)
                ->order("distance asc")
                ->limit($start,$pnum)
				->fetchAll();	
		foreach($list as $key=>$value){
            
			    $list[$key]['isAttn'] = getUserNoteAtte($uid, $value['id']);
                // 帖子点赞总数
                $list[$key]['attns'] = getNoteAttes($value['id']);
                // 用户是否收藏帖子
                $isCollect = getUserNoteColl($uid, $value['id']);
                $list[$key]['isCollect'] = $isCollect ? 1 : 0;
                
                $name = getUserInfo($value['uid']);
                $list[$key]['avatar']=get_upload_path($name['avatar']) ?:'';
                $list[$key]['user_nickname'] = $name['user_nickname'];
                
            	$value['thumb']=explode(",",$value['thumb']);
            	
            	foreach ($value['thumb'] as $k => $v) {
            	    $value['thumb'][$k] = get_upload_path($v);
            	}   
                $list[$key]['thumb'] = $value['thumb'];
                
                //获取帖子话题
                $list[$key]['custom'] = getShopTopic($value['custom']) ?: [];
                $list[$key]['topic'] = getShopTopic($value['topic']) ?: [];
		}
		
		return $list;
    }



	/* 推荐 */
	public function getRecommend($uid){

		$result=DI()->notorm->user
				->select("id,user_nicename,avatar,avatar_thumb")
				->where("isrecommend='1' AND id NOT IN (SELECT dynamic_id FROM cmf_user_hide_dynamic WHERE uid=?", $uid)
				->order("votestotal desc")
				->limit(0,12)
				->fetchAll();
		foreach($result as $k=>$v){
			$v['avatar']=get_upload_path($v['avatar']);
			$v['avatar_thumb']=get_upload_path($v['avatar_thumb']);
			$fans=getFans($v['id']);
			$v['fans']='粉丝 · '.$fans;
            
            $result[$k]=$v;
		}
		return  $result;
	}
	/* 关注推荐 */
	public function attentRecommend($uid,$touids){
		//$users=$this->getRecommend();
		//$users=explode(',',$touids);
        //file_put_contents('./attentRecommend.txt',date('Y-m-d H:i:s').' 提交参数信息 touids:'.$touids."\r\n",FILE_APPEND);
        $users=preg_split('/,|，/',$touids);
		foreach($users as $k=>$v){
			$touid=$v;
            //file_put_contents('./attentRecommend.txt',date('Y-m-d H:i:s').' 提交参数信息 touid:'.$touid."\r\n",FILE_APPEND);
			if($touid && !isAttention($uid,$touid)){
				DI()->notorm->user_black
					->where('uid=? and touid=?',$uid,$touid)
					->delete();
				DI()->notorm->user_attention
					->insert(array("uid"=>$uid,"touid"=>$touid));
			}
			
		}
		return 1;
	}

	/*获取收益排行榜*/
	public function profitList($uid,$type,$p){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;

		switch ($type) {
			case 'day':
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";

			break;

			case 'week':
                $w=date('w'); 
                //获取本周开始日期，如果$w是0，则表示周日，减去 6 天 
                $first=1;
                //周一
                $week=date('Y-m-d H:i:s',strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days')); 
                $week_start=strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days'); 

                //本周结束日期 
                //周天
                $week_end=strtotime("{$week} +1 week")-1;
                
				$where=" addtime >={$week_start} and addtime<={$week_end} and ";

			break;

			case 'month':
                //本月第一天
                $month=date('Y-m-d',strtotime(date("Ym").'01'));
                $month_start=strtotime(date("Ym").'01');

                //本月最后一天
                $month_end=strtotime("{$month} +1 month")-1;

				$where=" addtime >={$month_start} and addtime<={$month_end} and ";

			break;

			case 'total':
				$where=" ";
			break;
			
			default:
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";
			break;
		}




		$where.=" action in (1,2)";
		
		$result=DI()->notorm->user_voterecord
            ->select('sum(total) as totalcoin, uid')
            ->where($where)
            ->group('uid')
            ->order('totalcoin desc')
            ->limit($start,$pnum)
            ->fetchAll();

		foreach ($result as $k => $v) {
            $userinfo=getUserInfo($v['uid']);
            $v['avatar']=$userinfo['avatar'];
			$v['avatar_thumb']=$userinfo['avatar_thumb'];
			$v['user_nicename']=$userinfo['user_nicename'];
			$v['sex']=$userinfo['sex'];
			$v['level']=$userinfo['level'];
			$v['level_anchor']=$userinfo['level_anchor'];
			$v['totalcoin']=(string)intval($v['totalcoin']);
            
            $v['isAttention']=isAttention($uid,$v['uid']);//判断当前用户是否关注了该主播
            
            $result[$k]=$v;
		}

		return $result;
	}



	/*获取消费排行榜*/
	public function consumeList($uid,$type,$p){
        if($p<1){
            $p=1;
        }
		$pnum=50;
		$start=($p-1)*$pnum;

		switch ($type) {
			case 'day':
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";

			break;
            
            case 'week':
                $w=date('w'); 
                //获取本周开始日期，如果$w是0，则表示周日，减去 6 天 
                $first=1;
                //周一
                $week=date('Y-m-d H:i:s',strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days')); 
                $week_start=strtotime( date("Ymd")."-".($w ? $w - $first : 6).' days'); 

                //本周结束日期 
                //周天
                $week_end=strtotime("{$week} +1 week")-1;
                
				$where=" addtime >={$week_start} and addtime<={$week_end} and ";

			break;

			case 'month':
                //本月第一天
                $month=date('Y-m-d',strtotime(date("Ym").'01'));
                $month_start=strtotime(date("Ym").'01');

                //本月最后一天
                $month_end=strtotime("{$month} +1 month")-1;

				$where=" addtime >={$month_start} and addtime<={$month_end} and ";

			break;

			case 'total':
				$where=" ";
			break;
			
			default:
				//获取今天开始结束时间
				$dayStart=strtotime(date("Y-m-d"));
				$dayEnd=strtotime(date("Y-m-d 23:59:59"));
				$where=" addtime >={$dayStart} and addtime<={$dayEnd} and ";
			break;
		}

		$where.=" type=0 and action in ('1','2')";
		
        $result=DI()->notorm->user_coinrecord
            ->select('sum(totalcoin) as totalcoin, uid')
            ->where($where)
            ->group('uid')
            ->order('totalcoin desc')
            ->limit($start,$pnum)
            ->fetchAll();

		foreach ($result as $k => $v) {
            $userinfo=getUserInfo($v['uid']);
            $v['avatar']=$userinfo['avatar'];
			$v['avatar_thumb']=$userinfo['avatar_thumb'];
			$v['user_nicename']=$userinfo['user_nicename'];
			$v['sex']=$userinfo['sex'];
			$v['level']=$userinfo['level'];
			$v['level_anchor']=$userinfo['level_anchor'];
            
            $v['isAttention']=isAttention($uid,$v['uid']);//判断当前用户是否关注了该主播
            
            $result[$k]=$v;
		}

		return $result;
	}
    
    /* 分类下直播 */
    public function getClassLive($liveclassid,$p) {
        if($p<1){
            $p=1;
        }
		$pnum=50;
		//$start=($p-1)*$pnum;
		$start=0;
		$where=" islive='1' and liveclassid={$liveclassid} ";
        
		if($p!=1){
			$endtime=$_SESSION['getClassLive_starttime'];
            if($endtime){
                $where.=" and starttime < {$endtime}";
            }
			
		}
		$last_starttime=0;
		$result=DI()->notorm->live
				->select($this->live_fields)
				->where($where)
				->order("starttime desc")
				->limit(0,$pnum)
				->fetchAll();	
		foreach($result as $k=>$v){
			$v=handleLive($v);
            $result[$k]=$v;
		}
		if($result){
            $last=end($result);
			$_SESSION['getClassLive_starttime']=$last['starttime'];
		}

		return $result;
    }
    
    //获取推荐店铺
    public function getHotShop($uid, $P) {
        if($p<1){
            $p=1;
        }
		$pnum=20;
		$start=($p-1)*$pnum;
        
        $userRecom = DI()->notorm->user_recom
                ->select('uid, recom')
                ->limit(1)->order('rand()')
                // ->where('uid = ?', $uid)
                ->fetchOne();
                
        $userRecoms = explode(',', $userRecom['recom']);
        // var_dump($userRecoms);
        
        $recomShop = [];
        foreach ($userRecoms as $key => $value) {
            // $recomShop = [];
            $res = DI()->notorm->shop_apply
                    ->select('uid, name, thumb, des, topic, points, views')
                    ->where('status = 1 and classid = ?', $value)
                    ->fetchAll();
            
            foreach ($res as $k => $v) {
                
                //判断用户是否关注了店铺主播
                $isattention=isAttention($uid,$res[$k]['uid']);
                $res[$k]['isattention']=$isattention;
                $userinfo = getUserInfo($res[$k]['uid']);
                
                $res[$k]['user_nickname'] = $userinfo['user_nickname'];
                $res[$k]['avatar'] = get_upload_path($userinfo['avatar']);
                $res[$k]['avatar_thumb'] = get_upload_path($userinfo['avatar_thumb']);
                
                //获取店铺点赞信息
                $shop_fabulous = getShopFabulous($res[$k]['uid'], $uid);
                $res[$k]['fabulous'] = $shop_fabulous['fabulous'];
                $res[$k]['isfabulous'] = $shop_fabulous['isfabulous'];
            
            
                //获取店铺话题
                $res[$k]['topic'] = getShopTopic($res[$k]['topic']);

                // var_dump($res); die;
            }
            if($res) $recomShop[] = $res;
            // var_dump($res); die;
            
                
        }
        
        $recomShop = array3_to_array2($recomShop);
        
        return $recomShop;
        
        var_dump($recomShop);
        // var_dump($userRecom);
        die;
        
    }
    
    
    public function searchHome($uid,$keyword,$p,$lat,$lng,$catId){
        if($p<1){
            $p=1;
        }
        $pnums=10;
        $start=($p-1)*$pnums;
        $where="status=1 ";
        
        if($keyword){
            $where1 = "name LIKE '%{$keyword}%'";
            $sIds = DI()->notorm->shop_apply->where($where1)->select('id')->fetchAll();
            $new = [];
            foreach ($sIds as $val){
                $new[] = $val['id'];
            }
            $sIds=implode(",",$new);
            if($sIds){
                  //  print_r($sIds);
                 $where.= " and (shop_id in ({$sIds}) or title like '%".$keyword."%')" ;
            }else{
                 $where.=" and title like '%".$keyword."%'";
            }
           
        }
        
        
        
        if($keyword){

          //  $where.=" and title like '%".$keyword."%'";
        }
        if($catId){
            $where.=" and cat_id=".$catId;
        }
        $list=DI()->notorm->dynamic
       // ->select("*,getDistance('{$lat}','{$lng}',lat,lng) as distance")
        ->select("*,round((6378.138 * 2 * asin(sqrt(pow(sin((lat * pi() / 180 - ".$lat." * pi() / 180) / 2),2) + cos(lat * pi() / 180) * cos(".$lat." * pi() / 180) * pow(sin((lng * pi()/ 180 - ".$lng." * pi() / 180) / 2),2))) * 1),2) as distance,province")
        ->where($where)->order("distance asc")
        ->limit($start,$pnums)
        ->fetchAll();
        foreach($list as &$val){
            if($val['thumb']){
            	$thumb_arr=explode(',',$val['thumb']);
    	        foreach ($thumb_arr as $k => $v) {
    	        	$thumb_arr[$k]=get_upload_path($v);
    	        }
    	         $val['thumb']= $thumb_arr;   
            }
             if($val['uid']){
                $val['info']=getUserInfo($val['uid']);
                $val['info']['avatar'] = get_upload_path($val['info']['avatar']);
                $val['info']['avatar_thumb'] = get_upload_path($val['info']['avatar_thumb']);
            }
             $val['custom'] = getShopTopic($val['custom']) ?: [];
             $val['topic'] = getShopTopic($val['topic']) ?: [];
             $val['isCollect']  = getUserNoteColl($uid, $val['id']) ? 1 : 0;
             $val['isAttn']  = getUserNoteAtte($uid, $val['id']);
          
        }  
        //插入搜索记录
        $history = DI()->notorm->history_search->where([
            'uid'=>$uid,
            'keyword'=>$keyword
            ])->fetch();
        if($history){
            DI()->notorm->history_search->where([
            'uid'=>$uid,
            'keyword'=>$keyword
            ])->update(
                ['num'=>$history['num']+1]);
            
        }else{
            DI()->notorm->history_search->insert([
                'uid'=>$uid,
                'keyword'=>$keyword,
                'add_time'=>time(),
            ]);
        }    

        return $list;
        
    }
    
    
    public function qiniuConfig() {
        $info = DI()->notorm->plugin
        ->where('id = ?' , 2)
        ->fetchOne();
        
        return json_decode($info['config']);
    }
    
    
    
    
    
    
}
