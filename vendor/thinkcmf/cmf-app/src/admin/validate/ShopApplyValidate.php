<?php
namespace app\admin\validate;

use think\Validate;

class ShopApplyValidate extends Validate
{
    protected $rule = [
        'name'=>'require',
        'thumb' => 'require',
        'topic'=>'require',
        'thumb'=>'require',
        'service_phone'=>'require',
        'classid'=>'require',
    ];

    protected $message = [
        'name.require' => '店铺名称必须',
        'classid.require' => '店铺分类必须',
        'thumb.require' => '店铺主图必须',
        'topic.require' => '请选择话题',
        'thumb.require' => '店铺封面图',
        'service_phone.require' => '店铺电话请填写',
    ];


}