<?php
namespace app\admin\validate;

use think\Validate;

class ShopGoodsValidate extends Validate
{
    protected $rule = [
        'goods_name' => 'require',
        'store_count'=>'require',
        'cost_price'=>'require',
        'original_img'=>'require',
        'gold'=>'require',
        'goods_remark'=>'require',
    ];

    protected $message = [
        'goods_name.require' => '商品名称必须',
        'store_count.require' => '库存必须',
        'cost_price.require' => '成本价必须',
        'gold.require' => '金币价必须',
        'original_img.require' => '主图必须',
        'goods_remark.require' => '简介必须',
    ];


}